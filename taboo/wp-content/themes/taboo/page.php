<?php get_header(); ?>
<div class="c-content change-stage-color c-content--default">
<div class="o-wrapper o-wrapper--1000">
	<main id="Main" class="c-main-content o-main">
	  <?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	    <h1 class="u-alpha"><?php the_title(); ?></h1>
	    <div class="c-cms-content">
	      <?php the_content(); ?>
	    </div>
	    <?php wp_link_pages(); ?>
	  </article>
	  <?php endwhile; ?>
	</main>
</div>
</div>
<?php get_footer(); ?>