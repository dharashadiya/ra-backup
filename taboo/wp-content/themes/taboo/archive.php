<?php get_header(); ?>

<main class="c-main-content o-main u-lap-two-thirds  u-wall-three-quarters" role="main">
  <article <?php post_class(); ?>>
    <?php
      $blog_page = get_page_by_path("sample-page");
    ?>
    <h1 class="u-alpha u-alpha--main"><?php echo $blog_page->post_title; ?></h1>
    <div class="c-cms-content">
      <?php echo apply_filters('the_content', $blog_page->post_content); ?>
      <h2><?php the_archive_title(); ?></h2>
    </div>
    <?php get_template_part( 'loop', 'row' ); ?>
  </article>
</main>

<?php get_footer(); ?>