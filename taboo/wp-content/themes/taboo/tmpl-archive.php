<?php
/*
 * Template Name: Archive
 */
?>

<?php get_header(); ?>
<div class="c-archive">
  <div class="c-section--archive o-wrapper">

  <?php
    $project_args = array(
      'post_type' => 'ra-project',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'order_by' => 'date',
      'order' => 'DESC',
      // 'meta_key' => 'featured_project',
      // 'meta_value'  => true
    );
    $get_projects = new WP_Query( $project_args );

    $terms = get_terms('ra-project-category');
    $categories = array();
    foreach ($terms as $term ) {
      $categories[] = array(
        'name' => $term->name,
        'term_id' => $term->term_id,
        'slug' => $term->slug,
        'order' => get_field('tax_order', $term)
      );
    }
    usort($categories, function($a, $b) {
      return $a['order'] - $b['order'];
    });
  ?>

  <?php
    if ( $get_projects->have_posts() ) :
    ?>
      <div class="c-archive__container o-layout o-layout--large">
      <?php
        while ( $get_projects->have_posts() ) : $get_projects->the_post();

        // $categories = array();
        // if ( get_the_terms($post, 'ra-project-category') ) {
        //   foreach ( get_the_terms($post, 'ra-project-category') as $category) {
        //     $categories[] = $category->slug;
        //   }
        // }
        // $category = implode(" ", $categories);
        // $categoryvisible = implode(", ", str_replace("-"," ",$categories));

        $title = get_field('project_title');
        $client = get_field('client_name');
        $tagline = get_field('client_tagline');

      ?><div class="o-layout__item u-lap-one-half">
          <a class="c-archive__content <?php echo $category; ?>" href="<?php echo get_the_permalink(); ?>">
            <h2><strong><?php echo $client; ?></strong> – <?php echo $title; ?></h2>
            <h3 class="c-archive__tagline"><?php echo $tagline; ?></h3>
            <h3><?php echo get_the_date(); ?></h3>
          </a>
        </div><?php
        endwhile;
      ?>
      </div>
    <?php
    endif;
    wp_reset_postdata();
  ?>

  </div>  
</div>

<?php get_footer(); ?>