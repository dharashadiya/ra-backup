<?php
/*
 * Template Name: Our People
 */
?>

<?php get_header(); ?>
<div class="c-team u-footer-spacing">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <span class="cs-fadeup">
        <span class="c-team__people-side cs-side-text ">Our Team</span>
    </span>
    <div class="c-team__content cs-fadeup">
        <h1>Our People</h1>
        <?php the_content() ?>
    </div>
    <div class="c-team__group-photo rellax">
        <div class="c-team__gradient"></div>
        <div class="c-slider">
            <?php if (have_rows('carousel')) while (have_rows('carousel')): the_row(); ?>
                <img src="<?php echo the_sub_field(image)['url']; ?>" alt="">
            <?php endwhile; ?>
        </div>
    </div>
    <div class="change-stage-color">
        <div class="c-team__members">
            <!-- <span class="c-team__management-side cs-side-text"><?php the_field('team_member_title') ?></span> -->
            <div class="o-layout">
                <?php
                    if (have_rows('team_member')) while (have_rows('team_member')): the_row();
                    $name = get_sub_field('name');
                    $name_clean = str_replace(' ', '-', trim(strtolower($name)) );
                    $name_clean = str_replace('\'', '-', $name_clean );
                    $image = get_sub_field('image');
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    $row_index = get_row_index();
                    ?><a data-bio="<?php echo $name_clean ?>" href="javascript:void(0)" class="cs-fadeup c-team__single-member o-layout__item u-desk-one-fifth cs-bio">
                        <div class="c-team__photo-container">
                            <?php if($row_index == 6): ?>
                                <!-- <span class="c-team__management-side-2 cs-side-text"><?php the_field('team_member_title') ?></span> -->
                            <?php endif; ?>
                            <img src="<?php echo $image['url'] ?>" alt="">
                        </div>
                        <h3><?php echo $name ?></h3>
                        <p><?php echo $title ?></p>
                </a><div class="c-bio-overlay cs-bio-<?php echo $name_clean ?>" style="display: none;">
                    <div class="c-bio-overlay__content">
                        <h3><?php echo $name ?></h3>
                        <span><?php echo $title ?></span>
                        <?php echo $description ?>
                    </div>
                </div><?php endwhile; ?>
            </div>
        </div>
    </div>
    <?php endwhile; ?>
</div>

<?php get_footer(); ?>