<!DOCTYPE html>
<?php
$black_theme = "";
if (is_page(array('contact', 'about'))) {
  $black_theme = "black-theme";
} ?>
<html class="no-js <?php echo $black_theme; ?>" <?php language_attributes(); ?> dir="ltr">

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<!-- make IE render page content in highest IE mode available -->
	<title>
		<?php wp_title(); ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">
	<?php include('partials/inlinescripts.min.php'); ?>
	<?php wp_head(); ?>
	<script type="application/ld+json">
		{
			"@context": "https://schema.org",
			"@type": "Organization",
			"name": "The Taboo Group",
			"url": "http://www.taboo.com.au",
			"sameAs":
			[
				"https://www.facebook.com/thetaboogroup/",
				"https://www.instagram.com/the_taboo_group/",
				"https://www.linkedin.com/company/the-taboo-group/"
			]
		}
	</script>
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window,document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '182301269015667');
		fbq('track', 'PageView');
	</script>
	<noscript>
	<img height="1" width="1"
	src="https://www.facebook.com/tr?id=182301269015667&ev=PageView
	&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
</head>
<?php
	if (is_front_page() || get_page_template() === 'page.php' || get_page_template_slug() == 'tmpl-shopify.php') {
		$alt_header_class = 'c-header--dark';
	} else {
		$alt_header_class = '';
	}

	$landing = $_COOKIE["hideLanding"];
	$show_landing = true;
	if ($landing == "true") {
		$show_landing = false;
		$alt_header_class = '';
	}
?>


<body <?php body_class(); ?>>
	<?php if (is_front_page() && $show_landing ) : ?>
		<div class="c-landing">
			<div class="c-landing__hero">
				<img src="<?php echo ASSETS ?>/img/taboo-intro-animation-2.gif" alt="Taboo Landing Animation">
			</div>
			<a class="o-btn c-landing__showreel  fancybox-video fancybox.iframe" onclick="ga('send', 'event', 'Video', 'play', 'Watch Video from Nav')" href="<?php the_field('video_link', 'options') ?>?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;width=820&amp;height=461&amp;TB_iframe=true">
				<?php svgicon('play-arrow', '0 0 9.06 13.38'); ?>
				Play Showreel
			</a>
			<a class="c-landing__scroll cs-hide-landing" href="javascript:void(0)" data-target="#projects">
				<div class="c-landing__arrows">
					<?php svgicon('single-arrow', '0 0 7.66 4.16'); ?>
					<?php svgicon('single-arrow', '0 0 7.66 4.16'); ?>
					<?php svgicon('single-arrow', '0 0 7.66 4.16'); ?>
				</div>
				Scroll for Work
			</a>
		</div>
	<?php endif; ?>
	<?php if ( !(is_singular('ra-project')) ): ?>
		<div class="c-load-overlay"></div>
	<?php endif; ?>
	<div class="c-top">
		<header class="c-header <?php echo $alt_header_class ?>">
			<div class="o-wrapper">
				<!-- <div class="c-header__side-text-mask"></div> -->
				<div class="c-header__logo">
					<a href="/">
						<?php svgicon('inverted-t', "0 0 52.5 64"); ?>
					</a>
				</div>
				<div class="c-header__title">
					<a href="/">
						<?php svgicon('aboo', '0 0 125.28 26'); ?>
					</a>
				</div>
				<a href="javascript:void(0);" class="cs-toggle-menu c-header__hamburger"><span class="toggle-menu"></span>&nbsp;</a>
			</div>
		</header>
		<div class="c-nav">
			<div class="c-nav__wrapper">
				<?php
					$header_nav = wp_nav_menu( array(
						'theme_location' => 'primary-menu',
						'container' => false,
						'menu_class' => 'c-nav__menu',
						'menu_id' => 'menu',
						'echo' => false,
					));
					$showreel_link = '<a class="fancybox-video fancybox.iframe" onclick="ga(\'send\', \'event\', \'Video\', \'play\', \'Watch Video from Nav\')" href="' . get_field('video_link', 'options') . '?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;width=820&amp;height=461&amp;TB_iframe=true">' . get_field('video_button', 'options') . '</a>';
					$header_nav = str_replace('<a href="#">[showreel]</a>', $showreel_link, $header_nav);
					echo $header_nav;
				?>
				<div class="c-nav__socials">
					<a href="https://instagram.com/<?php the_field('instagram_handle', 'options') ?>">
						<?php echo svgicon('instagram', '0 0 34 34') ?>
					</a>
					<a href="https://linkedin.com/company/<?php the_field('linkedin_handle', 'options') ?>">
						<?php echo svgicon('linkedin', '0 0 34 34') ?>
					</a>
				</div>
			</div>
		</div>
	</div>
