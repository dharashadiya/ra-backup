<?php
/*
 * Template Name: About
 */
?>

<?php get_header(); ?>
<div class="c-about u-footer-spacing">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="c-about__container cs-fadeup">
		<span class="c-about__about-side cs-side-text">About</span>
		<div class="c-about__intro">
			<?php svgicon('inverted-t', '0 0 52.5 64'); ?>
			<?php the_content(); ?>
		</div>
	</div>
	<div class="c-about__photo">
		<div class="c-about__gradient"></div>
		<?php $agency_photo = get_field('agency_photo') ?>
		<video autoplay loop muted playsinline width="100%">
			<source src="<?php echo ASSETS ?>'/video/taboo-about-droneshot-adweek-top-100-V02.mp4'" type="video/mp4"/>
		</video>
	</div>
	<div class="c-about__container cs-fadeup">
		<span class="c-about__we-do-side cs-side-text"><?php the_field('what_we_do_title') ?></span>
		<div class="c-about__we-do">
			<h2><?php the_field('what_we_do_title') ?></h2>
			<?php the_field('what_we_do_description') ?>
			<?php $i = 0 ?>
			<div class="c-about__we-do-list">
				<p>
					<?php if (have_rows('what_we_do')) while (have_rows('what_we_do')): the_row();?>
						<span><?php the_sub_field('what_we_do_item') ?></span>
						<?php $i++; ?>
						<?php if ($i < count(get_field('what_we_do'))) {
							echo " // ";
						} ?>
					<?php endwhile; ?>
				</p>
			</div>
		</div>
	</div>
	<div class="c-about__container cs-fadeup">
		<span class="c-about__clients-side cs-side-text">Clients</span>
		<div class="c-about__clients">
			<h2><?php the_field('clients_title') ?></h2>
			<?php the_field('clients_description') ?>
			<div class="o-layout o-layout--flush">
				<?php 
					if (have_rows('clients')) while (have_rows('clients')): the_row();
				?><div class="o-layout__item u-one-half u-lap-one-quarter">
					<?php $image = get_sub_field('client_image') ?>
					<img src="<?php echo $image['url'] ?>" alt="">
				</div><?php endwhile; ?>
			</div>
		</div>
	</div>
	<?php endwhile; ?>
</div>

<?php get_footer(); ?>