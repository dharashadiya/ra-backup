<?php
    $old_campaigns = new WP_Query();

    $orig_query = $wp_query;
    
    $old_campaigns->query(
        array(
            'taxonomy' => 'category',
            'category_name' => 'campaign-museum',
            'post_type' => "ra-project",
            'order' => 'ASC',
            'orderby' => 'menu_order'
        )
    );
    $wp_query = $old_campaigns;
?>

<?php if ( have_posts() ) : ?>
    <div class="change-stage-color change-stage-color--white-bg">
        <div class="c-project-list__container u-footer-spacing o-layout o-layout--flush" id="projects">
        <?php 
            $i = 1;
            $row = 1;
            while ( have_posts() ) : the_post();
                $title = get_the_title();
                $client = get_field('client_name');
                $row_class = 'row-' . $row;
                if ($i % 4 == 0) {
                    $row++;
                }
                ?><a class="cs-fadeup c-project-list o-layout__item u-desk-one-quarter u-lap-one-half" href="<?php echo get_the_permalink(); ?>">
                <div class="c-project-list__box <?php echo $row_class . ' ' . $animate; ?>">
                    <div class="c-project-list__thumbnail-shift">
                    <?php
                        echo get_the_post_thumbnail($post, 'project-thumb', array('class' => 'c-project-list__thumbnail'));
                    ?>
                    </div>
                    <div data-tilt class="c-project-list__content">
                        <div class="c-project-list__info">
                            <?php if (get_field('client_logo_dark')): ?>
                            <img class="c-project-list__image" src="<?php echo get_field('client_logo_dark')['url']; ?>" alt="<?php echo get_field('client_logo_dark')['alt']; ?>" />
                            <?php endif ?>
                            <div class="c-project-list__text">
                            <h2><strong><?php echo $client; ?></strong> <?php echo $title; ?></h2>
                            </div>
                        </div>
                    </div>
                    <div class="c-project-list__spaceholder"></div>
                </div>
                </a><?php endwhile; // End the loop. 
            ?>
        </div>
    </div>
<?php endif; ?> 
<?php 
    $wp_query = $orig_query;
?>