<?php $projects = get_field('featured_projects'); ?>
<div class="change-stage-color change-stage-color--white-bg">
    <div class="c-project-list__container u-footer-spacing o-layout o-layout--flush" id="projects">
        <?php
        $i = 1;
        $row = 1;
		foreach ($projects as $feature) :
            $project = $feature['project'];
            $row_class = 'row-' . $row;
            if ($i % 4 == 0) {
            	$row++;
            }
			$item = array();
			if ($feature['feature'] && $feature['feature'] === 'Custom') {
				$custom_feature = $feature['custom_feature'][0];
				$item['link'] = $custom_feature['link'];
				$item['thumb_url'] = $custom_feature['background_image'];
				$item['thumb'] = '<img src="' . ASSETS . '/img/1x1.trans.gif" data-lazy-src="' . $custom_feature['background_image'] . '" class="c-project-list__thumbnail">';
				$item['title'] = $custom_feature['title'];
            	$item['logo'] = $custom_feature['logo'];
			} else {
				$item['link'] = get_the_permalink($project);
				$item['thumb_url'] = get_the_post_thumbnail_url($project, 'project-thumb');
				$item['thumb'] = get_the_post_thumbnail($project, 'project-thumb', array('class' => 'c-project-list__thumbnail'));
				$item['title'] = get_field('project_title', $project);
            	$item['client'] = get_field('client_name', $project);
				if (get_field('client_logo_dark', $project)) {
					$item['logo'] = get_field('client_logo_dark', $project)['url'];
				}
			}

            ?><a class="cs-fadeup c-project-list o-layout__item u-desk-one-quarter u-lap-one-half" href="<?php echo $item['link']; ?>">
            <div class="c-project-list__box <?php echo $row_class . ' ' . $animate; ?>">
                <div class="c-project-list__thumbnail-shift">
                <?php if ($landingstatus): ?>
                    <img src="<?php echo ASSETS; ?>/img/1x1.trans.gif" delayedsrc="<?php echo $item['thumb_url']; ?>" alt="<?php echo $item['client']; ?> <?php echo $item['title']; ?>">
                <?php else:
                    echo $item['thumb'];
                endif ?>
                </div>
                <div data-tilt class="c-project-list__content">
                    <div class="c-project-list__info">
                        <?php if ($item['logo']): ?>
                        	<img class="c-project-list__image" src="<?php echo $item['logo']; ?>" alt="<?php echo $item['client']; ?>" />
                        <?php endif ?>
                        <div class="c-project-list__text">
							<h2><?php if ($item['client']) : ?><strong><?php echo $item['client']; ?></strong><?php endif; ?><?php echo $item['title']; ?></h2>
                        </div>
                    </div>
                </div>
                <div class="c-project-list__spaceholder"></div>
            </div>
            </a><?php
        endforeach;
        ?>
        <?php if (get_field('museum_link')) :?>
            <div class="c-home__museum">
                <a class="o-btn o-btn--museum" href="<?php the_field('museum_link') ?>" alt="link to <?php the_field('link_text') ?>"><?php the_field('link_text') ?></a>
            </div>
        <?php endif; ?>
    </div>
</div>