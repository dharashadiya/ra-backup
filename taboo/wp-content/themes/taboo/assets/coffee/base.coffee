( ($) ->
  window.console or= {log: -> }
  window.site or= {}

  $.extend site,
    init_vars: ->
      @body = $('body')
      @root = $('html')
      @header = $('#Top')
      @main = $('#Main')
      @side = $('#Secondary')
      @siteurl = $('#Siteurl').attr('content')
      @themeurl = $('#Themeurl').attr('content')
      @tilt

    onReady: ->
      @init_vars()
      @unveil()
      @fadeInUpAnimation()
      @gallery()
      @fancybox()
      @aboutContent()
      @imageShift()
      @bannerParallaxProject()
      @menuToggle()
      @scrollToEl()
      @tColorTracker()
      # @sideTextReveal()
      @topNavScrollReveal()
      @hideLandingOnScroll()
      @rellax()
      @tilt()
      @relatedImageShift()
      @pageLoadOut()
      @profileBio()
      # if $('.c-stage').length > 0
      # pre 2019 sites staging function
      # @stage()
      @projectMenu()
      @slider()

    onLoad: ->
      @menuLinkNoReload()

    slider: ->
      if $('.c-slider').length > 0
        $('.c-slider').slick(
          autoplay: true
          autoplaySpeed: 4000
          arrows: false
        )
      if $('.c-shopify__product-slider').length > 0
        $('.c-shopify__product-slider').slick(
          autoplay: true
          autoplaySpeed: 4000
          arrows: false
          dots: true
        )

    profileBio: ->
      # if $('.cs-bio').length > 0
      $(document).on 'click', '.cs-bio', ->
        targetBio = $(this).attr('data-bio')
        $('html').addClass('scroll-lock--bio')
        $(".cs-bio-#{targetBio}").fadeIn(200)
        $('.c-header').hide()

        # $(document).on 'click', '.c-bio-overlay', (e) ->
      $('.c-bio-overlay').on 'click', (e) ->
        if e.target != this
          return
        $('.c-header').show()
        $('html').removeClass('scroll-lock--bio')
        $(".c-bio-overlay").fadeOut(200)


    tilt: ->
      if $('.cs-tilt').length > 0
        @tilt = $('.cs-tilt').tilt( {
          perspective: 100
        })

    rellax: ->
      if $('.rellax').length > 0
        rellax = new Rellax ('.rellax')

    pageLoadOut: ->
      if $('.c-load-overlay').length > 0

        cntrlOrCmdIsPressed = false
        $(document).keydown (event) ->
          if event.which == 17 || event.which == 91
            cntrlOrCmdIsPressed = true
        $(document).keyup ->
          cntrlOrCmdIsPressed = false

        $(document).on 'click', 'a', (e) ->
          url = $(this).attr('href')
          if url && url == '/'
            url = site.siteurl
          else if url && url.startsWith('/')
            url = site.siteurl + url

          if url.includes(location.hostname) && cntrlOrCmdIsPressed == false
            e.preventDefault()
            unless url.includes('/project/')
              $('.c-load-overlay').addClass('c-load-overlay--visible')
            setTimeout (->
              window.location.href = url
            ), 400

    scrollToEl: ->
      $(document).on 'click', '.cs-scrollto', (e) ->
        e.preventDefault()
        target = $(this).attr('data-target')
        $('html, body').animate({
          scrollTop: $(target).position().top
        }, 400)

    menuLinkNoReload: ->
      $(document).on 'click', '#menu-item-1099', ->
        console.log('####################')
        $('.c-landing').css('display', 'none');

    fadeInUpAnimation: ->
      if $(window).width() > 768
        $('.cs-fadeup').addClass('hide-me').viewportChecker
          classToAdd: 'visible animated fadeInUp'
          offset: 100
          # repeat: true

    menuToggle: ->
        $(document).on 'click', '.cs-toggle-menu', ->
          hasAltStyle = false
          if $('.c-header').hasClass('c-header--dark')
            hasAltStyle = true

          if $(this).hasClass('is-open')
            if hasAltStyle == true
              $('.c-header').addClass('c-header--dark')
            $("body, html").removeClass('scroll-lock')
            $('.cs-toggle-menu').removeClass('is-open')
            $('.c-header').removeClass('nav-open')
            $('.c-nav').fadeOut(200)
          else
            if hasAltStyle == true
              $('.c-header').removeClass('c-header--dark')
            $("body, html").addClass('scroll-lock')
            $('.c-header').addClass('nav-open')
            $('.cs-toggle-menu').addClass('is-open')
            $('.c-nav').fadeIn(200)

    topNavScrollReveal: ->
      if $('.c-header').length > 0
        # Delta controls minumum amount for scroll to trigger I think
        delta = ''
        didScroll = undefined
        lastScrollTop = 0
        navbarHeight = 10
        setTimeout (->
          navbarHeight = $('.c-header').outerHeight()
        ), 2000

        hasScrolled = ->
          scrollPage = $(this).scrollTop()

          if Math.abs(lastScrollTop - scrollPage) <= delta
            return

          if scrollPage > lastScrollTop and scrollPage > navbarHeight
            $('.c-header').removeClass('nav-down').addClass 'nav-up'
          else
            if scrollPage + $(window).height() < $(document).height()
              $('.c-header').removeClass('nav-up').addClass 'nav-down'
            else if  (lastScrollTop - scrollPage) <= delta
              $('.c-header').removeClass 'nav-down'

          if  scrollPage < navbarHeight
            $('.c-header').removeClass 'nav-down'

          lastScrollTop = scrollPage
          return

        $(window).scroll (event) ->
          didScroll = true
          return
        setInterval (->
          if didScroll && !$('.c-nav').hasClass('is-open')
            hasScrolled()
            didScroll = false
          return
        ), 250

    unveil: ->
      $('[data-lazy-src]').unveil(100)

    gallery: ->
      gallery = $('.gallery')
      if gallery.length > 0
        loadCSS("#{@themeurl}/assets/css/jquery.fancybox.css")
        $.getScript "#{@themeurl}/assets/lib/jquery.fancybox.js", ->
          gallery.addClass('o-layout').addClass('o-layout--small')
          $('.gallery-item').addClass('o-layout__item u-one-half u-wide-palm-one-third u-wall-one-quarter')
          $('.fancybox').fancybox({
            beforeShow: ->
              alt = this.element.find('img').attr('alt')
              this.inner.find('img').attr('alt', alt)
              this.title = alt
          })

    fancybox: ->
      video = $('.fancybox-video')
      if video.length > 0
        loadCSS("#{@themeurl}/assets/css/jquery.fancybox.css")
        $.getScript "#{@themeurl}/assets/lib/jquery.fancybox.js", ->
          $(".fancybox-video").fancybox({
            padding: 0
            width: '100%'
            height: '100%'
            aspectRatio: true
          })

    # adding a handler for the menu item for project page
    projectMenu: ->
      $(document).on 'click', '#menu-item-1102', ->
        if $('document').context.URL = 'http://taboo.test/#project' || $('document').context.URL = 'http://taboo.rockagency.com.au/#project'
          $('.c-nav').css('display', 'none')
          $('.c-load-overlay--visible').css('opacity', '0')
          $('.cs-toggle-menu').removeClass('is-open')
          $("body, html").removeClass('scroll-lock')



    aboutContent: ->
      scrollTop = $(window).scrollTop()
      prevDesc = ''
      prevRow = ''
      $(document).on 'click', '.c-about__item', ->
        desc = $(this).attr('data-item')
        descRow = $(this).attr('data-target-row')
        if $(window).width() > 671
          if prevDesc != desc
            prevDesc = desc
            $('.c-about__item').removeClass('active')
            $(this).addClass('active')
            $('.content-area[data-row="' + descRow + '"] .c-about__item-description').html('')
            if prevRow != descRow
              prevRow = descRow
              $('.content-area').hide(250)
            $('.content-area[data-row="' + descRow + '"] .c-about__item-description').html($('.mobile-desc.' + desc).html())
            $('.content-area[data-row="' + descRow + '"]').show(250)
          else
            prevDesc = ''
            prevRow = ''
            $('.c-about__item').removeClass('active')
            $('.content-area').hide(250)
            setTimeout (->
              $('.content-area[data-row="' + descRow + '"] .c-about__item-description').html('')
            ), 250
        else
          $(this).toggleClass('active')
          $('.mobile-desc.' + desc).toggle(250)

      $(document).on 'click', '.mobile-desc-close', ->
        $('.c-about__item').removeClass('active')
        $('.mobile-desc').hide(250)

      $(document).on 'click', '.c-about__desc-close', ->
        prevDesc = ''
        prevRow = ''
        $('.c-about__item').removeClass('active')
        $('.content-area').hide(250)
        setTimeout (->
          $('.content-area[data-row="' + descRow + '"] .c-about__item-description').html('')
        ), 250

    imageShift: ->
      $('.c-project-list__box').mouseover ->
        elm = this
        $(this).toggleClass('hover')
        if !(navigator.userAgent.indexOf("Safari") > -1)
          $('.hover .c-project-list__thumbnail-shift').addClass 'grow'
        document.onmousemove = (e) ->
          offsetX = ( ( e.pageX - $(elm).offset().left ) / $(elm).width() ) * 15
          offsetY = ( ( e.pageY - $(elm).offset().top ) / $(elm).height() ) * 10
          if $(window).innerWidth() > 671
            $(".grow .c-project-list__thumbnail").css('transform', 'scale(1.08)')
            $(".hover .grow").css('transform', 'translate(' + offsetX + 'px, ' + offsetY + 'px)')
      $('.c-project-list__box').mouseout ->
        $(".hover .c-project-list__thumbnail").css('transform', 'scale(1)')
        $(".hover .grow").delay(500).css('transform', 'translate(0px, 0px)')
        $('.hover .grow').delay(500).removeClass 'grow'
        $(this).removeClass('hover')

    relatedImageShift: ->
      $('.c-project__related').mouseover ->
        elm = this
        $('.c-project__image-wrap').toggleClass('hover')
        $('.hover').css('transform', 'scale(1.1)')
        document.onmousemove = (e) ->
          offsetX = ( ( e.pageX - $(elm).offset().left ) / $(elm).width() ) * 30
          offsetY = ( ( e.pageY - $(elm).offset().top ) / $(elm).height() ) * 20
          if $(window).innerWidth() > 671
            $(".hover").css('transform', 'translate(' + offsetX + 'px, ' + offsetY + 'px) scale(1.1)')
      $('.c-project__related').mouseout ->
        $(".hover .c-project__related").css('transform', 'scale(1)')
        $(".hover").delay(500).css('transform', 'translate(0px, 0px)')
        $('.c-project__image-wrap').removeClass('hover')


    bannerParallaxProject: ->
      $(window).scroll ->
        scrollTop = $(window).scrollTop()
        if $('.c-banner').length > 0
          $('.c-banner__container').css('background-position', 'center ' + scrollTop*.2 + 'px')

    stage: ->
      landing = Cookies.get('landingAnimation')
      date = new Date()
      date.setTime(date.getTime() + (15 * 60 * 1000))

      if (!landing || landing != true) && $('.c-stage').length > 0
        $("body, html").addClass('scroll-lock')
        $('.c-home').addClass('blur')

      if landing && landing == 'true'
        $('.c-stage').css('display', 'none')
        $("body, html").removeClass('scroll-lock')
        $('.c-home').removeClass('blur')
        $(".c-stage__content").addClass('animate')
        $(".c-project-list__box").removeClass('animate')
        $('img').each ->
          $(this).attr 'src', $(this).attr('delayedsrc')
          return

      timing = 3000
      if $('.c-stage__taboologo-container-video').length > 0
        timing = 9400

      setTimeout (->
        $('img').each ->
          $(this).attr 'src', $(this).attr('delayedsrc')
          return
      ), timing - 2000

      hide = ->
        $(".c-stage__taboologo").addClass('hide')
        $(".c-stage__content").addClass('animate')
        $(".c-project-list__box").removeClass('animate')
        $(".c-stage__skip").addClass('hide')
        Cookies.set('landingAnimation', 'true', { expires: date })

      onBack = ->
        $(".c-stage__taboologo").addClass('on-back')
        $("body, html").removeClass('scroll-lock')
        if document.addEventListener
          document.addEventListener 'mousewheel', MouseWheelHandler(), false
          document.addEventListener 'DOMMouseScroll', MouseWheelHandler(), false
        else
          sq.attachEvent 'onmousewheel', MouseWheelHandler()

      setTimeout (->
        hide()
      ), timing

      setTimeout (->
        onBack()
      ), timing + 1000

      $(document).on 'click', '.cs-skip', ->
        hide()
        onBack()

      $('.c-stage__container').on 'click', (e) ->
        if e.target != this
          return
        $('.c-stage').addClass('hide')
        $('.c-home').removeClass('blur')
        $("body, html").removeClass('scroll-lock')
        setTimeout (->
          $('.c-stage').addClass('on-back')
        ), 1000

      $(document).on 'click', '.c-stage__close', ->
        $('.c-stage').addClass('hide')
        $('.c-home').removeClass('blur')
        $("body, html").removeClass('scroll-lock')
        setTimeout (->
          $('.c-stage').addClass('on-back')
        ), 1000

      MouseWheelHandler = ->
        (e) ->
          `var e`
          # cross-browser wheel delta
          e = window.event or e
          delta = Math.max(-1, Math.min(1, e.wheelDelta or -e.detail))
          #scrolling down?
          if delta < 0
            $('.c-stage').addClass('hide')
            $('.c-home').removeClass('blur')
            $(".c-project-list__box").removeClass('animate')
            setTimeout (->
              $('.c-stage').addClass('on-back')
            ), 1000


    tColorTracker: ->
      if $('.change-stage-color').length > 0
        $(window).scroll ->
          stageH = $('.change-stage-color').position().top
          logo = $('.c-header__logo')
          header = $('.c-header')
          logoPosition = logo.offset().top + 50
          if logoPosition > stageH
            # console.log 'go dark'
            unless header.hasClass('c-header--dark')
              header.addClass('c-header--dark')
          else
            # console.log 'go light'
            if header.hasClass('c-header--dark')
              header.removeClass('c-header--dark')

    hideLandingOnScroll: ->
      landing = Cookies.get('hideLanding')
      date = new Date()
      date.setTime(date.getTime() + (15 * 60 * 1000))

      if $('.c-landing').length > 0
        initialHeight =  $('.c-project-list__intro').css('height')
        console.log initialHeight
        $('.c-project-list__spacer').css('height', '110vh')
        $('.c-project-list__intro-copy').fadeOut()
        $('.c-project-list__container').css('opacity', 0)
        $('.c-header').fadeOut()
        $('.c-footer').fadeOut()
        animationTriggered = false
        animate = ->
          $('html').addClass('scroll-lock-landing')
          $('.c-landing').hide()
          if window.innerWidth < 700
            $('.c-project-list__intro').css('height', '412px')
            $('.c-project-list__spacer').css('height', 0)
            console.log 'onmobile'
          else
            # $('.c-project-list__intro').css('height', initialHeight)
            # $('.c-project-list__spacer').css('height', initialHeight)
            $('.c-project-list__spacer').css('height', '350px')
          $('.c-project-list__container').fadeIn(800)
          $('.c-header').fadeIn()
          $('.c-header').removeClass('c-header--dark')
          setTimeout (->
            $('.c-project-list__intro-copy').fadeIn(500)
            $('.c-project-list__intro').css('transition', 'all 1ms')
            $('html').removeClass('scroll-lock-landing')
            $('.c-project-list__container').css('opacity', 1)
            $('.c-footer').fadeIn()
            Cookies.set('hideLanding', 'true', { expires: date })
          ), 1000
          animationTriggered = true;

        $(window).scroll ->
          if animationTriggered == false
            animate()
        $(document).on 'click', '.cs-hide-landing', ->
          if animationTriggered == false
            animate()


    sideTextReveal: ->
      if ('.cs-side-text').length > 0
        headerMask = $('.c-header__side-text-mask')
        headerMaskHeight = headerMask.height()
        offset = 40
        sideTextPositions = $('.cs-side-text').map ->
          topPos = $(this).offset().top
          return {
            top: topPos
            bot: $(this).height() + topPos
          }

        $(window).scroll ->
          detectionResults = undefined
          headerScrollPos = undefined
          headerScrollPos = headerMask.offset().top + headerMaskHeight - offset * 2
          # console.log jQuery.makeArray(sideTextPositions)
          detectionResults = jQuery.makeArray(sideTextPositions).some((elPos) ->
            # console.log $(document).scrollTop(), headerScrollPos, (elPos.top - offset), (elPos.bot + offset), headerScrollPos > (elPos.top - offset) and headerScrollPos < (elPos.bot + offset)
            headerScrollPos > (elPos.top - offset) and headerScrollPos < (elPos.bot + offset)
          )
          # console.log detectionResults
          if detectionResults
            headerMask.show()
          else
            headerMask.hide()







  $ ->
    site.onReady()

  $(window).on 'load', ->
    site.onLoad()

)(jQuery)