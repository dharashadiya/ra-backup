<?php
/*
 * Template Name: Old Campaigns
 */
?>
<?php get_header(); ?>
<div class="c-home c-projects <?php echo $blur ?> c-section c-project-lists c-section--gallery">
	<div class="c-projects-list">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div class="c-project-list__intro cd-fadeup">
			<div class="c-project-list__intro-copy">
				<span class="c-project-list__side-text c-project-list__side-text--museum cs-side-text"><?php the_title() ?></span>
				<h1><?php the_title() ?></h1>
				<?php the_content() ?>
			</div>
		</div>
		<div class="c-project-list__spacer"></div>
		<?php get_template_part('partials/list-old-projects') ?>
		<?php endwhile; ?>
	</div>
</div>
<?php get_footer(); ?>