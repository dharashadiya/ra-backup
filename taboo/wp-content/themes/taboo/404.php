<?php get_header(); ?>

<div class="c-content c-content-404">
  <div class="c-content-404__container">
  <main id="Main" class="c-main-content o-main">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post() ; ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <h1 class="u-alpha"><?php the_title(); ?></h1>
      <div class="c-cms-content">
        <?php the_content(); ?>
        <?php get_template_part( 'partials/post-meta' ); ?>
      </div>
      <?php wp_link_pages(); ?>
    </article>
    <?php endwhile;
      else : ?>
    <article>
      <h1 class="u-alpha">Page Not found</h1>
      <div class="c-cms-content">The content you are looking for not found. Please use navigation menu to go through the site.</div>
    </article>
    <?php
      endif;
    ?>
  </main>
  </div>
</div>

<?php get_footer(); ?>