		<?php
			if (is_page_template('tmpl-our-team.php') || is_singular('ra-project') || is_front_page() || is_page_template('tmpl-project-listing.php') || is_page_template('tmpl-old-campaigns.php') || is_page_template('tmpl-shopify.php') || is_page_template('page.php')) {
				$alt_class = 'c-footer--dark';
			} else {
				$alt_class = '';
			}
			?>
		<div class="c-footer <?php echo $alt_class ?>">
			<div class="o-wrapper">
				<div class="c-footer__logo">
					<a href="/">
						<?php svgicon('inverted-t', "0 0 52.5 64"); ?>
					</a>
				</div>
				<div class="c-footer__left">
					<div class="c-footer__copy">
						<?php the_field('copy', 'options') ?>
					</div>
					<a href="mailto:<?php the_field('contact_email', 'options') ?>" class="c-footer__mail">
						<?php echo svgicon('paper_plane', "0 0 18.13 14") ?>
						<span>
							<?php the_field('contact_email', 'options') ?>
						</span>
					</a>
					<a href="tel:<?php the_field('contact_phone', 'options') ?>" class="c-footer__phone">
						<?php echo svgicon('phone', "0 0 14.98 16") ?>
						<span>
							<?php the_field('contact_phone', 'options') ?>
						</span>
					</a>
					<div class="c-footer__socials">
						<a href="www.instagram.com/<?php the_field('instagram_handle', 'options') ?>">
							<?php echo svgicon('instagram', '0 0 34 34') ?>
						</a>
						<a href="www.linkedin.com/company/<?php the_field('linkedin_handle', 'options') ?>">
							<?php echo svgicon('linkedin', '0 0 34 34') ?>
						</a>
					</div>
					<p class="c-footer__copyright">© Copyright Taboo 2018. | <a href="/terms">Terms&nbsp;&&nbsp;Conditions</a>&nbsp;|&nbsp;<a href="/privacy">Privacy</a>
				</div>
			</div>
		</div>
		<?php wp_footer(); ?>
	</body>
<!-- end main container -->
</div>
</html>
