<?php
/*
 * Template Name: Shopify Page
 */
?>
<?php get_header(); ?>
<div class="c-content c-content--default c-shopify change-stage-color">
    <div class="o-wrapper o-wrapper--1000">
        <main id="Main" class="c-main-content o-main">
        <?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php $thumb = get_the_post_thumbnail_url(null, 'full'); ?>
                <?php if ($thumb) : ?>
                    <img class="c-shopify__logo" src="<?php echo $thumb; ?>" alt="<?php the_title() ?>">
                <?php else : ?>
                    <h1 class="u-alpha"><?php the_title(); ?></h1>
                <?php endif; ?>
                <div class="c-cms-content c-shopify__intro">
                    <?php the_content(); ?>
                </div>
                <?php $products = get_field('products'); ?>
                <?php if ($products && count($products) > 0) : ?>
                    <div class="c-shopify__products">
                        <?php
                            $embade_codes = array();
                            $i = 0; foreach ($products as $item) : ?>
                            <div class="c-shopify__product o-layout">
                                <div class="o-layout__item u-lap-one-third">
                                    <div class="c-shopify__product-slider">
                                        <?php foreach ($item['images'] as $image) : ?>
                                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $item['name']; ?>">
                                        <?php endforeach; ?>
                                    </div>
                                </div><div class="o-layout__item u-lap-two-thirds">
                                    <h3 class="c-shopify__product-name"><?php echo $item['name']; ?></h3>
                                    <div class="c-shopify__product-content"><?php echo $item['content']; ?></div>
                                    <div class="c-shopify__product-bottom">
                                        <div class="c-shopify__product-button"><?php
                                            if ($item['shopify_product_code']) {
                                                $embade_codes[] = array(
                                                    "id" => $item['shopify_product_code'],
                                                    "element" => 'product-' . $i
                                                );
                                                echo '<div id="product-' . $i . '"></div>';
                                                $i++;
                                            }
                                        ?></div>
                                        <h3 class="c-shopify__product-price"><?php echo $item['price']; ?></h3>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </article>
        <?php endwhile; ?>
        </main>
    </div>
</div>
<?php if ($embade_codes && count($embade_codes) > 0) : ?>
    <?php foreach ($embade_codes as $item) : ?>
        <script type="text/javascript">
            /*<![CDATA[*/
            (function () {
            var scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';
            if (window.ShopifyBuy) {
                if (window.ShopifyBuy.UI) {
                ShopifyBuyInit();
                } else {
                loadScript();
                }
            } else {
                loadScript();
            }
            function loadScript() {
                var script = document.createElement('script');
                script.async = true;
                script.src = scriptURL;
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
                script.onload = ShopifyBuyInit;
            }
            function ShopifyBuyInit() {
                var client = ShopifyBuy.buildClient({
                domain: 'taboo-backdoor-giftshop.myshopify.com',
                storefrontAccessToken: '4cccf261fd346d6ff0f1c77f6c8cdb41',
                });
                ShopifyBuy.UI.onReady(client).then(function (ui) {
                ui.createComponent('product', {
                    id: '<?php echo $item['id']; ?>',
                    node: document.getElementById('<?php echo $item['element']; ?>'),
                    moneyFormat: '%24%7B%7Bamount%7D%7D',
                    options: {
                        "product": {
                            "styles": {
                            "product": {
                                "@media (min-width: 601px)": {
                                "max-width": "calc(25% - 20px)",
                                "margin-left": "20px",
                                "margin-bottom": "50px"
                                },
                                "text-align": "right"
                            },
							"options": {
								'max-width': "180px !important",
								'vertical-align': 'top',
							},
                            "buttonWrapper": {
                                "margin-top": "0",
                                "display": "inline-block",
								'vertical-align': 'top',
                            },
                            "button": {
                                "font-family": "\"berthold_akzidenz_grotesk_bBd\", sans-serif",
                                "font-size": "18px",
                                "padding-top": "6px",
                                "padding-bottom": "6px",
                                "text-transform": "uppercase",
								"height": "32px",
                                "font-weight": "bold",
                                "width": "180px",
                                ":hover": {
                                "background-color": "#000000"
                                },
                                "background-color": "#000000",
                                ":focus": {
                                "background-color": "#000000"
                                },
                                "border-radius": "0px",
                                "padding-left": "4px",
                                "padding-right": "4px"
                            },
                            "quantityInput": {
                                "font-size": "18px",
                                "padding-top": "17px",
                                "padding-bottom": "17px"
                            }
                            },
                            "contents": {
                            "img": false,
                            "title": false,
                            "price": false
                            },
                            "text": {
                            "button": "Add to cart"
                            }
                        },
                        "productSet": {
                            "styles": {
                            "products": {
                                "@media (min-width: 601px)": {
                                "margin-left": "-20px"
                                }
                            }
                            }
                        },
                        "modalProduct": {
                            "contents": {
                            "img": false,
                            "imgWithCarousel": true,
                            "button": false,
                            "buttonWithQuantity": true
                            },
                            "styles": {
                            "product": {
                                "@media (min-width: 601px)": {
                                "max-width": "100%",
                                "margin-left": "0px",
                                "margin-bottom": "0px"
                                }
                            },
                            "button": {
                                "font-family": "Avant Garde, sans-serif",
                                "font-size": "18px",
                                "padding-top": "17px",
                                "padding-bottom": "17px",
                                ":hover": {
                                "background-color": "#000000"
                                },
                                "background-color": "#000000",
                                ":focus": {
                                "background-color": "#000000"
                                },
                                "border-radius": "0px",
                                "padding-left": "54px",
                                "padding-right": "54px"
                            },
                            "quantityInput": {
                                "font-size": "18px",
                                "padding-top": "17px",
                                "padding-bottom": "17px"
                            }
                            },
                            "text": {
                            "button": "Add to cart"
                            }
                        },
                        "cart": {
                            "styles": {
                            "button": {
                                "font-family": "Avant Garde, sans-serif",
                                "font-size": "18px",
                                "padding-top": "17px",
                                "padding-bottom": "17px",
                                ":hover": {
                                "background-color": "#000000"
                                },
                                "background-color": "#000000",
                                ":focus": {
                                "background-color": "#000000"
                                },
                                "border-radius": "0px"
                            }
                            },
                            "text": {
                            "total": "Subtotal",
                            "button": "Checkout"
                            },
                            "popup": false
                        },
                        "toggle": {
                            "styles": {
                            "toggle": {
                                "font-family": "Avant Garde, sans-serif",
                                "background-color": "#000000",
                                ":hover": {
                                "background-color": "#000000"
                                },
                                ":focus": {
                                "background-color": "#000000"
                                }
                            },
                            "count": {
                                "font-size": "18px"
                            }
                            }
                        }
                    },
                });
                });
            }
            })();
            /*]]>*/
        </script>
    <?php endforeach; ?>
<?php endif; ?>
<?php get_footer(); ?>