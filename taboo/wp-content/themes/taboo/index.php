<?php
  get_header();
?>

<main id="Main" class="c-main-content o-main o-layout__item u-lap-wide-two-thirds">
  <?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <h1 class="u-alpha"><?php the_title(); ?></h1>
    <div class="c-cms-content">
      <?php the_content(); ?>
      <?php get_template_part( 'partials/post-meta' ); ?>
    </div>
    <?php wp_link_pages(); ?>
  </article>
  <?php endwhile; ?>
</main><?php
  get_sidebar();
  get_footer();
?>