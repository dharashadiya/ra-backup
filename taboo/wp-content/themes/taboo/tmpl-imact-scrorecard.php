<?php
/*
 * Template Name: Impact Scorecard
 */
?>

<?php get_header(); ?>
<div class="c-impact u-footer-spacing">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="c-impact__container">
		<div class="c-impact__intro cs-fadeup">
			<?php svgicon('inverted-t', '0 0 52.5 64'); ?>
			<?php the_content(); ?>
		</div>
		<?php $img = get_field('image'); ?>
		<?php if ($img) : ?>
			<div class="c-impact__img cs-fadeup">
				<img src="<?php echo $img; ?>" alt="Impact Scorecard">
			</div>
		<?php endif; ?>
		<?php $table = get_field('impact_table');?>
		<?php if ($table) : ?>
		<div class="o-wrapper">
			<div class="c-impact__table cs-fadeup">
				<div class="c-impact__table-wrap">
					<table>
						<?php foreach ($table['body'] as $key => $row) :
							// if ($key === 0) {
							// 	$wrap_start = '<th>';
							// 	$wrap_end = '</th>';
							// } else {
								$wrap_start = '<td>';
								$wrap_end = '</td>';
							// }
							?>
							<tr>
								<?php foreach ($row as $column) : ?>
									<?php echo $wrap_start . $column['c'] . $wrap_end; ?>
								<?php endforeach; ?>
							</tr>
						<?php endforeach; ?>
					</table>
					<span class="c-impact__table-caption"><?php echo $table['caption']; ?></span>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<?php endwhile; ?>
</div>

<?php get_footer(); ?>


