<?php get_header(); ?>

<div class="c-project u-footer-spacing">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	<?php $campaign_services = get_field('campaign_services'); ?>
	<div class="c-project__banner" style="background-image: url(<?php echo get_field('client_banner_image')['url']?>)"></div>
	<!-- <img class="c-project__banner" src="<?php echo get_field('client_banner_image')['url']?>" alt=""> -->
	<div class="c-project__intro" id="intro">
		<div class="o-wrapper">
			<div class="c-project__description">
				<a href="/">
					<?php svgicon('long-arrow', "0 0 26.5 10"); ?>
					<span>back to projects</span>
				</a>
				<h1><?php the_field('client_name') ?></h1>
				<h3><?php the_field('project_title') ?></h3>
				<p><?php the_field('client_tagline') ?></p>
			</div>
			<div class="c-project__copy clearfix">
				<?php the_field('objective') ?>
			</div>
			<div class="c-project__copy clearfix">
				<?php the_field('solution') ?>
			</div>
			<div class="c-project__copy clearfix">
				<?php the_field('what_we_did') ?>
			</div>
			<?php if ($campaign_services) : ?>
				<div class="c-project__copy clearfix">
					<?php the_field('campaign_services') ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="change-stage-color change-stage-color--white-bg">
		<div class="c-project__media-gallery">
			<div class="o-layout">
				<?php if (have_rows('content')) while (have_rows('content')): the_row();?>
				<?php if (get_row_layout() == 'single_image'): ?><div class="o-layout__item">
					<div class="cs-fadeup c-project__media-wrap">
						<?php $image = get_sub_field('image') ?>
						<img src="<?php echo $image['url'] ?>" alt="">
						<p class="c-project__caption"><?php the_sub_field('caption') ?></p>
					</div>
				</div><?php elseif (get_row_layout() == 'dual_image'):  ?><div class="o-layout__item u-lap-one-half">
					<?php $image_l = get_sub_field('image_left');
						$image_r = get_sub_field('image_right')
					 ?>
					<div class="cs-fadeup c-project__media-wrap c-project__media-wrap--left">
						<img src="<?php echo $image_l['url'] ?>" alt="">
						<p class="c-project__caption"><?php the_sub_field('left_caption') ?></p>
					</div>
				</div><div class="o-layout__item u-lap-one-half">
					<div class="cs-fadeup c-project__media-wrap c-project__media-wrap--right">
						<img src="<?php echo $image_r['url'] ?>" alt="">
						<p class="c-project__caption"><?php the_sub_field('right_caption') ?></p>
					</div>
				</div><?php elseif (get_row_layout() == 'video'):  ?><div class="o-layout__item pos-rel">
					<?php if (get_sub_field('widescreen_embed')) {
							$video_mod = "--wide";
						} else {
							$video_mod = "";
						}?>
					<div class="cs-fadeup c-project__media-wrap c-project__video-media<?php echo $video_mod ?> c-project__video-media">
						<?php the_sub_field('video_link'); ?>
					</div>
					<p class="c-project__caption c-project__caption<?php echo $video_mod ?>
						<?php if (get_sub_field('vimeo')) {
							echo 'c-project__caption--vimeo';
						} else {
							echo 'c-project__caption--video';
						} ?>">
						<?php the_sub_field('video_caption');?>
					</p>
				</div><?php endif; ?>
				<?php endwhile; ?>
			</div>
			<a href="javascript:void(0)" class="cs-scrollto" data-target="#intro">
				<?php svgicon('long-arrow', "0 0 26.5 10"); ?>
			</a>
		</div>
	</div>
	<?php
		$terms = get_the_terms(get_the_ID(), 'category');
		$museum = false;
		foreach ($terms as $term) {
			if ($term->slug === 'campaign-museum') {
				$museum = true;
			}
		}
		if ($museum) {
			$all_posts = new WP_Query(array(
				'post_type' => 'ra-project',
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'posts_per_page' => -1,
				'category_name' => 'campaign-museum',
				'post_status' => 'publish'
			));
		} else {
			$all_posts = new WP_Query(array(
				'post_type' => 'ra-project',
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'posts_per_page' => -1,
				'category__not_in' => 29,
				'post_status' => 'publish'
			));
		}
		$nextID = 0;
		$prevID = 0;
		foreach($all_posts->posts as $key => $value) {
			if($value->ID == $post->ID){
				$nextID = $all_posts->posts[$key + 1]->ID;
				$prevID = $all_posts->posts[$key - 1]->ID;
				break;
			}
		}
		if ($nextID) {
			$next_post = $nextID;
		} else {
			$next_post = $all_posts->posts[0];
		}
		$related_image = get_field('client_banner_image', $next_post);
	?>
	<div class="c-project__related">
	<!-- <div class="c-project__related" style="background-image:url()"> -->
		<div class="c-project__filter"></div>
		<div class="c-project__image-wrap" style="background-image: url('<?php echo $related_image['url'] ?>')"></div>
		<a href="<?php echo esc_url( get_permalink( $next_post ) ); ?>">
			<div class="o-wrapper">
				<span>next project</span>	
				<h2><?php the_field('client_name', $next_post) ?></h1>
				<h3><?php the_field('project_title', $next_post) ?></h3>
				<p><?php the_field('client_tagline', $next_post) ?></p>
			</div>
		</a>
	</div>
	<?php endwhile; ?>
</div>


<?php get_footer();?>