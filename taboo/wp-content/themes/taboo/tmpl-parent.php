<?php
/*
 * Template Name: Parent Page
 */
?>

<?php get_header(); ?>
<main class="c-main-content o-main o-layout__item u-lap-wide-two-thirds" role="main">
  <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <article <?php post_class(); ?>>
      <h1 class="u-alpha"><?php the_title(); ?></h1>
      <div class="c-cms-content">
        <?php the_content(); ?>
      </div>
      <?php

        $items = new WP_Query();
        $items->query(array(
          'post_type' => 'page',
          'status' => 'published',
          'posts_per_page' => -1,
          'post_parent' => $post->ID,
          'paged' => $paged
        ));

        $orig_query = $wp_query;
        $wp_query = $items;
        get_template_part( 'loop','row' );
        $wp_query = $orig_query;
      ?>

      <?php wp_link_pages(); ?>
    </article>
  <?php endwhile; ?>
</main>

<?php

get_sidebar();

?>
<?php get_footer(); ?>