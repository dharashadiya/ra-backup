<?php
/*
 * Template Name: Contact
 */
?>

<?php get_header(); ?>
<div class="c-contact u-footer-spacing">
	<div class="c-contact__container">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<div class="c-contact__content">
				<div class="o-layout o-layout--flush">
					<div class="c-contact__copy o-layout__item">
						<span class="c-contact__side-text cs-side-text fadeIn">Contact</span>
						<h1>Want some?</h1>
						<?php the_content(); ?>
					</div><div class="c-contact__general o-layout__item u-lap-one-half">
						<h2>General Enquiries</h2>
						<ul>
							<li>
								<a href="mailto:<?php the_field('contact_email', 'options') ?>">
			
								<?php echo svgicon('paper_plane', "0 0 18.13 14") ?>
									<span>
										<?php the_field('contact_email', 'options') ?>
									</span>
								</a>
							</li>
							<li>
								<a href="tel:<?php the_field('contact_phone', 'options') ?>">
									<?php echo svgicon('phone', "0 0 14.98 16") ?>
									<span>
										<?php the_field('contact_phone', 'options') ?>
									</span>
								</a>
							</li>
						</ul>
					</div><div class="c-contact__freelance o-layout__item u-lap-one-half">
						<h2>Employment Enquiries</h2>
						<ul>
							<li>
								<a href="mailto:<?php the_field('careers_email', 'options') ?>">
									<?php echo svgicon('paper_plane', "0 0 18.13 14") ?>
									<span>
										<?php the_field('careers_email', 'options') ?>
									</span>
								</a>
							</li>
						</ul>
					</div><div class="c-contact__hq o-layout__item u-lap-one-half">
						<h2>Headquarters</h2>
						<ul>
							<li>
								<a href="https://google.com/maps/search/<?php the_field('address', 'options') ?>">
									<?php echo svgicon('pin', "0 0 8 15.47") ?>
									<span>
										<?php the_field('address', 'options') ?>
									</span>
								</a>
							</li>
						</ul>
					</div><div class="c-contact__social o-layout__item u-lap-one-half">
						<h2>Social</h2>
						<ul>
							<li>
								<a target="_blank" href="https://instagram.com/<?php the_field('instagram_handle', 'options') ?>">		
									<?php echo svgicon('instagram', '0 0 34 34') ?>
									<span>
										<?php the_field('instagram_handle', 'options') ?>
									</span>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://linkedin.com/company/<?php the_field('linkedin_handle', 'options') ?>">
									<?php echo svgicon('linkedin', '0 0 34 34') ?>
									<span>
										<?php the_field('linkedin_handle', 'options') ?>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</div>

<?php get_footer(); ?>