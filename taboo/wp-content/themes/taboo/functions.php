<?php

define( 'GOOGLE_ANALYTICS', 'UA-104064598-1');
include('functions/boilerplate.php');
include('functions/admin-config.php');
include('functions/theme-helpers.php');
include('functions/clean-gallery.php');
include('functions/acf-config.php');
include('functions/shortcodes.php');
include('functions/woocommerce-helpers.php');

// Author meta tag
define( 'AUTHOR', 'Taboo Theme');

// Disable responsive images to make lazy load work
add_filter( 'wp_calculate_image_srcset_meta', '__return_null' );

// Enable post thumbnails
add_theme_support( 'post-thumbnails' );

// Enable nav menus
add_theme_support( 'nav-menus' );

// Custom image sizes
add_image_size( 'featured-thumb', 300, 180, true );

// make custom image size choosable from add media
add_filter( 'image_size_names_choose', 'site_custom_sizes' );
function site_custom_sizes( $sizes ) {
  return array_merge( $sizes, array(
    'featured-thumb' => 'Featured Thumb',
  ) );
}

// Register nav menus
function register_site_menus() {
  register_nav_menu( 'primary-menu', 'Primary Menu' );
}
add_action('after_setup_theme', 'register_site_menus');

// Register widget areas
function site_widgets_init() {
  // Register main sidebar
  register_sidebar( array(
    'name' => 'Sidebar Widget Area',
    'id' => 'aside-sidebar',
    'description' => 'Widget in this area will be shown in sidebar area on all pages',
    'before_widget' => '<section id="%1$s" class="c-widget c-widget--side %2$s %1$s">',
    'after_widget' => '</div></section>',
    'before_title' => '<h3 class="c-widget__heading">',
    'after_title' => '</h3><div class="c-widget__content">'
  ));

  // Register footer sidebar
  register_sidebar( array(
    'name' => 'Footer Widget Area',
    'id' => 'footer-sidebar',
    'description' => 'Widget in this area will be shown in footer area on all pages',
    'before_widget' => '<section id="%1$s" class="c-widget c-widget--footer %2$s %1$s">',
    'after_widget' => '</div></section>',
    'before_title' => '<h3 class="c-widget__heading">',
    'after_title' => '</h3><div class="c-widget__content">'
  ));
}
add_action( 'widgets_init', 'site_widgets_init' );

// Remove contact from styles from all pages
add_filter('wpcf7_load_js', '__return_false');
add_filter('wpcf7_load_css', '__return_false');

// Load scripts using wp's enqueue
function site_scripts() {
  if ( !is_admin() ) {
    // Remove jquery that wordpress includes
    wp_deregister_script('jquery');
    $site_js = (is_localhost()) ? JS . '/site.js' : JS . '/site.min.js';
    $site_css = (is_localhost()) ? CSS . '/screen.dev.css' : CSS . '/screen.min.css';

    // Include all js including jQuery and register with name 'jquery' so other jquery dependable scripts load as well
    wp_enqueue_script('jquery',$site_js, false, $GLOBALS['site_cachebust'], true);
    wp_enqueue_style('site', $site_css, false);
  }
}
add_action('wp_enqueue_scripts', 'site_scripts');

// Boolean to check if the cf7 scripts are required
function load_cf7_scripts() {
  return ( is_page('contact') );
}

// Load cf7 scripts if required
function site_load_cf7_scripts() {
  if ( load_cf7_scripts() ) {
    if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
      wpcf7_enqueue_scripts();
    }
    // Not loading wpcf7 styles at all, if you need wpcf7 styles uncomment below lines
    // if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
    //   wpcf7_enqueue_styles();
    // }
  }
}
add_action( 'wp', 'site_load_cf7_scripts' );

// Allow search to search through pages and posts. Add any custom post type in array to search by them as well.
function site_search($query) {
  if (!is_admin() && $query->is_search) {
    $query->set('post_type', array('page','post'));
  }
  return $query;
}
add_filter('pre_get_posts', 'site_search');

// Add Order column on ra-project listing
function add_menu_order_column($menu_order_columns) {
  $menu_order_columns['menu_order'] = "Order";
  return $menu_order_columns;
}
add_action('manage_edit-ra-project_columns', 'add_menu_order_column');

// show custom order column values
function show_order_column($name){
  global $post;

  switch ($name) {
    case 'menu_order':
      $order = $post->menu_order;
      echo $order;
      break;
   default:
      break;
   }
}
add_action('manage_ra-project_posts_custom_column','show_order_column');

// make column sortable
function order_column_register_sortable($columns){
  $columns['menu_order'] = 'menu_order';
  return $columns;
}
add_filter('manage_edit-ra-project_sortable_columns','order_column_register_sortable');

function isMobile() {
  return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|iphone|ipad|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

?>