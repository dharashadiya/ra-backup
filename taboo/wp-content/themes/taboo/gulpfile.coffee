# Require coffee script and register it in order to use write gulpfile in coffee script
require('coffee-script/register')

path = require('path')
gulp = require('gulp')
gutil = require('gulp-util')
# automatically load gulp plugins without defining all of them here
gulpLoadPlugins = require('gulp-load-plugins')
plugins = gulpLoadPlugins()
# to run gulp tasks in sequence
runSequence = require('run-sequence')

# compile sass files into css, autoprefix them, optimise and minify them using cssmin
gulp.task('styles', ->
  gulp.src 'assets/sass/screen.scss'
    .pipe( plugins.sass({outputStyle: 'expanded'}).on('error', plugins.sass.logError) )
    .pipe(plugins.rename( (file) ->
      file.dirname = file.dirname.replace(path.sep + 'sass', path.sep + 'css')
    ))
    .pipe(gulp.dest('assets/css'))
    .pipe(plugins.autoprefixer({ browsers: ['last 3 versions'] }))
    .pipe(plugins.rename('screen.dev.css'))
    .pipe(gulp.dest('assets/css'))
    .pipe(plugins.cssmin())
    .pipe(plugins.concat('screen.min.css'))
    .pipe(gulp.dest('assets/css'))
    .pipe(plugins.livereload())
)

# Compile coffee to public
gulp.task('coffee', ->
  gulp.src('assets/coffee/*.coffee')
    .pipe(plugins.coffee({bare: true}).on('error', (err) ->
      gutil.log(err)
      @emit 'end'
    ) )
    .pipe(gulp.dest('assets/js'))
)

# JS linkting task
gulp.task('jshint', ->
  gulp.src('assets/js/*.js')
    .pipe(plugins.jshint())
    .pipe(plugins.jshint.reporter('default'))
    .pipe(plugins.jshint.reporter('fail'))
)

gulp.task('concat', ->
  gulp.src(['assets/lib/!(site).js', 'assets/js/base.js'])
  .pipe(plugins.concat('site.js'))
  .pipe(gulp.dest('assets/js'))

)

gulp.task('uglify', ->
  gulp.src([
    'assets/lib/jquery-3.3.1.js',
    'assets/lib/jquery.visible.js',
    'assets/lib/jquery.intentionscroll.js',
    'assets/lib/jquery.unveil.js',
    'assets/lib/jquery.cookie.js',
    'assets/lib/slick.js',
    'assets/lib/jquery.viewportchecker.min.js',
    'bower_components/loadcss/loadCSS.js',
    'assets/lib/svg4everybody.js',
    'assets/lib/rellax.js',
    'assets/lib/tilt.jquery.js',
    'assets/js/base.js'
  ])
  .pipe(plugins.concat('site.js'))
  .pipe(gulp.dest('assets/js'))
  .pipe(plugins.uglify())
  .pipe(plugins.rename({extname: ".min.js"}))
  .pipe(gulp.dest('assets/js'))
)

gulp.task('svgstore', ->
  gulp.src('assets/img/inline/*.svg')
    .pipe(plugins.svgmin( (file) ->
        plugins: [{
          cleanupIDs: {
            prefix: path.basename(file.relative, path.extname(file.relative)) + '-'
            minify: true
          }
        }]
    ))
    .pipe(plugins.svgstore())
    .pipe(gulp.dest('assets/img'))
)

gulp.task('modernizr', ->
  gulp.src(['assets/**/*.coffee', 'assets/**/*.scss'])
    .pipe(plugins.modernizr({
      options : ["setClasses"]
      tests: ['inlinesvg']
    }))
    .pipe(plugins.uglify())
    .pipe(plugins.concat('inlinescripts.min.php'))
    .pipe(plugins.header('<script>\n\t/* Contains custom build of Modernizr */\n\t'))
    .pipe(plugins.footer('\n</script>'))
    .pipe(gulp.dest("./partials"))
)

gulp.task('watch', ->
  plugins.livereload.listen()
  gulp.watch('**/*.php').on('change', plugins.livereload.changed)
  gulp.watch('assets/coffee/*.coffee', ['coffee'])
  gulp.watch(['assets/js/base.js'], ['uglify']).on('change', plugins.livereload.changed)
  gulp.watch(['assets/lib/**/*.js'], ['uglify']).on('change', plugins.livereload.changed)
  gulp.watch('assets/sass/**/*.scss', ['styles'])
  gulp.watch('assets/img/inline/*.svg', ['svgstore']).on('change', plugins.livereload.changed)
)

gulp.task('sftp', ->
  gulp.src([
    'style.css',
    'screenshot.jpg',
    '**!(bower_components)/*.php',
    '*.php',
    '*ssets/lib/*.js',
    '*ssets/js/*.min.js',
    '*ssets/*.js',
    '*ssets/css/*.min.css',
    '*ssets/css/jquery.fancybox.css',
    '*ssets/css/video-js.css',
    '*ssets/fonts/*.{eot,svg,ttf,woff,woff2}',
    '*ssets/img/inline/*.svg'
    '*ssets/img/logo-animation/*.svg'
    '*ssets/img/*.{svg,ico,jpg,jpeg,png,gif}'
    '*ssets/img/fancybox/*.{svg,ico,jpg,jpeg,png,gif}'
  ])
  .pipe(plugins.sftp({
    host: '103.27.32.31'
    auth: 'staging'
    port: '2683'
    remotePath: '/home/rockagen/public_html/staging/taboo/wp-content/themes/taboo/'
    authFile:'.ftppass'
  }))
)

gulp.task('default', (done) ->
  runSequence('styles', 'coffee', 'uglify', 'svgstore', 'modernizr', ['watch'], done)
)

gulp.task('deploy', (done)->
  runSequence('styles', 'coffee', 'uglify', 'svgstore', 'sftp', done)
)