<?php
/*
Plugin Name: Projects - Custom Post Type
Description: Projects - Custom Post Type for Taboo
*/

add_action( 'init', 'register_projects_custom_post_type' );
function register_projects_custom_post_type() {
  register_post_type( 'ra-project', array(
    'label'  => __('project', 'ra-project'),
    'description' => __('Projects', 'ra-project'),
    'labels' => array(
      'name'               => _x( 'Projects', 'post type general name', 'ra-project' ),
      'singular_name'      => _x( 'Project', 'post type singular name', 'ra-project' ),
      'menu_name'          => __( 'Projects', 'admin menu', 'ra-project' ),
      'parent_item_colon'  => __( 'Parent Projects:', 'ra-project' ),
      'name_admin_bar'     => __( 'Project', 'add new on admin bar', 'ra-project' ),
      'all_items'          => __( 'All Projects', 'ra-project' ),
      'view_item'          => __( 'View project', 'ra-project' ),
      'add_new_item'       => __( 'Add New project', 'ra-project' ),
      'add_new'            => __( 'Add New', 'project', 'ra-project' ),
      'edit_item'          => __( 'Edit project', 'ra-project' ),
      'update_item'        => __( 'Update project', 'ra-project' ),
      'search_items'       => __( 'Search Projects', 'ra-project' ),
      'new_item'           => __( 'New project', 'ra-project' ),
      'not_found'          => __( 'No Projects found.', 'ra-project' ),
      'not_found_in_trash' => __( 'No Projects found in Trash.', 'ra-project' ),
    ),

    'supports'      => array(
      'title',
      'excerpt',
      'author',
      'thumbnail',
      'page-attributes',
      'comments',
      'revisions',
      'custom-fields',
      'categories'
    ),
    'taxonomies' => array('category', 'ra-project-tag'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-video-alt',
    'can_export'          => true,
    'has_archive'         => false,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'query_var'           => true,
    'rewrite'             => array(
      'slug' => 'project',
      // 'with_front' => 'true'
    )
  ) );
}

?>