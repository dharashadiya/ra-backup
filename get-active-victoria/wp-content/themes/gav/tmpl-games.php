<?php
/*
 * Template Name: Games Page
 */
?>
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div class="c-games">
        <div class="o-wrapper">
            <div class="o-layout o-module c-games__list">
                <?php
                $args = array(
                    'post_type' => 'ra-games',
                    'status' => 'published',
                    'numberposts' => -1,
                    'fields' => 'ids',
                    'posts_per_page' => -1,
                );
				$posts = get_posts( $args );
				foreach ($posts as $item) :
						$slug = get_post_field( 'post_name', $item );
						$content = get_field('website_content', $item);
						$img = get_the_post_thumbnail_url($item);
						?>

						<div class="o-layout__item o-module__item c-activity-tile animate u-1/3@tablet">
							<div class="c-activity-tile__wrap">
							<?php if ($content['teaser_url']) : ?>
								<a href="javascript:void(0);" class="js-open-modal c-activity-tile__img lazyload" data-preview="<?php echo $slug; ?>" data-src="<?php echo $img ?>" data-content="<?php echo $content['teaser_url'] ?>" data-btn="Join to access games" data-sharelink="<?php echo get_the_permalink($item); ?>">
									<span><?php echo svgicon('play', '0 0 80 80','c-svgicon--play') ?></span>
								</a>
							<?php else : ?>
								<div class="c-activity-tile__img lazyload"  data-src="<?php echo $img ?>"></div>
							<?php endif; ?>
								<div class="c-activity-tile__content">
									<h3><?php echo get_the_title($item); ?></h3>
									<p><?php echo wp_trim_words($content['description'], 14) ?></p>
								</div>
								<a href="https://app.getactive.vic.gov.au" class="c-activity-tile__more">
									<span class="">Join to access games</span>
								</a>
							</div>
						</div>
				<?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endwhile; ?>

<?php get_footer(); ?>