<?php
function get_exercise() {
	$rows = [];
	$loop = get_posts( array( 'post_type' => array('ra-workout', 'ra-challenges', 'ra-games'), 'posts_per_page' => -1) );
	foreach ($loop as $item) {
    	$web_app_content = get_field('web_app_content', $item->ID);

		$exercise_type = '';
		if ($item->post_type === 'ra-workout') {
			$category = get_the_terms($item->ID, 'workout_category');
			$exercise_type = 'Workout';
		} elseif ($item->post_type === 'ra-challenges') {
			$category = get_the_terms($item->ID, 'challenges_category');
			$exercise_type = 'Challenge';
		} elseif ($item->post_type === 'ra-games') {
			$category = get_the_terms($item->ID, 'games_category');
			$exercise_type = 'Game';
		}

		$cat_name = null;
		$cat_arr = array();
		if (count($category) > 0) {
			$cat_name = $category[0]->name;
			foreach ($category as $cat) {
				$cat_arr[] = $cat->name;
			}
		}

		$cat_arr = array_unique($cat_arr);

		$row = array(
			'id'			=> $item->ID,
			'name'			=> $item->post_title,
			// 'category' 		=> $cat_name,
			'categories' 	=> $cat_arr,
			'exerciseType'	=> $exercise_type,
			'description' 	=> $web_app_content['description'],
			'effort' 		=> $web_app_content['effort'],
			'level' 		=> $web_app_content['level'],
			'image' 		=> $web_app_content['video_thumbnail'],
			'previewImage' 	=> $web_app_content['video_image'],
			'featured' 		=> $web_app_content['featured'] ? true : false,

			'downloadUrl' 	=> null,
			'videoSource' 	=> array(),
			'subtitleTracks' => null,
		);

		if ($web_app_content['video_download'] && $web_app_content['file_url']) {
			$row['downloadUrl'] = $web_app_content['file_url'];
		} elseif (!$web_app_content['video_download'] && $web_app_content['video_source'] && $web_app_content['video_source']['url']) {
			$row['videoSource'] = array($web_app_content['video_source']);
			$row['subtitleTracks'] = $web_app_content['subtitle_tracks'] ? $web_app_content['subtitle_tracks'] : null;
		}

		if ($row['videoSource'] || $row['downloadUrl']) {
			$rows[] = $row;
		}
	}
  	return $rows;
}

add_action( 'rest_api_init', function () {
	// register_rest_route( 'lga-offers/v1', '/all', array(
	// 	'methods' => 'GET',
	// 	'callback' => 'lga_offers',
	// ));

	// view /wp-json/exercise/v1/all
	register_rest_route( 'exercise/v1', '/all', array(
		'methods' => 'GET',
		'callback' => 'get_exercise',
	));
} );

?>