/* global site */
// [1] Import functions here
import {
	isMobile,
	openLinkNewTab,
	superfishMenu,
	toggleNav,
	slider,
	searchFunc,
	infoClick,
	contactTabs,
	accordion,
	slickSlider,
	homeSlider,
	openVideoModal,
	lazyLoad,
	inputFilename,
	introModal,
	fixedNav,
	fadeInUp,
	bottomInfoBanner,
	cloudinaryImageCDN
} from "./template-functions";
(function($) {
	window.site || (window.site = {});
	$.extend(
		site,
		{
			initVars: () => {
				// Global variables -> use as site.VariableName in other files
				site.body = $("body");
				site.root = $("html");
				site.header = $("#Top");
				site.main = $("#Main");
				site.side = $("#Secondary");
				site.siteurl = $("#Siteurl").attr("content");
				site.themeurl = $("#Themeurl").attr("content");
				return (site.ajaxurl = $("#Ajaxurl").attr("content"));
			},
			// [3] Call functions within either of these
			onReady: () => {
				svg4everybody();
				site.initVars();
				site.superfishMenu();
				site.toggleNav();
				site.searchFunc();
				site.infoClick();
				site.contactTabs();
				site.accordion();
				site.slickSlider();
				site.homeSlider();
				site.openVideoModal();
				site.lazyLoad();
				site.inputFilename();
				site.introModal();
				site.fixedNav();
				site.fadeInUp();
				site.bottomInfoBanner();
				// site.cloudinaryImageCDN()
				return site.isMobile();
			},
			onLoad: () => {
				// return site.slider()
			}
		},
		{
			// [2] Register functions here
			isMobile: isMobile,
			openLinkNewTab: openLinkNewTab,
			superfishMenu: superfishMenu,
			toggleNav: toggleNav,
			slider: slider,
			searchFunc: searchFunc,
			infoClick: infoClick,
			contactTabs: contactTabs,
			accordion: accordion,
			slickSlider: slickSlider,
			homeSlider: homeSlider,
			openVideoModal: openVideoModal,
			lazyLoad: lazyLoad,
			inputFilename: inputFilename,
			introModal: introModal,
			fixedNav: fixedNav,
			fadeInUp: fadeInUp,
			bottomInfoBanner: bottomInfoBanner,
			cloudinaryImageCDN: cloudinaryImageCDN
		}
	);
	$(() => {
		return site.onReady();
	});
	return $(window).on("load", () => {
		return site.onLoad();
	});
})(jQuery);
