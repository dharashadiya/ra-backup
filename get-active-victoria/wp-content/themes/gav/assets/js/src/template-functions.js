export function isMobile() {
	var result;
	result = false;
	if (
		/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
			navigator.userAgent
		)
	) {
		result = true;
	}
	return result;
}
export function getUrlParameter(name) {
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	var results = regex.exec(location.search);
	return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

// Open External Link in new tab
export function openLinkNewTab() {
	var base;
	base = window.location.hostname;
	return $("a").each(() => {
		let url = $(this).attr("href");
		if (
			url.indexOf("http") !== -1 &&
			(url.indexOf("javascript:void(0)") < 0 && url.indexOf(base) < 0)
		) {
			$(this).attr("target", "_blank");
			return $(this).attr("rel", "noopener");
		}
	});
}

export function cloudinaryImageCDN() {
	const localhost = site.siteurl.includes(".test") ? true : false;
	imagesLoaded("body", {
		background: true
	}, () => {
		Array.from($("[data-img], [data-bg], [data-src]")).forEach(image => {
			if (localhost) {
				if (image.classList.contains("lazy")) {
					return
				} else if (image.tagName === "IMG") {
					image.src = image.dataset.src;
				} else {
					image.style.backgroundImage = `url(${image.dataset.src})`;
				}
			} else {
				const clientWidth = image.clientWidth;
				const pixelRatio = window.devicePixelRatio || 1.0;
				const imageParams = "w_" + 100 * Math.round((clientWidth * pixelRatio) / 100) + ",c_fill,g_auto,f_auto";
				const baseUrl = "https://res.cloudinary.com/rock-agency/image/fetch";
				let url;
				if (image.classList.contains("lazy")) {
					image.dataset.src = baseUrl + "/" + imageParams + "/" + image.dataset.src;
				} else if (image.tagName === "IMG") {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.src = url;
				} else {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.style.backgroundImage = `url(${url})`;
				}
			}
		});
	});
}

export function superfishMenu() {
	if ($(window).width() > 1180) {
		$(".c-nav").superfish({
			animation: {
				height: "show"
			},
			delay: 250,
			speed: 'fast',
			onBeforeShow: function () {
				$('.c-top').addClass('subnav-open')
			},
			onBeforeHide: function () {
				$('.c-top').removeClass('subnav-open')
			}
		});
	} else {
		$('.c-nav').superfish('destroy');
	}
}

export function slider() {
	return $(".c-slider").cycle({
		log: false,
		// swipe: true
		slides: ".c-slide",
		"auto-height": "calc",
		timeout: 5000,
		speed: 600
	});
}

// Fade in animation
export function fadeInUp() {
	$(`.input-wrap,
	   .animate,
	   .animate-elems p,
	   .animate-elems h1,
	   .animate-elems h2,
	   .animate-elems h3,
	   .animate-elems h4,
	   .animate-elems h5,
	   .animate-elems h6,
	   .animate-elems li`
	).addClass('pre-animate').viewportChecker({
		classToAdd: "animated",
		classToRemove: 'pre-animate',
		offset: 0
	});
}

// LazyLoad
export function lazyLoad() {
	return $('.lazyload').Lazy({
		threshold: 400,
		effect: 'fadeIn',
	})
}

export function toggleNav() {
	$('#toggler').click(function(){
		$('.c-top').toggleClass('is-nav-open');
		$('.c-nav-toggle__icon').toggleClass('cross');
		$('html').toggleClass('scroll-lock');
	});
	$('.c-nav').find('ul.sub-menu').wrapInner('<div class="o-wrapper sub-menu-inner " />');
	// $('.c-site-nav .menu-item-has-children').each(function () {
	// });
	$('<span class="c-nav__subnav-toggle"></span>').insertBefore('.c-site-nav .menu-item-has-children .sub-menu')

	// mobile subnav
	$(document).on('click', '.c-nav__subnav-toggle', function () {
		// $('.c-nav__subnav-toggle').removeClass('active');
		// $('.sub-menu').removeClass('active');
		$(this).toggleClass('active')
		// $(this).siblings('.sub-menu').addClass('active')
		$(this).siblings('.sub-menu').toggle(300);
	});

}

export function searchFunc() {
	$('body').on('click', '.c-nav__social-search', function () {
		$('.c-search__input').focus();
		$('.c-search__container').addClass('is-active');
		$('.c-search').addClass('is-active');
		return $('html').addClass('overlay-open');
	});
	return $('body').on('click', '.c-overlay__close', function () {
		$('.c-search__container').removeClass('is-active');
		$('.c-search').removeClass('is-active');
		return $('html').removeClass('overlay-open');
	});
}

export function contactTabs() {
	$(document).on('click', '.js-tabs-link', function () {
		var targetValue;
		if ($(this).data('target') !== targetValue) {
			targetValue = $(this).data('target');
			$('.js-tabs-link').removeClass('active');
			$(this).addClass('active')
			$('.c-contact__tab-container').hide();
			$('.js-tabs__' + targetValue).show();
		}
	})
}
export function infoClick() {
	$(document).on('click', '.info-statement', function () {
		$(this).siblings('.statement-copy').toggleClass('reveal');
		$(this).toggleClass('flip');
	});
}

export function accordion() {
	return $('.c-accordion__question').click(function () {
		$(this).next('.c-accordion__answer').toggle(300);
		return $(this).toggleClass('active');
	});
}

export function slickSlider() {
	 $('.c-slick').slick({
	  lazyLoad: 'ondemand',
      dots: false,
      arrows: true,
      infinite: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }, {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
}
export function homeSlider() {
	$('.c-home__slider').slick({
	  lazyLoad: 'ondemand',
      dots: true,
      arrows: false,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
    });
	$('.c-home__slider .slick-slide').css('min-height', '100%')
	setTimeout(() => {
		let h = 0
		$('.c-home__slider .slick-slide').each(function () {
			let nH = $(this).outerHeight()
			if (nH > h) { h = nH }
		})
		$('.c-home__slider .slick-slide').css('min-height', `${h}px`)
	}, 100);


}


export function openVideoModal() {
	let preview = getUrlParameter('preview')

	if (preview === 'hula-hoop-routine-8211-part-1') {
		preview = 'hula-hoop-routine-part-1'
	}
	if (preview === 'train-like-emma-carney') {
		preview = 'bush-animal'
	}
	if (preview && $(`.js-open-modal[data-preview=${preview}]`).length > 0) {
		setTimeout(() => {
			$(`.js-open-modal[data-preview=${preview}]`).trigger('click')
		}, 100);
	}
	var thisPrev = ''
	$('.js-open-modal').on("click", function () {
		var video = $(this).attr("data-content");
		var btn = $(this).attr("data-btn");
		var shareLInk = $(this).attr('data-sharelink')
		thisPrev = $(this).attr("data-preview");
		if (video) {
			$('#js-video-btn').text(btn);
			$('#video_container').attr('src', video + '?title=0&byline=0&portrait=0&badge=0');
			$('.video-modal').show();
			$('.c-top').addClass('is-blured')
			$('.c-content').addClass('is-blured')
			$('.c-footer').addClass('is-blured')
			if (thisPrev) {
				window.history.replaceState(null, null, "?preview=" + thisPrev);
			}
			if (shareLInk) {
				$('#share_link').attr('href', 'https://www.facebook.com/sharer/sharer.php?u=' + shareLInk).show();
			} else {
				$('#share_link').attr('href', 'javascript:void(0)').hide();
			}
		}
	});
	$('.c-modal__close').on("click", function () {
		closeModel()
	});
	$('.c-modal').click(function (event) {
		if ((event.target == this)) {
			closeModel()
		}
	});
	function closeModel() {
		$('.c-modal').hide();
		$('#video_container').attr('src', '');
		$('.c-top').removeClass('is-blured')
		$('.c-content').removeClass('is-blured')
		$('.c-footer').removeClass('is-blured')
		if (thisPrev) {
			var uri = window.location.toString();
			var clean_uri = uri.substring(0, uri.indexOf("?"));
			window.history.replaceState({}, document.title, clean_uri);
		}
	}
}
export function introModal() {
	let intromodal = Cookies.get('intromodal')
	let referrer = document.referrer
	let qPAA = getUrlParameter('referrer')
	let show = false;
	if (referrer && /activeapril.vic.gov.au/i.test(referrer) ) {
		show = true;
	}
	if (qPAA && qPAA === 'premiers-active-april') {
		show = true;
	}
	if ($('.intro-modal').length > 0 && !intromodal && show) {
		setTimeout(() => {
			$('.intro-modal').fadeIn(300);
		}, 1500);
	}
	$(".c-modal__close").on("click", function (e) {
		$('.intro-modal').fadeOut(300);
		Cookies.set('intromodal', true);
	});
}

export function inputFilename() {
	$('#file-upload').change(function () {
		var file = $('#file-upload')[0].files[0].name;
		// var file = $('#file-upload').val();
		$('#file-label').html(file);
	});
}

export function fixedNav() {
	var nav = $('.c-top');
	$(window).scroll(function () {
		if ($(this).scrollTop() > 200) {
			nav.addClass("fixed-nav");
		} else {
			nav.removeClass("fixed-nav");
		}
	});
}

export function bottomInfoBanner() {
	if (!Cookies.get('bottom-info-banner')) {
		setTimeout((function () {
			$('.c-bottom-info-banner').addClass('active');
			$('body').css('padding-bottom', $('.c-bottom-info-banner').height() + 'px');
			$(document).on('click', '.c-bottom-info-banner__cross', function (e) {
				$('.c-bottom-info-banner').removeClass('active');
				$('body').css('padding-bottom', 0);
				Cookies.set('bottom-info-banner', true, {
					expires: null,
					path: '/'
				});
			});
		}), 5000);
	}
}