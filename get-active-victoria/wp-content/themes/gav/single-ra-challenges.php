<?php get_header();
if ( have_posts() ) while ( have_posts() ) : the_post() ;
	$website_content = get_field('website_content');
?>
<div class="c-single-teaser">
	<div class="o-wrapper o-wrapper--950">
		<div class="c-single-teaser__back"><a href="<?php echo SITE; ?>/around-home/challenges">< Back to challenges</a></div>
		<div class="c-single-teaser__intro">
			<h1><?php the_title(); ?></h1>
			<p><?php echo $website_content['description']; ?></p>
		</div>
		<div class="c-single-teaser__teaser o-aspect o-aspect--16by9">
			<iframe src="<?php echo $website_content['teaser_url']; ?>?title=0&byline=0&portrait=0&badge=0" style="border:none;overflow:hidden" width="890" height="500" scrolling="no" allowTransparency="true" allowFullScreen="true"></iframe>
		</div>
		<a href="https://app.getactive.vic.gov.au" title="Register for Get Active Victoria!" class="o-btn o-btn--join">Join today to watch the full challenge</a>
	</div>
</div>
<?php endwhile;
get_footer(); ?>