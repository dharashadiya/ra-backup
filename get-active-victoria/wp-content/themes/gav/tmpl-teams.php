<?php
/*
 * Template Name: Teams Page
 */
?>
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div class="c-teams">
        <div class="c-teams-list">
            <?php $sections = get_field('sections');
             foreach ($sections as $item) : ?>
                <div class="c-teams-list__each animate-elems">
                    <div class="c-teams-list__wrapcontent">
                        <div class="c-teams-list__content">
                            <h2 class="h2--alt"><?php echo $item['title'];?></h2>
                            <p><?php echo $item['copy'] ?></p>
                            <a href="<?php echo SITE. '/teams/' . $item['button']['url'] ?>" class="o-btn o-btn--blue"><?php echo $item['button']['title'] ?></a>
                        </div>
                    </div>
                    <div class="c-teams-list__img animate" style="background-image: url(<?php echo $item['image'] ?>)"></div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>