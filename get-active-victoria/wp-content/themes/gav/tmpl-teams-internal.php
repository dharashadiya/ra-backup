<?php
/*
 * Template Name: Teams Internal Page
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="c-section c-internal">
 <div class="o-wrapper">
		<div class="o-layout o-layout--large">
			<div class="o-layout__item u-3/4@laptopMed u-2/3@tablet">
				<div class="c-internal__content">
                    <?php $content = get_field('page_content'); if ($content) : ?>
                        <div class="large-para animate-elems">
                            <?php echo $content; ?>
                        </div>
                    <?php endif; ?>
                    <?php $faq_section = get_field('faq_section'); if ($faq_section) : ?>
                        <div class="c-accordion animate">
                            <h2 class="c-accordion__heading"><?php echo $faq_section['title'] ?></h2>
                            <?php foreach ($faq_section['faqs'] as $item) : ?>
                                <div class="c-accordion__item">
                                    <a href="javascript:void(0);" class="c-accordion__question"><?php echo $item['question'] ?><img src="<?php echo ASSETS ?>/img/select-arrow.png" alt=""></a>
                                    <div class="c-accordion__answer">
                                        <div class="c-accordion__content"><?php echo $item['answer'] ?></div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                     <?php endif; ?>
				</div>
				<div class="c-page-modified"><p><span>Last updated: </span><?php the_modified_date('F j, Y'); ?><span> at: </span><?php the_modified_date('g:i a'); ?></p></div>
			</div>
			<div class="o-layout__item u-1/4@laptopMed u-1/3@tablet sidebar-sticky">
				<div class="c-sidebar">
					<?php get_template_part( 'custom', 'sidebar' ); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; ?>

<?php get_footer(); ?>