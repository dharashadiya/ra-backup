const path = require('path')
var webpack = require('webpack');

module.exports = {
    "mode": 'none',  
    "output": {
        filename: "bundle.js"
    },
    "optimization": {
        minimize: false
    },
    "module": {
        "rules": [{
                "enforce": "pre",
                "test": /\.(js|jsx)$/,
                "exclude": /node_modules/,
                "use": "eslint-loader"
            },
            {
                "test": /\.js$/,
                "exclude": /node_modules/,
                "use": {
                    "loader": "babel-loader",
                    "options": {
                        "presets": [
                            "env"
                        ]
                    }
                }
            }
        ]
    }
}