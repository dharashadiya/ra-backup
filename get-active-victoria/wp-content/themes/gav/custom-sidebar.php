<?php if (!is_page_template('tmpl-teams-internal.php')) : ?>
	<div class="animate">
    	<a href="https://app.getactive.vic.gov.au" title="Register for Get Active Victoria!" class="c-sidebar__item c-sidebar__register o-btn o-btn--join">Join today</a>
	</div>
<?php endif; ?>

<div class="c-sidebar__item animate">
    <div class="c-sidebar__resources">
        <?php
        $args = array(
            'post_type' => 'post',
            'status' => 'published',
            'posts_per_page' => 1,
            'fields' => 'ids'
        );
        $posts = get_posts( $args );
        foreach ($posts as $item) :
            set_query_var( 'var', array('ID' => $item) );
		    get_template_part( 'partials/resource-tile' );
        endforeach; ?>
		<div class="c-sidebar__resources-more">
        	<a href="/resources" class="">More resources</a>
		</div>
    </div>
</div>