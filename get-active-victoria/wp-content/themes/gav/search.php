<?php get_header(); ?>

<div class="c-search">
	<main id="Main" class="c-main-content o-main" role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="c-cms-content c-search-content o-wrapper">
				<h1 class="page-title"><?php printf( __( 'Search results for: "%s"'), get_search_query() ); ?></h1>
				<?php
					$results = array();
					if ( have_posts() ) while (have_posts()) : the_post();
						$results[$post->post_type][] = $post->ID;
					endwhile;
				?>
				<?php if ($results['page'] && count($results['page']) > 0) : ?>
					<h2 class="c-search__title">Pages</h2>
					<div class="o-layout o-module">
						<?php foreach ($results['page'] as $item) :
							set_query_var( 'var', array('ID' => $item, 'class' => 'o-layout__item o-module__item u-1/4@laptop u-1/3@tablet u-1/2@mobileLandscape') );
							get_template_part( 'partials/page-tile' );
						endforeach; ?>
					</div>
				<?php endif; ?>
				<?php if ($results['post'] && count($results['post']) > 0) : ?>
					<h2 class="c-search__title">Resources</h2>
					<div class="o-layout o-module">
						<?php foreach ($results['post'] as $item) :
							set_query_var( 'var', array('ID' => $item, 'class' => 'o-layout__item o-module__item u-1/4@laptop u-1/3@tablet u-1/2@mobileLandscape') );
							get_template_part( 'partials/resource-tile' );
						endforeach; ?>
					</div>
				<?php endif; ?>

			</div>
		</article>
	</main>

</div>

<?php get_footer(); ?>