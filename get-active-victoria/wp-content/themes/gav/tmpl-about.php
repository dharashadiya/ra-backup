<?php
/*
 * Template Name: About Page
 */
?>
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div class="c-about">
        <div class="o-wrapper">
             <div class="o-layout o-layout--large">
                <div class="o-layout__item u-3/4@laptopMed u-2/3@tablet">
                    <div class="c-about__content animate-elems">
                        <h1 class="page-title"><?php the_title(); ?></h1>
                        <div class="large-para">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <?php $explores = get_field('more_explore');
                    if ($explores) : ?>
                        <div class="c-about__explores animate">
                            <h3>More to explore</h3>
                            <div class="o-layout">
                                <?php foreach ($explores as $item) : ?>
                                    <div class="o-layout__item u-1/2@tablet">
                                    <a href="<?php echo $item['link']['url'] ?>" >
                                        <div class="c-about__explores-each" style="background-image: url(<?php echo $item['image'] ?>)">
                                        </div>
                                        <span class="o-btn o-btn--pink"><?php echo $item['link']['title'] ?></span>
                                    </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="c-page-modified"><p><span>Last updated: </span><?php the_modified_date('F j, Y'); ?><span> at: </span><?php the_modified_date('g:i a'); ?></p></div>
                </div>
                 <div class="o-layout__item u-1/4@laptopMed u-1/3@tablet sidebar-sticky">
                    <div class="c-sidebar">
                        <?php get_template_part( 'custom', 'sidebar' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>