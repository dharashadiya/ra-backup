<?php get_header(); ?>
<div class="c-home">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<!-- home page top banner -->
		<?php $banner = get_field('top_banner');?>
		<?php if ($banner) : ?>
		<div class="c-home__slider">
			<?php foreach ($banner as $item) : ?>
				<div class="c-home-banner" style="background-image: url(<?php  echo $item['background_image'];  ?>)">
					<div class="o-wrapper">
						<div class="c-home-banner__content">
							<h1><?php echo $item['title']; ?></h1>
							<div class="large-para">
								<p><?php echo $item['copy'];  ?></p>
							</div>
							<?php if ($item['button']) : ?>
								<a href="<?php echo $item['button']['url']; ?>" class="o-btn o-btn--blue"><?php echo $item['button']['title']; ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php endif;?>
			<div class="o-wrapper">
				<div class="c-home-videos animate">
					<?php if (get_field('intro_heading')) : ?>
						<h2><?php echo get_field('intro_heading'); ?></h2>
					<?php endif; ?>
					<div class="c-home-videos__list">
						<div class="slick-controls">
							<button class="prev slick-arrow"> <?php echo svgicon('prev', '0 0 40	40')?> </button>
							<button class="next slick-arrow"> <?php echo svgicon('next', '0 0 40	40')?> </button>
						</div>
						<div class="c-slick">
						<?php
							$carousel_posts = get_field('workout_games_challenges');
							$terms = get_terms('workout_category', 'number=5');
							foreach ($terms as $item) :
								$cat_content = get_field('category_content', $item);
    							$img = $cat_content['featured_image'];
							?>
							<?php endforeach; ?>
							<?php foreach ($carousel_posts as $item) : ?>
								<div class="c-home-videos__each">
									<div class="c-home-videos__each-inner">
										<?php if ($item['select'] === 'Workout Category') :
											$id = $item['workout_category'];
											$cat_content = get_field('category_content', get_term($id));
											$img = $cat_content['featured_image'];?>
											<a href="javascript:void(0);" class="js-open-modal bgimg" style="background-image:url(<?php echo $img ?>)" data-content="<?php echo $cat_content['preview_video'] ?>" data-btn="Join today to watch the full workout" data-sharelink="<?php echo SITE; ?>/around-home/workouts-cat/<?php echo get_term( $id )->slug; ?>">
												<span><?php echo svgicon('play', '0 0 80 80','c-svgicon--play') ?></span>
											</a>
											<div class="c-home-videos__each-title">
												<span><?php echo get_term( $id )->name;?></span>
											</div>
										<?php elseif($item['select'] === 'Custom'):
											$custom = $item['custom'];
											$type = strtolower($custom['type']);?>
											<a href="javascript:void(0);" class="js-open-modal bgimg" style="background-image:url(<?php echo $custom['image'] ?>)" data-content="<?php echo $custom['teasure_url'] ?>" data-btn="Join today to watch the full <?php echo $type; ?>">
												<span><?php echo svgicon('play', '0 0 80 80','c-svgicon--play') ?></span>
											</a>
											<div class="c-home-videos__each-title">
												<span><?php echo $custom['title'];?></span>
											</div>
										<?php else:
											$id = $item['games_challenges'];
											$content = get_field('website_content', $id);
											$img = get_the_post_thumbnail_url($id);
											$post_type = get_post_type( $id );
											$btn = "Join today to watch the full games";
											if ($post_type === 'ra-challenges') {
												$btn = "Join today to watch the full challenge";
											}
											?>

											<a href="javascript:void(0);" class="js-open-modal bgimg" style="background-image:url(<?php echo $img ?>)" data-content="<?php echo $content['teaser_url'] ?>" data-btn="<?php echo $btn; ?>" data-sharelink="<?php echo get_the_permalink($id); ?>">
												<span><?php echo svgicon('play', '0 0 80 80','c-svgicon--play') ?></span>
											</a>
											<div class="c-home-videos__each-title">
												<span><?php echo wp_trim_words(get_the_title($id), 4); ?></span>
											</div>
										<?php endif; ?>
									</div>
								</div>

							<?php endforeach; ?>

						</div>
					</div>
					<a href="/around-home" class="o-btn o-btn--blue">Get inspired with workouts, challenges and games</a>
				</div>
			</div>
			<?php $team = get_field('team_section') ?>
			<div class="c-home-team lazyload" data-src="<?php echo $team['background_image'] ?>">
				<div class="o-wrapper">
					<div class="o-layout c-home-team__wrapper">
						<div class="o-layout__item u-1/2@tablet animate">
							<div class="c-home-team__content">
								<h2 class="page-title h2--alt"><?php echo $team['title']; ?></h2>
								<p><?php echo $team['copy']; ?></p>
								<a href="/teams" class="o-btn o-btn--blue">Learn about teams</a>
							</div>
						</div>
						<div class="o-layout__item u-1/2@tablet">
							<div class="o-layout">
								<?php foreach ($team['items'] as $item) : ?>
									<div class="o-layout__item u-1/2@mobileLandscape animate">
										<div class="c-home-team__list">
											<a href="<?php echo $item['link']['url'] ?>">
												<div class="bg lazyload" data-src="<?php echo $item['image'] ?>"></div>
												<span><?php echo $item['link']['title'] ?></span>
											</a>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>

				</div>
			</div>

			<?php $tracking = get_field('tracking_section'); ?>
			<div class="c-home-tracking">
				<div class="o-wrapper">
					<div class="o-layout">
						<div class="o-layout__item u-1/2@tablet animate">
							<!-- <img class="lazyload" data-src="<?php //echo $tracking['image'] ?>" alt="Tracking"> -->
							<video class="c-video" preload="metadata" autoplay loop muted playsinline>
								<source src="<?php echo $tracking['video']?>" type="video/mp4">
							</video>
						</div>
						<div class="o-layout__item u-1/2@tablet animate">
							<h2 class="page-title h2--alt"><?php echo $tracking['title']; ?></h2>
							<p><?php echo $tracking['copy']; ?></p>
							<a href="https://app.getactive.vic.gov.au" class="o-btn o-btn--blue">Join today</a>
						</div>
					</div>
				</div>
			</div>

			<?php $resources = get_field('resources_section'); ?>
			<div class="c-home-resources lazyload" data-src="<?php echo $resources['background_image']  ?>">
				<div class="o-wrapper">
					<div class="c-home-resources__content animate">
						<h2 class="page-title h2--alt"><?php echo $resources['title']; ?></h2>
						<p><?php echo $resources['copy']; ?></p>
					</div>
					<div class="o-layout o-module animate c-home-resources__list">
						<?php
						$args = array(
							'post_type' => 'post',
							'status' => 'published',
							'numberposts' => -1,
							'fields' => 'ids',
							'posts_per_page' => 3,
						);
						$posts = get_posts( $args );
						foreach ($posts as $item) :
							set_query_var( 'var', array('ID' => $item, 'class' => 'o-layout__item o-module__item u-1/3@tablet') );
							get_template_part( 'partials/resource-tile' );
						endforeach; ?>
					</div>
				</div>
			</div>
			<?php  ?>
		</article>
	<?php endwhile; ?>
</div>

<?php get_footer(); ?>