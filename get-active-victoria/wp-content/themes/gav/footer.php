<!-- page bottom banner -->
<?php if (is_page('around-home') || is_page_template('tmpl-workout.php') || is_page_template('tmpl-teams.php') || is_page_template('tmpl-games.php') || is_page_template('tmpl-challenges.php')) : ?>
	<div class="o-wrapper o-wrapper--1025 c-banner-bottom">
			<?php $bottom_banner = get_field('bottom_banner', 'options'); ?>
			<?php if ($bottom_banner) : ?>
			   <div class="c-banner" style="background-image: url(<?php echo $bottom_banner['background_image']  ?>)">
					<div class="c-banner__inner animate">
						<div class="c-banner__content">
							<div class="large-para">
								<p><?php echo $bottom_banner['copy']; ?></p>
							</div>
						</div>
						<a href="https://app.getactive.vic.gov.au" title="Register for Get Active Victoria!" class="o-btn o-btn--join">Join today</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
<?php endif; ?>


<!-- Restriction / Info bottom banner -->

<?php if (is_front_page() && get_field('popup_active', 'options')) { ?>
  <div class="c-bottom-info-banner">
	<div class="o-wrapper o-wrapper--1025">
		<div class="c-bottom-info-banner__content">
			<div class="c-bottom-info-banner__content_mobile"><?php echo get_field('mobile_text', 'options'); ?></div>
			<div class="c-bottom-info-banner__content_desktop"><?php echo get_field('desktop_text', 'options'); ?></div>
		</div>
	</div>
	<a href="javascript:void(0);" class="c-bottom-info-banner__cross">
		<?php echo svgicon('close', '0 0 35 35') ?>
	</a>
  </div>
<?php } ?>



</div><!-- .c-content -->
<div class="c-footer">
  <!-- <div class="c-footer__instagram">
		<div class="o-wrapper">
				<div class="c-footer__instagram-title">
					<h3>Latest Instagram Feed</h3>
				</div>
		</div>
		<div class="c-footer__instagram-pictures">
			<?php // echo wdi_feed(array('id'=>'1')); ?>
		</div>
	</div> -->
	<div class="o-wrapper o-wrapper--1080">
		<div class="o-layout">
			<div class="o-layout__item u-1/2@tablet">
				<div class="c-footer__info">
					<p>&copy; <?php echo date('Y'); ?> <a href="https://www.vic.gov.au/" id="dhhslink">Department of Jobs, Precincts & Regions</a></p>
					<span><a href="/terms-conditions/">Terms &amp; Conditions</a> | <a href="/privacy-policy/">Privacy</a> | <a href="/accessibility-information/ ">Accessibility</a>&nbsp;|&nbsp;<a href="/contact">Contact</a></p>
				</div>
			</div>
			<div class="o-layout__item u-1/2@tablet">
				<div class="c-footer__logo">
					<?php echo svgicon('vic-logo', '0 0 1263 728', 'c-footer__logo'); echo svgicon('logo', '0 0 174 104', 'c-footer__logo-img');
					?>
					<!-- <img src="<?php //echo STYLESHEET_URL; ?>/assets/img/vic-logo.png" alt="Victoria Premier's"><img class="c-footer__logo-img" src="<?php //echo STYLESHEET_URL; ?>/assets/img/footer-logo.png" alt="Get Active Victoria"> -->
				</div>
			</div>
		</div>
	</div>
</div>

<div class="c-search__container" data-toggle-content="search">
  <div class="o-wrapper o-wrapper--1080">
	  <a class="c-overlay__close" data-toggle="search"><?php echo svgicon('close', '0 0 35 35'); ?></a>
	<div class="c-search">
	  <form role="search" method="get" class="c-action  c-search__form search-form" action="<?php echo home_url( '/' ); ?>">
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
		<!-- <label for="search" class="c-search__label">Enter your search</label> -->
		<input type="search" class="search-field c-search__input"
		  placeholder="<?php echo esc_attr_x( 'Enter your search', 'placeholder' ) ?>"
		  value="<?php echo get_search_query() ?>" name="s"
		  title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" id="search"/>
		<input type="submit" value="Submit" class="c-search__submit o-btn"/>
	  </form>
	</div>
  </div>
</div>

<!-- Intro modal -->
<?php if (is_front_page()) : ?>
<div class="c-modal intro-modal">
	<div class="c-modal__container">
		<div class="c-modal__close"><?php echo svgicon('close', '0 0 35 35') ?></div>
		<div class="c-modal__content">
			<?php echo svgicon('logo', '0 0 174 104', 'c-modal__logo') ?>
			<p class="intro-para">Welcome to Get&nbsp;Active&nbsp;Victoria</p>
			<p>A new initiative from the Victorian State Government to inspire all Victorians to move more, every day.</p>
			<p>Premier’s Active April is now closed for 2020. But,  Get&nbsp;Active&nbsp;Victoria is here with tools and ideas for getting active all year round.</p>
		</div>
	</div>
</div>
<?php endif; ?>

<!-- workout video modal -->
<div class="c-modal video-modal">
	<div class="c-modal__close"><?php echo svgicon('close', '0 0 35 35') ?></div>
	<div class="c-modal__container">
		<div class="c-modal__content">
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>" id="share_link" class="c-modal__facebook"><img src="<?php echo ASSETS; ?>/img/facebook-share.png" alt=""></a>
			<div class="o-aspect o-aspect--16by9">
				<iframe id="video_container" src="" style="border:none;overflow:hidden" width="890" height="500" scrolling="no" allowTransparency="true" allowFullScreen="true"></iframe>
			</div>
			<a href="https://app.getactive.vic.gov.au" id="js-video-btn" title="Register for Get Active Victoria!" class="o-btn o-btn--join">Join today to watch the full workout</a>
		</div>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>