<?php get_header();

$term = get_queried_object();

if ($term && $term->taxonomy === 'workout_category') :
	$website_content = get_field('category_content', $term);
?>
<div class="c-single-teaser">
	<div class="o-wrapper o-wrapper--950">
		<div class="c-single-teaser__back"><a href="<?php echo SITE; ?>/around-home/workouts">< Back to workouts</a></div>
		<div class="c-single-teaser__intro">
			<h1><?php echo $term->name; ?></h1>
			<p><?php echo $term->description; ?></p>
		</div>
		<div class="c-single-teaser__teaser o-aspect o-aspect--16by9">
			<iframe src="<?php echo $website_content['preview_video']; ?>?title=0&byline=0&portrait=0&badge=0" style="border:none;overflow:hidden" width="890" height="500" scrolling="no" allowTransparency="true" allowFullScreen="true"></iframe>
		</div>
		<div class="c-single-teaser__stats">
			<p class="c-activity-tile__grad"><span class="c-activity-tile__workout">Workouts</span><?php echo $term->count ?></p>
			<p class="c-activity-tile__grad"><span class="c-activity-tile__effort">Effort</span><span><?php echo $website_content['effort'] ?><span></p>
			<p class="c-activity-tile__grad"><span class="c-activity-tile__level">Level</span><span><?php echo $website_content['level'] ?><span></p>
		</div>
		<a href="https://app.getactive.vic.gov.au" title="Register for Get Active Victoria!" class="o-btn o-btn--join">Join today to watch the full workout</a>
	</div>
</div>
<?php endif;
get_footer(); ?>