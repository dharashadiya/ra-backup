<?php get_header(); ?>

<div class="c-default">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="o-wrapper">
		<div class="o-layout o-layout--large">
			<div class="o-layout__item u-3/4@laptopMed u-2/3@tablet">
				<div class="c-default__content">
					<h1 class="page-title animate"><?php the_title(); ?></h1>
					<div class="large-para animate-elems">
						<?php the_content(); ?>
					</div>
					<?php  get_template_part( 'partials/flexible-content'); ?>
				</div>
				<div class="c-page-modified"><p><span>Last updated: </span><?php the_modified_date('F j, Y'); ?><span> at: </span><?php the_modified_date('g:i a'); ?></p></div>
			</div>
			<div class="o-layout__item u-1/4@laptopMed u-1/3@tablet sidebar-sticky">
				<div class="c-sidebar">
					<?php get_template_part( 'custom', 'sidebar' ); ?>
				</div>
			</div>
		</div>
	</div>
<?php endwhile; ?>

</div>

<?php get_footer(); ?>