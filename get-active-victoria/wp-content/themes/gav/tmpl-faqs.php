<?php
/*
 * Template Name: FAQs Page
 */
?>
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div class="c-faqs">
        <div class="o-wrapper">
             <div class="o-layout o-layout--large">
                <div class="o-layout__item u-3/4@laptopMed u-2/3@tablet">
                    <div class="c-faqs__content animate">
                        <h1 class="page-title"><?php the_title(); ?></h1>
                        <div class="large-para">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <div class="c-accordion animate">
                        <?php $faq_section = get_field('faq_section');
                        foreach ($faq_section as $section) : ?>
                            <h2 class="c-accordion__heading"><?php echo $section['heading'] ?></h2>
                            <?php foreach ($section['faqs'] as $item) : ?>
                                <div class="c-accordion__item">
                                    <a href="javascript:void(0);" class="c-accordion__question"><?php echo $item['question']; ?><img src="<?php echo ASSETS ?>/img/select-arrow.png" alt=""></a>
                                    <div class="c-accordion__answer">
                                        <div class="c-accordion__content"><?php echo $item['answer']; ?></div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>

                    <div class="c-page-modified"><p><span>Last updated: </span><?php the_modified_date('F j, Y'); ?><span> at: </span><?php the_modified_date('g:i a'); ?></p></div>
                </div>
                 <div class="o-layout__item u-1/4@laptopMed u-1/3@tablet sidebar-sticky">
                    <div class="c-sidebar">
                        <?php get_template_part( 'custom', 'sidebar' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php endwhile; ?>

<?php get_footer(); ?>