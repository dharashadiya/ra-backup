
 <?php
    $var = get_query_var('var');
    $term = $var['term'];
    $class = '';
    if ($var && $var['class']) {
        $class = $var['class'];
    }
    $cat_content = get_field('category_content', $term);
    $img = $cat_content['featured_image'];

 ?>

<div class="<?php echo $class?> c-activity-tile animate">
    <div class="c-activity-tile__wrap">
		<?php if ($cat_content['preview_video']) : ?>
			 <a href="javascript:void(0);" class="js-open-modal c-activity-tile__img lazyload" data-preview="<?php echo $term->slug; ?>" data-src="<?php echo $img ?>" data-content="<?php echo $cat_content['preview_video'] ?>"  data-btn="Join to access workouts" data-sharelink="<?php echo SITE; ?>/around-home/workouts-cat/<?php echo $term->slug; ?>">
				<span><?php echo svgicon('play', '0 0 80 80','c-svgicon--play') ?></span>
			</a>
		<?php else : ?>
			<div class="c-activity-tile__img lazyload"  data-src="<?php echo $img ?>"></div>
		<?php endif; ?>

        <div class="c-activity-tile__content">
            <h3><?php echo $term->name; ?></h3>
            <p><?php echo wp_trim_words($term->description, 9) ?></p>
			<p class="c-activity-tile__grad"><span class="c-activity-tile__workout">Workouts</span><?php echo $term->count ?></p>
			<p class="c-activity-tile__grad"><span class="c-activity-tile__effort">Effort</span><span><?php echo $cat_content['effort'] ?><span></p>
			<p class="c-activity-tile__grad"><span class="c-activity-tile__level">Level</span><span><?php echo $cat_content['level'] ?><span></p>
        </div>
        <a href="https://app.getactive.vic.gov.au" class="c-activity-tile__more">
            <span class="">Join to access workouts</span>
        </a>
    </div>
</div>