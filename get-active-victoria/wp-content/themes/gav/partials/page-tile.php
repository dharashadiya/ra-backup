<?php
    $var = get_query_var('var');
    $id = $var['ID'];
    $class = '';
    if ($var && $var['class']) {
        $class = $var['class'];
    }
 ?>
<div class="<?php echo $class?> c-page-tile animate">
    <a class="c-page-tile__wrap" href="<?php echo get_the_permalink($id); ?>" rel="bookmark" title="<?php echo get_the_title($id); ?>">
        <div class="c-page-tile__content">
            <h3><?php echo get_the_title($id); ?></h3>
            <p>
            <?php
                $str = get_the_excerpt($id);
                if (strlen($str) > 15) {
                    $str = substr($str, 0, 100) . '...';
                }
                print_r($str);
            ?>
            </p>
        </div>
        <div class="c-page-tile__more">
          <span class="">Read more</span>
        </div>
    </a>
</div>