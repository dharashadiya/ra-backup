<?php
    $var = get_query_var('var');
    $resource = $var['ID'];
    $class = '';
    if ($var && $var['class']) {
        $class = $var['class'];
    }
    $img = get_the_post_thumbnail_url($resource);
    $category = get_the_terms($resource, 'category');
 ?>

<div class="<?php echo $class?> c-resources-tile animate">
    <a href="<?php echo get_the_permalink($resource); ?>" class="c-resources-tile__wrap" alt="<?php echo get_the_title($resource); ?>">
        <div class="c-resources-tile__img lazyload" data-src="<?php echo $img ?>"></div>
        <div class="c-resources-tile__content">
            <h3><?php echo get_the_title($resource); ?></h3>
            <!-- <div class="c-resources-tile__date">
                <?php echo get_the_date('j F, Y', $resource); ?> | <span class="c-resources-tile__category"><?php
                foreach ($category as $key => $cats) {
					if ($cats->slug !== 'uncategorised') {
						echo ($key !== 0 ? ', ' : '') . $cats->name;
					}
				} ?></span>
            </div> -->
            <p><?php echo wp_trim_words(get_the_content(null, false, $resource), 14) ?></p>
            <!-- <p><?php echo mb_strimwidth(get_the_content(null, false, $resource), 0, 140, '...'); ?></p> -->
        </div>
        <div class="c-resources-tile__more">
            <span class="">Read more</span>
        </div>
    </a>
</div>