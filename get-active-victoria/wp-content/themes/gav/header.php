<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
	<title><?php wp_title(); ?> </title>

	<?php if(is_local_or_staging()) : ?>
		<meta name="robots" content="none" />
		<meta name="robots" content="noindex" />
		<meta name="googlebot" content="none" />
		<meta name="googlebot" content="noindex" />
	<?php endif; ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">
	<link rel="stylesheet" href="https://use.typekit.net/vil2bap.css">
	<?php
    	$preview = get_the_post_thumbnail_url(get_the_ID(), 'full');
		$term = get_queried_object();
		if ($term && isset($term->taxonomy) && $term->taxonomy === 'workout_category') {
			$cat_content = get_field('category_content', $term);
			$preview = $cat_content['featured_image'];
		}
    	if (!$preview) {
      		$preview = ASSETS . '/img/gav-social-image.jpg';
    	}
   	?>
	<?php if ($preview) : ?>
		<meta property="og:image" content="<?php echo $preview ?>" />
	<?php endif; ?>
	<?php wp_head(); ?>
	<?php include('partials/inlinescripts.min.php'); ?>

	<!-- faceboox pixel -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '599820354173114');
	fbq('track', 'PageView');
	</script>
	<noscript>
	<img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=599820354173114&ev=PageView&noscript=1"/>
	</noscript>


	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WFSNJ2X');</script>
	<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WFSNJ2X"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="c-top--spacer"></div>
<div class="c-top">
	<header class="c-header o-wrapper">
		<?php
			$logo_wrap_class = "c-logo-wrap";
			if (!is_front_page()) {
				$logo_wrap_open = '<a class="' . $logo_wrap_class . ' is-link" href="/" rel="home" title="Back to home">';
				$logo_wrap_close = '</a>';
			} else {
				$logo_wrap_open = '<span class="' . $logo_wrap_class . '">';
				$logo_wrap_close = '</span>';
			}
		?>
		<?php
			echo $logo_wrap_open;
			echo svgicon('logo', '0 0 174 104', 'c-header__logo');
			echo $logo_wrap_close;
		?>
		<a href="#Main" class="c-skip">Skip to main content</a>
		<nav class="c-site-nav" role="navigation">

			<div class="c-nav__mobile">
				<?php
					wp_nav_menu( array(
					'theme_location' => 'primary-menu',
					'container' => false,
					'menu_class' => 'c-nav',
					'menu_id' => 'menu'
					));
				?>
				 <ul class="c-nav__social">
					<li><a aria-label="Search Button" href="javascript:void(0)" class="c-nav__social-search" data-toggle="search"><?php echo svgicon('search', '0 0 32 32'); ?></a></li>
					<li><a aria-label="Facebook Link" href="https://www.facebook.com/getactivevic" class="c-social__link" data-toggle="search"><?php echo svgicon('facebook', '0 0 32 32'); ?></a></li>
					<li><a aria-label="Instagram Link" href="https://www.instagram.com/getactivevic" class="c-social__link" data-toggle="search"><?php echo svgicon('instagram', '0 0 32 32'); ?></a></li>
					<li><a href="https://app.getactive.vic.gov.au/login" title="Login to Get Active Victoria!" class="o-btn o-btn--yellow o-btn--nav">Login</a></li>
				</ul>
			</div>
		</nav>
		<button id="toggler" class="c-nav-toggle"><span class="c-nav-toggle__icon"><span></span></span></button>
	</header>
</div>
<div class="c-content">
<!-- top banner -->
<?php if ( !is_search() && have_posts() ) while ( have_posts() ) : the_post();
$banner = get_field('banner_image'); if ($banner) : ?>
<div class="c-banner lazyload" data-src="<?php echo $banner  ?>">
	<div class="o-wrapper c-banner__inner">
		<div class="c-banner__content animate">
			<h1><?php the_title(); ?></h1>
			<div class="large-para"><?php the_content(); ?></div>
		</div>
		<a href="https://app.getactive.vic.gov.au" title="Register for Get Active Victoria!" class="o-btn o-btn--join">Join today</a>
	</div>
</div>
<?php endif; ?>
<?php endwhile; ?>