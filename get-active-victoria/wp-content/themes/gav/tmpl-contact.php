<?php
/*
 * Template Name: Contact Page
 */
?>

<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="c-contact">
  <div class="o-wrapper">
    <div class="o-layout o-layout--large">
        <div class="o-layout__item u-3/4@laptopMed u-2/3@tablet animate">
            <h1 class="page-title"><?php the_title(); ?></h1>
            <div class="large-para">
                <?php the_content(); ?>
            </div>
			<?php $tabs = get_field('tabs'); ?>
			<?php if ($tabs) : ?>
				<div class="c-contact__tabs">
					<?php foreach ($tabs as $key => $item) : ?>
						<?php if ($item['tab_label']) : ?>
							<a href="javascript:void(0);" data-target="<?php echo stripString($item['tab_label']); ?>" class="js-tabs-link o-btn o-btn--round-outline"><?php echo $item['tab_label']; ?></a>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
				<?php foreach ($tabs as $key => $item) : ?>
					<div class="animate c-contact__tab-container js-tabs__<?php echo stripString($item['tab_label']); ?>">
						<?php if ($item['copy']) : ?>
							<div class="cms-content">
								<?php echo $item['copy']; ?>
							</div>
						<?php endif; ?>
						<?php if ($item['heading']) : ?>
							<h3><?php echo $item['heading']; ?></h3>
						<?php endif; ?>
						<?php if ($item['form']) : ?>
							<div class="c-form">
								<?php echo do_shortcode($item['form']) ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
        </div>
        <div class="o-layout__item u-1/4@laptopMed u-1/3@tablet sidebar-sticky">
            <div class="c-sidebar">
                <?php get_template_part( 'custom', 'sidebar' ); ?>
            </div>
        </div>
    </div>
  </div>
</div>
<?php endwhile; ?>

<?php get_footer(); ?>
