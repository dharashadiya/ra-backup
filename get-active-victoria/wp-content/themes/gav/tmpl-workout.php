<?php
/*
 * Template Name: Workout Page
 */
?>
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div class="c-workout">
        <div class="o-wrapper">
			<?php

			$args = array(
					'post_type' => 'ra-workout',
					'status' => 'published',
					'numberposts' => -1,
					'fields' => 'ids',
					'meta_query' => array(
						array(
							'key' => "website_content_featured",
							'value' => true,
							'compare' => 'LIKE'
						)
					)
                );

				$posts = get_posts( $args );
				if (count($posts) > 0) : ?>
					<div class="c-workout__features">
						<h2>Featured workouts</h2>
						<div class="o-layout o-module">
							<?php foreach ($posts as $key => $item) :
								$slug = get_post_field( 'post_name', $item );
								$workout = get_field('website_content', $item);
								$img = $workout['thumbnail']; ?>
								<div class="o-layout__item o-module__item u-1/2@tablet">
									<a href="javascript:void(0);" class="js-open-modal c-workout__featured" data-preview="<?php echo $slug; ?>" data-content="<?php echo $workout['teaser_url'] ?>" data-btn="Join to access workouts" data-sharelink="<?php echo get_the_permalink($item); ?>">
										<div class="c-workout__featured-img lazyload" data-src="<?php echo $img ?>"></div>
										<div class="c-workout__featured-inner">
											<span class="c-workout__featured-icon"><?php echo svgicon('play', '0 0 80 80','c-svgicon--play') ?></span>
											<div class="c-workout__featured-content">
												<h3><?php echo wp_trim_words(get_the_title($item)); ?></h3>
												<p><?php echo wp_trim_words($workout['description'], 9) ?></p>
											</div>
										</div>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				 <?php endif; ?>
			<?php
			$terms = get_terms('workout_category');
			if (count($terms) > 0) : ?>
			<div class="c-workout__list">
				<h2>Workouts</h2>
				<div class="o-layout o-module">
					<?php foreach ($terms as $key => $item) :
						$class = 'o-layout__item o-module__item u-1/3@tabletWide u-1/2@mobileLandscape ';
						set_query_var( 'var', array('term' => $item, 'class' => $class) );
						get_template_part( 'partials/activity-tile' );
					endforeach; ?>
				</div>
			</div>
			<?php endif; ?>
        </div>
    </div>
<?php endwhile; ?>

<?php get_footer(); ?>