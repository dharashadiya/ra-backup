<?php get_header(); ?>

<div class="c-single-post">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="c-cms-content o-wrapper o-wrapper--950 animate">
				<?php $img = get_the_post_thumbnail_url(); ?>
				<?php if ($img) : ?>
					<div class="c-single-post__banner lazyload" data-src="<?php echo $img ?>" ></div>
				<?php endif; ?>
				<a href="/resources" class="c-single-post__backlink">< Back to all resources</a>
                <h1 class="page-title"><?php the_title(); ?></h1>
				<?php $author = get_field('resource_author'); ?>
                <div class="large-para animate-elems">
					<?php if ($author) : ?>
						<p><?php echo $author; ?></p>
					<?php endif; ?>
                    <?php the_content(); ?>
				</div>
				 <div class="c-page-modified"><p><span>Last updated: </span><?php the_modified_date('F j, Y'); ?><span> at: </span><?php the_modified_date('g:i a'); ?></p></div>
			</div>
			<div class="c-single-post__related">
				<div class="o-wrapper o-wrapper--950 aniamte">
					<h2>Related articles</h2>
					<p>Looking for more information or inspiration? Check out some of our suggested articles below:</p>
					<div class="o-layout o-module">
						<?php
						$curr_category = get_the_category();
						$curr_cat = array();
						foreach ($curr_category as $value) {
							$curr_cat[] = $value->term_id;
						}

						$args = array(
							'post_type' => 'post',
							'status' => 'published',
							'numberposts' => -1,
							'fields' => 'ids',
							'posts_per_page' => 3,
							'exclude'      => array(get_the_id()),
							'category__in' => $curr_cat,
						);
						$posts = get_posts( $args );
						foreach ($posts as $item) :
							set_query_var( 'var', array('ID' => $item, 'class' => 'o-layout__item o-module__item u-1/3@tablet u-1/2@mobileLandscape') );
							get_template_part( 'partials/resource-tile' );
						endforeach; ?>
					</div>
				</div>
			</div>
		</article>
	<?php endwhile; ?>
</div>

<?php get_footer(); ?>