<?php
/*
 * Template Name: Resources Page
 */
?>
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div class="c-resources">
        <div class="o-wrapper">
            <?php
                $curr_cat = esc_attr( sanitize_text_field($_GET['cat']) );
                $page_no = esc_attr( sanitize_text_field($_GET['page_no']) );
                $posts_per_page = 12;
                if (!$page_no || !is_numeric($page_no)) {
                    $page_no = 1;
                }

                $categories = get_categories( array(
                    'orderby' => 'name',
                    'order'   => 'ASC'
                ));
				$is_only_uncategorised = true;
				foreach ($categories as $cat) {
					if ($cat->slug !== 'uncategorised') {
						$is_only_uncategorised = false;
					}
				}

				if ($is_only_uncategorised) {
					$curr_cat = '';
				}

                $args = array(
                    'post_type' => 'post',
                    'status' => 'published',
                    'numberposts' => -1,
                    'fields' => 'ids',
                    'posts_per_page' => -1,
                    'category_name' => $curr_cat,
                );
                $total_posts = get_posts( $args );
                $total_posts = count($total_posts);
                $pages = ceil($total_posts / $posts_per_page);

                $args['posts_per_page'] = $posts_per_page;
                $args['paged'] = $page_no;
                $posts = get_posts( $args );
            ?>
			<?php if (!$is_only_uncategorised) : ?>
				<div class="c-resources__filter animate">
					<a href="/resources" alt="all filter" class="c-resources__filter-item o-btn o-btn--round-outline <?php echo (!$curr_cat ? "is-active" : "") ?>">All</a>
					<?php foreach ($categories as $cat) : if ($cat->slug !== 'uncategorised') : ?>
						<a href="/resources?cat=<?php echo $cat->slug ?>" alt="<?php echo $cat->name; ?>" class="c-resources__filter-item o-btn o-btn--round-outline <?php echo ($curr_cat && $curr_cat === $cat->slug ? "is-active" : "") ?>"><?php  echo $cat->name; ?></a>
					<?php endif; endforeach; ?>
				</div>
			<?php endif; ?>
            <div class="c-resources__list">
                <?php if (count($posts) === 0) : ?>
                    <p class="c-resources__notfound">No resources found</p>
                <?php endif; ?>
                <div class="o-layout o-module">
                    <?php foreach ($posts as $item) :
                        set_query_var( 'var', array('ID' => $item, 'class' => 'o-layout__item o-module__item u-1/4@laptop u-1/3@tablet u-1/2@mobileLandscape') );
                        get_template_part( 'partials/resource-tile' );
                    endforeach; ?>
                </div>
				<?php if ($pages > 1) : ?>
					<div class="c-resources__pagination">
						<?php
							//Ref: https://stackoverflow.com/questions/6354303/paging-like-stackoverflows
							$cntAround = 1;
							for ($i = 1; $i <= $pages; $i++) {
								$url = get_the_permalink() . '?page_no=' . $i;
								if ($curr_cat) {
									$url = get_the_permalink() . '?cat=' . $curr_cat . '&page_no=' . $i;
								}

								$isGap = false;
								if ($cntAround >= 1 && $i > 1 && $i < $pages && abs($i - $page_no) > $cntAround) {
									$isGap = true;
									$i = ($i < $page_no ? $page_no - $cntAround : $pages) - 1;
								}

								if ($isGap) {
									$lnk = '<span>...</span>';
								} elseif ($i != $page_no && !$isGap) {
									$lnk = '<a href="' . $url . '">' . $i . '</a>';
								} elseif ($i == $page_no && !$isGap) {
									$lnk = '<a class="is-active" href="' . $url . '">' . $i . '</a>';
								}
								echo $lnk;
							}
						?>
					</div>
				<?php endif; ?>
            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>