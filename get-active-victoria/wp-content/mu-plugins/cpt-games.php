<?php
// Register Custom Post Type
add_action( 'init', 'ra_games_post_type', 0 );
function ra_games_post_type() {
	$args = array(
        'label'                 => __( 'Games', 'ra-games' ),
		'description'           => __( 'Games posts', 'ra-games' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array(
			'with_front' => false,
			'slug' => 'around-home/games'
		),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 5,
		'menu_icon'             => 'dashicons-format-video',
        'supports'           => array( 'title', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes' ),
    );
	register_post_type( 'ra-games', $args );
}


add_action( 'init', 'games_register_custom_taxonomy', 0 );
function games_register_custom_taxonomy() {
    $args = array(
        'label'        => __( 'Games Category', 'games_category' ),
        'public'       => true,
        'rewrite'      => true,
        'hierarchical' => true
    );
    register_taxonomy( 'games_category', 'ra-games', $args );
}

?>