<?php
/*
Plugin Name: Workout - Custom Post Type
Description: Workout - Custom Post Type for Active April
*/

add_action( 'init', 'register_workout_custom_post_type' );
function register_workout_custom_post_type() {
  register_post_type( 'ra-workout', array(
    'label'  => __('Workout', 'ra-workout'),
    'description' => __('Workout', 'ra-workout'),
    'labels' => array(
      'name'               => _x( 'Workout', 'post type general name', 'ra-workout' ),
      'singular_name'      => _x( 'Workout', 'post type singular name', 'ra-workout' ),
      'menu_name'          => __( 'Workout', 'admin menu', 'ra-workout' ),
      'parent_item_colon'  => __( 'Parent Workout:', 'ra-workout' ),
      'name_admin_bar'     => __( 'Workout', 'add new on admin bar', 'ra-workout' ),
      'all_items'          => __( 'All Workout', 'ra-workout' ),
      'view_item'          => __( 'View Workout', 'ra-workout' ),
      'add_new_item'       => __( 'Add New Workout', 'ra-workout' ),
      'add_new'            => __( 'Add New', 'Workout', 'ra-workout' ),
      'edit_item'          => __( 'Edit Workout', 'ra-workout' ),
      'update_item'        => __( 'Update Workout', 'ra-workout' ),
      'search_items'       => __( 'Search Workout', 'ra-workout' ),
      'new_item'           => __( 'New Workout', 'ra-workout' ),
      'not_found'          => __( 'No Workout found.', 'ra-workout' ),
      'not_found_in_trash' => __( 'No Workout found in Trash.', 'ra-workout' ),
    ),

    'supports'      => array(
      'title',
      'author',
      'page-attributes'
    ),
    'taxonomies' => array(),
    // 'taxonomies' => array('ra-workout-category', 'ra-workout-tag'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-format-video',
    'can_export'          => true,
    'has_archive'         => false,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'query_var'           => true,
    'rewrite'             => array(
      'slug' => 'around-home/workouts',
      'with_front' => false
    )
  ) );
}

// Register Custom Taxonomy
function taxonomy_workout_category() {
  $labels = array(
    'name'                       => _x( 'Workout Category', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Workout Category', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Workout Category', 'text_domain' ),
    'all_items'                  => __( 'All Workout Categories', 'text_domain' ),
    'parent_item'                => __( 'Parent Workout Category', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Workout Category:', 'text_domain' ),
    'new_item_name'              => __( 'New Workout Category', 'text_domain' ),
    'add_new_item'               => __( 'Add New Workout Category', 'text_domain' ),
    'edit_item'                  => __( 'Edit Workout Category', 'text_domain' ),
    'update_item'                => __( 'Update Workout Category', 'text_domain' ),
    'view_item'                  => __( 'View Workout Category', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate Workout Category with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove Workout Category', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Workout Category', 'text_domain' ),
    'search_items'               => __( 'Search Workout Category', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'items_list'                 => __( 'Workout Category list', 'text_domain' ),
    'items_list_navigation'      => __( 'Workout Category list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => !false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite'                    => array(
	  'slug' => 'around-home/workouts-cat',
      'with_front' => false
    )
  );
  register_taxonomy( 'workout_category', array( 'ra-workout' ), $args );
}
add_action( 'init', 'taxonomy_workout_category', 0 );
?>