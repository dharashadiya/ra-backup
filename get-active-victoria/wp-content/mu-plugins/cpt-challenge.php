<?php
// Register Custom Post Type
add_action( 'init', 'ra_challenges_post_type', 0 );
function ra_challenges_post_type() {
	$args = array(
        'label'                 => __( 'Challenges', 'ra-challenges' ),
		'description'           => __( 'Challenges posts', 'ra-challenges' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array(
			'with_front' => false,
			'slug' => 'around-home/challenges'
		),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 5,
		'menu_icon'             => 'dashicons-format-video',
        'supports'           => array( 'title', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes' ),
    );
	register_post_type( 'ra-challenges', $args );
}

add_action( 'init', 'challenges_register_custom_taxonomy', 0 );
function challenges_register_custom_taxonomy() {
    $args = array(
        'label'        => __( 'Challenges Category', 'challenges_category' ),
        'public'       => true,
        'rewrite'      => true,
        'hierarchical' => true
    );
    register_taxonomy( 'challenges_category', 'ra-challenges', $args );
}

?>