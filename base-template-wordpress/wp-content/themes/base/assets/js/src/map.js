(function($) {
	window.console ||
		(window.console = {
			log: function() {}
		});
	window.googleMap || (window.googleMap = {});
	$.extend(googleMap, {
		init_vars: function() {
			// Global variables -> use as googleMap.VariableName in other files
			this.apiKey = "API KEY GOES HERE";
			this.latitude = -37.8248196;
			this.longitude = 144.9897351;
			this.markerWidth = 128;
			this.markerHeight = 128;
			return (this.contentString =
				'<div id="c-map__popup">' +
				"</div>" +
				"<h3>Company Name</h3>" +
				'<div id="c-map__poppup_content">' +
				"Address Goes Here" +
				"</div>");
		},
		onready: function() {
			this.init_vars();
			if ($("#map").length > 0) {
				return this.loadMap();
			}
		},
		loadMap: function() {
			var isDraggable;
			//styles = styling map
			// styles = [
			//   {
			//     featureType: "all",
			//     stylers: [
			//       { saturation: -92 },
			//       { hue: "#2A00FF" },
			//       { visibility: "simplified" },
			//       { gamma: 2 }
			//     ]
			//   }
			// ]

			// isDraggable = if Modernizr.touchevents then false else true
			isDraggable = true;
			return $.getScript(
				"https://maps.googleapis.com/maps/api/js?key=" + this.apiKey,
				function() {
					var mapCanvas, mapOptions;
					mapCanvas = document.getElementById("map");
					mapOptions = {
						center: new google.maps.LatLng(
							this.latitude,
							this.longitude
						),
						zoom: 15,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						scrollwheel: false,
						mapTypeControl: false,
						streetViewControl: false,
						styles: styles
					};
					this.map = new google.maps.Map(mapCanvas, mapOptions);
					googleMap.map = this.map;
					return googleMap.markers();
				}
			);
		},
		fixBounds: function(points) {
			var bounds, i, len, point;
			bounds = new google.maps.LatLngBounds();
			for (i = 0, len = points.length; i < len; i++) {
				point = points[i];
				bounds.extend(point);
			}
			return this.map.fitBounds(bounds);
		},
		markers: function() {
			var contentString, image, infowindow, lat_lng, marker;
			image = {
				url: window.site.themeurl + "/assets/img/marker.png",
				scaledSize: new google.maps.Size(
					this.markerWidth,
					this.markerHeight
				)
			};
			lat_lng = {
				lat: this.latitude,
				lng: this.longitude
			};
			contentString = this.contentString;
			infowindow = new google.maps.InfoWindow({
				content: contentString
			});
			marker = new google.maps.Marker({
				position: lat_lng,
				icon: image,
				map: googleMap.map
			});
			return marker.addListener("click", function() {
				infowindow.open(map, marker);
			});
		}
	});
	return $(function() {
		return googleMap.onready();
	});
})(jQuery);
