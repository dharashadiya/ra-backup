<?php /* Template Name: Debug Template */ ?>

<?php get_header() ?>

<div class="o-wrapper">
  
  <h1>Spinner</h1>
  <div class="o-spinner"></div>

  <h1>Modal</h1>
  <div class="o-island">
    <div class="c-btn" data-modal="demo-modal">Open Modal</div>
  </div>

  <h1>Layout/Grid</h1>

  <div class="o-layout">
    <div class="o-layout__item o-island u-1/2@tablet">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
    </div>

    <div class="o-layout__item o-island u-1/2@tablet">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
    </div>

    <div class="o-layout__item o-island u-1/2@tablet u-1/3@desktop">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
    </div>

    <div class="o-layout__item o-island u-1/2@tablet u-1/3@desktop">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
    </div>

    <div class="o-layout__item o-island u-1/2@tablet u-1/3@desktop">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
    </div>

    <div class="o-layout__item o-island u-1/2@tablet u-1/4@desktop">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
    </div>

    <div class="o-layout__item o-island u-1/2@tablet u-1/4@desktop">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
    </div>

    <div class="o-layout__item o-island u-1/2@tablet u-1/4@desktop">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
    </div>

    <div class="o-layout__item o-island u-1/2@tablet u-1/4@desktop">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
    </div>

  </div>

  <h1>Equal Heights</h1>

  <div class="o-layout o-layout--stretch">

    <div class="o-layout__item o-island u-1/2@tablet u-1/4@desktop">
      <div class="o-box u-bg-brand">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
      </div>
    </div>

    <div class="o-layout__item o-island u-1/2@tablet u-1/4@desktop">
      <div class="o-box o-box--tiny u-bg-brand">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
      </div>
    </div>

    <div class="o-layout__item o-island u-1/2@tablet u-1/4@desktop">
      <div class="o-box o-box--small u-bg-brand">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
      </div>
    </div>

    <div class="o-layout__item o-island u-1/2@tablet u-1/4@desktop">
      <div class="o-box o-box--large u-bg-brand">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea porro amet quaerat ipsa, esse harum facilis iste delectus. Ut, in. Ipsum animi, doloremque dolores ipsa asperiores aliquid nesciunt, delectus quae.
      </div>
    </div>

  </div>

  <h1>Typography</h1>

  <h1>Heading 1</h1>
  <h2>Heading 2</h2>
  <h3>Heading 3</h3>
  <h4>Heading 4</h4>
  <h5>Heading 5</h5>
  <h6>Heading 6</h6>

</div>

<!-- Modal -->
<div class="c-modal" data-modal-id="demo-modal">
  <div class="c-modal__container">
    <div class="c-modal__content">
      <h1>Modal Heading</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut sunt aperiam ea voluptatem reiciendis nihil vitae voluptates cumque ad numquam illo, minus non repellat facere repellendus tenetur aspernatur nisi, iure.</p>
    </div>
    <button class="c-modal__close" data-modal="demo-modal"></button>
  </div>
</div>
<?php get_footer() ?>
