</div><!-- .c-content -->
<footer class="c-footer">
	<?php //echo do_shortcode('[instagram photos=6 handle=rockagencysocial]') ?>
	<div class="c-footer__map">
		<!-- <div id="map" class="c-map"></div> -->
	</div>
	<div class="o-wrapper">
		<?php if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
			<div class="c-footer__widgets">
				<?php dynamic_sidebar( 'footer-sidebar' ); ?>
			</div>
		<?php endif; ?>
		<div class="c-footer__copy">
			<p>&copy;&nbsp;Copyright&nbsp;Base&nbsp;Template&nbsp;<?php echo date("Y"); ?>. <a href="/terms-conditions/">Terms&nbsp;&amp;&nbsp;Conditions</a>&nbsp;|&nbsp;<a href="/privacy-policy/">Privacy</a> Website&nbsp;by&nbsp;<a href="https://rockagency.com.au/">Rock&nbsp;Agency</a>.</p>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>