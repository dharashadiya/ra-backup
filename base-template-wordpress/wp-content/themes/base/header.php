<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
	<title><?php wp_title(); ?> </title>

	<?php if(is_local_or_staging()) : ?>
		<meta name="robots" content="none" />
		<meta name="robots" content="noindex" />
		<meta name="googlebot" content="none" />
		<meta name="googlebot" content="noindex" />
	<?php endif; ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">
	<?php wp_head(); ?>
	<?php include('partials/inlinescripts.min.php'); ?>
</head>

<body <?php body_class(); ?>>
<div class="c-top">
	<header class="c-header o-wrapper">
		<?php
			$logo_wrap_class = "c-logo-wrap";
			if (!is_front_page()) {
				$logo_wrap_open = '<a class="' . $logo_wrap_class . ' is-link" href="/" rel="home" title="Back to home">';
				$logo_wrap_close = '</a>';
			} else {
				$logo_wrap_open = '<span class="' . $logo_wrap_class . '">';
				$logo_wrap_close = '</span>';
			}
		?>
		<?php 
			echo $logo_wrap_open;
			echo svgicon('logo', '0 0 218 210', 'c-header__logo');
			echo $logo_wrap_close; 
		?>
		<a href="#Main" class="c-skip">Skip to main content</a>
		<nav class="c-site-nav" role="navigation">
		<?php
			wp_nav_menu( array(
				'theme_location' => 'primary-menu',
				'container' => false,
				'menu_class' => 'c-nav',
				'menu_id' => 'menu'
			));
		?>
		</nav>
	</header>
</div>

<div class="c-content">