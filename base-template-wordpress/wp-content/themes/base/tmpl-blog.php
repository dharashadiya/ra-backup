<?php
/*
 * Template Name: Blogs Page
 */
?>

<?php get_header(); ?>

<div class="c-blogs">
<main id="Main" class="c-main-content c-main-content--blogs o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="c-cms-content o-wrapper">
			<div class="c-blogs__header">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
			<div class="c-blogs__listing">
				<?php
					$items = new WP_Query();
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$items->query(array(
						'post_type' => 'post',
						'status' => 'published',
						'posts_per_page' => 6,
						'paged' => $paged
					));
					$orig_query = $wp_query;
					$wp_query = $items;
					get_template_part( 'loop', 'row' );
					pagedNav();
					$wp_query = $orig_query;
				?>
			</div>
		</div>
		<?php wp_link_pages(); ?>
	</article>
	<?php endwhile; ?>
</main>
</div>

<?php get_footer(); ?>