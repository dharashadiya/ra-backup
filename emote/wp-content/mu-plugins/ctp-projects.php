<?php
/*
Plugin Name: Projects - Custom Post Type
Description: Projects - Custom Post Type for Brenda Janschek
*/

add_action( 'init', 'register_projects_custom_post_type' );
function register_projects_custom_post_type() {
  register_post_type( 'ra-project', array(
    'label'  => __('project', 'ra-project'),
    'description' => __('Projects', 'ra-project'),
    'labels' => array(
      'name'               => _x( 'Projects', 'post type general name', 'ra-project' ),
      'singular_name'      => _x( 'Project', 'post type singular name', 'ra-project' ),
      'menu_name'          => __( 'Projects', 'admin menu', 'ra-project' ),
      'parent_item_colon'  => __( 'Parent Projects:', 'ra-project' ),
      'name_admin_bar'     => __( 'Project', 'add new on admin bar', 'ra-project' ),
      'all_items'          => __( 'All Projects', 'ra-project' ),
      'view_item'          => __( 'View project', 'ra-project' ),
      'add_new_item'       => __( 'Add New project', 'ra-project' ),
      'add_new'            => __( 'Add New', 'project', 'ra-project' ),
      'edit_item'          => __( 'Edit project', 'ra-project' ),
      'update_item'        => __( 'Update project', 'ra-project' ),
      'search_items'       => __( 'Search Projects', 'ra-project' ),
      'new_item'           => __( 'New project', 'ra-project' ),
      'not_found'          => __( 'No Projects found.', 'ra-project' ),
      'not_found_in_trash' => __( 'No Projects found in Trash.', 'ra-project' ),
    ),

    'supports'      => array(
      'title',
      'editor',
      'thumbnail',
      'comments',
      'revisions',
      'custom-fields',
      'page-attributes'
    ),
    'taxonomies' => array('ra-project-category', 'ra-project-tag'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-video-alt',
    'can_export'          => true,
    'has_archive'         => false,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'query_var'           => true,
    'rewrite'             => array(
      'slug' => 'work',
      // 'with_front' => 'true'
    )
  ) );
}

add_action( 'init', 'create_projects_taxonomies', 0 );
function create_projects_taxonomies() {
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name'              => _x( 'Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Categories' ),
    'all_items'         => __( 'All Categories' ),
    'parent_item'       => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item'         => __( 'Edit Category' ),
    'update_item'       => __( 'Update Category' ),
    'add_new_item'      => __( 'Add New Category' ),
    'new_item_name'     => __( 'New Category Name' ),
    'menu_name'         => __( 'Categories' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'project-category' ),
  );

  register_taxonomy( 'ra-project-category', array( 'ra-project' ), $args );

  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name'                       => _x( 'Tags', 'taxonomy general name' ),
    'singular_name'              => _x( 'Tag', 'taxonomy singular name' ),
    'search_items'               => __( 'Search Tags' ),
    'popular_items'              => __( 'Popular Tags' ),
    'all_items'                  => __( 'All Tags' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Tag' ),
    'update_item'                => __( 'Update Tag' ),
    'add_new_item'               => __( 'Add New Tag' ),
    'new_item_name'              => __( 'New Tag Name' ),
    'separate_items_with_commas' => __( 'Separate tags with commas' ),
    'add_or_remove_items'        => __( 'Add or remove tags' ),
    'choose_from_most_used'      => __( 'Choose from the most used tags' ),
    'not_found'                  => __( 'No tags found.' ),
    'menu_name'                  => __( 'Tags' ),
  );

  $args = array(
    'hierarchical'          => false,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'project-tag' ),
  );

  register_taxonomy( 'ra-project-tag', 'ra-project', $args );
}



?>