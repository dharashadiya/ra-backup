<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<div class="c-home">
	<?php $top_banner = get_field('top_banner');
	 $top_mob_banner = get_field('top_banner_mobile');
	 $phone_mockup = get_field('top_phone_mockup_gif');
	 if ($phone_mockup) {
		 $phone_mockup = str_replace('-scaled', '', $phone_mockup);
	 }
	 ?>
		<div class="c-home-top">
			<?php if ($top_banner) : ?>
				<?php if ($phone_mockup) : ?>
					<div id="home-mobile-mockup" class="c-home-top__mockup">
						<div data-depth=".10">
							<div class="js-rand-parallax">
								<!-- <img class="" src="<?php echo STYLESHEET_URL; ?>/assets/img/phone-mockup.png" alt="Emote Digital"> -->
								<!-- <img class="gif-mockup" src="<?php echo STYLESHEET_URL; ?>/assets/img/homepage-gif.gif" alt="Emote Digital"> -->
								<img class="lazyload" data-src="<?php echo $phone_mockup; ?>" alt="Emote Digital">
							</div>
							<!-- <video class="js-rand-parallax" muted="" loop="" preload="metadata" playsinline autoplay src="<?php //echo $top_banner; ?>"></video> -->
						</div>
					</div>
				<?php endif; ?>
			<?php endif; ?>
				<img src="<?php echo STYLESHEET_URL; ?>/assets/img/banner-logo.png" alt="" class="c-home-top__logo lazyload">
				<img src="<?php echo STYLESHEET_URL; ?>/assets/img/banner-logo-mobile-r.png" alt="" class="c-home-top__logo-mobile lazyload">
			<div class="o-wrapper c-home-top__scroll">
				<a href="javascript:void(0);" class="page-scroll js-page-scroll"><?php echo svgicon('page-scroll', '0 0 20 175'); ?></a>
			</div>
		</div>
	<div class="o-wrapper">
		<?php $intro = get_field('intro_section');  if ($intro) : ?>
			<div class="c-home-intro lax"  data-lax-translate-y="vh 0, -elh -200" data-lax-anchor="self">
				<h1 class="page-title"><?php echo $intro['heading']; ?></h1>
				<div class="intro-para">
					<?php echo $intro['intro_para']; ?>
				</div>
				<a href="<?php echo $intro['button']['url']; ?>" class="o-btn"><?php echo $intro['button']['title']; ?><?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
			</div>
		<?php endif; ?>
	</div>
	<!-- Changed Services section here -->
	<div class="c-services lax"  data-lax-translate-y="vh 0, -elh -100" data-lax-anchor="self">
		<?php $services = get_field('services_section'); ?>
		<?php if ($services) : ?>
				<div class="c-services-list">
					<?php foreach ($services as $item) : ?>
					<div class="c-services-list__card <?php echo ($item['is_ecommerce'] ? 'ecom-section' : ''); ?>">
						<div class="c-services-list__wrap o-wrapper">
							<h2 class="lax" data-lax-translate-y="vh 0, -elh -50"> <?php echo $item['heading']; ?></h2>
							<div class="c-services-list__each">
								<div class="c-services-list__img lazyload" data-src="<?php echo $item['image'] ?>"></div>
								<div class="c-services-list__info lax" data-lax-translate-y="vh 0, -elh -100" data-lax-anchor="self">
									<h3><?php echo $item['copy']; ?></h3>
									<div class="c-services-list__btn">
									<?php if($item['link']['url']) : ?>
										<a href="<?php echo $item['link']['url']; ?>" class="o-btn"><?php echo $item['link']['title']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
									<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
		<?php endif; ?>
	</div>
	<div class="o-wrapper o-wrapper--nopadmobile">
		<?php $marketing_section = get_field('marketing_section'); ?>
		<?php if ($marketing_section) : ?>
			<div class="c-section c-home-marketing">
				<div class="o-layout">
					<div class="o-layout__item u-1/2@tablet">
						<?php if ($marketing_section['image']) : ?>
								<div class="weight-images">
									<img class="lazyload main-img" data-src="<?php echo $marketing_section['image']; ?>" alt="">
									<?php if ($marketing_section['show_animation']) : ?>
										<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/bike_green.png" alt="" class="weight weight-1 lazyload">
										<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/bike_red.png" alt="" class="weight weight-2 lazyload">
										<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/bike_yellow.png" alt="" class="weight weight-3 lazyload">
									<?php endif; ?>
								</div>
						<?php endif; ?>
						<div class="weight-mob-img">
							<img class="lazyload" data-src="<?php echo $marketing_section['mobile_image']; ?>" alt="">
						</div>
					</div>
					<div class="o-layout__item u-1/2@tablet">
						<div class="cms-content lax" data-lax-translate-y="vh 0, -elh -150" data-lax-anchor="self">
							<h2><?php echo $marketing_section['heading'] ?></h2>
							<span><?php echo $marketing_section['label'] ?></span>
							<p><?php echo $marketing_section['copy'] ?></p>
							<a href="<?php echo $marketing_section['button']['url']; ?>" class="js-hover-img-btn o-btn o-btn--alt"><?php echo $marketing_section['button']['title']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php $recipe_section = get_field('recipe_section'); ?>
		<?php if ($recipe_section) : ?>
			<div class="c-section c-home-recipe">
				<div class="o-layout">
					<div class="o-layout__item u-1/2@tablet">
						<div class="cms-content lax" data-lax-translate-y="vh 0, -elh -200" data-lax-anchor="self">
							<h2><?php echo $recipe_section['heading'] ?></h2>
							<span><?php echo $recipe_section['label'] ?></span>
							<p><?php echo $recipe_section['copy'] ?></p>
							<a href="<?php echo $recipe_section['button']['url']; ?>" class="o-btn"><?php echo $recipe_section['button']['title']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
						</div>
					</div>
					<?php $video = $recipe_section['video']; ?>
					<?php if ($video ) : ?>
						<div class="o-layout__item u-1/2@tablet animate">
							<div class="c-home-recipe__video-wrap">
								<video class="c-home-recipe__video lazyload" preload="metadata" autoplay="" loop="" muted="" playsinline="">
									<source src="<?php echo $video['url']; ?>" type="video/mp4">
								</video>
							</div>
						</div>
						<?php else: ?>
						<div class="o-layout__item u-1/2@tablet">
							<?php if ($recipe_section['image']) : ?>
								<img class="lazyload" data-src="<?php echo $recipe_section['image']; ?>" alt="">
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
		<?php $success_section = get_field('success_section'); ?>
		<?php if ($success_section) : ?>
			<div class="c-section c-home-success">
				<div class="o-layout">
					<div class="o-layout__item u-1/2@tablet">
						<div class="c-home-success__images">
							<?php if ($success_section['image']) : ?>
								<div class="skater-images">
									<img class="lazyload mobile" data-src="<?php echo $success_section['image']; ?>" alt="" >
									<?php if ($success_section['show_animation']) : ?>
										<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/skater-12.png" alt="" class="skater skater-1 lazyload">
										<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/skater-12.png" alt="" class="skater skater-2 lazyload">
										<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/skater-12.png" alt="" class="skater skater-3 lazyload">
										<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/skater-12.png" alt="" class="skater skater-4 lazyload">
									<?php endif; ?>
								</div>
								<div class="skater-mob-img">
									<img data-src="<?php echo $success_section['mobile_image']; ?>" class="lazyload" alt="">
								</div>
							<?php endif; ?>
						</div>
					</div>
					<div class="o-layout__item u-1/2@tablet">
						<div class="cms-content lax" data-lax-translate-y="vh 0, -elh -200" data-lax-anchor="self">
							<h2><?php echo $success_section['heading'] ?></h2>
							<span><?php echo $success_section['label'] ?></span>
							<p><?php echo $success_section['copy'] ?></p>
							<a href="<?php echo $success_section['button']['url']; ?>" class="js-hover-btn o-btn o-btn--alt"><?php echo $success_section['button']['title']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
						</div>
					</div>
				</div>
			</div>

		<?php endif; ?>
	</div>
	<?php $about_section = get_field('about_section'); ?>
		<?php if ($about_section) : ?>
			<div class="o-wrapper o-wrapper--1200">
				<div class="c-home-about">
					<div class="o-layout c-home-about__top">
						<div class="o-layout__item u-2/3@tablet lax" data-lax-translate-y="vh 0, -elh -150" data-lax-anchor="self">
							<div class="c-home-about__top-content">
								<div class="intro-para">
									<?php echo $about_section['copy'] ?>
								</div>

								<div class="intro-para-extended">
									<?php echo $about_section['copy_extended'] ?>
								</div>
								<a href="#" class="o-btn__extend down-arrow" data-extend="intro-para-extended" >More<?php echo svgicon('btn-arrow', '0 0 32 32'); ?></a>
							</div>
						</div>
						<div class="o-layout__item u-1/3@tablet">
							<?php if ($about_section['right_side_image']) : ?>
								<img data-src="<?php echo $about_section['right_side_image']; ?>" alt="" class="lazyload">
							<?php endif; ?>
						</div>
					</div>
					<div class="c-home-about__slider swiper-container">
						<div class="swiper-wrapper" >
							<!-- Slides -->
							<?php foreach ($about_section['image_slider'] as $item) : ?>
								<div class="swiper-slide">
									<div class="bgimg lazyload" data-src="<?php echo $item['image']; ?>"></div>
								</div>
							<?php endforeach; ?>
						</div>
						<div class="swiper-controls">
							<div class="swiper-button-prev"><?php svgicon('left-arrow', '0 0 18 18') ?></div>
							<div class="swiper-button-next"><?php svgicon('right-arrow', '0 0 18 18') ?></div>
						</div>
					</div>
					<div class="o-layout c-home-about__list lax" data-lax-translate-y="vh 0, -elh -40" data-lax-anchor="self">
					<?php foreach ($about_section['items'] as $item) : ?>
						<div class="o-layout__item u-1/4@tablet u-1/2@mobileLandscape">
							<?php if ($item['icon']) : ?>
								<img src="<?php echo $item['icon']; ?>" alt="">
							<?php endif; ?>
							<span class="c-home-about__metric"><span class="js-countup"><?php echo $item['metrics'] ?></span><span>+</span></span>
							<span class=""><?php echo $item['results'] ?></span>

						</div>
					<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php $clients_section = get_field('clients_section'); ?>
	<?php if ($clients_section) : ?>
		<div class="c-home-clients">
			<div class="o-wrapper">
				<div class="c-home-clients__top lax" data-lax-translate-y="vh 0, -elh -60" data-lax-anchor="self">
					<div class="intro-para">
						<?php echo $clients_section['copy'] ?>
					</div>
				</div>
				<div class="c-home-clients__list lax" data-lax-translate-y="vh 0, -elh -80" data-lax-anchor="self">
					<?php foreach ($clients_section['clients_logos'] as $logo) : ?>
						<div class="c-home-clients__each">
							<?php if ($logo['image']) : ?>
								<img class="lazyload" data-src="<?php echo $logo['image']; ?>">
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php $quotes = get_field('quote_section') ?>
	<?php if ($quotes) : ?>
		<div class="c-quotes">
			<div class="o-wrapper">
				<div class="c-quotes__list c-slider">
					<?php foreach ($quotes as $item) : ?>
						<div class="c-quotes__list-each c-slide">
							<div class="c-quotes__list-eachinner">
								<h2><?php echo $item['copy']; ?></h2>
								<p class="c-quotes__name"><span><?php echo $item['name']; ?></span></p>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="cycle-next c-quotes__list-next"><?php svgicon('right-arrow', '0 0 18 18') ?></div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div class="c-home-contact page-contact-bottom">
		<div class="o-wrapper lax" data-lax-translate-y="vh 0, -elh -50" data-lax-anchor="self">
			<p>For beautiful results, speak to&nbsp;us</p>
			<a href="/contact" class="o-btn">Contact Us <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
		</div>
	</div>
</div>
<?php endwhile; ?>

<?php get_footer(); ?>