/* global site */
// [1] Import functions here
import {
	isMobile,
	openLinkNewTab,
	browserClass,
	superfishMenu,
	slider,
	toogleNav,
	swiperMobile,
	homeScrollTop,
	fadeInUp,
	metricsCounter,
	aboutTeamBio,
	skaterAnimation,
	laxLibrary,
	parallax,
	fixedNav,
	historyTabs,
	filterTabs,
	blogPaginationLoad,
	loadmoreProjects,
	accordianDiv,
	lazyLoad,
	cloudinaryImageCDN,
	extendDiv,
	scrollContain
} from "./template-functions";

import { bgAnimate } from "./bg-animation";
(function($) {
	window.site || (window.site = {});
	$.extend(
		site,
		{
			initVars: () => {
				// Global variables -> use as site.VariableName in other files
				site.body = $("body");
				site.root = $("html");
				site.header = $("#Top");
				site.main = $("#Main");
				site.side = $("#Secondary");
				site.siteurl = $("#Siteurl").attr("content");
				site.themeurl = $("#Themeurl").attr("content");
				return (site.ajaxurl = $("#Ajaxurl").attr("content"));
			},
			// [3] Call functions within either of these
			onReady: () => {
				svg4everybody();
				site.initVars();
				site.browserClass();
				site.toogleNav();
				site.swiperMobile();
				site.homeScrollTop();
				site.aboutTeamBio();
				site.historyTabs();
				site.filterTabs();
				site.loadmoreProjects();
				site.accordianDiv();
				site.skaterAnimation();
				site.fadeInUp();
				site.parallax();
				site.fixedNav();
				site.laxLibrary();
				site.lazyLoad();
				site.slider();
				site.blogPaginationLoad();
				site.metricsCounter();
				site.superfishMenu();
				site.extendDiv();
				site.scrollContain();
				// site.bgAnimate();
				// site.cloudinaryImageCDN()
				return site.isMobile();
			},
			onLoad: () => {
				// return site.slider()
			},
			onResize: () => {
				site.swiperMobile();
			}
		},
		{
			// [2] Register functions here
			isMobile: isMobile,
			openLinkNewTab: openLinkNewTab,
			browserClass: browserClass,
			superfishMenu: superfishMenu,
			slider: slider,
			toogleNav: toogleNav,
			swiperMobile: swiperMobile,
			homeScrollTop: homeScrollTop,
			fadeInUp: fadeInUp,
			metricsCounter: metricsCounter,
			historyTabs: historyTabs,
			laxLibrary: laxLibrary,
			filterTabs: filterTabs,
			blogPaginationLoad: blogPaginationLoad,
			loadmoreProjects: loadmoreProjects,
			accordianDiv: accordianDiv,
			skaterAnimation: skaterAnimation,
			parallax: parallax,
			fixedNav: fixedNav,
			aboutTeamBio: aboutTeamBio,
			lazyLoad: lazyLoad,
			cloudinaryImageCDN: cloudinaryImageCDN,
			bgAnimate: bgAnimate,
			extendDiv: extendDiv,
			scrollContain: scrollContain
		}
	);
	$(() => {
		return site.onReady();
	});
	$(window).on("resize", () => {
		return site.onResize();
	});
	return $(window).on("load", () => {
		return site.onLoad();
	});
})(jQuery);
