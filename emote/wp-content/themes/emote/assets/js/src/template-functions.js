export function isMobile() {
	var result;
	result = false;
	if (
		/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
			navigator.userAgent
		)
	) {
		result = true;
	}
	return result;
}
export function browserClass() {
	if (/Chrome/i.test(navigator.userAgent)) {
		$('html').addClass('is-chrome')
	} else if (/Safari/i.test(navigator.userAgent)) {
		$('html').addClass('is-safari')
	} else if (/Firefox/i.test(navigator.userAgent)) {
		$('html').addClass('is-firefox')
	}
}
export function getUrlParameter(name) {
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	var results = regex.exec(location.search);
	return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
// Open External Link in new tab
export function openLinkNewTab() {
	var base;
	base = window.location.hostname;
	return $("a").each(() => {
		let url = $(this).attr("href");
		if (
			url.indexOf("http") !== -1 &&
			(url.indexOf("javascript:void(0)") < 0 && url.indexOf(base) < 0)
		) {
			$(this).attr("target", "_blank");
			return $(this).attr("rel", "noopener");
		}
	});
}

export function cloudinaryImageCDN() {
	const localhost = site.siteurl.includes(".test") ? true : false;
	imagesLoaded("body", {
		background: true
	}, () => {
		Array.from($("[data-img], [data-bg], [data-src]")).forEach(image => {
			if (localhost) {
				if (image.classList.contains("lazy")) {
					return
				} else if (image.tagName === "IMG") {
					image.src = image.dataset.src;
				} else {
					image.style.backgroundImage = `url(${image.dataset.src})`;
				}
			} else {
				const clientWidth = image.clientWidth;
				const pixelRatio = window.devicePixelRatio || 1.0;
				const imageParams = "w_" + 100 * Math.round((clientWidth * pixelRatio) / 100) + ",c_fill,g_auto,f_auto";
				const baseUrl = "https://res.cloudinary.com/rock-agency/image/fetch";
				let url;
				if (image.classList.contains("lazy")) {
					image.dataset.src = baseUrl + "/" + imageParams + "/" + image.dataset.src;
				} else if (image.tagName === "IMG") {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.src = url;
				} else {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.style.backgroundImage = `url(${url})`;
				}
			}
		});
	});
}

export function superfishMenu() {
	if ($(window).width() > 799) {
		$(".c-nav").superfish({
			animation: {
				height: "show"
			},
			delay: 250,
			speed: 100
		});
	}
}

export function slider() {
	if ($('.c-quotes__list-eachinner').length > 0) {
		let h = 0
		$('.c-quotes__list-each').each(function (index, elem) {
			if ($(this).height() > h) {
				h = $(this).height()
			}
		})
		$('.c-quotes__list-each').height(h).addClass('center-content')
	}
	$(".c-slider").cycle({
		log: false,
		// swipe: true
		slides: ".c-slide",
		// "auto-height": "calc",
		height: "auto",
		timeout: 5000,
		speed: 400,
		easing: 'linear',
	});

	let autoloop = $("body.home").length ? true : false;

	var swiperhome = new Swiper('.c-home-about__slider', {
		slidesPerView: 1,
		spaceBetween: 20,
		slidesPerView: 'auto',
		timeout: 5000,
		speed: 400,
		easing: 'linear',
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		autoplay: autoloop ? {delay: 5000} : false
	});
}
export function swiperMobile() {
	if ($('.swiper-mob-only').length > 0) {
		if ($(window).width() < 770) {
			let swiper3 = new Swiper('.swiper-mob-only', {
				slidesPerView: 1,
				autoplay: 2000,
				spaceBetween: 20,
				keyboard: {
					enabled: true,
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				}
			});
			setTimeout(() => {
				$('.swiper-mob-only').each(function (elem, index) {
					$(this).find('.swiper-slide').height('auto')
					let h = 0
					$(this).find('.swiper-slide').each(function (index, elem) {
						if ($(this).height() > h) {
							h = $(this).height()
						}
					})
					$(this).find('.swiper-slide').height(h)
				})
			}, 100);
		} else {
			$('.swiper-mob-only .swiper-wrapper').removeClass('swiper-wrapper')
			$('.swiper-mob-only .swiper-slide').removeClass('swiper-slide')
			$('.swiper-mob-only .swiper-slide').height('auto')
		}
	}
}
// Fade in animation
export function fadeInUp() {
	$(`.input-wrap,
	   .animate,
	   .animate-elems p,
	   .animate-elems span,
	   .animate-elems h1,
	   .animate-elems h2,
	   .animate-elems h3,
	   .animate-elems h4,
	   .animate-elems h5,
	   .animate-elems h6,
	   .animate-elems li`
	).addClass('pre-animate').viewportChecker({
		classToAdd: "animated",
		classToRemove: 'pre-animate',
		offset: 0
	});
	$(`.animate-rotate`).addClass('pre-animate').viewportChecker({
		classToAdd: "imgrotate",
		classToRemove: 'pre-animate',
		offset: 0
	});
	$(`.animate-rtl`).addClass('pre-animate').viewportChecker({
		classToAdd: "imgrtl",
		classToRemove: 'pre-animate',
		offset: 0
	});
}

export function laxLibrary() {
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE");
	var edge = ua.indexOf("Edge");
	if (msie > 0 || edge > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./) || isMobile() || $(window).width() < 770) return
	lax.setup()
	const updateLax = () => {
		lax.update(window.scrollY)
		window.requestAnimationFrame(updateLax)
	}
	window.requestAnimationFrame(updateLax)
}

// LazyLoad

export function lazyLoad() {
	return $('.lazyload').Lazy({
		threshold: 400,
		effect: 'fadeIn',
	})
}


// mobile nav

export function toogleNav() {
	$('.c-header__toggle').click(function () {
		$('.c-top').addClass('nav-open');
		$('html').addClass('scroll-lock');
	})
	$('.c-header__close').click(function () {
		$('.c-top').removeClass('nav-open');
		$('html').removeClass('scroll-lock');
	})
	$('.c-site-nav .menu-item-has-children').each(function () {
		$(this).append('<span class="c-nav__subnav-toggle"></span>')
	});
}

// scroll top

export function homeScrollTop() {
	if ($('.c-home-intro').length > 0) {
		$(document).on('click', '.js-top-scroll', function () {
			var target = $('.c-home-intro').offset().top - 50 ;
			$("html, body").animate({
				scrollTop: target
			}, 600);
		});
	}

	$(document).on('click', '.js-home-logotop-scroll', function () {
		$("html, body").animate({
			scrollTop: 0
		}, 600);
	});


	var winHeight = $(window).height();
	$(document).on('click', '.js-page-scroll', function () {
		$("html, body").animate({
			scrollTop: (winHeight / 2) + 200
		}, 600);
	});
}

// counter

export function metricsCounter() {
	$('.js-countup').viewportChecker({
		offset: 0,
		callbackFunction: function (elem, action) {
			$(elem).countUp({
				delay: 30,
				separator: ' ',
				time: 2000
			});
		},
	});
}

// About team bio

export function aboutTeamBio() {
	$('.js-about-bio').click( function () {
		$(this).siblings(".c-about-team__bio").fadeIn(500);
	});
	$('.js-about-bio__close').click( function () {
		$(this).parent(".c-about-team__bio").fadeOut(500);
	});
}

// about history tabs

export function historyTabs() {
	$(document).on('click', '.js-year', function () {
		// $('html, body').animate({ scrollTop: $(window).scrollTop() + 2 }, 100)
		var target = $(this).attr('data-target');
		$('.js-year').removeClass('active');
		$(this).addClass('active');
		$('.js-event').removeClass('year-active');

		//let sectionTimeOuts={};
		$(".js-event." + target).each(function (index, elem) {
			// let iterate = (index + 1)
			// clearTimeout(sectionTimeOuts['_section' + index])
			// sectionTimeOuts['_section' + index] = setTimeout(() => {
				$(this).addClass('year-active')
			// }, 180 * ((iterate-1) * 3) );
		})



		// $(".js-event." + target).addClass('year-active');
		// $(`.js-event.${target}`).show();
		$('.js-event.year-active').first().trigger('click')
	})
	$(document).on('click', '.js-event', function () {
		// $('html, body').animate({ scrollTop: $(window).scrollTop() + 2 }, 100)
		$('.js-event-img').removeClass('active');
		$(this).children(".js-event-img").addClass('active');
		$('.js-event').removeClass('active');
		$(this).addClass('active');
	})
}

//filter tabs

export function filterTabs() {
	$(document).on('click', '.js-tabs', function () {
		var target = $(this).attr('data-target');
		var container = $(this).attr('data-container');
		let tabList = $('.js-tab-list')
		let tabs = $('.js-tabs')
		let tabListTarget = $(`.js-tab-list.${target}`)
		if (container) {
			tabList = $(`${container} .js-tab-list`)
			tabListTarget = $(`${container} .js-tab-list.${target}`)
			tabs = $(`${container} .js-tabs`)
		}

		tabList.hide();
		tabs.removeClass('active');
		$(this).addClass('active');
		tabListTarget.show();
		if (target === 'all') {
			tabList.show();
		}
	})
	if ($('.c-service-process__container .js-tab-list').length > 0) {
		let minH = 0;
		$('.c-service-process__container .js-tab-list').each(function (index, elem) {
			let h = $(this).outerHeight()
			if (h > minH) {
				minH = h
			}
		})
		$('.c-service-process__container .js-tab-list').css('min-height', `${minH}px`)
	}

}

//skater-animation

export function skaterAnimation() {


	// Home Motor section
	if($(".c-home-marketing").length){
		popupAnimation(".c-home-marketing",".weight");
	}
	// Home skate section
	if($(".c-home-success").length){
		popupAnimation(".c-home-success",".skater");
	}
	// work motor section
	if($(".c-section-success").length){
		popupAnimation(".c-section-success",".weight");
	}

	// $('.js-hover-btn').mouseenter(function () {
	// 	// $('.skater-images').addClass('skater-animation')
	// 	$('img.skater').each(function (index, elem) {
	// 		let iterate = (index + 1)
	// 		clearTimeout(imgTimeOuts['img' + index])
	// 		imgTimeOuts['img' + index] = setTimeout(() => {
	// 			$(this).animate({
	// 				opacity: 1
	// 			}, 10)
	// 		}, 100 * iterate);
	// 	})


	// }).mouseleave(function () {
	// 	// $('.skater-images').removeClass('skater-animation')
	// 	let totlaImgs = $('img.skater').length
	// 	$('img.skater').each(function (index, elem) {
	// 		let iterate = totlaImgs - (index + 1)
	// 		clearTimeout(imgTimeOuts['img' + index])
	// 		imgTimeOuts['img' + index] = setTimeout(() => {
	// 			$(this).animate({
	// 				opacity: 0
	// 			}, 10)
	// 		}, 100 * iterate);
	// 	})
	// });
	// $('.js-hover-img-btn').mouseenter(function () {
	// 	// $('.weight-images').addClass('weight-animation')
	// 	$('img.weight').each(function (index, elem) {
	// 		let iterate = (index + 1)
	// 		clearTimeout(imgTimeOuts['img' + index])
	// 		imgTimeOuts['img' + index] = setTimeout(() => {
	// 			$(this).animate({
	// 				opacity: 1
	// 			}, 10)
	// 		}, 100 * iterate);
	// 	})
	// }).mouseleave(function () {
	// 	// $('.weight-images').removeClass('weight-animation')
	// 	let totlaImgs = $('img.weight').length
	// 	$('img.weight').each(function (index, elem) {
	// 		let iterate = totlaImgs - (index + 1)
	// 		clearTimeout(imgTimeOuts['img' + index])
	// 		imgTimeOuts['img' + index] = setTimeout(() => {
	// 			$(this).animate({
	// 				opacity: 0
	// 			}, 10)
	// 		}, 100 * iterate);
	// 	})
	// });
}


export function popupAnimation(section,image_name,appearOn=350){
	let imgTimeOuts={};
	$(window).scroll(function () {
		let section_position = $(section).offset().top - $(window).scrollTop();

		if(section_position < appearOn ){

			$('img'+image_name).each(function (index, elem) {
				let iterate = (index + 1)
				clearTimeout(imgTimeOuts[image_name+'_img' + index])
				imgTimeOuts[image_name+'_img' + index] = setTimeout(() => {
					$(this).animate({
						opacity: 1
					}, 10)
				}, 100 * iterate);
			})
		}else{
			let totlaImgs = $('img'+image_name).length
			$('img'+image_name).each(function (index, elem) {
				let iterate = totlaImgs - (index + 1)
				clearTimeout(imgTimeOuts[image_name+'_img' + index])
				imgTimeOuts[image_name+'_img' + index] = setTimeout(() => {
					$(this).animate({
						opacity: 0
					}, 10)
				}, 100 * iterate);
			})
		}
	})

}

export function parallax() {
	// var elem = document.getElementById('home-mobile-mockup'), pos = 0
	if ($('#home-mobile-mockup').length) {
		setInterval(function () {
			let rand1 = Math.random() * 10;
			let rand2 = Math.random() * 20;
			$('#home-mobile-mockup .js-rand-parallax').css({ 'transform': `matrix(1, 0, 0, 1, -${rand1}, ${rand2})` })
		}, 2000);

		let sceneSpeedBoat = document.getElementById('home-mobile-mockup');
		let parallaxInstance = new Parallax(sceneSpeedBoat, {});
		parallaxInstance.friction(0.2, 0.8);
	}
}

// load more projects

export function loadmoreProjects() {

	var show = 12;
	var activeCat = 'all';

	$('.js-project-tabs').on('click', function () {
		activeCat = $(this).attr('data-target');
		show = 12
		$('.js-project-tabs').removeClass('active');
		$(this).addClass('active');
		showProjects()
	});

	var total = $('.js-load-projects').attr('data-total');
	$(document).on('click', '.js-load-projects', function () {
		show = show + 12
		showProjects()
		$('.js-project-list').removeClass('animatein');
	});

	function showProjects() {
		$('.js-project-list').removeClass('animatein');
		$('.js-project-list').hide();
		$('.js-project-list.' + activeCat).each(function (index, elem) {
			if (index < show) {
				$(this).show();
				$(this).addClass('animatein');
			}
		});
		// $('html, body').animate({ scrollTop: $(window).scrollTop() + 1 }, 100)
		lazyLoad();
		total = $('.js-project-list.' + activeCat).length
		$('.js-load-projects').hide()
		if (show < total) {
			$('.js-load-projects').show()
		}
	}
}

// fixed nav
export function fixedNav() {

	if ($(window).scrollTop() >= 100)
		$('.c-top').addClass('fixed');

	$(window).scroll(function () {
		scroll = $(window).scrollTop();
		if (scroll >= 100)
			$('.c-top').addClass('fixed');
		else
			$('.c-top').removeClass('fixed');
	});
}

export function extendDiv(){

	$(".o-btn__extend").on("click",function(e){
		e.preventDefault();
		var div= $(this).data("extend");
		$("."+div).slideToggle();
		$(this).toggleClass("active");
	})
}

export function accordianDiv(){

	$(".js-accordian").on("click",function(e){
		$(this).siblings('.js-accordian-content').toggle();
	})
}

export function blogPaginationLoad() {
	let page_no = getUrlParameter('page_no')
	if (page_no && $(`.c-blogs__listing`).length > 0) {
		$("html, body").animate({
			scrollTop: $('.c-blogs__listing').position().top
		}, 300);
	}
}

export function scrollContain() {
	$(document).on('click', '.js-scroll-right', function () {
		let target = $(this).attr('data-target')
		let targetElm = $(target)
		$('.js-scroll-left').show();
		if (targetElm.length > 0) {
			let left = targetElm.scrollLeft()
			targetElm.animate({
				scrollLeft: left + 200
			}, 200)
		}
	})
	$(document).on('click', '.js-scroll-left', function () {
		let target = $(this).attr('data-target')
		let targetElm = $(target)
		if (targetElm.length > 0) {
			let left = targetElm.scrollLeft()
			targetElm.animate({
				scrollLeft: left - 200
			}, 500)
		}
	})
}