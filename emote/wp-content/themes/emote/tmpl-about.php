<?php
/*
 * Template Name: About Page
 */
?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<div class="c-about">
	<div class="c-about-top o-wrapper">
		<a href="javascript:void(0);" class="page-scroll js-page-scroll"><?php svgicon('page-scroll', '0 0 20 175') ?></a>
		<div class="c-about-top__content">
			<h1 class="page-title"><?php the_title() ?></h1>
			<div class="cms-content intro-para">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="c-about-top__image lazyload" data-src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>"></div>
	</div>
	<div class="c-about-team">
		<div class="o-wrapper o-wrapper">
			<?php
				$team_section = get_field('team_section');
				$teams = $team_section['teams'];

				$types = array();
				$prep_teams = array();
				foreach ($teams as $key => $value) {
					$type = stripString($value['team_type']);
					$types[$type] = $value['team_type'];
					$prep_teams[$type][] = $value;
				}
			?>
			<h2 class="page-title-alt animate"><?php echo $team_section['heading']; ?></h2>
			<div class="c-about-team__types">
				<?php $i = 0; foreach ($types as $key => $item) :
					if ($i === 0) {
						$active_type = $key;
					}
					?>
					<a href="javascript: void(0);" class="js-tabs <?php echo ($i == 0 ? 'active' : ''); ?>" data-target="<?php echo $key ; ?>"><?php echo $item; ?></a>
				<?php $i++; endforeach; ?>
			</div>
			<?php foreach ($prep_teams as $type => $items) : ?>
				<div class="c-about-team__list js-tab-list <?php echo $type; ?>" style="<?php echo ($type === $active_type ? '' : 'display:none;'); ?>">
					<?php foreach ( $items as $team) : ?>
						<div class="c-about-team__each">
							<div class="c-about-team__img lazyload" data-src="<?php  echo $team['headshot'];  ?>"></div>
							<div class="c-about-team__info">
								<span class="name"><?php echo $team['first_name']; ?></span>
								<span class="name"><?php echo $team['last_name']; ?></span>
								<span><?php echo $team['title']; ?></span>
								<?php if ($team['bio']) : ?>
									<a class="c-about-team__btn js-about-bio o-btn" href="javascript:void(0)">Bio <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
								<?php endif; ?>
								<div class="c-about-team__bio">
									<?php //echo wp_trim_words( $team['bio'], 35, '...' );  ?>
									<?php echo $team['bio'];  ?>
									<a class="c-about-team__btn js-about-bio__close" href="javascript:void(0)">Close <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endforeach; ?>
		</div>
	</div>

	<?php $values_sec = get_field('values_section');
	if ($values_sec['heading'] && $values_sec['values']) : ?>
		<div class="c-about-values">
			<div class="o-wrapper">
				<div class="o-layout o-module">
					<div class="o-layout__item o-module__item u-1/2@tablet">
						<h2 class="page-title-alt animate"><?php echo $values_sec['heading']; ?></h2>
					</div>
					<?php foreach ($values_sec['values'] as $item) : ?>
						<div class="o-layout__item o-module__item u-1/2@tablet">
							<div class="c-about-values__section">
								<span class="c-about-values__section-label"><?php echo $item['label']; ?></span>
								<h2><?php echo $item['heading']; ?></h2>
								<?php echo $item['contents']; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>


	<?php $history = get_field('history_section'); ?>
	<?php if ($history) : ?>
		<div class="c-about-history">
			<h3 class="animate">Our History</h3>
			<div class="c-about-history__content">
				<a href="javascript:void(0);" class="js-scroll-right c-about-history__next" data-target=".c-about-history__years"><?php svgicon('right-arrow', '0 0 18 18') ?></a>
				<a href="javascript:void(0);" class="js-scroll-left c-about-history__prev" data-target=".c-about-history__years"><?php svgicon('right-arrow', '0 0 18 18') ?></a>
				<div class="c-about-history__years">
				<?php foreach ($history as $key => $item) : ?>
						<a href="javascript:void(0);" class="js-year <?php echo ($key === 0 ? 'active' : ''); ?>" data-target="<?php echo $item['year']; ?>"><h3><?php echo $item['year']; ?> <?php svgicon('arrow-down', '0 0 22 34'); ?></h3></a>
				<?php endforeach; ?>
				</div>
				<div class="c-about-history__events">
					<?php foreach ($history as $key => $item) : ?>
						<?php foreach ($item['events'] as $eKey => $event) : ?>
							<div class="js-event c-about-history__event <?php echo $item['year']; ?> <?php echo ($key == 0 ? 'year-active' : '') . ' ' .  ($eKey == 0 ? 'active' : ''); ?>">
								<div class="c-about-history__event-info">
									<div class="c-about-history__event-content">
										<h3><?php echo $event['title']; ?></h3>
										<p><?php echo $event['description']; ?></p>
									</div>
									<?php //svgicon('l-arrow', '0 0 79 19'); ?>
								</div>
								<div class="js-event-img c-about-history__event-img <?php echo ($eKey == 0 ? 'active' : ''); ?>">
									<?php if ($event['image']) : ?>
										<div class="bgimg lazyload" data-src="<?php echo $event['image']; ?>" ></div>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					<?php endforeach; ?>
				</div>

			</div>
		</div>
	<?php endif; ?>
	<?php $images = get_field('images_section'); ?>
	<?php if ($images && ($images['image1'] || $images['image2'] || $images['image3'])) : ?>
		<div class="c-about-images c-images-layout animate">
			<div class="o-layout o-module">
				<div class="o-layout__item o-module__item u-2/4@tablet">
					<div class="bgimg bgimg--3 bgimg--land lazyload" data-src="<?php echo $images['image2'] ?>"></div>
				</div>
				<div class="o-layout__item o-module__item u-1/4@tablet u-1/2">
					<div class="bgimg bgimg--1 lazyload" data-src="<?php echo $images['image1'] ?>"></div>
				</div>
				<div class="o-layout__item o-module__item u-1/4@tablet u-1/2">
					<div class="bgimg bgimg--2 lazyload" data-src="<?php echo $images['image3'] ?>"></div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div class="c-about-career">
		<div class="o-wrapper">
			<?php $career = get_field('career_section') ?>
			<h2 class="page-title-alt animate"><?php echo $career['heading'] ?></h2>
			<?php if ($career['testimonials']) : ?>
				<div class="c-about-career__testimonial c-quotes">
					<div class="c-quotes__list c-slider">
						<?php foreach ($career['testimonials'] as $item) : ?>
							<div class="c-quotes__list-each c-slide">
								<div class="c-quotes__list-eachinner">
									<h2><?php echo $item['copy']; ?></h2>
									<p class="c-quotes__name"><span><?php echo $item['name']; ?></span></p>
								</div>
							</div>
						<?php endforeach; ?>
						<div class="cycle-next c-quotes__list-next"><?php svgicon('right-arrow', '0 0 18 18') ?></div>
					</div>
				</div>
			<?php endif; ?>
			<?php $middle_section = $career['middle_section']; ?>
			<?php if ($middle_section) : ?>
				<div class="c-about-middle-section">
					<div class="c-about-middle-section__content">
						<h3 class="page-title-alt"><?php echo $middle_section['title']; ?></h3>
						<p><?php echo $middle_section['copy']; ?></p>
					</div>
					<div class="c-about-middle-section__slider">
						<img src="<?php echo $middle_section['image']; ?>" alt="">
					</div>
				</div>
			<?php endif; ?>
			<?php $positions = $career['open_positions']; ?>
			<?php if ($positions) : ?>
			<div class="c-about-positions lax" data-lax-translate-y="vh 0, -elh -150" data-lax-anchor="self">
				<h2 class="animate">Open Positions</h2>
				<div class="c-about-positions__list swiper-container swiper-mob-only o-layout">
					<div class="swiper-wrapper">
						<?php foreach ($positions as $item) : ?>
							<?php
								$job_link = $item['job_link']['url'];
								$target = $item['job_link']['target'];
							?>
							<a href="<?php echo $job_link; ?>" target="_blank" target="<?php echo $target ; ?>" class="c-about-positions__list-item swiper-slide o-layout__item u-1/3@tablet">
								<div class="bgimg lazyload" data-src="<?php echo $item['image'] ?>">
									<h2><?php echo $item['title']; ?></h2>
								</div>
							</a>
						<?php endforeach; ?>
					</div>
					<div class="swiper-controls">
						<div class="swiper-button-prev"><?php svgicon('left-arrow', '0 0 18 18') ?></div>
						<div class="swiper-button-next"><?php svgicon('right-arrow', '0 0 18 18') ?></div>
					</div>
					<!-- <div class="o-layout__item u-1/3@tablet">
						<a href="#" class="bgimg c-about-positions__seeall">
							<h2>See all open positions at Emote.</h2>
						</a>
					</div> -->
				</div>
			</div>
			<?php endif; ?>

		</div>
	</div>
	<?php $bottom_copy = get_field('bottom_copy') ?>
	<?php if ($bottom_copy) : ?>
		<div class="c-about-bottom page-contact-bottom">
			<div class="o-wrapper">
				<p><?php echo $bottom_copy; ?></p>
				<a href="/contact" class="o-btn">Contact us <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>