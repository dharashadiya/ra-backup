<?php
/*
 * Template Name: Work Page
 */
?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<div class="c-work">
	<div class="o-wrapper">
		<div class="c-work-top">
			<h1 class="page-title"><?php the_title() ?></h1>
			<div class="cms-content intro-para">
				<?php the_content(); ?>
			</div>
			<a href="javascript:void(0);" class="page-scroll js-page-scroll"><?php svgicon('page-scroll', '0 0 20 175') ?></a>
		</div>
		<div class="c-work-middle">
			<?php $success_section = get_field('success_section'); ?>
			<?php if ($success_section) : ?>
				<div class="c-section c-section-success">
					<div class="o-layout">
						<div class="o-layout__item u-1/2@tablet">
							<?php $is_video = $success_section['imagevideo'];  ?>
							<?php if ($is_video) : ?>
								<?php if ($success_section['video_file']) : ?>
									<video class="c-video" preload="metadata" autoplay="" loop="" muted="" playsinline="">
										<source src="<?php echo $success_section['video_file']; ?>" type="video/mp4">
									</video>
								<?php endif; ?>
							<?php else : ?>
								<?php if ($success_section['image']) : ?>
									<div class="weight-images">
										<img class="lazyload mobile" data-src="<?php echo $success_section['image']; ?>" alt="">
											<?php if ($success_section['show_animation']) : ?>
												<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/bike_green.png" alt="" class="weight weight-1 lazyload">
												<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/bike_red.png" alt="" class="weight weight-2 lazyload">
												<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/bike_yellow.png" alt="" class="weight weight-3 lazyload">
											<?php endif; ?>
									</div>
								<?php endif; ?>
								<div class="weight-mob-img">
									<img data-src="<?php echo $success_section['mobile_image']; ?>" alt="" class="lazyload">
								</div>
							<?php endif; ?>
						</div>
						<div class="o-layout__item u-1/2@tablet">
							<div class="cms-content lax" data-lax-translate-y="vh 0, -elh -150" data-lax-anchor="self">
								<h2><?php echo $success_section['heading'] ?></h2>
								<span><?php echo $success_section['label'] ?></span>
								<p><?php echo $success_section['copy'] ?></p>
								<a href="<?php echo $success_section['button']['url']; ?>" class="js-hover-img-btn o-btn o-btn--alt"><?php echo $success_section['button']['title']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<?php $recipe_section = get_field('recipe_section'); ?>
			<?php $marketing_section = get_field('marketing_section'); ?>
			<div class="c-section">
				<div class="o-layout o-module">
					<?php if ($recipe_section && $recipe_section['image'] && $recipe_section['heading']) : ?>
						<div class="o-layout__item o-module__item u-1/2@tablet">
							<div class="o-module__content c-section-recipe">
								<?php $is_video = $recipe_section['imagevideo'];  ?>
								<?php if ($is_video) : ?>
									<?php if ($recipe_section['video_file']) : ?>
										<video class="c-video" preload="metadata" autoplay="" loop="" muted="" playsinline="">
											<source src="<?php echo $recipe_section['video_file']; ?>" type="video/mp4">
										</video>
									<?php endif; ?>
								<?php else : ?>
									<?php if ($recipe_section['image']) : ?>
										<img class="lazyload" data-src="<?php echo $recipe_section['image']; ?>" alt="">
									<?php endif; ?>
								<?php endif; ?>
								<div class="cms-content lax" data-lax-translate-y="vh 0, -elh -100" data-lax-anchor="self">
									<h2><?php echo $recipe_section['heading'] ?></h2>
									<span><?php echo $recipe_section['label'] ?></span>
									<p><?php echo $recipe_section['copy'] ?></p>
									<a href="<?php echo $recipe_section['button']['url']; ?>" class="o-btn"><?php echo $recipe_section['button']['title']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($marketing_section && $marketing_section['image'] && $marketing_section['heading']) : ?>
						<div class="o-layout__item o-module__item u-1/2@tablet animate">
							<div class="o-module__content c-section-marketing">
							<?php $is_video = $marketing_section['imagevideo'];  ?>
								<?php if ($is_video) : ?>
									<?php if ($marketing_section['video_file']) : ?>
										<video class="c-video" preload="metadata" autoplay="" loop="" muted="" playsinline="">
											<source src="<?php echo $marketing_section['video_file']; ?>" type="video/mp4">
										</video>
									<?php endif; ?>
								<?php else : ?>
									<?php if ($marketing_section['image']) : ?>
										<img class="lazyload" data-src="<?php echo $marketing_section['image']; ?>" alt="">
									<?php endif; ?>
								<?php endif; ?>

								<div class="cms-content lax" data-lax-translate-y="vh 0, -elh -100" data-lax-anchor="self">
									<h2><?php echo $marketing_section['heading'] ?></h2>
									<span><?php echo $marketing_section['label'] ?></span>
									<p><?php echo $marketing_section['copy'] ?></p>
									<?php if ($marketing_section['button']) : ?>
										<a href="<?php echo $marketing_section['button']['url']; ?>" class="o-btn"><?php echo $marketing_section['button']['title']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="c-work-listing">
		<div class="o-wrapper o-wrapper--1200">
				<?php
				$args = array(
					'post_type' => 'ra-project',
					'status' => 'published',
					'posts_per_page' => -1,
					'orderby' => 'date',
					'order' => 'DESC',
					'fields' => 'ids'
				);
				$posts = get_posts( $args );
				$total_post_count = count($posts);
				$categories = get_terms('ra-project-category');
			?>
			<div class="c-work__categories">
				<?php
				if ( $categories) :?>
					<a href="javascript: void(0);" class="js-project-tabs active" data-target="all">All</a>
				<?php foreach ( $categories as $term ) : ?>
						<a href="javascript: void(0);" class="js-project-tabs" data-target="<?php echo stripString($term->name) ; ?>"><?php echo $term->name; ?></a>
					<?php endforeach ?>
				<?php endif;?>
			</div>
			<div class="c-work__list o-layout">
				<?php foreach ($posts as $key => $item) :
					$img = get_the_post_thumbnail_url($item);
					$cate = get_the_terms($item , 'ra-project-category');
					$cate_name = $cate[0]->name ;
					$show_list = 12;
					$show = true;
					if ($key >= $show_list) {
						$show = false;
					}?>
					<div class="o-layout__item u-1/4@laptopMed u-1/3@tablet u-1/2@mobileLandscape all js-project-list <?php echo stripString($cate_name); ?>" style="display:<?php echo ($show ? 'inline-block': 'none'); ?>">
						<a href="<?php echo get_permalink( $item ); ?>" class="c-work__list-each">
							<div class="c-work__list-inner" style="background-image: url(<?php echo $img ?>)">
							</div>
							<div class="c-work__list-info">
								<h3><?php echo get_the_title($item); ?></h3>
								<span><?php echo $cate_name; ?></span>
							</div>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
			<?php if ($total_post_count > 12) : ?>
				<div class="c-work__loadmore">
					<a href="javascript:void(0)" class="js-load-projects">Load more <?php echo svgicon('s-arrow-down', '0 0 19 19'); ?>	</a>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>