</div><!-- .c-content -->
<footer class="c-footer">
	<div class="o-wrapper">
		<div class="c-footer-top">
			<div class="c-footer-top__left">
				<div class="c-cols">
				<?php if (is_front_page()): ?>
						<a href="javascript:void(0);" class="js-home-logotop-scroll"><?php echo svgicon('logo', '0 0 114 18', 'c-footer__logo'); ?></a>
					<?php else : ?>
						<a href="/" rel="home" title="Back to home"><?php echo svgicon('logo', '0 0 114 18', 'c-footer__logo'); ?></a>
					<?php endif; ?>

				</div>
				<div class="c-cols c-footer__links">
					<?php $col1_links = get_field('column_1_links', 'options'); ?>
					<?php if ($col1_links) : ?>
						<?php foreach ($col1_links as $item) : ?>
							<a href="<?php echo $item['link']['url']; ?>"><?php echo $item['link']['title']; ?></a>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<div class="c-cols c-footer__links">
					<?php $col2_links = get_field('column_2_links', 'options'); ?>
					<?php if ($col2_links) : ?>
						<?php foreach ($col2_links as $item) : ?>
							<a href="<?php echo $item['link']['url']; ?>"><?php echo $item['link']['title']; ?></a>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<div class="c-cols c-footer__links">
					<?php $col3_links = get_field('column_3_links', 'options'); ?>
					<?php if ($col3_links) : ?>
						<?php foreach ($col3_links as $item) : ?>
							<a href="<?php echo $item['link']['url']; ?>" class="c-footer__links-dm"><?php echo $item['link']['title']; ?></a>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="c-footer-top__right c-footer__contact">
				<?php $contact = get_field('contact_details', 'options'); ?>
				<?php if ($contact) : ?>
				<div class="c-footer__contact-details">
					<a href="tel:<?php echo $contact['phone']; ?>"><?php echo $contact['phone']; ?></a>
					<a class="c-footer__contact-email" href="mailto:<?php echo $contact['email']; ?>"><?php echo $contact['email']; ?></a>
					<a href="<?php echo $contact['address_link']; ?>"><?php echo $contact['address']; ?></a>
					<?php echo do_shortcode('[social-links]'); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="c-footer__copy">
			<!-- <p><a href="/terms-conditions/">Terms&nbsp;&amp;&nbsp;Conditions</a>&nbsp;|&nbsp;<a href="/privacy-policy/">Privacy.</a></p> -->
			<p>Part of the <a href="http://bigpicturegroup.com.au/" class="partner-link">Big Picture Group</a></p>
			<p>&copy;Emote Digital <?php echo date("Y"); ?>. All Rights Reserved. </p>
			<p><strong>Servicing businesses australia wide.</strong></p>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
