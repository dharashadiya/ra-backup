<?php
    $blog = get_query_var('var');
    $img = get_the_post_thumbnail_url($blog);
    $category = get_the_category($blog, 'category');
 ?>
<div class="o-layout__item u-1/4@tablet animate">
	<div class="c-blogs-list__inner">
		<a href="<?php echo get_the_permalink($blog); ?>" class="c-blogs-list__container" title="<?php echo get_the_title($blog) ?>">
			<div class="c-blogs-list__img lazyload" data-src="<?php echo $img ?>"></div>
			<div class="c-blogs-list__cate">
				<span><?php if ($category[0]->name !== 'Uncategorised') : ?>
					<?php echo $category[0]->name; ?>
				<?php endif; ?></span>
				<!-- <span class="new-tag">New</span>  -->
			</div>
			<div class="c-blogs-list__content">
				<h4 class="c-blogs-list__heading"><?php echo wp_trim_words(get_the_title($blog),12); ?></h4>
				<!-- <div class="c-blogs-list__description"><?php echo wp_trim_words(get_the_content(null, false, $blog), 14)  ?></div> -->
			</div>
		</a>
	</div>
</div>