<?php
/*
 * Template Name: Blogs Page
 */
?>

<?php get_header(); ?>

<div class="c-blogs">
<main id="Main" class="c-main-content c-main-content--blogs o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="o-wrapper">
			<div class="c-blogs__header">
				<h1 class="page-title"><?php the_title(); ?></h1>
				<div class="cms-content intro-para">
					<?php the_content(); ?>
				</div>
				<a href="javascript:void(0);" class="page-scroll js-page-scroll"><?php svgicon('page-scroll', '0 0 20 175') ?></a>
			</div>
			<?php
				$page_no = esc_attr( sanitize_text_field($_GET['page_no']) );
				$curr_cat = esc_attr( sanitize_text_field($_GET['cat']) );
				$posts_per_page = 12;
				if (!$page_no || !is_numeric($page_no)) {
					$page_no = 1;
				}
				$categories = get_categories( array(
                    'orderby' => 'name',
                    'order'   => 'ASC'
                ));

				$args = array(
					'post_type' => 'post',
					'status' => 'published',
					'orderby' => 'date',
					'order' => 'DESC',
					'fields' => 'ids'
				);

				// latest post
				$args['posts_per_page'] = 1;
				$latest_post = get_posts($args);
				$latest_post = $latest_post[0];
				$category = get_the_category($latest_post, 'category');

				// for the rest of the posts
				$args['posts_per_page'] = -1;
				$args['category_name'] = $curr_cat;
				$args['exclude'] = array($latest_post);
				$total_posts = count(get_posts($args));
				$pages = ceil($total_posts / $posts_per_page);

				// get paged post
				$args['posts_per_page'] = $posts_per_page;
				$args['paged'] = $page_no;
				$posts = get_posts( $args );
			?>
			<?php if ($latest_post) : ?>
				<div class="c-blogs-top">
					<div class="c-blogs-top__img lazyload" data-src="<?php echo get_the_post_thumbnail_url($latest_post);; ?>">
						<!-- <img class="lazyload" data-src="<?php echo get_the_post_thumbnail_url($latest_post);; ?>" alt=""> -->
					</div>
					<div class="c-blogs-top__content lax" data-lax-translate-y="vh 0, -elh -100" data-lax-anchor="self">
						<?php if ($category[0]->name !== 'Uncategorised') : ?>
							<div class="c-blogs-top__content-tag"><span class="new-tag">New</span> <span><?php echo $category[0]->name; ?></span></div>
						<?php endif; ?>
						<h2><?php echo wp_trim_words(get_the_title($latest_post),12); ?></h2>
						<a href="<?php echo get_the_permalink($latest_post); ?>" class="o-btn o-btn--alt"> Read more <?php svgicon('btn-arrow', '0 0 13 13'); ?></a>
					</div>
				</div>
			<?php endif; ?>
			<div class="c-blogs-middle-section">
				<div class="o-layout">
					<?php $top_stories = get_field('top_stories'); ?>
					<div class="o-layout__item u-1/2@tabletWide animate">
						<div class="c-blogs__top-stories animate">
							<span class="c-blogs__top-stories--title">Top stories </span>
							<div class="c-blogs__top-stories--list">
								<?php foreach ($top_stories as $item) :
								$id = $item['blog_post'];
								$img = get_the_post_thumbnail_url($id);
								$category = get_the_category($id, 'category');?>
								<a href="<?php echo get_the_permalink($id); ?>" class="c-blogs-list__inner" title="<?php echo get_the_title($id) ?>">
									<div class="c-blogs-list__img lazyload" data-src="<?php echo $img ?>"></div>
									<div class="c-blogs-list__content">
										<h4 class="c-blogs-list__heading"><?php echo wp_trim_words(get_the_title($id),12); ?></h4>
									</div>
								</a>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
					<?php $featured_category_post = get_field('featured_category_post'); ?>
					<div class="o-layout__item u-1/2@tabletWide">
						<div class="c-blogs__featured-posts animate">
								<?php foreach ($featured_category_post as $blog) :
									$id = $blog['post'];
									$img = get_the_post_thumbnail_url($id);
									$category = get_the_category($id, 'category');?>
								<a href="<?php echo get_the_permalink($id); ?>" class="c-blogs-list__inner" title="<?php echo get_the_title($id) ?>">
									<div class="c-blogs-list__content">
										<div class="c-blogs-list__cate">
											<span><?php echo $category[0]->name; ?></span>
										</div>
										<h4 class="c-blogs-list__heading"><?php echo wp_trim_words(get_the_title($id),14); ?></h4>
									</div>
									<div class="c-blogs-list__img lazyload" data-src="<?php echo $img ?>"></div>
								</a>
						<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="o-wrapper o-wrapper--1100" id="bloglist">
			<div class="c-blogs__listing">
				<h2 class="page-title-alt">The meat of it</h2>
				<?php if (!$is_only_uncategorised) : ?>
					<div class="c-blogs__categories animate">
						<a href="<?php the_permalink(); ?>#bloglist" alt="all filter" class="c-blogs__categories-item <?php echo (!$curr_cat ? "active" : "") ?>">All</a>
						<?php foreach ($categories as $cat) : if ($cat->slug !== 'uncategorised') : ?>
							<a href="<?php the_permalink(); ?>?cat=<?php echo $cat->slug ?>#bloglist" alt="<?php echo $cat->name; ?>" class="c-blogs__categories-item <?php echo ($curr_cat && $curr_cat === $cat->slug ? "active" : "") ?>"><?php  echo $cat->name; ?></a>
						<?php endif; endforeach; ?>
					</div>
				<?php endif; ?>

				<div class="o-layout c-blogs-list">
					<?php
						foreach ($posts as $item) {
							set_query_var( 'var', $item);
							get_template_part( 'partials/loop-row' );
						}
					?>
				</div>
				<?php if ($pages > 1) : ?>
					<div class="c-blogs__pagination">
						<?php if ($page_no && $page_no > 1) : ?>
							<a href="<?php echo get_the_permalink() . '?page_no=' . ($page_no - 1); ?>" class="c-blogs__pagination-arrows prev"><?php echo svgicon('left-arrow', '0 0 18 18') ?></a>
						<?php endif; ?>
						<div class="c-blogs__pagination-pages">
							<?php
								//Ref: https://stackoverflow.com/questions/6354303/paging-like-stackoverflows
								$cntAround = 1;
								for ($i = 1; $i <= $pages; $i++) {
									$url = get_the_permalink() . '?page_no=' . $i;
									if ($curr_cat) {
										$url = get_the_permalink() . '?cat=' . $curr_cat . '&page_no=' . $i;
									}

									$isGap = false;
									if ($cntAround >= 1 && $i > 1 && $i < $pages && abs($i - $page_no) > $cntAround) {
										$isGap = true;
										$i = ($i < $page_no ? $page_no - $cntAround : $pages) - 1;
									}

									if ($isGap) {
										$lnk = '<span>...</span>';
									} elseif ($i != $page_no && !$isGap) {
										$lnk = '<a href="' . $url . '">' . $i . '</a>';
									} elseif ($i == $page_no && !$isGap) {
										$lnk = '<a class="is-active" href="' . $url . '">' . $i . '</a>';
									}
									echo $lnk;
								}
							?>
						</div>
						<?php if ($page_no && $page_no < $pages) : ?>
							<a href="<?php echo get_the_permalink() . '?page_no=' . ($page_no + 1) ; ?>" class="c-blogs__pagination-arrows next"><?php echo svgicon('right-arrow', '0 0 18 18') ?></a>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</article>
	<?php endwhile; ?>
</main>
</div>

<?php get_footer(); ?>