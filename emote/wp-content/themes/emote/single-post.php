<!-- single Blog post -->

<?php get_header(); ?>

<div class="c-blog">
	<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="c-blog__sidelinks o-wrapper animate">
				<div class="c-blog__sidelinks-back">
					<a href="/digital-agency-blog" class="c-blog__back">back to blog</a>
				</div>
				<div class="c-blog__sidelinks-sharelinks">
					<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_the_permalink(); ?>" target="_blank"><?php echo svgicon('s-instagram', '0 0 18 18'); ?></a>
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>" target="_blank"><?php echo svgicon('s-facebook', '0 0 18 18'); ?></a>
					<a href="https://twitter.com/home?status=<?php echo get_the_permalink(); ?>" target="_blank"><?php echo svgicon('s-twitter', '0 0 18 18'); ?></a>
					<span class="share-span">Share</span>
				</div>
			</div>
			<div class="o-wrapper o-wrapper--950 c-blog__top animate">
				<h1><?php the_title(); ?></h1>
				<?php $img = get_the_post_thumbnail_url(); ?>
				<?php if ($img) : ?>
					<div class="c-blog__banner lazyload" data-src="<?php echo $img ?>" ></div>
				<?php endif; ?>
			</div>
			<div class="o-wrapper o-wrapper--750 c-cms-content animate animate-elems lax" data-lax-translate-y="vh 0, -elh -150" data-lax-anchor="self">
				<?php the_content(); ?>
			</div>

			<div class="o-wrapper">
				<?php
				$next_post = get_previous_post();?>
				<?php  if ( is_a( $next_post , 'WP_Post' )) : ?>
					<div class="c-blog__nextpost" style="background-image: url(<?php echo get_the_post_thumbnail_url( $next_post->ID ); ?>)">
						<div class="c-blog__nextpost-info">
							<h3>Up next: <?php echo get_the_title( $next_post->ID ); ?></h3>
							<a href="<?php echo get_permalink( $next_post->ID ); ?>">Read More <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>

						</div>
					</div>
				<?php endif; ?>
			</div>
		</article>
	<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>