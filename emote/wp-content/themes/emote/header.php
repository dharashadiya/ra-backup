<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
	<title><?php wp_title(); ?> </title>

	<?php if(is_local_or_staging()) : ?>
		<meta name="robots" content="none" />
		<meta name="robots" content="noindex" />
		<meta name="googlebot" content="none" />
		<meta name="googlebot" content="noindex" />
	<?php endif; ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="thumbnail" content="<?php echo ASSETS; ?>/img/emote-logo.jpg">
	<?php
    	$preview = get_the_post_thumbnail_url(get_the_ID(), 'full');
		if (!$preview) {
      		$preview = ASSETS . '/img/emote-logo.jpg';
    	}
   	?>
	<?php if ($preview) : ?>
		<meta property="og:image" content="<?php echo $preview ?>" />
	<?php endif; ?>
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.png">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/apple-touch-icon.png">
	<?php wp_head(); ?>
	<?php include('partials/inlinescripts.min.php'); ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MV3CSL');</script>
	<!-- End Google Tag Manager -->
	<script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-7101771-1', 'auto'); ga('send', 'pageview'); </script>

</head>

<body <?php body_class(); ?>>
<div class="c-top--spacer"></div>
<div class="c-top-headerbg" id="headerBg">
	<video class="c-video" preload="metadata" autoplay="" loop="" muted="" playsinline="">
		<source src="<?php echo STYLESHEET_URL; ?>/assets/img/header-top.mp4" type="video/mp4">
		</video>
</div>
<div class="c-top">
	<header class="c-header o-wrapper">
		<?php
			$logo_wrap_class = "c-logo-wrap";
			if (!is_front_page()) {
				$logo_wrap_open = '<a class="' . $logo_wrap_class . ' is-link" href="/" rel="home" title="Back to home">';
				$logo_wrap_close = '</a>';
			} else {
				$logo_wrap_open = '<span class="js-home-logotop-scroll ' . $logo_wrap_class . '">';
				$logo_wrap_close = '</span>';
			}
			// $logo_wrap_class = "c-logo-wrap";
			// $logo_wrap_open = '<a class="' . $logo_wrap_class . ' is-link" href="/" rel="home" title="Back to home">';
			// $logo_wrap_close = '</a>';
		?>
		<?php
			echo $logo_wrap_open;
			echo svgicon('logo', '0 0 114 18', 'c-header__logo');
			echo $logo_wrap_close;
		?>
		<a href="javascript:void(0);" class="c-header__toggle"><?php echo svgicon('nav', '0 0 25 16', 'c-header__toggle-icon'); ?></a>
		<a href="javascript:void(0);" class="c-header__close"><?php echo svgicon('close', '0 0 32 32', 'c-header__close-icon'); ?></a>
		<a href="#Main" class="c-skip">Skip to main content</a>
		<nav class="c-site-nav" role="navigation">
		<?php
			wp_nav_menu( array(
				'theme_location' => 'primary-left-menu',
				'container' => false,
				'menu_class' => 'c-nav c-nav-left ',
				'menu_id' => 'menu'
			));
			wp_nav_menu( array(
				'theme_location' => 'primary-right-menu',
				'container' => false,
				'menu_class' => 'c-nav c-nav-right',
				'menu_id' => 'menu'
			));
		?>
		<div class="c-header__social-mobile">
			<?php echo do_shortcode('[social-links]'); ?>
		</div>
		</nav>
	</header>
</div>

<div class="c-content">