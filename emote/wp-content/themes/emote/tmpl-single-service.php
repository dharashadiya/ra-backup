<?php
/*
 * Template Name: Single Service Page
 */
?>

<?php get_header(); ?>

<div class="c-service">
	<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php $intro = get_field('intro_content') ?>
			<div class="o-wrapper c-service__top">
				<a href="javascript:void(0);" class="page-scroll js-page-scroll"><?php svgicon('page-scroll', '0 0 20 175') ?></a>
				<h1 class="page-title"><?php the_title(); ?></h1>
				<div class="c-service__top-content">
					<h2 class=""><?php echo $intro['title']; ?></h2>
					<p><?php echo $intro['copy']; ?> </p>
					<?php $button = $intro['button']; ?>
					<?php if ($button['link']) : ?>
						<a href="<?php echo $button['link']; ?>" class="o-btn"><?php echo $button['label']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
					<?php endif; ?>
				</div>
			</div>
			<?php $img = $intro['featured_image'] ?>
			<?php if ($img) : ?>
				<div class="o-wrapper">
					<div class="c-service__banner" style="background-image: url(<?php echo $img ?>)"></div>
				</div>
			<?php endif; ?>
			<div class="o-wrapper o-wrapper--1200 animate">
				<div class="c-cms-content w-950 animate-elems <?php echo (get_field('two_column_intro_content') ? 'content-two-col' : ''); ?>">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="c-service-sections ">
				<?php $modules = get_field('modules'); ?>
				<?php foreach ($modules as $secKey => $section) :	?>
					<?php if ($section['acf_fc_layout']=== 'benefits_section') : ?>
						<div class="c-section c-service-benifits animate">
							<div class="o-wrapper o-wrapper--1200">
								<h2 class="c-service-h1"><?php echo $section['heading']; ?></h2>
								<div class="swiper-container swiper-mob-only c-service-benifits__list">
									<div class="swiper-wrapper o-layout o-layout--stretch o-layout--center">
										<!-- Slides -->
										<?php foreach ($section['benifits'] as $item) : ?>
											<div class="o-layout__item u-1/3@tabletWide u-1/2@tablet swiper-slide">
												<div class=" c-service-benifits__item">
													<div class="c-service-benifits__item-title">
														<?php if ($item['image']) : ?>
															<div class="c-service-benifits__item-img"><img src="<?php echo $item['image']; ?>" class="animate" alt=""></div>
														<?php endif; ?>
														<h2 class="c-service-benifits__heading"><?php echo $item['title']; ?></h2>
													</div>
													<div class="c-service-benifits__item-content">
														<p><?php echo $item['content']; ?></p>
													</div>
												</div>
											</div>
										<?php endforeach; ?>
									</div>
									<div class="swiper-controls">
										<div class="swiper-button-prev"><?php svgicon('left-arrow', '0 0 18 18') ?></div>
										<div class="swiper-button-next"><?php svgicon('right-arrow', '0 0 18 18') ?></div>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout']=== 'services_section') : ?>
						<div class="c-section c-service-services animate tabs-list-container-<?php echo $secKey; ?>">
							<div class="o-wrapper o-wrapper--1200">
								<h2 class="c-service-h1"><?php echo $section['heading']; ?></h2>
								<p><?php echo $section['copy'] ?></p>
								<div class="c-service-services__content">
									<div class="c-service-services__list">
										<?php foreach ($section['services'] as $key => $item) : ?>
											<a href="javascript:void(0);" class="js-tabs js-list <?php echo ($key === 0 ? 'active' : ''); ?>" data-target="<?php echo stripString($item['title']); ?>" data-container=".tabs-list-container-<?php echo $secKey; ?>"><h3><?php echo $item['title']; ?></h3></a>
										<?php endforeach; ?>
									</div>
									<div class="c-service-services__container">
										<?php foreach ($section['services'] as $key => $item) : ?>
											<div class="cms-content js-tab-list <?php echo stripString($item['title']); ?>" style="<?php echo ($key === 0 ? '' : 'display:none;'); ?>">
												<?php if ($item['image']) : ?>
													<img data-src="<?php echo $item['image']; ?>" class="lazyload" alt="">
												<?php endif; ?>
												<p><?php echo $item['content']; ?></p>
												<?php if ($item['button']['link']) : ?>
													<a href="<?php echo $item['button']['link']; ?>" class="o-btn"><?php echo $item['button']['label']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
												<?php endif; ?>
											</div>
										<?php endforeach; ?>
									</div>

								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout']=== 'features_section') : ?>
						<div class="c-section c-service-features animate">
							<div class="o-wrapper o-wrapper--1200">
								<div class="o-layout c-service-features__sections">
									<?php foreach ($section['features'] as $item) : ?>
										<div class="o-layout__item u-1/4@tablet c-service-features__img">
											<?php if ($item['image']) : ?>
												<img data-src="<?php echo $item['image']; ?>" class="lazyload animate" alt="">
											<?php endif; ?>
										</div>
										<div class="o-layout__item u-3/4@tablet">
											<div class="c-service-features__content animate">
												<h2><?php echo $item['title']; ?></h2>
												<p><?php echo $item['content']; ?></p>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout']=== 'heading_with_right_side_content') : ?>
						<div class="c-section c-service-heading-content animate">
							<div class="o-wrapper o-wrapper--1200">
								<div class="o-layout c-service-heading-content__wrap">
									<div class="o-layout__item u-1/2@tablet">
										<h2 class="c-service-h1"><?php echo $section['heading']; ?></h2>
									</div>
									<div class="o-layout__item u-1/2@tablet">
										<div class="c-service-heading-content__contents">
											<div><?php echo $section['content']; ?></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout']=== 'results_content') : ?>
						<div class="c-section c-service-results animate">
							<div class="o-wrapper o-wrapper--1200">
								<h2 class="c-service-results__heading"><?php echo $section['heading']; ?></h2>
								<?php foreach ($section['content'] as $item) : ?>
								<div class="o-layout o-module c-service-results__sections">
									<div class="o-layout__item o-module__item u-1/3@tablet c-service-results__img <?php echo $item['align_image']; ?>">
										<?php if ($item['image']) : ?>
											<img data-src="<?php echo $item['image']; ?>" class="lazyload animate" alt="">
										<?php endif; ?>
									</div>
									<div class="o-layout__item o-module__item u-2/3@tablet">
										<div class="c-service-results__content">
											<h2><?php echo $item['title']; ?></h2>
											<p><?php echo $item['copy']; ?></p>
											<?php $metrics = $item['metrics_table']; ?>
												<?php if ($metrics) : ?>
												<div class="c-metrics">
													<?php foreach ($metrics as $item) : ?>
														<div class="c-metrics__each">
															<h3><span class="js-countup"><?php echo $item['metrics_value'] ?></span><span><?php echo $item['suffix']; ?></span></h3>
															<p><?php echo $item['label'] ?></p>
														</div>
													<?php endforeach; ?>
												</div>
												<?php endif; ?>
											<?php if ($item['button']['link']) : ?>
												<a href="<?php echo $item['button']['link']; ?>" class="o-btn"><?php echo $item['button']['label']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout']=== 'two_column_content_image') : ?>
						<div class="c-section c-service-tc-content-image">
							<div class="o-wrapper o-wrapper--1200">
								<div class="o-layout">
									<div class="o-layout__item u-1/2@tabletWide u-3/5@tablet animate">
										<div class="c-service-tc-content-image__content">
											<h2><?php echo $section['heading']; ?></h2>
											<p><?php echo $section['content']; ?></p>
											<?php if ($section['button']['link']) : ?>
												<a href="<?php echo $section['button']['link']; ?>" class="o-btn"><?php echo $section['button']['label']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
											<?php endif; ?>
										</div>
									</div>
									<div class="o-layout__item u-1/2@tabletWide u-2/5@tablet">
										<?php if ($section['image']) : ?>
											<img data-src="<?php echo $section['image']; ?>" class="animate lazyload" alt="">
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout']=== 'two_column_image_content') : ?>
						<div class="c-section c-service-tc-image-content">
							<div class="o-wrapper o-wrapper--1200">
								<div class="o-layout o-order o-order--tablet animate">
									<div class="o-layout__item u-1/2@tabletWide u-3/5@tablet o-order__2@tablet">
										<div class="c-service-tc-image-content__content">
											<h2><?php echo $section['heading']; ?></h2>
											<p><?php echo $section['content']; ?></p>
											<?php if ($section['button']['link']) : ?>
												<a href="<?php echo $section['button']['link']; ?>" class="o-btn"><?php echo $section['button']['label']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
											<?php endif; ?>
										</div>
									</div>
									<div class="o-layout__item u-1/2@tabletWide u-2/5@tablet o-order__1@tablet">
										<?php if ($section['image']) : ?>
											<img data-src="<?php echo $section['image']; ?>" class="animate lazyload" alt="">
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>

					<?php if ($section['acf_fc_layout']=== 'partners_section') : ?>
						<div class="o-wrapper o-wrapper--1200">
							<div class="c-section c-service-partners2 animate">
								<div class="o-layout">
									<div class="o-layout__item u-1/3@tablet">
										<?php if ($section['image']) : ?>
											<img data-src="<?php echo $section['image']; ?>" class="animate lazyload" alt="">
										<?php endif; ?>
									</div>
									<div class="o-layout__item u-2/3@tablet">
										<div class="c-service-partners2__content">
											<h2 class="c-service-h1"><?php echo $section['heading']; ?></h2>
											<div><?php echo $section['content']; ?></div>
										</div>
										<?php if ($section['features']) : ?>
										<div class="o-layout o-layout--small c-service-partners2__features ">
											<?php foreach ($section['features'] as $features) : ?>
												<div class="o-layout__item u-1/3@tablet">
													<a class="js-accordian"><h5><?php echo $features['title']; ?>&nbsp;<?php svgicon('select-arrow', '0 0 19 11'); ?></h5></a>
													<p class="js-accordian-content c-service-partners2__features-copy"><?php echo $features['copy']; ?></p>
												</div>
											<?php endforeach; ?>
										</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout']=== 'wordpress_partners') : ?>
						<div class="o-wrapper o-wrapper--1200">
							<div class="c-section c-service-partners animate">
								<div class="o-layout o-order o-order--mob-only">
									<div class="o-layout__item u-2/3@tablet o-order__2@mob-only">
										<div class="c-service-partners__content">
											<h2 class="c-service-h1"><?php echo $section['heading']; ?></h2>
											<div><?php echo $section['content']; ?></div>
										</div>
									</div>
									<div class="o-layout__item u-1/3@tablet o-order__1@mob-only">
										<?php if ($section['image']) : ?>
											<img data-src="<?php echo $section['image']; ?>" class="animate lazyload" alt="">
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout']=== 'e-com_platforms') : ?>
						<div class="c-section c-service-ecom animate">
							<div class="o-wrapper o-wrapper--1200">
								<h2 class="c-service-h1"><?php echo $section['heading']; ?></h2>
								<div class="o-layout c-service-ecom__list ">
									<?php foreach ($section['images'] as $logo) : ?>
										<div class="o-layout__item u-1/3@tablet u-1/2">
											<img data-src="<?php echo $logo['image']; ?>" class="animate lazyload" alt="">
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout']=== 'projects_section') : ?>
						<div class="c-section c-service-projects animate">
								<?php foreach ($section['projects'] as $item) : ?>
								<div class="c-service-projects__list">
									<div class="o-wrapper o-wrapper--1200">
									<div class="o-layout o-module">
										<div class="o-layout__item o-module__item u-3/5@tablet">
											<div class="o-module__content c-service-projects__content">
												<span><?php echo $item['label'] ?></span>
												<h2><?php echo $item['title'] ?></h2>
												<p><?php echo $item['copy']; ?></p>
												<?php if ($item['button']['link']) : ?>
													<a href="<?php echo $item['button']['link']; ?>" class="o-btn"><?php echo $item['button']['label']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
												<?php endif; ?>
											</div>
										</div>
										<div class="o-layout__item o-module__item u-2/5@tablet c-service-projects__img">
											<img data-src="<?php echo $item['image']; ?>" class="animate lazyload" alt="">
										</div>
									</div>
									</div>

								</div>
								<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout']=== 'integration_logos') : ?>
						<div class="c-section c-service-logos">
							<div class="o-wrapper o-wrapper--1200 animate">
								<h2 class="c-service-h1"><?php echo $section['heading']; ?></h2>
								<div class="c-service-logos__content"><?php echo $section['copy']; ?></div>
								<div class="o-layout c-service-logos__list ">
									<?php foreach ($section['images'] as $logo) : ?>
										<div class="o-layout__item u-1/3@tablet u-1/2">
											<img data-src="<?php echo $logo['logo']; ?>" class="animate lazyload" alt="">
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>

					<?php if ($section['acf_fc_layout']=== 'two_column_content_image_alternate') : ?>
						<div class="c-section c-service-tc-content-image-alt animate <?php echo ($section['image_on_left'] ? 'img-left' : ''); ?>">
							<div class="o-wrapper o-wrapper--1200">
								<h2 class="c-service-tc-content-image-alt__heading c-service-h1"><?php echo $section['heading']; ?></h2>
								<div class="c-service-tc-content-image-alt__content animate">
									<p><?php echo $section['content']; ?></p>
								</div>
								<?php if (!$section['image']) : ?>
									<img class="c-service-tc-content-image-alt__img lazyload animate" data-src="<?php echo $section['image']; ?>" alt="">
								<?php endif; ?>
							</div>
						</div>
					<?php endif; ?>

					<?php if ($section['acf_fc_layout']=== 'two_column_content') : ?>
						<div class="c-section c-service-tc-contents animate">
							<div class="o-wrapper o-wrapper--1200">
								<div class="o-layout o-layout--small o-module">
									<?php foreach ($section['contents'] as $item) : ?>
										<div class="o-layout__item  o-module__item u-1/2@tabletWide">
											<div class="c-service-tc-contents__content">
												<h2 class="c-service-tc-contents__heading"><?php echo $item['heading']; ?></h2>
												<div class="content"><?php echo $item['content']; ?></div>
												<?php $anim_class = $item['image_animation'];
												if ($anim_class === 'Spinner') {
													$anim_class = 'animate-rotate';
												}
												 if ($anim_class === 'Right To left') {
													$anim_class = 'animate-rtl';
												}?>
												<img data-src="<?php echo $item['image']; ?>" class="lazyload <?php echo $anim_class; ?>" alt="">
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>

					<?php if ($section['acf_fc_layout']=== 'ongoing_support') : ?>
						<div class="c-section c-service-support">
							<div class="o-wrapper o-wrapper--1200 animate">
								<h2 class="c-service-h1"><?php echo $section['heading']; ?></h2>
								<p class="c-service-support__copy"><?php echo $section['copy'] ?></p>
								<div class="swiper-container swiper-mob-only c-service-support__list">
								<div class="swiper-wrapper o-layout o-module">
										<!-- Slides -->
										<?php foreach ($section['items'] as $item) : ?>
										<div class="o-layout__item o-module__item u-1/2@tablet swiper-slide">
											<div class="c-service-support__item">
												<div class="c-service-support__item-content">
													<?php if ($item['image']) : ?>
														<img src="<?php echo $item['image']; ?>" class="animate" alt="">
													<?php endif; ?>
													<h3 class="c-service-support__heading"><?php echo $item['title']; ?></h3>
													<p><?php echo $item['copy']; ?></p>
												</div>
											</div>
										</div>
									<?php endforeach; ?>
									</div>
									<div class="swiper-controls">
										<div class="swiper-button-prev"><?php svgicon('left-arrow', '0 0 18 18') ?></div>
										<div class="swiper-button-next"><?php svgicon('right-arrow', '0 0 18 18') ?></div>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>

					<?php if ($section['acf_fc_layout']=== 'testimonial_section') : ?>
						<div class="c-quotes c-section c-service-testimonials animate">
							<div class="o-wrapper o-wrapper--1200">
								<div class="c-quotes__list c-slider">
									<?php foreach ($section['testimonials'] as $item) : ?>
										<div class="c-quotes__list-each c-slide">
											<div class="c-quotes__list-eachinner">
												<h2><?php echo $item['copy']; ?></h2>
												<p class="c-quotes__name"><span><?php echo $item['name']; ?></span></p>
											</div>
										</div>
									<?php endforeach; ?>
									<div class="cycle-next c-quotes__list-next"><?php svgicon('right-arrow', '0 0 18 18') ?></div>
								</div>
							</div>
						</div>
					<?php endif; ?>

					<?php if ($section['acf_fc_layout']=== 'process_section') : ?>
						<div class="c-section c-service-process">
							<div class="o-wrapper o-wrapper--1200 animate">
								<h2><?php echo $section['heading']; ?></h2>
								<p><?php echo $section['copy'] ?></p>
								<div class="c-service-process__content">
									<a href="javascript:void(0);" class="js-scroll-right c-service-process__next" data-target=".c-service-process__list"><?php svgicon('right-arrow', '0 0 18 18') ?></a>
									<a href="javascript:void(0);" class="js-scroll-left c-service-process__prev" data-target=".c-service-process__list"><?php svgicon('right-arrow', '0 0 18 18') ?></a>
									<div class="c-service-process__list">
										<?php foreach ($section['contents'] as $key => $item) : ?>
											<a href="javascript:void(0);" class="js-tabs js-list <?php echo ($key === 0 ? 'active' : ''); ?>" data-target="<?php echo stripString($item['title']); ?>"><h3><?php echo $item['title']; ?></h3></a>
										<?php endforeach; ?>
									</div>
									<div class="c-service-process__container">
										<?php foreach ($section['contents'] as $key => $item) : ?>
											<div class="cms-content js-tab-list <?php echo stripString($item['title']); ?>" style="<?php echo ($key === 0 ? '' : 'display:none;'); ?>">
												<?php echo $item['description']; ?>
											</div>
										<?php endforeach; ?>
									</div>

								</div>
							</div>
						</div>
					<?php endif; ?>

					<?php if ($section['acf_fc_layout']=== 'additional_services') : ?>
						<div class="c-section c-service-additional-services <?php echo $section['heading']; ?>">
							<div class="o-wrapper o-wrapper--1200 animate">
								<h2 class="c-service-h1"><?php echo $section['heading']; ?></h2>
								<?php if($section['sub_heading']): ?>
									<p class=""><?php echo $section['sub_heading']; ?></p>
								<?php endif; ?>
								<div class="swiper-container swiper-mob-only c-service-additional-services__content">
									<div class="swiper-wrapper c-service-additional-services__list">
										<?php foreach ($section['services'] as $key => $item) : ?>
										<div class="c-service-additional-services__list-wrap swiper-slide">
											<a href="<?php echo $item['link']; ?>" class="c-service-additional-services__list-inner">
												<img src="<?php echo $item['image']; ?>" alt="<?php echo $item['title']; ?>" >
												<h3 style="text-transform: capitalize;"><?php echo $item['title']; ?></h3>
												<?php if($item['intro']): ?>
													<p><?php echo $item['intro']; ?></p>
												<?php else: ?>
												<?php endif; ?>
												<span>Learn more <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></span>
											</a>
										</div>
										<?php endforeach; ?>
									</div>
									<div class="swiper-controls">
										<div class="swiper-button-prev"><?php svgicon('left-arrow', '0 0 18 18') ?></div>
										<div class="swiper-button-next"><?php svgicon('right-arrow', '0 0 18 18') ?></div>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout']=== 'related_blogs') : ?>
						<div class="c-section c-service-blogs">
							<div class="o-wrapper o-wrapper--1200 animate">
								<h2 class="c-service-h1"><?php echo $section['heading']; ?></h2>
								<?php $args = array(
									'post_type' => 'post',
									'status' => 'published',
									'posts_per_page' => 4,
									'orderby' => 'date',
									'order' => 'DESC',
									'fields' => 'ids');

								if ($section['post_to_show'] === 'custom'){
									$args['category'] = $section['select_category'];
								} else {
									$section['select_category'] = null;
								}

								$bposts = get_posts( $args );
								if (count($bposts) > 0) : ?>
								<div class="o-layout c-blogs-list">
									<?php foreach ($bposts as $blog) :
									    $img = get_the_post_thumbnail_url($blog);
    									$category = get_the_category($blog, 'category');
										?>
										<div class="o-layout__item u-1/4@tablet animate">
											<div class="c-blogs-list__inner">
												<a href="<?php echo get_the_permalink($blog); ?>" class="c-blogs-list__container" title="<?php echo get_the_title($blog) ?>">
													<div class="c-blogs-list__img lazyload" data-src="<?php echo $img ?>"></div>
													<div class="c-blogs-list__cate">
														<?php foreach ($category as $item) : ?>
															<span><?php
															if ((!$section['select_category'] && $item->name !== 'Uncategorised') ||
																($section['select_category'] && $section['select_category'] === $item->term_id) ) : ?>
																<?php echo $item->name; ?>
															<?php endif; ?></span>
														<?php endforeach; ?>
														<!-- <span class="new-tag">New</span>  -->
													</div>
													<div class="c-blogs-list__content">
														<h4 class="c-blogs-list__heading"><?php echo wp_trim_words(get_the_title($blog),12); ?></h4>
														<!-- <div class="c-blogs-list__description"><?php echo wp_trim_words(get_the_content(null, false, $blog), 14)  ?></div> -->
													</div>
												</a>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
								<?php endif; ?>
							</div>
						</div>
					<?php endif; ?>

				<?php endforeach; ?>
				<!-- digital transformation page -->
					<?php
						$projects = get_field('projects');
						$digital_project = get_field('digital_project');

					if ($digital_project && count($digital_project) > 0) :
						 $project1 = $digital_project[0];
						 $project2 = $digital_project[1];
					?>
					<div class="o-wrapper o-wrapper--1200">
						<div class="c-section c-service__projects">
							<?php if ($project1) : ?>
								<div class="c-home-marketing c-service__projects-each">
									<div class="o-layout">
										<div class="o-layout__item u-1/2@tablet animate">
											<?php if ($project1['image']) : ?>
													<div class="weight-images">
														<img class="lazyload main-img" data-src="<?php echo $project1['image']; ?>" alt="">
														<?php if ($project1['show_animation']) : ?>
															<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/bike_green.png" alt="" class="weight weight-1 lazyload">
															<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/bike_red.png" alt="" class="weight weight-2 lazyload">
															<img data-src="<?php echo STYLESHEET_URL; ?>/assets/img/animation/bike_yellow.png" alt="" class="weight weight-3 lazyload">
														<?php endif; ?>
													</div>
											<?php endif; ?>
											<div class="weight-mob-img">
												<img data-src="<?php echo $project1['mobile_image']; ?>" alt="" class="lazyload">
											</div>
										</div>
										<div class="o-layout__item u-1/2@tablet">
											<div class="cms-content lax" data-lax-translate-y="vh 0, -elh -100" data-lax-anchor="self">
												<h2><?php echo $project1['heading'] ?></h2>
												<span><?php echo $project1['label'] ?></span>
												<p><?php echo $project1['copy'] ?></p>
												<a href="<?php echo $project1['button']['url']; ?>" class="js-hover-img-btn o-btn"><?php echo $project1['button']['title']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
											</div>
										</div>
									</div>
								</div>
							<?php endif; ?>
							<?php if ($project2) : ?>
									<div class="c-service__projects-each">
										<div class="o-layout">
											<div class="o-layout__item u-1/2@tablet">
												<div class="cms-content lax" data-lax-translate-y="vh 0, -elh -50" data-lax-anchor="self">
													<h2><?php echo $project2['heading'] ?></h2>
													<span><?php echo $project2['label'] ?></span>
													<p><?php echo $project2['copy'] ?></p>
													<a href="<?php echo $project2['button']['url']; ?>" class="o-btn"><?php echo $project2['button']['title']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
												</div>
											</div>
											<?php $video = $project2['image_video']; ?>
											<?php if ($video ) : ?>
												<div class="o-layout__item u-1/2@tablet animate">
													<div class="c-service__video-wrap">
														<video class="c-service__video lazyload" preload="metadata" autoplay="" loop="" muted="" playsinline="">
															<source src="<?php echo $video['url']; ?>" type="video/mp4">
														</video>
													</div>
												</div>
											<?php else : ?>
												<div class="o-layout__item u-1/2@tablet animate">
													<?php if ($project2['image']) : ?>
														<img class="lazyload" data-src="<?php echo $project2['image']; ?>" alt="">
													<?php endif; ?>
												</div>
											<?php endif; ?>
										</div>
									</div>
								<?php endif; ?>
						</div>
					</div>

					<?php endif; ?>
			</div>

			<?php $list = get_field('related_services'); ?>
			<?php if ($list) : ?>
			<div class="c-service__related animate">
				<h2 class="c-service__related-title c-service-h1">Other Services</h2>
				<div class="c-services-list">
					<?php foreach ($list as $item) : ?>
						<div class="c-services-list__card <?php echo ($item['is_ecommerce'] ? 'ecom-section' : ''); ?>">
							<div class="o-wrapper o-wrapper--1200  c-services-list__wrap">
								<h2 class="page-title"><?php echo $item['title']; ?></h2>
								<div class="c-services-list__each">
									<div class="c-services-list__img animate lazyload" data-src="<?php echo $item['image'] ?>"></div>
									<div class="c-services-list__info lax" data-lax-translate-y="vh 0, -elh -100" data-lax-anchor="self">
										<h3><?php echo $item['copy']; ?></h3>
										<div class="c-services-list__btn">
											<a href="<?php echo $item['button']['url']; ?>" class="o-btn"><?php echo $item['button']['title']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<?php endif; ?>
			<?php $contact = get_field('contact_bottom_copy') ?>
			<?php if ($contact) : ?>
				<div class="c-section page-contact-bottom">
				<div class="o-wrapper o-wrapper--1200 animate">
					<p><?php echo $contact; ?></p>
					<a href="/contact" class="o-btn o-btn--teal">Contact Us <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
				</div>
			<?php endif; ?>
			</div>
		</article>
	<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>