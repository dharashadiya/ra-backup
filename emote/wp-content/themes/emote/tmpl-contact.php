<?php
/*
 * Template Name: Contact Page
 */
?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<div class="c-contact">
	<div class="o-wrapper">
		<div class="c-contact__top">
			<a href="javascript:void(0);" class="page-scroll js-page-scroll"><?php svgicon('page-scroll', '0 0 20 175') ?></a>
			<div class="o-layout">
				<div class="o-layout__item u-1/2@tablet">
					<h1 class="page-title"><?php the_title(); ?></h1>
						<div class="cms-content intro-para">
							<?php the_content(); ?>
						</div>
				</div>
				<div class="o-layout__item u-1/2@tablet">
					<?php $contact = get_field('contact_details', 'options'); ?>
					<?php if ($contact) : ?>
						<div class="c-contact__details lax" data-lax-translate-y="vh 0, -elh -150" data-lax-anchor="self">
							<span>Email</span>
							<a class="email" href="mailto:<?php echo $contact['email']; ?>"><?php echo $contact['email']; ?></a>
							<span>Call</span>
							<a href="tel:<?php echo $contact['phone']; ?>"><?php echo $contact['phone']; ?></a>
							<span>Visit</span>
							<a href="<?php echo $contact['address_link']; ?>"><?php echo $contact['address']; ?></a>
						</div>
					<?php endif ?>
				</div>
			</div>
		</div>
	</div>
	<div class="c-contact__bottom">
		<div class="c-contact__image">
			<img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="">
		</div>
		<div class="c-contact__form lax" data-lax-translate-y="vh 0, -elh -150" data-lax-anchor="self">
			<h2>Make an enquiry</h2>
			<?php //echo do_shortcode('[contact-form-7 id="127" title="Enquiry"]') ?>
			<?php echo do_shortcode('[ninja_form id=2]') ?>


			<?php $walkthrough_section = get_field('walkthrough_section'); ?>
			<?php if ($walkthrough_section && ($walkthrough_section['walkthrough_image'] || $walkthrough_section['walkthrough_url']  )) : ?>
				<div class="c-contact__walkthrough animated">
					<a href="<?= $walkthrough_section['walkthrough_url']?>" target="_blank">
						<img src="<?=$walkthrough_section['walkthrough_image']?>">
					</a>

					<a  class=" o-btn" href="<?= $walkthrough_section['walkthrough_url']?>" target="_blank">
						walk through our office
						<svg id="btn-arrow" viewBox="0 0 13 13"><g fill="currentColor">
						<path class="st0" d="M0 0h13v2H0z"></path>
						<path class="st0" d="M11 0h2v13h-2z"></path>
						<path transform="rotate(-45.001 6.042 6.96)" class="st0" d="M-1.1 5.9h14.3V8H-1.1z"></path></g></svg>
					</a>

				</div>
			<?php endif; ?>


		</div>
	</div>
	<?php $images = get_field('images_section'); ?>
	<?php if ($images && ($images['image1'] || $images['image2'] || $images['image3'])) : ?>
		<div class="c-about-images c-images-layout animate">
			<div class="o-layout o-module">
				<div class="o-layout__item o-module__item u-2/4@tablet">
					<div class="bgimg bgimg--3 bgimg--land lazyload" data-src="<?php echo $images['image2'] ?>"></div>
				</div>
				<div class="o-layout__item o-module__item u-1/4@tablet u-1/2">
					<div class="bgimg bgimg--1 lazyload" data-src="<?php echo $images['image1'] ?>"></div>
				</div>
				<div class="o-layout__item o-module__item u-1/4@tablet u-1/2">
					<div class="bgimg bgimg--2 lazyload" data-src="<?php echo $images['image3'] ?>"></div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>


<!-- contact form 7
<div class="input-wrap half left">
<span class="label">First Name</span>[text* first-name placeholder "First Name"]
</div>
<div class="input-wrap half right">
	<span class="label">Last Name</span>[text last-name placeholder "Last Name"]
</div>
<div class="input-wrap half left">
	<span class="label">Email</span>[email* email placeholder "Email"]
</div>
<div class="input-wrap half right">
	<span class="label">Phone</span>[tel* phone placeholder "Phone"]
</div>
<div class="input-wrap half left">
	<span class="label">Company Name</span>[text* company-name placeholder "Company Name"]
</div>
<div class="input-wrap half right">
	<span class="label">Website</span>[text* website placeholder "Website"]
</div>
<div class="">
	<h2>What are you interested in? </h2>
	[checkbox interest use_label_element "New Website" "Hosting & Maintenance" "Existing Website" "Digital Transformation" "Digital Marketing"]
</div>
<div class="input-wrap">
	<h2>Describe your project, objectives, timelines and challenges.</h2>
	[textarea description]
</div>
<div class="input-wrap">
	[checkbox newsletter use_label_element "Sign up to our monthly newsletter"]
</div>
<div class="input-wrap o-btn submit">
	[submit "Send enquiry" ] <svg id="btn-arrow" viewBox="0 0 13 13"><g fill="currentColor"><path class="st0" d="M0 0h13v2H0z"></path><path class="st0" d="M11 0h2v13h-2z"></path><path transform="rotate(-45.001 6.042 6.96)" class="st0" d="M-1.1 5.9h14.3V8H-1.1z"></path></g></svg>
</div> -->




