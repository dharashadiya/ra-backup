<?php  get_header(); ?>

<div class="c-404">
	<main id="Main" class="c-main-content o-main" role="main">
		<article>
		<div class="c-cms-content o-wrapper">
			<h1 class="page-title-alt">Page Not found</h1>
			<p> The content you are looking for not found. Please use navigation menu to go through the site. </p>
			<a href="/" class="o-btn" title="Back to home">Back to home <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
		</div>
		</article>
	</main>
</div>
<!-- <?php get_sidebar(); ?> -->

<?php get_footer(); ?>