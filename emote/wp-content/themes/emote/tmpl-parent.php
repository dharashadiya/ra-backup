<?php
/*
 * Template Name: Parent Page
 */
?>

<?php get_header(); ?>

<div class="c-parent">
	<main id="Main" class="c-main-content o-main" role="main">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<article <?php post_class(); ?>>
			<div class="c-cms-content o-wrapper">
				<div class="c-parent__header">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>

				<div class="c-parent__pages">
					<?php
						$items = new WP_Query();
						$items->query(array(
							'post_type' => 'page',
							'status' => 'published',
							'posts_per_page' => -1,
							'post_parent' => $post->ID,
							'paged' => $paged
						));

						$orig_query = $wp_query;
						$wp_query = $items;
						get_template_part( 'loop','row' );
						$wp_query = $orig_query;
					?>
					<?php wp_link_pages(); ?>
				</div>
			</div>
		</article>
		<?php endwhile; ?>
	</main>
	<!-- <?php get_sidebar(); ?> -->
</div>

<?php get_footer(); ?>