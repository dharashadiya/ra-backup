<?php
/*
 * Template Name: Services Page
 */
?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<div class="c-services">
	<div class="o-wrapper">
		<div class="c-services-top">
			<h1 class="page-title"><?php the_title() ?></h1>
			<div class="cms-content intro-para">
				<?php the_content(); ?>
			</div>
			<a href="javascript:void(0);" class="page-scroll js-page-scroll"><?php svgicon('page-scroll', '0 0 20 175') ?></a>
		</div>
	</div>
	<?php $list = get_field('services'); ?>
	<?php if ($list) : ?>
		<div class="c-services-list">
			<?php foreach ($list as $item) : ?>
			<div class="c-services-list__card <?php echo ($item['is_ecommerce'] ? 'ecom-section' : ''); ?>">
				<div class="o-wrapper c-services-list__wrap">
					<h2><?php echo $item['title']; ?></h2>
					<div class="c-services-list__each">
						<div class="c-services-list__img lazyload" data-src="<?php echo $item['image'] ?>"></div>
						<div class="c-services-list__info lax" data-lax-translate-y="vh 0, -elh -100" data-lax-anchor="self">
							<h3><?php echo $item['copy']; ?></h3>
							<div class="c-services-list__btn">
								<a href="<?php echo $item['button']['url']; ?>" class="o-btn"><?php echo $item['button']['title']; ?> <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<?php $quotes = get_field('quote_section') ?>
	<?php if ($quotes) : ?>
		<div class="c-quotes">
			<div class="o-wrapper">
				<div class="c-quotes__list c-slider">
					<?php foreach ($quotes as $item) : ?>
						<div class="c-quotes__list-each c-slide">
							<div class="c-quotes__list-eachinner">
								<h2><?php echo $item['copy']; ?></h2>
								<p class="c-quotes__name"><span><?php echo $item['name']; ?></span></p>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="cycle-next c-quotes__list-next"><?php svgicon('right-arrow', '0 0 18 18') ?></div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div class="c-section page-contact-bottom">
		<div class="o-wrapper animate">
			<p>Are you ready to take your brand to the next&nbsp;level?</p>
			<a href="/contact" class="o-btn">Contact us <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
		</div>
	</div>


</div>
<?php endwhile; ?>
<?php get_footer(); ?>