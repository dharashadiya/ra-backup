<!-- single case study  -->

<?php get_header(); ?>

<div class="c-project">
	<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="o-wrapper">
				<div class="c-project-top">
					<div class="c-project-top__prevnext">
						<?php
						$prev_post = get_next_post();
						if($prev_post): ?>
							<a rel="prev" class="prev" href="<?php echo get_permalink($prev_post->ID); ?>">Previous case study</a>
						<?php endif; ?>
						<?php
						$next_post = get_previous_post();
						if($next_post): ?>
							<a rel="next" class="next" href="<?php echo get_permalink($next_post->ID); ?>">Next case study</a>
						<?php endif; ?>

					</div>
					<div class="c-project-top__content animate">
						<?php $postcat = get_the_terms($post->ID , 'ra-project-category');
						if ( ! empty( $postcat ) ) {
							$catname = esc_html( $postcat[0]->name );
						} ?>
						<p class="label">Case study &nbsp;|&nbsp;<span class="c-project-top__name"><?php the_title() ?></span>&nbsp;|&nbsp;<span class="c-project-top__cate"><?php echo $catname ?></span></p>
						<div class="intro-para">
							<?php echo the_content(); ?>
						</div>
					</div>
					<?php 
						$project_banner_image = get_field('project_banner_image'); 
						$image = $project_banner_image['image'];
						$enable_featured_image = $project_banner_image['enable_featured_image'];
					?>
					<?php if(has_post_thumbnail() && $enable_featured_image ) : ?>
						<div class="bgimg" style="background-image: url(<?php echo  get_the_post_thumbnail_url() ?>)"></div>
						<?php else: ?>
							<div class="bgimg" style="background-image: url(<?php echo  $image['url'] ?>)"></div>
					<?php endif; ?>
				</div>
			</div>

			<?php $modules = get_field('modules');?>
			<?php foreach ($modules as $section) : ?>
				<?php $bg_overlay_color = $section['background_overlay_color']; ?>
				<?php  if ($section['acf_fc_layout'] === 'intro_section') :?>
					<div class="c-section c-project__intro animate">
						<div class="o-wrapper o-wrapper--950">
							<div class="c-project__intro-content"><?php echo $section['intro_copy']; ?></div>
						</div>
					</div>
				<?php endif; ?>
			
				<?php  if ($section['acf_fc_layout'] === 'two_column_content_image_with_background_image') :?>
					<div class="c-section c-project__content-image-bg <?php echo $bg_overlay_color; ?>" style="background-image: url(<?php echo $section['background_image']; ?>)">
						<div class="o-wrapper">
							<div class="o-layout c-project__content-image-bg--content o-wrapper--950">
								<div class="o-layout__item u-1/2@tablet">
									<?php $content = $section['content']; ?>
									<h2 class="animate"><?php echo $content['heading']; ?></h2>
									<div class="cms-content animate">
										<?php echo $content['copy']; ?>
										<?php if ($metrics) : ?>
										<?php $metrics = $content['metrics']; ?>
											<div class="c-metrics">
												<?php foreach ($metrics as $item) : ?>
													<div class="c-metrics__each">
														<h3><?php echo $item['metrics__value'] ?></h3>
														<p><?php echo $item['results'] ?></p>
													</div>
												<?php endforeach; ?>
											</div>
										<?php else: ?>
										<?php endif; ?>
									</div>
								</div>
								<div class="o-layout__item u-1/2@tablet">
									<img class="animate"  src="<?php echo $section['image']; ?>" alt="">
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<?php  if ($section['acf_fc_layout'] === 'only_content_with_background_image') :?>
					<div class="c-section c-project__content-bg  <?php echo $bg_overlay_color; ?>" style="background-image: url(<?php echo $section['background_image']; ?>)">
						<div class="o-wrapper">
							<div class="o-layout c-project__content-bg--content o-wrapper--950">
								<div class="o-layout__item u-1/2@tablet" >
									<img class="animate" src="<?php echo $section['image']; ?>" alt="">
								</div>
								<div class="o-layout__item u-1/2@tablet c-project__contents">
									<?php $content = $section['content']; ?>
									<h2 class="animate"><?php echo $content['heading']; ?></h2>
									<div class="cms-content animate" >
										<?php echo $content['copy']; ?>
										<?php $metrics = $content['metrics']; ?>
										<?php if ( !empty ($metrics)) : ?>
										<div class="c-metrics">
											<?php foreach ($metrics as $item) : ?>
												<div class="c-metrics__each">
													<h3><?php echo $item['metrics__value'] ?></h3>
													<p><?php echo $item['results'] ?></p>
												</div>
											<?php endforeach; ?>
										</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<?php  if ($section['acf_fc_layout'] === 'two_column_content_only_with_background_image') :?>
					<div class="c-section c-project__two-column-content-bg  <?php echo $bg_overlay_color; ?>" style="background-image: url(<?php echo $section['background_image']; ?>)">
						<div class="o-wrapper">
							<div class="o-layout c-project__content-bg--content o-wrapper--950">
								<div class="o-layout__item u-1/2@tablet c-project__contents">
									<?php $content = $section['content']; ?>
									<div class="two_column_content">
										<h2 class="animate"><?php echo $content['heading']; ?></h2>
										<div class="cms-content animate">
											<?php echo $content['copy']; ?>
										</div>
									</div>
								</div>
								<div class="o-layout__item u-1/2@tablet c-project__contents">
									<?php $metrics = $section['metrics']; ?>
									<?php if ( !empty ($metrics)) : ?>
										<?php foreach ($metrics as $item) : ?>
											<div class="c-metrics two-column-metrics">
												<div class="c-metrics__each">						
													<h3><?php echo $item['metrics__value'] ?></h3>
													<p><?php echo $item['results'] ?></p>
												</div>
											</div>
										<?php endforeach; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<?php  if ($section['acf_fc_layout'] === 'two_column_content_image_left') :?>
					<div class="c-section c-project__content-image">
						<div class="o-wrapper">
							<div class="o-layout c-project__content-image-left--content o-wrapper--950">
								<div class="o-layout__item u-1/2@tablet">
									<img class="animate" src="<?php echo $section['image']; ?>" alt="">
								</div>
								<div class="o-layout__item c-project__contents u-1/2@tablet">
									<?php $content = $section['content']; ?>
									<h2  class="animate"><?php echo $content['heading']; ?></h2>
									<div class="cms-content animate">
										<?php echo $content['copy']; ?>
										<?php $metrics = $content['metrics']; ?>
										<?php if ( !empty ($metrics)) : ?>
											<div class="c-metrics">
												<?php foreach ($metrics as $item) : ?>
													<div class="c-metrics__each">
														<h3><?php echo $item['metrics__value'] ?></h3>
														<p><?php echo $item['results'] ?></p>
													</div>
												<?php endforeach; ?>
											</div>
											<?php else: ?>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php  if ($section['acf_fc_layout'] === 'image_carousel') :?>
					<?php $layout = $section['choose_carousel_layout']; ?>
					<?php if(isset($layout)) : ?>
						<?php if($layout== 'landscape-middle') : ?>
							<div class="c-section c-project__image-carousel">
								<div class="o-wrapper c-images-layout">
									<div class="o-layout o-module">
										<div class="o-layout__item o-module__item u-2/4@tablet animate">
											<?php if ($section['image_portrait1']) : ?>
												<div class="bgimg bgimg--1" style="background-image: url(<?php echo $section['image_portrait1']; ?>)"></div>
											<?php endif; ?>
										</div>
										<div class="o-layout__item o-module__item u-1/4@tablet u-1/2 animate">
											<?php if ($section['image_portrait2']) : ?>
												<div class="bgimg bgimg--2" style="background-image: url(<?php echo $section['image_portrait2']; ?>)"></div>
											<?php endif; ?>
										</div>
										<div class="o-layout__item o-module__item u-1/4@tablet u-1/2 animate">
											<?php if ($section['image_portrait3']) : ?>
												<div class="bgimg bgimg--3" style="background-image: url(<?php echo $section['image_portrait3']; ?>)"></div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
							<?php elseif($layout== 'landscape-right'): ?>
							<div class="c-section c-project__image-carousel">
								<div class="o-wrapper c-images-layout">
									<div class="o-layout o-module">
										<div class="o-layout__item o-module__item u-1/4@tablet u-1/2 animate">
											<?php if ($section['image_portrait1']) : ?>
												<div class="bgimg bgimg--1" style="background-image: url(<?php echo $section['image_portrait1']; ?>)"></div>
											<?php endif; ?>
										</div>
										<div class="o-layout__item o-module__item u-1/4@tablet u-1/2 animate">
											<?php if ($section['image_portrait2']) : ?>
												<div class="bgimg bgimg--2" style="background-image: url(<?php echo $section['image_portrait2']; ?>)"></div>
											<?php endif; ?>
										</div>
										<div class="o-layout__item o-module__item u-2/4@tablet animate">
											<?php if ($section['image_portrait3']) : ?>
												<div class="bgimg bgimg--3" style="background-image: url(<?php echo $section['image_portrait3']; ?>)"></div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
							<?php elseif($layout== 'landscape-left'): ?>
							<div class="c-section c-project__image-carousel">
								<div class="o-wrapper c-images-layout">
									<div class="o-layout o-module">
										<div class="o-layout__item o-module__item u-1/4@tablet animate">
											<?php if ($section['image_portrait1']) : ?>
												<div class="bgimg bgimg--2" style="background-image: url(<?php echo $section['image_portrait1']; ?>)"></div>
											<?php endif; ?>
										</div>
										<div class="o-layout__item o-module__item u-2/4@tablet u-1/2 animate ">
											<?php if ($section['image_portrait2']) : ?>
												<div class="bgimg bgimg--1" style="background-image: url(<?php echo $section['image_portrait2']; ?>)"></div>
											<?php endif; ?>
										</div>
										<div class="o-layout__item o-module__item u-1/4@tablet u-1/2 animate">
											<?php if ($section['image_portrait3']) : ?>
												<div class="bgimg bgimg--3" style="background-image: url(<?php echo $section['image_portrait3']; ?>)"></div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
							<?php else: ?>
							<div class="c-section c-project__image-carousel">
								<div class="o-wrapper c-images-layout">
									<div class="o-layout o-module">
										<div class="o-layout__item o-module__item u-1/3@tablet u-1/2 animate">
											<?php if ($section['image_portrait1']) : ?>
												<div class="bgimg " style="background-image: url(<?php echo $section['image_portrait1']; ?>)"></div>
											<?php endif; ?>
										</div>
										<div class="o-layout__item o-module__item u-1/3@tablet u-1/2 animate">
											<?php if ($section['image_portrait2']) : ?>
												<div class="bgimg" style="background-image: url(<?php echo $section['image_portrait2']; ?>)"></div>
											<?php endif; ?>
										</div>
										<div class="o-layout__item o-module__item u-1/3@tablet animate">
											<?php if ($section['image_portrait3']) : ?>
												<div class="bgimg " style="background-image: url(<?php echo $section['image_portrait3']; ?>)"></div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endif;  ?>
					<?php endif; ?>
				<?php endif; ?>
				<?php  if ($section['acf_fc_layout'] === 'two_column_content_image_right') :?>
					<div class="c-section c-project__content-image-left animate">
						<div class="o-wrapper">
							<div class="o-layout c-project__content-image-right--content o-wrapper--950">
								<div class="o-layout__item u-1/2@tablet c-project__contents">
									<?php $content = $section['content']; ?>
									<h2 class="animate"><?php echo $content['heading']; ?></h2>
									<div class="cms-content animate">
										<?php echo $content['copy']; ?>
										<?php $metrics = $content['metrics']; ?>
										<?php if ($metrics) : ?>
										<div class="c-metrics">
											<?php foreach ($metrics as $item) : ?>
												<div class="c-metrics__each">
													<h3><?php echo $item['metrics__value'] ?></h3>
													<p><?php echo $item['results'] ?></p>
												</div>
											<?php endforeach; ?>
										</div>
										<?php endif; ?>
									</div>
								</div>
								<div class="o-layout__item u-1/2@tablet">
									<img class="animate" src="<?php echo $section['image']; ?>" alt="">
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>

			<?php $next_post = get_previous_post();?>
			<?php  if ($next_post) : ?>
				<?php  $post_cats=get_the_terms($next_post->ID, "ra-project-category");?>
				<div class="o-wrapper">
					<div class="c-project__nextpost" style="background-image: url(<?php echo get_the_post_thumbnail_url( $next_post->ID ); ?>)">
						<div class="c-project__nextpost-info">
							<h3>Next Case Study: <?php echo get_the_title( $next_post->ID ); ?></h3>
							<h4>
								<?php
								$no_of_cats=sizeof($post_cats);
								foreach($post_cats as $post_cat){
									echo $post_cat->name;

									if($no_of_cats-- >1)
									echo " | ";
								}
								?>

							</h4>
							<a class="o-btn" href="<?php echo get_permalink( $next_post->ID ); ?>">View Next <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
						</div>
					</div>
				</div>
			<?php endif; ?>

			<div class="c-section page-contact-bottom">
				<div class="o-wrapper animate">
					<p>Are you ready to take your brand to the next&nbsp;level?</p>
					<a href="/contact" class="o-btn">Contact Us <?php echo svgicon('btn-arrow', '0 0 13 13'); ?></a>
				</div>
			</div>
		</article>
	<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>