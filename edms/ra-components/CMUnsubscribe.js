"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true,
});
exports.default = undefined;

var _createClass = (function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }
  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
})();

var _class, _temp;

var _mjmlValidator = require("mjml-validator");

var _mjmlCore = require("mjml-core");

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError(
      "this hasn't been initialised - super() hasn't been called"
    );
  }
  return call && (typeof call === "object" || typeof call === "function")
    ? call
    : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError(
      "Super expression must either be null or a function, not " +
        typeof superClass
    );
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true,
    },
  });
  if (superClass)
    Object.setPrototypeOf
      ? Object.setPrototypeOf(subClass, superClass)
      : (subClass.__proto__ = superClass);
}

(0, _mjmlValidator.registerDependencies)({
  // Tell the validator which tags are allowed as our component's parent
  "mj-hero": ["cm-unsubscribe"],
  "mj-column": ["cm-unsubscribe"],
  // Tell the validator which tags are allowed as our component's children
  "cm-unsubscribe": [],
});

/*
  Our component is a (useless) simple text tag, that adds colored stars around the text.
  It can take 3 attributes, to specify size and colors.
*/
var CMUnsubscribe =
  ((_temp = _class = (function (_BodyComponent) {
    _inherits(CMUnsubscribe, _BodyComponent);

    function CMUnsubscribe() {
      _classCallCheck(this, CMUnsubscribe);

      return _possibleConstructorReturn(
        this,
        (CMUnsubscribe.__proto__ || Object.getPrototypeOf(CMUnsubscribe)).apply(
          this,
          arguments
        )
      );
    }

    _createClass(CMUnsubscribe, [
      {
        key: "getStyles",

        // This functions allows to define styles that can be used when rendering (see render() below)

        // Tells the validator which attributes are allowed for mj-layout
        value: function getStyles() {
          return {
            wrapperDiv: {
              color: this.getAttribute("color"), // this.getAttribute(attrName) is the recommended way to access the attributes our component received in the mjml
              "font-size": this.getAttribute("font-size"),
            },
            contentP: {
              "text-align": this.getAttribute("align"),
              "font-size": "20px",
            },
          };
        },

        /*
      Render is the only required function in a component.
      It must return an html string.
    */

        // What the name suggests. Fallback value for this.getAttribute('attribute-name').

        // Tell the parser that our component won't contain other mjml tags
      },
      {
        key: "render",
        value: function render() {
          return (
            "\n      <unsubscribe\n        " +
            this.htmlAttributes({
              // this.htmlAttributes() is the recommended way to pass attributes to html tags
              class: this.getAttribute("css-class"),
              style: "wrapperDiv", // This will add the 'wrapperDiv' attributes from getStyles() as inline style
            }) +
            "\n      >\n      <p " +
            this.htmlAttributes({
              style: "contentP", // This will add the 'contentP' attributes from getStyles() as inline style
            }) +
            ">\n        Unsubscribe\n      </p>\n      </unsubscribe>\n\t\t"
          );
        },
      },
    ]);

    return CMUnsubscribe;
  })(_mjmlCore.BodyComponent)),
  (_class.endingTag = true),
  (_class.allowedAttributes = {
    "stars-color": "color",
    color: "color",
    "font-size": "unit(px)",
    align: "enum(left,right,center)",
  }),
  (_class.defaultAttributes = {
    color: "black",
    "font-size": "12px",
    align: "center",
  }),
  _temp);
exports.default = CMUnsubscribe;
