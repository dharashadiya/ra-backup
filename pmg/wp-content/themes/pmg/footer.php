		</div><!-- .c-content -->
		
		<?php $footer_content = get_field('footer_content','option'); ?>
		<?php
			$email = $footer_content['email'];
			$address = $footer_content['address'];
			$copyright_text = $footer_content['copyright_text'];
			$short_copyright_text = $footer_content['short_copyright_text'];
			$page_links = $footer_content['page_links'];
			$social_media_links = $footer_content['social_media_links'];
			$partners = $footer_content['partners'];
		?>
		<footer class="c-footer">
			<div class="o-wrapper">
				<div class="c-footer__container">
					<div class="o-layout">
						<div class="o-layout__item u-3/5@tablet">
							<div class="c-footer__left-content">
								<?php
									$logo_wrap_class = "c-footer__logo-wrap";
									if (!is_front_page()) {
										$logo_wrap_open = '<a class="' . $logo_wrap_class . ' is-link" href="/" rel="home" title="Back to home">';
										$logo_wrap_close = '</a>';
									} else {
										$logo_wrap_open = '<a class="' . $logo_wrap_class . '">';
										$logo_wrap_close = '</a>';
									}
								?>
								<?php 
									echo $logo_wrap_open;
									echo svgicon('logo', '0 0 129 53','c-footer__logo');
									echo $logo_wrap_close; 
								?>
								<?php if($email) : ?>
									<a class="c-footer__email" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
								<?php endif; ?>
								<?php if($address) : ?>
									<div class="c-footer__address"><?php echo $address?></div>
								<?php endif; ?>
							</div>
						</div>
						<div class="o-layout__item  u-2/5@tablet">
							<div class="o-layout ">
								<div class="o-layout__item  u-1/2@tablet">
									<div class="c-footer__right-content">
										<div class="c-footer__menus">
											<?php
												wp_nav_menu( array(
													'theme_location' => 'primary-menu',
													'container' => false,
													'menu_class' => 'c-nav',
													'menu_id' => 'menu'
												));
											?>
										</div>
									</div>
								</div>
								<div class="o-layout__item  u-1/2@tablet">
									<ul class="c-footer__links">
										<?php foreach ( $social_media_links as $link) :?> 
										<?php $social__name = $link['social__name']; ?>
											<li class="c-footer__link">
												<?php if($link) : ?>
													<a class="<?php echo $social__name; ?>" 
														href="<?php echo $link['link']['url']; ?>" 
														target="<?php echo $link['link']['target']; ?>">
														<?php echo $link['link']['title']; ?>
													</a>
												<?php endif; ?>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
				
							<div class="c-footer__our-partners">
								<div class="o-layout">
									<?php foreach($partners as $partner ) : ?>
										<div class="o-layout__item u-1/2">
											<?php 
												$title = $partner['title'];
												$logo = $partner['logo'];
												$logo_link = $partner['logo_link'];
											?>
											<div class="c-footer__title"><?php echo $title; ?></div>
											<div class="c-footer__icon-wrap">
												<a class="c-footer__icon-link" href="<?php echo $logo_link['url']; ?>" target="_blank">
													<img class="c-footer__icon" src="<?php echo $logo['url'] ?>" >
												</a>
											</div>
										</div>
									<?php endforeach; ?>		
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="o-wrapper">
					<div class="footer-bottom__container">
						<div class="o-layout o-order o-order--tabletLarge">
							<div class="o-layout__item  u-2/5@tabletLarge o-order__2@tabletLarge">
								<div class="footer-bottom__page-links">
									<div class="o-layout o-layout--normal@tabletLarge o-layout--auto o-layout--flush">
										<?php foreach ( $page_links as $page_link) :?> 
											<?php if($page_link) : ?>
												<div class="o-layout__item u-1/2@tabletLarge">
													<a class="footer-bottom__page-link" href="<?php echo get_the_permalink($page_link->ID); ?>">
														<?php echo $page_link->post_title; ?>
													</a>
												</div>
											<?php endif; ?>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
							<div class="o-layout__item u-3/5@tabletLarge o-order__1@tabletLarge">
								<div class="footer-bottom__copy-wrap">
									<?php if($copyright_text) : ?>
										<div class="footer-bottom__copy"><?php echo $copyright_text ?></div>
									<?php endif; ?>
									<?php if($short_copyright_text) : ?>
										<div class="footer-bottom__copy-short"><?php echo $short_copyright_text ?></div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<?php wp_footer(); ?>
		<div class="space-bottom"></div>
	</body>
</html>
