<?php 
       $services = $block['service'];
       $page_link = $block['page_link'];
       $title = $block['title'];
?>
<div class="c-services-three-column">
	<div class="o-wrapper">
		<div class="c-services-three-column__container">
            <div class="o-layout o-layout--services-three-column">
                <div class="o-layout__item u-1/4@laptopXSmall u-1/1@laptopXSmall">
                    <div class="c-services-three-column__header"  data-aos="fade-scale" data-aos-duration="800">
                        <h6 class="u-h2 c-services-three-column__heading"><?php echo $title ?></h6>
                        <?php if($page_link) : ?>
                            <a class="c-services-three-column__link" href="<?php echo $page_link['url']; ?>"> <?php echo $page_link['title']; ?>  <?php	echo svgicon('arrow-next', '0 0 16 16', 'c-case-studies__arrow');?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <?php foreach($services as $service->ID) : ?>
                    <?php 
                        $service_content = get_field('service_content', $service->ID);
                        $intro =  $service_content['intro'];
                        $icon = $service_content['icon'];
                        ?>
                    <div class="o-layout__item u-1/4@laptopXSmall u-1/3@tabletSmall" >
                        <div class="c-service-three-column"> 
                            <div class="c-service-three-column__wrap-one"  data-aos="flip-down" data-aos-duration="1200"> 
                                <div class="c-service-three-column__icon--wrap" >
                                    <?php if($icon) : ?>
                                        <img class="c-service-three-column__icon" data-src=" <?php echo $icon['url'] ?>" alt="">
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="c-service-three-column__wrap-two"  data-aos="fade-up" data-aos-duration="1000">
                                <h6 class="c-service-three-column__title" ><?php echo get_the_title($service->ID); ?></h6>
                                <?php if($intro) : ?>
                                    <div class="c-service-three-column__intro" ><?php echo $intro ?></div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?Php endforeach; ?>
            </div>
		</div>
	</div>
</div>

<div class="space-bottom"></div>