<?php 
    $column_one = $block['column_one'];
    $column_two = $block['column_two'];   
?>
<div class="c-two-column">
    <div class="o-wrapper">
        <div class="c-two-column__container">
            <div class="o-layout c-two-column__wrap  o-layout--two-column">
                <div class="o-layout__item u-1/2@tablet">
                    <?php if($column_one) : ?>
                        <div class="c-two-column__content-one" data-aos="fade-up" data-aos-duration="1200" data-aos-delay="100"><?php echo $column_one; ?></div>
                    <?php endif; ?>
                </div>
                <div class="o-layout__item u-1/2@tablet">
                    <?php if($column_two) : ?>
                        <div class="c-two-column__content-two" data-aos="fade-up" data-aos-duration="1200" data-aos-delay="200"><?php echo $column_two; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="space-bottom"></div>