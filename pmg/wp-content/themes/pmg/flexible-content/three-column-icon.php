<?php 
    $columns = $block['columns'];
?>
<div class="c-three-columns">
    <div class="o-wrapper">
        <div class="c-three-columns__container">
            <div class="o-layout o-layout--three-columns">
                <?php foreach($columns as $column) : ?>
                    <?php 
                        $icon =  $column['icon'];
                        $title = $column['title'];
                        $content = $column['content'];
                    ?>
                    <div class="o-layout__item u-1/3@tablet">
                        <div class="c-three-column"> 
                            <div class="c-three-column__icon--wrap" data-aos="fade-up" data-aos-duration="1000">
                                <?php if($icon) : ?>
                                    <img class="c-three-column__icon" data-src=" <?php echo $icon['url'] ?>" alt="">
                                <?php endif; ?>
                            </div>
                            <?php if($title) : ?>
                                <h6 class="u-h6 c-three-column__title" data-aos="fade-up" data-aos-duration="1400"><?php echo $title ?></h6>
                            <?php endif; ?>
                            <?php if($content) : ?>
                                <div class="c-three-column__content" data-aos="fade-up" data-aos-duration="1600"><?php echo $content ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?Php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<div class="space-bottom"></div>