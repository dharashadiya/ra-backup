<?php 
	$case_studies = $block['case_study'];
	$heading = $block['heading'];
	$intro = $block['intro'];
    $page_link = $block['page_link'];
	$bg_colour = $block['background_colour'];
?>

<!-- Home page layout: case_study -->
<div class="c-case-studies <?php echo $bg_colour; ?>">
	<div class="o-wrapper">
		<div class="c-case-studies__container">
			<div class="c-case-studies__top-content"  data-aos="fade-up" data-aos-delay="800" data-aos-duration="1400">
				<div class="o-layout u-align-center">
					<?php if ($heading) : ?>
						<div class="o-layout__item ">
							<h2 class="u-h2 c-case-studies__heading"><?php echo $heading; ?></h2>
						</div>
					<?php endif; ?>
					<?php if ( $intro ) : ?>
						<div class="c-case-studies__intro o-layout__item "><?php echo $intro; ?></div>
					<?php endif; ?>
				</div>
			</div>
			<?php foreach($case_studies as $case_study) : ?>
				<?php
					$case_study_content = get_field('case_study_content', $case_study->ID);
					$intro = $case_study_content['case_study_intro'];
					$title = get_the_title($case_study->ID);
					$image = $case_study_content['image_home_page'];
				?>
				<div class="c-case-study">
					<div class="o-layout  o-order--tablet o-layout--case-study-home">	
						<div class="o-layout__item u-1/2@tablet o-order__2@tablet">
							<div class="c-case-study__img-wrap" data-aos="fade-left"data-aos-duration="1600" data-aos-delay="800">
								<div class="c-case-study__img-wrap-two"> 
									<?php if($image) : ?>
										<img class="c-case-study__img" data-src="<?php echo $image['sizes']['case-study']; ?>" alt="<?php echo $image['title']; ?>">
										<img class="c-case-study__img-two" data-src="<?php echo $image['sizes']['square']; ?>" alt="<?php echo $image['title']; ?>">
									<?php endif; ?>
								</div>
							</div>
						</div>	
						<div class="o-layout__item u-1/2@tablet o-order__1@tablet" data-aos="fade-right" data-aos-duration="1600" data-aos-delay="800">
							<div class="c-case-study__content" >
								<?php if($title) : ?>
									<h4 class="u-h4 c-case-study__title">Our Clients: <?php echo $title; ?></h4>
								<?php endif; ?>
								<?php if($intro) : ?>
									<div class="c-case-study__intro-home-page"><?php echo $intro; ?></div>
								<?php endif; ?>
							</div>
							<div class="c-case-study__btn-wrap" >
								<a class="c-case-study__btn o-btn o-btn-simple" href=" <?php echo $page_link['url']; ?>"><?php echo $page_link['title']; ?></a>
							</div>
						</div>
					
					</div>
				</div>
			<?php endforeach ; ?> 
		</div>
	</div>
</div>
<div class="space-bottom"></div>
<!-- div class="c-case-study__content lax" data-lax-translate-y="vh 0, -elh -50" data-lax-anchor="self" -->
<!-- <div class="o-layout__item u-1/2@tablet lax" data-lax-translate-y="vh 0, -elh -60" data-lax-anchor="self"> -->