<?php 
    $title = $block['title'];
    $heading = $block['heading'];
    $intro_home_page = $block['intro_home_page'];
    $intro = $block['intro'];
    $button_link = $block['button_link'];
?>
<?php if(is_front_page()): ?>
    <div class="space-top"></div>
<?php endif; ?>
<div class="c-page-intro">
    <div class="o-wrapper">
        <div class="c-page-intro__container">
            <div class="o-layout u-align-center" data-aos="fade-up" data-aos-duration="1200">
                <div class="o-layout__item u-1/1">
                    <?php if( $title ) : ?>
                        <h6 class="u-h6 c-page-intro__title"><?php echo $title; ?></h6>
                    <?php endif; ?>
                    <?php if( $heading ) : ?>
                        <h2 class="u-h2 c-page-intro__heading"><?php echo $heading; ?></h2>
                    <?php endif; ?>
                    <?php if( $intro_home_page ) : ?>
                        <?php if (is_front_page()): ?>
                            <div class=" c-page-intro__intro-home-page c-page-intro__intro"><?php echo $intro_home_page; ?></div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if( $intro ) : ?>
                        <h5 class="u-h5 c-page-intro__intro"><?php echo $intro; ?></h5>
                    <?php endif; ?>
                    <?php if( $button_link ) : ?>
                        <a data-aos="fade-up" data-aos-duration="1000" class="c-page-intro__btn o-btn o-btn-simple" href="<?php echo $button_link['url']; ?>">
                            <?php echo $button_link['title']; ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (is_front_page()): ?>
    <div class="space-bottom"></div>
<?php endif; ?>
