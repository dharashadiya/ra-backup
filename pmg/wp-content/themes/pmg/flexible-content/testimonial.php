<?php 
    $title = $block['title'];
    $quotes = $block['quotes'];   
?>
<div class="c-testimonial">
    <div class="o-wrapper">
        <div class="c-testimonial__container">
            <div class="u-align-center" data-aos="fade-scale" data-aos-duration="1400" data-aos-delay="800">
                <?php if($title) : ?>
                    <div class="u-h6 c-testimonial__title">
                        <?php echo $title; ?>
                    </div>
                <?php endif; ?>
                <div class="c-testimonial__quotes one-time-carousel">
                    <?php foreach($quotes as $quote) : ?>
                        <?php if($quote) : ?>
                            <div class="c-testimonial__quote ">
                                <?php echo $quote['quote']; ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach ; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="space-bottom"></div>