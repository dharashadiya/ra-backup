<?php 
    $heading = $block['heading'];
    $intro = $block['intro'];
    $layout = $block['layout'];
    $select_our_team = $block['select_our_team'];
?>
<div class="c-directors c-directors--<?php echo $layout; ?>">
    <div class="o-wrapper">
        <div class="c-directors__container">
            <div class="c-directors__top-content" data-aos="fade-scale" data-aos-duration="1200" data-aos-delay="400">
                <div class="o-layout u-align-center">
                    <?php if ($heading) : ?>
                        <div class="o-layout__item ">
                            <h2 class="u-h2 c-directors__heading"><?php echo $heading; ?></h2>
                        </div>
                    <?php endif; ?>
                    <?php if ( $intro ) : ?>
                        <div class="c-directors__intro o-layout__item "><?php echo $intro; ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="c-directors__items team-member">
                <div class="o-layout <?php echo $layout != 'vertical' ? 'o-layout--medium' : ''; ?>">
                    <?php foreach($select_our_team as $key => $member) : ?>
                        <?php 
                            $our_team = get_field('our_team', $member->ID);
                            $position = $our_team['position']; 
                            $bio = $our_team['bio'];
                            $image = $our_team['image'];
                            $social_links = $our_team['social_links'];
                            $title = get_the_title($member->ID);
                        ?>
                        <div class="c-director__wrap o-layout__item u-1/4@laptopLarge  u-1/3@tabletWide u-1/2@mobileLandscape ">
                            <div class="c-director c-director--horizontal" data-aos="fade-up" data-aos-delay="<?php echo($key+1)*200; ?>" data-aos-duration="1000">
                            
                                <div class="c-director__img--wrap">
                                    <div class="c-director__img-wrap-two">
                                        <?php if($image) : ?>
                                            <img data-src="<?php echo $image['sizes']['team']; ?>" alt="" class="c-director__img team-member">
                                        <?php endif; ?>
                                    </div>
                                </div>                                                                           
                                <div class="c-director__content">
                                    <?php if($title) : ?>
                                        <div class="c-director__name u-h4"><?php echo  $title; ?></div>
                                    <?php endif; ?>
                                    <?php if($position) : ?>
                                        <div class="c-director__position"><?php echo $position; ?></div>
                                    <?php endif; ?>
                                    <?php if($bio) : ?>
                                        <div class="c-director__intro"><?php echo $bio?></div>
                                    <?php endif; ?>
                                    <div class="c-director__social-links">
                                        <?php foreach($social_links as $link) : ?>
                                            <?php if($link) : ?>
                                                <a href="<?php echo $link['link']['url']; ?>" class="c-director__social-link" target="_blank">
                                                    <?php echo svgicon($link['icon'], '0 0 22 22'); ?>
                                                </a>
                                            <?php endif; ?>
                                        <?php endforeach ; ?>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="space-bottom"></div>
