
<?php 
    $services = $block['services'];
?>

<div class="c-services">
	<div class="o-wrapper">
		<div class="c-services__container">
			<?php foreach($services as $key=>$service) : ?>

				<?php
					$service_content = get_field('service_content', $service->ID);
					$intro = $service_content['intro'];
					$title = get_the_title($service->ID);
					$image = $service_content['image']
				?>
				
				<div class="c-service" data-aos="fade-animate" data-aos-duration="1400" data-aos-delay="<?php echo($key+1)*200; ?>">
			
					<div class="o-layout o-order o-order--tablet">
						<div class="o-layout__item  o-order-center u-1/2@tablet <?php echo ($key+1)%2 == 1 ? 'o-order__2@tablet' : ''; ?>">
							<div class="c-service__content lax" data-lax-translate-y="vh 0, -elh 10" data-lax-anchor="self">
								<div class="c-service__title-wrap">
									<h4 class="u-h4 c-service__title"><?php echo $title; ?></h4>
								</div>
								<?php if($intro) : ?>
									<div class="c-service__intro">
										<?php echo $intro . ' ';?>	
									</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="o-layout__item u-1/2@tablet 
						<?php echo ($key+1)%2 == 1 ? 'o-order__1@tablet' : ''; ?>">
							<div class="c-service__img-wrap lax" data-lax-translate-y="vh 0, -elh -10" data-lax-anchor="self" >							
								<?php if($image ) : ?>
									<img class="c-service__img" data-src="<?php echo $image['sizes']['services']; ?>" alt="<?php echo $image['title']; ?>">
									<img class="c-service__img-two" data-src="<?php echo $image['sizes']['square']; ?>" alt="<?php echo $image['title']; ?>">
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach ; ?> 
		</div>
	</div>
</div>
<div class="space-bottom"></div>