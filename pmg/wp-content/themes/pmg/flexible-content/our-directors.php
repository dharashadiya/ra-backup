<?php 
    $heading = $block['heading'];
    $page_link = $block['page_link'];
    $intro_content = $block['intro_content'];
    $layout = $block['layout'];
    $select_directors = $block['select_directors'];
?>
<div class="c-directors c-directors--<?php echo $layout; ?>">
    <div class="o-wrapper">
        <div class="c-directors__container">
            <div class="c-directors__top-content" data-aos="fade-scale" data-aos-duration="1200" data-aos-delay="200">
                <div class="o-layout u-align-center">
                    <?php if ($heading) : ?>
                        <div class="o-layout__item ">
                            <h2 class="u-h2 c-directors__heading"><?php echo $heading; ?></h2>
                        </div>
                    <?php endif; ?>
                    <?php if ( $intro_content ) : ?>
                        <div class="c-directors__intro o-layout__item "><?php echo $intro_content; ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="c-directors__items">
                <!-- Our Directors -->
                <?php if ($layout != 'vertical') : ?><div class=" o-layout <?php echo $layout != 'vertical' ? 'o-layout--medium' : ''; ?>"><?php endif; ?>
                    <!-- foreach loop -->
                    <?php foreach($select_directors as $key => $director) : ?>
                        <?php 
                            $our_team = get_field('our_team', $director->ID);
                            $position = $our_team['position']; 
                            $bio = $our_team['bio'];
                            $social_links = $our_team['social_links'];
                            $title = get_the_title($director->ID);
                            $thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id($director->ID), 'director');
                        ?>
                        <?php if ($layout != 'vertical') : ?><div class="c-director__wrap o-layout__item u-1/3@tabletXLarge  u-1/2@tablet"><?php endif; ?>
                            <div class="c-director c-director--<?php echo $layout; ?>" data-aos="fade-scale" data-aos-delay="<?php echo($key+1)*200; ?>" data-aos-duration="1000">
                                <?php if ($layout == 'vertical') : ?><div class="o-layout o-layout--middle"><?php endif; ?>   

                                    <?php if ($layout == 'vertical') : ?><div class="o-layout__item u-3/6@tablet u-1/1"><?php endif; ?>              
                                        <div class="c-director__img--wrap">
                                            <div class="c-director__img-wrap-two">
                                                <?php if($thumbnail_src) : ?>
                                                    <img data-src="<?php echo $thumbnail_src[0]; ?>" alt="" class="c-director__img">
                                                <?php endif; ?>
                                            </div>
                                        </div>                                           
                                    <?php if ($layout == 'vertical') : ?></div><?php endif; ?>
                 
                                    <?php if ($layout == 'vertical') : ?><div class="o-layout__item u-3/6@tablet u-1/1"><?php endif; ?>
                                        <div class="c-director__content">
                                            <?php if($title) : ?>
                                                <div class="c-director__name u-h4"><?php echo  $title; ?></div>
                                            <?php endif; ?>
                                            <?php if($position) : ?>
                                                <div class="c-director__position"><?php echo $position; ?></div>
                                            <?php endif; ?>
                                            <?php if($bio) : ?>
                                                <div class="c-director__intro"><?php echo $bio?></div>
                                            <?php endif; ?>
                                            <div class="c-director__social-links">
                                                <!-- foreach loop -->
                                                <?php foreach($social_links as $link) : ?>
                                                    <?php if($link) : ?>
                                                        <a href="<?php echo $link['link']['url']; ?>" class="c-director__social-link" target="_blank">
                                                            <?php echo svgicon($link['icon'], '0 0 22 22'); ?>
                                                        </a>
                                                    <?php endif; ?>
                                                <?php endforeach ; ?>    
                                            </div>
                                        </div>
                                    <?php if ($layout == 'vertical') : ?></div><?php endif; ?>

                                <?php if ($layout == 'vertical') : ?></div><?php endif; ?>        
                            </div>
                        <?php if ($layout != 'vertical') : ?></div><?php endif; ?>
                    <?php endforeach ; ?>
                <?php if ($layout != 'vertical') : ?></div><?php endif; ?>
            </div>

            <?php if(is_front_page()) : ?>
                <?php if( $page_link ) : ?>
                    <div class="o-layout u-align-center" data-aos="fade-up" data-aos-delay="800" data-aos-duration="1200">
                        <div class="o-layout__item">
                            <a class="c-directors__btn o-btn o-btn-simple " href="<?php echo $page_link['url']; ?>">
                                <?php echo $page_link['title']; ?>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>

    </div>
</div>
<div class="space-bottom"></div>
