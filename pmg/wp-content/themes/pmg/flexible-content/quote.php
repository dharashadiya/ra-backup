<?php 
    $quote = $block['quote'];
    $quote_by = $block['quote_by'];   
    $name = $quote_by['name'];
    $position= $quote_by['position'];
?>
<div class="c-quote">
    <div class="o-wrapper c-quote__wrap">
        <div class="c-quote__container">
            <div class="o-layout o-layout--center">
                <div class="o-layout__item o-island u-1/1">
                    <?php if($name) : ?>
                        <div class="c-quote__quote  o-layout--center" data-aos="fade-scale" data-aos-duration="1200"><?php echo $quote ?></div>
                    <?php endif; ?>
                    <?php if($name || $position) : ?>
                        <div class="u-h6 c-quote__quote-by  o-layout--center" data-aos="fade-up" data-aos-duration="1200"><?php echo $name ?>, <?php echo $position ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <span class="c-quote__curve1">
            <?php echo svgicon('curve1', '0 0 948 652'); ?>
        </span>
        <span class="c-quote__curve2">
            <?php echo svgicon('curve2', '0 0 938 1052'); ?>
        </span>
        <span class="c-quote__curve3">
            <?php echo svgicon('curve3', '0 0 888 1222'); ?>
        </span>
    </div>

</div>
<div class="space-bottom"></div>