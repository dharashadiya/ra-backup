<?php
    // $enquiry_form_content = get_field('enquiry_form_content');
    $enquiry_form =  $block['enquiry_form'];
    $title =  $block['title'];
    $info =  $block['info'];
?>
<div class="c-enquiry">
    <div class="o-wrapper">
        <div class="c-enquiry__container">
            <div class="c-enquiry__content">
                <div class="o-layout">
                    <?php if (!is_page_template( 'page-contact.php')): ?>
                        <div class="o-layout__item u-3/6@tablet">
                            <?php if ($title) : ?>
                                <div class="c-enquiry__title-wrap" data-aos="fade-up" data-aos-duration="1200"> 
                                    <h2 class="u-h2 c-enquiry__title"><?php echo $title; ?></h2>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if (is_page_template( 'page-contact.php')): ?>
                            <div class="o-layout__item u-1/1">
                        <?php else: ?>
                            <div class="o-layout__item u-3/6@tablet ">
                    <?php endif; ?>
                        <?php if ( $info ) : ?>
                            <div class="c-enquiry__intro-wrap" data-aos="fade-up" data-aos-duration="1200">
                                <div class="c-enquiry__info"><?php echo $info; ?></div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php if($enquiry_form) : ?>
                <div class="c-enquiry__form-content" data-aos="fade-up" data-aos-delay="400" data-aos-duration="1400">
                    <?php echo $enquiry_form ; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>   

<?php if (!is_page_template( 'page-contact.php')): ?>
    <div class="space-bottom"></div>
<?php endif; ?>