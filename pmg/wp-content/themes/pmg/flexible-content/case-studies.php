
<?php 
    $case_studies = $block['case_studies'];
	$page_link = $block['page_link'];
	$heading = $block['heading'];
	$intro = $block['intro'];
?>
<div class="c-case-studies">
	<div class="o-wrapper">
		<div class="c-case-studies__container">
			<div class="c-case-studies__top-content" data-aos="fade-up" data-aos-duration="1200">
				<div class="o-layout u-align-center">
					<?php if ($heading) : ?>
						<div class="o-layout__item ">
							<div class="u-h2 c-case-studies__heading"><?php echo $heading; ?></div>
						</div>
					<?php endif; ?>
					<?php if ( $intro ) : ?>
						<div class="c-case-studies__intro o-layout__item "><?php echo $intro; ?></div>
					<?php endif; ?>
				</div>
			</div>
			<?php foreach($case_studies as $key=>$case_study) : ?>
				<?php
					$case_study_content = get_field('case_study_content', $case_study->ID);
					$intro = $case_study_content['content'];
					$title = get_the_title($case_study->ID);
					$image = $case_study_content['image'];
				?>		
				<div class="c-case-study" data-aos="fade-animate"  data-aos-duration="1400" data-aos-delay="<?php echo($key+1)*200; ?>">
					<div class="o-layout o-order o-order--tablet o-layout--case-studies o-layout--stretch">
						<div class="o-layout__item u-1/2@tablet <?php echo ($key+1)%2 == 0 ? 'o-order__2@tablet' : ''; ?>">
							<div class="c-case-study__content lax" data-lax-translate-y="vh 0, -elh 10" data-lax-anchor="self">
								<?php if($title) : ?>
									<h4 class="u-h4 c-case-study__title"><?php echo $title; ?></h4>
								<?php endif; ?>
								<?php if($intro) : ?>
									<div class="c-case-study__intro"><?php echo $intro;?>	
								</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="o-layout__item u-1/2@tablet <?php echo ($key+1)%2 == 0 ? 'o-order__1@tablet' : ''; ?>">
							<div class="c-case-study__img-wrap">
								<div class="c-case-study__img-wrap-two lax" data-lax-translate-y="vh 0, -elh -10" data-lax-anchor="self"> 
									<?php if($image ) : ?>
										<img class="c-case-study__img" data-src="<?php echo $image['sizes']['case-study']; ?>" alt="<?php echo $image['title']; ?>">
										<img class="c-case-study__img-two" data-src="<?php echo $image['sizes']['square']; ?>" alt="<?php echo $image['title']; ?>">
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach ; ?> 
		</div>
	</div>
</div>
<div class="space-bottom"></div>