
<div class="c-enquiry" id="content"> 
    <?php  
        $address = $block['address'];
        $email = $block['email_address'];
        $phone_number = $block['phone_number'];
    ?>
    <div id="popup" data-lat="<?php echo esc_attr($address['lat']); ?>" data-lng="<?php echo esc_attr($address['lng']); ?>">
       
        <p><?php echo $address['address']; ?></p> 
        <p><?php echo $email; ?></p>
        <p><?php echo $phone_number; ?></p>
       
    </div>
    <div class="c-enquiry__map--wrap" id="map">
        <div class="c-enquiry__map" data-zoom="16"> 
        </div>
    </div>
</div>   
<div class="space-bottom"></div>