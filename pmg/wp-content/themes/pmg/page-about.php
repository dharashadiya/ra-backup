<?php get_header(); ?>
<?php
    /** Template Name: About Page */
 ?>
<?php  
    get_template_part( 'partials/page','banner');  
    $content_blocks = get_field('content_blocks');
    if(!empty($content_blocks)) {
      include('partials/flexible-content.php');
    }
?>
<?php get_footer(); ?>



