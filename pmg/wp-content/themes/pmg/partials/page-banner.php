<?php $page_banner = get_field('page_banner'); ?>
<?php
    $title= get_the_title();
    $thumbnail_url = wp_get_attachment_url( get_post_thumbnail_id($albums_list->ID));
    $intro = $page_banner['intro'];
    $client_name = $page_banner['client_name'];
    $short_intro = $page_banner['short_intro'];
    $video = $page_banner['background_video'];
    $cta_link = $page_banner['cta_link'];
    $banner = get_field('404_content', 'option');
?>

<div class="c-top-fixed"></div>
<?php if ( is_front_page()): ?>
        <?php if($video) : ?>
            <div class="c-fullscreen-video"> 
            <div class="c-fullscreen-video__container">
                <video class="c-fullscreen-video__video" autoplay muted loop playsinline>
                    <source src="<?php echo $video['url']; ?>" type="video/mp4">
                </video>
            </div>
        <?php else: ?>
            <div class="c-page-banner <?php echo $intro ? 'has-intro' : ''; ?>" style="background-image: url(<?php echo $thumbnail_url; ?>)">
        <?php endif; ?>
    <?php else: ?>
        <?php if ( is_404()): ?>
            <?php if($banner) : ?>
                <div class="c-page-banner <?php echo $intro ? 'has-intro' : ''; ?>" style="background-image: url(<?php echo $banner['page_banner_background_image']['sizes']['page-banner'] ?>)">
            <?php endif; ?>
        <?php else :?>
            <?php if($thumbnail_url) : ?>
                <div class="c-page-banner <?php echo $intro ? 'has-intro' : ''; ?>" style="background-image: url(<?php echo $thumbnail_url; ?>)">
            <?php endif; ?>
        <?php endif ; ?>
<?php endif; ?>

    <div class="o-wrapper">
        <div class="c-page-banner__container lax" data-lax-opacity="0 1, vh 0">
            <div class="o-layout u-align-center">
                <div class="o-layout__item">
                    <?php if ( !is_404() && !is_front_page()): ?>
                        <h1 class=" c-page-banner__title" data-aos="fade-right" data-aos-duration="1000"><?php echo $title; ?></h1>
                    <?php endif; ?>
                    <?php if ( is_404()):  ?>
                        <h1 class=" c-page-banner__title" data-aos="fade-up" data-aos-duration="1000"><?php echo $banner['title'] ?></h1>
                    <?php endif; ?>
                    <?php if ( is_front_page()): ?>
                        <div class=" c-page-banner__title" data-aos="fade-right" data-aos-duration="1000"><?php echo svgicon('logo', '0 0 129 53', 'c-page-banner__svg'); ?></div>
                    <?php endif; ?>

                </div>

                <div class="o-layout__item">
                    <?php if($intro) : ?>
                        <div class="c-page-banner__intro" data-aos="fade-left" data-aos-anchor-placement="top-bottom" data-aos-duration="1200"><?php echo $intro; ?></div>
                    <?php endif; ?>
                </div>

                <?php if ( is_front_page() ): ?>
                <div class="o-layout__item">
                    <div class="u-align-center">
                        <?php if($cta_link) : ?>
                            <a class="c-page-banner__btn" href="<?php echo $cta_link['url']; ?>"><?php echo $cta_link['title']; ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endif; ?>

            </div>   
        </div>
    </div>
    <?php if ( !is_front_page() ): ?>   
        <a href="#content">
            <span class="c-page-banner__arrow  lax" data-lax-opacity="0 1, vh 0">
                <?php echo svgicon('arrow', '0 0 29 30"'); ?>
            </span> 
        </a> 
    <?php endif; ?>
</div>
<?php if(!is_front_page()): ?>
    <div class="space-bottom"></div>
<?php endif; ?>

