<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
	<title><?php wp_title(); ?> </title>

	<?php if(is_local_or_staging()) : ?>
		<meta name="robots" content="none" />
		<meta name="robots" content="noindex" />
		<meta name="googlebot" content="none" />
		<meta name="googlebot" content="noindex" />
	<?php endif; ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">
	<link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap" rel="stylesheet">
	
	<?php wp_head(); ?>
	<?php include('partials/inlinescripts.min.php'); ?>
</head>

<body <?php body_class(); ?>>
<div class="c-top">
	<header class="c-header" data-toggle-content="main-nav">
		<div class="o-wrapper">
			<div class="c-header__container">
				<?php
					$logo_wrap_class = "c-header__logo-wrap";
					if (!is_front_page()) {
						$logo_wrap_open = '<a class="' . $logo_wrap_class . ' is-link" href="/" rel="home" title="Back to home">';
						$logo_wrap_close = '</a>';
					} else {
						$logo_wrap_open = '<a class="' . $logo_wrap_class . '">';
						$logo_wrap_close = '</a>';
					}
				?>
				<?php 
					echo $logo_wrap_open;
					echo svgicon('logo', '0 0 129 53', 'c-header__logo');
					echo $logo_wrap_close; 
				?>
				
				<div class="c-header__nav-toggle-wrap">
					<button class="c-header__nav-toggle" data-toggle="main-nav" id="hamburger">
						<span></span>
						<span></span>
						<span></span>
					</button>
				</div>
			</div>
		</div>

		<nav class="c-overlay-nav" id="myNav" data-toggle-content="main-nav">
			<div class="c-overlay-nav__container">
				<div class="c-overlay-nav__menus">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'primary-menu',
							'container' => false,
							'menu_class' => 'c-nav',
							'menu_id' => 'menu'
						));
					?>
				<div class="c-overlay-nav__bottom-icons">
					<?php 
						$footer_content = get_field('footer_content','option'); 
						$social_media_links = $footer_content['social_media_links'];
					?>

					<?php foreach($social_media_links as $link) : ?>
						<?php $social__name = $link['social__name']; ?>
						
						<?php if($link) : ?>
							<a class="c-overlay-nav__icon-link <?php echo $social__name; ?>" href="<?php echo $link['link']['url']; ?>" target="<?php echo $link['link']['target']; ?>">
								<img class="c-overlay-nav__icon" src="<?php echo $link['icon']['url']?>" alt="<?php $link['icon']['title']?>">
							</a>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
			</div>
		</nav>
	</header>
</div>

<div class="c-content">

