<?php get_header(); ?>
<?php
	get_template_part( 'partials/page','banner');  
	$banner = get_field('404_content', 'option')
?>

<div class="c-404" id="content">
    <div class="o-wrapper">
		<main class="c-404__container" >
			<article>
				<div class="c-404__content" data-aos="fade-scale" data-aos-duration="1000">
					<?php echo $banner['message']; ?>
				</div>
			</article>
		</main>
	</div>
</div>

<div class="space-bottom"></div>

<?php get_footer(); ?>