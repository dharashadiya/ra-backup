<?php

define( 'GOOGLE_ANALYTICS', '');
include('functions/boilerplate.php');
include('functions/admin-config.php');
include('functions/theme-helpers.php');
include('functions/clean-gallery.php');
include('functions/acf-config.php');
include('functions/shortcodes.php');
include('functions/custom-post-type.php');
// include('functions/woocommerce-helpers.php');
// include('functions/woocommerce-customisation.php');

// Author meta tag
define( 'AUTHOR', 'Base Theme');

// Enable post thumbnails
add_theme_support( 'post-thumbnails' );

// Enable nav menus
add_theme_support( 'nav-menus' );

// Custom image sizes
add_image_size( 'preview', 300, 200, false );

// make custom image size choosable from add media
add_filter( 'image_size_names_choose', 'site_custom_sizes' );
function site_custom_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'preview' => 'Preview',
	) );
}

// For development
show_admin_bar(!is_localhost() && is_user_logged_in());

// Register nav menus
function register_site_menus() {
	register_nav_menu( 'primary-menu', 'Primary Menu' );
}
add_action('after_setup_theme', 'register_site_menus');

// Register widget areas
function site_widgets_init() {
	// Register main sidebar
	register_sidebar( array(
		'name' => 'Sidebar Widget Area',
		'id' => 'aside-sidebar',
		'description' => 'Widget in this area will be shown in sidebar area on all pages',
		'before_widget' => '<section id="%1$s" class="c-widget c-widget--side %2$s %1$s"><div class="c-widget__content">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="c-widget__heading">',
		'after_title' => '</h3>'
	));

	// Register footer sidebar
	register_sidebar( array(
		'name' => 'Footer Widget Area',
		'id' => 'footer-sidebar',
		'description' => 'Widget in this area will be shown in footer area on all pages',
		'before_widget' => '<section id="%1$s" class="c-widget c-widget--footer %2$s %1$s"><div class="c-widget__content">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="c-widget__heading">',
		'after_title' => '</h3>'
	));
}
add_action( 'widgets_init', 'site_widgets_init' );

// Remove contact from styles from all pages
add_filter('wpcf7_load_js', '__return_false');
add_filter('wpcf7_load_css', '__return_false');

// Load scripts using wp's enqueue
function site_scripts() {
	if ( !is_admin() ) {
		// Remove jquery that wordpress includes
		wp_deregister_script('jquery');
	
		$site_js = (is_localhost()) ? JS . '/dist/site.js' : JS . '/dist/site.min.js';
		$site_css = (is_localhost()) ? CSS . '/screen.dev.css' : CSS . '/screen.min.css';

		// Include all js including jQuery and register with name 'jquery' so other jquery dependable scripts load as well
		$google_map = get_field('google_map', 'option');
		$key = $google_map['google_map_api_key'];
		wp_enqueue_script( 'google_map', "https://maps.googleapis.com/maps/api/js?key=$key");
		//wp_enqueue_script( 'google_polyfill', "https://polyfill.io/v3/polyfill.min.js?features=default");
		wp_enqueue_script('jquery',$site_js, false, $GLOBALS['site_cachebust'], true);
		wp_enqueue_style('site', $site_css, false);
	}
}
add_action('wp_enqueue_scripts', 'site_scripts');

// Boolean to check if the cf7 scripts are required
function load_cf7_scripts() {
	return true;
}

// Check if staging or test enviroment
function is_local_or_staging() {
	if ($_SERVER['SERVER_ADDR'] == '103.27.32.31' || $_SERVER['SERVER_ADDR'] == '127.0.0.1') {
		return true;
	} else {
		return false;
	}
}

// For Debugging
function ra_console_log( $data ){
  echo '<script>';
  echo 'console.log('. json_encode( $data ) .')';
  echo '</script>';
}

// Load cf7 scripts if required
function site_load_cf7_scripts() {
	if ( load_cf7_scripts() ) {
		if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
			wpcf7_enqueue_scripts();
		}
		// Not loading wpcf7 styles at all, if you need wpcf7 styles uncomment below lines
		// if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
		//   wpcf7_enqueue_styles();
		// }
	}
}
add_action( 'wp', 'site_load_cf7_scripts' );

// Allow search to search through pages and posts. Add any custom post type in array to search by them as well.
function site_search($query) {
	if (!is_admin() && $query->is_search) {
		$query->set('post_type', array('page','post'));
	}
	return $query;
}
add_filter('pre_get_posts', 'site_search');

// REMOVE GUTTENBURG
// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);


?>