<?php get_header(); ?>
<?php
    /** Template Name: Terms & Conditions Page */
 ?>
<?php
    get_template_part( 'partials/page','banner');  
?>

<div class="c-terms-privacy" id="content">
    <div class="o-wrapper">
        <div class="c-terms-privacy__container">
            <?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
        </div>   
    </div>
</div>
<div class="space-bottom"></div>

<?php get_footer(); ?>



