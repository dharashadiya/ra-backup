<?php 
    function site_custom_post_type() {
       
     
        register_post_type( 'team-member',
        array(
          'labels' => array(
            'name' => __( 'Our Team Members' ),
            'singular_name' => __( 'Team Member' ),
            'edit_item' => __( 'Edit Team Member' ),
            'add_new_item' => __( 'Add New Team Member' ),
            'new_item' => __( 'New Team Member' ),
            'view_item' => __( 'View Team Member' ),
            'view_items' => __( 'View Team Members' ),
            'search_items' => __( 'Team Members' ),
          ),
          'public' => false,
          'show_ui' => true,
          'hierarchical' => false,
          'capability_type' => 'post',
          'supports' => array( 'title', 'thumbnail' ),
          'rewrite' => array(
            'slug' => 'team-member',
            'with_front' => true
          ),
          'show_in_rest' => false,
          'show_in_nav_menus' => false,
          'menu_icon' => 'dashicons-groups',
        )
      ); 

      register_post_type( 'our-client',
        array(
          'labels' => array(
            'name' => __( 'Our clients' ),
            'singular_name' => __( 'Our client' ),
            'edit_item' => __( 'Edit Our client' ),
            'add_new_item' => __( 'Add New client' ),
            'new_item' => __( 'New client' ),
            'view_item' => __( 'View Our client' ),
            'view_items' => __( 'View clients' ),
            'search_items' => __( 'Our clients' ),
          ),
          'public' => true,
          'show_ui' => true,
          'hierarchical' => false,
          'capability_type' => 'post',
          'supports' => array( 'title', 'thumbnail' ),
          'rewrite' => array(
            'slug' => 'our-client',
            'with_front' => true
          ),
          'show_in_rest' => false,
          'show_in_nav_menus' => false,
          'menu_icon' => 'dashicons-analytics',
        )
      );    

      register_post_type( 'service',
      array(
        'labels' => array(
          'name' => __( 'Services' ),
          'singular_name' => __( 'Service' ),
          'edit_item' => __( 'Edit Service' ),
          'add_new_item' => __( 'Add New Service' ),
          'new_item' => __( 'New Service' ),
          'view_item' => __( 'View Service' ),
          'view_items' => __( 'View Services' ),
          'search_items' => __( 'Services' ),
        ),
        'public' => false,
        'show_ui' => true,
        'hierarchical' => false,
        'capability_type' => 'post',
        'supports' => array( 'title', 'thumbnail' ),
        'rewrite' => array(
          'slug' => 'service',
          'with_front' => true
        ),
        'show_in_rest' => false,
        'show_in_nav_menus' => false,
        'menu_icon' => 'dashicons-media-spreadsheet',
      )
    );    


    }
    add_action( 'init', 'site_custom_post_type' );


    // function site_create_taxonomies() {
    //     register_taxonomy( 'case-study-category', array('case-study'), array(
    //         'hierarchical' => true,
    //         'query_var' => true,
    //         'labels' => array (
    //             'name' => 'Case Study Category'
    //         )
    //     ));
    // }
    
    // add_action( 'init', 'site_create_taxonomies' );
