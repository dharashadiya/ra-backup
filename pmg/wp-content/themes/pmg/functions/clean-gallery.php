<?php
add_theme_support( 'post-thumbnails' );
// Set gallery image thumbnail size
add_image_size( 'page-banner', 2400, 1200, true  ); 
add_image_size( 'director', 1000, 1000, true  ); 
add_image_size( 'team', 1000, 1000, true  ); 
add_image_size( 'services', 1260, 800, true  ); 
add_image_size( 'square', 1260, 1260, true  ); 
add_image_size( 'case-study', 1036, 812, true  ); 

// And then we'll add the custom size that spans the width of the blog to the Gutenberg image dropdown

// Removes default gallery styling
add_theme_support( 'cleaner-gallery' );

?>