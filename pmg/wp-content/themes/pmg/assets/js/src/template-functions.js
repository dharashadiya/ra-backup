export function isMobile() {
	var result;
	result = false;
	if (
		/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
			navigator.userAgent
		)
	) {
		result = true;
	}
	return result;
}
// Open External Link in new tab
export function openLinkNewTab() {
	var base;
	base = window.location.hostname;
	return $("a").each(() => {
		let url = $(this).attr("href");
		if (
			url.indexOf("http") !== -1 &&
			(url.indexOf("javascript:void(0)") < 0 && url.indexOf(base) < 0)
		) {
			$(this).attr("target", "_blank");
			return $(this).attr("rel", "noopener");
		}
	});
}

export function cloudinaryImageCDN() {
	const localhost = site.siteurl.includes(".test") ? true : false;
	imagesLoaded("body", {
		background: true
	}, () => {
		Array.from($("[data-img], [data-bg], [data-src]")).forEach(image => {
			if (localhost) {
				if (image.classList.contains("lazy")) {
					return
				} else if (image.tagName === "IMG") {
					image.src = image.dataset.src;
				} else {
					image.style.backgroundImage = `url(${image.dataset.src})`;
				}
			} else {
				const clientWidth = image.clientWidth;
				const pixelRatio = window.devicePixelRatio || 1.0;
				const imageParams = "w_" + 100 * Math.round((clientWidth * pixelRatio) / 100) + ",c_fill,g_auto,f_auto";
				const baseUrl = "https://res.cloudinary.com/rock-agency/image/fetch";
				let url;
				if (image.classList.contains("lazy")) {
					image.dataset.src = baseUrl + "/" + imageParams + "/" + image.dataset.src;
				} else if (image.tagName === "IMG") {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.src = url;
				} else {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.style.backgroundImage = `url(${url})`;
				}
			}
		});
	});
}

export function superfishMenu() {
	if ($(window).width() > 799) {
		return $(".c-nav").superfish({
			animation: {
				height: "show"
			},
			delay: 250,
			speed: 100
		});
	}
}

export function slider() {
	return $(".c-slider").cycle({
		log: false,
		// swipe: true
		slides: ".c-slide",
		"auto-height": "calc",
		timeout: 5000,
		speed: 600
	});
}

// Fade in animation
export function fadeInUp() {
	return setTimeout(() => {
		return $(".o-animateup").viewportChecker({
			classToAdd: "animated",
			offset: 60
		});
	}, 500);
}

// LazyLoad
export function lazyLoad() {
	return $('.lazyload').Lazy({
		threshold: 400,
		effect: 'fadeIn',
	})
}