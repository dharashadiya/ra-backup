// * this is an example of how to use a hook for the options page
import { useStaticQuery, graphql } from "gatsby"


const useSiteOptions = () => {
    const { allWordpressAcfOptions } = useStaticQuery(
        graphql`
            {
                allWordpressAcfOptions {
                    edges {
                        node {
                            id
                            wordpress_id
                            options {
                                email
                                facebook
                                instagram
                                linkedin
                                location
                                phone
                                help_form {
                                    title
                                    content
                                }
                                brand_logos {
                                    logo {
                                        localFile {
                                            childImageSharp {
                                                fluid(
                                                    quality: 100
                                                    maxWidth: 120
                                                ) {
                                                    ...GatsbyImageSharpFluid_withWebp
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `
    )
    return allWordpressAcfOptions.edges[0].node.options
}

export default useSiteOptions
