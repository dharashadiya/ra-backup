import { generateMedia } from "styled-media-query"
const colors = {
    primary: "#28A8D0",
    secondary: "#1E2054",
    tertiary: "#FBAE17",
    content: "#101338",
}

const media = generateMedia({
    mobile: "375px",
	mobileLandscape: "600px",
	tablet: "770px",
	tabletWide: "1000px",
	laptop: "1280px",
	desktop: "1440px",
	wideScreen: "1600px",
})



const fontSizes = {
    body: "16px",
    h1: "85px",
    h2: "50px",
    h3: "38px",
    h4: "22px"
}

const lineHeights = {
    body: "24px",
    h1: "85px",
    h2: "60px",
    h3: "45px",
    h4: "26px",
}

const fontFamily = {
    bodyLight: "Brown Light",
    body: "Brown Regular",
    heading: "Brown Bold",
    link: "sans serif",
}

const theme = {
    colors,
    media,
    fontSizes,
    fontFamily,
    lineHeights
}

export default theme