import React, { useState } from "react"
import GoogleMapReact from "google-map-react"
import MapMarker from "../assets/svg/map-marker.inline.svg"
import { Waypoint } from "react-waypoint"
import { animated, useSpring, config } from "react-spring"

const defaultProps = {
    center: {
        lat: -37.84,
        lng: 144.94,
    },
    zoom: 15,
}

const AnimatedMarker = animated(MapMarker)

const GoogleMap = () => {
    const [isAnimated, toggleIsAnimated] = useState(false)

    const spring = useSpring({
        config: config.stiff,
        opacity: isAnimated ? 1 : 0,
        transform: isAnimated
            ? `translate3d(0, 0 ,0)`
            : `translate3d(0, -100%, 0)`,
    })

    return (
        <div style={{ height: "396px", width: "100%" }}>
            <Waypoint
                bottomOffset="22%"
                onEnter={() => {
                    if (!isAnimated) toggleIsAnimated(true)
                }}
            />
            <GoogleMapReact
                bootstrapURLKeys={{
                    key: "AIzaSyDGvKV7yw_MbFDA64c94p5XBC6_VAt4Dxo",
                }}
                defaultCenter={defaultProps.center}
                defaultZoom={defaultProps.zoom}
            >
                <AnimatedMarker style={spring} lat={-37.84} lng={144.94} />
            </GoogleMapReact>
        </div>
    )
}

export default GoogleMap
