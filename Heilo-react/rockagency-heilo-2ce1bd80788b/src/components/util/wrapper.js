import React from "react";
import '../../styles/all.scss'


const Wrapper = ({size, children, className}) => (
    <div className={`o-wrapper o-wrapper--${size} ${className}`}>
        {children}
    </div>
)

export default Wrapper