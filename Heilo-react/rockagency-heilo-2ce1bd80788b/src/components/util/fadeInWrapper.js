import React, { useState } from "react"
import { Waypoint } from "react-waypoint"
import { animated, useTrail, config } from "react-spring"

const FadeInWrapper = ({ children }) => {
    console.log(children)
    const [isAnimated, toggleIsAnimated] = useState(false)

    const trail = useTrail(children.length, {
        config: config.stiff,
        opacity: isAnimated ? 1 : 0,
        transform: isAnimated
            ? `translate3d(0, 0 ,0)`
            : `translate3d(0, 15%,0)`,
    })

    return (
        <>
            <Waypoint
                bottomOffset="15%"
                onEnter={() => {
                    if (!isAnimated) toggleIsAnimated(true)
                }}
            />
            {trail.map((animation, index) => (
                <animated.div key={index} style={animation}>
                    {children[index]}
                </animated.div>
            ))}
        </>
    )
}

export default FadeInWrapper