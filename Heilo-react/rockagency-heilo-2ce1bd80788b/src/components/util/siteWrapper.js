/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import styled, { createGlobalStyle } from "styled-components"
import Footer from "../footer"
import Header from "../header"
import { ThemeProvider } from "styled-components"
import theme from "../../styles/theme"

const GlobalStyles = createGlobalStyle`
    html, 
    body, 
    #___gatsby, 
    #gatsby-focus-wrapper {
        height: 100%;
    }
`

const SiteFlex = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
`

const ContentWrap = styled.div`
    flex: 1 0 auto;
`

const FooterWrap = styled.div`
    flex-shrink: 0;
`

const SiteWrapper = ({ children, links }) => (
    <ThemeProvider theme={theme}>
        <SiteFlex>
            <GlobalStyles />
            <ContentWrap>
                <Header links={links}></Header>
                {children}
            </ContentWrap>
            <FooterWrap>
                <Footer></Footer>
            </FooterWrap>
        </SiteFlex>
    </ThemeProvider>
)

export default SiteWrapper
