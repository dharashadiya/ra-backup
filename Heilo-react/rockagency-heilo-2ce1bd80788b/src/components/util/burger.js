import React, { useState } from "react"
import "../../styles/all.scss"
import styled from "styled-components"


const BurgerWrap = styled.div`
    position: relative;
    max-width: 36px;
    max-height: 36px;
    width: 100%;
    height: 100%;
`
const StyledButton = styled.div`
    position: absolute;
    display: block;
    width: 100%;
    height: 2px;
    background: #FFF;
    border-radius: 1px;
    transform: translateY(50%);
    top: 50%;

    &::before,
    &::after {
        content: " ";
        display: block;
        width: 100%;
        height: 2px;
        background: #FFF;
        position: absolute;
        left: 0;
        transition: top 0.2s 0.2s, transform 0.2s;
    }

    &::before {
        top: -10px;
    }

    &::after {
        top: 10px;
    }

    &.is-open {
        background: transparent;

        &::before,
        &::after {
            top: 0;
            transform: rotate(45deg);
            transition: top 0.2s, transform 0.2s 0.2s;
        }

        &::after {
            transform: rotate(-45deg);
        }
    }
`

const Burger = ({ onClick, className }) => {
    const [open, setOpen] = useState(false);

    const handleClick = () => {
        setOpen(!open)
        onClick ? onClick() : ''
    }

    return (
        <BurgerWrap onClick={() => handleClick()}>
            <StyledButton className={`${open ? 'is-open' : 'is-closed'} ${className}`}></StyledButton>
        </BurgerWrap>
    )
}

export default Burger
