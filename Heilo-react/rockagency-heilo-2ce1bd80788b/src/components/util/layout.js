import React, { useState } from "react"
import '../../styles/all.scss'
import { Waypoint } from "react-waypoint"
import { animated, useTrail, config } from "react-spring"
const Layout = ({children, size, itemSize, moduleOption, className, fadeInUp, ...props}) => {
    const [isAnimated, toggleIsAnimated] = useState(fadeInUp ? false : true)

    const trail = useTrail(children.length, {
        config: config.stiff,
        opacity: isAnimated ? 1 : 0,
        transform: isAnimated
            ? `translate3d(0, 0 ,0)`
            : `translate3d(0, 15%,0)`,
    })
    const moduleClass = moduleOption ? 'o-module__item' : ''
    const layoutItems = React.Children.map(children , (child, index) => {
        const defaultItemSize = itemSize === undefined ? '' : itemSize
        return (
            <animated.div
                style={trail[index]}
                key={index}
                className={`o-layout__item ${
                    child.props.itemSize === undefined
                        ? defaultItemSize
                        : child.props.itemSize
                } ${moduleClass === undefined ? "" : moduleClass}`}
            >
                {child}
            </animated.div>
        )
    })
    
    return (
        <>
            <Waypoint
                bottomOffset="15%"
                onEnter={() => {
                    if (!isAnimated) toggleIsAnimated(true)
                }}
            />
            <section
                className={`o-layout 
                ${size ? `o-layout--${size}` : ""} 
                ${moduleOption ? `o-module` : ""}
                ${className}`}
            >
                {layoutItems}
            </section>
        </>
    )
}

export default Layout