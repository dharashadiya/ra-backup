import React, { useState } from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import styled from "styled-components"
import Img from "gatsby-image"
import Wrapper from "../components/util/wrapper"
import Layout from './util/layout';
import { Waypoint } from "react-waypoint"
import { animated, useTrail, config } from "react-spring"

const AutomationImg = styled(Img)`
    opacity: ${props => props.current ? "1" : ".75"};
    transition: .25s;
    max-width: 100px;
    width: 100%;
`;

const SingleAutomationType = styled(Link)`
	max-width: 96px;
    text-decoration: none;
    display: block;
    margin: 0 auto;
    text-align: center;

	p {
		color: #FFF;
		font-size: 14px;
		line-height: 16px;
		margin: 0 auto;
    }
    
    &:hover {
        ${AutomationImg} {
            opacity: 1;
        }
    }
`;

const AutomationTypesSection = styled.section`
    text-align: center;
    padding-top: 48px;
    padding-bottom: 60px;
    h4 {
        text-transform: uppercase;
		margin-bottom: 26px;
        color: #fff;
    }
    background-color: #0098b4;
`

const AutomationList = (props) => {
    const { allWordpressWpAutomationTypes } = useStaticQuery(graphql`
        {
            allWordpressWpAutomationTypes(sort: { fields: title }) {
                edges {
                    node {
                        slug
                        title
                        path
                        acf {
                            icon {
                                localFile {
                                    childImageSharp {
                                        fluid(maxWidth: 100) {
                                            ...GatsbyImageSharpFluid_withWebp
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    `)
    const automationTypes = allWordpressWpAutomationTypes.edges.map(post => (
        <SingleAutomationType key={post.node.slug} to={post.node.path}>
            <AutomationImg
                current={props.current === post.node.title}
                fluid={post.node.acf.icon.localFile.childImageSharp.fluid}
            ></AutomationImg>
            <p dangerouslySetInnerHTML={{ __html: post.node.title }}></p>
        </SingleAutomationType>
    ))

    const [isAnimated, toggleIsAnimated] = useState(false)


    const trail = useTrail(automationTypes.length, {
        transform: isAnimated
            ? `translate3d(0, 0 ,0)`
            : `translate3d(0, 50%,0)`,
        config: config.stiff
    })

    return (
        <AutomationTypesSection>
            <Waypoint
                bottomOffset="15%"
                onEnter={() => {
                    if (!isAnimated) toggleIsAnimated(true)
                }}
            />
            <Wrapper>
                <h4>Automation in all its forms</h4>
                <Layout itemSize="u-1/6@tabletWide u-1/3">
                    {trail.map((animation, index) => (
                        <animated.div
                            key={index}
                            style={animation}
                        >
                            {automationTypes[index]}
                        </animated.div>
                    ))}
                </Layout>
            </Wrapper>
        </AutomationTypesSection>
    )
}
 


export default AutomationList;