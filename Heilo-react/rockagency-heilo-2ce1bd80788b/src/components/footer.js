import React from "react"
import styled from 'styled-components'
import { Link, graphql, useStaticQuery } from 'gatsby'
import Wrapper from '../components/util/wrapper'
import Logo from "../assets/svg/logo.inline.svg"
import Facebook from "../assets/svg/facebook.inline.svg"
import Instagram from "../assets/svg/instagram.inline.svg"
import Linkedin from "../assets/svg/linkedin.inline.svg"
import Arrow from "../assets/svg/arrow.inline.svg"
import Newsletter from "./newsletter"
import LogoSlider from "./logoSlider"
import TopArrow from "../assets/images/top-arrow@2x.png"

const StyledArrow = styled.img`
    display: inline-block;
    max-width: 10px;
    margin-left: 9px;
`

const LogoLink = styled(Link)`
    &&& {
        opacity: 1;
    }
`;

const StyledFB = styled(Facebook)`
	width: 15px;
`
const StyledInstagram = styled(Instagram)`
	width: 15px;
`

const StyledLinkedin = styled(Linkedin)`
    width: 14px;
`
const StyledFooter = styled.footer`
    position: relative;
    padding-top: 44px;
    background-color: ${({ theme }) => theme.colors.content};

    ${({ theme }) => theme.media.lessThan("tablet")`
        padding-top: 78px;
    `};

    h5 {
        text-transform: uppercase;
        font-family: ${({ theme }) => theme.fontFamily.heading};
        opacity: 0.85;
        font-size: 14px;
        line-height: 16px;
        color: #fff;
    }

    a {
        font-size: 14px;
        line-height: 25px;
        display: block;
        text-decoration: none;
        color: #fff;
        opacity: 0.8;
        transition: .25s;
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
        ${({ theme }) => theme.media.lessThan("tablet")`
            font-size: 11px;
        `};

        &:hover {
            opacity: 1;
        }
    }
`

const Socials = styled.div`

    ${({ theme }) => theme.media.lessThan('tablet')`
        margin-top : 25px;
        display: flex;
        justify-content: space-between;
    `};


    a {
		margin-left: 12px;
		display: inline-block;
		
		&:first-child {
			margin-left: 0;
		}
    }
`

const Col = styled.div`
	max-width: 162px;

	:first-of-type {
		> div {
			margin-bottom: 25px;
		}
    }
    
    :last-of-type {
        max-width: 182px;
    }

    ${({ theme }) => theme.media.lessThan("1000px")`
        :nth-child(4) {
            display: none;
        }
    `};

    ${({ theme }) => theme.media.lessThan("790px")`
        :nth-child(3) {
            display: none;
        }
    `};
`;

const FooterInnerWrap = styled.footer`
    display: flex;
    justify-content: space-between;
    width: 100%;

    ${({ theme }) => theme.media.lessThan("tablet")`
        display: none;
    `};
`

const StyledLogo = styled(Logo)`
    width: 144px;
`

const FooterLower = styled.div`
    display: flex;
    justify-content: space-between;
    padding-bottom: 29px;
    padding-top: 60px;
    font-size: 12px;

    ${({ theme }) => theme.media.lessThan("tablet")`
        padding-top: 36px;
        flex-direction: column;
        text-align: center;
        font-size: 11px;
    `};

    span {
        opacity: 0.6;
        color: #fff;

        &.bold {
            font-family: ${({ theme }) => theme.fontFamily.body};
        }
    }

    a {
        font-size: 12px;
        opacity: 0.6;
        display: inline-block;
        transition: .25s;
        font-family: ${({ theme }) => theme.fontFamily.body};

        &:hover {
            opacity: 1;
        }

        ${({ theme }) => theme.media.lessThan("tablet")`
            font-size: 11px;
        `};
    }
`

const ToTop = styled.div`
    ${({ theme }) => theme.media.lessThan("tablet")`
        position: absolute;
        top: 30px;
        right: 50%;
        transform: translateX(50%);
        font-size: 11px;
    `};
    a {
        text-decoration: underline;
    }
`

const MobileFooterRight = styled.div`
    max-width: 140px;
    margin-left: 19px;
    margin-top: 24px;

    a {
        font-size: 12px;
        line-height: 25px;
    }
`

const MobileFooter = styled.div`
    display: flex;
    justify-content: center;
    ${({ theme }) => theme.media.greaterThan("tablet")`
        display: none;
    `};
`

const MobileFooterLeft = styled.div`
    margin-right: 19px;
`

const Footer = () => {
    const date = new Date()
	const {
        allWordpressAcfOptions,
        allWordpressWpSmartLives,
        allWordpressWpAutomationTypes,
    } = useStaticQuery(graphql`
        {
            allWordpressAcfOptions {
                edges {
                    node {
                        id
                        wordpress_id
                        options {
                            email
                            facebook
                            instagram
                            linkedin
                            location
                            phone
                        }
                    }
                }
            }
            allWordpressWpSmartLives(sort: { fields: acf___menu_order }) {
                edges {
                    node {
                        title
                        path
                    }
                }
            }
            allWordpressWpAutomationTypes(sort: { fields: acf___menu_order }) {
                edges {
                    node {
                        title
                        path
                    }
                }
            }
        }
    `)
	const linkFactory = item => (
        <Link
            key={item.node.title}
            to={item.node.path}
            dangerouslySetInnerHTML={{ __html: item.node.title }}
        >
        </Link>
    )
	const contact = allWordpressAcfOptions.edges[0].node.options
	const automationTypes = allWordpressWpAutomationTypes.edges.map(item =>
        linkFactory(item)
    )
	const smartLives = allWordpressWpSmartLives.edges.map(item => linkFactory(item))
    return (
        <>
            <LogoSlider></LogoSlider>
            <StyledFooter>
                <Wrapper>
                    <FooterInnerWrap>
                        <Col>
                            <h5>
                                Contact
                                <br />
                                Heilo
                            </h5>
                            <div>
                                <a
                                    target="_blank"
                                    title="Google maps Address"
                                    href={
                                        "https://www.google.com/maps?q=" +
                                        contact.location
                                    }
                                >
                                    {contact.location}
                                </a>
                            </div>
                            <div>
                                <a
                                    title="Heilo Email Address"
                                    href={"mailto:" + contact.email}
                                >
                                    {contact.email}
                                </a>
                                <a
                                    title="Explore Careers Phone Number"
                                    href={"tel:" + contact.phone}
                                >
                                    {contact.phone}
                                </a>
                            </div>
                            <Socials>
                                <a href={contact.facebook} target="_blank">
                                    <StyledFB></StyledFB>
                                </a>
                                <a href={contact.instagram} target="_blank">
                                    <StyledInstagram></StyledInstagram>
                                </a>
                                <a href={contact.linkedin} target="_blank">
                                    <StyledLinkedin></StyledLinkedin>
                                </a>
                            </Socials>
                        </Col>
                        <Col>
                            <h5>
                                Automation
                                <br />
                                Types
                            </h5>
                            {automationTypes}
                        </Col>
                        <Col>
                            <h5>
                                Smart
                                <br />
                                lives
                            </h5>
                            {smartLives}
                        </Col>
                        <Col>
                            <h5>
                                Join our
                                <br />
                                Newsletter
                            </h5>
                            <Newsletter></Newsletter>
                        </Col>
                        <LogoLink to="/">
                            <StyledLogo></StyledLogo>
                        </LogoLink>
                    </FooterInnerWrap>
                    <MobileFooter>
                        <MobileFooterLeft>
                            <LogoLink to="/">
                                <Logo />
                            </LogoLink>
                            <Socials>
                                <a href={contact.facebook} target="_blank">
                                    <StyledFB></StyledFB>
                                </a>
                                <a href={contact.instagram} target="_blank">
                                    <StyledInstagram></StyledInstagram>
                                </a>
                                <a href={contact.linkedin} target="_blank">
                                    <StyledLinkedin></StyledLinkedin>
                                </a>
                            </Socials>
                        </MobileFooterLeft>
                        <MobileFooterRight>
                            <div>
                                <a
                                    target="_blank"
                                    title="Google maps Address"
                                    href={
                                        "https://www.google.com/maps?q=" +
                                        contact.location
                                    }
                                >
                                    {contact.location}
                                </a>
                            </div>
                            <div>
                                <a
                                    title="Heilo Email Address"
                                    href={"mailto:" + contact.email}
                                >
                                    {contact.email}
                                </a>
                                <a
                                    title="Explore Careers Phone Number"
                                    href={"tel:" + contact.phone}
                                >
                                    {contact.phone}
                                </a>
                            </div>
                        </MobileFooterRight>
                    </MobileFooter>
                    <FooterLower>
                        <ToTop>
                            <a href="#top">
                                Return to Top
                                <StyledArrow src={TopArrow}></StyledArrow>
                            </a>
                        </ToTop>
                        <div>
                            <Link to={"/terms-conditions/"}>
                                Terms&nbsp;&amp;&nbsp;Conditions
                            </Link>
                            <span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
                            <Link to={"/privacy-policy/"}>Privacy</Link>
                            <span>
                                &nbsp;&nbsp;|&nbsp;&nbsp;Built&nbsp;by&nbsp;
                            </span>
                            <a
                                target="_blank"
                                href="https://rockagency.com.au/"
                            >
                                Rock&nbsp;Agency
                            </a>
                        </div>
                        <div>
                            <span className="bold">
                                &copy;&nbsp;Copyright&nbsp;Heilo&nbsp;
                                {date.getFullYear()}.
                            </span>
                        </div>
                    </FooterLower>
                </Wrapper>
            </StyledFooter>
        </>
    )
}

export default Footer	
