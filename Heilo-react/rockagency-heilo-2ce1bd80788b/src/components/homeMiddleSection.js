import React, { useState } from 'react'
import Wrapper from "./util/wrapper"
import Layout from "./util/layout"
import styled from "styled-components"
import CallLink from "./callLink"
import { Waypoint } from "react-waypoint"
import { animated, useTrail, config } from "react-spring"


const MiddleSectionWrap = styled.section`
    padding-top: 85px;
    padding-bottom: 32px;
    ${({ theme }) => theme.media.lessThan("tablet")`
        text-align: center;
        padding-bottom: 24px;
        padding-top: 50px;
    `};

    h3 {
        text-align: right;

        ${({ theme }) => theme.media.lessThan("tablet")`
            text-align: center;
        `};
    }

    h4 {
        text-transform: uppercase;
    }

    p {
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
        font-size: 16px;
        line-height: 24px;
    }
`

const HomeMiddleSection = ({heading, content}) => {
    const [isAnimated, toggleIsAnimated] = useState(false)

    const contentItems = [
        <h3>{heading}</h3>,
        content[0],
        <>
            {content[1]}
            <CallLink call></CallLink>
        </>,
    ]

    const trail = useTrail(contentItems.length, {
        opacity: isAnimated ? 1 : 0,
        transform: isAnimated ? `translate3d(0, 0 ,0)` : `translate3d(0, 15%,0)`,
    })

    return (
        <MiddleSectionWrap>
            <Waypoint
                bottomOffset="15%"
                onEnter={() => {
                    if (!isAnimated) toggleIsAnimated(true)
                }}
            />
            <Wrapper>
                <Layout size="home-content" itemSize="u-1/3@tabletWide">
                    {trail.map((animation, index) => (
                        <animated.div key={index} style={animation}>
                            {contentItems[index]}
                        </animated.div>
                    ))}
                </Layout>
            </Wrapper>
        </MiddleSectionWrap>
    )
}

export default HomeMiddleSection