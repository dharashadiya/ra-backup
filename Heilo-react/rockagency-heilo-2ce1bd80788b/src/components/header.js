import React, { useState, useEffect, useRef } from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Logo from "../assets/svg/logo.inline.svg"
import Wrapper from "../components/util/wrapper"
import styled from "styled-components"
import { useMainMenu } from "../hooks/useMainMenu"
import SideBar from "../components/sidebar"

const StyledHeader = styled.header`
	position: absolute;
	top: 0;
	width: 100%;
	transition: 0.4s;
	z-index: 10;
	&.nav-up {
		top: -120px;
	}
`

const HeaderInnerWrap = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    padding: 20px 0;
`

const NavItem = styled(Link)`
	color: #000;
	display: inline-block;
	margin: 8px 16px;
	text-decoration: none;
`

const StyledLogo = styled(Logo)`
	width: 100px;
`


const Header = () => {
	const [path, setPath] = useState("/")
	const [height, setHeight] = useState(0)
	const [mobileMenuOpen, setMobileMenuOpen] = useState(false)

	useEffect(() => {
		setHeight(document.querySelector('#headerWrap').clientHeight)
	})

	useEffect(() => {
		setPath(window.location.pathname)
	})
	
	const openMobileMenu = () => {
		setMobileMenuOpen(!mobileMenuOpen)
	}

	const links = useMainMenu().items.map((item) => (
		<NavItem to={`/${item.object_slug}`} key={item.title}> 
			{item.title}
		</NavItem>
	))
	
	return (
		<StyledHeader id="header">
			<Wrapper>
				<HeaderInnerWrap id="headerWrap">
					<Link to="/">
						<StyledLogo></StyledLogo>
					</Link>
					<SideBar></SideBar>
				</HeaderInnerWrap>
			</Wrapper>
		</StyledHeader>
    )
}

export default Header
