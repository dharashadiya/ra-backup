import React, { useState } from "react"
import Wrapper from "./util/wrapper"
import styled from "styled-components"
import { Waypoint } from "react-waypoint"
import { animated, useTrail, config } from "react-spring"
import BackgroundImage from "gatsby-background-image"

const CustomerSectionWrap = styled.section`
    background-color: #f3fafd;
    padding: 80px 0;
    text-align: center;

    ${({ theme }) => theme.media.lessThan("tablet")`
        padding-top: 52px;
        padding-bottom: 58px;
    `};

    h3,
    p {
        max-width: 604px;
        margin: 0 auto;
    }

    p {
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
    }

    h3 {
        margin-bottom: 22px;
    }
`

const CustomerCol = styled(animated.div)`
    display: flex;
    flex-direction: column;
    align-items: ${props => (props.align ? props.align : "")};

    &:first-of-type {
        margin-right: 22px;

        > :first-child {
            margin-top: 58px;
            margin-bottom: 18px;
        }
    }

    &:nth-child(2) {
        > :first-child {
            margin-bottom: 23px;
        }
    }

    &:last-of-type {
        margin-left: 26px;

        > :first-child {
            margin-top: 44px;
            margin-bottom: 26px;
        }
    }

    ${({ theme }) => theme.media.lessThan("tablet")`
        align-items: center;

        &:first-of-type {
            margin-right: 0;

            > :first-child {
                margin-top: 16px;
                margin-bottom: 16px;
            }

            > :last-child {
                margin-bottom: 0;
            }
        }
        
        &:nth-child(2) {
            > :first-child {
                margin-top: 16px;
                margin-bottom: 16px;
            }

            > :last-child {
                margin-bottom: 0;
            }
        }

        &:last-of-type {
            margin-left: 0;

            > :first-child {
                margin-top: 16px;
                margin-bottom: 16px;
            }

            > :last-child {
                margin-bottom: 0;
            }
        }
    `};
`

const Person = styled(BackgroundImage)`
    width: ${props => personDimensions.desktop[props.index].width};
    height: ${props => personDimensions.desktop[props.index].height};
    position: relative;
    box-shadow: 0px 3px 6px #00000029;
    transition: 0.25s;

    &:hover {
        transform: scale(1.08);
    }

    ${({ theme }) => theme.media.lessThan("tablet")`
        width: 222px;
        height: ${props => personDimensions.tablet[props.index].height};
        margin-bottom: 16px;
    `};
    ${({ theme }) => theme.media.between("tablet", "tabletWide")`
        width: 220px;
    `};

    p {
        width: fit-content;
        position: absolute;
        bottom: 18px;
        left: 50%;
        max-width: 86%;
        padding: 9px 18px;
        text-transform: uppercase;
        font-family: ${({ theme }) => theme.fontFamily.heading};
        transform: translateX(-50%);
        color: ${({ theme }) => theme.colors.primary};
        background-color: #fff;
        font-size: 14px;
        line-height: 16px;
        ${({ theme }) => theme.media.lessThan("tabletWide")`
        font-size: 12px;
        padding: 8px 17px;
    `};
    }
`

const CustomerWrap = styled.div`
    margin-top: 62px;
    display: flex;
    justify-content: center;

    ${({ theme }) => theme.media.lessThan("tablet")`
        flex-direction: column;
        margin-top: 10px;
    `};
`

const personDimensions = {
    desktop: [
        {
            width: "319px",
            height: "270px",
        },
        {
            width: "314px",
            height: "345px",
        },
        {
            width: "319px",
            height: "262px",
        },
        {
            width: "255px",
            height: "239px",
        },
        {
            width: "313px",
            height: "267px",
        },
        {
            width: "320px",
            height: "270px",
        },
        {
            width: "254px",
            height: "238px",
        },
    ],

    tablet: [
        {
            width: "222px",
            height: "187px",
        },
        {
            width: "222px",
            height: "245px",
        },
        {
            width: "222px",
            height: "182px",
        },
        {
            width: "255px",
            height: "208px",
        },
        {
            width: "313px",
            height: "189px",
        },
        {
            width: "320px",
            height: "270px",
        },
        {
            width: "254px",
            height: "208px",
        },
    ],
}

const CustomerSection = ({heading, content, peopleData}) => {
    const [isAnimated, toggleIsAnimated] = useState(false)

    const people = peopleData.map((person, index) => (
        <Person
            index={index}
            fluid={person.image.localFile.childImageSharp.fluid}
        >
            <p>{person.name}</p>
        </Person>
    ))

    const customerCols = [
        <>
            {people[0]}
            {people[3]}
        </>,
        <>
            {people[1]}
            {people[4]}
        </>,
        <>
            {people[2]}
            {people[5]}
        </>,
    ]

    const trail = useTrail(customerCols.length, {
        opacity: isAnimated ? 1 : 0,
        transform: isAnimated
            ? `translate3d(0, 0 ,0)`
            : `translate3d(0, 35%,0)`,
    })
    return (
        <CustomerSectionWrap>
            <Waypoint
                bottomOffset="22%"
                onEnter={() => {
                    if (!isAnimated) toggleIsAnimated(true)
                }}
            />
            <Wrapper>
                <h3>{heading}</h3>
                <p>{content}</p>
                <CustomerWrap>
                    {trail.map((animation, index) => (
                        <CustomerCol key={index} align={index === 0 ? "flex-end" : "" } style={animation}>
                            {customerCols[index]}
                        </CustomerCol>
                    ))}
                </CustomerWrap>
            </Wrapper>
        </CustomerSectionWrap>
    )
}

export default CustomerSection