import React, { useState, useEffect } from "react"
import useForm from "react-hook-form"
import * as qs from "query-string"
import axios from "axios"
import Button from "../components/util/button"
import styled from "styled-components"

const FullButton = styled(Button)`
    width: 100%;
`;

const StyledInput = styled.input`
    &&& { 
        display: inline-block;
        padding: 2px 11px;
        margin-bottom: 14px;
        width: 100%;
        max-width: 300px;
        &::placeholder {
            font-weight: 200;
            opacity: .75;
        }
    }

`

const Message = styled.div`
    margin-top: 12px;
    font-size: 12px;
    color: ${({ theme }) => theme.colors.tertiary};
`;


const Newsletter = (props) => {
    const { register, handleSubmit, watch, errors } = useForm()
    const [path, setPath] = useState("/")
    const [message, setMessage] = useState("")
    const allFields = watch()

    useEffect(() => {
        setPath(window.location.pathname)
    })

    const onSubmit = async data => {
        const axiosOptions = {
            url: path,
            method: "post",
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
            data: qs.stringify(data),
        }
        let result
        try {
            result = await axios(axiosOptions)
            if (result.status === 200) {
                setMessage("Thank you for subscribing")
            }
        } catch (errors) {
            console.log(errors)
            setMessage("Sorry your message could not be sent")
        }
    }

    return (
        <form
            name="newsletter"
            className={props.className}
            onSubmit={handleSubmit(onSubmit)}
            data-netlify="true"
            data-netlify-honeypot="bot-field"
        >
            <input type="hidden" ref={register} name="bot-field" />
            <input
                type="hidden"
                ref={register({ required: true })}
                name="form-name"
                value="newsletter"
                required
            />
            <StyledInput
                name="name"
                type="text"
                className="dark"
                placeholder="Your name"
                ref={register({ required: true })}
                required
                />
            <StyledInput
                name="email"
                type="email"
                className="dark"
                placeholder="Your Email"
                ref={register({ required: true })}
                required
            />
            <FullButton mod={allFields.email ? null : "disabled"}>Subscribe</FullButton>
            <Message>
                {message}
            </Message>
        </form>
    )
}

export default Newsletter
