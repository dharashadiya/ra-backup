import React from 'react';
import useSiteOptions from "../hooks/useSiteOptions"
import styled from "styled-components"
import Phone from "../assets/svg/phone.inline.svg"

const StyledAnchor = styled.a`
    display: inline-flex;
    align-items:center;
    color: ${({ theme }) => theme.colors.tertiary};
    font-family: ${({ theme }) => theme.fontFamily.heading};
    text-transform: uppercase;
    text-decoration: none;
    font-size: 22px;
    ${({ theme }) => theme.media.lessThan('mobile')`
        font-size: 20px;
    `};

    svg {
        margin-right: 15px;
        max-width: 21px;
    }
`;

const CallLink = (props) => {
    const options = useSiteOptions()
    return (
        <StyledAnchor href={`tel:${options.phone}`}>
            {props.call &&
                <>
                    <Phone></Phone><span>call:&nbsp;</span>
                </>
            }
            {options.phone}
        </StyledAnchor>
    )
}

export default CallLink