import React, { useState } from "react"
import Wrapper from "./util/wrapper"
import styled from "styled-components"
import CallLink from "./callLink"
import { Waypoint } from "react-waypoint"
import { animated, useSpring, config } from "react-spring"
import BackgroundImage from "gatsby-background-image"
import Button from "../components/util/button"
import { Link } from "gatsby"

const AutomationSection = styled(animated.section)`
    position: relative;
    background: rgba(255, 255, 255, 0.9);
    max-width: 349px;
    margin-left: auto;
    padding-top: 36px;
    padding-left: 35px;
    padding-right: 38px;
    padding-bottom: 44px;

    ${({ theme }) => theme.media.lessThan("tablet")`
        text-align: center;
        padding-bottom: 24px;
        padding: 27px 14px 14px 14px;
    `};

    h3 {
        max-width: 256px;
        margin-bottom: 15px;
        ${({ theme }) => theme.media.lessThan("tablet")`
            margin-bottom: 5px;
            max-width: 100%;
        `};
    }

    p {
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
        ${({ theme }) => theme.media.lessThan("tablet")`
            margin: 0 auto 8px;
            max-width: 243px;
        `};
    }
`

const AutomationBG = styled(BackgroundImage)`
    padding-top: 80px;
    padding-bottom: 84px;
    overflow: hidden;

    ${({ theme }) => theme.media.lessThan("tablet")`
        padding-top: 30px;
        padding-bottom: 28px;
    `};
`

const HomeAutomationSection = ({ heading, content, button, image }) => {
    const [isAnimated, toggleIsAnimated] = useState(false)

    const contentItems = [
        <h3>{heading}</h3>,
        content[0],
        <>
            {content[1]}
            <CallLink call></CallLink>
        </>,
    ]

    const spring = useSpring( {
        config: config.stiff,
        opacity: isAnimated ? 1 : 0,
        transform: isAnimated
            ? `translate3d(0, 0 ,0)`
            : `translate3d(100%, 0, 0)`,
    })

    return (
        <>
            <Waypoint
                bottomOffset="22%"
                onEnter={() => {
                    if (!isAnimated) toggleIsAnimated(true)
                }}
            />
            <AutomationBG fluid={image}>
                <Wrapper>
                    <AutomationSection style={spring}>
                        <h3>{heading}</h3>
                        <p>{content}</p>
                        <Button>
                            <Link to={button.url}>{button.title}</Link>
                        </Button>
                    </AutomationSection>
                </Wrapper>
            </AutomationBG>
        </>
    )
}

export default HomeAutomationSection
