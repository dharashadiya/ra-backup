import React, { useState, useEffect } from "react"
import useForm from 'react-hook-form'
import * as qs from "query-string"
import axios from 'axios'
import Layout from './util/layout'
import Button from './util/button'
import styled from "styled-components"
import useSiteOptions from "../hooks/useSiteOptions"
import CallLink from "./callLink"

const FieldWrapper = styled.div`
    position: relative;
    margin-top: 24px;
    ${({ theme }) => theme.media.lessThan("tablet")`
        text-align: center;
        margin-top: 0;
    `};
`

const StyledTextArea = styled.textarea`
    padding-left: 12px;
    padding-right: 12px;
    ${({ theme }) => theme.media.lessThan("tablet")`
        margin: 0 auto;
    `};
`

const ContentWrap = styled.div`
    text-align: center;
    max-width: 881px;
    margin: 0 auto;

    ${({ theme }) => theme.media.lessThan("tablet")`
        h2 {
            font-size: 26px;
        }
    `};

    p {
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
    }

    a {
        color: ${({ theme }) => theme.colors.tertiary};
        text-decoration: none;
        font-size: 22px;
        line-height: 30px;

        ${({ theme }) => theme.media.lessThan("tablet")`
            display: flex;
            justify-content: center;
            margin: 12px 0 24px;
        `};
    }
`

const StyledButton = styled(Button)`
    margin-top: 8px;
    max-width: 300px;
    width: 100%;
    margin-left: 35px;

    ${({ theme }) => theme.media.lessThan("tablet")`
        margin-left: 0;
    `};
`

const StyledInput = styled.input`
    display: block;
    padding: 8px 12px;
    width: 100%;
    max-width: 300px;
    margin-bottom: 10px;
    margin-left: 35px;

    ${({ theme }) => theme.media.lessThan('tablet')`
        margin-left: 0;
        max-width: 100%;
    `};
`

const Message = styled.div`
    margin-top: 12px;
    font-size: 12px;
    color: ${({ theme }) => theme.colors.tertiary};
`

const HelpForm = (props) => {
	const { register, handleSubmit, watch, errors } = useForm()
    const [path, setPath] = useState('/')
    const [width, setWidth] = useState(0)
    const [message, setMessage] = useState(
        ""
    )
    const allFields = watch()

    const options = useSiteOptions()

	useEffect(() => {
		setPath(window.location.pathname)
    })
    
	useEffect(() => {
		setWidth(window.innerWidth)
	})
  
    const onSubmit = async data => {
		const axiosOptions = {
			url: path,
			method: "post",
			headers: { "Content-Type": "application/x-www-form-urlencoded" },
			data: qs.stringify(data),
		}
		let result 
		try {
			result = await axios(axiosOptions)
            if (result.status === 200) {
                setMessage("Thank you your message has been sent")
            }
		} catch (errors) {
            console.log(errors)
            setMessage("Sorry your message could not be sent")
		}
    }

	return (
        <div>
            <ContentWrap>
                <h2>{options.help_form.title}</h2>
                <p>
                    <span
                        dangerouslySetInnerHTML={{
                            __html: options.help_form.content.replace(
                                "AUTOMATION",
                                props.automationType.toLowerCase()
                            ),
                        }}
                    ></span>
                    &nbsp;<CallLink call={width < 770}></CallLink>
                </p>
            </ContentWrap>
            <form
                name="contact"
                className={props.className}
                onSubmit={handleSubmit(onSubmit)}
                data-netlify="true"
                data-netlify-honeypot="bot-field"
            >
                <input type="hidden" ref={register} name="bot-field" />
                <input
                    type="hidden"
                    ref={register({ required: true })}
                    name="form-name"
                    value="contact"
                    required
                />
                <Layout size="help-form">
                    <FieldWrapper itemSize="u-2/3@tablet">
                        <StyledTextArea
                            name="message"
                            placeholder="Your home automation question"
                            ref={register({ required: true })}
                        ></StyledTextArea>
                        <Message>{message}</Message>
                    </FieldWrapper>
                    <FieldWrapper itemSize="u-1/3@tablet">
                        <StyledInput
                            name="name"
                            type="text"
                            ref={register}
                            placeholder="Your name"
                            required
                        />
                        <StyledInput
                            name="email"
                            type="email"
                            placeholder="Your email"
                            ref={register({ required: true })}
                            required
                        />
                        <StyledButton
                            mod={
                                allFields.email &&
                                allFields.name &&
                                allFields.lastname
                                    ? "full"
                                    : "disabled"
                            }
                            className="o-btn--hover-shadow"
                        >
                            Submit Your Question
                        </StyledButton>
                    </FieldWrapper>
                </Layout>
            </form>
        </div>
    )
}


export default HelpForm