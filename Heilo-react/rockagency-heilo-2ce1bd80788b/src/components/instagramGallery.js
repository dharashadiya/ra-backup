import React from "react"
import styled from "styled-components"
import { Link, graphql, StaticQuery, useStaticQuery } from "gatsby"
import Img from "gatsby-image"
import Layout from "./util/layout"

const InstagramGallery = (props) => {
    const data = useStaticQuery(graphql`
        {
            instaNode {
                username
            }
            allInstaNode {
                edges {
                    node {
                        timestamp
                        localFile {
                            childImageSharp {
                                fluid(maxWidth: 700) {
                                    ...GatsbyImageSharpFluid_noBase64
                                }
                            }
                        }
                    }
                }
            }
        }
    `)
    const imageCount = props.imageCount || 6
    const usernameLink = `https://instagram.com/${data.instaNode.username}`
    const imagesSorted = data.allInstaNode.edges.sort(
        (edgeA, edgeB) =>
            edgeB.node.timestamp - edgeA.node.timestamp
    )
    const images = imagesSorted
        .slice(0, imageCount)
        .map((edge, index) => (
            <a 
                href={usernameLink} 
                target="_blank"
                key={index}
            >
                <Img
                    fluid={
                        edge.node.localFile.childImageSharp
                            .fluid
                    }
                />
            </a>
        ))

    return (
        <Layout
            size="flush"
            itemSize="u-1/6@laptop u-1/3@tablet u-1/2@mobileLandscape"
        >
            {images}
        </Layout>
    )

}

export default InstagramGallery