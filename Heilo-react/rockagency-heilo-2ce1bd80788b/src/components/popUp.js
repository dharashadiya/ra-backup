import React from "react"
import Popup from "reactjs-popup"



export default PopUp = () => {
    return (
        <Popup
            trigger={<button> Trigger</button>}
            position="right center"
            lockScroll
            modal
        >
            <div>Popup content here !!</div>
        </Popup>
    )
}
