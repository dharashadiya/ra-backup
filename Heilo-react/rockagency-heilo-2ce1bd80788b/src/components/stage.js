import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"
import Button from "./util/button"
import Wrapper from "./util/wrapper"

const StyledStage = styled.div`
    height: 575px;
    ${props => `background-image: url(${props.backgroundImage});`}
    background-position: center center;
    background-size: cover;
    position: relative;
    display: flex;
    flex-direction: column;
    text-align: center;
    justify-content: center;

    ${({ theme }) => theme.media.lessThan("tablet")`
        height: 485px;

        a {
            position: absolute;
            bottom: 80px;
            left: 50%;
            transform: translateX(-50%);
            width: fit-content;
        }
    `};

    p {
        font-size: 20px;
        line-height: 25px;
        color: #fff;
        ${({ theme }) => theme.media.lessThan("tablet")`
            font-size: 16px;
        `};
    }

    h1 {
        max-width: 686px;
        margin: 0 auto 11px;
    }
`
const StyledWrapper = styled(Wrapper)`
    width: 100%;
    `;

const StyledStageInner = styled.div`

    div {
        margin: 0 auto;
        max-width: ${props =>
            props.contentMaxWidth ? props.contentMaxWidth : "686px"};
    }
    display: flex;
    flex-direction: column;
    text-align: center;
    justify-content: center;
`

const Stage = (props) => {
    return (
        <StyledStage backgroundImage={props.backgroundImage}>
            <StyledWrapper>
                <StyledStageInner contentMaxWidth={props.contentMaxWidth}>
                    <h1 dangerouslySetInnerHTML={{ __html: props.title }} />
                    <div dangerouslySetInnerHTML={{ __html: props.content }} />
                    {props.buttonLink && (
                        <Link to={props.buttonLink}>
                            <Button>{props.buttonTitle}</Button>
                        </Link>
                    )}
                </StyledStageInner>
            </StyledWrapper>
        </StyledStage>
    )
}

export default Stage
