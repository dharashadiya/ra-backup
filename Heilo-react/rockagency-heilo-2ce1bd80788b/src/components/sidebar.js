import React, { useState, useEffect, useRef } from "react"
import { slide as Menu } from "react-burger-menu"
import { Link, graphql, useStaticQuery } from 'gatsby'
import styled from "styled-components"
import HomeIcon from "../assets/svg/home.inline.svg";
import CalendarIcon from "../assets/svg/calendar.inline.svg";
import CrossIcon from "../assets/images/cross@2x.png"
import { useMainMenu } from "../hooks/useMainMenu"

const MenuInner = styled.div`
    display: flex;
    height: 100%;
    overflow: hidden;
`

const StyledCalendarIcon = styled(CalendarIcon)`
    color: #696969;
    &:hover {
        color: ${({ theme }) => theme.colors.tertiary};
    }
`

const StyledHomeIcon = styled(HomeIcon)`
    color: #696969;
    &:hover {
        color: ${({ theme }) => theme.colors.tertiary};
    }
`

const StyledLink = styled(Link)`
    color: #696969;
    display: block;
    text-decoration: none;
    width: 100%;
    max-width: 140px;
    line-height: 18px;
    margin: 0 auto 18px;
    transition: .25s;

    ${({ theme }) => theme.media.lessThan("tablet")`
        margin-bottom: 12px;
    `};

    &:hover {
        color: ${({ theme }) => theme.colors.tertiary};
    }
`

const MenuLeft = styled.nav`
    display: inline-block;
    margin: 0 10px;
    max-width: 172px;
    width: 100%;
    padding: 34px 0 0;
    text-align: center;

    ${({ theme }) => theme.media.lessThan("tablet")`
        padding-top: 18px;
    `};
    section {
        margin-top: 26px;
        padding-bottom: 6px;
        border-bottom: 1px solid #70707025;
        ${({ theme }) => theme.media.lessThan("tablet")`
            margin-top: 8px;
        `};

        &:first-of-type {
            margin-top: 0;
        }

        &:last-of-type {
            border-bottom: none;
        }
    }

    h5 {
        color: #69696975;
        font-weight: 400;
        letter-spacing: 1.2px;
        text-transform: uppercase;
        max-width: 96px;
        font-size: 12px;
        line-height: 14px;
        margin: 0 auto 21px;
        ${({ theme }) => theme.media.lessThan("tablet")`
            margin-bottom: 8px;
        `};
    }
`
const MenuLink = styled(Link)`
    display: block;
    margin-top: 42px;
    margin-left: auto;
    margin-right: auto;
    transition: 0.25s;

    &:first-of-type {
        margin-top: 67px;
    }

    ${({ theme }) => theme.media.lessThan("tablet")`
        margin-top: 32px;
        &:first-of-type {
            margin-top: 57px;
        }
    `};

    &:hover {
        color: ${({ theme }) => theme.colors.tertiary};
    }
`

const MenuRight = styled.nav`
    width: 70px;
    display: inline-block;
    margin-top: 33px;
    padding: 0 25px;
    border-left: 1px solid #70707025;
    height: 100%;

    ${({ theme }) => theme.media.lessThan("tablet")`
        margin-top: 18px;
    `};
`

const SideBar = () => {
    
    const {
        allWordpressWpSmartLives,
        allWordpressWpAutomationTypes,
    } = useStaticQuery(graphql`
        {
            allWordpressWpSmartLives(sort: { fields: acf___menu_order }) {
                edges {
                    node {
                        title
                        path
                    }
                }
            }
            allWordpressWpAutomationTypes(sort: { fields: acf___menu_order }) {
                edges {
                    node {
                        title
                        path
                    }
                }
            }
        }
    `)

    const linkFactory = item => (
        <StyledLink
            key={item.node.title}
            to={item.node.path}
            dangerouslySetInnerHTML={{ __html: item.node.title }}
        >
        </StyledLink>
    )
    const automationTypes = allWordpressWpAutomationTypes.edges.map(item =>
        linkFactory(item)
    )
    const smartLives = allWordpressWpSmartLives.edges.map(item =>
        linkFactory(item)
    )
    const pages = useMainMenu().items.map(item => (
        <StyledLink key={item.title} to={item.object_slug}>
            {item.title}
        </StyledLink>
    ))

    const [sidebarOpen, setSidebarOpen] = useState(false)

    useEffect(() => {
        const handleScroll = () => {
            setSidebarOpen(false)
        }

        window.addEventListener("scroll", handleScroll, { passive: true })

        return () => window.removeEventListener("scroll", handleScroll)
    }, [sidebarOpen])

    const detectIfSidebarOpen = (state) => {
        if (state.isOpen) {
            setSidebarOpen(true)
        }
    }
	
	return (
        <Menu
            isOpen={sidebarOpen}
            right
            width={270}
            onStateChange={detectIfSidebarOpen}
            customCrossIcon={<img src={CrossIcon} />}
        >
            <div>
                <MenuInner>
                    <MenuLeft>
                        <section>
                            <h5>Who is Heilo</h5>
                            {pages}
                        </section>
                        <section>
                            <h5>Smart Lives</h5>
                            {smartLives}
                        </section>
                        <section>
                            <h5>Automation Types</h5>
                            {automationTypes}
                        </section>
                    </MenuLeft>
                    <MenuRight>
                        <MenuLink to="/">
                            <StyledHomeIcon></StyledHomeIcon>
                        </MenuLink>
                        <MenuLink>
                            <StyledCalendarIcon></StyledCalendarIcon>
                        </MenuLink>
                    </MenuRight>
                </MenuInner>
            </div>
        </Menu>
    )
}

export default SideBar
