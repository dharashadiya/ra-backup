import React from "react"
import Slider from "react-slick"
import styled from "styled-components"
import { graphql } from "gatsby"
import useSiteOptions from "../hooks/useSiteOptions"
import Img from "gatsby-image"
import Arrow from "../assets/svg/alt-arrow.inline.svg"
import Wrapper from "../components/util/wrapper"

const ContentWrap = styled.div`
    margin-bottom: 28px;
    h4 {
        text-transform: uppercase;
        text-align: center;
        margin-top: 58px;
        margin-bottom: 18px;
        ${({ theme }) => theme.media.lessThan("tablet")`
            margin-top: 40px;
        `};
    }
`

const LeftArrow = styled(Arrow)`
    position: absolute;
    top: 50%;
    transform: translateY(-50%) rotate(180deg);
    left: 0;
    z-index: 2;
    ${({ theme }) => theme.media.lessThan('tablet')`
        left: -24px;
    `};
`

const RightArrow = styled(Arrow)`
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 0;
    z-index: 2;
    ${({ theme }) => theme.media.lessThan("tablet")`
        right: -24px;
    `};
`

const StyledImg = styled(Img)`
    max-width: 120px;
    margin: 0 auto;
    
    ${({ theme }) => theme.media.lessThan('tablet')`
        max-width: 74px;
    `};
`;

const StyledSlider = styled(Slider)`

    .slick-list {
        max-width: 1008px;
        margin: 0 auto;
    }

    .slick-dots {
        opacity: 0;
    }
`;


const LogoSlider = (props) => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        autoplay: true,
        slidesToShow: 7,
        slidesToScroll: 1,
        nextArrow: <RightArrow />,
        prevArrow: <LeftArrow />,
        centerPadding: "50px",
        responsive: [ 
            {
                breakpoint: 770,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 1210,
                settings: {
                    slidesToShow: 5
                }
            }
        ]
    }

    const logos = useSiteOptions().brand_logos.map((node, index) => 
        <div>
            <StyledImg key={index} fluid={node.logo.localFile.childImageSharp.fluid}></StyledImg>
        </div>
    )

    return (
        <ContentWrap>
            <h4>Brands Heilo Partners with</h4>
            <Wrapper>
                <StyledSlider {...settings}>
                    {logos}
                    {logos}
                </StyledSlider>
            </Wrapper>
        </ContentWrap>
    )
}


export default LogoSlider
