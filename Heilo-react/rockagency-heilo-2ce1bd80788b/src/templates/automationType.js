import React from "react"
import SEO from "../components/seo"
import SiteWrapper from "../components/util/siteWrapper"
import styled from "styled-components"
import Button from "../components/util/button"
import FadeInWrapper from "../components/util/fadeInWrapper"
import { useStaticQuery, Link, graphql } from "gatsby"
import Wrapper from "../components/util/wrapper"
import Img from "gatsby-image"
import Layout from "../components/util/layout"
import Slider from "react-slick"
import AutomationList from "../components/automationList"
import ArrowIcon from "../assets/svg/slider-arrow.inline.svg"
import HelpForm from "../components/helpForm"
import Stage from "../components/stage"

const FAQ = styled.div`
    h2 {
        text-align: center;
        padding-top: 112px;
        margin-bottom: 52px;

        ${({ theme }) => theme.media.lessThan("tablet")`
            padding-top: 60px;
            margin-bottom: 35px;
        `};
    }

    h4 {
        text-transform: uppercase;
        text-align: center;
    }

    p {
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
    }
`

const SingleFAQ = styled.div`
    max-width: 313px;
    margin: 0 auto;
    text-align: center;

    ${({ theme }) => theme.media.lessThan('tablet')`
        max-width: 100%;
    `};
`;

const MainContentMobile = styled.div`
    display: none;
    margin-bottom: 24px;
    font-family: ${({ theme }) => theme.fontFamily.bodyLight};
    font-size: 14px;

    ${({ theme }) => theme.media.lessThan("tablet")`
        display: block;
    `};
`

const MainContentWrap = styled.section`
    display: flex;
    margin-top: 70px;
    margin-bottom: 85px;
    align-items: center;

    ${({ theme }) => theme.media.lessThan('tablet')`
        flex-direction: column;
        text-align: center;
        margin-top: 20px;
        margin-bottom: 24px;
    `};
`;

const FormWrap = styled.div`
    margin-top: 30px;
    padding-bottom: 87px;

    ${({ theme }) => theme.media.lessThan('tablet')`
        margin-top: 0;
        padding-bottom: 14px;
    `};
`;

const BlueBG = styled.section`
    background-color: #f3fafd;
`

const MainContent = styled.div`
    max-width: 408px;
    margin-right: 22px;
    ${({ theme }) => theme.media.lessThan("tablet")`
        margin-top: 24px;
        margin-left: auto;
        margin-right: auto;
    `};

    p {
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
        max-width: 330px;
        ${({ theme }) => theme.media.lessThan("tablet")`
            max-width: 100%;
            display: none;
        `};
    }
`

const MainImg = styled(Img)`
    max-width: 685px;
    width: 100%;
`;

const StyledSlider = styled(Slider)`
    margin-bottom: -6px;
    overflow: hidden;

    ${({ theme }) => theme.media.lessThan('tablet')`
        margin-top: 35px;
    `};
`;

const StyledArrow = styled(ArrowIcon)`
    width: 26px;
    height: 46px;
    z-index: 2;
    opacity: .75;
    transition: .25s;

    &:hover {
        opacity: 1;
    }
`

const LeftArrow = styled(StyledArrow)`
    position: absolute;
    top: 50%;
    transform: translateY(-50%) rotate(180deg);
    left: 85px;
`


const RightArrow = styled(StyledArrow)`
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 85px;
`

const ContentBlock = styled.div`
    ${({ theme }) => theme.media.lessThan("tablet")`
        text-align: center;
    `};

    h4 {
        text-align: center;
        text-transform: uppercase;
        margin-bottom: 30px;
    }

    p {
        padding-top: 25px;
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
        ${({ theme }) => theme.media.lessThan("tablet")`
            margin-bottom: 36px;
            padding-top: 21px;
        `};
    }
`

export default ({pageContext: data}) => {

    const postData = data
    const pageContext = data
    const fields = data.acf
    const contentBlocks = fields.content_blocks.map(item => (
        <ContentBlock key={item.title}>
            <h4>{item.title}</h4>
            <Img fluid={item.image.localFile.childImageSharp.fluid}></Img>
            <div
                dangerouslySetInnerHTML={{
                    __html: item.content,
                }}
            />
        </ContentBlock>
    ))
    const sliderImages = fields.slider_images.map((slide, index) => (
        <Img key={index} fluid={slide.image.localFile.childImageSharp.fluid}></Img>
    ))

    const sliderSettings = {
        nextArrow: <RightArrow />,
        prevArrow: <LeftArrow />,
        dots: false,
        responsive: [
            {
                breakpoint: 770,
                settings: {
                    nextArrow: null,
                    prevArrow: null
                },
            }
        ],
    }

    const faqs = fields.faq.map(singleFAQ => (
        <SingleFAQ>
            <h4>{singleFAQ.question}</h4>
            <div dangerouslySetInnerHTML={{ __html: singleFAQ.answer }} />
        </SingleFAQ>
    ))

    return (
        <SiteWrapper>
            <SEO
                title={pageContext.yoast_meta.yoast_wpseo_title}
                description={pageContext.yoast_meta.yoast_wpseo_metadesc}
            />
            <Stage
                backgroundImage={postData.featured_media.source_url}
                title={postData.title}
                content={postData.content}
                buttonLink={fields.top_button.link}
                buttonTitle={fields.top_button.title}
            ></Stage>
            <Wrapper>
                <MainContentWrap>
                    <MainContent>
                        <h2>{fields.main_content.title}</h2>
                        <div
                            dangerouslySetInnerHTML={{
                                __html: fields.main_content.content,
                            }}
                        />
                    </MainContent>
                    <MainImg
                        loading="eager"
                        backgroundColor="#FFF"
                        fluid={
                            fields.main_content.image.localFile.childImageSharp
                                .fluid
                        }
                    ></MainImg>
                    <MainContentMobile>
                        <div
                            dangerouslySetInnerHTML={{
                                __html: fields.main_content.content,
                            }}
                        />
                    </MainContentMobile>
                </MainContentWrap>
                <Layout fadeInUp size="large2" itemSize="u-1/3@tablet">
                    {contentBlocks}
                </Layout>
            </Wrapper>
            <StyledSlider {...sliderSettings}>{sliderImages}</StyledSlider>
            <AutomationList current={postData.title}></AutomationList>
            <BlueBG>
                <Wrapper>
                    <FAQ>
                        <h2 dangerouslySetInnerHTML={{
                                __html: postData.title + " FAQs",
                            }}></h2>
                        <Layout fadeInUp itemSize="u-1/3@tablet">
                            {faqs}
                        </Layout>
                    </FAQ>
                    <FormWrap>
                        <HelpForm automationType={postData.title}></HelpForm>
                    </FormWrap>
                </Wrapper>
            </BlueBG>
        </SiteWrapper>
    )
}