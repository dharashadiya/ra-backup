import React from "react"
import Layout from "../components/util/layout"
import Header from "../components/header"

export default ({ pageContext }) =>  {
    console.log(pageContext)
    return (
        <Layout>
            <Header></Header>
            <h1 dangerouslySetInnerHTML={{ __html: pageContext.title }} />
            <div dangerouslySetInnerHTML={{ __html: pageContext.content }} />
        </Layout>
    )
}
