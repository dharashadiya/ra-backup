import React from "react"
import styled from "styled-components"
import SEO from "../components/seo"
import SiteWrapper from "../components/util/siteWrapper"
import Wrapper from "../components/util/wrapper"
import Stage from "../components/stage"
import PageBG from "../assets/images/page-bg.jpg"
import Layout from "../components/util/layout"

const BlueBG = styled.section`
    background-color: #f3fafd;
    padding-top: 135px;
    
    ${({ theme }) => theme.media.lessThan("tablet")`
        padding-top: 80px;
    `};

    h1 {
        margin-bottom: 24px;
    }
`

const SideTitle = styled.h3`
    text-align: right;
    ${({ theme }) => theme.media.lessThan("tablet")`
        text-align: center;
    `};
`

const Page = styled.main`
    p:first-of-type {
        margin-bottom: 44px;
    }
    h4 {
        text-transform: uppercase;
        margin-bottom: 8px;
        margin-top: 36px;
    }

    ul {
        list-style-position: inside;

        li{
            text-indent: -20px;
            padding-left: 20px;
        }
    }
`


export default ({ pageContext }) => {
    return (
        <SiteWrapper>
            {/* <SEO
                title={pageContext.yoast_meta.yoast_wpseo_title}
                description={pageContext.yoast_meta.yoast_wpseo_metadesc}
            /> */}
            <Stage title={pageContext.data.title} backgroundImage={PageBG} />
            <Page>
                <BlueBG>
                    <Wrapper>
                        <Layout size="large2">
                            <SideTitle
                                itemSize="u-1/3@tablet"
                                dangerouslySetInnerHTML={{
                                    __html: pageContext.data.acf.sub_heading,
                                }}
                            />
                            <div
                                itemSize="u-2/3@tablet"
                                dangerouslySetInnerHTML={{
                                    __html: pageContext.data.content,
                                }}
                            />
                        </Layout>
                    </Wrapper>
                </BlueBG>
            </Page>
        </SiteWrapper>
    )
}
