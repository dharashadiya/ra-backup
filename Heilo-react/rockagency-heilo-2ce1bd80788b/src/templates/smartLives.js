import React, { useState } from "react"
import Layout from "../components/util/layout"
import Header from "../components/header"
import SEO from "../components/seo"
import SiteWrapper from "../components/util/siteWrapper"
import { graphql, Link, useStaticQuery } from "gatsby"
import Wrapper from "../components/util/wrapper"
import styled from "styled-components"
import Button from "../components/util/button"
import AutomationList from "../components/automationList"
import YouTube from "react-youtube"
import PlayIcon from "../assets/svg/play-button.inline.svg"

const ContentBlockWrapper = styled.div`
    margin-top: 51px;
    margin-bottom: 18px;
`

const StyledStage = styled.div`
    height: 728px;
    ${props => `background-image: url(${props.backgroundImage});`}
    background-position: center center;
    background-size: cover;
    position: relative;
    display: flex;
    flex-direction: column;
    text-align: center;
    justify-content: center;
    ${({ theme }) => theme.media.lessThan("tablet")`
        height: 375px;
    `};
`

const ContentBlock = styled.div`
    text-align: center;


    h4 {
        ${({ theme }) => theme.media.lessThan("tablet")`
            margin-bottom: 18px;
        `};
        text-align: center;
        text-transform: uppercase;
        margin-bottom: 30px;
    }

    p {
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
        ${({ theme }) => theme.media.lessThan("tablet")`
            margin-bottom: 24px;
        `};
    }
`

const StageContent = styled.div`
    position: absolute;
    left: 82px;
    bottom: 41px;
    text-align: left;

    ${({ theme }) => theme.media.lessThan("tablet")`
        left: 36px;
        bottom: 24px;
    `};

    h3 {
        margin-bottom: 0;
    }
    h1 {
        margin-top: 9px;
    }
`

const StyledWrapper = styled(Wrapper)`
    position: relative;
    height:100%;
    width: 100%;
`;

const PlayButton = styled(PlayIcon)`
    position: absolute;
    cursor: pointer;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    transition: 0.25s;
    &:hover {
        transform: translate(-50%, -50%) scale(1.1);
    }

    ${({ theme }) => theme.media.lessThan("tablet")`
        max-width: 80px;
    `};
`

const MainContentWrap = styled.section`
    background-color: #f3fafd;
    padding-top: 120px;
    padding-bottom: 42px;
    ${({ theme }) => theme.media.lessThan("tablet")`
        padding-top: 45px;
        text-align: center;
    `};

    h2 {
        max-width: 337px;
        margin-bottom: 0;
        ${({ theme }) => theme.media.lessThan("tablet")`
            max-width: 100%;
        `};
    }

    p {
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
        margin-bottom: 32px;
    }
`
const ContentInnerWrap = styled.div`
    margin-top: 18px;
`;

const PlayerWrapper = styled.div`
    pointer-events: none;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: ${props => props.show ? "2" : "-1"};

    iframe {
        pointer-events: all;
    }
`

export default ({ pageContext: data }) => {
    const [player, setPlayer] = useState(null)
    const [showPlayer, setShowPlayer] = useState(false)
    const postData = data
    const pageContext = data
    const fields = postData.acf
    const contentBlocks = fields.content_blocks.map(item => (
        <ContentBlock key={item.title}>
            <h4>{item.title}</h4>
            <div
                dangerouslySetInnerHTML={{
                    __html: item.content,
                }}
            />
        </ContentBlock>
    ))

    const videoReady = event => {
        setPlayer(event.target)
    }

    const playVideo = () => {
        if (player) {
            player.playVideo()
            setShowPlayer(true)
        }
    }

    const options = {
        width: "100%",
        height: "758px",
    }

    return (
        <SiteWrapper>
            <SEO
                title={pageContext.yoast_meta.yoast_wpseo_title}
                description={pageContext.yoast_meta.yoast_wpseo_metadesc}
            />
            <PlayerWrapper show={showPlayer}>
                <YouTube
                    videoId={fields.video_id}
                    onReady={videoReady}
                    opts={options}
                ></YouTube>
            </PlayerWrapper>
            <StyledStage backgroundImage={postData.featured_media.source_url}>
                <StyledWrapper>
                    <PlayButton onClick={playVideo}></PlayButton>
                    <StageContent>
                        <h3>Introducing</h3>
                        <h1>{postData.title}</h1>
                    </StageContent>
                </StyledWrapper>
            </StyledStage>
            <MainContentWrap>
                <Wrapper>
                    <Layout fadeInUp size="large3" moduleOption>
                        <h2 itemSize="u-1/3@tablet">{fields.sub_title}</h2>
                        <ContentInnerWrap itemSize="u-2/3@tablet">
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: postData.content,
                                }}
                            />
                            <Link to="/showroom">
                                <Button>Visit our showroom</Button>
                            </Link>
                        </ContentInnerWrap>
                    </Layout>
                </Wrapper>
            </MainContentWrap>
            <ContentBlockWrapper>
                <Wrapper>
                    <Layout fadeInUp size="large2" itemSize="u-1/3@tablet">
                        {contentBlocks}
                    </Layout>
                </Wrapper>
            </ContentBlockWrapper>
            <AutomationList></AutomationList>
        </SiteWrapper>
    )
}
