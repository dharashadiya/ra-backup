import React from "react"
import SEO from "../components/seo"
import SiteWrapper from "../components/util/siteWrapper"
import { graphql } from "gatsby"
import Stage from "../components/stage"
import styled from "styled-components"
import Wrapper from "../components/util/wrapper"
import Layout from "../components/util/layout"
import CallLink from "../components/callLink"

const BlueBG = styled.section`
    background-color: #f3fafd;
`

const ContentWrap = styled.section`
    padding-top: 120px;
    padding-bottom: 43px;

    ${({ theme }) => theme.media.lessThan("tabletWide")`
       text-align: center;
       padding-top: 35px;
        padding-bottom: 22px;
    `};

    h3 {
        text-align: right;
        max-width: 374px;
        margin-left: auto;
        ${({ theme }) => theme.media.lessThan("tabletWide")`
            text-align: center;
            margin-right: auto;
            margin-bottom: 9px;
        `};
    }

    article {
        max-width: 445px;
        ${({ theme }) => theme.media.lessThan("tabletWide")`
            margin: 0 auto;
        `};
    }

    p {
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
    }

    strong {
        font-family: ${({ theme }) => theme.fontFamily.body};
        font-weight: normal;
        font-size: 18px;
    }
`

export default ({ data }) => {
    const fields = data.allWordpressPage.edges[0].node
    const pageContext = fields
    return (
        <SiteWrapper>
            <SEO
                title={pageContext.yoast_meta.yoast_wpseo_title}
                description={pageContext.yoast_meta.yoast_wpseo_metadesc}
            />
            <Stage
                backgroundImage={fields.featured_media.source_url}
                title={fields.title}
                content={fields.content}
                buttonTitle={fields.acf.top_button.title}
                buttonLink={fields.acf.top_button.url}
                contentMaxWidth="489px"
            ></Stage>
            <BlueBG>
                <Wrapper>
                    <ContentWrap>
                        <Layout fadeInUp size="about" itemSize="u-1/2@tabletWide">
                            <h3>{fields.acf.about.title}</h3>
                            <div>
                                <article
                                    dangerouslySetInnerHTML={{ __html: fields.acf.about.content }}
                                />
                                <CallLink
                                    call></CallLink>
                            </div>
                        </Layout>
                    </ContentWrap>
                </Wrapper>
            </BlueBG>
        </SiteWrapper>
    )
}

export const query = graphql`
           query aboutPage($id: Int) {
               allWordpressPage(filter: { wordpress_id: { eq: $id } }) {
                   edges {
                       node {
                           title
                           content
                           acf {
                               top_button {
                                   title
                                   url
                               }
                               about {
                                   title
                                   content
                               }
                           }
                           featured_media {
                               source_url
                           }
                           yoast_meta {
                               yoast_wpseo_metadesc
                               yoast_wpseo_title
                           }
                       }
                   }
               }
           }
       `
