import React from "react"
import { graphql, useStaticQuery, Link } from "gatsby"
import SEO from "../components/seo"
import SiteWrapper from "../components/util/siteWrapper"
import Wrapper from "../components/util/wrapper"
import styled from "styled-components"
import BackgroundImage from "gatsby-background-image"
import Button from "../components/util/button"
import Layout from "../components/util/layout"
import Img from "gatsby-image"
import AutomationList from "../components/automationList"
import HomeMiddleSection from "../components/homeMiddleSection"
import CustomerSection from "../components/customerSection"
import DownArrow from "../assets/svg/down-arrow.inline.svg"
import HomeAutomationSection from "../components/homeAutomationSection"
import { animated, useSpring, config } from "react-spring"

const SingleSmartLives = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 232px;
    ${props => `background-image: url(${props.backgroundImage});`}
    background-position: center center;
    background-size: cover;
    transition: .25s;
`
const SingleSmartLivesButton = styled(Button)`
    position: absolute;
    bottom: 32px;
    transform: translateX(-50%);
    left: 50%;
`

const SingleSmartLivesWrapper = styled.div`
    text-align: center;
    position: relative;
    overflow: hidden;
    width: 100%;
    height: 232px;

    .bg {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        background-color: #000;
        transition: .25s;
        opacity: 0;
    }

    &:hover {
        .bg {
            opacity: .2;
        }
        ${SingleSmartLives} {
            transform: scale(1.1);
        }
    }
`

const HomeStageBG = styled.div`
    ${props => `background-image: url(${props.backgroundImage});`}
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: -1;
    background-position: center center;
    background-size: cover;
    ${({ theme }) => theme.media.lessThan("mobile")`
        ${props => `background-image: url(${props.mobileImage});`}
    `};
`

const AnimatedHomeBG = animated(HomeStageBG)

const HomeStage = styled.div`
    position: relative;
    height: 670px;
    width: 100%;
    text-align: center;

    ${({ theme }) => theme.media.lessThan("mobile")`
        padding: 0 30px;
        height: 583px;
    `};

    ${({ theme }) => theme.media.lessThan("laptop")`
        height: 570px;
    `};

    h1 {
        padding-top: 315px;
        max-width: 780px;
        margin: 0 auto 23px;

        ${({ theme }) => theme.media.lessThan("mobile")`
            font-size: 35px;
            padding-top: 300px;
            flex-direction: column;
        `};

        ${({ theme }) => theme.media.lessThan("laptop")`
            padding-top: 290px;
        `};
    }
`

const StyledDownArrow = styled(DownArrow)`
    position: absolute;
    left: 28px;
    bottom: 26px;
    ${({ theme }) => theme.media.lessThan("tablet")`
        bottom: 53px;
    `};
`  

const StageContentWrap = styled.section`
    margin: 0 auto 23px;
    max-width: 780px;
    justify-content: center;
    align-items: center;
    display: flex;
    text-align: right;

    ${({ theme }) => theme.media.lessThan("tablet")`
        flex-direction: column;
        text-align: center;
    `};

    p {
        max-width: 476px;
        margin-right: 22px;
        color: #fff;
        font-size: 20px;
        line-height: 25px;
        margin-bottom: 0;
        ${({ theme }) => theme.media.lessThan("tablet")`
            margin-bottom: 24px;
            font-size: 16px;
            max-width: 303px;
            margin-left: auto;
            margin-right: auto;
        `};
    }

    button {
        margin-top: 8px;
    }

    a {
        text-decoration: none;
        font-size: 18px;
    }
`

export default ({data}) => {
	const {
        allWordpressPage,
        allWordpressWpSmartLives,
    } = data

    const spring = useSpring({
        config: config.slow,
        from: {
            transform: `scale(1.2)`
        },
        to : {
            transform: `scale(1)`
        }
    })

    const home = allWordpressPage.edges[0].node
	const topButton = allWordpressPage.edges[0].node.acf.top_button
	const middleSection = allWordpressPage.edges[0].node.acf.home.middle_section
	const lowerSection = allWordpressPage.edges[0].node.acf.home.lower_section
	const automationSection = allWordpressPage.edges[0].node.acf.home.automation_section
	const automationContent  = middleSection.content_block.map((item) => <div key={item.title}>
			<h4>{item.title}</h4>
			<p>{item.content}</p>
        </div>)
	
	const smartLives = allWordpressWpSmartLives.edges.map((item, index) => (
        <SingleSmartLivesWrapper>
            <SingleSmartLives
                key={index}
                backgroundImage={item.node.featured_media.source_url}
            ></SingleSmartLives>
            <div className="bg" />
            <Link to={item.node.path}>
                <SingleSmartLivesButton>
                    {item.node.title}
                </SingleSmartLivesButton>
            </Link>
        </SingleSmartLivesWrapper>
    ))

	return (
        <SiteWrapper>
            <SEO title={home.title} description={home.description} />
            <HomeStage>
                <AnimatedHomeBG
                    mobileImage={home.acf.home.mobile_banner.source_url}
                    backgroundImage={home.featured_media.source_url}
                ></AnimatedHomeBG>
                <h1>{home.title}</h1>
                <StageContentWrap>
                    <div dangerouslySetInnerHTML={{ __html: home.content }} />
                    <Button mod="white">
                        <Link to={topButton.url}>{topButton.title}</Link>
                    </Button>
                </StageContentWrap>
                <a href="#automation">
                    <StyledDownArrow></StyledDownArrow>
                </a>
            </HomeStage>
            <Layout
                itemSize="u-1/1@mobileLandscape u-1/2@tablet u-1/4@tabletWide"
                size="flush"
            >
                {smartLives}
            </Layout>
            <HomeMiddleSection
                heading={middleSection.heading}
                content={automationContent}
            />
            <div id="automation"></div>
            <HomeAutomationSection
                image={automationSection.image.localFile.childImageSharp.fluid}
                heading={automationSection.title}
                content={automationSection.content}
                button={automationSection.button}
            />
            <AutomationList></AutomationList>
            <CustomerSection
                heading={lowerSection.title}
                content={lowerSection.content}
                peopleData={lowerSection.people}
            />
        </SiteWrapper>
    )
}

export const query = graphql`
           {
               allWordpressWpSmartLives(sort: { fields: acf___menu_order }) {
                   edges {
                       node {
                           title
                           path
                           featured_media {
                               source_url
                           }
                       }
                   }
               }
               allWordpressPage(filter: { wordpress_id: { eq: 2 } }) {
                   edges {
                       node {
                           title
                           content
                           featured_media {
                               source_url
                           }
                           yoast_meta {
                               yoast_wpseo_metadesc
                               yoast_wpseo_title
                           }
                           acf {
                               top_button {
                                   title
                                   url
                               }
                               home {
                                   middle_section {
                                       heading
                                       content_block {
                                           title
                                           content
                                       }
                                   }
                                   mobile_banner {
                                       source_url
                                   }
                                   lower_section {
                                       title
                                       content
                                       people {
                                           name
                                           image {
                                               localFile {
                                                   childImageSharp {
                                                       fluid(
                                                           quality: 100
                                                           maxWidth: 320
                                                       ) {
                                                           ...GatsbyImageSharpFluid_withWebp
                                                       }
                                                   }
                                               }
                                           }
                                       }
                                   }
                                   automation_section {
                                       title
                                       content
                                       button {
                                           title
                                           url
                                       }
                                       image {
                                           localFile {
                                               childImageSharp {
                                                   fluid(
                                                       quality: 100
                                                       maxWidth: 2600
                                                   ) {
                                                       ...GatsbyImageSharpFluid_withWebp
                                                   }
                                               }
                                           }
                                       }
                                   }
                               }
                           }
                       }
                   }
               }
           }
       `