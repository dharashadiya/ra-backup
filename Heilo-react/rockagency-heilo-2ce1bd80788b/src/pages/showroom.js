import { graphql, useStaticQuery } from "gatsby"
import styled from "styled-components"
import React from "react"
import SiteWrapper from "../components/util/siteWrapper"
import SEO from "../components/seo"
import Wrapper from "../components/util/wrapper"
import Stage from "../components/stage"
import Layout from "../components/util/layout"
import CallLink from "../components/callLink"


const ContentWrap = styled.div`
    margin-top: 12px;
`

const MainContent = styled.div`
    background-color: #f3fafd;
    padding-top: 120px;
    padding-bottom: 50px;

    ${({ theme }) => theme.media.lessThan("tablet")`
        padding-top: 45px;
        text-align: center;
    `};

    h3 {
        text-align: right;
        max-width: 374px;
        margin-left: auto;
        ${({ theme }) => theme.media.lessThan("tablet")`
            max-width: 100%;
            text-align: center;
        `};
    }

    p {
        font-family: ${({ theme }) => theme.fontFamily.bodyLight};
    }

    strong {
        font-size: 18px;
    }
`

const CallWrap = styled.div`
    margin: 12px 0;
`

export default () => {
    const data = useStaticQuery(graphql`
        {
            allWordpressPage(filter: { slug: { eq: "showroom" } }) {
                edges {
                    node {
                        content
                        title
                        acf {
                            top_button {
                                url
                                title
                            }
                            showroom {
                                content
                                sub_title
                            }
                        }
                        yoast_meta {
                            yoast_wpseo_metadesc
                            yoast_wpseo_title
                        }
                        featured_media {
                            source_url
                        }
                    }
                }
            }
        }
    `)
    const pageContext = data.allWordpressPage.edges[0].node
    return (
        <SiteWrapper>
            <SEO
                title={pageContext.yoast_meta.yoast_wpseo_title}
                description={pageContext.yoast_meta.yoast_wpseo_metadesc}
            />
            <Stage
                backgroundImage={pageContext.featured_media.source_url}
                title={pageContext.title}
                contentMaxWidth="489px"
                content={pageContext.content}
                buttonLink={pageContext.acf.top_button.link}
                buttonTitle={pageContext.acf.top_button.title}
            ></Stage>
            <MainContent>
                <Wrapper>
                    <Layout fadeInUp size="large2" itemSize="u-1/2@tablet">
                        <h3>{pageContext.acf.showroom.sub_title}</h3>
                        <ContentWrap>
                            <div
                                dangerouslySetInnerHTML={{
                                    __html:
                                        pageContext.acf.showroom.content,
                                }}
                            />
                            <CallWrap>
                                <CallLink call></CallLink>
                            </CallWrap>
                        </ContentWrap>
                    </Layout>
                </Wrapper>
            </MainContent>
        </SiteWrapper>
    )
}
