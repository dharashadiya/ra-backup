const _ = require("lodash")
const path = require("path")
const slash = require(`slash`)
const {
    createFilePath
} = require("gatsby-source-filesystem")
const {
    paginate
} = require("gatsby-awesome-pagination")

const toCamelCase = s => {
    return s.replace(/([-_][a-z])/gi, $1 => {
        return $1
            .toUpperCase()
            .replace("-", "")
            .replace("_", "")
    })
}
const getOnlyPublished = edges =>
    _.filter(edges, ({
        node
    }) => node.status === "publish")

const formatTemplate = string => string.slice(0, -4).toLowerCase()

const pagesToSkipCreation = [
    "home",
    "contact",
    "showroom"
]

exports.createPages = async ({
    actions,
    graphql
}) => {
    const {
        createPage,
        createRedirect
    } = actions
    // Redirecting to home page
    createRedirect({
        fromPath: "/home",
        toPath: "/",
        redirectInBrowser: true,
        isPermanent: true,
    })
    try {
            // 	========= PAGES ============
            const pagesData = await graphql(
                `
                    {
                        allWordpressPage {
                            edges {
                                node {
                                    id
                                    slug
                                    status
                                    template
                                    title
                                    content
                                    excerpt
                                    acf {
                                        sub_heading
                                    }
                                    wordpress_id
                                    wp_path: path
                                    yoast_meta {
                                        yoast_wpseo_metadesc
                                        yoast_wpseo_title
                                    }
                                }
                            }
                        }
                    }
                `
            )

            const allPages = pagesData.data.allWordpressPage.edges
            const pages =
                process.env.NODE_ENV === "production"
                    ? getOnlyPublished(allPages)
                    : allPages

            // Call `createPage()` once per WordPress page
            // Each page is required to have a `path` as well
            // as a template component. The `context` is
            // optional but is often necessary so the template
            // can query data specific to each page.
            _.each(pages, ({ node: page }) => {
                if (pagesToSkipCreation.includes(page.slug)) {
                    return
                }
                const pageTemplate = page.template
                    ? path.resolve(
                          `./src/templates/${toCamelCase(
                              formatTemplate(page.template)
                          )}.js`
                      )
                    : path.resolve("./src/templates/page.js")
                console.log(`CREATING PAGE - ${page.slug}`)
                createPage({
                    path: `${page.wp_path}`,
                    component: pageTemplate,
                    context: {
                        id: page.wordpress_id,
                        data: page
                    },
                })
            })
            // 	========= END PAGES ========

            // 	========= CPT - AUTOMATION TYPES ============
            const automationPostsData = await graphql(`
                {
                    allWordpressWpAutomationTypes {
                        edges {
                            node {
                                content
                                title
                                slug
                                status
                                wordpress_id
                                yoast_meta {
                                    yoast_wpseo_metadesc
                                    yoast_wpseo_title
                                }
                                acf {
                                    content_blocks {
                                        content
                                        title
                                        image {
                                            localFile {
                                                childImageSharp {
                                                    fluid(
                                                        quality: 100
                                                        maxWidth: 320
                                                    ) {
                                                        base64
                                                        aspectRatio
                                                        src
                                                        srcSet
                                                        srcWebp
                                                        srcSetWebp
                                                        sizes
                                                    }
                                                }
                                            }
                                            source_url
                                        }
                                    }
                                    faq {
                                        answer
                                        question
                                    }
                                    main_content {
                                        content
                                        title
                                        image {
                                            localFile {
                                                childImageSharp {
                                                    fluid(
                                                        quality: 100
                                                        maxWidth: 685
                                                    ) {
                                                        base64
                                                        aspectRatio
                                                        src
                                                        srcSet
                                                        srcWebp
                                                        srcSetWebp
                                                        sizes
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    slider_images {
                                        image {
                                            localFile {
                                                childImageSharp {
                                                    fluid(
                                                        quality: 100
                                                        maxWidth: 2000
                                                    ) {
                                                        base64
                                                        aspectRatio
                                                        src
                                                        srcSet
                                                        srcWebp
                                                        srcSetWebp
                                                        sizes
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    top_button {
                                        title
                                        url
                                    }
                                }
                                featured_media {
                                    localFile {
                                        size
                                    }
                                    source_url
                                }
                            }
                        }
                    }
                }
            `)

            const automationTemplate = path.resolve(
                `./src/templates/automationType.js`
            )

            // In production builds, filter for only published posts.
            const automationPosts =
                automationPostsData.data.allWordpressWpAutomationTypes.edges
            const automationPostsArray =
                process.env.NODE_ENV === "production"
                    ? getOnlyPublished(automationPosts)
                    : automationPosts

            // Iterate over the array of posts
            _.each(automationPostsArray, ({ node: post }) => {
                console.log(
                    `CREATING AUTOMATION - ${post.title} with ID ${post.wordpress_id}`
                )
                // Create the Gatsby page for this WordPress post
                createPage({
                    path: `automation-types/${post.slug}`,
                    component: automationTemplate,
                    context: post,
                })
            })
            // 	========= END AUTOMATION TYPES ========
            // 	========= CPT - SMART LIVES TYPES ============
            const smartLivesData = await graphql(`
                {
                    allWordpressWpSmartLives {
                        edges {
                            node {
                                content
                                title
                                slug
                                status
                                wordpress_id
                                yoast_meta {
                                    yoast_wpseo_metadesc
                                    yoast_wpseo_title
                                }
                                acf {
                                    sub_title
                                    video_id
                                    content_blocks {
                                        title
                                        content
                                    }
                                }
                                featured_media {
                                    localFile {
                                        size
                                    }
                                    source_url
                                }
                            }
                        }
                    }
                }
            `)

            const smartLivesTemplate = path.resolve(
                `./src/templates/smartLives.js`
            )

            // In production builds, filter for only published posts.
            const smartLives =
                smartLivesData.data.allWordpressWpSmartLives.edges
            const smartLivesArray =
                process.env.NODE_ENV === "production"
                    ? getOnlyPublished(smartLives)
                    : smartLives

            // Iterate over the array of posts
            _.each(smartLivesArray, ({ node: post }) => {
                console.log(
                    `CREATING SMART LIVES - ${post.title} with ID ${post.wordpress_id}`
                )
                // Create the Gatsby page for this WordPress post
                createPage({
                    path: `smart-lives/${post.slug}`,
                    component: smartLivesTemplate,
                    context: post,
                })
            })
            // 	========= END SMART LIVES TYPES ========
        } catch (error) {
        console.log(error)
    }
}