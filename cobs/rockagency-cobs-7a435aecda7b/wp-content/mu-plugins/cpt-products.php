<?php
/*
Plugin Name: Products - Custom Post Type
Description: Products - Custom Post Type
*/

add_action( 'init', 'register_products_custom_post_type' );
function register_products_custom_post_type() {
  register_post_type( 'ra-products', array(
    'label'  => __('Products', 'ra-products'),
    'description' => __('Products', 'ra-products'),
    'labels' => array(
		'name'               => _x( 'Products', 'post type general name', 'ra-products' ),
		'singular_name'      => _x( 'Product', 'post type singular name', 'ra-products' ),
		'menu_name'          => __( 'Products', 'admin menu', 'ra-products' ),
		'parent_item_colon'  => __( 'Parent Product:', 'ra-products' ),
		'name_admin_bar'     => __( 'Product', 'add new on admin bar', 'ra-products' ),
		'all_items'          => __( 'Products', 'ra-products' ),
		'view_item'          => __( 'View Product', 'ra-products' ),
		'add_new_item'       => __( 'Add New Product', 'ra-products' ),
		'add_new'            => __( 'Add New', 'Product', 'ra-products' ),
		'edit_item'          => __( 'Edit Product', 'ra-products' ),
		'update_item'        => __( 'Update Product', 'ra-products' ),
		'search_items'       => __( 'Search Products', 'ra-products' ),
		'new_item'           => __( 'New Product', 'ra-products' ),
		'not_found'          => __( 'No Products found.', 'ra-products' ),
		'not_found_in_trash' => __( 'No Products found in Trash.', 'ra-products' ),
    ),

    'supports'      => array(
      'title',
      'thumbnail',
      'page-attributes',
      'revisions',
      'editor',
	  'comments'
    ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-carrot',
    'can_export'          => true,
    'has_archive'         => false,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'query_var'           => true,
    'rewrite'             => array(
    	'slug' => 'product',
		// 'with_front' => 'true'
    )
  ) );
}


// Register Custom Taxonomy
function ra_brand_taxomony() {
  $labels = array(
    'name'                       => _x( 'Brands', 'Taxonomy General Name', 'ra-brand' ),
    'singular_name'              => _x( 'Brand', 'Taxonomy Singular Name', 'ra-brand' ),
    'menu_name'                  => __( 'Brands', 'ra-brand' ),
    'all_items'                  => __( 'All Brands', 'ra-brand' ),
    'parent_item'                => __( 'Parent Brand', 'ra-brand' ),
    'parent_item_colon'          => __( 'Parent Brand:', 'ra-brand' ),
    'new_item_name'              => __( 'New Brand', 'ra-brand' ),
    'add_new_item'               => __( 'Add New Brand', 'ra-brand' ),
    'edit_item'                  => __( 'Edit Brand', 'ra-brand' ),
    'update_item'                => __( 'Update Brand', 'ra-brand' ),
    'view_item'                  => __( 'View Brand', 'ra-brand' ),
    'separate_items_with_commas' => __( 'Separate Brand with commas', 'ra-brand' ),
    'add_or_remove_items'        => __( 'Add or remove Brand', 'ra-brand' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'ra-brand' ),
    'popular_items'              => __( 'Popular Brand', 'ra-brand' ),
    'search_items'               => __( 'Search Brand', 'ra-brand' ),
    'not_found'                  => __( 'Not Found', 'ra-brand' ),
    'items_list'                 => __( 'Brand list', 'ra-brand' ),
    'items_list_navigation'      => __( 'Brand list navigation', 'ra-brand' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => !false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
  );
  register_taxonomy( 'ra-brand', array( 'ra-products' ), $args );
}
add_action( 'init', 'ra_brand_taxomony', 0 );
?>