<?php
	$var = get_query_var('var');
	$item = $var['ID'];
	$category = get_the_terms($item, 'category');
?>
<div class="o-layout__item o-module__item u-1/3@tabletWide u-1/2@tablet c-news-tile animate">
	<a href="<?php echo get_the_permalink($item); ?>" class="c-news-tile__wrap">
		<div class="c-news-tile__img lazyload" data-src="<?php echo get_the_post_thumbnail_url($item) ?>"></div>
			<div class="c-news-tile__content">
			<h4><?php echo get_the_title($item); ?></h4>
				<div class="c-news-tile__date">
					<?php echo get_the_date('F j, Y', $item); ?>&nbsp;<span class="c-news-tile__category"><strong>Category: </strong>
					<?php
					//foreach ($category as $key => $cats) {
						// if ($cats->slug !== 'uncategorised') {
						// 	echo ($key !== 0 ? ', ' : '') . $cats->name;
						// }
					//}
					echo $category[0]->name;
					?>
					</span>
				</div>
			<p class="c-news-tile__copy"><?php echo wp_trim_words(get_the_content(null, false, $item), 24) ?>&nbsp;<span class="c-news-tile__more">(read more)</span></p>
		</div>
	</a>
</div>