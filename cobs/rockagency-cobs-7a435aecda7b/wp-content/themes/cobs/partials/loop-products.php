<?php

$var = get_query_var('var');
$args = array(
	"post_type" => 'ra-products',
	"numberposts" => -1,
	"post_status" => "publish",
	"fields" => "ids"
);
if ($var &&  $var['prd_cate']) {
	$args['tax_query'] = array(array(
		'taxonomy' => 'ra-brand',
		'field'    => 'slug',
		'terms' => $var['prd_cate']
	));
}
if ($var && $var['dietary']) {
	$args['meta_query'] = array(array(
		'key' => 'dietary_filters',
		'value'   =>  $var['dietary'],
        'compare' => 'LIKE',
	));
}
$posts = get_posts( $args ); ?>


<div class="o-layout o-module">
	<?php if (count($posts) > 0) : ?>
		<?php foreach ($posts as $item) :
			$thumb = get_the_post_thumbnail_url($item);
			$left_garnish = get_field('left_garnish_element', $item);
			$right_garnish = get_field('right_garnish_element', $item);
			$dietary_filters = get_field('dietary_filters', $item);
			$badge = get_field('product_badge', $item);
			$category = get_the_terms($item, 'ra-brand');
			?>
			<div class="o-layout__item o-module__item u-1/4@tabletWide u-1/3@tablet u-1/2@mobileLandscape animate  c-product-tile">
				<div class="c-product-tile__wrap">
					<?php if ($thumb) : ?>
						<a class="c-product-tile__img"  href="<?php echo get_the_permalink($item); ?>">
							<img data-src="<?php echo $thumb; ?>" class="c-product-tile__thumb lazyload" src="<?php echo ASSETS; ?>/img/product-placeholder.gif" alt="<?php echo get_the_title($item); ?>">
							<?php if ($badge) : $badges = product_badges(); ?>
								<span class="c-product-tile__badge">
									<?php if ($badges[$badge['value']]) : ?>
										<?php echo $badges[$badge['value']]; ?>
									<?php else : ?>
										<?php if (get_field('custom_badge_text', $item)) : ?>
											<span class="c-product-tile__badge-custom"><span class="c-product-tile__badge-custom-text"><?php echo get_field('custom_badge_text', $item); ?></span></span>
										<?php endif; ?>
									<?php endif; ?>
								</span>
							<?php endif; ?>
							<?php if ($left_garnish || $right_garnish) : ?>
								<div class="c-product-tile__garnish">
									<?php if ($left_garnish) : ?>
										<img data-src="<?php echo $left_garnish; ?>" class="c-product-tile__garnish-left lazyload" alt="Garnish Element">
									<?php endif; ?>
									<?php if ($right_garnish) : ?>
										<img data-src="<?php echo $right_garnish; ?>" class="c-product-tile__garnish-right lazyload" alt="Garnish Element">
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</a>
					<?php endif; ?>
					<div class="c-product-tile__info">
						<span class="c-product-tile__brand" style="color:<?php echo get_field('brand_color', $category[0]); ?>"><?php echo $category[0]->name; ?></span>
						<a class="c-product-tile__title" href="<?php echo get_the_permalink($item); ?>"><?php echo get_the_title($item); ?></a>
						<div class="content"><?php echo wp_trim_words(get_the_excerpt($item), 10) ; ?>&nbsp;<a href="<?php echo get_the_permalink($item) ?>" class="c-product-tile__more">(read more)</a></div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php else : ?>
		<div class="o-layout__item o-module__item c-products__list-notfound animate"><p>No products found</p></div>
	<?php endif; ?>
</div>