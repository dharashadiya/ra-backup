<?php get_header(); ?>

<div class="c-default">
	<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php if (get_field('show_feature_image_on_stage')) : ?>
				<div class="c-page-banner">
					<div class="c-page-banner__img js-parallax parallax-img lazyload" data-src="<?php echo get_the_post_thumbnail_url(); ?>"></div>
					<h1 class="page-title c-page-banner__title animate"><?php the_title(); ?></h1>
				</div>
			<?php endif; ?>
			<div class="c-cms-content o-wrapper o-wrapper--small animate-elems">
				<?php the_content(); ?>
		</article>
	<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>