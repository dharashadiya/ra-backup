<?php
/*
 * Template Name: Products Page
 */
?>

<?php get_header(); ?>

<div class="c-products">
	<span class="c-popcorn c-popcorn--product-1 right"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--product-2 right"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--product-3 left"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--product-4 left"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--product-5 left"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--product-6 right"><span class="js-parallax"></span></span>
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	<div class="c-products__wrap" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="o-wrapper o-wrapper--small">
			<h1 class="page-title c-products__title animate"><?php the_title(); ?></h1>
			<?php $categories = get_terms( array(
					'taxonomy' => 'ra-brand',
					'hide_empty' => true,
				) );
				$cat_colors = array();
			?>

			<div class="c-products__categories animate">
				<a href="javascript:void(0);" class="c-products__categories-each js-product-filter is-active all" data-filter="all">All</a>
				<?php foreach( $categories as $key => $category ) :
					$cat_colors[$category->slug] = get_field('brand_color', $category);
				?>
					<a href="javascript:void(0);" class="c-products__categories-each js-product-filter color-<?php echo $category->slug; ?>" data-filter="<?php echo $category->slug; ?>"><?php echo $category->name; ?></a>
				<?php endforeach; ?>
			</div>
			<style>
			<?php foreach ($cat_colors as $key => $item) : ?>
				<?php echo '.color-' . $key . ' { background-color: ' . $item . '; border-color: '. $item .'}'; ?>
				<?php echo '.color-' . $key . ':hover, .color-' . $key . '.is-active {color: ' . $item . '; border-color: '. $item .'}'; ?>
			<?php endforeach; ?>
			</style>

			<?php
				$product_ids = get_posts(array('post_type' => 'ra-products', 'fields' => 'ids', 'numberposts' => -1));
				$diets  = array();
				foreach ($product_ids as $id) {
					$diet = get_field('dietary_filters', $id);
					if ($diet && count($diet) > 0) {
						foreach ($diet as $value) {
							$key = stripString($value);
							if ($key) {
								$diets[$key] = $value;
							}
						}
					}
				}
				sort($diets);
			?>
			<div class="c-products__diet-filter animate">
				<span>Dietary filters:</span>
				<ul class="c-products__diet-list">
					<li class="label">Please Select</li>
					<li class="value is-active">All</li>
					<?php foreach ($diets as $item) : if ($item) : ?>
						<li class="value" data-value="<?php echo $item; ?>"><?php echo $item; ?></li>
					<?php endif; endforeach; ?>
				</ul>
			</div>
		</div>
		<div class="c-products__list o-wrapper animate">
			<?php get_template_part('partials/loop-products'); ?>
		</div>
	</div>
	<?php endwhile; ?>
</div>
<?php get_footer(); ?>