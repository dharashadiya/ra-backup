<?php
/*
 * Template Name: FAQs Page
 */
?>

<?php get_header(); ?>

<div class="c-faqs">
	<span class="c-popcorn c-popcorn--faq-1 left"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--faq-2 right"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--faq-3 right"><span class="js-parallax"></span></span>
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<?php if (get_field('show_feature_image_on_stage')) : ?>
			<div class="c-page-banner">
				<div class="c-page-banner__img js-parallax parallax-img lazyload" data-src="<?php echo get_the_post_thumbnail_url(); ?>"></div>
				<h1 class="page-title c-page-banner__title animate"><?php the_title(); ?></h1>
			</div>
		<?php endif; ?>
		<div class="c-cms-content o-wrapper o-wrapper--small">
		<?php $faqs = get_field('faqs');  ?>
		<?php if ($faqs ) : ?>
			<div class="c-faqs__list animate">
				<?php foreach ($faqs as $key => $item) : ?>
					<div class="c-faqs__list-each">
						<a href="javascript:void(0);" class="c-faqs__list-question js-accordian" data-target="qa-<?php echo $key; ?>"><?php echo $item['question']; ?></a>
						<div class="c-faqs__list-answer js-accordian__container qa-<?php echo $key; ?>"><?php echo $item['answer']; ?></div>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="c-faqs__cta animate">
				<a href="/contact" class="o-btn">Have a question? Get in touch</a>
			</div>
		<?php endif; ?>
		</div>
	<?php endwhile; ?>
</div>

<?php get_footer(); ?>
