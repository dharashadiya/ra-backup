export function reviewModal() {
	$(document).on('click', '.c-review-form__close', function () {
		$(`.c-review-form`).hide();
	})
	$(document).on('click', '.js-open-modal', function () {
		$(`.c-review-form`).show();
	})
}

export function reviewForm() {
	let nameField = $('input[name=review-name]')
	let emailField = $('input[name=review-email]')
	// let ratingField = $('select[name=review-rating]')
	let ratingField = $('input[name=rating]:checked')
	let textField = $('textarea[name=review-text]')

	$('#product-review-form').on('submit', function () {
		let post_id = $('input[name=post_id]').val()
		let name = nameField.val()
		let email = emailField.val()
		let text = textField.val()
		let valid = true;
		let rating = null
		if ($('input[name=rating]:checked').length > 0) {
			rating = $('input[name=rating]:checked').val()
		}

		$('.c-review-form__msg').text('').hide().removeClass('error')
		nameField.removeClass('invalid')
		emailField.removeClass('invalid')
		$('.c-review-form__rating .label').removeClass('invalid')
		textField.removeClass('invalid')

		if (!name) {
			valid = false
			nameField.addClass('invalid')
		}
		if (!email) {
			valid = false
			emailField.addClass('invalid')
		}
		if (!rating) {
			valid = false
			$('.c-review-form__rating .label').addClass('invalid')
		}
		if (!text) {
			valid = false
			textField.addClass('invalid')
		}

		if (post_id && valid) {
			let data = {
				'action': 'ra_post_review',
				'post_id': post_id,
				'name': name,
				'email': email,
				'rating': rating,
				'text': text
			}
			$.post(site.ajaxurl, data, function (res) {
				if (res) {
					let data = JSON.parse(res)
					$('#product-review-form')[0].reset()
					$('.c-review-form__msg').text(data.msg).show()
					if (!data.valid) {
						$('.c-review-form__msg').addClass('error')
					}
				}
			});

		} else {
			$('.c-review-form__msg').text('One or more fields have an error. Please check and try again.').show().addClass('error')
		}
	})
}