export function productFilter() {
	if ($('.c-products__list').length === 0) {
		return
	}
	let prd_cate = null
	let dietary = null
	$(document).on('click', '.js-product-filter', function () {
		$('.js-product-filter').removeClass('is-active')
		$(this).addClass('is-active')

		prd_cate = $(this).attr('data-filter')
		if (prd_cate === 'all') {
			prd_cate = null
		}
		loadPosts()
	})

	$(document).on('click', '.c-products__diet-list li', function () {
		if ($(this).hasClass('label')) {
			$('.c-products__diet-list .value').toggle(200)
		}
		if ($(this).hasClass('value')) {
			dietary = $(this).text()
			if (dietary == 'All') {
				dietary = null
			}
			loadPosts()
			$('.c-products__diet-list .label').text(dietary || 'Please select')
			$('.c-products__diet-list .value').toggle(200)
			$('.c-products__diet-list .value').removeClass('is-active')
			$(this).addClass('is-active')
		}
	})
	$(document).on('change', '#dietary-filters', function () {
		dietary = $('#dietary-filters').val()
		loadPosts()
	})

	function loadPosts() {
		let data = {
			'action': 'ra_get_products',
		}
		if (dietary) {
			data.dietary = dietary
		}
		if (prd_cate) {
			data.prd_cate = prd_cate
		}
		console.log(data)
		$.post(site.ajaxurl, data, function (res) {
			if (res) {
				let data = JSON.parse(res)
				if (data && data.html) {
					$('.c-products__list').html(data.html)
					// $('html, body').animate({ scrollTop: $('.c-products__list').position().top - 100 }, 200)
					site.fadeInUp()
					site.lazyLoad()
				}

			}
		});
	}
}