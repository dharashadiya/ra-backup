import { each } from "jquery";

export function isMobile() {
	var result;
	result = false;
	if (
		/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
			navigator.userAgent
		)
	) {
		result = true;
	}
	return result;
}
export function getParameterByName(name) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results == null) {
		return 0;
	}
	return results[1] || 0;
}

// Open External Link in new tab
export function openLinkNewTab() {
	var base;
	base = window.location.hostname;
	return $("a").each(() => {
		let url = $(this).attr("href");
		if (
			url.indexOf("http") !== -1 &&
			(url.indexOf("javascript:void(0)") < 0 && url.indexOf(base) < 0)
		) {
			$(this).attr("target", "_blank");
			return $(this).attr("rel", "noopener");
		}
	});
}

export function cloudinaryImageCDN() {
	const localhost = site.siteurl.includes(".test") ? true : false;
	imagesLoaded("body", {
		background: true
	}, () => {
		Array.from($("[data-img], [data-bg], [data-src]")).forEach(image => {
			if (localhost) {
				if (image.classList.contains("lazy")) {
					return
				} else if (image.tagName === "IMG") {
					image.src = image.dataset.src;
				} else {
					image.style.backgroundImage = `url(${image.dataset.src})`;
				}
			} else {
				const clientWidth = image.clientWidth;
				const pixelRatio = window.devicePixelRatio || 1.0;
				const imageParams = "w_" + 100 * Math.round((clientWidth * pixelRatio) / 100) + ",c_fill,g_auto,f_auto";
				const baseUrl = "https://res.cloudinary.com/rock-agency/image/fetch";
				let url;
				if (image.classList.contains("lazy")) {
					image.dataset.src = baseUrl + "/" + imageParams + "/" + image.dataset.src;
				} else if (image.tagName === "IMG") {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.src = url;
				} else {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.style.backgroundImage = `url(${url})`;
				}
			}
		});
	});
}

export function superfishMenu() {
	if ($(window).width() > 799) {
		return $(".c-nav").superfish({
			animation: {
				height: "show"
			},
			delay: 250,
			speed: 100
		});
	}
}

export function slider() {
	return $(".c-slider").cycle({
		log: false,
		// swipe: true
		slides: ".c-slide",
		"auto-height": "calc",
		timeout: 5000,
		speed: 600
	});

}

// Fade in animation
export function fadeInUp() {
	$(`.animate,
	   .animate-elems p,
	   .animate-elems span,
	   .animate-elems h1,
	   .animate-elems h2,
	   .animate-elems h3,
	   .animate-elems h4,
	   .animate-elems h5,
	   .animate-elems h6,
	   .animate-elems li`
	).addClass('pre-animate').viewportChecker({
		classToAdd: "animated",
		classToRemove: 'pre-animate',
		offset: 0
	});
	$(`.animate-slidetop`).addClass('pre-animate').viewportChecker({
		classToAdd: "anim-slideInTop",
		classToRemove: 'pre-animate',
		offset: 0
	});
	$(`.animate-zoomin`).addClass('pre-animate').viewportChecker({
		classToAdd: "img-zoomin",
		classToRemove: 'pre-animate',
		offset: 0
	});
	$('.c-popcorn').addClass('pre-animate').viewportChecker({
		classToAdd: "animate-slidein",
		classToRemove: 'pre-animate',
		offset: 0
	});
	setTimeout(() => {
		$(`.animate-special`).addClass('pre-animate').viewportChecker({
			classToAdd: "animate",
			classToRemove: 'pre-animate',
			offset: 0
		});
	}, 100);
}

// LazyLoad
export function lazyLoad() {
	if ($(window).width() < 770) {
		$('.lazyload[data-mob-src]').each(function () {
			let mobS = $(this).attr('data-mob-src')
			if (mobS) {
				$(this).attr('data-src', mobS)
			}
		})
	}
	$('.lazyload').Lazy({
		threshold: 400,
		effect: 'fadeIn',
	})
}


export function menuFunc() {
	$(document).on('click', '.c-header__nav-toggle', function () {
		// $('.c-site-nav').toggle(400)
		$('.c-top').toggleClass('menu-open')
		$('html, body').toggleClass('scroll-lock');
	})
}


export function productSlider() {

	if ($(".js-products__slider").length > 0) {
		var slideCount = jQuery(".c-home-products__slider").length;
		if (slideCount <= 3) {
			jQuery(".c-home-products__slider").children().clone(true, true).appendTo(".c-home-products__slider");
		}
		$(".js-products__slider").slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: true,
			fade: false,
			speed: 3000,
			autoplaySpeed: 100,
			autoplay: true,
			cssEase: 'linear',
			// infinite: false,
			lazyLoad: 'ondemand',
			useTransform: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 900,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 530,
					// centerMode: true,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});

	}
}
export function reviewSlider() {
	var slideCount = jQuery(".js-review-slider").length;
	if (slideCount <= 3) {
		// clone element
		jQuery(".js-review-slider").children().clone(true, true).appendTo(".js-review-slider");
	}
	if ($(".js-review-slider").length > 0) {
		let options = {
			slidesToShow: 3,
			slidesToScroll: 1,
			autoplaySpeed: 10000,
			autoplay: true,
			arrows: true,
			fade: false,
			infinite: true,
			centerMode: false,
			responsive: [
				{
					breakpoint: 1900,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 900,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 530,
					centerMode: true,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		}

		if ($(".js-review-slider .c-single-product__reviews-each").length > 3 && $(window).width() > 900) {
			$(".js-review-slider .c-single-product__reviews-each").addClass('is-centerMode')
			options.centerMode = true
		}
		$(".js-review-slider").slick(options);

	}
}
export function aboutStorySlider() {
	if ($(".c-about__story").length > 0) {
		$(".c-about__story").slick({
			slidesToShow: 1.05,
			slidesToScroll: 1,
			arrows: true,
			fade: false,
			dots: false,
			lazyLoad: 'ondemand',
			infinite: false,
			// customPaging: function (slider, i) {
			// 	var title = $(slider.$slides[i]).attr('data-title');
			// 	return '<a class="pager__item"> ' + title + ' </a>';
			// },
			asNavFor: `.c-about__story-nav`,
			responsive: [
				{
					breakpoint: 700,
					settings: {
						slidesToShow: 1.05,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1,
					}
				},
			]

		});

		$(`.c-about__story-nav`).slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			asNavFor: `.c-about__story`,
			dots: false,
			arrows: false,
			centerMode: false,
			focusOnSelect: true,
			infinite: false,
			responsive: [
				{
					breakpoint: 700,
					settings: {
						slidesToShow: 3,
					}
				}
			],
		});

		$('.c-about__story-nav').on('afterChange', function (event, slick, currentSlide) {
			$('.c-about__story-nav').removeClass('nav-bgchange');
			if(slick.currentSlide >= (slick.slideCount - 2)){
				$('.c-about__story-nav').addClass('nav-bgchange');
			}
		})

	}
}
export function homeSlider() {
	if ($(".c-home-banner").length > 0) {
		$(".c-home-banner").on('init', function (event, slick) {
			$(slick.$slides[0]).addClass('animate-slide');

		});
		$(".c-home-banner").slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplaySpeed: 15000,
			autoplay: true,
			arrows: false,
			fade: false,
			dots: true,
			infinite: false,
			// lazyLoad: 'ondemand',
		});
		$(".c-home-banner").on('afterChange', function (event, slick, currentSlide, nextSlide) {
			$(".c-home-banner .slick-slide").removeClass('animate-slide')
			$(slick.$slides[currentSlide]).addClass('animate-slide');
		});
	}
}

export function socialPostsLoadmore() {
	if ($(window).width() < 770) {
		$(document).on('click', '.sbi_load_btn', function() {
			$('.c-social-posts__fade').hide();
			$('#sbi_images').css('max-height', '100%');
		})
	}
}

export function accodianFaqs() {
	$(document).on('click', '.js-accordian', function() {
		let target = $(this).attr('data-target');
		$(this).toggleClass('is-active');
		$(`.js-accordian__container.${target}`).toggle(400);
	})
}

export function closeAds() {
	$(document).on('click', '.js-ads-close', function() {
		$('.c-news__ads-wrap').animate({
			opacity: 0
		}, 300, function () {
			$('.c-news__ads').hide(300);
			$("html, body").animate({
				scrollTop: $(window).scrollTop() + 2
			}, 600);
		});
	})
	$(document).on('click', '.js-modal-close', function() {
		$('.c-video-modal').hide();
	})
	$(document).on('click', '.js-open-video-modal', function() {
		let video = $(this).attr('data-video')
		if (video && $('.c-video-modal video').length > 0) {
			$('.c-video-modal video').attr('src', video)
			// $('.c-video-modal video').pause();
		}
		$('.c-video-modal').show();
	})
}

export function ingTabs() {
	$(document).on('click', '.js-tab-link', function () {
		let target = $(this).attr('data-tab')
		$('.js-tab-link').removeClass('is-active')
		$(this).addClass('is-active')
		$('.js-tab-content').hide(300)
		$(`.js-tab-content[data-tab=${target}]`).show(300)
	})
}

export function homeScrollDown() {
	$(document).on('click', '.js-scroll-down', function () {
		var target = $('.c-home-products').offset().top ;
		$("html, body").animate({
			scrollTop: target
		}, 600);
	})
	if ($('.js-scroll-dietary').length > 0) {
		$(document).on('click', '.js-scroll-dietary', function () {
			var target = $('.c-single-product__ing').offset().top ;
			$("html, body").animate({
				scrollTop: target - 120
			}, 600);
		})
	}
}
// fixed nav
export function fixedNav() {
	$(window).on('scroll', function () {
		scroll = $(window).scrollTop();
		if (scroll >= 100)
			$('.c-top').addClass('fixed');
		else
			$('.c-top').removeClass('fixed');
	});
}
export function parallaxEffect() {
	if ($('.js-parallax').length > 0) {
		$(document).on('mousemove', function (event) {
			let x = event.pageX * .01
			let y = event.pageY * .01
			$('.js-parallax').css({ 'transform': `matrix(1, 0, 0, 1, -${x}, ${y})` })
			let i = event.pageX * .025
			let j = event.pageY * .015
			// $('.js-parallax.c-popcorn').removeClass('left')
			// $('.js-parallax.c-popcorn').removeClass('right')
			$('.js-parallax.c-popcorn').css({ 'transform': `matrix(1, 0, 0, 1, -${i}, ${j})` })
		});
	}
	if ($('.parallax-img').length > 0) {
		$(window).on('scroll', function () {
			let sT = $(window).scrollTop()
			$('.parallax-img').css('background-position', `center ${sT / 5}px`);
		})
	}
}

export function readMoreFunc() {
	if ($('.js-readmore').length > 0) {
		let show = null
		$('.js-readmore').each(function () {
			let target = $(this).attr('data-target')
			show = $(target).height()
			let tH = $(target)[0].scrollHeight
			if (tH > show + 100) {
				$(this).show()
				$(target).height(show)
			} else {
				$(this).hide()
				$(target).height('auto')
			}
		})

		$(document).on('click', '.js-readmore', function () {
			let target = $(this).attr('data-target')
			$(target).removeClass('content-readmore')
			if ($(this).hasClass('read-less')) {
				$(target).height(show)
				$(target).addClass('content-readmore')
				$(this).text('(Read more)').removeClass('read-less')
			} else {
				$(target).height($(target)[0].scrollHeight)
				$(this).text('(Read less)').addClass('read-less')
			}
		})
	}
}

export	function flipProduct() {
	function flipImage() {
		$('.c-single-product__overlay-back').toggle()
		$('.c-single-product__overlay-front').toggle()
	}

	$(document).on('click', '.js-flip-product', function () {
		flipImage()
	})

	$(document).on('click', '.js-open-product-overlay', function () {
		let img = $(this).attr('data-image')
		if (img && img == 'back') {
			flipImage()
		}
		$('.c-single-product__overlay').fadeIn(300)
	})
	$(document).on('click', '.c-single-product__overlay-close', function () {
		$('.c-single-product__overlay').fadeOut(300)
	})

	$(document).on('click', '.c-single-product__overlay', function (e) {
		if (e.target == this) {
			$('.c-single-product__overlay').fadeOut(300)
		}
	})
}

export function loadingAnimation() {
	var animation = Cookies.get('loadingAnimation')
	if (!animation) {
		$('html, body').addClass('scroll-lock');
		$('.c-loading').removeClass('hided')
		setTimeout(() => {
			$('.c-loading').fadeOut(500).addClass('hided')
			$('html, body').removeClass('scroll-lock');
			Cookies.set('loadingAnimation', true)
		}, 2000);
	} else {
		$('.c-loading').fadeOut(500).addClass('hided')
	}
}

export function imgloaded() {
	$('.js-load-img').each(function () {
		let image = $(this)
		var img = new Image();
		img.onload = function () {
			image.parents('.c-home-banner__products').addClass('img-loaded');
		}
		img.src = $(this).attr('src');
	});

}

