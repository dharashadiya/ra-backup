export function newsFilter() {
	if ($('.c-news__list').length === 0) {
		return
	}
	let keyUpTimeout = null
	let category = null
	let offset = 0
	$(document).on('keyup', '#news-search-input', function () {
		clearTimeout(keyUpTimeout);
		keyUpTimeout = setTimeout(() => {
			offset = 0
			loadPosts()
		}, 600);
	})

	$(document).on('click', '.js-news-filter', function () {
		$('.js-news-filter').removeClass('is-active')
		$(this).addClass('is-active')

		category = $(this).attr('data-filter')
		if (category === 'all') {
			category = null
		}
		offset = 0
		loadPosts()
	})

	$(document).on('click', '.js-loadmore-posts', function () {
		offset = $('.c-news-tile').length
		loadPosts()
	})

	function loadPosts() {
		let search = $('#news-search-input').val()
		let data = {
			'action': 'ra_get_news',
			'offset': offset
		}
		if (search) {
			data.search = search
		}
		if (category) {
			data.category = category
		}
		$.post(site.ajaxurl, data, function (res) {
			if (res) {
				let data = JSON.parse(res)
				if (data) {
					if (offset === 0) {
						$('.c-news__list').html('')
					}
					if (data.post_count > 0) {
						$('.c-news__list').append(data.html)
					} else {
						$('.c-news__list').html('<div class="o-layout__item o-module__item c-news__list-notfound">No posts found</div>')
					}

					if (data.post_count && data.post_count > $('.c-news-tile').length) {
						$('.js-loadmore-posts').show()
					} else {
						$('.js-loadmore-posts').hide()
					}
					site.fadeInUp()
					site.lazyLoad()
				}
			}
		});
	}
}