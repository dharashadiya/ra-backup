/* global site */
// [1] Import functions here
import {
	isMobile,
	openLinkNewTab,
	superfishMenu,
	slider,
	menuFunc,
	loadingAnimation,
	fixedNav,
	productSlider,
	homeSlider,
	homeScrollDown,
	aboutStorySlider,
	reviewSlider,
	parallaxEffect,
	socialPostsLoadmore,
	accodianFaqs,
	closeAds,
	ingTabs,
	fadeInUp,
	readMoreFunc,
	flipProduct,
	imgloaded,
	lazyLoad,
	cloudinaryImageCDN
} from "./template-functions";
import { newsFilter } from "./news-filter";
import { reviewModal, reviewForm } from "./review-form";
import { productFilter } from "./product-filter";
(function($) {
	window.site || (window.site = {});
	$.extend(
		site,
		{
			initVars: () => {
				// Global variables -> use as site.VariableName in other files
				site.body = $("body");
				site.root = $("html");
				site.header = $("#Top");
				site.main = $("#Main");
				site.side = $("#Secondary");
				site.siteurl = $("#Siteurl").attr("content");
				site.themeurl = $("#Themeurl").attr("content");
				site.ajaxurl = $("#Ajaxurl").attr("content");
			},
			// [3] Call functions within either of these
			onReady: () => {
				svg4everybody();
				site.initVars();
				site.menuFunc();
				site.loadingAnimation();
				site.fixedNav();
				site.productSlider();
				site.homeSlider();
				site.homeScrollDown();
				site.aboutStorySlider();
				site.reviewSlider();
				site.reviewModal();
				site.reviewForm();
				site.parallaxEffect();
				site.socialPostsLoadmore();
				site.accodianFaqs();
				site.closeAds();
				site.ingTabs();
				site.fadeInUp();
				site.readMoreFunc();
				site.flipProduct();
				site.imgloaded();
				site.lazyLoad();
				site.newsFilter();
				site.productFilter();

				// site.cloudinaryImageCDN()
				return site.isMobile();
			},
			onLoad: () => {
				// return site.slider()
			},
			onResize: () => {
				//site.equalHeight();
			}
		},
		{
			// [2] Register functions here
			isMobile: isMobile,
			openLinkNewTab: openLinkNewTab,
			superfishMenu: superfishMenu,
			slider: slider,
			menuFunc: menuFunc,
			loadingAnimation: loadingAnimation,
			fixedNav: fixedNav,
			productSlider: productSlider,
			homeSlider: homeSlider,
			homeScrollDown: homeScrollDown,
			aboutStorySlider: aboutStorySlider,
			reviewSlider: reviewSlider,
			reviewModal: reviewModal,
			reviewForm: reviewForm,
			parallaxEffect: parallaxEffect,
			socialPostsLoadmore: socialPostsLoadmore,
			accodianFaqs: accodianFaqs,
			closeAds: closeAds,
			ingTabs: ingTabs,
			fadeInUp: fadeInUp,
			readMoreFunc: readMoreFunc,
			flipProduct: flipProduct,
			imgloaded: imgloaded,
			lazyLoad: lazyLoad,
			newsFilter: newsFilter,
			productFilter: productFilter,
			cloudinaryImageCDN: cloudinaryImageCDN
		}
	);
	$(() => {
		return site.onReady();
	});
	$(window).on("resize", () => {
		site.onResize();
	});
	$(window).on("load", () => {
		site.onLoad();
	});
})(jQuery);
