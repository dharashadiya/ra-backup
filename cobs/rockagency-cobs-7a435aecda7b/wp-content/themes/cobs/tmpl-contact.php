<?php
/*
 * Template Name: Contact Page
 */
?>

<?php get_header(); ?>

<div class="c-contact">
	<span class="c-popcorn c-popcorn--contact-1 left"><span class="js-parallax"></span></span>
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<?php if (get_field('show_feature_image_on_stage')) : ?>
			<div class="c-page-banner">
				<div class="c-page-banner__img js-parallax parallax-img lazyload" data-src="<?php echo get_the_post_thumbnail_url(); ?>"></div>
				<h1 class="page-title c-page-banner__title animate"><?php the_title(); ?></h1>
			</div>
		<?php endif; ?>
		<div class="c-cms-content o-wrapper o-wrapper--small">
			<div class="c-contact__contents animate">
				<?php the_content(); ?>
			</div>
			<div class="c-contact__form animate">
				<?php echo do_shortcode('[contact-form-7 id="30" title="Contact form"]') ?>
			</div>
		</div>
	<?php endwhile; ?>
</div>

<?php get_footer(); ?>



<!-- <div class="input-wraps">
<div class="input-wrap half left"> [text* first-name placeholder "First name*"]</div>
<div class="input-wrap half right"> [text* last-name placeholder "Last name*"]</div>
<div class="input-wrap half left"> [email* your-email placeholder "Email address*"]</div>
<div class="input-wrap half right">[select* enquiry-type first_as_label "Enquiry type (please select)*" "General" "Media"]</div>
<div class="input-wrap full">[textarea* enquiry placeholder "Enquiry*"] </div>
<div class="input-wrap submit">[submit "Send enquiry"] </div>
</div> -->