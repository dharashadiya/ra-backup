<?php
add_action( 'wp_ajax_nopriv_ra_get_news', 'ra_get_news' );
add_action( 'wp_ajax_ra_get_news', 'ra_get_news' );
function ra_get_news() {
	$posts_per_page = 9;
	$offset = 0;
	if ($_POST['offset']) {
		$offset = $_POST['offset'];
	}
	$args = array(
		"post_type" => 'post',
		"numberposts" => 9999999,
		"post_status" => "publish",
		"fields" => "ids"
	);
	if ($_POST['search']) {
		$args['s'] = $_POST['search'];
	}
	if ($_POST['category']) {
		$args['category_name'] = $_POST['category'];
	}

	$post_count = get_posts($args);
	$result['post_count'] = count($post_count);

	$args['numberposts'] = $posts_per_page;
	$args['offset'] = $offset;
	$posts = get_posts( $args );

	$result['args'] = $args;
	$result['posts'] = $posts;

	$html = '';
	foreach ($posts as $item) {
		$var = array('ID' => $item);
		$html .= load_template_part('partials/loop-news', $var);
	}
	$result['html'] = $html;

	$result = json_encode($result);
    wp_die($result);
}



add_action( 'wp_ajax_nopriv_ra_get_products', 'ra_get_products' );
add_action( 'wp_ajax_ra_get_products', 'ra_get_products' );
function ra_get_products() {
	$var = array();
	$result = array();

	if ($_POST['prd_cate']) {
		$var['prd_cate'] = $_POST['prd_cate'];
	}
	if ($_POST['dietary']) {
		$var['dietary'] = $_POST['dietary'];
	}
	// $result['posts'][] = load_template_part($template, $var);
	$result['var'] = $var;
	$result['html'] = load_template_part('partials/loop-products', $var);

	$result = json_encode($result);
    wp_die($result);
}


// Post review to product
add_action( 'wp_ajax_nopriv_ra_post_review', 'ra_post_review' );
add_action( 'wp_ajax_ra_post_review', 'ra_post_review' );
function ra_post_review() {
	$post_id = $_POST['post_id'];
	$name = $_POST['name'];
	$email = $_POST['email'];
	$rating = $_POST['rating'];
	$text = $_POST['text'];

	if (!$name || !$email || !$text  || !$rating) {
		$result = array('valid' => false, 'msg' => 'One or more fields have an error. Please check and try again.');
	} elseif (!$post_id || !get_post_status($post_id)) {
		$result = array('valid' => false, 'msg' => 'Something went wrong please try again.');
	} else {
		$commentdata = array(
			'comment_post_ID' => $post_id,
			'comment_author' => $name,
			'comment_author_email' => $email,
			'comment_content' => $text
		);

		$comment_id = wp_new_comment( $commentdata, true );
		if ( is_wp_error($comment_id) ) {
			$error_data = intval( $comment_id->get_error_data() );
			if ( ! empty( $error_data ) ) {
				$result = array('valid' => false, 'msg' => $comment_id->get_error_message());
			} else {
				$result = array('valid' => false, 'msg' => 'Unknown error');
			}
		} else {
			add_comment_meta($comment_id, 'ra_rating', $rating);
			$result = array('valid' => true, 'msg' => 'Review posted successfully.');
		}
	}
	$result = json_encode( $result );
    wp_die($result);
}