<?php get_header(); ?>
<div class="c-home">
	<main id="Main" class="c-main-content o-main" role="main">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<div class="c-home-top">
			<div class="c-home-banner">
				<?php
					$banners = get_field('banners') ;
					$badges = product_badges();

				foreach ($banners as $item) :
					$is_video = false;
					if ($item['imagevideo'] && $item['video_file']) {
						$is_video = true;
					} ?>
					<div class="c-home-banner__wrap animate-special <?php echo ($is_video ? 'c-home-banner__video-wrap' : ''); ?>" style="background-color:<?php echo $item['background_color']; ?>;background-image: url(<?php echo ASSETS; ?>/img/bg-paper-texture.png);">
						<?php if ($is_video) : ?>
							<div class="c-home-banner__video">
								<video class="c-bg-video" playsinline="true" preload="metadata" autoplay="true" muted="true" loop="">
									<source src="<?php echo $item['video_file']; ?>" type="video/mp4">
								</video>
							</div>
						<?php else : ?>
							<div class="c-home-banner__sidebg">
								<div class="c-home-banner__sidebg-img parallax-img" style="background-image: url(<?php echo ASSETS; ?>/img/banner/Cobs-Homepage-RoundedCornerBackground.jpg)"></div>
							</div>
							<div class="c-home-banner__left-deco">
								<img data-lazy="<?php echo ASSETS; ?>/img/banner/Cobs-home-foodasset-bowl.png" class="c-home-banner__left-deco--img-1 lazyload" alt="Product Decorations">
								<span class="c-popcorn c-popcorn--home-banner-1"><span class="js-parallax"></span></span>
								<span class="c-popcorn c-popcorn--home-banner-2"><span class="js-parallax"></span></span>
								<span class="c-popcorn c-popcorn--home-banner-3"><span class="js-parallax"></span></span>
							</div>
							<div class="c-home-banner__products">
								<?php $product = $item['select_product'];
									$thumb = get_the_post_thumbnail_url($product);
									$thumb_back = get_field('product_back_image', $product);
								 ?>
								<?php if ($thumb) : ?>
									<div class="c-home-banner__product-thumb">
										<img src="<?php echo ASSETS; ?>/img/banner/explosion-left.png" class="c-home-banner__explosion-1" alt="Popcorn1">
										<img src="<?php echo ASSETS; ?>/img/banner/explosion-middle.png" class="c-home-banner__explosion-2" alt="Popcorn2">
										<img src="<?php echo ASSETS; ?>/img/banner/explosion-right.png" class="c-home-banner__explosion-3" alt="Popcorn3">
										<img src="<?php echo ASSETS; ?>/img/popcorn.png" class="c-home-banner__explosion-4" alt="Popcorn1">
										<img src="<?php echo ASSETS; ?>/img/popcorn4.png" class="c-home-banner__explosion-5" alt="Popcorn1">
										<a href="<?php echo get_the_permalink($product); ?>"><img src="<?php echo $thumb ?>" class="c-home-banner__products-1 js-load-img" alt="<?php echo get_the_title($product); ?>"></a>
									</div>
								<?php endif; ?>
								<!-- <?php if ($thumb_back['url']) : ?> -->
									<a href="<?php echo get_the_permalink($product); ?>"><img data-lazy="<?php echo $thumb ?>" class="c-home-banner__products-2" alt="<?php echo get_the_title($product); ?>"></a>
								<!-- <?php endif; ?> -->
							</div>
							<div class="c-home-banner__badges">
								<?php
									foreach ($item['select_product_badges'] as $value) {
										if ($badges[ $value['value'] ]) {
											echo $badges[ $value['value'] ];
										} else {
											echo '<span class="c-home-banner__badges-custom"><span class="c-home-banner__badges-custom-text">' . $value['label'] . '</span></span>';
										}
									}
								 ?>
							</div>
						<?php endif; ?>
						<?php if ($item['contents']['heading'] || $item['contents']['copy']) : ?>
							<div class="c-home-banner__contents">
								<?php if ($item['new_product_label']) : ?>
									<span class="c-home-banner__newtag"><?php svgicon('new-tag', '0 0 95 98'); ?></span>
								<?php endif; ?>
								<h2><?php echo $item['contents']['heading']; ?></h2>
								<p><?php echo $item['contents']['copy'] ?></p>
								<?php if ($is_video) : ?>
									<a href="javascript:void(0);" class="o-btn o-btn--white  o-btn--video js-open-video-modal" data-video="<?php echo $item['video_file']; ?>"><?php svgicon('play', '0 0 14 16'); ?> Watch video</a>
								<?php else : ?>
									<a href="/products" class="o-btn o-btn--white">Our products</a>
								<?php endif; ?>
								<a href="/where-to-buy" class="o-btn o-btn--outline">Where to buy</a>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="c-home-top__scroll animate">
				<a href="javascript:void(0);" class="js-scroll-down">Scroll down for our full range <?php svgicon('scroll-down', '0 0 23 22'); ?></a>
			</div>
		</div>
		<?php $product_sec = get_field('product_section'); ?>
		<div class="c-home-products">
			<span class="c-popcorn c-popcorn--home-6 left"><span class="js-parallax"></span></span>
			<span class="c-popcorn c-popcorn--home-7 left"><span class="js-parallax"></span></span>
			<div class="o-wrapper o-wrapper--large">
				<h1 class="c-home-title h1-alt animate"><?php echo $product_sec['heading']; ?></h1>
				<p class="c-home-copy animate"><?php echo $product_sec['copy']; ?></p>
				<?php
					if ($product_sec['products'] && count($product_sec['products']) > 0) {
						$products = array();
						foreach ($product_sec['products'] as $item) {
							$products[] = $item['select_products'];
						}
					} else {
						$args = array(
							'post_type'      => 'ra-products',
							'post_status'    => 'publish',
							'posts_per_page' => 12,
							'orderby' => 'date',
							'order' => 'DESC',
							'fields' => 'ids'
						);
						$products = get_posts( $args );
					}
					if (count($products) > 0) : ?>
						<div class="c-home-products__list animate">
							<div class="c-home-products__slider js-products__slider">
								<?php foreach ($products as $item) :
								$thumb = get_the_post_thumbnail_url($item);
								$left_garnish = get_field('left_garnish_element', $item);
								$right_garnish = get_field('right_garnish_element', $item);	 ?>
								<?php if ($thumb) : ?>
									<div class="c-home-products__slide c-product-tile">
										<a class="c-product-tile__img"  href="<?php echo get_the_permalink($item); ?>">
											<img data-lazy="<?php echo $thumb; ?>" class="c-product-tile__thumb" alt="<?php echo get_the_title($item); ?>">
											<?php if ($left_garnish || $right_garnish) : ?>
												<div class="c-product-tile__garnish">
													<?php if ($left_garnish) : ?>
														<img data-src="<?php echo $left_garnish; ?>" class="c-product-tile__garnish-left lazyload" alt="Garnish Element">
													<?php endif; ?>
													<?php if ($right_garnish) : ?>
														<img data-src="<?php echo $right_garnish; ?>" class="c-product-tile__garnish-right lazyload" alt="Garnish Element">
													<?php endif; ?>
												</div>
											<?php endif; ?>
											<div class="c-product-tile__info">
												<p class="c-product-tile__title"><?php echo get_the_title($item); ?></p>
												<div class="content"><?php echo wp_trim_words(get_the_excerpt($item), 8) ; ?>&nbsp;<span class="c-product-tile__more">(read more)</span></div>
											</div>
										</a>
									</div>
								<?php endif; ?>
								<?php endforeach; ?>
							</div>
							<?php if ($product_sec['button']) : ?>
								<div class="c-home-products__cta animate">
									<a href="<?php echo $product_sec['button']['url']; ?>" class="o-btn"><?php echo $product_sec['button']['title']; ?></a>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<?php $about_sec = get_field('about_section'); ?>
			<div class="c-home-about">
				<span class="c-popcorn c-popcorn--home-1 left"><span class="js-parallax"></span></span>
				<span class="c-popcorn c-popcorn--home-2 left"><span class="js-parallax"></span></span>
				<span class="c-popcorn c-popcorn--home-3 left"><span class="js-parallax"></span></span>
				<span class="c-popcorn c-popcorn--home-4 right"><span class="js-parallax"></span></span>
				<span class="c-popcorn c-popcorn--home-5 right"><span class="js-parallax"></span></span>
				<div class="o-wrapper o-wrapper--mid animate">
					<h2 class="h1-alt c-home-title"><?php echo $about_sec['heading']; ?></h2>
					<p class="c-home-copy"><?php echo $about_sec['copy']; ?></p>
					<?php if ($about_sec['image']) : ?>
						<div class="c-home-about__image">
							<img data-src="<?php echo $about_sec['image']; ?>" class="lazyload" alt="Cobs Image">
						</div>
					<?php endif; ?>
					<?php if ($about_sec['button']) : ?>
						<div class="c-home-about__cta">
							<a href="<?php echo $about_sec['button']['url']; ?>" class="o-btn"><?php echo $about_sec['button']['title']; ?></a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endwhile; ?>
	</main>
</div>

<div class="c-video-modal">
	<a href="javascript:void(0);" class="c-video-modal__close js-modal-close"><?php svgicon('close', '0 0 20 19'); ?></a>
	<div class="c-video-modal__container">
		<video playsinline="true" controls="true"></video>
	</div>
</div>

<?php get_footer(); ?>