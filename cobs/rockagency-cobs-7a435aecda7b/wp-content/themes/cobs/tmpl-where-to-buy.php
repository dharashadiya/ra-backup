<?php
/*
 * Template Name: Where to buy Page
 */
?>

<?php get_header(); ?>

<div class="c-wtob">
	<span class="c-popcorn c-popcorn--faq-1 left"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--faq-2 right"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--faq-3 right"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--faq-4 left"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--faq-5 left"><span class="js-parallax"></span></span>
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<?php if (get_field('show_feature_image_on_stage')) : ?>
			<div class="c-page-banner">
				<div class="c-page-banner__img js-parallax parallax-img lazyload" data-src="<?php echo get_the_post_thumbnail_url(); ?>"></div>
				<h1 class="page-title c-page-banner__title animate"><?php the_title(); ?></h1>
			</div>
		<?php endif; ?>
		<div class="c-cms-content">
			<?php $sections = get_field('logo_sections');  ?>
			<?php if ($sections ) : ?>
				<?php foreach ($sections as $item) : ?>
					<div class="c-wtob__list animate">
						<div class="o-wrapper o-wrapper--small c-wtob__list-header">
							<?php if ( $item['heading']) : ?>
								<h3><?php echo $item['heading']; ?></h3>
							<?php endif; ?>
							<?php if ($item['copy']) : ?>
								<p><?php echo $item['copy']; ?></p>
							<?php endif; ?>
						</div>
						<div class="o-wrapper">
							<div class="o-layout o-module">
								<?php foreach ($item['logos'] as $key => $logo) : ?>
									<div class="o-layout__item o-module__item u-1/4@tabletWide u-1/3@mobileLandscape u-1/2">
									<?php if ($logo['logo_link']) : ?>
										<a href="<?php echo $logo['logo_link']; ?>" target="_blank" class="c-wtob__list-each">
									<?php else : ?>
										<a href="javascript:void(0);" class="c-wtob__list-each no-link">
										<?php endif; ?>
											<img data-src="<?php echo $logo['logo']; ?>" class="lazyload" alt="Client Logo">
											<?php if ($logo['label']) : ?>
												<span><?php echo $logo['label']; ?></span>
											<?php endif; ?>
										</a>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	<?php endwhile; ?>
</div>

<?php get_footer(); ?>
