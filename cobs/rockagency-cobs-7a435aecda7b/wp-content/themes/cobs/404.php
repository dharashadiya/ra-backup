<?php  get_header(); ?>

<div class="c-404">
	<main id="Main" class="c-main-content o-main" role="main">
		<article>
			<div class="c-404__overlay">
				<div class="c-404__overlay-content c-cms-content o-wrapper">
					<h1 class="page-title">404</h1>
					<h3> Page Not found</h3>
					<a href="/" class="o-btn o-btn--white" title="Back to home">Back to homepage</a>
				</div>
				<img class="c-404__product" src="<?php echo ASSETS; ?>/img/product.png" alt="">
			</div>
		</article>
	</main>
</div>

<?php get_footer(); ?>