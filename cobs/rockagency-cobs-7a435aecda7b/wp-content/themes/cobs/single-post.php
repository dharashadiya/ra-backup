<?php get_header(); ?>
<div class="c-single-news">
	<span class="c-popcorn c-popcorn--s-news-1 right"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--s-news-2 right"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--s-news-3 left"><span class="js-parallax"></span></span>
	<span class="c-popcorn c-popcorn--s-news-4 left"><span class="js-parallax"></span></span>
	<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
			<div class="o-wrapper o-wrapper--small">
				<h1 class="page-title c-single-news__title animate"><?php the_title(); ?></h1>
				<div class="c-single-news__date animate">
					<?php echo get_the_date('F j, Y'); ?>&nbsp;<span class="c-single-news__category"><strong>Category: </strong>
					<?php $category = get_the_terms(get_the_id(), 'category');
					//foreach ($category as $key => $cats) {
						// if ($cats->slug !== 'uncategorised') {
						// 	echo ($key !== 0 ? ', ' : '') . $cats->name;
						// }
					//}
					echo $category[0]->name;
					?>
					</span>
				</div>
			</div>
			<div class="o-wrapper">
				<div class="c-single-news__banner animate lazyload" data-src="<?php echo get_the_post_thumbnail_url()?>"></div>
			</div>
			<div class="c-single-news__contents c-cms-content o-wrapper o-wrapper--small animate-elems">
				<?php the_content(); ?>
				<p class="c-single-news__getin">Get in touch: <a href="mailto:cobs@cobs.com.au">cobs@cobs.com.au</a></p>
				<div class="c-single-news__sharelinks">
					<span class="share-span">Share:</span>
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>" target="_blank"><?php echo svgicon('facebook', '0 0 18 18'); ?></a>
					<!-- <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_the_permalink(); ?>" target="_blank"><?php echo svgicon('instagram', '0 0 18 18'); ?></a> -->
					<a href="mailto:?&subject=&body=<?php echo get_the_permalink(); ?>" target="_blank"><?php echo svgicon('email', '0 0 18 18'); ?></a>
					<a href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink(); ?>" target="_blank"><?php echo svgicon('twitter', '0 0 18 18'); ?></a>
				</div>
			</div>
	<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>