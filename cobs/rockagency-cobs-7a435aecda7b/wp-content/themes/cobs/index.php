<?php get_header(); ?>

<div class="c-post">
	<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
			<div class="c-cms-content o-wrapper o-wrapper--small">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
	<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>