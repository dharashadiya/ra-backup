// Provides way to work with files & directories
const path = require('path');
const gulp = require('gulp');
const prettier = require('gulp-prettier');
const prompt = require('gulp-prompt')
const zip = require('gulp-zip');
const webpack = require('webpack-stream')
const gulpLoadPlugins = require('gulp-load-plugins');
const plugins = gulpLoadPlugins();
const webpackConfig = require('./webpack.config')
const sftp = require('gulp-sftp-up4');
const terser = require('gulp-terser');

const themeFiles = [
	'**!(bower_components)/*.php',
	'*.php',
	'*/**/*.php',
	'*unctions/acf-jsons/*.json',
	'*ssets/lib/*.js',
	'*ssets/js/dist/*.min.js',
	'*ssets/*.js',
	'*ssets/css/*.min.css',
	'*ssets/fonts/*.{eot,svg,ttf,woff,woff2,otf}',
	'*ssets/img/inline/*.svg',
	'*ssets/img/*.{svg,ico,jpg,jpeg,png,gif}',
	'*ssets/img/*/*.{svg,ico,jpg,jpeg,png,gif}',
	'style.css'
]

const credentials = {
	staging: {
		host: '103.27.32.31',
		auth: 'staging',
		port: '2683',
		remotePath: '/home/rockagen/public_html/staging/cobs/wp-content/themes/cobs/'
	},
	production: {
		host: '',
		auth: '',
		port: '',
		remotePath: ''
	}
}

gulp.task('styles', () => {
	return gulp.src('assets/sass/screen.scss')
		.pipe(plugins.sass({
				outputStyle: 'expanded'
			})
			.on('error', plugins.sass.logError))
		.pipe(plugins.rename((file) => {
			return file.dirname = file.dirname.replace(path.sep + 'sass', path.sep + 'css');
		}))
		.pipe(gulp.dest('assets/css'))
		.pipe(plugins.autoprefixer({
			overrideBrowserslist: ['last 3 versions'],
			grid: true
		}))
		.pipe(plugins.rename('screen.dev.css'))
		.pipe(gulp.dest('assets/css'))
		.pipe(plugins.cssmin())
		.pipe(plugins.concat('screen.min.css'))
		.pipe(gulp.dest('assets/css'));
});

gulp.task('webpack', function () {
	return gulp.src('assets/js/src/base.js')
		.pipe(webpack(webpackConfig))
		.pipe(gulp.dest('assets/js/dist/'));
});

gulp.task('format-js', () => {
	return gulp.src([
			'assets/js/*.js',
			'assets/js/!(site).js',
		])
		.pipe(prettier({
			tabWidth: 4,
			useTabs: true,
			trailingCommas: 'es5'
		}))
		.pipe(gulp.dest('assets/js/'))
})

gulp.task('format-sass', () => {
	return gulp.src('assets/sass/**/**.scss')
		.pipe(prettier({
			tabWidth: 4,
			useTabs: true,
		}))
		.pipe(gulp.dest('assets/sass/'))
})

// Execute format-js & format-sass simultaneously
gulp.task('format', gulp.parallel('format-js', 'format-sass'), (done) => {
	done()
})

gulp.task('uglify', () => {
	return gulp.src([
			'node_modules/jquery/dist/jquery.js',
			'node_modules/svg4everybody/dist/svg4everybody.js',
			'node_modules/jquery-lazy/jquery.lazy.js',
			'node_modules/imagesloaded/imagesloaded.pkgd.js',
			'node_modules/body-scroll-lock/lib/bodyScrollLock.js',
			'assets/lib/jquery.viewportchecker.min.js',
			'assets/lib/jquery.cookie.js',
			'assets/lib/jquery.sonar.js',
			'assets/lib/slick.js',
			'assets/lib/parallax.js',
			'assets/js/dist/bundle.js'
		])
		.pipe(plugins.concat('site.js'))
		.pipe(terser())
		.pipe(gulp.dest('assets/js/dist'))
		.pipe(plugins.rename({
			extname: ".min.js"
		}))
		.pipe(gulp.dest('assets/js/dist'));
});

gulp.task('svgstore', () => {
	return gulp.src('assets/img/inline/*.svg')
		.pipe(plugins.svgmin((file) => {
			return {
				plugins: [{
					cleanupIDs: {
						prefix: path.basename(file.relative, path.extname(file.relative)) + '-',
						minify: true
					}
				}]
			};
		}))
		.pipe(plugins.svgstore())
		.pipe(gulp.dest('assets/img'));
});

// Adds script to render next-gen-new-technologies in new browsers
// Also keeps the website compatible with old browsers
gulp.task('modernizr', () => {
	return gulp.src('assets/**/*.scss')
		.pipe(plugins.modernizr({
			options: ["setClasses"],
			tests: ['inlinesvg']
		}))
		.pipe(terser())
		.pipe(plugins.concat('inlinescripts.min.php'))
		.pipe(plugins.header('<script>\n\t/* Contains custom build of Modernizr */\n\t'))
		.pipe(plugins.footer('\n</script>'))
		.pipe(gulp.dest("./partials"));
});

gulp.task('watch', () => {
	gulp.watch('assets/js/src/*.js', gulp.series('webpack', 'uglify'));
	gulp.watch('assets/sass/**/*.scss', gulp.series('styles'));
	gulp.watch('assets/img/inline/*.svg', gulp.series('svgstore'));
});


// Deploy site on staging environment
// Requirments: 1.should have .ftpass file in wp-content folder,	2. Change the folder name to your-theme's staging folder name
// Can create similar task to deploy website on live site (not recommended until extremely necessary)
gulp.task('sftp', () => {
	return gulp.src(themeFiles)
		.pipe(sftp(credentials.staging));
});

gulp.task('push-live', () => {
	return gulp.src(themeFiles)
		.pipe(prompt.confirm('Always take a backup. Are you sure you want to push the site live?'))
		.pipe(sftp(credentials.production));
});

gulp.task('package-theme', () => {
	const themeName = __dirname.split('/').pop()
	return gulp.src(themeFiles)
		.pipe(zip(`${themeName}.zip`))
		.pipe(gulp.dest('../'))
})

gulp.task('default', gulp.series('webpack', 'format', 'styles', 'uglify', 'svgstore', 'modernizr', ['watch']));

gulp.task('build-theme', gulp.series('webpack', 'format', 'styles', 'uglify', 'svgstore', 'modernizr', 'package-theme'));

gulp.task('deploy', gulp.series('webpack', 'format', 'styles', 'uglify', 'svgstore', 'modernizr', 'sftp'));

gulp.task('push-production', gulp.series('webpack', 'format', 'styles', 'uglify', 'svgstore', 'modernizr', 'push-live'));