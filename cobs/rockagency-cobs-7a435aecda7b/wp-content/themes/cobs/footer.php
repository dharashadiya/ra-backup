<?php if (!is_404()) : ?>
	<div class="c-social-posts">
		<span class="c-popcorn c-popcorn--footer-1 right"><span class="js-parallax"></span></span>
		<span class="c-popcorn c-popcorn--footer-2 right"><span class="js-parallax"></span></span>
		<span class="c-popcorn c-popcorn--footer-3 left"><span class="js-parallax"></span></span>
		<span class="c-popcorn c-popcorn--footer-4 left"><span class="js-parallax"></span></span>
		<div class="o-wrapper o-wrapper--mid">
			<div class="c-social-posts__wrap animate">
				<div class="c-social-posts__top">
					<div class="c-social-posts__header">
						<h2 class="h1-alt">Follow us</h2>
						<a href="https://www.instagram.com/cobspopcorn/" target="_blank"><span><?php svgicon('instagram', '0 0 24 24'); ?></span> @cobspopcorn</a>
					</div>
					<!-- <a class="c-social-posts__top-cta o-btn" href="javascript:void(0);">See more posts</a> -->
				</div>
				<div class="c-social-posts__list">
					<?php echo do_shortcode('[instagram-feed]') ?>
					<div class="c-social-posts__fade"></div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
</div><!-- .c-content -->
<footer class="c-footer">
	<div class="o-wrapper">
		<div class="c-footer__top">
			<a href="/" class="c-footer__logo"><img src="<?php echo ASSETS; ?>/img/logo.png" alt="Logo"></a>
			<?php $page_links = get_field('page_links', 'options'); ?>
			<div class="c-footer__links">
				<div class="c-footer__links-top">
					<ul>
						<?php foreach ($page_links as $item) : ?>
							<li><a href="<?php echo $item['link']['url']; ?>"><?php echo $item['link']['title']; ?></a></li>
						<?php endforeach; ?>
					</ul>

				</div>
				<div class="c-footer__links-bottom">
					<ul>
						<li><a href="/disclaimers/">Disclaimer</a></li>
						<li><a href="/terms-conditions/">Terms&nbsp;&amp;&nbsp;Conditions</a></li>
						<li><a href="/privacy-policy/">Privacy</a></li>
						<!-- <li><a href="/distributors/">Distributors</a></li> -->
					</ul>
				</div>
			</div>
			<?php echo do_shortcode('[social-links]'); ?>
		</div>
		<div class="c-footer__copy">
			<p><span>&copy;&nbsp;Copyright&nbsp;Cobs&nbsp;Popcorn&nbsp;<?php echo date("Y"); ?></span><span>Built&nbsp;by&nbsp;<a href="https://rockagency.com.au/">Rock&nbsp;Agency</a></span></p>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>