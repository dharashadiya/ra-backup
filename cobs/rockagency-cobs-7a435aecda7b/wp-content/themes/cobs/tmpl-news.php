<?php
/*
 * Template Name: News Page
 */
?>

<?php get_header(); ?>

<div class="c-news">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	<div class="c-news__wrap" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="o-wrapper">
			<?php $ads = get_field('ads_module');
			$is_video = $ads['imagevideo']; ?>
			<?php if($ads['heading'] || $ads['image'] || $ads['video'] ) : ?>
				<div class="c-news__ads animate">
					<a href="javascript:void(0);" class="c-news__ads-close js-ads-close"><?php svgicon('close', '0 0 20 19'); ?></a>
						<div class="c-news__ads-wrap lazyload <?php echo ($ads['imagevideo'] ? 'c-news__ads-videowrap' : ''); ?>" data-src="<?php echo $ads['image']; ?>" data-mob-src="<?php echo $ads['mobile_image']; ?>">
							<?php if ($is_video) : ?>
								<video class="c-bg-video" playsinline="true" autoplay="true" muted="true" loop="">
									<source src="<?php echo $ads['video'] ; ?>" type="video/mp4">
								</video>
							<?php endif; ?>
							<div class="c-news__ads-wrap-content animate">
								<span><?php echo $ads['label']; ?></span>
								<h2 class="page-title"><?php echo $ads['heading']; ?></h2>
								<?php if (!$is_video && $ads['button']) : ?>
									<a href="<?php echo $ads['button']['url']; ?>" class="o-btn o-btn--white"><?php echo $ads['button']['title']; ?></a>
								<?php else : ?>
									<a href="javascript:void(0);" class="o-btn o-btn--white  o-btn--video js-open-video-modal" data-video="<?php echo $ads['video']; ?>"><?php svgicon('play', '0 0 14 16'); ?> Watch video</a>
								<?php endif; ?>
							</div>
						</div>
				</div>
			<?php endif; ?>
			<div class="c-news__contents animate">
				<h3 class="c-news__title"><?php the_title(); ?></h3>
				<?php the_content(); ?>
			</div>
			<?php $categories = get_categories( array(
				'orderby' => 'name',
				'order'   => 'ASC'
			)); ?>
			<div class="c-news__search animate">
				<input class="input-serach" type="text" placeholder="Search..." id="news-search-input">
			</div>
			<?php foreach( $categories as $category )	?>
			<div class="c-news__categories animate">
				<a href="javascript:void(0);" class="c-news__categories-each js-news-filter is-active" data-filter="all">All</a>
				<?php foreach( $categories as $key => $category ) : ?>
					<a href="javascript:void(0);" class="c-news__categories-each js-news-filter" data-filter="<?php echo $category->slug; ?>"><?php echo $category->name; ?></a>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="c-news__list-wrap">
			<div class="o-wrapper">
				<?php
				$posts_per_page = 9;
				$args = array(
					"post_type" => 'post',
					"numberposts" => 9999999,
					"post_status" => "publish",
					"fields" => "ids"
				);

				$post_count = get_posts($args);
				$post_count = count($post_count);
				$args['numberposts'] = $posts_per_page;
				$args['offset'] = 0;
				$posts = get_posts( $args );

				$show_loadmore = false;
				if ($post_count > count($posts)) {
					$show_loadmore = true;
				}

				?>
				<span class="c-popcorn c-popcorn--news-1 right"><span class="js-parallax"></span></span>
				<span class="c-popcorn c-popcorn--news-2 right"><span class="js-parallax"></span></span>
				<span class="c-popcorn c-popcorn--news-3 left"><span class="js-parallax"></span></span>
				<span class="c-popcorn c-popcorn--news-4 left"><span class="js-parallax"></span></span>
				<span class="c-popcorn c-popcorn--news-5 left"><span class="js-parallax"></span></span>
				<div class="c-news__list o-layout o-module">
					<?php foreach ($posts as $item) :?>
						<?php
							set_query_var('var', array('ID' => $item));
							get_template_part('partials/loop-news');
						?>
					<?php endforeach; ?>
				</div>
				<div class="c-news__loadmore animate">
					<a href="javascript:void(0);"
						class="o-btn js-loadmore-posts"
						data-total="<?php echo $post_count; ?>"
						style="<?php echo ($show_loadmore ? '' : 'display: none;'); ?>"
						>Load more articles</a>
				</div>
			</div>
		</div>
	</div>
	<?php endwhile; ?>
</div>

<div class="c-video-modal">
	<a href="javascript:void(0);" class="c-video-modal__close js-modal-close"><?php svgicon('close', '0 0 20 19'); ?></a>
	<div class="c-video-modal__container">
		<video playsinline="true" controls="true"></video>
	</div>
</div>
<?php get_footer(); ?>