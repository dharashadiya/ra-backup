<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
	<title><?php wp_title(); ?> </title>

	<?php if(is_local_or_staging()) : ?>
		<meta name="robots" content="none" />
		<meta name="robots" content="noindex" />
		<meta name="googlebot" content="none" />
		<meta name="googlebot" content="noindex" />
	<?php endif; ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<?php
    	$preview = get_the_post_thumbnail_url(get_the_ID(), 'full');
		if (!$preview) {
      		$preview = ASSETS . '/img/logo.png';
    	}
   	?>
	<?php if ($preview) : ?>
		<meta property="og:image" content="<?php echo $preview ?>" />
	<?php endif; ?>
	<meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">
	<?php wp_head(); ?>
	<?php include('partials/inlinescripts.min.php'); ?>
</head>

<body <?php body_class(); ?>>

<!-- loading -->
<?php //if (is_front_page()) : ?>
	<div class="c-loading hided">
		<div class="c-loading__content">
			<a href="/"><img src="<?php echo ASSETS; ?>/img/logo.png" alt="Logo"></a>
		</div>
	</div>
<?php //endif; ?>
<?php
$top_classes = '';
$stage = get_field('show_feature_image_on_stage');
if (!$stage && !is_front_page() && !is_singular('ra-products')) {
	$top_classes .= ' green';
} ?>
<div class="c-top <?php echo $top_classes; ?>">
	<header class="c-header o-wrapper">
		<div class="c-header__social-mobile">
			<?php echo do_shortcode('[social-links]'); ?>
		</div>
		<?php
			$logo_wrap_class = "c-logo-wrap";
			if (!is_front_page()) {
				$logo_wrap_open = '<a class="' . $logo_wrap_class . ' is-link" href="/" rel="home" title="Back to home">';
				$logo_wrap_close = '</a>';
			} else {
				$logo_wrap_open = '<span class="' . $logo_wrap_class . '">';
				$logo_wrap_close = '</span>';
			}
		?>
		<?php
			echo $logo_wrap_open;
			echo '<img src="'. ASSETS .'/img/logo.png" alt="Logo" class="c-header__logo animate-zoomin"/>';
			echo $logo_wrap_close;
		?>
		<a href="#Main" class="c-skip">Skip to main content</a>
		<a href="javascript:void(0);"  class="c-header__nav-toggle"><span></span></a>
		<nav class="c-site-nav animate-slidetop" role="navigation">
		<?php
			wp_nav_menu( array(
				'theme_location' => 'primary-menu',
				'container' => false,
				'menu_class' => 'c-nav',
				'menu_id' => 'menu'
			));
		?>
		</nav>
		<div class="c-header__social animate">
			<?php echo do_shortcode('[social-links]'); ?>
		</div>
	</header>
</div>

<div class="c-content">