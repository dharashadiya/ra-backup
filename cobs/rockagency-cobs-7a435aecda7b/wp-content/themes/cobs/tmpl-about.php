<?php
/*
 * Template Name: About Page
 */
?>

<?php get_header(); ?>

<div class="c-about">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	<div class="c-about__wrap" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<span class="c-popcorn c-popcorn--about-1 left"><span class="js-parallax"></span></span>
		<span class="c-popcorn c-popcorn--about-2 left"><span class="js-parallax"></span></span>
		<span class="c-popcorn c-popcorn--about-3 right"><span class="js-parallax"></span></span>
		<span class="c-popcorn c-popcorn--about-4 right"><span class="js-parallax"></span></span>
		<div class="c-cms-content o-wrapper o-wrapper--mid">
			<h1 class="page-title c-about__title animate"><?php the_title(); ?></h1>
			<?php $story = get_field('story_slider');
			if ($story) : ?>
				<div class="c-about__story animate">
					<?php foreach ($story as $item) : ?>
						<div class="c-about__story-item" style="background-image: url(<?php echo $item['image'] ?>)">
							<div class="c-about__story-item-content">
								<h3><?php echo $item['heading']; ?></h3>
								<p><?php echo $item['content']; ?></p>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="c-about__story-nav">
					<?php foreach ($story as $item) : ?>
						<div class="c-about__story-nav-item">
							<a href="javascript:void(0);"><?php svgicon('nav-asset', '0 0 47 44'); ?><?php echo $item['year']; ?></a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>


			<div class="c-about__contents animate">
				<?php if (get_the_content()) : ?>
					<?php the_content(); ?>
					<a href="/products" class="o-btn">Explore our entire range</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php endwhile; ?>
</div>
<?php get_footer(); ?>