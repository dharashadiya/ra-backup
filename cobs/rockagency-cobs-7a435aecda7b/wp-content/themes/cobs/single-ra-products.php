<?php get_header(); ?>
<div class="c-single-product">
	<main id="Main" class="c-main-content o-main" role="main">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<?php $stage = get_field('product_stage');
		$thumb = get_the_post_thumbnail_url();
		$brand = get_the_terms(get_the_ID(), 'ra-brand');
		$badge = get_field('product_badge');
		$custom_badge = get_field('custom_badge_text');
		$ingredients = get_field('ingredients');
		$back_image = get_field('product_back_image');
		$available_in = get_field('available_in');
		$next_post = get_previous_post();
		$prev_post = get_next_post();
		?>
		<div class="c-single-product__stage animate-special">
			<div class="o-wrapper">
				<div class="o-layout">
					<div class="o-layout__item u-1/2@tablet animate">
						<h1 class="page-title c-single-product__stage-title"><?php the_title(); ?></h1>
						<div class="c-single-product__stage-content">
							<div class="c-single-product__stage-brief content-readmore">
								<?php the_content(); ?>
							</div>
							<a href="javascript:void(0);" class="c-single-product__stage-more js-readmore" data-target=".c-single-product__stage-brief" hidden>(Read more)</a>
						</div>
						<div class="c-single-product__stage-ctas">
							<?php if ($ingredients && count($ingredients) > 0): ?>
								<a href="javascript:void(0);" class="o-btn o-btn--white js-scroll-dietary">Dietary information</a>
							<?php endif; ?>
							<?php if ($stage['buy_now_button']) : ?>
								<a href="<?php echo $stage['buy_now_button']; ?>" class="o-btn o-btn--outline">Buy Now</a>
							<?php endif; ?>
						</div>
					</div>
					<div class="o-layout__item u-1/2@tablet">
						<div class="c-single-product__stage-right">
							<?php if ($thumb) : ?>
							<div class="c-single-product__stage-thumb animate">
								<a href="javascript:void(0);" class="js-open-product-overlay">
									<img class="c-single-product__stage-thumb-main lazyload" src="<?php echo ASSETS; ?>/img/product-placeholder.gif" data-src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>">
								</a>
								<?php if ($badge) :  $badges = product_badges(); ?>
									<span class="c-single-product__stage-badge">
										<?php if ($badges[$badge['value']]) : ?>
											<?php echo $badges[$badge['value']]; ?>
										<?php else : ?>
											<?php if (get_field('custom_badge_text', $item)) : ?>
												<span class="c-single-product__stage-badge-custom"><span class="c-single-product__stage-badge-custom-text"><?php echo get_field('custom_badge_text', $item); ?></span></span>
											<?php endif; ?>
										<?php endif; ?>
									</span>
								<?php endif; ?>

							</div>
							<?php endif; ?>
							<?php if ($stage['features']) : ?>
								<div class="c-single-product__stage-features animate">
									<?php foreach ($stage['features'] as $item) : ?>
										<p><?php svgicon('list-style', '0 0 18 18'); ?><?php echo $item['feature']; ?></p>
									<?php endforeach; ?>
									<?php if ($stage['label']) : ?>
										<p class="c-single-product__stage-features-label"><?php echo $stage['label']; ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="c-single-product__stage-nav">
					<?php if ($prev_post) : ?>
						<a href="<?php echo get_permalink( $prev_post->ID); ?>" class="c-single-product__stage-nav-prev" title="Previous product"><?php svgicon('prev', '0 0 62 62'); ?></a>
					<?php endif; ?>
					<?php if ($next_post) : ?>
						<a href="<?php echo get_permalink($next_post->ID); ?>" class="c-single-product__stage-nav-next" title="Next product"><?php svgicon('next', '0 0 62 62'); ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php if ($ingredients && count($ingredients) > 0) : ?>
			<div class="c-single-product__ing">
				<span class="c-popcorn c-popcorn--s-product-1 right"><span class="js-parallax"></span></span>
				<span class="c-popcorn c-popcorn--s-product-2 right"><span class="js-parallax"></span></span>
				<div class="o-wrapper">
					<div class="c-single-product__ing-conainer animate">
						<div class="c-single-product__ing-content">
							<div class="o-layout">
								<div class="o-layout__item u-1/2@tablet">
									<div class="c-single-product__ing-img">
										<?php if ($thumb) : ?>
											<img class="c-single-product__ing-front lazyload js-open-product-overlay" src="<?php echo ASSETS; ?>/img/product-placeholder.gif" data-src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>">
										<?php endif; ?>
										<?php if ($back_image && $back_image['url']) : ?>
											<img class="c-single-product__ing-back lazyload js-open-product-overlay" data-image="back" src="<?php echo ASSETS; ?>/img/product-placeholder.gif" data-src="<?php echo $back_image['url']; ?>" alt="<?php echo $back_image['title']; ?>">
										<?php endif; ?>
									</div>
									<?php if ($available_in && count($available_in) > 0) : ?>
										<div class="c-single-product__ing-available">
											<h4>Purchase online</h4>
											<ul>
												<?php foreach ($available_in as $item) : ?>
													<li><a href="<?php echo $item['link']; ?>" target="_blank">
														<img data-src="<?php echo $item['icon']; ?>" class="lazyload" alt="<?php echo $item['name']; ?>">
													</a></li>
												<?php endforeach; ?>
											</ul>
										</div>
									<?php endif; ?>
								</div>
								<div class="o-layout__item u-1/2@tablet">
									<h2 class="c-single-product__ing-title">Ingredients</h2>
									<?php foreach ($ingredients as $key => $item) : ?>
										<div class="c-single-product__ing-tabcontent js-tab-content"
												data-tab="<?php echo $key . '-' . stripString($item['tab_title']); ?>"
												<?php echo ($key === 0 ? '' : 'hidden'); ?>>
											<div class="c-single-product__ing-tabinner">
												<?php echo $item['tab_content']; ?>
												<?php if ($item['table']) :
													$table = $item['table'];
													?>
													<table>
														<?php if ($table['header']) : ?>
															<thead>
																<tr>
																	<?php foreach ($table['header'] as $c) : ?>
																		<th><?php echo $c['c']; ?></th>
																	<?php endforeach; ?>
																</tr>
															</thead>
														<?php endif; ?>
														<tbody>
															<?php foreach ($table['body'] as $r) : ?>
																<tr>
																	<?php foreach ($r as $c) : ?>
																		<td><?php echo $c['c']; ?></td>
																	<?php endforeach; ?>
																</tr>
															<?php endforeach; ?>
														</tbody>
													</table>
												<?php endif; ?>
											</div>
										</div>
									<?php endforeach; ?>
									<ul class="c-single-product__ing-tablinks">
										<?php foreach ($ingredients as $key => $item) : ?>
											<li>
												<a href="javascript:void(0);" class="c-single-product__ing-tablink js-tab-link <?php echo ($key === 0 ? 'is-active' : ''); ?>" data-tab="<?php echo $key . '-' . stripString($item['tab_title']); ?>"><?php echo $item['tab_title']; ?></a>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="c-single-product__sharelinks o-wrapper animate">
			<span class="share-span">Share:</span>
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>" target="_blank"><?php echo svgicon('facebook', '0 0 18 18'); ?></a>
			<!-- <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_the_permalink(); ?>" target="_blank"><?php echo svgicon('instagram', '0 0 18 18'); ?></a> -->
			<a href="mailto:?&subject=&body=<?php echo get_the_permalink(); ?>" target="_blank"><?php echo svgicon('email', '0 0 18 18'); ?></a>
			<a href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink(); ?>" target="_blank"><?php echo svgicon('twitter', '0 0 18 18'); ?></a>
		</div>

		<?php

			$comments = get_comments(array(
				'post_id' => get_the_ID(),
				'status' => 'approve',
				'number' => 20,
				'parent' => null
			));
		?>
		<div class="c-single-product__reviews animate">
			<span class="c-popcorn c-popcorn--s-product-3 left"><span class="js-parallax"></span></span>
			<h3 class="c-single-product__reviews-title">Latest Product Reviews</h3>
			<?php if (count($comments) > 0) : ?>
				<div class="c-single-product__reviews-list js-review-slider">
					<?php foreach ($comments as $comment) :
						$name = $comment->comment_author;
						$email = $comment->comment_author_email;
						$text = $comment->comment_content;
						$rating = get_comment_meta($comment->comment_ID, 'ra_rating', true);
						$stars = '';
						for ($i=0; $i < $rating; $i++) {
							$stars .= get_svgicon('star-full', '0 0 15 14');
						}
						?>
						<div class="c-single-product__reviews-each">
							<span><strong><?php echo $name; ?></strong></span>
							<span><?php echo $stars; ?></span>
							<span>“<?php echo $text; ?></span>
						</div>
					<?php endforeach; ?>
				</div>
			<?php else : ?>
			<p class="c-single-product__reviews-msg">Click the button below and be the first to review this product.</p>
			<?php endif; ?>
			<div class="c-single-product__reviews-cta">
				<a href="javascript:void(0);" class="o-btn js-open-modal">Tried this product? Leave&nbsp;us&nbsp;a&nbsp;review</a>
			</div>
		</div>

		<div class="c-single-product__related">
			<span class="c-popcorn c-popcorn--s-product-4 right"><span class="js-parallax"></span></span>
			<?php $args = array(
				'post_type'      => 'ra-products',
				'post_status'    => 'publish',
				'posts_per_page' => 4,
				'orderby' => 'date',
				'order' => 'DESC',
				'exclude' => get_the_ID(),
				'tax_query' => array(array(
					'taxonomy' => 'ra-brand',
					'field'    => 'slug',
					'terms' => $brand[0]->slug
				)),
				'fields' => 'ids'
			);
			$products = get_posts( $args );
			if (count($products) > 0) : ?>
				<div class="o-wrapper c-single-product__related-list animate">
					<h3 class="c-single-product__related-title">You may also be interested in</h3>
					<div class="js-products__slider slide-<?php echo $uniq_id; ?>">
						<?php foreach ($products as $item) :
						$thumb = get_the_post_thumbnail_url($item);
						$left_garnish = get_field('left_garnish_element', $item);
						$right_garnish = get_field('right_garnish_element', $item);
							?>
							<div class="c-product-tile">
								<?php if ($thumb) : ?>
									<a class="c-product-tile__img"  href="<?php echo get_the_permalink($item); ?>">
										<img data-lazy="<?php echo $thumb ; ?>" class="c-product-tile__thumb" alt="<?php echo get_the_title($item); ?>">
										<?php if ($left_garnish || $right_garnish) : ?>
											<div class="c-product-tile__garnish">
												<?php if ($left_garnish) : ?>
													<img src="<?php echo $left_garnish; ?>" class="c-product-tile__garnish-left" alt="Garnish Element">
												<?php endif; ?>
												<?php if ($right_garnish) : ?>
													<img src="<?php echo $right_garnish; ?>" class="c-product-tile__garnish-right" alt="Garnish Element">
												<?php endif; ?>
											</div>
										<?php endif; ?>
									</a>
								<?php endif; ?>
								<div class="c-product-tile__info">
									<a class="c-product-tile__title" href="<?php echo get_the_permalink($item); ?>"><?php echo get_the_title($item); ?></a>
									<div class="content"><?php echo wp_trim_words(get_the_content(null, false, $item), 10) ; ?>&nbsp;<span class="c-product-tile__more">(read more)</span></div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	<?php endwhile; ?>
	</main>
	<!-- Review form  -->
	<div class="c-review-form">
		<div class="c-review-form__container">
			<a href="javascript:void(0);" class="c-review-form__close"><?php svgicon('close', '0 0 20 19'); ?></a>
			<h3>Leave a review</h3>
			<p>Leave a review for this product by completing the form below</p>
			<form action="javascript:void(0);" class="c-review-form__form" id="product-review-form">
				<input type="hidden" name="post_id" value="<?php echo get_the_ID(); ?>">
				<div class="input-wrap">
					<input type="text" name="review-name" id="review-name" placeholder="Name*">
				</div>
				<div class="input-wrap">
					<input type="email" name="review-email" id="review-email" placeholder="Email address*">
				</div>
				<div class="input-wrap c-review-form__rating">
					<div class="label">Rating:</div>
					<div class="c-review-form__rating-stars">
						<input type="radio" name="rating" id="rating-five" value="5">
						<label for="rating-five">
							<?php svgicon('star', '0 0 34 32'); ?>
						</label>
						<input type="radio" name="rating" id="rating-four" value="4">
						<label for="rating-four">
							<?php svgicon('star', '0 0 34 32'); ?>
						</label>
						<input type="radio" name="rating" id="rating-three" value="3">
						<label for="rating-three">
							<?php svgicon('star', '0 0 34 32'); ?>
						</label>
						<input type="radio" name="rating" id="rating-two" value="2">
						<label for="rating-two">
							<?php svgicon('star', '0 0 34 32'); ?>
						</label>
						<input type="radio" name="rating" id="rating-one" value="1">
						<label for="rating-one">
							<?php svgicon('star', '0 0 34 32'); ?>
						</label>
					</div>
				</div>
				<div class="input-wrap">
					<textarea name="review-text" id="review-text" maxlength="280" placeholder="Review (max 280 characters)"></textarea>
				</div>
				<div class="input-wrap submit-wrap">
					<input type="submit" class="o-btn" value="Submit review">
				</div>
				<span class="c-review-form__msg" hidden></span>
			</form>
		</div>
	</div>
	<!-- product image overlay -->
	<div class="c-single-product__overlay">
		<div class="c-single-product__overlay-close"><?php svgicon('close', '0 0 20 19'); ?></div>
		<div class="c-single-product__overlay-content">
			<?php if ($thumb) : ?>
				<img class="c-single-product__overlay-front lazyload" data-src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>">
			<?php endif; ?>
			<?php if ($back_image && $back_image['url']) : ?>
				<img class="c-single-product__overlay-back" src="<?php echo $back_image['url']; ?>" alt="<?php echo $back_image['title']; ?>">
				<a href="javascript:void(0);" class="o-btn o-btn--white js-flip-product">Flip product <?php svgicon('flip', '0 0 26 18'); ?></a>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>