<?php get_header(); ?>

<div class="c-default">
	<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="c-cms-content o-wrapper o-wrapper--1100 animate-elems">
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php the_content(); ?>
		</article>
	<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>