<?php /* Template Name:  Services Page */ ?>

<?php get_header(); ?>
<div class="c-services" style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>)">
	<main id="Main" class="c-main-content o-main" role="main">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>

		<?php
				$args = array(
					'post_type' => 'ra-services',
					'status' => 'published',
					'posts_per_page' => -1,
					'orderby' => 'menu_order',
					'order' => 'ASC',
					'fields' => 'ids'
				);
				$services = get_posts( $args );
			 ?>

		<div class="o-wrapper o-wrapper--1100">
			<h1 class="page-title c-services__heading animate"><?php echo the_title(); ?></h1>
			<div class="c-services__list">
				<div class="o-layout o-layout--large o-module">
					<?php foreach ($services as $item) : ?>
						<div class="o-layout__item o-module__item c-service-tile u-1/2@tablet js-equal-height">
							<a href="<?php echo get_the_permalink($item); ?>" class="c-service-tile__wrap animate">
								<div class="c-service-tile__bgimg lazyload" data-src="<?php echo get_field('hover_background_image', $item) ?>"></div>
								<div class="c-service-tile__content animate">
									<h3><?php echo get_the_title($item); ?></h3>
									<p><?php echo get_field('short_copy', $item); ?></p>
									<span class="c-service-tile__link">Learn More</span>
								</div>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php $cta = get_field('cta_copy'); ?>
		<?php if ($cta) : ?>
			<div class="c-cta animate">
				<div class="o-wrapper">
					<h2 class="c-cta__heading"><?php echo $cta; ?></h2>
					<a href="/contact" class="o-btn o-btn--blue">Contact Us</a>
				</div>
			</div>
		<?php endif; ?>

		<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>