<?php get_header(); ?>
<div class="c-single-service" style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>)">
	<main id="Main" class="c-main-content o-main" role="main">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<div class="o-wrapper o-wrapper--1100">
			<div class="c-single-service__intro animate">
				<h1 class="page-title c-single-service__heading"><?php echo the_title(); ?></h1>
				<div class="intro-para">
					<?php the_content(); ?>
				</div>
			</div>
			<?php $service_content = get_field('service_content'); ?>
			<?php if ($service_content && $service_content['slider_images'] && $service_content['heading']) : ?>
				<div class="c-single-service__content">
					<div class="o-layout o-layout--middle o-layout--large">
					<?php $imgs = $service_content['slider_images']; if ($imgs && count($imgs) > 0) :?>
						<div class="o-layout__item u-1/2@tablet">
							<div class="c-single-service__content-imgs">
								<button class="prev slick-arrow"> <?php echo svgicon('slick-next', '0 0 15 25')?> </button>
								<button class="next slick-arrow"> <?php echo svgicon('slick-next', '0 0 15 25')?> </button>
								<button class="down slick-arrow-down slick-arrow"> <?php echo svgicon('slick-next', '0 0 15 25')?></button>
								<button class="up slick-arrow-up slick-arrow"> <?php echo svgicon('slick-next', '0 0 15 25')?></button>
								<div class="c-single-service__content-slider cs-single-service__imgs <?php echo (count($imgs) == 1) ? 'slick-none': ''; ?>">
									<?php foreach ($imgs as $item) : ?>
										<div>
											<img src="<?php echo $item; ?>" alt="">
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					<?php endif; if ($service_content['heading'] || $service_content['content']) :?>
						<div class="o-layout__item u-1/2@tablet">
							<div class="c-single-service__content-details animate-elems">
								<h3 class=c-single-service__content-title><?php echo $service_content['heading']; ?></h3>
								<div class="content">
									<?php echo $service_content['content']; ?>
								</div>
							</div>
						</div>
					</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>

			<?php $features = get_field('features'); ?>
			<?php if ($features) : ?>
				<div class="c-single-service__features">
					<div class="o-layout o-layout--large">
						<?php foreach ($features as $item) : ?>
							<div class="o-layout__item u-1/3@tablet animate">
								<h3 class=c-single-service__features-title><?php echo $item['heading']; ?></h3>
								<div class="content">
									<?php echo $item['content']; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<?php $projects = get_field('projects');
		if ($projects) : ?>
			<div class="c-single-service__projects">
				<?php foreach ($projects as $project) :
					$post_id = $project['select_project'];
					$thumb_images = array();
					$thumbs = get_field('image_thumbnails', $post_id);
					foreach ($thumbs as $img) {
						if ($img && $img['image']) {
							$thumb_images[] = $img['image'];
						}
					}
					if (count($thumb_images) < 3 && count($thumb_images) > 0) {
						if (!$thumb_images[1]) {
							$thumb_images[1] = $thumb_images[0];
						}
						if (!$thumb_images[2]) {
							$thumb_images[2] = $thumb_images[0];
						}
					}?>
					<a href="<?php echo get_the_permalink($post_id); ?>" class="js-project-tiles c-project-tiles <?php echo $category[0]->slug;?> <?php echo $class; ?>" data-id="<?php echo $post_id; ?>">
						<?php foreach ($thumb_images as $key => $img) : ?>
							<div class="c-project-tiles__thumb <?php echo 'c-project-tiles__thumb-'. ($key + 1); ?>">
								<div class="bgimg lazyload" data-src="<?php echo $img ?>"></div>
							</div>
						<?php endforeach; ?>
					</a>
				<?php endforeach; ?>
				<div class="c-single-service__projects-btn animate">
					<a href="/projects" class="o-btn o-btn--link">All Projects</a>
				</div>
			</div>
				<?php endif; ?>

			<?php $works = get_field('why_work_with_us'); ?>
			<?php if ($works && $works['heading'] && $works && $works['list']) : ?>
				<div class="c-single-service__work-with">
					<div class="o-wrapper">
						<h3 class="-single-service__work-with-heading animate"><?php echo $works['heading']; ?></h3>
						<div class="o-layout o-layout--large o-layout--center o-module">
							<?php foreach ($works['list'] as $item) : ?>
								<div class="o-layout__item o-module__item u-1/3@tablet animate">
									<div class="c-single-service__work-with-list">
										<h3 class=c-single-service__work-with-list-title><?php echo $item['heading']; ?></h3>
										<p><?php echo $item['copy']; ?></p>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>


		<?php
			$args = array(
				'post_type' => 'ra-services',
				'status' => 'published',
				'posts_per_page' => -1,
				'exclude' => array(
					get_the_ID()
				),
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'fields' => 'ids'
			);
			$services = get_posts( $args );?>

			<?php if(count($pages) > 0): ?>
				<div class="c-single-service__related">
					<button class="s-prev slick-arrow"> <?php echo svgicon('slick-next', '0 0 15 25')?> </button>
					<button class="s-next slick-arrow"> <?php echo svgicon('slick-next', '0 0 15 25')?> </button>
					<div class="o-wrapper ">
						<h3 class="animate">Our Services</h3>
						<div class="c-single-service__related-list">
							<?php foreach ($services as $item) : ?>
								<div class="c-service-tile animate">
									<a href="<?php echo get_the_permalink($item); ?>" class="c-service-tile__wrap animate">
										<div class="c-service-tile__bgimg lazyload" data-src="<?php echo get_field('hover_background_image', $item) ?>"></div>
										<div class="c-service-tile__content animate">
											<h3><?php echo get_the_title($item); ?></h3>
											<p><?php echo get_field('short_copy', $item); ?></p>
											<span class="c-service-tile__link">Learn More</span>
										</div>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>

				</div>
			<?php endif ?>
			<?php $cta = get_field('cta_copy'); ?>
			<?php if ($cta) : ?>
				<div class="c-cta animate">
					<div class="o-wrapper">
						<h2 class="c-cta__heading"><?php echo $cta; ?></h2>
						<a href="/contact" class="o-btn o-btn--blue">Contact Us</a>
					</div>
				</div>
			<?php endif; ?>



		<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>