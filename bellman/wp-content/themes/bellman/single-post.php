
<?php get_header(); ?>
<div class="c-single-blog">
	<main id="Main" class="c-main-content o-main" role="main">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<div class="o-wrapper">
			<div class="c-single-blog__banner"style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>)">	</div>
		</div>

		<div class="o-wrapper o-wrapper--1100 c-single-blog__content">
			<div class="c-single-blog__intro animate">
				<p class="c-single-blog__date"><?php echo get_the_date('j F Y') ; ?></p>
				<h1 class="page-title c-single-blog__heading"><?php echo the_title(); ?></h1>
			</div>
			<div class="c-cms-content animate">
				<?php echo the_content(); ?>
			</div>
			<?php $modules = get_field('flexible_content'); ?>
			<?php foreach ($modules as $section) : ?>
				<?php if ($section['acf_fc_layout'] === 'single_image_and_content') : ?>
					<div class="c-flexible-content__img-content">
						<div class="c-flexible-content__img-content-img animate lazyload" data-src="<?php echo $section['image'];?>"></div>
						<div class="content animate">
							<?php echo $section['content']; ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if ($section['acf_fc_layout'] === 'single_image') : ?>
					<?php if ($section['image']) : ?>
						<div class="c-flexible-content__singleimg">
							<div class="c-flexible-content__singleimg-img animate lazyload" data-src="<?php echo $section['image'];?>"></div>
						</div>
					<?php endif; ?>
				<?php endif; ?>
				<?php if ($section['acf_fc_layout'] === 'two_column_images') : ?>
					<div class="c-flexible-content__twoimg">
						<?php if ($section['image1'] && $section['image2']) : ?>
							<div class="o-layout o-layout--large">
								<div class="o-layout__item u-1/2@tablet">
									<div class="c-flexible-content__twoimg-img animate lazyload" data-src="<?php echo $section['image1'];?>"></div>
								</div>
								<div class="o-layout__item u-1/2@tablet">
									<div class="c-flexible-content__twoimg-img animate lazyload" data-src="<?php echo $section['image2'];?>"></div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
		<?php echo get_template_part('partials/latest-articles');?>
		<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>