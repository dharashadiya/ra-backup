<?php /* Template Name: About Page */ ?>
<?php get_header(); ?>
<div class="c-about">
	<main id="Main" class="c-main-content o-main" role="main">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<div class="o-wrapper">
			<div class="c-about__banner"style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>)">	</div>
			<?php $intro = get_field('intro_section') ?>
			<div class="c-about__intro animate">
				<div class="o-layout o-layout--large">
					<div class="o-layout__item u-2/3@tablet">
						<div class="c-about__intro-content">
							<?php if ($intro && $intro['heading'] ) : ?>
								<h2 class="c-about__intro-heading"><?php echo $intro['heading']; ?></h2>
								<div class=" intro-para"><?php echo $intro['content']; ?></div>
							<?php endif; ?>
						</div>
					</div>
					<?php if ($intro['side_image']) : ?>
						<div class="o-layout__item u-1/3@tablet">
							<img class="lazyload" data-src="<?php echo $intro['side_image']; ?>" alt="About">
						</div>
					<?php endif; ?>
				</div>
			</div>
			<?php $flexible_two_imgs = get_field('two_image_section');
				if ($flexible_two_imgs) :
					foreach ($flexible_two_imgs as $section) : ?>
					<?php if ($section['acf_fc_layout'] === 'two_image_section') : ?>
						<div class="c-about__twoimgs">
							<div class="c-flexible-content animate">
								<div class="o-layout o-layout--large o-module c-flexible-content__two-col-img">
									<?php $wide2img = $section['wider_second_image'] ?>
									<?php if ($section['image1']) : ?>
										<div class="o-layout__item o-module__item <?php echo ($wide2img ? 'u-2/5@tablet' : 'u-3/5@tablet'); ?>">
											<div data-src="<?php  echo $section['image1']; ?>" class="lazyload c-flexible-content__two-col-img--bg <?php echo ($wide2img ? 'bgimg2' : 'bgimg1'); ?>"></div>
										</div>
									<?php endif; ?>
									<?php if ($section['image2']) : ?>
										<div class="o-layout__item o-module__item <?php  echo ($wide2img ? 'u-3/5@tablet' : 'u-2/5@tablet'); ?>">
											<div data-src="<?php  echo $section['image2']; ?>" class="lazyload c-flexible-content__two-col-img--bg <?php echo ($wide2img ? 'bgimg1' : 'bgimg2'); ?>"></div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($section['acf_fc_layout'] === 'clients') : ?>
						<?php $clients = get_field('client_logos', 'options');
						if ($clients) : ?>
						<?php $client_heading = get_field('clients_section_heading');?>
							<div class="c-clients">
								<h3 class="c-clients__heading animate">Clients</h3>
								<div class="c-clients__list">
									<?php foreach ($clients as $logo) : ?>
										<div class="c-clients__list-each animate">
											<img src="<?php echo $logo['logo']; ?>" alt="Client logo">
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
					<?php endif; ?>

					<?php if ($section['acf_fc_layout'] === 'team_section') : ?>
						<div class="c-about__team">
								<h3 class="c-clients__heading animate"><?php echo $section['heading']; ?></h3>
								<?php
									$teams = $section['teams'];
									$types = array();
									$prep_teams = array();
									foreach ($teams as $key => $value) {
										$type = stripString($value['team_type']);
										$types[$type] = $value['team_type'];
										$prep_teams[$type][] = $value;
									}
								?>
								<div class="c-about__team-types">
									<?php $i = 0; foreach ($types as $key => $item) :
										if ($i === 0) {
											$active_type = $key;
										} ?>
										<a href="javascript: void(0);" class="o-btn o-btn--filter js-tabs <?php echo ($i == 0 ? 'active' : ''); ?>" data-target="<?php echo $key ; ?>"><?php echo $item; ?></a>
									<?php $i++; endforeach; ?>
								</div>
							<?php foreach ($prep_teams as $type => $items) : ?>
								<div class="o-layout o-layout--large o-module c-about__team-list js-tab-list <?php echo $type; ?>" style="<?php echo ($type === $active_type ? '' : 'display:none;'); ?>">
									<?php foreach ( $items as $team) : ?>
										<div class="o-layout__item o-module__item u-1/3@tabletWide u-1/2@tablet c-about__team-each animate">
											<div class="c-about__team-content">
												<div class="c-about__team-each-wrap">
													<?php if ($team['headshot']) : ?><div class="c-about__team-img lazyload" data-src="<?php  echo $team['headshot'];  ?>"></div><?php endif; ?>
													<?php if ($team['bio']) : ?>
														<div class="c-about__team-bio">
															<?php echo $team['bio'];  ?>
														</div>
													<?php endif; ?>
												</div>
												<span class="name"><?php echo $team['name']; ?></span>
												<span><?php echo $team['title']; ?></span>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
			<?php endforeach ;endif; ?>
		</div>
		<?php echo get_template_part('partials/instagram');?>
		<!-- latest-article -->
		<?php echo get_template_part('partials/latest-articles');?>


		<?php endwhile; ?>
	</main>
</div>


<?php get_footer(); ?>