<?php /* Template Name:  Projects Page */ ?>

<?php get_header(); ?>
<div class="c-projects" style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>)">
	<main id="Main" class="c-main-content o-main" role="main">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<div class="o-wrapper o-wrapper--1100">
			<h1 class="page-title c-projects__heading animate"><?php echo the_title(); ?></h1>
			<?php
				$categories = get_terms('ra-project-category');
			?>
			<div class="c-projects__categories animate">
				<div class="c-projects__categories-desk">
					<?php
					if ( $categories) :?>
						<a href="javascript: void(0);" class="js-project-tabs is-active" data-target="all">All Categories</a>
						<?php foreach ( $categories as $term ) : ?>
							<a href="javascript: void(0);" class="js-project-tabs" data-target="<?php echo $term->slug ; ?>"><?php echo $term->name; ?></a>
						<?php endforeach ?>
					<?php endif;?>
				</div>
				<div class="c-projects__categories-mob">
					<?php if ( $categories) :?>
						<select name="project-categories" id="project-categories">
							<option value="all">All Categories</option>
							<?php foreach ( $categories as $term ) : ?>
								<option value="<?php echo $term->slug ; ?>" data-target="<?php echo $term->slug ; ?>"><?php echo $term->name; ?></option>
							<?php endforeach ?>
						</select>
					<?php endif;?>
				</div>
			</div>
		</div>

		<div class="c-projects__list" id="projects-list">
			<?php
				$posts_per_page = 8;
				$args = array(
					'post_type' => 'ra-project',
					'status' => 'published',
					'posts_per_page' => -1,
					'offset' => 0,
					'orderby' => 'menu_order',
					'order' => 'ASC',
					'fields' => 'ids'
				);
				$total_post_count = count( get_posts( $args) );

				$args['posts_per_page'] = $posts_per_page;
				$posts = get_posts( $args );
			 ?>


			<div class="c-projects__list-wrapper" id="projects-loop-content">
				<?php foreach ($posts as $key => $item) {
					if ($key % 2 === 0) {
						$class = 'odd';
					} else {
						$class = 'even';
					}
					$var = array('ID' => $item, 'class' => $class);
					set_query_var( 'var', $var );
					get_template_part('partials/project-tiles');
				} ?>
			</div>
			<div class="c-projects__loadmore animate">
				<a href="javascript:void(0);" class="o-btn  o-btn--link js-loadmore" <?php echo (count($posts) < $total_post_count ? '' : 'hidden'); ?>>Load More</a>
			</div>
		</div>
		<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>