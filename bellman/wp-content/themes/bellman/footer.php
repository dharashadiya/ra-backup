</div><!-- .c-content -->
<footer class="c-footer">
	<div class="o-wrapper">
		<div class="c-footer__container">
			<div class="o-layout">
				<div class="o-layout__item u-2/5@tablet">
					<p class="c-footer__logo"><?php svgicon('full-logo', '0 0 180 37'); ?></p>
					<p>Part of <a href="http://bigpicturegroup.com.au/" class="linktd">Big Picture Group</a></p>
				</div>
				<?php $qlinks = get_field('quick_links', 'options'); ?>
				<?php if ($qlinks) : ?>
					<div class="o-layout__item u-1/5@tablet">
						<p class="c-footer__heading">Quick Links</p>
						<ul class="c-footer__links">
							<?php foreach ($qlinks as $item) : ?>
								<li><a href="<?php echo $item['link']['url']; ?>"><?php echo $item['link']['title']; ?></a></li>
							<?php endforeach; ?>
						</ul>
					</div>
				<?php endif; ?>
				<?php $services =  get_field('services', 'options'); ?>
				<div class="o-layout__item u-1/5@tablet">
					<p class="c-footer__heading">Services</p>
					<ul class="c-footer__links">
						<?php foreach ($services as $item) : ?>
							<li><a href="<?php echo get_the_permalink($item['link']); ?>"><?php echo get_the_title($item['link']); ?></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
				<?php $contact =  get_field('contact_details', 'options'); ?>
				<div class="o-layout__item u-1/5@tablet">
					<p class="c-footer__heading">Contact</p>
					<div class="c-footer__contact-details">
						<?php echo $contact; ?>
						<?php echo do_shortcode('[social-links]'); ?>
					</div>
				</div>
			</div>
			<div class="c-footer__copy">
				<span class="c-footer__copy-label">&copy;&nbsp;Bellman&nbsp;Agency&nbsp;<?php echo date("Y"); ?></span>
				<div class="c-footer__copy-links">
					<a href="/privacy-policy/" class="linktd">Privacy&nbsp;Policy</a>
					<a href="/terms-conditions/" class="linktd">Terms&nbsp;&amp;&nbsp;Conditions</a>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>