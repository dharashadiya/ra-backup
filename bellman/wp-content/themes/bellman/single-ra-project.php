
<?php get_header(); ?>
<div class="c-single-project">
	<main id="Main" class="c-main-content o-main" role="main">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<div class="o-wrapper">
			<div class="c-single-project__banner lazyload" data-src="<?php echo get_the_post_thumbnail_url() ?>"></div>
		</div>

		<div class="o-wrapper o-wrapper--1100 c-single-project__content">
			<div class="c-single-project__intro">
				<?php if (get_field('client')) : ?>
					<p class="c-single-project__client animate">Client: <?php echo get_field('client'); ?></p>
				<?php endif; ?>
				<h1 class="page-title c-single-project__heading animate"><?php echo the_title(); ?></h1>
				<?php $cate = get_the_terms(get_the_ID(),'ra-project-category');?>
				<?php foreach ($cate as $item) : ?>
					<span class="c-single-project__category animate"><?php echo $item->name ?></span>
				<?php endforeach; ?>
			</div>
			<div class="c-cms-content animate-elems">
				<?php echo the_content(); ?>
			</div>
			<?php $modules = get_field('flexible_content'); ?>
			<?php foreach ($modules as $section) : ?>
				<?php if ($section['acf_fc_layout'] === 'single_image_and_content') : ?>
					<div class="c-flexible-content__img-content animate">
						<div class="c-flexible-content__img-content-img lazyload" data-src="<?php echo $section['image'];?>"></div>
						<div class="content">
							<?php echo $section['content']; ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if ($section['acf_fc_layout'] === 'single_image') : ?>
					<?php if ($section['image']) : ?>
						<div class="c-flexible-content__singleimg animate">
							<img class="lazyload" data-src="<?php echo $section['image'];?>"></img>
						</div>
					<?php endif; ?>
				<?php endif; ?>
				<?php if ($section['acf_fc_layout'] === 'two_column_images') : ?>
					<div class="c-flexible-content__twoimg">
						<?php if ($section['image1'] && $section['image2']) : ?>
							<div class="o-layout o-layout--large">
								<div class="o-layout__item u-1/2@tablet animate">
									<div class="c-flexible-content__twoimg-img lazyload" data-src="<?php echo $section['image1'];?>"></div>
								</div>
								<div class="o-layout__item u-1/2@tablet animate">
									<div class="c-flexible-content__twoimg-img lazyload" data-src="<?php echo $section['image2'];?>"></div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>

		<?php $next_post = get_previous_post();
			$thumb_images = array();
			$thumbs = get_field('image_thumbnails', $next_post->ID);
			foreach ($thumbs as $img) {
				if ($img && $img['image']) {
					$thumb_images[] = $img['image'];
				}
			}
			if (count($thumb_images) < 3 && count($thumb_images) > 0) {
				if (!$thumb_images[1]) {
					$thumb_images[1] = $thumb_images[0];
				}
				if (!$thumb_images[2]) {
					$thumb_images[2] = $thumb_images[0];
				}
			}
		?>
		<?php if ($next_post) : ?>
			<div class="c-single-project__nextproj animate">
				<div class="o-wrapper"><h3>Next Project</h3></div>
				<a href="<?php echo get_the_permalink($next_post->ID); ?>" class="js-project-tiles c-project-tiles <?php echo $category[0]->slug;?> even no-animate" data-id="<?php echo $next_post->ID; ?>">
					<?php foreach ($thumb_images as $key => $img) : ?>
						<div class="c-project-tiles__thumb <?php echo 'c-project-tiles__thumb-'. ($key + 1); ?>">
							<div class="bgimg lazyload" data-src="<?php echo $img ?>"></div>
						</div>
					<?php endforeach; ?>
				</a>
				<div class="c-single-project__nextproj-all animate">
					<a href="/projects" class="o-btn o-btn--link">All Projects</a>
				</div>
			</div>
			<?php endif; ?>

			<div class="c-single-project__form">
				<div class="o-wrapper o-wrapper--1100 animate">
					<h2>Talk to us today about your next project</h2>
					<?php //echo do_shortcode('[contact-form-7 id="9" title="Project Enquiry Form"]'); ?>
					<?php echo do_shortcode('[ninja_form id=2]'); ?>
				</div>
			</div>

		<?php endwhile; ?>
	</main>
</div>
<!--
<div class="input-wraps">
	<div class="input-wrap half left"><span class="label">First Name*</span>[text* first-name placeholder "First Name*"]</div>
	<div class="input-wrap half right"><span class="label">Last Name*</span>[text* last-name placeholder "Last Name*"]</div>
	<div class="input-wrap half left"><span class="label">Email*</span>[email* your-email placeholder "Email*"]</div>
	<div class="input-wrap half right"><span class="label">Phone*</span>[tel* phone placeholder "Phone*"]</div>
	<div class="input-wrap half left"><span class="label">Company</span>[text company placeholder "Company"]</div>
	<div class="input-wrap half right"><span class="label">Message</span>[textarea message placeholder "Message"]</div>
	<div class="input-wrap half left checkbox-wrap"><span class="label">*Required Field</span><span>[checkbox checkbox-signup use_label_element exclusive "Yes"]Sign up to our monthly Beautiful Brands newsletter</span></div>
	<div class="input-wrap t-center"><div class="submit">[submit "Submit"]</div></div>
</div>-->


<?php get_footer(); ?>