<?php  get_header(); ?>

<div class="c-404">
	<main id="Main" class="c-main-content o-main" role="main">
		<article>
		<div class="c-cms-content o-wrapper o-wrapper--1100 animate-elems">
			<h1 class="page-title">Page Not found</h1>
			<p> The content you are looking for not found. Please use navigation menu to go through the site. </p>
			<a href="/" class="o-btn o-btn--blue" title="Back to home">Back to home</a>
		</div>
		</article>
	</main>
</div>

<?php get_footer(); ?>