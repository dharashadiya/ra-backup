export function projectFilterLoadmore() {
	if ($('#projects-loop-content').length === 0) {
		return
	}
	let currCat = $('.js-project-tabs.is-active').attr('data-target')
	let offset = $('.c-project-tiles').length
	$(document).on('click', '.js-project-tabs', function () {
		$('.js-project-tabs').removeClass('is-active')
		$(this).addClass('is-active');
		let cat = $(this).attr('data-target');
		currCat = cat
		offset = 0
		getProjects()
	})

	$(document).on('change', '#project-categories', function () {
		let cat = $(this).val();
		currCat = cat
		offset = 0
		getProjects()
	})

	let lodMoreBtn = $('.js-loadmore')
	$(document).on('click', '.js-loadmore', function () {
		getProjects();
	})

	function getProjects() {
		lodMoreBtn.hide()
		let data = {
			'action': 'ra_get_projects',
			'cat': currCat,
			'offset': offset
		}
		$.post(site.ajaxurl, data, function (res) {
			if (res) {
				let data = JSON.parse(res)
				if (data && data.html) {
					if (offset === 0) {
						$('#projects-loop-content').html(data.html)
					} else {
						$('#projects-loop-content').append(data.html)
					}
					offset = $('.c-project-tiles').length
					console.log(offset, data.total_posts);
					if (offset < data.total_posts) {
						lodMoreBtn.show()
					}
					// $('html, body').animate({ scrollTop: $('#projects-loop-content').position().top - 100 }, 300)
					projectTilesPrep();
					site.lazyLoad();
				}

			}
		});
	}
}


export function projectTilesPrep() {

	if ($('.js-project-tiles').length > 0) {
		let wW = $(window).width()
		$('.js-project-tiles').each(function (index, item) {
			if (index % 2 === 0) {
				$(this).addClass('odd')
			} else {
				$(this).addClass('even')
			}
		})

		$('.js-project-tiles').css('overflow', 'hidden')
		$('.js-project-tiles').each(function (index, item) {
			let thisScrollWidth = $(this)[0].scrollWidth;
			if (thisScrollWidth > wW) {
				$(this).scrollLeft((thisScrollWidth - wW) / 2)
				if (!$(this).hasClass('no-animate')) {
					$(this).addClass('scroll-section')
				}
			}
		})

		$('.scroll-section').each(function (index, elem) {
			if (index % 2 === 0) {
				$(this).attr('data-scroll-dir', 'reverse')
			}
		})
	}
}

export function projectTilesScroll() {
	if ($(window).width() > 770) {
		let prevST = 0
		$(window).on('scroll', function () {
			let sT = $(window).scrollTop()
			let scrollDir = 'down'
			if (sT < prevST) {
				scrollDir = 'up'
			}
			prevST = sT
			$('.scroll-section').each(function (index, item) {
				let thisTop = $(this).offset().top - $(this).height()
				let thisID = $(this).attr('data-id')
				var thisBottom = $(this).offset().top + $(this).height();
				let reverse = $(this).attr('data-scroll-dir')

				if (sT > thisTop && sT < thisTop + $(this).height() + $(this).height()) {
					let left = $(this).scrollLeft()
					if ((scrollDir === 'up' && reverse) || (scrollDir === 'down' && !reverse)) {
						left = left + 2.5
					} else {
						left = left - 2.5
					}

					// if (scrollDir === 'up') {
					// 	if (reverse) {
					// 		left = left + 1
					// 	} else {
					// 		left = left - 1
					// 	}
					// } else {
					// 	if (reverse) {
					// 		left = left - 1
					// 	} else {
					// 		left = left + 1
					// 	}
					// }
					$(this).scrollLeft(left)
				}
			})
		})
	}
}