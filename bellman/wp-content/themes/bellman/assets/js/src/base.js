/* global site */
// [1] Import functions here
import {
	isMobile,
	mobileClass,
	openLinkNewTab,
	superfishMenu,
	slider,
	menuFunc,
	jsTabs,
	slickSlider,
	jsScrollDown,
	lazyLoad,
	fadeInUp,
	equalHeight,
	homeShowreelVideo,
	cloudinaryImageCDN,
} from "./template-functions";
import { projectFilterLoadmore, projectTilesPrep, projectTilesScroll } from "./project-filter";
import { blogPagination } from "./blog-pagination";

(function($) {
	window.site || (window.site = {});
	$.extend(
		site,
		{
			initVars: () => {
				// Global variables -> use as site.VariableName in other files
				site.body = $("body");
				site.root = $("html");
				site.header = $("#Top");
				site.main = $("#Main");
				site.side = $("#Secondary");
				site.siteurl = $("#Siteurl").attr("content");
				site.themeurl = $("#Themeurl").attr("content");
				return (site.ajaxurl = $("#Ajaxurl").attr("content"));
			},
			// [3] Call functions within either of these
			onReady: () => {
				svg4everybody();
				site.initVars();
				site.mobileClass();
				site.menuFunc();
				site.jsTabs();
				site.jsScrollDown();
				site.slickSlider();
				site.lazyLoad();
				site.fadeInUp();
				site.equalHeight();
				site.homeShowreelVideo();
				site.projectTilesPrep();
				site.projectTilesScroll();
				site.projectFilterLoadmore();
				site.blogPagination();

				// site.cloudinaryImageCDN()
				return site.isMobile();

			},
			onLoad: () => {
				// return site.slider()
			},
			onResize: () => {
				site.equalHeight();
				site.homeShowreelVideo();
				site.projectTilesPrep();
			}
		},
		{
			// [2] Register functions here
			isMobile: isMobile,
			mobileClass: mobileClass,
			openLinkNewTab: openLinkNewTab,
			superfishMenu: superfishMenu,
			slider: slider,
			menuFunc: menuFunc,
			jsTabs: jsTabs,
			jsScrollDown: jsScrollDown,
			slickSlider: slickSlider,
			lazyLoad: lazyLoad,
			fadeInUp: fadeInUp,
			equalHeight: equalHeight,
			homeShowreelVideo: homeShowreelVideo,
			projectFilterLoadmore: projectFilterLoadmore,
			projectTilesPrep: projectTilesPrep,
			projectTilesScroll: projectTilesScroll,
			blogPagination: blogPagination,
			cloudinaryImageCDN: cloudinaryImageCDN
		}
	);
	$(() => {
		return site.onReady();
	});
	$(window).on("load", () => {
		return site.onLoad();
	});
	return $(window).on("resize", () => {
		return site.onResize();
	});
})(jQuery);
