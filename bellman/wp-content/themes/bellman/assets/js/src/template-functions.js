export function isMobile() {
	var result;
	result = false;
	if (
		/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
			navigator.userAgent
		)
	) {
		result = true;
	}
	return result;
}

export function mobileClass() {
	if (/iPhone/i.test(navigator.userAgent)) {
		$('html').addClass('is-iphone')
	}
}
// Open External Link in new tab
export function openLinkNewTab() {
	var base;
	base = window.location.hostname;
	return $("a").each(() => {
		let url = $(this).attr("href");
		if (
			url.indexOf("http") !== -1 &&
			(url.indexOf("javascript:void(0)") < 0 && url.indexOf(base) < 0)
		) {
			$(this).attr("target", "_blank");
			return $(this).attr("rel", "noopener");
		}
	});
}

export function cloudinaryImageCDN() {
	const localhost = site.siteurl.includes(".test") ? true : false;
	imagesLoaded("body", {
		background: true
	}, () => {
		Array.from($("[data-img], [data-bg], [data-src]")).forEach(image => {
			if (localhost) {
				if (image.classList.contains("lazy")) {
					return
				} else if (image.tagName === "IMG") {
					image.src = image.dataset.src;
				} else {
					image.style.backgroundImage = `url(${image.dataset.src})`;
				}
			} else {
				const clientWidth = image.clientWidth;
				const pixelRatio = window.devicePixelRatio || 1.0;
				const imageParams = "w_" + 100 * Math.round((clientWidth * pixelRatio) / 100) + ",c_fill,g_auto,f_auto";
				const baseUrl = "https://res.cloudinary.com/rock-agency/image/fetch";
				let url;
				if (image.classList.contains("lazy")) {
					image.dataset.src = baseUrl + "/" + imageParams + "/" + image.dataset.src;
				} else if (image.tagName === "IMG") {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.src = url;
				} else {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.style.backgroundImage = `url(${url})`;
				}
			}
		});
	});
}

export function superfishMenu() {
	if ($(window).width() > 799) {
		return $(".c-nav").superfish({
			animation: {
				height: "show"
			},
			delay: 250,
			speed: 100
		});
	}
}

export function slider() {
	return $(".c-slider").cycle({
		log: false,
		// swipe: true
		slides: ".c-slide",
		"auto-height": "calc",
		timeout: 5000,
		speed: 600
	});
}

// Fade in animation
export function fadeInUp() {
	$(`.input-wrap,
	   .animate,
	   .animate-elems p,
	   .animate-elems h1,
	   .animate-elems h2,
	   .animate-elems h3,
	   .animate-elems h4,
	   .animate-elems h5,
	   .animate-elems h6,
	   .animate-elems li`
	).addClass('pre-animate').viewportChecker({
		classToAdd: "animated",
		classToRemove: 'pre-animate',
		offset: 0
	});
}
// LazyLoad
export function lazyLoad() {
	return $('.lazyload').Lazy({
		threshold: 400,
		effect: 'fadeIn',
	})
}

export function menuFunc() {
	$(document).on('click', '.c-header__toggle-nav', function () {
		$('.c-top').toggleClass('menu-open')
		$('body').toggleClass('scroll-lock')
	})
}

export function slickSlider() {
	// $('.c-single-service__content-slider').slick({
	// 	// dots: true,
	// 	arrows: true,
	// 	prevArrow: $('.prev'),
	// 	nextArrow: $('.next'),
	// 	infinite: true,
	// 	speed: 500,
	// 	autoplay: true,
	// 	slidesToShow: 1,
	// 	slidesToScroll: 1,
	// 	adaptiveHeight: true
	// });
	$('.c-single-service__content-slider').slick({
		arrows: true,
		prevArrow: $('.up'),
		nextArrow: $('.down'),
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		adaptiveHeight: true,
		row: 0,
		vertical: true,
		verticalSwiping: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		dots: false,
		pauseOnHover: false,
		responsive: [
			{
				breakpoint: 770,
				settings: {
					prevArrow: $('.prev'),
					nextArrow: $('.next'),
					slidesToShow: 1,
					slidesToScroll: 1,
					vertical: false,
					verticalSwiping: false,
					settings: "unslick"
				}
			}
		]
	});
	$('.c-single-service__content-slider').slick('refresh');

	$('.c-single-service__related-list').slick({
		arrows: true,
		prevArrow: $('.s-prev'),
		nextArrow: $('.s-next'),
		infinite: false,
		speed: 500,
		adaptiveHeight: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		adaptiveHeight: true,
		responsive: [
			{
				breakpoint: 770,
				settings: {
					slidesToShow: 1,
				}
			},
			{
				breakpoint: 1300,
				settings: {
					slidesToShow: 2,
				}
			}
		]
	});
}

export function equalHeight() {
	if ($('.js-equal-height').length === 0) return;
	$('.js-equal-height').height('auto')

	let h = 0
	$('.js-equal-height').each(function (params) {
		let thisH = $(this).height()
		if (thisH > h) {
			h = thisH
		}
	})
	$('.js-equal-height').height(h)
}

export function jsScrollDown() {
	$(document).on('click', '.js-scroll-dwn', function () {
		$("html").animate({
			scrollTop: $(window).height()
		}, 100);
	});
}
export function homeShowreelVideo() {
	if ($('.js-src-and-play').length === 0) return
	let heightTimeout = null
	$('.js-src-and-play').each(function (index, elem) {
		let src = $(this).attr('data-src')
		let srcM = $(this).attr('data-src-mob')
		let parentheight = $(this).attr('data-parentheight')
		if ($(window).width() < 770 && srcM) {
			src = srcM
		}
		$(this).attr('src', src)
		if (parentheight) {
			$(this).css({ 'width': '100%', 'height': 'auto', 'opacity': '0' });
			clearTimeout(heightTimeout)
			heightTimeout = setTimeout(() => {
				let container = $(parentheight);
				if (container.length > 0 && container.outerHeight() > $(this).height()) {
					$(this).css({ 'width': 'auto', 'height': '100%' });
				}
				$(this).css({ 'opacity': '1' });
			}, 500);
		}

	})
}

export function jsTabs() {
	$(document).on('click', '.js-tabs', function () {
		var target = $(this).attr('data-target');
		let tabList = $('.js-tab-list')
		let tabs = $('.js-tabs')
		let tabListTarget = $(`.js-tab-list.${target}`)
		tabList.hide();
		tabs.removeClass('active');
		$(this).addClass('active');
		tabListTarget.show();
		site.fadeInUp();
		// $("html").animate({ scrollTop: tabListTarget.position().top - 100}, 400);
	})

}
