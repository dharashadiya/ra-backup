export function blogPagination() {
	if ($('#blog-content').length === 0) {
		return
	}
	let currPage = $('.js-pageno.is-active').attr('data-pageno')
	$(document).on('click', '.js-pageno', function () {
		let pageNo = $(this).attr('data-pageno')
		currPage = pageNo
		getBlogs()
	})

	function getBlogs() {
		let data = {
			'action': 'ra_get_blogs',
			'page_no': currPage,
		}

		// console.log(data);
		$.post(site.ajaxurl, data, function (res) {
			if (res) {
				let data = JSON.parse(res)
				if (data && data.html) {
					$('#blog-content').html(data.html)
					$('html, body').animate({ scrollTop: $('#blog-content').position().top - 200 }, 200)
					site.lazyLoad();
				}
			}
		});
	}
}