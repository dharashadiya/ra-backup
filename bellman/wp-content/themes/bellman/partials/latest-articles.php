<?php
// latest  3 post
$args = array(
	'post_type' => 'post',
	'status' => 'published',
	'posts_per_page' => 3,
	'orderby' => 'date',
	'order' => 'DESC',
	'fields' => 'ids'
);
$latest_post = get_posts($args);?><?php if (count($latest_post) > 0) : ?>
	<div class="o-wrapper">
		<div class="c-latest-article animate">
			<h3 class="c-latest-article__heading">Latest Articles</h3>
			<div class="o-layout o-layout--large c-blogs-list">
				<?php foreach ($latest_post as $item) :
					$img = get_the_post_thumbnail_url($item, 'blog-thumb'); ?>
					<div class="o-layout__item u-1/3@tablet c-blog-tile animate">
						<a href="<?php echo get_the_permalink($item); ?>" class="c-blog-tile__wrap"><div class="c-blog-tile__img lazyload" data-src="<?php echo $img; ?>"></div></a>
						<h3><a href="<?php echo get_the_permalink($item); ?>"><?php echo wp_trim_words(get_the_title($item), 12); ?></a></h3>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="c-latest-article__all">
				<a href="/blog" class="o-btn o-btn--link">View The Blog </a>
			</div>
		</div>
	</div>
<?php endif; ?>