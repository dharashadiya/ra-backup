<?php
$var = get_query_var('var');
$post_id = $var['ID'];
$class = $var['class'];
if (!$post_id) {
	return;
}

$category = get_the_terms($post_id, 'ra-project-category');

$thumb_images = array();
$thumbs = get_field('image_thumbnails', $post_id);
foreach ($thumbs as $img) {
	if ($img && $img['image']) {
		$thumb_images[] = $img['image'];
	}
}
if (count($thumb_images) < 3 && count($thumb_images) > 0) {
	if (!$thumb_images[1]) {
		$thumb_images[1] = $thumb_images[0];
	}
	if (!$thumb_images[2]) {
		$thumb_images[2] = $thumb_images[0];
	}
}
?><a href="<?php echo get_the_permalink($post_id); ?>" class="js-project-tiles animate c-project-tiles <?php echo $category[0]->slug;?> <?php echo $class; ?>" data-id="<?php echo $post_id; ?>">
	<?php foreach ($thumb_images as $key => $img) : ?>
		<div class="c-project-tiles__thumb <?php echo 'c-project-tiles__thumb-'. ($key + 1); ?>">
			<div class="bgimg lazyload" data-src="<?php echo $img ?>"></div>
		</div>
	<?php endforeach; ?>
</a>