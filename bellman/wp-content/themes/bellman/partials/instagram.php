<div class="c-instagram">
	<div class="o-wrapper">
		<h3 class="c-instagram__heading animate">Instagram</h3>
		<div class="c-instagram__tiles animate">
			<?php echo do_shortcode('[instagram-feed]'); ?>
			<div class="c-instagram__visit">
				<a href="https://www.instagram.com/bellmanagency/" class="c-about__instagram-link" target="_blank">Visit our instagram</a>
			</div>
		</div>
	</div>
</div>