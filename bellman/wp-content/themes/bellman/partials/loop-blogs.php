<?php
$args = array(
	'post_type' => 'post',
	'status' => 'published',
	'posts_per_page' => 5,
	'orderby' => 'date',
	'order' => 'DESC',
	'fields' => 'ids'
);

$latest_post = get_posts($args);
$var = get_query_var('var');
if ($var && $var['page_no']) {
	$page_no = $var['page_no'];
} else {
	$page_no = 1;
}
$posts_per_page = 8;


$args = array(
	'post_type' => 'post',
	'status' => 'published',
	'posts_per_page' => -1,
	'orderby' => 'date',
	'exclude' => $latest_post,
	'order' => 'DESC',
	'fields' => 'ids'
);
$total_posts = get_posts( $args );
$total_posts = count($total_posts);
$pages = ceil($total_posts / $posts_per_page);
$args['posts_per_page'] = $posts_per_page;
$args['paged'] = $page_no;
$posts = get_posts( $args );
if (count($posts) > 0): ?>
<div class="c-blogs__listing">
	<h3 class="animate">More Blogs</h3>
	<div class="o-layout o-module o-layout--large c-blogs-list">
		<?php foreach ($posts as $item) :
		$img = get_the_post_thumbnail_url($item, 'blog-thumb'); ?>
			<div class="o-layout__item o-module__item u-1/4@tabletWide u-1/3@tablet c-blog-tile animate">
				<div class="c-blog-tile__inner">
					<a href="<?php echo get_the_permalink($item); ?>" class="c-blog-tile__wrap"><div class="c-blog-tile__img lazyload" data-src="<?php echo $img; ?>"></div></a>
					<div class="c-blog-tile__content">
						<h3><a href="<?php echo get_the_permalink($item); ?>"><?php echo wp_trim_words(get_the_title($item), 8) ?></a></h3>
						<p class="c-blog-tile__date"><?php echo get_the_date('j F Y', $item) ; ?></p>
						<a href="<?php echo get_the_permalink($item); ?>" class="c-blog-tile__link">Read</a>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>

<?php if ($pages > 1) : ?>
		<div class="c-blogs__pagination">
			<div class="c-blogs__pagination-list animate">
				<?php if ($page_no) : ?>
					<a href="javascript:void(0);" class="c-blogs__pagination-arrows prev <?php echo ($page_no > 1) ? "active js-pageno" : "inactive" ?>" data-pageno="<?php echo $page_no - 1; ?>"><?php svgicon('prev', '0 0 8 10') ?></a>
				<?php endif; ?>
				<?php
					//Ref: https://stackoverflow.com/questions/6354303/paging-like-stackoverflows
					$cntAround = 1;
					for ($i = 1; $i <= $pages; $i++) {
						$url = get_the_permalink() . '?page_no=' . $i;
						if ($curr_cat) {
							$url = get_the_permalink() . '?cat=' . $curr_cat . '&page_no=' . $i;
						}

						$isGap = false;
						if ($cntAround >= 1 && $i > 1 && $i < $pages && abs($i - $page_no) > $cntAround) {
							$isGap = true;
							$i = ($i < $page_no ? $page_no - $cntAround : $pages) - 1;
						}

						if ($isGap) {
							$lnk = '<span>...</span>';
						} elseif ($i != $page_no && !$isGap) {
							$lnk = '<a href="javascript:void(0);" class="js-pageno" data-pageno="' . $i . '">' . $i . '</a>';
						} elseif ($i == $page_no && !$isGap) {
							$lnk = '<a class="is-active" href="javascript:void(0);" data-pageno="' . $i . '">' . $i . '</a>';
						}
						echo $lnk;
					}
				?>
				<?php if ($page_no) : ?>
					<a href="javascript:void(0);" class="c-blogs__pagination-arrows next <?php echo ($page_no < $pages) ? "active js-pageno" : "inactive" ?>" data-pageno="<?php echo $page_no + 1; ?>"><?php svgicon('next', '0 0 8 10') ?></a>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>