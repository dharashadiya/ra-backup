<?php get_header(); ?>
<div class="c-home">
	<main id="Main" class="c-main-content o-main" role="main">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
			<div class="c-home-top">
				<?php
					$video = get_field('show_reel_video', 'options');
					$mob_video = get_field('show_reel_video_mobile', 'options');
				?>
				<?php if ($video) : ?>
					<div class="c-home-top__video animate">
						<video class="js-src-and-play" preload="metadata" autoplay="" loop="" muted="" playsinline=""
							data-src="<?php echo $video['url'] ?>"
							data-src-mob="<?php echo $mob_video['url']; ?>"
							data-parentheight=".c-home-top__video"
						></video>
						<?php echo do_shortcode('[social-links]') ?>
						<a class="c-home-top__tel" href="tel: (03) 9049 5555">(03) 9049 5555</a>
						<a class="c-home-top__scroll js-scroll-dwn" href="javascript:void(0);"><?php svgicon('next', '0 0 18 40'); ?></a>
					</div>
				<?php endif; ?>
			</div>
			<?php $intro = get_field('intro_section'); if ($intro && $intro['heading'] && $intro['column_1_content']) : ?>
				<div class="c-home-intro">
					<div class="o-wrapper o-wrapper--1100 animate">
						<h2 class="c-home-intro__heading"><?php echo $intro['heading']; ?></h2>
						<div class="c-home-intro__content">
							<div class="intro-para">
								<?php echo $intro['column_1_content']; ?>
							</div>
							<div class="column2-content">
								<?php echo $intro['column_2_content']; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>

			<?php $projects_sec = get_field('projects_section'); if ($projects_sec && $projects_sec['heading']) : ?>
				<div class="c-home-projects">
					<div class="o-wrapper animate">
						<h3 class="c-home-projects__heading "><?php echo $projects_sec['heading']; ?></h3>
					</div>
					<div class="c-home-projects__content">
						<?php $projects_services = $projects_sec['projects-services'];
						foreach ($projects_services as  $item) : ?>
							<?php if ($item['projects']) : ?>
								<div class="animate c-project-tiles odd js-project-tiles" data-id="<?php echo $post_id; ?>">
									<?php foreach ($item['projects'] as $project) :
										$post_id = $project['select_project'];
										$thumb_images = array();
										$thumbs = get_the_post_thumbnail_url($post_id);
										$thumb_images[] = $thumbs;	?>
										<?php foreach ($thumb_images as $key => $img) : ?>
											<a href="<?php echo get_the_permalink($post_id); ?>" class="c-project-tiles__thumb <?php echo 'c-project-tiles__thumb-'. ($key + 1); ?>">
												<?php if ($img) : ?>
													<div class="bgimg lazyload" data-src="<?php echo $img ?>"></div>
												<?php endif; ?>
											</a>
										<?php endforeach; ?>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
								<?php if ($item['services']) : ?>
									<div class="c-home-projects__services animate">
										<?php foreach ($item['services'] as $service) :
											$service_id = $service['select_service']; ?>
												<a href="<?php echo get_the_permalink($service_id); ?>" class="c-home-projects__services-tile">
													<div class="c-home-projects__services-tile-bgimg lazyload" data-src="<?php echo get_field('hover_background_image', $service_id) ?>"></div>
													<div class="c-home-projects__services-tile-wrap">
														<?php if (get_field('icon', $service_id)) : ?>
															<img data-src="<?php echo get_field('icon', $service_id); ?>" class="lazyload" alt="<?php echo get_the_title($service_id); ?>">
														<?php endif; ?>
														<div class="c-home-projects__services-tile-content">
															<h3><?php echo get_the_title($service_id); ?></h3>
															<p><?php echo wp_trim_words(get_field('short_copy', $service_id), 10); ?></p>
															<span class="c-home-projects__services-tile-link">Learn More</span>
														</div>
													</div>
												</a>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
						<?php endforeach; ?>
						<div class="c-home-projects__btn">
							<a href="/projects" class="o-btn o-btn--link">All Projects</a>
						</div>
					</div>
				</div>
			<?php endif; ?>

			<?php $clients = get_field('client_logos', 'options'); if ($clients) : ?>
			<?php $client_heading = get_field('clients_section_heading');?>
				<div class="c-clients">
					<div class="o-wrapper">
						<h3 class="c-clients__heading animate"><?php echo ($client_heading ? $client_heading : 'Clients'); ?></h3>
						<div class="c-clients__list animate">
							<?php foreach ($clients as $logo) : ?>
								<div class="c-clients__list-each">
									<img src="<?php echo $logo['logo']; ?>" alt="Client logo">
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>

			<!-- latest-article -->
			<?php echo get_template_part('partials/latest-articles');?>
			<?php echo get_template_part('partials/instagram');?>

			<?php $cta = get_field('cta_copy'); ?>
			<?php if ($cta) : ?>
				<div class="c-cta animate">
					<div class="o-wrapper">
						<h2 class="c-cta__heading"><?php echo $cta; ?></h2>
						<a href="/contact" class="o-btn o-btn--blue">Contact Us</a>
					</div>
				</div>
			<?php endif; ?>
		<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>