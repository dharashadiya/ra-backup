<?php /* Template Name: Contact Page */ ?>
<?php get_header(); ?>
<div class="c-contact">
	<main id="Main" class="c-main-content o-main" role="main">
		<div class="o-wrapper o-wrapper--1100 c-contact__content">
			<div class="o-layout o-layout--large">
				<div class="o-layout__item u-1/2@tablet">
					<div class="c-contact__left animate">
						<h1 class="c-contact__heading page-title"><?php the_title(); ?></h1>
						<div class="intro-para"><?php the_content(); ?></div>
						<div class="c-contact__form">
							<?php //echo do_shortcode('[contact-form-7 id="9" title="Project Enquiry Form"]'); ?>
							<?php echo do_shortcode('[ninja_form id=2]'); ?>
						</div>
					</div>
				</div>
				<div class="o-layout__item u-1/2@tablet">
				<div class="c-contact__right animate">
					<?php $img = get_the_post_thumbnail_url() ;?>
					<?php if ($img) : ?>
						<img class="c-contact__sideimg animate lazyload" data-src="<?php echo $img ; ?>" alt="contact">
					<?php endif; ?>
					<div class="c-contact__details animate-elems">
						<?php echo get_field('contact_details'); ?>
					</div>
				</div>
				</div>
			</div>
		</div>
		<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
			<?php $flexible_two_imgs = get_field('two_image_section');
				if ($flexible_two_imgs) :
					foreach ($flexible_two_imgs as $section) : ?>
					<?php if ($section['acf_fc_layout'] === 'two_image_section') : ?>
						<div class="o-wrapper">
							<div class="c-flexible-content">
								<div class="o-layout o-layout--large o-module c-flexible-content__two-col-img">
									<?php $wide2img = $section['wider_second_image'] ?>
									<?php if ($section['image1']) : ?>
										<div class="o-layout__item o-module__item <?php echo ($wide2img ? 'u-2/5@tablet' : 'u-3/5@tablet'); ?>">
											<div data-src="<?php  echo $section['image1']; ?>" class="animate lazyload c-flexible-content__two-col-img--bg <?php echo ($wide2img ? 'bgimg2' : 'bgimg1'); ?>"></div>
										</div>
									<?php endif; ?>
									<?php if ($section['image2']) : ?>
										<div class="o-layout__item o-module__item <?php  echo ($wide2img ? 'u-3/5@tablet' : 'u-2/5@tablet'); ?>">
											<div data-src="<?php  echo $section['image2']; ?>" class="animate lazyload c-flexible-content__two-col-img--bg <?php echo ($wide2img ? 'bgimg1' : 'bgimg2'); ?>"></div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
			<?php endforeach ;endif; ?>

		<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>





<!-- <div class="input-wraps"><div class="input-wrap half left"><span class="label">Name*</span>[text* your-name placeholder "Name*"]</div><div class="input-wrap half right"><span class="label">Email*</span>[email* your-email placeholder "Email*"]</div><div class="input-wrap t-center"><div class="submit">[submit "Submit"]</div></div>
</div> -->

<!-- <div class="input-wraps">
	<div class="input-wrap half left"><span class="label">First Name*</span>[text* first-name placeholder "First Name*"]</div>
	<div class="input-wrap half right"><span class="label">Last Name*</span>[text* last-name placeholder "Last Name*"]</div>
	<div class="input-wrap half left"><span class="label">Email*</span>[email* your-email placeholder "Email*"]</div>
	<div class="input-wrap half right"><span class="label">Phone*</span>[tel* phone placeholder "Phone*"]</div>
	<div class="input-wrap half left"><span class="label">Company</span>[text company placeholder "Company"]</div>
	<div class="input-wrap half right"><span class="label">Message</span>[textarea message placeholder "Message"]</div>
	<div class="input-wrap half left checkbox-wrap"><span class="label">*Required Field</span><span>[checkbox checkbox-signup use_label_element exclusive "Yes"]Sign up to our monthly Beautiful Brands newsletter</span></div>
	<div class="input-wrap t-center"><div class="submit">[submit "Submit"]</div></div>
</div> -->