<?php

add_action( 'wp_ajax_nopriv_ra_get_projects', 'ra_get_projects' );
add_action( 'wp_ajax_ra_get_projects', 'ra_get_projects' );
function ra_get_projects() {
	$offset = 0;
	if ($_POST['offset']) {
		$offset = $_POST['offset'];
	}

	$posts_per_page = 8;
	$args = array(
		'post_type' => 'ra-project',
		'status' => 'published',
		'posts_per_page' => -1,
		'offset' => 0,
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'fields' => 'ids'
	);
	if ($_POST['cat'] && $_POST['cat'] !== 'all'){
		$args['tax_query'] = array(array(
			'taxonomy' => 'ra-project-category',
			'terms' => $_POST['cat'],
			'field' => 'slug',
		));
	}
	$result['total_posts'] = count( get_posts( $args) );

	$args['posts_per_page'] = $posts_per_page;
	$args['offset'] = $offset;
	$posts = get_posts( $args );
	$result['html'] = '';
	foreach ($posts as $key => $item) {
		$var = array('ID' => $item);
		set_query_var( 'var', $var );
		$result['html'] .= load_template_part('partials/project-tiles', $var);
	}

	$result = json_encode($result);
	wp_die($result);
}




add_action( 'wp_ajax_nopriv_ra_get_blogs', 'ra_get_blogs' );
add_action( 'wp_ajax_ra_get_blogs', 'ra_get_blogs' );
function ra_get_blogs() {
	$data = $_POST;

	$var = array();
	$result = array();
	if ($_POST['page_no']) {
		$var['page_no'] = $_POST['page_no'];
	}
	// $result['posts'][] = load_template_part($template, $var);
	$result['var'] = $var;
	$result['html'] = load_template_part('partials/loop-blogs', $var);

	$result = json_encode($result);
    wp_die($result);
}


?>