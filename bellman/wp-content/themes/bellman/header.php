<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
	<title><?php wp_title(); ?> </title>

	<?php if(is_local_or_staging()) : ?>
		<meta name="robots" content="none" />
		<meta name="robots" content="noindex" />
		<meta name="googlebot" content="none" />
		<meta name="googlebot" content="noindex" />
	<?php endif; ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


	<meta name="thumbnail" content="http://bellmanagency.com.au/bellman-agency-logo.jpg" />

	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'container_width'); ?>" />
	<meta property="og:image:width" content="600" />
	<meta property="og:image:height" content="315" />
	<meta property="og:title" content="<?php the_title(); ?>" />
	<meta property="og:description" content="<?php the_excerpt(); ?>" />
	<meta property="og:url" content="<?php the_permalink(); ?>"/>
	<meta property="og:site_name" content="Bellman Agency" />

	<meta name="twitter:site" content="@bellmanAU">
	<meta name="twitter:creator" content="@bellmanAU">
	<meta name="twitter:image" content="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'container_width'); ?>" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:title" content="<?php the_title(); ?>" />
	<meta name="twitter:description" content="<?php the_excerpt(); ?>" />

	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">
	<?php wp_head(); ?>
	<?php include('partials/inlinescripts.min.php'); ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PBV9LG');</script>
	<!-- End Google Tag Manager -->
	<script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-53914120-1', 'auto'); ga('send', 'pageview'); </script>

	<script> ! function(f, b, e, v, n, t, s) { if (f.fbq) return; n = f.fbq = function() { n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments) }; if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0; t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s) }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js'); fbq('init', '124489901354989'); fbq('track', 'PageView'); </script>
</head>

<body <?php body_class(); ?>>
<div class="c-top--spacer"></div>
<div class="c-top">
	<div class="c-header__video">
		<?php
			$video = get_field('show_reel_video', 'options');
			$mob_video = get_field('show_reel_video_mobile', 'options');
		?>
		<video class="js-src-and-play" preload="metadata" autoplay="" loop="" muted="" playsinline=""
			data-src="<?php echo $video['url'] ?>"
			data-src-mob="<?php echo $mob_video['url']; ?>"
		></video>
	</div>
	<header class="c-header">
		<?php if (!is_front_page()) : ?>
			<a href="/" class="c-logo-wrap is-link" rel="home" title="Back to home"><?php svgicon('logo', '0 0 42 42', 'c-header__logo'); ?><?php svgicon('full-logo', '0 0 180 37', 'c-header__logo-nav'); ?></a>
		<?php else: ?>
			<span class="c-logo-wrap"><?php svgicon('logo', '0 0 42 42', 'c-header__logo'); ?><?php svgicon('full-logo', '0 0 180 37', 'c-header__logo-nav'); ?></span>
		<?php endif; ?>
		<a href="#Main" class="c-skip">Skip to main content</a>
		<a href="javascript:void(0);" class="c-header__toggle-nav"><span></span></a>
		<nav class="c-site-nav" role="navigation">
		<?php
			wp_nav_menu( array(
				'theme_location' => 'primary-menu',
				'container' => false,
				'menu_class' => 'c-site-nav__nav',
				'menu_id' => 'menu'
			));
		?>
		</nav>
		<div class="c-header__bottom">
			<div class="c-header__bottom-inner">
				<span>Part of <a href="http://bigpicturegroup.com.au/" class="linktd">Big Picture Group</a></span>
				<span class="c-header__bottom-copy">
					<span class="c-header__bottom-copy-label">&copy;&nbsp;Bellman&nbsp;Agency&nbsp;<?php echo date("Y"); ?></span>
					<div class="c-header__bottom-copy-links">
						<a href="/privacy-policy/" class="linktd">Privacy&nbsp;Policy</a>
						<a href="/terms-conditions/" class="linktd">Terms&nbsp;&amp;&nbsp;Conditions</a>
					</div>
				</span>
			</div>
		</div>
	</header>
</div>

<div class="c-content" data-scrollbar>