<?php
/*
 * Template Name: Blogs Page
 */
?>

<?php get_header(); ?>

<div class="c-blogs">
<main id="Main" class="c-main-content c-main-content--blogs o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="o-wrapper o-wrapper--1100">
			<div class="c-blogs__header animate">
				<h1 class="page-title"><?php the_title(); ?></h1>
				<div class="intro-para"><?php the_content(); ?></div>
			</div>
			<?php
				// latest  5 post
				$args = array(
					'post_type' => 'post',
					'status' => 'published',
					'posts_per_page' => 5,
					'orderby' => 'date',
					'order' => 'DESC',
					'fields' => 'ids'
				);

				$latest_post = get_posts($args);
				$latest_post1 = $latest_post[0];

			?>
			<?php if ($latest_post) : ?>
				<div class="c-blogs-top animate">
					<a href="<?php echo get_the_permalink($latest_post1); ?>" class="c-blogs-top__img">
						<div class="c-blogs-top__img-bg lazyload" data-src="<?php echo get_the_post_thumbnail_url($latest_post1, 'blog-thumb'); ?>">
						</div>
					</a>
					<div class="c-blogs-top__content animate">
						<h3><a href="<?php echo get_the_permalink($latest_post1); ?>"><?php echo get_the_title($latest_post1); ?></a></h3>
						<p class="c-blogs-top__content-date"><?php echo get_the_date('j F Y', $latest_post1) ; ?></p>
						<p><?php echo wp_trim_words(get_the_content(null, null,  $latest_post1),30); ?></p>
						<a href="<?php echo get_the_permalink($latest_post1); ?>" class="o-btn o-btn--blue"> Read</a>
					</div>
				</div>
			<?php endif; ?>
			<div class="c-blogs-latest">
				<div class="o-layout o-module o-layout--large">
					<?php foreach ($latest_post as $key => $blog) :
					if ($key < 1) continue;
					$img = get_the_post_thumbnail_url($blog, 'blog-thumb');?>
					<div class="o-layout__item o-module__item u-1/2@tabletWide">
						<div class="c-blogs-latest__tile animate">
							<a href="<?php echo get_the_permalink($blog); ?>" class="c-blogs-latest__tile-wrap">
								<div class="c-blogs-latest__tile-img lazyload" data-src="<?php echo $img; ?>"></div>
							</a>
							<div class="c-blogs-latest__tile-content">
								<h3><a href="<?php echo get_the_permalink($latest_post1); ?>"><?php echo wp_trim_words(get_the_title($blog), 10); ?></a></h3>
								<p class="c-blogs-top__content-date"><?php echo get_the_date('j F Y', $blog) ; ?></p>
								<a href="<?php echo get_the_permalink($blog); ?>" class="o-btn o-btn--navy">Read</a>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="o-wrapper">
			<div id="blog-content">
				<?php get_template_part('partials/loop-blogs') ?>
			</div>

			<div class="c-blogs-form animate">
				<h2>Never miss a blog post! Sign up for emails.</h2>
				<?php //echo do_shortcode('[contact-form-7 id="303" title="Newsletter"]'); ?>
				<?php echo do_shortcode('[ninja_form id=3]'); ?>
			</div>
		</div>
	</article>
	<?php endwhile; ?>
</main>
</div>

<?php get_footer(); ?>


<!--
<div class="input-wraps">
	<div class="input-wrap half left"><span class="label">Name*</span>[text* your-name placeholder "Name*"]</div>
	<div class="input-wrap half right"><span class="label">Email*</span>[email* your-email placeholder "Email*"]</div>
	<div class="input-wrap t-center"><div class="submit">[submit "Submit"]</div></div>
</div>-->