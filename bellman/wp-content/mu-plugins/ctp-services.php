<?php
/*
Plugin Name: Services - Custom Post Type
Description: Services - Custom Post Type for Brenda Janschek
*/

add_action( 'init', 'register_services_custom_post_type' );
function register_services_custom_post_type() {
  register_post_type( 'ra-services', array(
    'label'  => __('Services', 'ra-services'),
    'description' => __('Services', 'ra-services'),
    'labels' => array(
      'name'               => _x( 'Services', 'post type general name', 'ra-services' ),
      'singular_name'      => _x( 'Services', 'post type singular name', 'ra-services' ),
      'menu_name'          => __( 'Services', 'admin menu', 'ra-services' ),
      'parent_item_colon'  => __( 'Parent Services:', 'ra-services' ),
      'name_admin_bar'     => __( 'Services', 'add new on admin bar', 'ra-services' ),
      'all_items'          => __( 'All Services', 'ra-services' ),
      'view_item'          => __( 'View Services', 'ra-services' ),
      'add_new_item'       => __( 'Add New Services', 'ra-services' ),
      'add_new'            => __( 'Add New', 'Services', 'ra-services' ),
      'edit_item'          => __( 'Edit Services', 'ra-services' ),
      'update_item'        => __( 'Update Services', 'ra-services' ),
      'search_items'       => __( 'Search Services', 'ra-services' ),
      'new_item'           => __( 'New Services', 'ra-services' ),
      'not_found'          => __( 'No Services found.', 'ra-services' ),
      'not_found_in_trash' => __( 'No Services found in Trash.', 'ra-services' ),
    ),

    'supports'      => array(
      'title',
      'editor',
      'thumbnail',
      'comments',
      'revisions',
      'custom-fields',
      'page-attributes'
    ),
     'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-video-alt',
    'can_export'          => true,
    'has_archive'         => false,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'query_var'           => true,
    'rewrite'             => array(
      'slug' => 'branding-services',
      // 'with_front' => 'true'
    )
  ) );
}



?>