let base = 16;

module.exports = {
  purge: ["./src/**/*.html", "./src/**/*.liquid"],
  prefix: "",
  important: false,
  separator: ":",
  theme: {
    screens: {
      xs: "370px",
      sm: "640px",
      md: "768px",
      tablet: "800px",
      lg: "1024px",
      mlg: "1090px",
      ml: "1240px",
      xml: "1300px",
      xl: "1440px",
      wxl: "1450px",
      wide: "2400px",
    },
    colors: {
      transparent: "transparent",
      current: "currentColor",
      black: "#000",
      white: "#fff",
      red: "#ff6d6d",
      blue: "#6787A9",
      orange: "#EC9F33",
      purpule: "#C468C1",
      "dark-choc": "#191818",
      "dark-black": "#151414",
      grey: {
        200: "#F5F5F5",
        300: "#C4C4C4",
        350: "#BCBCBC",
        400: "#939292",
        500: "#333333",
        600: "#848484",
        700: "#928E8B",
        800: "#191818",
      },
      pink: {
        100: "#FDF7F1",
        200: "#F6F3EF",
        300: "#F7DED3",
        400: "#EBB1A0",
        500: "#EEBAA8",
        600: "#EEC1D2",
        900: "#693548",
      },
    },
    spacing: {
      auto: "auto",
      "0": "0",
      "0-1": 1 / base + "rem", // 1
      "0-2": 2 / base + "rem", // 2
      "0-3": 3 / base + "rem", // 3
      "0-4": 4 / base + "rem", // 4
      "0-5": 5 / base + "rem", // 5
      "0-6": 6 / base + "rem", // 6
      "0-7": 7 / base + "rem", // 7
      "0-8": 8 / base + "rem", // 8
      "0-9": 9 / base + "rem", // 9
      "1": 10 / base + "rem", // 10
      "1-1": 11 / base + "rem", // 11
      "1-2": 12 / base + "rem", // 12
      "1-3": 13 / base + "rem", // 13
      "1-4": 14 / base + "rem", // 14
      "1-5": 15 / base + "rem", // 15
      "1-6": 16 / base + "rem", // 16
      "1-7": 17 / base + "rem", // 17
      "1-8": 18 / base + "rem", // 18
      "1-9": 19 / base + "rem", // 19
      "2": 20 / base + "rem", // 20
      "2-1": 21 / base + "rem", // 21
      "2-2": 22 / base + "rem", // 22
      "2-3": 23 / base + "rem", // 23
      "2-4": 24 / base + "rem", // 24
      "2-5": 25 / base + "rem", // 25
      "2-6": 26 / base + "rem", // 26
      "2-7": 27 / base + "rem", // 27
      "2-8": 28 / base + "rem", // 28
      "2-9": 29 / base + "rem", // 29
      "3": 30 / base + "rem", // 30
      "3-1": 31 / base + "rem", // 32
      "3-2": 32 / base + "rem", // 32
      "3-3": 33 / base + "rem", // 33
      "3-4": 34 / base + "rem", // 34
      "3-5": 35 / base + "rem", // 35
      "3-6": 36 / base + "rem", // 36
      "3-7": 37 / base + "rem", // 37
      "3-8": 38 / base + "rem", // 38
      "3-9": 39 / base + "rem", // 38
      "4": 40 / base + "rem", // 40
      "4-1": 41 / base + "rem", // 41
      "4-2": 42 / base + "rem", // 42
      "4-3": 43 / base + "rem", // 43
      "4-4": 44 / base + "rem", // 44
      "4-5": 45 / base + "rem", // 45
      "4-6": 46 / base + "rem", // 46
      "4-7": 47 / base + "rem", // 47
      "4-8": 48 / base + "rem", // 48
      "4-9": 49 / base + "rem", // 49
      "5": 50 / base + "rem", // 50
      "5-1": 51 / base + "rem", // 51
      "5-2": 52 / base + "rem", // 52
      "5-3": 53 / base + "rem", // 53
      "5-4": 54 / base + "rem", // 54
      "5-5": 55 / base + "rem", // 55
      "5-6": 56 / base + "rem", // 56
      "5-7": 57 / base + "rem", // 57
      "5-8": 58 / base + "rem", // 58
      "5-9": 59 / base + "rem", // 59
      "6": 60 / base + "rem", // 60
      "6-1": 61 / base + "rem", // 61
      "6-2": 62 / base + "rem", // 62
      "6-3": 63 / base + "rem", // 63
      "6-4": 64 / base + "rem", // 64
      "6-5": 65 / base + "rem", // 65
      "6-6": 66 / base + "rem", // 66
      "6-7": 67 / base + "rem", // 67
      "6-8": 68 / base + "rem", // 68
      "7": 70 / base + "rem", // 70
      "7-1": 71 / base + "rem", // 71
      "7-2": 72 / base + "rem", // 72
      "7-3": 73 / base + "rem", // 73
      "7-4": 74 / base + "rem", // 74
      "7-5": 75 / base + "rem", // 75
      "7-6": 76 / base + "rem", // 76
      "7-7": 77 / base + "rem", // 77
      "7-8": 78 / base + "rem", // 78
      "7-9": 79 / base + "rem", // 79
      "8": 80 / base + "rem", // 80
      "8-1": 81 / base + "rem", // 81
      "8-2": 82 / base + "rem", // 82
      "8-3": 83 / base + "rem", // 83
      "8-4": 84 / base + "rem", // 83
      "8-5": 85 / base + "rem", // 85
      "8-6": 86 / base + "rem", // 86
      "8-7": 87 / base + "rem", // 87
      "8-8": 88 / base + "rem", // 88
      "8-9": 89 / base + "rem", // 89
      "9": 90 / base + "rem", // 90
      "9-1": 91 / base + "rem", // 91
      "9-2": 92 / base + "rem", // 92
      "9-3": 93 / base + "rem", // 93
      "9-4": 94 / base + "rem", // 93
      "9-5": 95 / base + "rem", // 95
      "9-6": 96 / base + "rem", // 96
      "9-7": 97 / base + "rem", // 97
      "9-8": 98 / base + "rem", // 98
      "9-9": 99 / base + "rem", // 99
      "10": 100 / base + "rem", // 100
      "10-1": 101 / base + "rem", //101
      "10-2": 102 / base + "rem", //102
      "10-3": 103 / base + "rem", //103
      "10-4": 104 / base + "rem", //104
      "11": 110 / base + "rem", // 110
      "11-3": 113 / base + "rem", // 113
      "11-4": 114 / base + "rem", // 114
      "11-5": 115 / base + "rem", // 115
      "11-6": 116 / base + "rem", // 116
      "12": 120 / base + "rem", // 120
      "12-4": 124 / base + "rem", //124
      "12-8": 128 / base + "rem", // 128
      "13": 130 / base + "rem", // 130
      "13-8": 138 / base + "rem", // 138
      "14": 140 / base + "rem", // 140
      "14-5": 145 / base + "rem", // 145
      "15": 150 / base + "rem", // 150
      "15-3": 153 / base + "rem", // 153
      "15-7": 157 / base + "rem", // 157
      "16": 160 / base + "rem", // 160
      "16-5": 165 / base + "rem", // 165
      "16-6": 166 / base + "rem", // 166
      "17": 170 / base + "rem", // 170
      "18": 180 / base + "rem", // 180
      "18-7": 187 / base + "rem", // 187
      "19": 190 / base + "rem", // 190
      "19-1": 191 / base + "rem", // 191
      "20": 200 / base + "rem", // 200
      "21": 210 / base + "rem", // 210
      "22": 220 / base + "rem", // 220
      "22-5": 225 / base + "rem", // 225
      "23": 230 / base + "rem", // 230
      "24": 240 / base + "rem", // 240
      "25": 250 / base + "rem", // 250
      "26": 260 / base + "rem", // 260
      "28": 280 / base + "rem", // 280
      "29": 290 / base + "rem", // 290
      "30": 300 / base + "rem", // 300
      "31-4": 314 / base + "rem", // 314
      "31-8": 318 / base + "rem", // 318
      "32": 320 / base + "rem", // 320
      "34-4": 344 / base + "rem", // 344
      "36": 360 / base + "rem", // 360
      "37": 370 / base + "rem", // 370
      "40": 400 / base + "rem", // 400
      "43-6": 436 / base + "rem", // 436
      "47-5": 475 / base + "rem", // 475
      "50": 500 / base + "rem", // 500
      "51-6": 516 / base + "rem", // 516
      "53": 530 / base + "rem", // 530
      "56": 560 / base + "rem", // 560
      "58": 580 / base + "rem", // 580
      "61": 610 / base + "rem", // 610
      "65": 650 / base + "rem", // 650
      "72": 720 / base + "rem", // 720
      "75": 750 / base + "rem", // 750
      "76": 760 / base + "rem", // 760
      "82": 820 / base + "rem", // 820
      "90": 900 / base + "rem", // 900
      "92": 920 / base + "rem", // 920
      "110": 1100 / base + "rem", // 1100
      "120": 1200 / base + "rem", // 1200
      "140": 1400 / base + "rem", // 1400
      "150": 1500 / base + "rem", // 1500
      "160": 1600 / base + "rem", // 1600
      "165": 1650 / base + "rem", // 1650
      "17-16": 1716 / base + "rem", // 1780
      "180": 1800 / base + "rem", // 1800
      "190": 1900 / base + "rem", // 1900
      "200": 2000 / base + "rem", // 2000
      medium: 1340 / base + "rem", // 1340
      container: 1440 / base + "rem", // 1440
      wide: 2400 / base + "rem", // 2400
      full: "100%",
      "10vw": "10vw",
      "15vw": "15vw",
      "25vw": "25vw",
      "30vw": "30vw",
      "100vw": "100vw",
      "100vh": "100vh",
      "1000": "1000px",
    },
    backgroundColor: (theme) => theme("colors"),
    backgroundOpacity: (theme) => theme("opacity"),
    backgroundPosition: {
      bottom: "bottom",
      center: "center",
    },
    backgroundSize: {
      auto: "auto",
      cover: "cover",
      contain: "contain",
    },
    borderColor: (theme) => ({
      ...theme("colors"),
    }),
    borderOpacity: (theme) => theme("opacity"),
    borderRadius: {
      none: "0",
      xs: "2px",
      s: "5px",
      default: "8px",
      md: "10px",
      lg: "25px",
      full: "9999px",
    },
    borderWidth: {
      sm: "0.5px",
      default: "1px",
      "0": "0",
      "2": "2px",
      "1/5": "1.5px",
    },
    boxShadow: {
      outline: "0 0 0 3px rgba(66, 153, 225, 0.5)",
      md: " 0px 15px 25px rgba(0, 0, 0, 0.12)",
      none: "none",
    },
    container: {},
    cursor: {
      auto: "auto",
      default: "default",
      pointer: "pointer",
      "not-allowed": "not-allowed",
    },
    divideColor: (theme) => theme("borderColor"),
    divideOpacity: (theme) => theme("borderOpacity"),
    divideWidth: (theme) => theme("borderWidth"),
    fill: {
      current: "currentColor",
    },
    flex: {
      "1": "1 1 0%",
      auto: "1 1 auto",
      initial: "0 1 auto",
      none: "none",
    },
    flexGrow: {
      "0": "0",
      default: "1",
    },
    flexShrink: {
      "0": "0",
      default: "1",
    },
    fontFamily: {
      baskerville: ["baskerville", '"Helvetica Neue"', "Arial"],
      condensed: ["condensed", '"Times New Roman"', "Times", "serif"],
    },
    fontSize: {
      "4xs": 7 / base + "rem", // 7px
      "2xs": 12 / base + "rem", // 12px
      "1/2xs": 14 / base + "rem", // 14px
      xs: 15 / base + "rem", // 15px
      sxs: 17 / base + "rem", // 17px
      sm: 18 / base + "rem", // 18px
      base: "1rem", // 16px
      md: 20 / base + "rem", // 20px
      lg: 22 / base + "rem", //22px
      "2lg": 24 / base + "rem", // 24px
      xl: 26 / base + "rem", // 26px
      "2xl": 28 / base + "rem", // 28px
      "29": 29 / base + "rem", // 29px
      "2-3xl": 32 / base + "rem", //32px
      "3xl": 36 / base + "rem", // 36px
      "4xl": 40 / base + "rem", // 40px
      "5xl": 42 / base + "rem", // 42px
      "6xl": 62 / base + "rem", // 62px
    },
    fontWeight: {
      thin: "300",
      normal: "normal",
      medium: "500",
      bold: "bold",
      extraBold: "900"
    },
    height: (theme) => ({
      ...theme("spacing"),
      "9/12": "75%",
      "10/12": "83.333333%",
    }),
    inset: {
      "0": "0",
      "40": "40%",
      "50": "50%",
      "51": "51%",
      "52": "52%",
      "100": "100%",
      auto: "auto",
    },
    letterSpacing: {
      normal: "0",
      sm: "0.05em",
      md: "0.1em",
      mlg: "0.15em",
      lg: "0.2em",
      xl: "0.25em",
      wide: "1px",
    },
    lineHeight: {
      base: "normal",
      none: "1",
      tightest: "1.1",
      tight: "1.25",
      normal: "1.5",
      loose: "2",
      looser: "3",
    },
    listStyleType: {
      none: "none",
      disc: "disc",
      decimal: "decimal",
    },
    margin: (theme, { negative }) => ({
      auto: "auto",
      ...theme("spacing"),
      ...negative(theme("spacing")),
    }),
    maxHeight: (theme) => ({
      ...theme("spacing"),
    }),
    maxWidth: (theme) => ({
      ...theme("spacing"),
    }),
    minHeight: (theme) => ({
      ...theme("spacing"),
    }),
    minWidth: (theme) => ({
      ...theme("spacing"),
    }),
    objectPosition: {
      bottom: "bottom",
      center: "center",
      left: "left",
      "left-bottom": "left bottom",
      "left-top": "left top",
      right: "right",
      "right-bottom": "right bottom",
      "right-top": "right top",
      top: "top",
    },
    opacity: {
      "0": "0",
      "20": "0.2",
      "25": "0.25",
      "30": "0.3",
      "40": "0.4",
      "50": "0.5",
      "60": "0.6",
      "70": "0.7",
      "75": "0.75",
      "80": "0.8",
      "90": "0.9",
      "100": "1",
    },
    order: {
      first: "-9999",
      last: "9999",
      none: "0",
      "1": "1",
      "2": "2",
    },
    padding: (theme, { negative }) => ({
      auto: "auto",
      ...theme("spacing"),
      ...negative(theme("spacing")),
    }),
    placeholderColor: (theme) => theme("colors"),
    stroke: {
      current: "currentColor",
    },
    textColor: (theme) => theme("colors"),
    width: (theme) => ({
      auto: "auto",
      ...theme("spacing"),
      "1/2": "50%",
      "1/3": "33.333333%",
      "2/3": "66.666667%",
      "1/4": "25%",
      "2/4": "50%",
      "3/4": "75%",
      "1/5": "20%",
      "2/5": "40%",
      "3/5": "60%",
      "4/5": "80%",
      "1/6": "16.666667%",
      "2/6": "33.333333%",
      "3/6": "50%",
      "4/6": "66.666667%",
      "5/6": "83.333333%",
      "1/8": "12.5%",
      "1/9": "11.111111%",
      "4/9": "44.444444%",
      "5/9": "55.555556%",
      "8/9": "88.888889%",
      "1/10": "10%",
      "3/10": "30%",
      "7/10": "70%",
      "1/12": "8.333333%",
      "2/12": "16.666667%",
      "3/12": "25%",
      "4/12": "33.333333%",
      "5/12": "41.666667%",
      "6/12": "50%",
      "7/12": "58.333333%",
      "8/12": "66.666667%",
      "9/12": "75%",
      "10/12": "83.333333%",
      "11/12": "91.666667%",
      full: "100%",
    }),
    zIndex: {
      auto: "auto",
      "-1": "-1",
      "0": "0",
      "1": "1",
      "2": "2",
      "3": "3",
      "4": "4",
      "10": "10",
      "12": "12",
      "full": "9999",
    },
  },
  variants: {
    accessibility: ["responsive", "focus"],
    alignContent: ["responsive"],
    alignItems: ["responsive"],
    alignSelf: ["responsive"],
    appearance: ["responsive"],
    backgroundAttachment: ["responsive"],
    backgroundColor: ["responsive", "hover", "focus", "group-hover"],
    backgroundPosition: ["responsive"],
    backgroundRepeat: ["responsive"],
    backgroundSize: ["responsive"],
    borderCollapse: ["responsive"],
    borderColor: ["responsive", "hover", "focus"],
    borderRadius: ["responsive"],
    borderStyle: ["responsive"],
    borderWidth: ["responsive", "last", "hover"],
    boxShadow: ["responsive", "hover", "focus"],
    cursor: ["responsive"],
    display: ["responsive", "group-hover"],
    fill: ["responsive"],
    flex: ["responsive", "group-hover"],
    flexDirection: ["responsive"],
    flexGrow: ["responsive"],
    flexShrink: ["responsive"],
    flexWrap: ["responsive"],
    float: ["responsive"],
    fontFamily: ["responsive"],
    fontSize: ["responsive"],
    fontSmoothing: ["responsive"],
    fontStyle: ["responsive"],
    fontWeight: ["responsive", "hover", "focus"],
    height: ["responsive"],
    inset: ["responsive"],
    justifyContent: ["responsive"],
    letterSpacing: ["responsive"],
    lineHeight: ["responsive"],
    listStylePosition: ["responsive"],
    listStyleType: ["responsive"],
    margin: ["responsive"],
    maxHeight: ["responsive"],
    maxWidth: ["responsive"],
    minHeight: ["responsive"],
    minWidth: ["responsive"],
    objectFit: ["responsive"],
    objectPosition: ["responsive"],
    opacity: ["responsive", "hover", "focus", "group-hover"],
    order: ["responsive"],
    outline: ["responsive", "focus"],
    overflow: ["responsive"],
    padding: ["responsive, first, last"],
    placeholderColor: ["responsive", "focus"],
    pointerEvents: ["responsive", "group-hover"],
    position: ["responsive"],
    resize: ["responsive"],
    stroke: ["responsive"],
    tableLayout: ["responsive"],
    textAlign: ["responsive"],
    textColor: ["responsive", "hover", "focus", "group-hover"],
    textDecoration: ["responsive", "hover", "focus"],
    textTransform: ["responsive"],
    userSelect: ["responsive"],
    verticalAlign: ["responsive"],
    visibility: ["responsive", "group-hover"],
    whitespace: ["responsive"],
    width: ["responsive"],
    wordBreak: ["responsive"],
    zIndex: ["responsive"],
  },
  corePlugins: {},
  plugins: [
    function({ addUtilities }) {
      const newUtilities = {
        ".translate-x-50": {
          transform: "translateX(-50%)",
        },
      };

      addUtilities(newUtilities);
    },
  ]
};
