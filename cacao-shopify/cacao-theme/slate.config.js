/* eslint-disable */

// Configuration file for all things Slate.
// For more information, visit https://github.com/Shopify/slate/wiki/Slate-Configuration

const path = require('path');
const fs = require("fs");
const tailwindcss = require("tailwindcss");
module.exports = {
  "webpack.babel.enable": true,
  "cssVarLoader.liquidPath": ["src/snippets/css-variables.liquid"],
  "webpack.extend": {
    resolve: {
      alias: {
        jquery: path.resolve("./node_modules/jquery"),
        "lodash-es": path.resolve("./node_modules/lodash-es"),
      },
    },
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            {
              loader: "postcss-loader",
              options: {
                ident: "postcss",
                importLoaders: 1,
                plugins: [
                  require('postcss-preset-env')({ stage: 0 }),
                  require("tailwindcss"),
                  require("autoprefixer")
                ],
              },
            },
          ],
          include: path.resolve(__dirname, "./src/styles/theme.scss"),
        }
      ]
    }
  },
};
