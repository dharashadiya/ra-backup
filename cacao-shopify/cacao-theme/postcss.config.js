const fs = require("fs");
const tailwindcss = require("tailwindcss");

const siteSpecificPath = `./tailwind.config.${process.env.SITE}.js`;

module.exports = {
  plugins: [
    require("postcss-import"),
    require('postcss-preset-env')({ stage: 0 }),
    require("tailwindcss"),
    require("postcss-nested"), // or require('postcss-nesting')
    require("autoprefixer")
  ],
};

// const purgecss = require("@fullhuman/postcss-purgecss")({
//   // Specify the paths to all of the template files in your project
//   content: [
//     "./src/**/*.html",
//     "./src/**/*.liquid",
//     "./src/**/*.jsx",
//     // etc.
//   ],
//
//   // Include any special characters you're using in this regular expression
//   defaultExtractor: (content) => content.match(/[A-Za-z0-9-_:/]+/g) || [],
// });
//
// module.exports = {
//   plugins: [
//     require("tailwindcss"),
//     require("autoprefixer"),
//     ...(process.env.NODE_ENV === "production" ? [purgecss] : []),
//   ],
// };
