$(document).ajaxComplete(function (event, request, settings) {
  // if (request.responseJSON.message == 'Cart Error') {
  //   window.cartError = request.responseJSON.description;
  // }
  if(CartJS.cart.items.length > 0) {
    var cartTotalItems = 0

    CartJS.cart.items.forEach(function(item,index) {
      if(item.properties && item.properties.Box) {

      }
      else {
        cartTotalItems += item.quantity
      }
    })

    $('[data-cart-count]').text(cartTotalItems);
  }
});

$(document).on('cart.requestComplete', function (event, cart) {
  if($('body').hasClass('template-cart')) {
    location.reload();
  }
  else {
    if(CartJS.cart.items.length > 0) {
      let cart = CartJS.cart
      var html = '';
      var cartTotalItems = 0;

      cart.items.forEach(function(item,index) {
        var qty = 0;
        var itemIndex = ++index;
        var individual = 'flex'

        if(item.properties && item.properties.Box) {
        }
        else {
          cartTotalItems += item.quantity
        }
      })

      $('[data-cart-count]').text(cartTotalItems);

    //  location.reload();
      //$('[data-minicart]').addClass('active');

  //     cart.items.forEach(function(item,index) {
  //         var qty = 0;
  //         var itemIndex = ++index;
  //         var individual = 'flex'
  //
  //         if(item.properties && item.properties.Box) {
  //           individual = 'hidden'
  //         }
  //         else {
  //           cartTotalItems++
  //         }
  //         html += '<div class="'+individual+' sm:items-center border-b border-black sm:py-0 pt-1-2 pb-2 relative" data-product-line="'+item.id+'" data-item-line="'+itemIndex+'">';
  //         html += '<div class="w-9 sm:mr-2 mr-0-7"><a href="'+item.url+'"><img src="'+item.image+'" /></a></div>';
  //         html += '<div class="flex-1 flex sm:items-center sm:flex-row flex-col">';
  //         html += '<div class="flex-1 sm:py-1">';
  //         html += '<div><div class="flex-1 sm:block flex">';
  //         html += '<p class="font-condensed text-sm font-bold text-black leading-normal tracking-sm sm:flex-1 uppercase sm:pr-0 pr-2">'+item.product_title+'</p>';
  //         var priceTax = item.line_price * 1.1
  //         var priceOrgTax = item.original_price * 1.1
  //         html += '<p class="font-condensed text-sm font-bold tracking-sm leading-normal text-black" data-mincart-lineprice="'+item.id+'" data-price-line="'+itemIndex+'" data-line-price="'+priceOrgTax+'">' + Shopify.formatMoney(priceTax, window.moneyFormat) + '</p>';
  //         html += '</div></div>';
  //         if(item.properties && item.properties.selection) {
  //           var propertySelection = item.properties.selection
  //           html += '<div class=""><div class="flex"><p class="uppercase text-1/2xs tracking-md font-condensed mr-0-5">Selection: </p><p class="uppercase text-1/2xs tracking-md font-condensed">'+propertySelection+'</p></div></div>';
  //         }
  //         html += '</div>'
  //         html += '<div class="sm:pr-3 sm:pl-1">'
  //         html += '<div class="flex items-center" data-id="'+item.id+'" data-max="" data-line="'+itemIndex+'">'
  //         html += '<a class="font-condensed text-base tracking-md w-2 h-2-6 cursor-pointer flex justify-center items-center" data-mini-minus="">-</a>'
  //         html += '<input type="number" value="'+item.quantity+'"  pattern="[0-9]*" class="w-5 mx-0-5 border-0 text-center font-condensed text-md tracking-md" max="'+qty+'">'
  //         html += '<a class="font-condensed text-base tracking-md w-2 h-2-6 cursor-pointer flex justify-center items-center" data-mini-plus>+</a>'
  //         html += '</div></div>'
  //
  //         html += '<div class="sm:static absolute right-0 top-0 bottom-0 flex items-center"><a data-total-qty="'+item.quantity+'" data-relevant-ids="'+item.properties['ids']+'" data-remove-item="'+item.id+'" class="font-condensed text-2xs font-bold tracking-md text-black text-opacity-50 uppercase cursor-pointer"><svg class="w-1-2 h-1-2 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M22.66 24L12 13.34 1.33 24 0 22.66 10.66 12 0 1.33 1.33 0 12 10.66 22.66 0 24 1.33 13.34 12 24 22.66 22.66 24z"></path></svg></a></div>';
  //         html += '</div>';
  //         html += '</div>';
  //     });
  //
  //     $('[data-cart-count]').text(cartTotalItems);
  //     $('[data-cart-view]').html(html);
  //     $('[data-minicart-checkout]').removeClass('hidden');
  //     $('[data-empty-text]').addClass('hidden');
  //     var subPriceTax = cart.total_price * 1.1
  //     $('[data-minicart-subtotal]').text(Shopify.formatMoney(subPriceTax, window.moneyFormat));
  //     $('[data-cart-subtotal]').text(Shopify.formatMoney(subPriceTax, window.moneyFormat));
  //     setTimeout(function(){
  //       $('[data-minicart]').addClass('active');
  //     },600);
     }
 }



  if(CartJS.cart.item_count == 0) {
    document.querySelector('[data-minicart] form').classList.add('hidden')
    document.querySelector('[data-empty-text]').classList.remove('hidden')
  }


  // $('[data-minicart-trigger]').removeClass('is-loading');
  //
  // let button = $('[data-cart-add].is-loading, [data-add-to-cart].is-loading, #preorder-me-btn.is-loading');
  // if (button.attr('value') == 'Pre-order') {
  //   button = button.parent();
  // }
  // button.removeClass('is-loading')
  //
  // if (window.cartError) {
  //   $('[data-minicart] p.error').text(window.cartError);
  //   $('[data-minicart]').removeClass('is-add').addClass('is-error');
  //   window.cartError = null;
  // } else {
  //   button.addClass('is-complete');
  //
  //   setTimeout(() => {
  //     button.removeClass('is-complete').removeClass('is-error');
  //   }, 2500);
  // }
  //
  // $('[data-cart-view] [type="submit"]').prop("disabled", false);
  // $('[data-minicart]').find('div.products').scrollTop(0).find('div.product').first().addClass('top');
  //
  // if ($('[data-minicart]').find('div.product').length < 3) {
  //   $('[data-minicart] [data-up], [data-minicart] [data-down]').removeClass('is-active');
  //   $('[data-minicart] [data-down]').css('display','none');
  //   $('[data-minicart] [data-up]').css('display','none');
  // } else {
  //   $('[data-minicart] [data-down]').addClass('is-active');
  //   $('[data-minicart] [data-down]').css('display','flex');
  //   $('[data-minicart] [data-up]').css('display','flex');
  // }
  //
  // if (!$('body').hasClass('template-cart')) {
  //   $('[data-minicart]').addClass('is-active');
  //   setTimeout(function () {
  //     $('[data-minicart]').removeClass('is-active').removeClass('is-add').removeClass('is-remove').removeClass('is-error');
  //     $('[data-minicart] p.error').text('');
  //   }, 7000);
  // }
});
