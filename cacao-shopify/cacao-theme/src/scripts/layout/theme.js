import $ from 'jquery'
import 'lazysizes/plugins/object-fit/ls.object-fit';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import 'lazysizes/plugins/rias/ls.rias';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes';
import 'lazysizes/plugins/respimg/ls.respimg';
import AOS from 'aos';
import 'aos/dist/aos.css';
import lity from 'lity';
import "../../styles/components/accordion.scss";
import '../components/accordion'
import '../components/tab'
import '../components/mini-cart'
import Cookie from "js.cookie";

import {focusHash, bindInPageLinks} from '@shopify/theme-a11y';
import {cookiesEnabled} from '@shopify/theme-cart';

import "../../styles/theme.scss";
import "../../styles/theme.scss.liquid";
import "../sections/header"
// Common a11y fixes
focusHash();
bindInPageLinks();

AOS.init();
window.theme = {
  lity: lity,
  Cookie: Cookie
}
let scrollpos = window.scrollY
const header = document.querySelector('[data-section-type="header"]')
const header_height = 10
const miniCart = document.querySelector('[data-minicart-open]')
const miniCartClose = document.querySelectorAll('[data-cart-close]')

if (miniCart) {
  miniCart.addEventListener('click', () => {
    document.querySelector('[data-minicart]').classList.add('active')
    //document.querySelector('[data-section-id="header"]').classList.add("open-nav")
  })
}
if (miniCartClose) {
  miniCartClose.forEach(item => {
    item.addEventListener('click', () => {
      document.querySelector('[data-minicart]').classList.remove('active')
      //document.querySelector('[data-section-id="header"]').classList.remove("open-nav")
    })
  })
}

const close = document.querySelectorAll('[data-close-lity]')
close.forEach((element)=>{
  element.addEventListener('click', ()=> {
    let closeLity = document.querySelector('[data-lity-close]')
    closeLity.click()
  })
})

const popup = document.querySelector('#popupBanner')
if(popup && !Cookie.get('closeBanner')) {
  setTimeout(()=>{
    lity(popup)
  },5000)
}


const announcement = document.querySelector('#announcementBanner')
let cookie_name = $("#announcementBanner").data("announcement_cookie");
if(announcement && !Cookie.get(cookie_name)) {
  setTimeout(()=>{
    lity("#announcementBanner")
  },5000)
  Cookie.set(cookie_name, '1', { expires: 1 });

}



$(document).on('lity:close', function(event, instance) {
  let target = event.target.id

  if(target == 'video') {
    var video = $(this).find('video')
    if(video) {
      video.get(0).pause()
    }
    $(this).find('.video-wrapper').fadeOut(500);
  }
  if(target == 'popupBanner') {
    Cookie.set('closeBanner', '1', { expires: 1 });
  }

  if(target == 'productsRecommended') {
    Cookie.set('productsRecommended', true, { expires: 10 });
    const getUrl = window.location.href

    // if(getUrl.includes('selector')) {}
    // reload for all products add to cart 
      location.reload();
    
  }
});

$(document).on('lity:open', function(event, instance) {
  let target = event.target.id
  console.log(target)
  if($('.lity-opened').find('#popupBanner')) {
    setTimeout(()=>{
      $('#popupBanner').addClass('open')
    },300)
  }
  // if we can find the video div on the pageXOffset, play it when its open
  // if($("#video").length)
  //   if(window.outerWidth < 680) {
  //     if($(this).find('video')) {
  //       $(this).find('video').get(0).play()
  //     }
  //   }
});

$(document).on("click",'.show-video', function(event) {
  $("#video").find('video').get(0).play()
});


$(document).on('lity:ready', function(event, instance) {
  let target = event.target.id
  if(target == 'video') {
    var video = $(this).find('.video-wrapper')
    video.fadeIn(1000);
    // if(video) {
    //   video.addClass('loaded')
    // }
  }
});

const productPlus = document.querySelector('[data-product-plus]')
const productMinus = document.querySelector('[data-product-minus]')

if(productPlus) {
  productPlus.addEventListener('click', ()=> {
    //let maxInventory = productPlus.parentElement.querySelector('[data-max]').dataset.max
    let currentQty = productPlus.parentElement.querySelector('#Quantity').value
    let productId = productPlus.dataset.productPlus
    let productPrice = document.querySelector(`[data-product-price="${productId}"]`)
    let mainPrice = productPrice.dataset.mainPrice
    currentQty++

    //if(currentQty <= maxInventory) {
    productPlus.parentElement.querySelector('#Quantity').value = currentQty
    let newPrice = mainPrice * currentQty
    productPrice.innerText = Shopify.formatMoney(newPrice, window.moneyFormat)
    //}

  })
}
if(productMinus) {
  productMinus.addEventListener('click', ()=> {
    //let maxInventory = productMinus.parentElement.querySelector('[data-max]').dataset.max
    let currentQty = productMinus.parentElement.querySelector('#Quantity').value
    let productId = productMinus.dataset.productMinus
    let productPrice = document.querySelector(`[data-product-price="${productId}"]`)
    let mainPrice = productPrice.dataset.mainPrice
    currentQty--

    if(currentQty > 0) {
      productMinus.parentElement.querySelector('#Quantity').value = currentQty
      let newPrice = mainPrice * currentQty
      productPrice.innerText = Shopify.formatMoney(newPrice, window.moneyFormat)
    }
  })
}

window.addEventListener('scroll', function() {
  scrollpos = window.scrollY;
  scrollpos = scrollpos

  if (scrollpos >= header_height) {
    header.classList.add('fixed-header')
  }
  else {
    header.classList.remove('fixed-header')
  }
})
window.addEventListener('load', function() {
  scrollpos = window.scrollY;
  scrollpos = scrollpos

  if (scrollpos >= header_height) {
    header.classList.add('fixed-header')
  }
  else {
    header.classList.remove('fixed-header')
  }
})

$(document).ready(function(){
  setTimeout(function(){
    $('body').addClass('loaded')
  },400)
  if(window.outerWidth < 680) {
    if(document.querySelector('[data-section-type="footer"]')) {
      document.querySelector('[data-section-type="footer"]').classList.add('mobile-size')
    }
  }
  if(CartJS.cart.items.length > 0) {
    var cartTotalItems = 0

    CartJS.cart.items.forEach(function(item,index) {
      if(item.properties && item.properties.Box) {

      }
      else {
        cartTotalItems += item.quantity
      }
    })

    $('[data-cart-count]').text(cartTotalItems);
  }

  //add the message to the multishipping page about calculating shipping price
  $("#gsCheckoutSidebar .gs__total-line.gs__total-line-taxes small").append("<p style='color: #C468C1;font-weight: 600;'>Shipping is calculated separately for each address<br />and charges will apply if order value is under $99.</p>");

})
$(document).resize(function(){
  if(window.outerWidth < 680) {
    if(document.querySelector('[data-section-type="footer"]')) {
      document.querySelector('[data-section-type="footer"]').classList.add('mobile-size')
    }
  }
  else {
    if(document.querySelector('[data-section-type="footer"]')) {
      document.querySelector('[data-section-type="footer"]').classList.remove('mobile-size')
    }
  }
})
