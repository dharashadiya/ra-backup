import $ from 'jquery'
const itemRemove = document.querySelectorAll('[data-remove-item]')

// if(itemRemove && itemRemove.length > 0) {
//   itemRemove.forEach((item, i) => {
//     item.addEventListener('click', ()=>{
//       let itemId = item.dataset.removeItem
//       CartJS.updateItemById(itemId, 0).then(()=>{
//         document.querySelector(`[data-product-line="${itemId}"]`).classList.add('hidden')
//         document.querySelector('[data-cart-subtotal]').innerText = Shopify.formatMoney(CartJS.cart.total_price, window.moneyFormat)
//         document.querySelector('[data-cart-count]').innerText = CartJS.cart.item_count
//         if(CartJS.cart.item_count == 0) {
//           document.querySelector('[data-minicart] form').classList.add('hidden')
//           document.querySelector('[data-empty-text]').classList.remove('hidden')
//         }
//       })
//
//     })
//   });
// }

function compareArrays(array1, array2) {
  var is_same = array1.length == array2.length && array1.every(function(element, index) {
      return element === array2[index];
  });
  return is_same;
}

var newItems = []
var removeItems = []
var plusItems = []
var minusItems = []
const productLists = document.querySelectorAll('[data-product-line]')

function searchOne(nameKey, myArray){
  for (var key of Object.keys(myArray)) {
    if(key == nameKey) {
      return myArray;
    }
  }
}

function search(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        var newArr = myArray[i]
        for (var key of Object.keys(newArr)) {
          if(key == nameKey) {
            return newArr;
          }
        }
    }
}

if(productLists) {
  productLists.forEach((item, i) => {
      if(!item.classList.contains('hidden')) {
        let itemId = item.dataset.productLine
        let lineIndex = item.dataset.itemLine
            lineIndex--
        var itemArray = {}
        var itemPlus = {}
        var itemMinus = {}

        let mainQuantity = CartJS.cart.items[lineIndex].quantity
        let cartminusitem = CartJS.cart.items[lineIndex].quantity
          --cartminusitem


        itemArray[itemId] = 0;
        itemPlus[itemId] = ++mainQuantity

        itemMinus[itemId] = cartminusitem

        if(CartJS.cart.items[lineIndex].properties && CartJS.cart.items[lineIndex].properties['ids']) {
          CartJS.cart.items[lineIndex].properties['ids'].forEach((item, i) => {
            if(document.querySelector(`[data-product-line="${item}"]`)) {
              let lineItem = document.querySelector(`[data-product-line="${item}"]`).dataset.itemLine
              lineItem--
              let itemQty = CartJS.cart.items[lineItem].quantity

              var itemParent = itemQty / CartJS.cart.items[lineIndex].quantity
              var minusQuantity = itemQty - itemParent
              var plusQuantity = itemQty + itemParent

              itemPlus[item] = plusQuantity
              itemMinus[item] = minusQuantity
              itemArray[item] = 0;
            }
          });
        }

        removeItems.push(itemArray)
        plusItems.push(itemPlus)
        minusItems.push(itemMinus)
      }
  });
}

$(document).on('click', '[data-remove-item]', function (e) {
  let itemId = $(this).data('remove-item')
  var relevantIds = $(this).data('relevant-ids')
  let itemQuantity = $(this).data('total-qty')
  let itemLine = $(this).parent().parent().parent().data('item-line')

  if(search(itemId,removeItems)) {
    let newUpdate = search(itemId,removeItems)
    $.post('/cart/update.js', {
        updates: newUpdate
      })
      .done(function() {
        console.log( "success" );
        location.reload();
      })
      .fail(function() {
        console.log( "error" );
      });
  }
})

$(document).on('click', '[data-mini-plus]', function (e) {
    var itemId = $(this).parent().data('id')
    let index = $(this).parent().data('line')
    let productLine = $(this).parent().data('line')
    var currentValue = $(this).parent().find('input').val()
    var plusValue = ++currentValue
    index++

    if(search(itemId,plusItems)) {
      let newUpdate = search(itemId,plusItems)
      $.post('/cart/update.js', {
          updates: newUpdate
        })
        .done(function() {
          console.log( "success" );
          location.reload();
        })
        .fail(function() {
          console.log( "error" );
        });
    }
})
$(document).on('click', '[data-mini-minus]', function (e) {
  var itemId = $(this).parent().data('id')
  let index = $(this).parent().data('line')
  let productLine = $(this).parent().data('line')
  var currentValue = $(this).parent().find('input').val()
  var minusValue = --currentValue
  index--

  if(search(itemId,minusItems)) {
    let newUpdate = search(itemId,minusItems)
    $.post('/cart/update.js', {
        updates: newUpdate
      })
      .done(function() {
        console.log( "success" );
        location.reload();
      })
      .fail(function() {
        console.log( "error" );
      });
  }
})
