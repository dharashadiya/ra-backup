const accordionItems = document.querySelectorAll('[data-accordion-title]')
const stickyBox = document.querySelector('[data-sticky]')

if(accordionItems && accordionItems.length > 0) {
  accordionItems.forEach(item => {
    item.addEventListener('click', ()=>{
      const itemParent = item.parentElement
      const itemHeight = itemParent.querySelector('.content > div').offsetHeight
      
      if(itemParent.classList.contains('open')) {
        itemParent.classList.remove('open')
        itemParent.querySelector('.content').style.maxHeight = 0
      }
      else {
        itemParent.classList.add('open')
        itemParent.querySelector('.content').style.maxHeight = itemHeight + 'px'
      }
    })
  });
}
