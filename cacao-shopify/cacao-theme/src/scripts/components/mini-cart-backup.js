import $ from 'jquery'
const itemRemove = document.querySelectorAll('[data-remove-item]')

// if(itemRemove && itemRemove.length > 0) {
//   itemRemove.forEach((item, i) => {
//     item.addEventListener('click', ()=>{
//       let itemId = item.dataset.removeItem
//       CartJS.updateItemById(itemId, 0).then(()=>{
//         document.querySelector(`[data-product-line="${itemId}"]`).classList.add('hidden')
//         document.querySelector('[data-cart-subtotal]').innerText = Shopify.formatMoney(CartJS.cart.total_price, window.moneyFormat)
//         document.querySelector('[data-cart-count]').innerText = CartJS.cart.item_count
//         if(CartJS.cart.item_count == 0) {
//           document.querySelector('[data-minicart] form').classList.add('hidden')
//           document.querySelector('[data-empty-text]').classList.remove('hidden')
//         }
//       })
//
//     })
//   });
// }

function compareArrays(array1, array2) {
  var is_same = array1.length == array2.length && array1.every(function(element, index) {
      return element === array2[index];
  });
  return is_same;
}

$(document).on('click', '[data-remove-item]', function (e) {
  let itemId = $(this).data('remove-item')
  var relevantIds = $(this).data('relevant-ids')
  let itemQuantity = $(this).data('total-qty')
  let itemLine = $(this).parent().parent().parent().data('item-line')

  CartJS.removeItem(itemLine);
 $(this).parent().parent().parent().addClass('hidden')

  CartJS.cart.items.forEach(function(item,index) {
    if(relevantIds) {
      var ids = relevantIds.split(',')
      ids.forEach((childItem, i) => {
        if(childItem == item.id) {
          let itemQty = parseInt(item.quantity)
          itemQty--
          CartJS.updateItemById(childItem, itemQty)
        }
      })
    }
  })
})

$(document).on('click', '[data-mini-plus]', function (e) {
    var itemId = $(this).parent().data('id')
    let index = $(this).parent().data('line')
    let productLine = $(this).parent().data('line')
    var currentValue = $(this).parent().find('input').val()
    //currentValue++;
    var plusValue = ++currentValue
    index--
    // var maxQty = $(this).parent().data.max
    // var orgPrice = $("[data-cart-subtotal]").data('cart-subtotal')
    // var line = $(this).parent().data('line')
    // var newLineArray = [];

  //  var linePrice = $("[data-mincart-lineprice='"+itemId+"']").data('line-price')
    //var priceOrg = linePrice * plusValue
    // CartJS.cart.items.forEach((element, index) => {
    //   if(element.id == itemId) {
    //     newLineArray.push(++index);
    //   }
    // })

    CartJS.updateItem(productLine, plusValue);

    if(CartJS.cart.items[index].properties && CartJS.cart.items[index].properties['ids']) {
      CartJS.cart.items[index].properties['ids'].forEach((item, i) => {
        let itemLine = $('[data-product-line="'+item+'"]').data('item-line')
        let itemQty = $('[data-id="'+item+'"]').find('input').val()
        itemQty++
        
        CartJS.updateItem(itemLine, itemQty)
      });
    }


    // var newLine = newLineArray[0];
    // CartJS.updateItemById(itemId, plusValue).then(function(){
    //   let priceTax = CartJS.cart.total_price * 1.1
    //   $("[data-cart-subtotal]").html(Shopify.formatMoney(priceTax, window.moneyFormat));
    //   $("[data-cart-subtotal]").attr('data-cart-subtotal',priceTax);
    //   $('[data-cart-count]').text(CartJS.cart.item_count);
    //   $("[data-product-line='"+ itemId +"']").find('input').val(plusValue);
    //   $("[data-mincart-lineprice='"+itemId+"']").text(Shopify.formatMoney(priceOrg, window.moneyFormat));

      // CartJS.cart.items.forEach((element, index) => {
      //   var itemIndex = ++index
      //   if (element.id == itemId) {
      //       var price = Shopify.formatMoney(element.line_price, window.moneyFormat)
      //       var linePrice = $("[data-price-line='"+line+"']").text()
      //       if(orgPrice < CartJS.cart.total_price) {
      //         $("[data-product-line='"+ itemId +"']").find('input').val(plusValue);
      //         $("[data-price-line='"+line+"']").text(price);
      //       }
      //   }
      // })
    // });
})
$(document).on('click', '[data-mini-minus]', function (e) {
     console.log('itemId')
    var itemId = $(this).parent().data('id')
    var currentValue = $(this).parent().find('input').val()
    var minusValue = --currentValue
    var newLineArray = [];

    var line = $(this).parent().data('line')
    var linePrice = $("[data-mincart-lineprice='"+itemId+"']").data('line-price')
    var priceOrg = linePrice * minusValue
    // if(minusValue == 0) {
    //   $("[data-item-line='"+ line +"']").remove()
    // }
     // CartJS.cart.items.forEach((element, index) => {
     //   if(element.id == itemId) {
     //     newLineArray.push(++index);
     //   }
     // })


  CartJS.updateItemById(itemId, minusValue).then(function(){

     let priceTax = CartJS.cart.total_price
     $("[data-cart-subtotal]").html(Shopify.formatMoney(priceTax, window.moneyFormat));
     $("[data-cart-subtotal]").attr('data-cart-subtota',priceTax);
     $("[data-product-line='"+ itemId +"']").find('input').val(minusValue);
     $("[data-mincart-lineprice='"+itemId+"']").text(Shopify.formatMoney(priceOrg, window.moneyFormat));

     $('[data-cart-count]').text(CartJS.cart.item_count);
     if(CartJS.cart.item_count < 1) {
       $('[data-minicart-checkout]').addClass('hidden');
       $('[data-empty-text]').removeClass('hidden');
     }

     // CartJS.cart.items.forEach((element, index) => {
     //   var itemIndex = ++index
     //   if (element.id == itemId) {
     //       var price = Shopify.formatMoney(element.line_price, window.moneyFormat)
     //       var linePrice = $("[data-price-line='"+line+"']").text()
     //       $("[data-product-line='"+ itemId +"']").find('input').val(minusValue);
     //       $("[data-price-line='"+line+"']").text(price);
     //   }
     // })
   });
})
