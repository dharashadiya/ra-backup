const tabs = document.querySelectorAll('[data-tab-id]')
const tabsContent = document.querySelectorAll('.content-tab')

if(tabs && tabs.length > 0) {
  tabs.forEach((item, i) => {
    item.addEventListener('click', ()=> {
      let itemId = item.dataset.tabId
      tabsContent.forEach((item, i) => {
        item.classList.remove('active')
      });
      tabs.forEach((item, i) => {
        item.classList.remove('active')
      });

      item.classList.add('active')
      document.getElementById(itemId).classList.add('active')
    })
  });
}
