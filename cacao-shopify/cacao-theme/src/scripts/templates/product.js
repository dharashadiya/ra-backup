import {load} from '@shopify/theme-sections';
import '../sections/product';
import '../sections/product-selector';
import '../sections/product-boxes';
import './page';
load('*');
