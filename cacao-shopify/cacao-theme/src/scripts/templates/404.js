/* add "page" in start of the URL if page not exist(hit 404 page), 
*  and url not contains blog, page, product and collection. which means 
* it may redirected from indexed pages with no "page" in url from old site.
*/ 
let getUrl = window.location.href
if(!getUrl.includes('collection') && !getUrl.includes('product') && !getUrl.includes('blog') && !getUrl.includes('page')) {
  window.location.href = '/pages'+window.location.pathname
}
