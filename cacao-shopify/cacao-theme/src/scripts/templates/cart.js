import "../../styles/templates/cart.scss";
const itemPlus = document.querySelectorAll('[data-cart-plus]')
const itemMinus = document.querySelectorAll('[data-cart-minus]')

function compareArrays(array1, array2) {
  var is_same = array1.length == array2.length && array1.every(function(element, index) {
      return element === array2[index];
  });
  return is_same;
}

function compareArrays(array1, array2) {
  var is_same = array1.length == array2.length && array1.every(function(element, index) {
      return element === array2[index];
  });
  return is_same;
}

var newItems = []
var removeItems = []
var plusItems = []
var minusItems = []
var productSelector = false
const productLists = document.querySelectorAll('[data-product-line]')

function searchOne(nameKey, myArray){
  for (var key of Object.keys(myArray)) {
    if(key == nameKey) {
      return myArray;
    }
  }
}

function search(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        var newArr = myArray[i]
        console.log(newArr)
        for (var key of Object.keys(newArr)) {
          if(key == nameKey) {
            return newArr;
          }
        }
    }
}

function removeDups(names) {
  let unique = {};
  names.forEach(function(i) {
    if(!unique[i]) {
      unique[i] = true;
    }
  });
  return Object.keys(unique);
}


setTimeout(()=>{
  if(productLists) {
    productLists.forEach((item, i) => {
        if(!item.classList.contains('hidden')) {
          let itemId = item.dataset.productLine
          let lineIndex = item.dataset.itemLine
              lineIndex--
          var itemArray = {}
          var itemPlus = {}
          var itemMinus = {}
         var carTotalNum = CartJS.cart.items[lineIndex].quantity

          let mainQuantity = CartJS.cart.items[lineIndex].quantity
          let cartminusitem = CartJS.cart.items[lineIndex].quantity
            --cartminusitem


          itemArray[itemId] = 0;
          itemPlus[itemId] = ++mainQuantity

          itemMinus[itemId] = cartminusitem

          if(CartJS.cart.items[lineIndex].properties && CartJS.cart.items[lineIndex].properties['ids']) {
            const productItemsUnique = removeDups(CartJS.cart.items[lineIndex].properties['ids']);
            let titleParentUni = CartJS.cart.items[lineIndex].title
            productSelector = true
            productItemsUnique.forEach((item, i) => {
              if(document.querySelector(`[data-product-line="${item}"][data-parent-title="${titleParentUni}"]`)) {
                // console.log(document.querySelector(`[data-product-line="${item}"][data-parent-title="${titleParentUni}"]`))
                let lineItem = document.querySelector(`[data-product-line="${item}"][data-parent-title="${titleParentUni}"]`).dataset.itemLine
                lineItem--
                let itemQty = CartJS.cart.items[lineItem].quantity

                var plusQty = itemQty / carTotalNum
                var plusQuantity = itemQty + plusQty

                var minusQty = itemQty / carTotalNum
                var minusQuantity = itemQty - minusQty

                itemPlus[item] = plusQuantity
                itemMinus[item] = minusQuantity
                itemArray[item] = 0;
              }
            });
          }

          removeItems[lineIndex] = itemArray
          plusItems[lineIndex] = itemPlus
          minusItems[lineIndex] = itemMinus
        }
    });
  }
}, 1000)


//interval to call multi shipping checker function every 1 second 
var check_multishipping = setInterval(() => {
  checkout_multishipping();
}, 1000);


//function to check the if multishipping available for customer and because its base on
//another plugin, we do it by interval so if the js load slower, still we can show the 
//multi shipping checkbox on checkout. then stop the interval.
function checkout_multishipping(){
  console.log("check for multi shipping...");
  $("#gs__toggle-box").addClass("w-full")
    .addClass("lg:w-4/12")
    .addClass("sm:pl-2")
    .addClass("lg:border-l")
    .addClass("lg:border-grey-200")
    .addClass("float-right")

    $(document).on('click','#gs__toggle-box',function(){
      jQuery("body").addClass("active_multishipping")
       
    })

  if(CartJS.cart.item_count < 2) {
    if(document.querySelector('#gs__toggle-box')) {
      document.querySelector('#gs__toggle-box').remove()
    }
  }

  if(!productSelector) {
    if(document.querySelector('#gs__toggle-box')) {
      document.querySelector('#gs__toggle-box').classList.add('show')
      clearInterval(check_multishipping)
    }
  }
  else {
    if(document.querySelector('#gs__toggle-box')) {
      document.querySelector('#gs__toggle-box').remove()
    }
  }
}



$(document).on('click', '[data-cart-remove-item]', function (e) {
  let itemId = $(this).data('remove-item')
  let index = $(this).parent().data('line')
  var relevantIds = $(this).data('relevant-ids')
  let itemQuantity = $(this).data('total-qty')
  let itemLine = $(this).parent().parent().parent().data('item-line')
  index--

  if(removeItems[index]) {
    //let newUpdate = search(itemId,removeItems)
    $.post('/cart/update.js', {
        updates: removeItems[index]
      },null,'json').done(function() {

        location.reload(true);
      }).fail(function() {
        console.log( "error" );
        location.reload(true);
      });
  }
})

$(document).on('click', '[data-cart-plus]', function (e) {
    var itemId = $(this).parent().data('id')
    let index = $(this).parent().data('line')
    let productLine = $(this).parent().data('line')
    var currentValue = $(this).parent().find('input').val()

    var plusValue = ++currentValue
    index--

    if(plusItems[index]) {
      console.log(plusItems[index])
      $.post('/cart/update.js', {
          updates: plusItems[index]
        },null,'json').done(function() {
          console.log( "success" );
          location.reload(true);
        }).fail(function() {
          console.log( "error" );
        });
    }
    // if(search(itemId,plusItems)) {
    //   let newUpdate = search(itemId,plusItems)
    //   console.log(newUpdate)
    //   // $.post('/cart/update.js', {
    //   //     updates: newUpdate
    //   //   })
    //   //   .done(function() {
    //   //     console.log( "success" );
    //   //     location.reload(true);
    //   //   })
    //   //   .fail(function() {
    //   //     console.log( "error" );
    //   //   });
    // }
})
$(document).on('click', '[data-cart-minus]', function (e) {
  var itemId = $(this).parent().data('id')
  let index = $(this).parent().data('line')
  let productLine = $(this).parent().data('line')
  var currentValue = $(this).parent().find('input').val()
  var minusValue = --currentValue
  index--

  if(minusItems[index]) {
    //let newUpdate = search(itemId,minusItems)
    $.post('/cart/update.js', {
        updates: minusItems[index]
      },null,'json').done(function() {
        console.log( "success" );
        location.reload(true);
      }).fail(function() {
        console.log( "error" );
        location.reload(true);
      });
  }
})


$("#CartNote").on("keyup",function(){
  var len = $(this).val().length; 
  $('#char_count').text(200 - len); 
})












// $(document).on('click', '[data-cart-remove-item]', function (e) {
//   let itemId = $(this).data('cart-remove-item')
//   var relevantIds = $(this).data('cart-relevant-ids')
//   let itemQuantity = $(this).data('carttotal-qty')
//   let itemLine = $(this).parent().parent().parent().data('item-line')
//
//   CartJS.removeItem(itemLine);
//  //$(this).parent().parent().parent().addClass('hidden')
//
//   CartJS.cart.items.forEach(function(item,index) {
//     if(relevantIds) {
//       var ids = relevantIds.split(',')
//       ids.forEach((childItem, i) => {
//         if(childItem == item.id) {
//           let itemQty = parseInt(item.quantity)
//           itemQty--
//           CartJS.updateItemById(childItem, itemQty)
//         }
//       })
//     }
//   })
//
// })
// $(document).on('click', '[data-cart-plus]', function (e) {
//     var itemId = $(this).parent().data('id')
//     let index = $(this).parent().data('line')
//     let productLine = $(this).parent().data('line')
//     var currentValue = $(this).parent().find('input').val()
//     var plusValue = ++currentValue
//     index--
//
//     CartJS.updateItem(productLine, plusValue);
//
//     console.log(index)
//     console.log(CartJS.cart.items[index].properties)
//     if(CartJS.cart.items[index].properties && CartJS.cart.items[index].properties['ids']) {
//       CartJS.cart.items[index].properties['ids'].forEach((item, i) => {
//         let itemLine = $('[data-product-line="'+item+'"]').data('item-line')
//         let itemQty = $('[data-id="'+item+'"]').find('input').val()
//         itemQty++
//
//         CartJS.updateItem(itemLine, itemQty)
//       });
//     }
// })
// $(document).on('click', '[data-cart-minus]', function (e) {
//   var itemId = $(this).parent().data('id')
//   let index = $(this).parent().data('line')
//   let productLine = $(this).parent().data('line')
//   var currentValue = $(this).parent().find('input').val()
//   var minusValue = --currentValue
//   index--
//
//   CartJS.updateItem(productLine, minusValue);
//
//   if(CartJS.cart.items[index].properties && CartJS.cart.items[index].properties['ids']) {
//     CartJS.cart.items[index].properties['ids'].forEach((item, i) => {
//       let itemLine = $('[data-product-line="'+item+'"]').data('item-line')
//       let itemQty = $('[data-id="'+item+'"]').find('input').val()
//       itemQty--
//
//       CartJS.updateItem(itemLine, itemQty)
//     });
//   }
// })
// if(itemPlus) {
//   itemPlus.forEach((item, i) => {
//     item.addEventListener('click', ()=>{
//       let itemId = item.dataset.cartPlus
//       let input = item.parentElement.querySelector('[data-max]')
//       let currentValue = input.value
//       let maxValue = item.parentElement.querySelector('[data-max]').getAttribute('max')
//       let priceOrg = document.querySelector(`[data-price-line="${itemId}"]`).dataset.originalPrice
//       currentValue++
//
//       if(currentValue < maxValue) {
//         CartJS.updateItemById(itemId, currentValue).then(function(){
//           let priceTotalTax = CartJS.cart.total_price * 1.1
//           document.querySelector('[data-cart-subtotal]').innerText = Shopify.formatMoney(priceTotalTax, window.moneyFormat)
//           document.querySelector('[data-cart-count]').innerText = CartJS.cart.item_count
//           input.value = currentValue
//
//           let finalPrice = priceOrg * currentValue
//           document.querySelector(`[data-price-line="${itemId}"]`).innerText = Shopify.formatMoney(finalPrice, window.moneyFormat)
//
//           // CartJS.cart.items.forEach((element, index) => {
//           //   if (element.id == itemId) {
//           //       var price = Shopify.formatMoney(element.line_price, window.moneyFormat)
//           //
//           //   }
//           // })
//         });
//       }
//     })
//   });
// }
//
// if(itemMinus) {
//   itemMinus.forEach((item, i) => {
//     item.addEventListener('click', ()=>{
//       let itemId = item.dataset.cartMinus
//       let input = item.parentElement.querySelector('[data-max]')
//       let currentValue = input.value
//       let maxValue = item.parentElement.querySelector('[data-max]').getAttribute('max')
//       currentValue--
//
//       let priceOrg = document.querySelector(`[data-price-line="${itemId}"]`).dataset.originalPrice
//
//       if(currentValue > 0) {
//         CartJS.updateItemById(itemId, currentValue).then(function(){
//           let priceTotalTax = CartJS.cart.total_price * 1.1
//           document.querySelector('[data-cart-subtotal]').innerText = Shopify.formatMoney(priceTotalTax, window.moneyFormat)
//           document.querySelector('[data-cart-count]').innerText = CartJS.cart.item_count
//           input.value = currentValue
//
//           let finalPrice = priceOrg * currentValue
//           document.querySelector(`[data-price-line="${itemId}"]`).innerText = Shopify.formatMoney(finalPrice, window.moneyFormat)
//           // CartJS.cart.items.forEach((element, index) => {
//           //   if (element.id == itemId) {
//           //       var price = Shopify.formatMoney(element.line_price, window.moneyFormat)
//           //       document.querySelector(`[data-price-line="${itemId}"]`).innerText = price
//           //   }
//           // })
//         });
//       }
//     })
//   });
// }
