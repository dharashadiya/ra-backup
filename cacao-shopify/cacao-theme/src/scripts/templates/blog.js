import "../../styles/templates/blog.scss";

const searchbtn = document.querySelector('[data-blog-search]')
const searchInput = document.querySelector('[data-search-value]')
const articles = document.querySelectorAll('[data-article-id]')
const loadMore = document.querySelector('[data-load]')
const searchInfo = document.querySelector('[data-search-info]')

if(searchbtn) {
  searchbtn.addEventListener('click',()=>{
    let value = searchInput.value
    value = value.replace(/\s+/g, '-').toLowerCase()
    let count = 0
    articles.forEach((item, i) => {
      let itemId = item.dataset.articleId
      item.classList.add('hidden')
      if(itemId.indexOf(value) > -1) {
        item.classList.remove('hidden')
        count++
      }
    });
    if(count > 0) {
      searchInfo.querySelector('p').innerText = `Search result for: ‘${searchInput.value}’`
    }
    else if(count == 0) {
      searchInfo.querySelector('p').innerText = `No Search result for: ‘${searchInput.value}’`
    }
    searchInfo.classList.remove('hidden')
  })
}

if(searchInput) {
  searchInput.addEventListener('keypress', (e)=> {
    if(e.keyCode == 13) {
      let value = searchInput.value
      value = value.replace(/\s+/g, '-').toLowerCase()
      let count = 0
      articles.forEach((item, i) => {
        let itemId = item.dataset.articleId
        item.classList.add('hidden')
        if(itemId.indexOf(value) > -1) {
          item.classList.remove('hidden')
          count++
        }
      });
      if(count > 0) {
        searchInfo.querySelector('p').innerText = `Search result for: ‘${searchInput.value}’`
      }
      else if(count == 0) {
        searchInfo.querySelector('p').innerText = `No Search result for: ‘${searchInput.value}’`
      }
      searchInfo.classList.remove('hidden')
    }
  })
}

if(loadMore) {
  loadMore.addEventListener('click', ()=>{
    loadMore.classList.add('hidden')
    articles.forEach((item, i) => {
      item.classList.remove('hidden')
    });
  })
}
