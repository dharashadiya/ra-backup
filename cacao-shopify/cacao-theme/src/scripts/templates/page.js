import "../../styles/templates/page.scss";
import $ from 'jquery'

const tabs = document.querySelectorAll('[data-tab-id]')
const tabsContent = document.querySelectorAll('.content-tab')

if(tabs && tabs.length > 0) {
  tabs.forEach((item, i) => {
    item.addEventListener('click', ()=> {
      console.log('click')
      let itemId = item.dataset.tabId
      tabsContent.forEach((item, i) => {
        item.classList.remove('active')
      });

      item.classList.add('active')
      document.getElementById(itemId).classList.add('active')
    })
  });
}

const submitGift = document.querySelector('[data-gift-submit]')
const inputGift = document.querySelector('[data-gift-code]')
const error = document.querySelector('[data-gift-error]')
const balance = document.querySelector('[data-balance]')
const balanceWrapper = document.querySelector('[data-balance-wrapper]')
if(submitGift) {
  submitGift.addEventListener('click', ()=> {
    let inputVal = inputGift.value
    var lastFive = inputVal.substr(inputVal.length - 4);
        lastFive = lastFive.toLowerCase()

    $.ajax({
      url: 'https://cacao-dev.myshopify.com/admin/api/2020-07/gift_cards/search.json?query=last_characters:'+lastFive,
      dataType: 'json',
      type: 'get',
      success: function(itemData) {
        console.log('sssss')
        console.log(itemData.gift_cards[0])
        if(itemData.gift_cards[0]) {
          balanceWrapper.classList.remove('hidden')
          balance.innerText = '$'+ itemData.gift_cards[0].balance
        }
        else {
          console.log('error')
          error.classList.remove('hidden')
        }
        //document.querySelector('[data-ballance]').innerText = itemData.gift_cards[0].balance
      }
    })
  })
}

if(inputGift) {
  inputGift.addEventListener('click',()=>{
    error.classList.add('hidden')
  })
}
