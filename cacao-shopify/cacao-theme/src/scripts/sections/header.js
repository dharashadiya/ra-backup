const links = document.querySelectorAll("[data-nav-child]");
const openNav = document.querySelector("[data-open-nav]");
const closeNav = document.querySelector("[data-close-nav]");
const mobileMenu = document.querySelector("#mobile-menu");

const mobilelinks = document.querySelectorAll("[data-mobile-sub]");

if (links) {
    links.forEach(function(link) {
        link.addEventListener("mouseenter", () => {
          document.querySelector('[data-section-id="header"]').classList.add("open-nav");
          link.classList.add('open')
        });
        link.addEventListener("mouseleave", () => {
          document.querySelector('[data-section-id="header"]').classList.remove("open-nav");
          link.classList.remove('open')
        });
    });
}


if(openNav) {
    openNav.addEventListener('click', (e)=> {
        e.preventDefault();
        document.querySelector('[data-section-id="header"]').classList.add('open-mobile-nav')
        mobileMenu.classList.add("open");
    })
}

if (closeNav) {
    closeNav.addEventListener("click", (e) => {
        e.preventDefault();
        document.querySelector('[data-section-id="header"]').classList.remove('open-mobile-nav')
        mobileMenu.classList.remove("open");
        //document.querySelector('[data-section-id="header"]').classList.remove("open-nav")
    });
}
if(mobilelinks) {
  mobilelinks.forEach(item => {
    item.addEventListener('click', ()=>{
      const itemId = item.dataset.mobileSub
      const itemHeight =  document.querySelector(`[data-mobile-child="${itemId}"] div`).offsetHeight
      const itemParent = document.querySelector(`[data-mobile-child="${itemId}"]`).parentElement


      if(itemParent.classList.contains('open')) {
        document.querySelector(`[data-mobile-child="${itemId}"]`).parentElement.classList.remove('open')
        document.querySelector(`[data-mobile-child="${itemId}"]`).style.maxHeight = 0
      }
      else {
        document.querySelector(`[data-mobile-child="${itemId}"]`).parentElement.classList.add('open')
        document.querySelector(`[data-mobile-child="${itemId}"]`).style.maxHeight = itemHeight + 'px'
      }
    })
  });
}
