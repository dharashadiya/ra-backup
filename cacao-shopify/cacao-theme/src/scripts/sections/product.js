/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
 * @namespace product
 */
import $ from 'jquery';
import {getUrlWithVariant, ProductForm} from '@shopify/theme-product-form';
import {formatMoney} from '@shopify/theme-currency';
import {register} from '@shopify/theme-sections';
import {forceFocus} from '@shopify/theme-a11y';
import slick from "slick-carousel";
import '../../styles/components/slick.scss';
import '../../styles/templates/product.scss';

const classes = {
  hide: 'hide',
};

const keyboardKeys = {
  ENTER: 13,
};

const selectors = {
  submitButton: '[data-submit-button]',
  submitButtonText: '[data-submit-button-text]',
  comparePrice: '[data-compare-price]',
  comparePriceText: '[data-compare-text]',
  priceWrapper: '[data-price-wrapper]',
  imageWrapper: '[data-product-image-wrapper]',
  visibleImageWrapper: `[data-product-image-wrapper]:not(.${classes.hide})`,
  imageWrapperById: (id) => `${selectors.imageWrapper}[data-image-id='${id}']`,
  productForm: '[data-product-form]',
  productPrice: '[data-product-price]',
  thumbnail: '[data-product-single-thumbnail]',
  thumbnailById: (id) => `[data-thumbnail-id='${id}']`,
  thumbnailActive: '[data-product-single-thumbnail][aria-current]',
};
register('product', {
  async onLoad() {
    const productFormElement = document.querySelector(selectors.productForm);

    this.product = await this.getProductJson(
      productFormElement.dataset.productHandle,
    );
    this.productForm = new ProductForm(productFormElement, this.product, {
      onOptionChange: this.onFormOptionChange.bind(this),
    });

    this.onThumbnailClick = this.onThumbnailClick.bind(this);
    this.onThumbnailKeyup = this.onThumbnailKeyup.bind(this);

    this.container.addEventListener('click', this.onThumbnailClick);
    this.container.addEventListener('keyup', this.onThumbnailKeyup);

    if(document.querySelector('[data-variant-time]')) {
      document.querySelector('[data-variant-time]').addEventListener('click',()=>{
        document.querySelector('select#Option1').value = 'one time'
      })
    }

    if(document.querySelector('[data-subscribe]')) {
      document.querySelector('[data-subscribe]').addEventListener('click',()=>{
        document.querySelector('select#Option1').value = '2 weeks'
      })
    }

    const images = document.querySelectorAll('[data-image]')
    if (images) {
        images.forEach(function(image) {
          let imageId = image.dataset.image
            image.addEventListener("mouseenter", () => {
              document.querySelector(`[data-image-id="${imageId}"]`).classList.add('hover')
            });
            image.addEventListener("mouseleave", () => {
              document.querySelector(`[data-image-id="${imageId}"]`).classList.remove('hover')
            });
        });
    }

    //  $('[data-slick="product"]').slick({
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   arrows: false,
    //   infinite: true,
    //   dots: false,
    //   speed: 500,
    //   cssEase: 'linear'
    // });

    $('.slider-for').not('.slick-initialized').slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: true,
     infinite: true,
     asNavFor: '.slider-nav',
      nextArrow:
        '<button class="slick-arrow slick-next bg-cover bg-center bg-no-reapet right-0"></button>',
      prevArrow:
        '<button class="slick-arrow slick-prev bg-cover bg-center bg-no-reapet left-0"></button>',
    });

    $('.slider-nav').not('.slick-initialized').slick({
     slidesToShow: 1,
     asNavFor: '.slider-for',
     variableWidth: true,
     dots: false,
     arrows: false,
     centerMode: true,
     infinite: false,
     focusOnSelect: true
    });


    // const imageThumb = document.querySelectorAll('[data-image-id]')
    // if(imageThumb && imageThumb.length > 0) {
    //   imageThumb.forEach((item, i) => {
    //     item.addEventListener('click', ()=> {
    //       document.querySelector('[data-image-id].active').classList.remove('active')
    //       item.classList.add('active')
    //       let imageId = item.dataset.imageId
    //       const slickIndex = document.querySelector(`[data-image-id="${imageId}"]`).parentElement.parentElement.dataset.slickIndex
    //       $('[data-slick="product"]').slick('slickGoTo', slickIndex);
    //     })
    //
    //   })
    // }


  },
  onUnload() {
    console.log('Heresssss')
    this.productForm.destroy();
    this.removeEventListener('click', this.onThumbnailClick);
    this.removeEventListener('keyup', this.onThumbnailKeyup);
  },

  getProductJson(handle) {
    return fetch(`/products/${handle}.js`).then((response) => {
      return response.json();
    });
  },

  onFormOptionChange(event) {
    console.log(event)
    const variant = event.dataset.variant;

    this.renderImages(variant);
    this.renderPrice(variant);
    this.renderComparePrice(variant);
    this.renderDropdownPrice(variant);
    this.renderSubmitButton(variant);

    this.updateBrowserHistory(variant);
  },

  onThumbnailClick(event) {
    const thumbnail = event.target.closest(selectors.thumbnail);

    if (!thumbnail) {
      return;
    }

    event.preventDefault();

    this.renderFeaturedImage(thumbnail.dataset.thumbnailId);
    this.renderActiveThumbnail(thumbnail.dataset.thumbnailId);
  },

  onThumbnailKeyup(event) {
    if (
      event.keyCode !== keyboardKeys.ENTER ||
      !event.target.closest(selectors.thumbnail)
    ) {
      return;
    }

    const visibleFeaturedImageWrapper = this.container.querySelector(
      selectors.visibleImageWrapper,
    );

    forceFocus(visibleFeaturedImageWrapper);
  },

  renderSubmitButton(variant) {
    const submitButton = this.container.querySelector(selectors.submitButton);
    const submitButtonText = this.container.querySelector(
      selectors.submitButtonText,
    );

    if (!variant) {
      submitButton.disabled = true;
      submitButtonText.innerText = `Unavailable`;
    } else if (variant.available) {
      submitButton.disabled = false;
      submitButtonText.innerText = `Add To Cart`;
    } else {
      submitButton.disabled = true;
      submitButtonText.innerText = `sold Out`;
    }
  },

  renderImages(variant) {
    console.log(variant)
    if (!variant || variant.featured_image === null) {
      return;
    }

    this.renderFeaturedImage(variant.featured_image.id);
    this.renderActiveThumbnail(variant.featured_image.id);
  },

  renderPrice(variant) {
    const priceElement = this.container.querySelector(selectors.productPrice);
    const priceWrapperElement = this.container.querySelector(
      selectors.priceWrapper,
    );

    priceWrapperElement.classList.toggle(classes.hide, !variant);

    if (variant) {
      priceElement.innerText = formatMoney(variant.price, theme.moneyFormat);
    }
  },

  renderComparePrice(variant) {
    if (!variant) {
      return;
    }
    const comparePriceElement = this.container.querySelector(
      selectors.comparePrice,
    );
    const compareTextElement = this.container.querySelector(
      selectors.comparePriceText,
    );

    if (!comparePriceElement || !compareTextElement) {
      return;
    }

    if (variant.compare_at_price > variant.price) {
      comparePriceElement.innerText = formatMoney(
        variant.compare_at_price,
        theme.moneyFormat,
      );
      compareTextElement.classList.remove(classes.hide);
      comparePriceElement.classList.remove(classes.hide);
    } else {
      comparePriceElement.innerText = '';
      compareTextElement.classList.add(classes.hide);
      comparePriceElement.classList.add(classes.hide);
    }
  },

//update the gift card hidden variation on option change to fix adding wrong product to cart
  renderDropdownPrice(variant) {
    if (!variant) {
      return;
    }
    console.log("renderprice1")
    $("#ProductSelect-product-gift option:contains("+variant.option1+")").prop('selected', true)
  },

  renderActiveThumbnail(id) {
    const activeThumbnail = this.container.querySelector(
      selectors.thumbnailById(id),
    );
    const inactiveThumbnail = this.container.querySelector(
      selectors.thumbnailActive,
    );

    if (activeThumbnail === inactiveThumbnail) {
      return;
    }

    inactiveThumbnail.removeAttribute('aria-current');
    activeThumbnail.setAttribute('aria-current', true);
  },

  renderFeaturedImage(id) {
    const activeImage = this.container.querySelector(
      selectors.visibleImageWrapper,
    );
    const inactiveImage = this.container.querySelector(
      selectors.imageWrapperById(id),
    );

    activeImage.classList.add(classes.hide);
    inactiveImage.classList.remove(classes.hide);
  },

  updateBrowserHistory(variant) {
    console.log('Heresssss4444')
    const enableHistoryState = this.productForm.element.dataset
      .enableHistoryState;

    if (!variant || enableHistoryState !== 'true') {
      return;
    }

    const url = getUrlWithVariant(window.location.href, variant.id);
    window.history.replaceState({path: url}, '', url);
  },
});
