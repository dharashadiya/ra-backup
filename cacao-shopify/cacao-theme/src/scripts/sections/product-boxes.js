/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
 * @namespace product
 */
import $ from 'jquery';
import {getUrlWithVariant, ProductForm} from '@shopify/theme-product-form';
import {formatMoney} from '@shopify/theme-currency';
import {register} from '@shopify/theme-sections';
import '../../styles/components/slick.scss';
import '../../styles/templates/product.scss';
import Cookie from "js.cookie";

const classes = {
  hide: 'hide',
};

const keyboardKeys = {
  ENTER: 13,
};
var chocolatePropertyArray = []
var chocolateArray = []
var itemsArray = []

register('product-boxes', {
  async onLoad() {
    function getOccurrence(array, value) {
      return array.filter((v) => (v === value)).length;
    }
    function add(arr, name) {
      const { length } = arr;
      const id = length + 1;
      const found = arr.some(el => el.username === name);
      if (!found) arr.push({ id, username: name });
      return arr;
    }

    function search(nameKey, myArray){
        for (var i=0; i < myArray.length; i++) {
            if (myArray[i].id === nameKey) {
                return i;
            }
        }
    }
    function removeByAttr(arr, attr, value){
        var i = arr.length;
        var count = 0
        while(i--){
           if( arr[i] && arr[i].hasOwnProperty(attr) && (arguments.length > 2 && arr[i][attr] === value && count < 1 ) ){
               arr.splice(i,1);
               count++
           }
        }
        return arr;
    }
    // delete all the images in product selector HTMLTableSectionElement(step3)
    function emptyProductHolder(){
      const allSellectedProducts = document.querySelectorAll(`[data-variant-index] [data-index]`)
      if(allSellectedProducts) { 
        allSellectedProducts.forEach((item, i) => { 
            item.querySelector(`.bg-selected`).innerHTML = ""
            item.querySelector(`[data-remove-choc]`).classList.add('hidden')
            item.classList.remove('added') 
        })
      }
    }
    const productBoxes= document.querySelectorAll('[data-value-id]')
    const productImages = document.querySelectorAll('[data-image-id]')
    const typeChoc = document.querySelectorAll('[data-type]')
    const addSelection = document.querySelectorAll('[data-add-choc]')
    const removeSelection = document.querySelectorAll('[data-remove-choc]')
    const submitClick = document.querySelector('[data-submit]')
    const itemCountes = document.querySelectorAll('[data-type-count]')
    const productItems = document.querySelectorAll(`[data-variant-index]`)

    var count = 1
    var itemCount = {};

    function inputSelected(item) {
      // on changing the box size, empty the array of items and remove all the images from stage 3
      itemsArray = [];
      emptyProductHolder();

      chocolatePropertyArray = []
      chocolateArray = []
      let id = item.dataset.valueId
      //for each chocolate/macarons in step 2 remove the default mouse browser effect?????
      if(addSelection && addSelection.length > 0) {
        addSelection.forEach((item, i) => {
          item.classList.remove('pointer-events-none')
        });
      }

      //trigger when switch box size
      if(removeSelection && removeSelection.length > 0) {
        removeSelection.forEach((item, i) => {
          item.classList.add('hidden')
        //  item.parentElement.querySelector('[data-type-count]').parentElement.classList.add('opacity-0')
        });
      }

      if(itemCountes) {
        itemCountes.forEach((item, i) => {
          item.parentElement.classList.add('opacity-0')
          item.innerText = '';
        });
      }

      //update the total max number on chocolate/macaron  selector section - step2(top right) eg 0/16  
      if(document.querySelector('[data-total-select]')) {
        let maxBox = document.querySelector(`[data-variant="${id}"]`).dataset.box
        document.querySelector('[data-total-select]').innerText = maxBox
        //document.querySelector('[data-box-counter]').classList.add('hidden')
      }

      //Set the number of selected  chocolate/macaron to 0  on  selector section - step2(top right), eg 0/10
      if(document.querySelector('[data-current-select]')) {
        document.querySelector('[data-current-select]').innerText = 0
      }

      //hide all the chocolate/macaron selected section (step 3)
      productItems.forEach((item, i) => {
        item.classList.add('hidden')
      });

      // hide the main image on stap1 (chocolate box)
      productImages.forEach((image, i) => {
        image.classList.add('hidden')
      });

      let price = item.dataset.valuePrice
      // update the price field and it's data price on step3
      document.querySelector('[data-main-price]').innerText = Shopify.formatMoney(price, window.moneyFormat)
      document.querySelector('[data-main-price]').setAttribute('data-main-price',item.dataset.valuePrice)

      // show the correct selected box section on step 3(correct size)
      document.querySelector(`[data-variant-index="${id}"]`).classList.remove('hidden')
      // show the correct image of box  on step 1
      document.querySelector(`[data-image-id="${id}"]`).classList.remove('hidden')
    }

    // select the chocolate/macaron box size is url contain hash number
    if(window.location.hash) {
      const url = window.location.hash.replace('#','')
      if(document.querySelector(`[data-size="${url}"]`)) {
        //window.location.href.substr(0, window.location.href.indexOf('#'))
        // select the box size size, then call the inputselected function.
        document.querySelector(`[data-size="${url}"]`).click()
        inputSelected(document.querySelector(`[data-size="${url}"]`))
      }
    }

    //select each chocolate box by click,call the inputselected function
    if(productBoxes) {
      productBoxes.forEach((item, i) => {
        // when item(box) clicked, get the box size 
        item.addEventListener('click', ()=> {

          // if hash selected in url, update the hash noConflict, so after refresh customer see the correct selector 
          let itemSize = item.dataset.size
          if(window.location.hash) {
            window.location.hash = itemSize;
          }
          //window.location.href.substr(`#${itemSize}`, window.location.href.indexOf('#'))
          inputSelected(item)
        })
      });
    }

    if(addSelection && addSelection.length > 0) {
      addSelection.forEach((item, i) => {
        item.addEventListener('click',()=> {
          var value = item.dataset.addChoc
          let productTitle = item.dataset.productTitle
          let image = document.querySelector(`[data-type-value="${value}"]`).dataset.type
          let boxValue = document.querySelector('[name="options[Size]"]:checked').value
          let variantID = document.querySelector('[name="options[Size]"]:checked').dataset.valueId
          let maxBox = document.querySelector(`[data-variant="${boxValue}"]`).dataset.box
          let boxCounter = document.querySelector('[data-box-counter]')
          let boxCurrent = document.querySelector('[data-current-select]')
          let boxTotal = document.querySelector('[data-total-select]')
          let parentTitle = document.querySelector('[name="options[Size]"]:checked').dataset.productTitle

          let typeCount = getOccurrence(chocolateArray,value)

          var maxNum = getOccurrence(chocolateArray,value)
          maxNum++;
          var newVal = maxNum + ' x ' + productTitle

          chocolateArray.push(value)


          let count = 0
          var itemValueCart = {}

          if(search(value, itemsArray) >= 0) {
            let indexId = search(value, itemsArray)
            itemsArray[indexId].quantity = ++itemsArray[indexId].quantity
            itemsArray[indexId].properties.Qty = ++itemsArray[indexId].properties.Qty

            //var itemValueCart = { quantity: count, id: value, properties: { 'Box': productTitle} }
          }
          else {
            itemValueCart = { quantity: 1, id: value, properties: { 'Box': parentTitle, 'Qty': 1 } }
            itemsArray.push(itemValueCart)
          }

          count = chocolateArray.length

          if(chocolateArray.length == maxBox) {
            document.querySelector('[data-submit]').classList.add('hidden')
            addSelection.forEach((item, i) => {
              item.classList.add('pointer-events-none')
            });
          }

          boxCurrent.innerText = count
          boxCounter.classList.remove('hidden')
          //console.log(variantID)

          let imageHtml = `<div class="w-full bg-cover bg-center absolute inset-0 bg-image" style="background-image: url('${image}')"></div>`
          //document.querySelector(`[data-variant-index="${variantID}"] [data-index="${count}"] .bg-selected`).innerHTML = imageHtml
          //document.querySelector(`[data-index="${count}"] div`).innerHTML = imageHtml
          //document.querySelector(`[data-variant-index="${variantID}"] [data-index="${count}"] [data-remove-choc]`).classList.remove('hidden')
          document.querySelector(`[data-type-value="${value}"] [data-type-count]`).innerText = maxNum
          document.querySelector(`[data-type-value="${value}"] [data-type-count]`).parentElement.classList.remove('opacity-0')

          document.querySelector(`[data-variant-index="${variantID}"] [data-index="${count}"] [data-remove-choc]`).setAttribute('data-remove-choc',value)
          document.querySelector(`[data-variant-index="${variantID}"] [data-index="${count}"] [data-remove-choc]`).setAttribute('data-product-title',productTitle)

          const allBox = document.querySelectorAll(`[data-variant-index="${variantID}"] [data-index]`)

          if(allBox) {
            let counter = 0
            allBox.forEach((item, i) => {
              if(!item.classList.contains('added') && counter < 1) {
                item.querySelector(`.bg-selected`).innerHTML = imageHtml
                item.querySelector(`[data-remove-choc]`).classList.remove('hidden')
                item.classList.add('added')
                counter++
              }
            });

          }

          chocolatePropertyArray.map((item,i) => {
            let newItem = item.split(' x ')

            if(newItem[1].includes(productTitle)) {
              chocolatePropertyArray.splice(i, 1);
            }
            else {

            }
          })

          chocolatePropertyArray.push(newVal+", ")

          let imageAdded = document.querySelectorAll(`[data-variant-index="${variantID}"] .added`)

          //console.log(maxBox)
          //console.log(test.length)
          if(maxBox == imageAdded) {
            submitClick.classList.add('hidden')
            addSelection.forEach((item, i) => {
              item.classList.add('pointer-events-none')
            });
          }

          document.querySelector('#selection').value = chocolatePropertyArray
        })
      });
    }

    if(removeSelection && removeSelection.length > 0) {
      removeSelection.forEach((item, i) => {
        item.addEventListener('click',()=> {
          let value = item.dataset.removeChoc
          let productTitle = item.dataset.productTitle
          let boxValue = document.querySelector('[name="options[Size]"]:checked').value
          let maxBox = document.querySelector(`[data-variant="${boxValue}"]`).dataset.box
          let variantID = document.querySelector('[name="options[Size]"]:checked').dataset.valueId
          //console.log(chocolateArray.indexOf(value))
          const index = chocolateArray.indexOf(value);
          submitClick.classList.remove('hidden')

          console.log(search(value, itemsArray));
          if (index > -1) {
            chocolateArray.splice(index, 1);
          }

          // if(search(value, itemsArray)) {
          //
          // }
          // removeByAttr(itemsArray, 'id', value)

          if(search(value, itemsArray) >= 0) {
            let indexId = search(value, itemsArray)
            itemsArray[indexId].quantity = --itemsArray[indexId].quantity
            itemsArray[indexId].properties.Qty = --itemsArray[indexId].properties.Qty

            //var itemValueCart = { quantity: count, id: value, properties: { 'Box': productTitle} }
          }
          else {
            let indexId = search(value, itemsArray)
            itemsArray.splice(indexId, 1);
          //  itemValueCart = { quantity: 1, id: value, properties: { 'Box': productTitle, 'Qty': 1 } }
            //itemsArray.push(itemValueCart)
          }


          console.log(itemsArray)

          let selectedImage = document.querySelectorAll(`[data-index]`)

          item.parentElement.querySelector('.bg-selected').innerHTML= ''
          item.parentElement.querySelector('[data-remove-choc]').classList.add('hidden')
          item.parentElement.classList.remove('added')


          item.setAttribute('data-remove-choc','')
          item.setAttribute('data-product-title','')

          if(chocolateArray.length < maxBox) {
            addSelection.forEach((item, i) => {
              item.classList.remove('pointer-events-none')
            });
          }

          var maxNum = getOccurrence(chocolateArray,value)
          document.querySelector(`[data-type-value="${value}"] [data-type-count]`).innerText = maxNum
          if(maxNum < 1) {
            document.querySelector(`[data-type-value="${value}"] [data-type-count]`).parentElement.classList.add('opacity-0')
          }
          maxNum++;
          var newVal = maxNum + ' x ' + productTitle

          document.querySelector('[data-current-select]').innerText = chocolateArray.length



        chocolatePropertyArray.map((item,i) => {
          let newItem = item.split(' x ')
          if(newItem[1].includes(productTitle)) {
            chocolatePropertyArray.splice(i, 1);
          }
        })

          count--;
          document.querySelector('#selection').value = chocolatePropertyArray
        })
      });
    }

    const steps = document.querySelectorAll('[data-next-id]')
    if(steps) {
      steps.forEach((item, i) => {
        item.addEventListener('click', ()=> {
          let itemId = item.dataset.nextId
          let itemTarget = document.getElementById(itemId).offsetTop + 180
          $("html, body").stop().animate({ scrollTop: itemTarget }, 500, "swing");
        })
      });
    }

    const removeItem = document.querySelectorAll('[data-remove-box]')
    if(removeItem) {
      removeItem.forEach((item, i) => {
        item.addEventListener('click',()=>{
          let index = item.parentElement.querySelector('[data-type-count]').innerText
          index = parseInt(index)
          let value = item.dataset.removeBox.toLowerCase()
          let countIndex = 0
          removeSelection.forEach((remove, i) => {
            let removeValue = remove.dataset.removeChoc
            if(removeValue == value) {
              countIndex++
              if(index == countIndex) {
                remove.click()
              }
            }
          });

        })
      })
    }


    //add the selected item to the cart
    const addCart = document.querySelector('[data-add-cart]')
    if(addCart) {
      //when add to cart clicked
      addCart.addEventListener('click', ()=> {
        let mainId = document.querySelector('[name="options[Size]"]:checked').value
        itemsArray.push({ quantity: 1, id: mainId, properties: { "selection": chocolatePropertyArray, 'ids': chocolateArray } })

        
        $.post('/cart/add.js', {
          items: itemsArray
        })
        .done(function() {
          // when added successfully show the recommended product if its not on mobile and cookie never set for the custorme 
          if(!Cookie.get('productsRecommended') && document.querySelector('#productsRecommended')) {
            if(window.outerWidth > 880) {
              window.theme.Cookie.set('productsRecommended', true, { expires: 10 });
              window.theme.lity(document.querySelector('#productsRecommended'))
            }else{
              //reload the page to refresh all selection
              location.reload();
            }
          }
          else {
            //reload the page to refresh all selection
            location.reload();
          }
        })
        .fail(function() {
          console.log( "error" );
        });

      //   let mainId = document.querySelector('[name="options[Size]"]:checked').value
      //   let boxName = document.querySelector('[name="options[Size]"]:checked').dataset.productTitle
      //   //CartJS.setAttributes(chocolateArray);
      //
      //
      //   if(chocolateArray) {
      //     chocolateArray.forEach((item) => {
      //       CartJS.addItem(item, 1, {
      //         "Box": boxName
      //       });
      //     });
      //   }
      //
      //   CartJS.addItem(mainId, 1, {
      //     "selection": chocolatePropertyArray,
      //     "ids": chocolateArray
      //   })
      //
      //   setTimeout(()=>{
      //     chocolatePropertyArray = []
      //     chocolateArray = []
      //
      //     if(addSelection && addSelection.length > 0) {
      //       addSelection.forEach((item, i) => {
      //         item.classList.remove('pointer-events-none')
      //       });
      //     }
      //     if(removeSelection && removeSelection.length > 0) {
      //       removeSelection.forEach((item, i) => {
      //         item.classList.add('hidden')
      //       //  item.parentElement.querySelector('[data-type-count]').parentElement.classList.add('opacity-0')
      //       });
      //     }
      //     if(itemCountes) {
      //       itemCountes.forEach((item, i) => {
      //         item.parentElement.classList.add('opacity-0')
      //         item.innerText = '';
      //       });
      //     }
      //     if(document.querySelector('[data-current-select]')) {
      //       document.querySelector('[data-current-select]').innerText = 0
      //     }
      //     var selectedImage = document.querySelectorAll(`[data-index]`)
      //     if(selectedImage) {
      //       selectedImage.forEach((image, i) => {
      //         image.classList.remove('added')
      //         if(image.querySelector('.bg-image')) {
      //           image.querySelector('.bg-image').removeAttribute('style')
      //         }
      //       });
      //     }
      //
      //     if(itemCountes) {
      //       itemCountes.forEach((item, i) => {
      //         item.parentElement.classList.add('opacity-0')
      //         item.innerText = '';
      //       });
      //     }
      //   },1000)
      })
    }


    // if(submitClick) {
    //   submitClick.addEventListener('click',()=> {
    //
    //   })
    // }

  },
  onUnload() {},
});
