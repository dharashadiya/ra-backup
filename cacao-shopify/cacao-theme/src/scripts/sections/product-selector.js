/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
 * @namespace product
 */
import $ from 'jquery';
import {getUrlWithVariant, ProductForm} from '@shopify/theme-product-form';
import {formatMoney} from '@shopify/theme-currency';
import {register} from '@shopify/theme-sections';
import {forceFocus} from '@shopify/theme-a11y';
import '../../styles/components/slick.scss';
import '../../styles/templates/product.scss';

const classes = {
  hide: 'hide',
};

const keyboardKeys = {
  ENTER: 13,
};
var chocolatePropertyArray = []
var chocolateArray = []

const selectors = {
  submitButton: '[data-submit-button]',
  submitButtonText: '[data-submit-button-text]',
  comparePrice: '[data-compare-price]',
  comparePriceText: '[data-compare-text]',
  priceWrapper: '[data-price-wrapper]',
  imageWrapper: '[data-product-image-wrapper]',
  visibleImageWrapper: `[data-product-image-wrapper]:not(.${classes.hide})`,
  imageWrapperById: (id) => `${selectors.imageWrapper}[data-image-id='${id}']`,
  productForm: '[data-product-form]',
  productPrice: '[data-product-price]',
  thumbnail: '[data-product-single-thumbnail]',
  thumbnailById: (id) => `[data-thumbnail-id='${id}']`,
  thumbnailActive: '[data-product-single-thumbnail][aria-current]',
};


//show the scroll icon on the selector page
$( ".box-overflow" ).scroll(function() {
  let scroll_position= $(".box-overflow").scrollTop();
  console.log(scroll_position)
  if(scroll_position > 90){
    $(".scroll-icon").addClass("hide");
  }else{
    $(".scroll-icon").removeClass("hide"); 
  }
 });



register('product-selector', {
  async onLoad() {
    const productFormElement = document.querySelector(selectors.productForm);

    this.product = await this.getProductJson(
      productFormElement.dataset.productHandle,
    );
    this.productForm = new ProductForm(productFormElement, this.product, {
      onOptionChange: this.onFormOptionChange.bind(this),
    });
    function getOccurrence(array, value) {
      return array.filter((v) => (v === value)).length;
    }

    const steps = document.querySelectorAll('[data-next-id]')
    if(steps) {
      steps.forEach((item, i) => {
        item.addEventListener('click', ()=> {
          let itemId = item.dataset.nextId
          let itemTarget = document.getElementById(itemId).offsetTop + 180
          $("html, body").stop().animate({ scrollTop: itemTarget }, 500, "swing");
        })
      });
    }

    const typeChoc = document.querySelectorAll('[data-type]')
    var count = 1
    var itemCount = {};
    // if(typeChoc && typeChoc.length > 0) {
    //   typeChoc.forEach((item, i) => {
    //     item.addEventListener('click', ()=>{
    //       let value = item.dataset.typeValue
    //       let image = item.dataset.type
    //       chocolateArray.push(value)
    //       var typeCount = getOccurrence(chocolateArray,value)
    //       const imageHtml = `<div class="w-full bg-cover bg-center absolute inset-0" style="background-image: url('${image}')"></div>`
    //       document.querySelector(`[data-index="${count}"] div`).innerHTML = imageHtml
    //
    //       item.querySelector('[data-type-count]').innerText = typeCount
    //       item.querySelector('[data-type-count]').parentElement.classList.remove('opacity-0')
    //       count++;
    //     })
    //   });
    // }

    const addSelection = document.querySelectorAll('[data-add-choc]')
    const removeSelection = document.querySelectorAll('[data-remove-choc]')
    const submitClick = document.querySelector('[data-submit]')

    if(addSelection && addSelection.length > 0) {
      addSelection.forEach((item, i) => {
        item.addEventListener('click',()=> {
          var value = item.dataset.addChoc
          let image = document.querySelector(`[data-type-value="${value}"]`).dataset.type
          let boxValue = document.querySelector('[name="options[Size]"]:checked').value
          let variantID = document.querySelector('[name="options[Size]"]:checked').dataset.valueId
          let maxBox = document.querySelector(`[data-variant="${boxValue}"]`).dataset.box
          let boxCounter = document.querySelector('[data-box-counter]')
          let boxCurrent = document.querySelector('[data-current-select]')
          let boxTotal = document.querySelector('[data-total-select]')

          let typeCount = getOccurrence(chocolateArray,value)

          var maxNum = getOccurrence(chocolateArray,value)
          maxNum++;
          var newVal = maxNum + ' x ' + value
          chocolateArray.push(value)
          count = chocolateArray.length
          if(chocolateArray.length == maxBox) {
            document.querySelector('[data-submit]').classList.add('hidden')
            addSelection.forEach((item, i) => {
              item.classList.add('pointer-events-none')
            });
          }

          boxCurrent.innerText = count
          boxCounter.classList.remove('hidden')
          //console.log(variantID)
          let imageHtml = `<div class="w-full bg-cover bg-center absolute inset-0 bg-image" style="background-image: url('${image}')"></div>`
          //document.querySelector(`[data-variant-index="${variantID}"] [data-index="${count}"] .bg-selected`).innerHTML = imageHtml
          //document.querySelector(`[data-index="${count}"] div`).innerHTML = imageHtml
          //document.querySelector(`[data-variant-index="${variantID}"] [data-index="${count}"] [data-remove-choc]`).classList.remove('hidden')
          document.querySelector(`[data-type-value="${value}"] [data-type-count]`).innerText = maxNum
          document.querySelector(`[data-type-value="${value}"] [data-type-count]`).parentElement.classList.remove('opacity-0')
          document.querySelector(`[data-variant-index="${variantID}"] [data-index="${count}"] [data-remove-choc]`).setAttribute('data-remove-choc',value)

          const allBox = document.querySelectorAll(`[data-variant-index="${variantID}"] [data-index]`)
          if(allBox) {
            let counter = 0
            allBox.forEach((item, i) => {
              if(!item.classList.contains('added') && counter < 1) {
                item.querySelector(`.bg-selected`).innerHTML = imageHtml
                item.querySelector(`[data-remove-choc]`).classList.remove('hidden')
                item.classList.add('added')
                counter++
              }
            });

          }

          chocolatePropertyArray.map((item,i) => {
            let newItem = item.split(' x ')

            if(newItem[1].includes(value)) {
              chocolatePropertyArray.splice(i, 1);
            }
            else {

            }
          })

          chocolatePropertyArray.push(newVal)
          let imageAdded = document.querySelectorAll(`[data-variant-index="${variantID}"] .added`)

          //console.log(maxBox)
          //console.log(test.length)
          if(maxBox == imageAdded) {
            submitClick.classList.add('hidden')
            addSelection.forEach((item, i) => {
              item.classList.add('pointer-events-none')
            });

          }
          document.querySelector('#selection').value = chocolatePropertyArray
        })
      });
    }

    if(removeSelection && removeSelection.length > 0) {
      removeSelection.forEach((item, i) => {
        item.addEventListener('click',()=> {
          let value = item.dataset.removeChoc
          let boxValue = document.querySelector('[name="options[Size]"]:checked').value
          let maxBox = document.querySelector(`[data-variant="${boxValue}"]`).dataset.box
          let variantID = document.querySelector('[name="options[Size]"]:checked').dataset.valueId
          //console.log(chocolateArray.indexOf(value))
          const index = chocolateArray.indexOf(value);
          submitClick.classList.remove('hidden')
          if (index > -1) {
            chocolateArray.splice(index, 1);
          }

          let selectedImage = document.querySelectorAll(`[data-index]`)

          // selectedImage.forEach((item, i) => {
          //   item.querySelector('div').innerHTML = ''
          // });

            item.parentElement.querySelector('.bg-selected').innerHTML= ''
            item.parentElement.querySelector('[data-remove-choc]').classList.add('hidden')
            item.parentElement.classList.remove('added')

          // removeSelection.forEach((item, i) => {
          //   item.classList.add('hidden')
          // });


          // chocolateArray.forEach((item, i) => {
          //   let image = document.querySelector(`[data-type-value="${item}"]`).dataset.type
          //   let index = i + 1
          //   let imageHtml = `<div class="w-full bg-cover bg-center absolute inset-0 bg-image" style="background-image: url('${image}')"></div>`
          //   //document.querySelector(`[data-index="${index}"] div`).innerHTML = imageHtml
          //     document.querySelector(`[data-variant-index="${variantID}"] [data-index="${index}"] .bg-selected`).innerHTML = imageHtml
          //   document.querySelector(`[data-remove-choc="${item}"]`).classList.remove('hidden')
          // });



          if(chocolateArray.length < maxBox) {
            addSelection.forEach((item, i) => {
              item.classList.remove('pointer-events-none')
            });
          }

          var maxNum = getOccurrence(chocolateArray,value)
          //console.log(maxNum)
          document.querySelector(`[data-type-value="${value}"] [data-type-count]`).innerText = maxNum
          if(maxNum < 1) {
            document.querySelector(`[data-type-value="${value}"] [data-type-count]`).parentElement.classList.add('opacity-0')
          }
          maxNum++;
          var newVal = maxNum + ' x ' + value


          document.querySelector('[data-current-select]').innerText = chocolateArray.length
                    //console.log(chocolatePropertyArray.length)
        chocolatePropertyArray.map((item,i) => {
          let newItem = item.split(' x ')

          if(newItem[1].includes(value)) {
            chocolatePropertyArray.splice(i, 1);
          }
          else {

          }
        })

        chocolatePropertyArray.push(newVal)
          count--;
          document.querySelector('#selection').value = chocolatePropertyArray
        })
      });
    }

    const removeItem = document.querySelectorAll('[data-remove-box]')
    if(removeItem) {
      removeItem.forEach((item, i) => {
        item.addEventListener('click',()=>{
          let index = item.parentElement.querySelector('[data-type-count]').innerText
          index = parseInt(index)
          let value = item.dataset.removeBox.toLowerCase()
          let countIndex = 0
          removeSelection.forEach((remove, i) => {
            let removeValue = remove.dataset.removeChoc
            if(removeValue == value) {
              countIndex++
              if(index == countIndex) {
                remove.click()
              }
            }
          });

        })
      })
    }

    // if(submitClick) {
    //   submitClick.addEventListener('click',()=> {
    //
    //   })
    // }

  },
  onUnload() {
    this.productForm.destroy();
    this.removeEventListener('click', this.onThumbnailClick);
    this.removeEventListener('keyup', this.onThumbnailKeyup);
  },

  getProductJson(handle) {
    return fetch(`/products/${handle}.js`).then((response) => {
      return response.json();
    });
  },

  onFormOptionChange(event) {
    const variant = event.dataset.variant;
    chocolatePropertyArray = []
    chocolateArray = []
    this.renderImages(variant);
    this.renderComparePrice(variant);
    this.renderSubmitButton(variant);
    this.updateBrowserHistory(variant);
    document.getElementById('ProductSelect-product-selector').value = variant.id

    const addSelection = document.querySelectorAll('[data-add-choc]')
    const removeSelection = document.querySelectorAll('[data-remove-choc]')
    const itemCountes = document.querySelectorAll('[data-type-count]')

    if(addSelection && addSelection.length > 0) {
      addSelection.forEach((item, i) => {
        item.classList.remove('pointer-events-none')
      });
    }
    if(removeSelection && removeSelection.length > 0) {
      removeSelection.forEach((item, i) => {
        item.classList.add('hidden')
      //  item.parentElement.querySelector('[data-type-count]').parentElement.classList.add('opacity-0')
      });
    }
    document.getElementById('Quantity').value = 1

    if(document.querySelector(`[data-value-id="${variant.id}"]`)) {
      var max = document.querySelector(`[data-value-id="${variant.id}"][data-value-max]`).dataset.valueMax
      document.getElementById('Quantity').setAttribute('data-max',max)
    }

    if(itemCountes) {
      itemCountes.forEach((item, i) => {
        item.parentElement.classList.add('opacity-0')
      });
    }


    if(document.querySelector('[data-total-select]')) {
      let boxValue = document.querySelector('[name="options[Size]"]:checked').value
      let maxBox = document.querySelector(`[data-variant="${boxValue}"]`).dataset.box
      document.querySelector('[data-total-select]').innerText = maxBox
      //document.querySelector('[data-box-counter]').classList.add('hidden')
    }

    if(document.querySelector('[data-current-select]')) {
      document.querySelector('[data-current-select]').innerText = 0
    }
  },

  onThumbnailClick(event) {
    const thumbnail = event.target.closest(selectors.thumbnail);

    if (!thumbnail) {
      return;
    }

    event.preventDefault();

    this.renderFeaturedImage(thumbnail.dataset.thumbnailId);
    this.renderActiveThumbnail(thumbnail.dataset.thumbnailId);
  },

  onThumbnailKeyup(event) {
    if (
      event.keyCode !== keyboardKeys.ENTER ||
      !event.target.closest(selectors.thumbnail)
    ) {
      return;
    }

    const visibleFeaturedImageWrapper = this.container.querySelector(
      selectors.visibleImageWrapper,
    );

    forceFocus(visibleFeaturedImageWrapper);
  },

  renderSubmitButton(variant) {
    const submitButton = this.container.querySelector(selectors.submitButton);
    const submitButtonText = this.container.querySelector(
      selectors.submitButtonText,
    );

    if (!variant) {
      submitButton.disabled = true;
      submitButtonText.innerText = theme.strings.unavailable;
    } else if (variant.available) {
      submitButton.disabled = false;
      submitButtonText.innerText = 'Add To Cart';
    } else {
      submitButton.disabled = true;
      submitButtonText.innerText = 'Sold Out';
    }
  },

  renderImages(variant) {
    const variantImages = document.querySelectorAll('[data-image-id]')
    var maxBox = document.querySelector(`[data-image-id="${variant.id}"]`).dataset.box
    variantImages.forEach((item, i) => {
      item.classList.add('hidden')
    });

    document.querySelector(`[data-image-id="${variant.id}"]`).classList.remove('hidden')
    maxBox = parseInt(maxBox)
    // let itemBoxs = document.querySelectorAll('[data-index]')
    //
    // itemBoxs.forEach((item, i) => {
    //   item.classList.add('hidden')
    //   item.querySelector('div').innerHTML = ''
    //   if(i < maxBox) {
    //     item.classList.remove('hidden')
    //   }
    // });

    let bgSelect = document.querySelectorAll('.bg-selected')
    if(bgSelect) {
      bgSelect.forEach((item, i) => {
        item.innerHTML = ''
      });
    }

    let boxes = document.querySelectorAll('[data-variant-index]')
    if(boxes) {
      boxes.forEach((item, i) => {
        let boxId = item.dataset.variantIndex
        item.classList.add('hidden')
        if(variant.id == boxId) {
          item.classList.remove('hidden')
        }
      });

    }




    // if (!variant || variant.featured_image === null) {
    //   return;
    // }
    //
    // this.renderFeaturedImage(variant.featured_image.id);
    // this.renderActiveThumbnail(variant.featured_image.id);
  },

  renderComparePrice(variant) {
    if (!variant) {
      return;
    }

    document.querySelector('[data-product-price]').innerText = formatMoney(variant.price,theme.moneyFormat);


    // const comparePriceElement = this.container.querySelector(
    //   selectors.comparePrice,
    // );
    // const compareTextElement = this.container.querySelector(
    //   selectors.comparePriceText,
    // );
    //
    // if (!comparePriceElement || !compareTextElement) {
    //   return;
    // }

    // if (variant.compare_at_price > variant.price) {
    //   comparePriceElement.innerText = formatMoney(
    //     variant.compare_at_price,
    //     theme.moneyFormat,
    //   );
    //   compareTextElement.classList.remove(classes.hide);
    //   comparePriceElement.classList.remove(classes.hide);
    // } else {
    //   comparePriceElement.innerText = '';
    //   compareTextElement.classList.add(classes.hide);
    //   comparePriceElement.classList.add(classes.hide);
    // }
  },

  renderActiveThumbnail(id) {
    const activeThumbnail = this.container.querySelector(
      selectors.thumbnailById(id),
    );
    const inactiveThumbnail = this.container.querySelector(
      selectors.thumbnailActive,
    );

    if (activeThumbnail === inactiveThumbnail) {
      return;
    }

    inactiveThumbnail.removeAttribute('aria-current');
    activeThumbnail.setAttribute('aria-current', true);
  },

  renderFeaturedImage(id) {
    const activeImage = this.container.querySelector(
      selectors.visibleImageWrapper,
    );
    const inactiveImage = this.container.querySelector(
      selectors.imageWrapperById(id),
    );

    activeImage.classList.add(classes.hide);
    inactiveImage.classList.remove(classes.hide);
  },

  updateBrowserHistory(variant) {
    const enableHistoryState = this.productForm.element.dataset
      .enableHistoryState;

    if (!variant || enableHistoryState !== 'true') {
      return;
    }

    const url = getUrlWithVariant(window.location.href, variant.id);
    window.history.replaceState({path: url}, '', url);
  },
});
