import $ from 'jquery'
import '../../styles/sections/video.scss';

$('[data-video-play]').on('click', function(){
  var video = $(this).parent().find('video')
  $(this).addClass('hidden')
  video.get(0).play()
})
