window.jQuery = $;
import 'jquery-parallax.js';

// $('.wide-parallax').parallax({
//   mirrorContainer: '.wide-parallax-mirror'
// });

$('.desktop-parallax').parallax({
  mirrorContainer: '.desktop-parallax-mirror',
  naturalHeight: 'auto'
});

$(window).trigger('resize').trigger('scroll');
