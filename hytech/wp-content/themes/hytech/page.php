<?php get_header(); ?>

<div class="c-default">
	<main id="Main" class="c-main-content o-main" role="main">
	<?php $banner = get_field('banner') ?>
    <div class="c-banner"  style="background-image: url(<?php echo $banner['image'] ?>)">
        <div class="o-wrapper">
            <div class="c-banner__content">
                <h1><?php echo $banner['title'] ?></h1>
                <p><?php echo $banner['short_copy'] ?></p>
            </div>
        </div>
    </div>
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="o-wrapper">
			<div class="c-cms-content">
				<?php the_content(); ?>
			</div>
		</div>
		</article>
	<?php endwhile; ?>
	</main>
</div>

<?php get_footer(); ?>