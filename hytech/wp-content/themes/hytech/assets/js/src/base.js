/* global site */
// [1] Import functions here
import {
	isMobile,
	openLinkNewTab,
	superfishMenu,
	slider,
	menuToggle,
	toggleTab,
	cloudinaryImageCDN
} from "./template-functions";
(function($) {
	window.site || (window.site = {});
	$.extend(
		site,
		{
			initVars: () => {
				// Global variables -> use as site.VariableName in other files
				site.body = $("body");
				site.root = $("html");
				site.header = $("#Top");
				site.main = $("#Main");
				site.side = $("#Secondary");
				site.siteurl = $("#Siteurl").attr("content");
				site.themeurl = $("#Themeurl").attr("content");
				return (site.ajaxurl = $("#Ajaxurl").attr("content"));
			},
			// [3] Call functions within either of these
			onReady: () => {
				site.initVars();
				site.menuToggle();
				site.slider();
				site.toggleTab();
				// site.cloudinaryImageCDN()
				return site.isMobile();
			},
			onLoad: () => {
				// return site.slider()
			}
		},
		{
			// [2] Register functions here
			isMobile: isMobile,
			openLinkNewTab: openLinkNewTab,
			superfishMenu: superfishMenu,
			slider: slider,
			menuToggle: menuToggle,
			toggleTab: toggleTab,
			cloudinaryImageCDN: cloudinaryImageCDN
		}
	);
	$(() => {
		return site.onReady();
	});
	return $(window).on("load", () => {
		return site.onLoad();
	});
})(jQuery);
