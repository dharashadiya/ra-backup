<?php
$image_gallery = get_field('image_gallery');

if ($image_gallery) : ?>
<div class="c-image-gallery">
	<?php foreach ($image_gallery as $image) {
		
		// Single Landscape Image
		if ($image['acf_fc_layout'] == "single_landscape_image") { ?>
			<div class="c-image-gallery--landscape c-image-gallery--all o-layout">
				<div class="o-layout__item u-1/1@mobile">
					<img src="<?php echo $image['image']['url']; ?>" alt="<?php echo $image['image']['alt']; ?>">
				</div>
			</div>
		<?php }

		// 2 Images Module
		else if ($image['acf_fc_layout'] == "2_images") { ?>
			<div class="c-image-gallery--2images c-image-gallery--all o-layout">
				<div class="o-layout__item u-1/2@tablet u-1/1@mobile c-image-gallery--2images_left">
					<img src="<?php echo $image['image_1']['url']; ?>" alt="<?php echo $image['image_1']['alt']; ?>">
				</div><div class="o-layout__item u-1/2@tablet u-1/1@mobile c-image-gallery--2images_right">
					<img src="<?php echo $image['image_2']['url']; ?>" alt="<?php echo $image['image_2']['alt']; ?>">
				</div>
			</div>
		<?php }

		// 3 Images Module
		else if ($image['acf_fc_layout'] == "3_images") { ?>
			<div class="c-image-gallery--3-images c-image-gallery--all o-layout">
				<div class="o-layout__item u-1/3@tablet u-1/1@mobile c-image-gallery--3-images_first">
					<img src="<?php echo $image['image_1']['url']; ?>" alt="<?php echo $image['image_1']['alt']; ?>">
				</div><div class="o-layout__item u-1/3@tablet u-1/1@mobile c-image-gallery--3-images_second">
					<img src="<?php echo $image['image_2']['url']; ?>" alt="<?php echo $image['image_2']['alt']; ?>">
				</div><div class="o-layout__item u-1/3@tablet u-1/1@mobile c-image-gallery--3-images_third">
					<img src="<?php echo $image['image_3']['url']; ?>" alt="<?php echo $image['image_3']['alt']; ?>">
				</div>
			</div>
		<?php }

		// 1 Image on Left, 2 Images on Right Module
		else if ($image['acf_fc_layout'] == "1_on_left_2_on_right_images") { ?>
			<div class="c-image-gallery--1left-2right c-image-gallery--all o-layout">
				<div class="o-layout__item u-1/2@tablet u-1/1@mobile c-image-gallery--1left-2right_left">
					<img src="<?php echo $image['left_image']['url']; ?>" alt="<?php echo $image['left_image']['alt']; ?>">
				</div><div class="o-layout__item u-1/2@tablet u-1/1@mobile c-image-gallery--1left-2right_right">
					<img src="<?php echo $image['right_image_1']['url']; ?>" alt="<?php echo $image['right_image_1']['alt']; ?>">
					<img src="<?php echo $image['right_image_2']['url']; ?>" alt="<?php echo $image['right_image_2']['alt']; ?>">
				</div>
			</div>
		<?php }

		// 2 Images on Left, 1 Image on Right Module
		else if ($image['acf_fc_layout'] == "2_on_left_1_on_right_images") { ?>
			<div class="c-image-gallery--2left-1right c-image-gallery--all o-layout">
				<div class="o-layout__item u-1/2@tablet u-1/1@mobile c-image-gallery--2left-1right_left">
					<img src="<?php echo $image['left_image_1']['url']; ?>" alt="<?php echo $image['left_image_1']['alt']; ?>">
					<img src="<?php echo $image['left_image_2']['url']; ?>" alt="<?php echo $image['left_image_2']['alt']; ?>">
				</div><div class="o-layout__item u-1/2@tablet u-1/1@mobile c-image-gallery--2left-1right_right">
					<img src="<?php echo $image['right_image']['url']; ?>" alt="<?php echo $image['right_image_1']['alt']; ?>">
				</div>
			</div>
		<?php }

		else {
			echo "<pre>";
			print_r($image);
			echo "</pre>";
		}
	} ?>
</div>
<?php endif; ?>