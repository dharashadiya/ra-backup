<?php
$flexible_content = get_field('flexible_content');

if ($flexible_content) : ?>
<div class="c-flex-content">
	<?php foreach ($flexible_content as $key => $content) {
		
		// Content Module
		if ($content['acf_fc_layout'] == "content_module") { ?>
			<div class="c-flex-content--content c-flex-content--all ">
				<?php echo $content['content']; ?>
			</div>
		<?php }

		// 2 Column Content Module
		else if ($content['acf_fc_layout'] == "2_column_content_module") { ?>
			<div class="c-flex-content--2-column-content c-flex-content--all ">
				<?php echo $content['content']; ?>
			</div>
		<?php } 
		
		// Repeating Blocks Module
		else if ($content['acf_fc_layout'] == "repeating_blocks_module") { ?>
			<div class="c-flex-content--repeater c-flex-content--all ">
				<?php foreach ($content['repeating_content'] as $key => $block) { ?>
					<div class="c-flex-content--repeater_block">
						<h2><?php echo $block['title']; ?></h2>
						<div><?php echo $block['content']; ?></div>
						<img src="<?php echo $block['image']['url']; ?>" alt="<?php echo $block['image']['alt']; ?>">
					</div>
				<?php } ?>
			</div>
		<?php }

		// Image Left, Content Right Module
		else if ($content['acf_fc_layout'] == "image_left_content_right_module") { ?>
			<div class="c-flex-content--image-content c-flex-content--all ">
				<div class="o-layout">
					<div class="o-layout__item u-1/2@tablet u-1/1@mobile c-flex-content--image-content_left">
						<img src="<?php echo $content['image']['url']; ?>" alt="<?php echo $content['image']['alt']; ?>">
					</div><div class="o-layout__item u-1/2@tablet u-1/1@mobile c-flex-content--image-content_right">
						<?php echo $content['content']; ?>
					</div>
				</div>
			</div>
		<?php }

		// Content Left, Image Right Module
		else if ($content['acf_fc_layout'] == "content_left_image_right_module") { ?>
			<div class="c-flex-content--content-image c-flex-content--all ">
				<div class="o-layout">
					<div class="o-layout__item u-1/2@tablet u-1/1@mobile c-flex-content--content-image_left">
						<?php echo $content['content']; ?>
					</div><div class="o-layout__item u-1/2@tablet u-1/1@mobile c-flex-content--content-image_right">
						<img src="<?php echo $content['image']['url']; ?>" alt="<?php echo $content['image']['alt']; ?>">
					</div>
				</div>
			</div>
		<?php }
	} ?>
</div>
<?php endif; ?>