<?php $testimonials = get_field("testimonials", 'options');
if (is_page('about') || is_front_page() || is_page_template('tmpl-special-offer.php')) { ?>
	<div class="c-testimonials">
		<div class="o-wrapper">
			<div class="o-layout">
				<div class="o-layout__item u-2/5@tablet">
					<div class="c-testimonials__header">
						<h2>Testimonials from our customers</h2>
						<?php svgicon('arrow', '0 0 140 52') ?>
					</div>
				</div>
				<div class="o-layout__item u-3/5@tablet">
					<div class="c-slider">
						<?php foreach ($testimonials as $testimony) { ?>
							<div class="c-slide">
								<p><?php echo $testimony['testimony'] ?></p>
								<p class="c-testimonials__name"><?php echo $testimony['name'] ?></p>
							</div>
						<?php }	?>
						<div class="c-slider__pager cycle-pager"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

</div><!-- .c-content -->
<footer class="c-footer">
	<div class="c-footer__top">
		<div class="o-wrapper">
			<div class="o-layout">
				<div class="o-layout__item u-1/4@tabletWide u-1/2@mobileLandscape">
					<?php svgicon('logo', '0 0 394 74', 'c-footer__logo'); ?>
					<?php echo do_shortcode('[social-links]'); ?>
				</div>
				<div class="o-layout__item u-1/4@tabletWide u-1/2@mobileLandscape">
					<h4>Quick Links</h4>
					<?php $links = get_field('quick_links', 'options') ?>
					<div class="c-footer__links">
						<?php foreach ($links as $link) : ?>
							<a href="<?php echo $link['link_url'] ?>"><?php echo $link['link_text'] ?></a>
						<?php endforeach; ?>
					</div>		
				</div>
				<div class="o-layout__item u-1/4@tabletWide u-1/2@mobileLandscape">
					<h4>Contact Us</h4>	
					<?php $contact = get_field('contact', 'options');
					if($contact) :?>
					<div class="c-footer__links">
						<a href="mailto:<?php echo $contact['email'] ?>"><?php echo $contact['email'] ?></a>
						<a href="tel:<?php echo $contact['phone'] ?>"><?php echo $contact['phone'] ?></a>
						<a href="https://www.google.com/maps/place/31+Coventry+St,+Southbank+VIC+3006/@-37.8295057,144.9676426,17z/data=!3m1!4b1!4m5!3m4!1s0x6ad642a8d0aa3239:0x1658c91c63aee7d1!8m2!3d-37.8295057!4d144.9698313?q=31+Coventry+St,Southbank+VIC+3006&um=1&ie=UTF-8&sa=X&ved=2ahUKEwjz_dz1k-nnAhWm7XMBHWY3BKkQ_AUoAXoECA0QAw&shorturl=1" class="address"><?php echo $contact['address'] ?></a>
					</div>
					<?php endif; ?>
				</div>
				<div class="o-layout__item u-1/4@tabletWide u-1/2@mobileLandscape">
					<a href="<?php echo SITE ?>/#quote-form" class="o-btn c-footer__btn">Request quote</a>
				</div>
				<div class="o-layout__item u-1/4@tabletWide"></div>
				<div class="o-layout__item u-1/4 u-1/2@mobileLandscape  u-1/4@tabletWide">
					<img style="max-width:200px;" src="<?php echo STYLESHEET_URL; ?>/assets/img/approved-solar.png" alt="Approved Solar">
				</div>
				<div class="o-layout__item u-1/4 u-1/2@mobileLandscape">
					<img style="max-width:200px;" src="<?php echo STYLESHEET_URL; ?>/assets/img/clean-energy.png" alt="Clean Energy">
				</div>
			</div>
		</div>
	</div>
	<div class="c-footer__copy">
		<div class="o-wrapper">
			<p>&copy;&nbsp;Copyright&nbsp;HyTech&nbsp;Solar&nbsp;<?php echo date("Y"); ?>. <a href="#">Terms&nbsp;&amp;&nbsp;Conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="/privacy/">Privacy</a> &nbsp;&nbsp;|&nbsp;&nbsp;Built&nbsp;by&nbsp;<a href="https://rockagency.com.au/">Rock&nbsp;Agency</a>.</p>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
