<?php get_header(); ?>
<main id="Main" class="c-main-content o-main" role="main">
	<div class="c-home">
		<?php $banner = get_field('banner') ?>
		<div class="c-home__banner" style="background-image: url(<?php echo $banner['image'] ?>)" >
			<div class="o-wrapper">
				<div class="o-layout">
					<div class="o-layout__item u-1/2@tabletWide">
						<div class="c-home__banner-content">
							<h1><?php echo $banner['title'] ?></h1>
							<p><?php echo $banner['short_copy'] ?></p>
							<img src="<?php echo STYLESHEET_URL; ?>/assets/img/icon-arrow.png" alt="Icon" class="c-home__banner-arrow">
						</div>
					</div>
					<div class="o-layout__item u-1/2@tabletWide">
						<div class="c-home__banner-quote" id="quote-form">
							<?php $quotes = get_field('quote_form') ?>
							<h2><?php echo $quotes['title'] ?></h2>
							<?php echo do_shortcode($quotes['form_shotcode']) ?>
						</div>
					</div>
				</div>
				<div class="c-home__banner-bottom">
					<img src="<?php echo STYLESHEET_URL; ?>/assets/img/c-clean-energy.png" alt="Approved Solar">
					<img src="<?php echo STYLESHEET_URL; ?>/assets/img/c-approved-solar.png" alt="Clean Energy">
				</div>
			</div>
		</div>
		<div class="c-home__content">
			<div class="o-wrapper">
				<?php $content = get_field('content'); ?>
				<?php foreach ($content as $content) :?>
					<div class="c-home__content-list">
						<div class="c-home__content-list-img">
							<div class="c-bgimg" style="background-image: url(<?php echo $content['image'] ?>)"></div>
						</div>
						<div class="c-home__content-list-text">
							<div class="c-contents">
								<div class="c-home__content-list-copy">
									<?php echo $content['copy'] ?>
								</div>
								<?php if ($content['link']) : ?>
									<a href="<?php echo $content['link']?>" class="o-btn o-btn--transparent">Tell me more</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<?php $bottom_content = get_field('bottom_content'); if ($bottom_content): ?>
			<div class="c-home__bottom-content">
				<div class="o-wrapper">
					<?php echo $bottom_content; ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</main>



<?php get_footer(); ?>


