<?php 
/*  Template Name: Panels and Inverters Page */
?>

<?php get_header(); ?>
<div class="c-solar">
    <?php $banner = get_field('banner') ?>
    <div class="c-banner"  style="background-image: url(<?php echo $banner['image'] ?>)">
        <div class="o-wrapper">
            <div class="c-banner__content">
                <h1><?php echo $banner['title'] ?></h1>
                <p><?php echo $banner['short_copy'] ?></p>
            </div>		
        </div>
    </div>
    <div class="c-solar-sections">
        <div class="o-wrapper">
            <?php $sections = get_field('brand_sections');
            foreach ($sections as $section) : ?>
                <div class="c-solar-section">
                    <h3><?php echo $section['section_title'] ?></h3>
                    <p><?php echo $section['section_copy'] ?></p>
                    <div class="c-brands">
                        <div class="o-layout">
                            <?php $brands = $section['brands'];
                            foreach ($brands as $brand) : ?>
                            <div class="o-layout__item u-1/4@tablet u-1/2@mobileSmall">
                                <div class="c-brands__content">
                                    <a href="<?php echo $brand['brand_link']; ?>" title="<?php echo $brand['name']; ?>" target="_blank">
                                        <img src="<?php echo $brand['logo']; ?>" alt="<?php echo $brand['name']; ?>">
                                    </a>
                                    <h4><?php echo $brand['name']; ?></h4>
                                    <?php echo $brand['description']; ?>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="c-solar-quote">
        <div class="o-wrapper">
            <p>Our Solar Specialists can provide further information and recommendations on the type of solar best for your specific solution</p>
            <a href="<?php echo SITE ?>/#quote-form" class="o-btn o-btn--transparent">Request quote</a>
        </div>
    </div>

</div>


<?php get_footer(); ?>