<?php 
/*  Template Name: Contact Page */
?>

<?php get_header(); ?>
<div class="c-contact">
    <div class="o-wrapper">
        <div class="o-layout">
            <?php $contact = get_field('contact') ?>
            <div class="o-layout__item u-1/2@tablet">
                <img src="<?php echo $contact['image'] ?>" alt="Contact">
            </div>
            <div class="o-layout__item u-1/2@tablet">
                <div class="c-contact__list">
                    <span>Email</span>
                    <a href="mailto:<?php echo $contact['email'] ?>"><?php echo $contact['email'] ?></a>
                </div>
                <div class="c-contact__list">
                    <span>Phone</span>
                    <a href="tel:<?php echo $contact['phone'] ?>"><?php echo $contact['phone'] ?></a>
                </div>
                <div class="c-contact__list">
                    <span>Address</span>
                    <?php $address = $contact['address'];
                    foreach ($address as $address) { ?>
                        <a href="<?php echo $address['link'] ?>"><?php echo $address['address'] ?></a>
                    <?php }  ?>
                </div>
            </div>

        </div>
    </div>
</div>


<?php get_footer(); ?>