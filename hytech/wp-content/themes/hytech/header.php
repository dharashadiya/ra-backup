<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
	<title><?php wp_title(); ?> </title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.png">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">
	<?php wp_head(); ?>
	<?php include('partials/inlinescripts.min.php'); ?>
	<link rel="stylesheet" href="https://use.typekit.net/zbd6acq.css">
	<?php if(!is_local_or_staging()) : ?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-70041565-54"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());  gtag('config', 'UA-70041565-54');
		</script>
		<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-589QV48');</script>
<!-- End Google Tag Manager -->
	<?php endif; ?>
</head>

<body <?php body_class(); ?>>
<?php if(!is_local_or_staging()) : ?>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-589QV48"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php endif; ?>
<div class="c-top--spacer"></div>
<div class="c-top">
	<header class="c-header o-wrapper o-wrapper--100">
		<?php
			$logo_wrap_class = "c-logo-wrap";
			if (!is_front_page()) {
				$logo_wrap_open = '<a class="' . $logo_wrap_class . ' is-link" href="' . SITE . '" rel="home" title="Back to home">';
				$logo_wrap_close = '</a>';
			} else {
				$logo_wrap_open = '<span class="' . $logo_wrap_class . '">';
				$logo_wrap_close = '</span>';
			}
		?>
		<?php
			echo $logo_wrap_open;
			echo svgicon('logo', '0 0 394 74', 'c-header__logo');
			echo $logo_wrap_close;
		?>
		<a href="#Main" class="c-skip">Skip to main content</a>
		<a href="javascript:void(0);" class="c-header__toggle">
			<span></span>
			<span></span>
			<span></span>
		</a>
		<nav class="c-site-nav" role="navigation">
		<?php
			wp_nav_menu( array(
				'theme_location' => 'primary-menu',
				'container' => false,
				'menu_class' => 'c-nav',
				'menu_id' => 'menu'
			));
		?>
		<a href="<?php echo SITE ?>/#quote-form" class="o-btn o-btn--transparent js-close-nav">Request quote</a>
		</nav>
	</header>
</div>

<div class="c-content">