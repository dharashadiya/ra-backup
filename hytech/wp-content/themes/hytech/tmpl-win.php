<?php
/*  Template Name: Win Page */
?>

<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<div class="c-win">
    <?php $banner = get_field('banner') ?>
    <div class="c-banner"  style="background-image: url(<?php echo $banner['image'] ?>)">
        <div class="o-wrapper">
            <div class="c-banner__content">
                <h1><?php echo $banner['title'] ?></h1>
                <p><?php echo $banner['short_copy'] ?></p>
            </div>
        </div>
    </div>
    <div class="c-win__contents">
        <div class="o-wrapper">
		<?php $img = get_field('image'); ?>
			<div class="o-layout">
				<div class="o-layout__item u-1/2@tablet c-win__contents-text">
					<div class="c-contents">
						<?php echo the_content(); ?>
					</div>
				</div>
				<div class="o-layout__item u-1/2@tablet c-win__contents-img">
					<?php if ($img) : ?>
						<div class="c-bgimg" style="background-image: url(<?php echo $img ?>)"></div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php $info = get_field('competition_info'); ?>
	<?php if ($info['title'] && $info['content'] ) : ?>
		<div class="c-win__compeinfo">
			<div class="o-wrapper">
				<div class="o-layout">
					<div class="o-layout__item u-1/2@tablet">
						<div class="c-win__compeinfo-header">
							<h2><?php echo $info['title']; ?></h2>
							<?php svgicon('arrow', '0 0 140 52') ?>
						</div>
					</div>
					<div class="o-layout__item u-1/2@tablet">
						<?php if ($info['content']) : ?>
							<div class="c-content">
								<?php echo $info['content'] ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php $competition = get_field('competition_form') ?>
	<?php if ($competition['title']) : ?>
		<div class="o-wrapper">
			<div class="c-win__competition">
				<div class="c-win__form">
					<h2><?php echo $competition['title'] ?></h2>
					<?php// echo do_shortcode('[contact-form-7 id="240" title="Competition form"]') ?>
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
					<script>
					hbspt.forms.create({
						portalId: "8113857",
						formId: "971bbd60-cb73-4c38-b62d-c0ba4513eecd"
					});
					</script>
				</div>
				<?php if ($competition['terms_link']) : ?>
					<p class="c-win__competition-link">Competition Terms & Conditions can be found <a href="<?php echo $competition['terms_link']['url']; ?>">here.</a></p>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

</div>
<?php endwhile; ?>

<?php get_footer(); ?>




