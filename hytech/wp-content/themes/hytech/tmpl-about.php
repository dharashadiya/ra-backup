<?php 
/*  Template Name: About Page */
?>

<?php get_header(); ?>
<div class="c-about">
    <?php $banner = get_field('banner') ?>
    <div class="c-banner"  style="background-image: url(<?php echo $banner['image'] ?>)">
        <div class="o-wrapper">
            <div class="c-banner__content">
                <h1><?php echo $banner['title'] ?></h1>
                <p><?php echo $banner['short_copy'] ?></p>
            </div>		
        </div>
    </div>
    <div class="c-about__contents">
        <div class="o-wrapper">
        <?php $content = get_field('content'); ?>
        <?php foreach ($content as $content) :?>
            <div class="c-about__contents-list">
                <div class="c-about__contents-list-img">
                    <div class="c-bgimg" style="background-image: url(<?php echo $content['image'] ?>)"></div>
                </div>
                <div class="c-about__contents-list-text">
                    <div class="c-contents">
                        <?php echo $content['copy'] ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>

</div>


<?php get_footer(); ?>