		</div><!-- .c-content -->
		<?php get_template_part( 'partials/brand-logo-slider' ); ?>
		<footer class="c-footer">

			<div class="o-wrapper">
				<?php 
					$subscribe_form = get_field('subscribe_form','options');
					$title = $subscribe_form['title'];
					$form = $subscribe_form['subscribe_form_text'];
					$location = get_field('location','options');
					$email = get_field('email','options');
					$phone = get_field('phone','options');
					$social_links = get_field('social_links','options');
					$brand_logo = get_field('brand_logo','options');
				?>
				<div class="o-layout">
					<div class="o-layout__item u-hide@tabletS">
						<div class="c-footer__return-copy-top ">
							<a href="#top-page-banner">Return to Top<img class="c-footer__top-arrow-img" src="<?php echo ASSETS .'/img/top-arrow.png'; ?>"> </a>
						</div>
					</div>
				</div>
				<div class="c-footer__top-content">
					<div class="o-layout o-layout--flush o-order o-order-center o-order--tabletS">
						<div class="o-layout__item u-1/5@tabletWide u-1/4@tabletM u-1/3@tabletS u-1/2 o-order__3@tabletS o-order__4@tablet o-order__5@tabletWide u-max-width-1@tabletS">
							<img class="c-footer__logo" style="width:100px; height:auto;" src="<?php echo $brand_logo['url']; ?>" alt="">
							<p class="c-footer__social-two">
								<?php foreach($social_links as $logo) : ?>
									<a href="<?php echo $logo['social_link']['url']; ?>" target="_blank"> 
										<img class="<?php echo $logo['select']; ?>"
										style="width:15px;" 
										src="<?php echo $logo['social_icon']['url']; ?>" alt="">
									</a>
								<?php endforeach; ?>
							</p>
						</div>
						<div class="o-layout__item u-1/5@tabletWide u-1/4@tabletM u-1/3@tabletS u-1/2 u-max-width-2@tabletS o-order__1@tabletS padding-right">
							<div class="c-footer__contact">
								<h5>Contact<br>Heilo</h5>
								<p class="margin-bottom location"><a target="_blank" href="https://www.google.com/maps?q=<?php echo $location; ?>"><?php echo $location; ?></p></a>
								<a class="email" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
								<a class="margin-bottom" href="tel:<?php echo $phone; ?>" ><?php echo $phone; ?></a>
								<p class="c-footer__social-one">
									<?php foreach($social_links as $logo) : ?>
										<a href="<?php echo $logo['social_link']['url']; ?>" target="_blank"> 
											<img class="<?php echo $logo['select']; ?>"
											style="width:15px;" 
											src="<?php echo $logo['social_icon']['url']; ?>" alt="">
										</a>
									<?php endforeach; ?>
								</p>
							</div>
						</div>
						<div class="o-layout__item u-1/5@tabletWide u-1/4@tabletM u-1/3@tabletS u-show@tabletS o-order__2@tabletS">
							<div class="c-footer__automation-types">
								<h5>Automation<br>Types</h5>
								<?php
									wp_nav_menu( array(
										'theme_location' => 'automation-types',
										'container' => false,
										'menu_class' => 'c-nav',
										'menu_id' => 'menu'
									));
								?>
							</div>
						</div>
						<div class="o-layout__item u-1/5@tabletWide u-show@tabletM u-1/4@tabletM o-order__3@tablet">
							<div class="c-footer__smart-lives">
								<h5>Smart<br>lives</h5>
								<?php
									wp_nav_menu( array(
										'theme_location' => 'smart-lives',
										'container' => false,
										'menu_class' => 'c-nav',
										'menu_id' => 'menu'
									));
								?>
							</div>
						</div>
						<div class="o-layout__item  u-1/5@tabletWide u-show@tabletWide o-order__4@tabletWide ">
							<div class="c-footer__subscribe-form">
								<h5>Join our<br>Newsletter</h5>
								<?php echo $form; ?>
							</div>
						</div>
						
					</div>
				</div>

				<div class="o-layout o-layout--flush-footer-bottom">
					<div class="c-footer__bootom-content">
						<div class="o-layout__item u-2/7@tablet u-align-start u-show@tabletS">
							<div class="c-footer__copy-left">
								<a href="#top-page-banner">Return to Top<img class="c-footer__top-arrow-img" src="<?php echo ASSETS .'/img/top-arrow.png'; ?>"> </a>
							</div>
						</div>
						<div class="o-layout__item u-3/7@tablet u-align-center">
							<div class="c-footer__copy-mid">
								<a href="/terms-conditions/">Terms & Conditions</a><p>&nbsp;&nbsp;|&nbsp;&nbsp;</p><a href="/privacy-policy/">Privacy</a><p>&nbsp;&nbsp;|&nbsp;&nbsp;</p><p>&nbsp;Built&nbsp;by&nbsp;</p><a href="https://rockagency.com.au/">Rock&nbsp;Agency</a>.
							</div>
						</div>
						<div class="o-layout__item u-2/7@tablet u-align-end">
							<div class="c-footer__copy-right">
								<p>&copy;&nbsp;Copyright&nbsp;Heilo&nbsp;<?php echo date("Y"); ?>.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>