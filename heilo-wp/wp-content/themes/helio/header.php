<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
	<title><?php wp_title(); ?> </title>

	<?php if(is_local_or_staging()) : ?>
		<meta name="robots" content="none" />
		<meta name="robots" content="noindex" />
		<meta name="googlebot" content="none" />
		<meta name="googlebot" content="noindex" />
	<?php endif; ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">
	<?php wp_head(); ?>
	<?php include('partials/inlinescripts.min.php'); ?>
</head>

<body <?php body_class(); ?>>
	<div class="c-top">
		<header id="header" class="c-header" >
			<div class="o-wrapper">
				<div id="headerWrap" class="c-header__HeaderInnerWrap">
					<?php
						$logo_wrap_class = "c-logo-wrap";
						if (!is_front_page()) {
							$logo_wrap_open = '<a class="' . $logo_wrap_class . ' is-link" href="/" rel="home" title="Back to home">';
							$logo_wrap_close = '</a>';
						} else {
							$logo_wrap_open = '<a class="' . $logo_wrap_class . '">';
							$logo_wrap_close = '</a>';
						}
					?>
					<?php $brand_logo = get_field('brand_logo','options'); ?>
					<?php echo $logo_wrap_open;	?>
						<img class="c-header__logo" style="width:100px; height:auto;" src="<?php echo $brand_logo['url']; ?>" alt="">
					<?php echo $logo_wrap_close; ?>

					<div id="" class="sidebar" data-toggle="main-nav">
						<div class="sidebar__menu-inner">
							<nav class="sidebar__MenuLeft">
								<section>
									<h5>Who is Heilo</h5>
									<?php
										wp_nav_menu( array(
											'theme_location' => 'primary-menu',
											'container' => false,
											'menu_class' => 'c-nav',
											'menu_id' => 'menu'
										));
									?>
								</section>
								<section>
									<h5>Smart Lives</h5>
									<?php
										wp_nav_menu( array(
											'theme_location' => 'smart-lives',
											'container' => false,
											'menu_class' => 'c-nav',
											'menu_id' => 'menu'
										));
									?>
								</section>
								<section>
									<h5>Automation Types</h5>
									<?php
										wp_nav_menu( array(
											'theme_location' => 'automation-types',
											'container' => false,
											'menu_class' => 'c-nav',
											'menu_id' => 'menu'
										));
									?>
								</section>
							</nav>
							<nav class="sidebar__MenuRight">
							<div class="sidebar__MenuRight-inner">
								<div class="sidebar__bm-cross-button"  data-toggle-content="main-nav">
									<?php echo svgicon('cross', '0 0  18 18','cross-svg'); ?>
								</div>
								<a href="#">
									<?php echo svgicon('home', '0 0  20 22','home-svg'); ?>
								</a>
								<a href="#">
									<?php echo svgicon('calendar', '0 0  20 22','calendar-svg'); ?>
								</a>
							</div>
							</nav> 
						</div>
					</div>
				
					<div class="bm-burger-button" style="z-index:1000" data-toggle-content="main-nav">
						<span>
							<span class="bm-burger-bars" style="position: absolute; height: 20%; left: 0px; right: 0px; top: 0%; opacity: 1;">
							</span>
							<span class="bm-burger-bars" style="position: absolute; height: 20%; left: 0px; right: 0px; top: 40%; opacity: 1;">
							</span>
							<span class="bm-burger-bars" style="position: absolute; height: 20%; left: 0px; right: 0px; top: 80%; opacity: 1;">
							</span>
						</span>
						<a style="position:absolute;left:0;top:0;width:100%;height:100%;margin:0;padding:0;border:none;font-size:0;background:transparent;cursor:pointer" data-toggle="main-nav" id="hamburger">Open Menu</a>
					</div>
					
				</div>
			</div>
		</header>
	</div>

	<div class="c-content">