<?php

// Hide ACF field group menu item
// add_filter('acf/settings/show_admin', '__return_false');

add_filter('acf/settings/save_json', 'acf_json_save_point');
function acf_json_save_point( $path ) {
	$path = get_stylesheet_directory() . '/functions/acf-jsons';
    return $path;
}

add_filter('acf/settings/load_json', 'acf_json_load_point');
function acf_json_load_point( $paths ) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/functions/acf-jsons';
    return $paths;
}

// Register Options page
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title'  => 'General Site Options',
		'menu_title'  => 'Site Options',
		'menu_slug'   => 'site-options',
		'capability'  => 'edit_posts',
		'redirect'    => false
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> '404 Page Settings',
		'menu_title'	=> '404 Page',
		'capability'	=> 'edit_posts',
		'parent_slug'	=> 'site-options',
		'redirect'    => true
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer Settings',
		'menu_title'	=> 'Footer',
		'capability'	=> 'edit_posts',
		'parent_slug'	=> 'site-options',
		'redirect'    => true
	));
}

	function my_acf_google_map_api( $api_key ) {
		$key = get_field('api_key', 'options');
		$api_key['key'] = $key;
		return $api_key;
	}
	add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
?>