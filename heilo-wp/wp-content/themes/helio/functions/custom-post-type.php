<?php 
    function create_custom_smart_lives_post_type() {
        register_post_type('smart_lives', array(
            'labels' => array(
                'name' => __('Smart Lives'),
                'singular_name' => __('Smart Lives')
            ),
            'menu_icon' => 'dashicons-admin-home',
            'public' => true,
            'show_in_admin_bar' => true,
            'supports' => array('title', 'editor', 'page-attributes'),
            'show_in_rest' => true,
                    'rewrite' => array(
                'slug' => "smart-lives"
            )
        ));
        
        add_post_type_support('smart_lives', array('thumbnail', 'excerpt'));
    }

    function create_custom_automation_types_post_type() {
        register_post_type('automation_types', array(
            'labels' => array(
                'name' => __('Automation Types'),
                'singular_name' => __('Automation Types')
            ),
            'menu_icon' => 'dashicons-admin-settings',
            'public' => true,
            'show_in_admin_bar' => true,
            'show_in_rest' => true,
            'supports' => array('title', 'editor', 'page-attributes'),
            'rewrite' => array(
                'slug' => "automation-types"
            )
        ));
        
        add_post_type_support('automation_types', array('thumbnail', 'excerpt'));
    }
    add_action('init', create_custom_automation_types_post_type());
    add_action('init', create_custom_smart_lives_post_type());

; ?>