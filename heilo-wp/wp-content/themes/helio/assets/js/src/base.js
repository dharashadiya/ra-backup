/* global site */
// [1] Import functions here
import {
	isMobile,
	openLinkNewTab,
	superfishMenu,
	slider,
	cloudinaryImageCDN
} from "./template-functions";
(function($) {
	window.site || (window.site = {});
	
	AOS.init({
		once: true
	});

	$.extend(
		site,
		{
			initVars: () => {
				// Global variables -> use as site.VariableName in other files
				site.body = $("body");
				site.root = $("html");
				site.header = $("#Top");
				site.main = $("#Main");
				site.side = $("#Secondary");
				site.siteurl = $("#Siteurl").attr("content");
				site.themeurl = $("#Themeurl").attr("content");
				return (site.ajaxurl = $("#Ajaxurl").attr("content"));
			},
			// [3] Call functions within either of these
			onReady: () => {
				svg4everybody();
				site.initVars();
				site.toggle();
				site.smoothScroll();
				site.slider();
				site.unveil();
				site.cloudinaryImageCDN()
				site.backgroundVideo()
				site.fixedHeader()
				return site.isMobile();
			},
			onLoad: () => {
				// return site.slider()
			},
			
			toggle: () => {
				$('body').on("click", "[data-toggle]", function() {
					$('body').find('[data-toggle-content=' + $(this).data('toggle') + ']').toggleClass('is-active');
					$('body').find('[data-toggle=' + $(this).data('toggle') + ']').toggleClass('is-active');

					if ( $(this).data('toggle') == 'main-nav' || $(this).data('toggle').includes('overlay') ) {
					  if ( $('body').find('[data-toggle=' + $(this).data('toggle') + ']').hasClass('is-active') ) {
						$('html, body').addClass('no-overflow');
					  } else {
						$('html, body').removeClass('no-overflow');
					  }

					}
				});
			},

			slider: () => {
				$('.carousel').slick({
					autoplay: true,
					speed: 500,
					infinite: true,
					slidesToShow: 6,
					slidesToScroll: 1,
					prevArrow: $('.slick-prev'),
					nextArrow: $('.slick-next'),
					centerPadding: "50px",
					responsive: [ 
						{
							breakpoint: 770,
							settings: {
								slidesToShow: 3,
							}
						},
						{
							breakpoint: 1210,
							settings: {
								slidesToShow: 5,
							}
						}
					]
				});  	
				$('.slider-carousel').slick({
					autoplay: false,
					speed: 500,
					dots: false,
					infinite: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					prevArrow: $('.slick-prev'),
					nextArrow: $('.slick-next')

				}); 
				
				AOS.refresh();
			},
			smoothScroll: () => {
				$(window).on('click', 'a[href^="#"]', function (event) {
					event.preventDefault();

					$('html, body').animate({
					  scrollTop: $($.attr(this, 'href')).offset().top
					}, 300);
				});
			},
			unveil: () => {
				$('[data-src]').unveil(-1);
			},

			backgroundVideo: () => {
				$(window).on('scroll', () => {
					let banner = $('.c-fullscreen-video');
					if ( banner.length > 0 ) {
						let scrollPosition = window.scrollY;
						let bannerHeight = banner.innerHeight();
						let bannerTop = banner.offset().top;
						if ( scrollPosition >= bannerTop + bannerHeight + 100 ) {
							banner.addClass('is-scrolled');
						} else {
							banner.removeClass('is-scrolled');
						}
					}
				})
			},
			fixedHeader: () => {
				$(window).on('scroll', () => {
					let header = $('.c-header');
					let headerHeight = header.innerHeight();
					let scroll = jQuery(window).scrollTop();
					if (scroll >= headerHeight) {
						jQuery(".c-top").addClass("pa-fixed-header");
					} else{
						jQuery(".c-top").removeClass("pa-fixed-header");
					}
					
				})
			}
		},
		
		{
			// [2] Register functions here
			isMobile: isMobile, 
			openLinkNewTab: openLinkNewTab,
			superfishMenu: superfishMenu,
			cloudinaryImageCDN: cloudinaryImageCDN
		}
	);
	$(() => {
		return site.onReady();
	});
	return $(window).on("load", () => {
		return site.onLoad();
	});
})(jQuery);

var rellax = new Rellax('.rellax');

window.onload = function() {
	lax.setup() // init

	const updateLax = () => {
		lax.update(window.scrollY)
		window.requestAnimationFrame(updateLax)
	}

	window.requestAnimationFrame(updateLax)
}

