<?php get_header(); ?>
<?php
	get_template_part( 'partials/page-banner');
	$banner = get_field('404_content', 'option')
?>

<div class="c-404" id="content">
    <div class="o-wrapper">
		<main class="c-404__container" >
			<article>
				<p class="c-404__content">
					<?php echo $banner['message']; ?>
				</p>
			</article>
		</main>
	</div>
</div>

<?php get_footer(); ?>