<?php /* Template name: About Page*/ ?>


<?php get_header(); ?>
<?php
    if(have_posts()) {
        while(have_posts()) {
            get_template_part( 'partials/page-banner');
            get_template_part( 'partials/about-partials/about-two-column');
            the_post();		
        }
    }
    ?>
   
<?php get_footer(); ?>

