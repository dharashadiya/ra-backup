
<?php get_header(); ?>
<?php
    if(have_posts()) {
        while(have_posts()) {
            get_template_part( 'partials/page-banner');
            get_template_part( 'partials/single-smart-lives-partials/two-column');
            get_template_part( 'partials/single-smart-lives-partials/three-column');
            get_template_part( 'partials/automation-list' );
            the_post();						
        }
    }
?>
<?php get_footer(); ?>


