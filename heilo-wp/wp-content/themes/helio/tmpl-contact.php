<?php /* Template name: Contact Us Page*/ ?>


<?php get_header(); ?>
<?php
    if(have_posts()) {
        while(have_posts()) {
            get_template_part( 'partials/page-banner');
            get_template_part( 'partials/contact-partials/contact-two-column');
            get_template_part( 'partials/contact-partials/map');
            the_post();		
        }
    }
    ?>

<?php get_footer(); ?>

