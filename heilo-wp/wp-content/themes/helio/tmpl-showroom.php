<?php /* Template name: Heilo Showroom Page*/ ?>


<?php get_header(); ?>
<?php
    if(have_posts()) {
        while(have_posts()) {
            get_template_part( 'partials/page-banner');
            get_template_part( 'partials/showroom-partials/showroom-two-column');
            the_post();		
        }
    }
?>
   
<?php get_footer(); ?>

