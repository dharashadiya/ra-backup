
<?php get_header(); ?>
<?php
    if(have_posts()) {
        while(have_posts()) {
            get_template_part( 'partials/page-banner');
            get_template_part( 'partials/single-automation-types-partials/main-content');
            get_template_part( 'partials/single-automation-types-partials/content-blocks');
            get_template_part( 'partials/single-automation-types-partials/image-slider');
            get_template_part( 'partials/automation-list' );
            get_template_part( 'partials/single-automation-types-partials/faq');
            get_template_part( 'partials/single-automation-types-partials/help-form');
            the_post();						
        }
    }
    ?>
<?php get_footer(); ?>


