<?php 
    $automation_lists = get_field('automation_lists', 'options'); 
    $heading = get_field('heading', 'options'); 
?>
<div class="c-automation-list gnzPdz">
    <div class="o-wrapper">
        <h4 class="c-automation-list__heading"> <?php echo $heading; ?> </h4>
        <div class="o-layout">
            <?php foreach($automation_lists as $key => $list) : ?>
                <?php 
                    $name = $list['automation_name']; 
                    $image = $list['automation_image'];
                    $link = $list['link']; 
                ?>
                <div class="o-layout__item u-1/6@tabletWide u-1/3@mobileMedium u-1/2">
                    <div class="c-automation-list__wrapper" data-aos="fade-animate" data-aos-duration="400"  data-aos-delay="<?php echo ($key+1)*100 ?>">
                        <a class="c-automation-list__link cYmgiE" href="<?php echo $link['url']; ?>">
                            <div class="c-automation-list__image-wrapper" >
                                <img class="c-automation-list__image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
                            </div>
                            <p class="c-automation-list__name" ><?php echo $name; ?></p>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>