<?php 
    $showroom = get_field('showroom'); 
    $title = $showroom['sub_title'];
    $content = $showroom['content'];
    $phone = $showroom['phone'];
?>

<section class="showroom-two-column">
    <div class="o-wrapper">
        <section class="o-layout o-layout--home-content ">
            <div class="o-layout__item u-1/2@tablet ">
                <h3 class="showroom-two-column__heading" data-aos="fade-animate" data-aos-duration="800"  data-aos-delay="0"><?php echo $title; ?></h3>
            </div>
            <div class="o-layout__item u-1/2@tablet">
                <div class="showroom-two-column__wrapper" data-aos="fade-animate" data-aos-duration="1000"  data-aos-delay="200">
                    <div class="showroom-two-column__content"><?php echo $content; ?></div>
                    <a href="tel:<?php echo $phone; ?>" class="showroom-two-column__phone-link">
                        <?php echo svgicon('phone', '0 0  22 22','showroom-two-column__phone-link__svg'); ?><span>call:&nbsp;</span><?php echo $phone; ?>
                    </a>
                </div>
            </div>
        </section>
    </div>
</section>