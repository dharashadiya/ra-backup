<?php 
    $contact = get_field('contact'); 
    $title = $contact['sub_title'];
    $content_one = $contact['content_before_phone'];
    $content_two = $contact['content_after_phone'];
    $phone = $contact['phone'];
?>

<section class="contact-two-column">
    <div class="o-wrapper">
        <section class="o-layout o-layout--home-content ">
            <div class="o-layout__item u-1/2@tablet ">
                <h3 class="contact-two-column__heading"  data-aos="fade-animate" data-aos-duration="800"  data-aos-delay="0"><?php echo $title; ?></h3>
            </div>
            <div class="o-layout__item u-1/2@tablet">
                <div class="contact-two-column__wrapper"  data-aos="fade-animate" data-aos-duration="1000"  data-aos-delay="200">
                    <div class="contact-two-column__content-one "><?php echo $content_one; ?></div>
                    <a href="tel:<?php echo $phone; ?>" class="contact-two-column__phone-link">
                        <?php echo svgicon('phone', '0 0  22 22','contact-two-column__phone-link__svg'); ?><span>call:&nbsp;</span><?php echo $phone; ?>
                    </a>
                    <div class="contact-two-column__content-two"><?php echo $content_two; ?></div>
                </div>
            </div>
        </section>
    </div>
</section>