

<?php 
    $contact = get_field('contact'); 
    $address = $contact['google_map'];
?>

<div class="map" id="map" data-lat="<?php echo esc_attr($address['lat']); ?>" 
    data-lng="<?php echo esc_attr($address['lng']); ?>">
    <div class="marker" data-lat="<?php echo esc_attr($address['lat']); ?>" 
    data-lng="<?php echo esc_attr($address['lng']); ?>" data-pin="<?php echo ASSETS . '/img/inline/map-marker.svg'; ?>"></div>
</div>
