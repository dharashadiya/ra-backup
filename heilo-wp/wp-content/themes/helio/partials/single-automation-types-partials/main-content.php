

<div class="automation-type-main-content">
    <div class="o-wrapper">
        <?php 
            $main_content = get_field('main_content');
            $title = $main_content['title'];
            $content = $main_content['content'];
            $image = $main_content['image'];
        ?>
        <div class="o-layout o-layout--middle">
            <div class="o-layout__item margin u-3/8@laptop u-4/8@tabletWide u-5/8@tablet ">
                <div class="automation-type-main-content__content-wrapper">
                    <h2 class="automation-type-main-content__title"><?php echo $title; ?></h2>
                    <div class="automation-type-main-content__intro-one"><?php echo $content; ?></div>
                </div>
            </div>
            <div class="o-layout__item u-5/8@laptop  margin u-4/8@tabletWide u-3/8@tablet ">
                <div class="automation-type-main-content__image-wrapper">
                    <img class="automation-type-main-content__image" data-aos="blur" src="<?php echo $image['url']; ?>">
                    <div class="automation-type-main-content__intro-two"><?php echo $content; ?></div>
                </div>
            </div>
        </div> 
    </div>
</div>
