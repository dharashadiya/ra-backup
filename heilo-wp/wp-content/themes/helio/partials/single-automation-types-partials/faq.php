<?php 
    $faqs = get_field('faq');
    $faq_title = get_field('faq_title');
?>

<div class="automation-type-faq">
    <div class="o-wrapper">
        <div class="o-layout o-layout--center ">
            <h2 class="automation-type-faq__title"><?php echo $faq_title; ?></h2>
            <?php foreach($faqs as $key => $faq) : ?>
                <?php 
                    $question = $faq['question'];
                    $answer = $faq['answer'];
                ?>
                <div class="o-layout__item u-1/3@tablet ">
                    <div class="automation-type-faq__content-wrapper" data-aos="fade-animate" data-aos-duration="1000"  data-aos-delay="<?php echo ($key+1)*200 ?>">
                        <h4 class="automation-type-faq__question"><?php echo $question; ?></h4>
                        <div class="automation-type-faq__answer"><?php echo $answer; ?></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>