
<?php 
    $help_form = get_field('help_form','options');
    $content = $help_form['content'];
    $title = $help_form['title'];
    $phone = $help_form['phone'];
    $form = $help_form['help_form_text']; 
?>
<div class="help-form">
    <div class="o-wrapper">
        <div class="o-layout">
            <div class="o-layout__item">
                <div class="help-form__top-content">
                    <h2 class="help-form__title"><?php echo $title; ?></h2>
                    <p class="help-form__content"><?php echo $content; ?>
                    <a class="help-form__phone-link" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></p>
                </div>
            </div>
            <div class="o-layout__item">
                <div class="help-form__help-form">
                    <?php echo $form; ?>
                </div>
            </div>
        </div>
    </div>
</div>