<?php 
    $content_blocks = get_field('content_blocks');
    $title = $content_blocks['title'];
    $content = $content_blocks['content'];
    $image = $content_blocks['image'];
?>
<div class="automation-type-content-blocks">
    <div class="o-wrapper">
        <div class="o-layout o-layout--center o-layout--large2">
            <?php foreach($content_blocks as $key => $block) : ?>
                <?php 
                    $title = $block['title'];
                    $content = $block['content'];
                    $image = $block['image'];
                ?>
                <div class="o-layout__item u-1/3@tablet ">
                    <div class="automation-type-content-blocks__content-wrapper" data-aos="fade-animate" data-aos-duration="1000"  data-aos-delay="<?php echo ($key+1)*200 ?>">
                        <h4 class="automation-type-content-blocks__title"><?php echo $title; ?></h4>
                        <img class="automation-type-content-blocks__image" src=" <?php echo $image['url']; ?>">
                        <div class="automation-type-content-blocks__intro"><?php echo $content; ?></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>