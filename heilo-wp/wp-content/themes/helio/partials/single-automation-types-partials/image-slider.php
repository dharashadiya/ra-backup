

<?php 
    $slider_images = get_field('slider_images'); 
?>

<div class="automation-type-image-slider">
    <div class="slider-carousel">
        <?php foreach($slider_images as $slider_image) : ?>
            <?php $image = $slider_image['image']; ?>
                <div class="automation-type-image-slider__image-wrap">
                    <img class="automation-type-image-slider__image" src="<?php echo $image['url']; ?>">
                </div>
        <?php endforeach; ?>
    </div>
    <button class="slick-prev">
        <?php echo svgicon('slider-arrow', '0 0  27 47', 'automation-type-image-slider__left-arrow-svg'); ?>
    </button>
    <button class="slick-next">
        <?php echo svgicon('slider-arrow', '0 0  27 47', 'automation-type-image-slider__right-arrow-svg '); ?>
    </button>
</div>