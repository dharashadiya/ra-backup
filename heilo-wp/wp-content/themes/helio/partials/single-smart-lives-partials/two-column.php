

<div class="two-column">
    <div class="o-wrapper">
        <?php 
            $sub_title = get_field('sub_title');
        ?>
        <div class="o-layout">
            <div class="o-layout__item margin u-3/8@tablet ">
                <div class="two-column__content-wrapper" data-aos="fade-animate" data-aos-duration="800"  data-aos-delay="0">
                    <h2 class="two-column__title"><?php echo $sub_title; ?></h2>
                </div>
            </div>
            <div class="o-layout__item margin u-5/8@tablet ">
                <div class="two-column__image-wrapper" data-aos="fade-animate" data-aos-duration="1000"  data-aos-delay="200">
                    <div class="two-column__intro"><?php the_content(); ?></div>
                    <a class="two-column__btn" href="/showroom"><button class="o-btn ">Visit our showroom</button></a>
                </div>
            </div>
        </div> 
    </div>
</div>
