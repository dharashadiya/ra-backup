<?php 
    $content_blocks = get_field('content_blocks');
?>

<div class="three-column">
    <div class="o-wrapper">
        <div class="o-layout o-layout--center  o-layout--large2">
            <?php foreach($content_blocks as $key => $content_block) : ?>
                <?php 
                    $title = $content_block['title'];
                    $content = $content_block['content'];
                ?>
                <div class="o-layout__item u-1/3@tablet ">
                    <div class="three-column__content-wrapper" data-aos="fade-animate" data-aos-duration="1000"  data-aos-delay="<?php echo ($key+1)*200 ?>">
                        <h4 class="three-column__title"><?php echo $title; ?></h4>
                        <div class="three-column__content"><?php echo $content; ?></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>