<?php 
    $home_partials = get_field('home'); 
    $middle_contact_section = $home_partials['middle_contact_section'];
    $heading = $middle_contact_section['heading'];
    $content_block = $middle_contact_section['content_block'];
?>
<section class="contact-section">
    <div class="o-wrapper">
        <section class="o-layout o-layout--home-content undefined">
            <div class="o-layout__item u-1/3@tabletWide ">
                <h3 class="contact-section__heading" data-aos="fade-animate" data-aos-duration="800" data-aos-delay="0" ><?php echo $heading; ?></h3>
            </div>
            
            <?php foreach($content_block as $key=>$contact_section) : ?>
                <?php 
                    $title = $contact_section['title'];
                    $content = $contact_section['content'];
                    $phone = $contact_section['phone'];
                ?>
                <div class="o-layout__item u-1/3@tabletWide u-3/3@tablet">
                    <div class="contact-section__wrapper" data-aos="fade-animate" data-aos-duration="1000"  data-aos-delay="<?php echo ($key+1)*200 ?>">
                        <h4 class="contact-section__title"><?php echo $title; ?></h4>
                        <p class="contact-section__content"><?php echo $content; ?></p>
                        <?php if($phone) : ?>
                            <a href="tel:<?php echo $phone; ?>" class="contact-section__phone-link">
                                <?php echo svgicon('phone', '0 0  22 22','contact-section__phone-link__svg'); ?><span>call:&nbsp;</span><?php echo $phone; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </section>
    </div>
</section>