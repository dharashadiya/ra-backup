<?php 
    $home_partials = get_field('home'); 
    $smart_living_section = $home_partials['smart_living_section'];
?>

<section class="c-home-smart-lives o-layout o-layout--flush">
    <?php foreach($smart_living_section as $section) : ?>
      
        <?php 
            $bg_image = $section['background_image'];
            $page_link = $section['page_link'];
        ?>
        <div class="o-layout__item u-1/1@mobileLandscape u-1/2@tablet u-1/4@tabletWide">
            <div class="c-home-smart-lives__wrapper ">
                <div class="c-home-smart-lives__background-image" 
                style="background-image: url(<?php echo $bg_image['url']; ?>);">
                </div>
                <div class="c-home-smart-lives__bg"></div>
                <a href="<?php  echo $page_link['url'];  ?>">
                    <button class="o-btn c-home-smart-lives__btn"><?php echo $page_link['title']; ?></button>
                </a>
            </div>
        </div>
    <?php endforeach; ?>
</section>