<?php 
    $home_partials = get_field('home'); 
    $lower_customer_section = $home_partials['lower_customer_section'];
    $title = $lower_customer_section['title'];
    $content = $lower_customer_section['content'];
    $customers = $lower_customer_section['people'];
?>

<section class="c-home-customer-section">
    <span style="font-size:0"></span>
    <div class="o-wrapper">
        <h3 class="c-home-customer-section__title"><?php echo $title; ?></h3>
        <p class="c-home-customer-section__sub-title"><?php echo $content; ?></p>
        <div class="c-home-customer-section__main">

            <div class="o-layout">
                <?php foreach($customers as $key => $customer) : ?> 
                    <?php 
                        $image = $customer['image'];
                        $name = $customer['name'];
                    ?>
                    <div class="o-layout__item u-1/3@tablet u-1/2@tabletS c-home-customer-section__customer">
                        <div class="c-home-customer-section__customer-wrapper" data-aos="fade-animate" data-aos-duration="1000" data-aos-delay="<?php echo ($key+1)*100 ?>">
                            <div class="c-home-customer-section__content-wrapper"> 
                                <div class="c-home-customer-section__background-image" 
                                style="background-image: url(<?php echo $image['url']; ?>);">
                                    <p class="c-home-customer-section__name-btn"> <?php echo $name; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

</section>