<?php 
    $home_partials = get_field('home'); 
    $automation_section = $home_partials['automation_section'];
    $image = $automation_section['image'];
    $title = $automation_section['title'];
    $content = $automation_section['content'];
    $button = $automation_section['button'];
?>

<div id="automation" class="c-home-automation-section" style="background-image:url(<?php echo $image['url']; ?>);">
    <div class="o-wrapper" id="trigger-right">
    <div  data-aos="fade-left" data-aos-anchor="#trigger-right" data-aos-duration="160" data-aos-delay="600" >
        <section class="c-home-automation-section__section" >
            <h3><?php echo $title; ?></h3>
            <p><?php echo $content; ?></p>
            <button class="c-home-automation-section__btn o-btn">
                <a href="<?php echo $button['url']; ?>"><?php echo $button['title']; ?></a>
            </button>
        </section>
    </div>
    </div>
</div>