<?php 
    $page_selector = get_field('page_selector');
    $top_btn = get_field('top_button');
    $thumbnail_url = wp_get_attachment_url( get_post_thumbnail_id());
    $home = get_field('home');
    $mobile_banner = $home['mobile_banner'];
    $content_404 = get_field('404_content', 'option');
    $title_404 = $content_404['title'];
    $bg_404 = $content_404['page_banner_background_image'];
    $video_id = get_field('video_id');
 ?>
<div class="page-banner" id="top-page-banner">
    <?php if(is_404()) : ?>
            <div class="page-banner__background-image" style="background-image: url(<?php echo $bg_404['url']; ?>);">
            </div>
        <?php else: ?>
            <div class="page-banner__background-image" style="background-image: url(<?php echo $thumbnail_url; ?>);">
            </div>
    <?php endif; ?>
    <div class="page-banner__background-image" style="background-image: url(<?php echo $thumbnail_url; ?>);">
    </div>
    <?php if(is_singular('smart_lives')) : ?>
        <div class="page-banner__background-video" data-toggle-content="page-banner-play-btn">
            <iframe frameborder="0" allowfullscreen="1" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" title="YouTube video player" width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $video_id; ?>" id="widget4"></iframe>
        </div>
    <?php else: ?> <?php endif; ?>
    <?php if( is_single() ) : ?> <div class="o-wrapper page-banner__single-wrapper"><?php else: ?> 
    <?php endif; ?>
        <div class="page-banner__content-wrap <?php echo $page_selector; ?>">
            <div class="o-layout">
                <div class="o-layout__item">
                    <?php if(is_singular('smart_lives')) : ?>
                        <h3>Introducing</h3>
                    <?php else: ?> <?php endif; ?>
                    <?php if(is_404()) : ?> 
                        <h1  class="page-banner__heading"><?php echo $title_404; ?></h1>
                        <?php else: ?>
                            <h1  class="page-banner__heading"><?php the_title(); ?></h1>
                    <?php endif; ?>
                </div>
                <div class="o-layout__item">
                    <section class="page-banner__content-wrap-inner">
                        <?php if(!is_page_template('tmpl-privacy-policy.php') && !is_page_template('tmpl-terms-and-conditions.php') && !is_singular('smart_lives')) : ?>
                            <div class="page-banner__intro"><?php the_content(); ?></div>
                        <?php endif; ?>
                        <?php if(is_front_page()) : ?>
                            <button class="page-banner__btn o-btn o-btn--white">
                                <a href="<?php echo $top_btn['url']; ?>" target="<?php echo $top_btn['target']; ?>"><?php echo $top_btn['title']; ?></a>
                            </button>
                        <?php else: ?> <?php endif; ?>
                        <?php if(is_page_template('tmpl-about.php')) : ?>
                            <button class="page-banner__btn o-btn ">
                                <a href="<?php echo $top_btn['url']; ?>" target="<?php echo $top_btn['target']; ?>"><?php echo $top_btn['title']; ?></a>
                            </button>
                        <?php else: ?> <?php endif; ?>
                    </section>
                </div>
            </div>
        </div>
    <?php if( is_single() ) : ?> </div><?php else: ?> <?php endif; ?>
    <?php if(is_front_page()) : ?>
        <a href="#automation" class="page-banner__down-arrow-link" >
            <?php echo svgicon('down-arrow', '0 0 20 22', 'page-banner__down-arrow-svg'); ?>
        </a>
    <?php else: ?> <?php endif; ?>
    <?php if(is_singular('smart_lives')) : ?>
        <a class="page-banner__play-button-link"  data-toggle="page-banner-play-btn">
            <?php echo svgicon('play-button', '0 0 159 159', 'page-banner__play-button-svg'); ?>
        </a>
    <?php else: ?> <?php endif; ?>
</div>