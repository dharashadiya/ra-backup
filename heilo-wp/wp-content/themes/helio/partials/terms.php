<?php $heading = get_field('sub_heading'); ?>


<div class="privacy-terms">
    <div class="o-wrapper">
        <div class="o-layout o-layout--large2">
            <div class="o-layout__item u-1/3@tablet">
                <h3 class="privacy-terms__heading"><?php echo $heading; ?></h3>
            </div>
            <div class="o-layout__item u-2/3@tablet">
                <div itemsize="u-2/3@tablet">
                    <p><?php the_content(); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>


