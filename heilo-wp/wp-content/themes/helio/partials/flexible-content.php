<?php
/**
* Content Blocks: Include this file to loop through any content blocks added to current template
*/
?>
<?php $content_block_directory = dirname(__DIR__).'/flexible-content/'; ?>
<div id="content" class="content-blocks">
  <?php 
    foreach ( $content_blocks as $content_block_key => $block ):
	  $layout = str_replace('_', '-', $block['acf_fc_layout']);

      $file = $content_block_directory.$layout.'.php';
        if ( file_exists($file) ):
          $block_id = '';
          if ( isset($block['id']) && $block['id']!="" ) :
            $block_id = $block['id'];
          endif;
        ?>
          <div
            id="flexible-content-<?php echo $content_block_key; ?>"
            class="c-content-block c-<?php echo $layout; ?>-block layout_<?php echo $content_block_key; ?>"
            data-block="<?= $layout; ?>"
            data-id="<?= $content_block_key; ?>"
              >
            <?php include($file); ?>
          </div>
      <?php
        elseif( WP_DEBUG === true ) :
          echo "<p>File not found: " . $file . '</p>';
        endif;
    endforeach; ?>
</div>
