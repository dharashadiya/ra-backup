<?php 
    $logo_slider = get_field('brand_logos', 'options'); 
    $heading = get_field('heading', 'options'); 
?>

<div class="logo-slider">
    <h4 class="logo-slider__heading"><?php echo $heading; ?></h4>
    <div class="o-wrapper">
        <div class="logo-slider__slider-wrap">
            <div class="carousel">
                <?php foreach($logo_slider as $logo) : ?>
                    <?php $icon = $logo['logo']; ?>
                    <div class="logo-slider__slider ">
                        <img class="logo-slider__image" src="<?php echo $icon['url']; ?>">
                    </div>
                <?php endforeach; ?>
            </div>
            <button class="slick-prev">
                <?php echo svgicon('left-arrow', '0 0 22 20', 'logo-slider__left-arrow-svg'); ?>
            </button>
            <button class="slick-next">
                <?php echo svgicon('right-arrow', '0 0 22 20', 'logo-slider__right-arrow-svg '); ?>
            </button>
        </div>
    </div>
</div>