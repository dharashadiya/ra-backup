<?php get_header(); ?>
<div class="c-home">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php get_template_part( 'partials/flexible-content' ); ?>
			<?php get_template_part( 'partials/page-banner' ); ?>
			<?php get_template_part( 'partials/home-partials/smart-lives-section' ); ?>
			<?php get_template_part( 'partials/home-partials/contact-section' ); ?>
			<?php get_template_part( 'partials/home-partials/automation-section' ); ?>
			<?php get_template_part( 'partials/automation-list' ); ?>
			<?php get_template_part( 'partials/home-partials/customer-section' ); ?>
		</article>
	<?php endwhile; ?>
</div>

<?php get_footer(); ?>