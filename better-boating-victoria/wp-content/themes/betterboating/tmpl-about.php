<?php
/*
 * Template Name: About Page
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="c-about js-change-stage">
    <div class="o-wrapper o-wrapper--small">
        <div class="c-about__head">
            <div class="c-about__content">
                <div class="c-breadcrumb c-breadcrumb--dark-to-white">
                    <?php if(function_exists('bcn_display')) {
                        bcn_display();
                    }?>
                </div>
                <h1 class="heading"><?php the_title() ?></h1>
                <div class="intro-para">
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="c-about__lighthouse">
                <img class="lazy" data-src="<?php echo ASSETS ?>/img/BetterBoating-Lighthouse-Animated.gif" alt="Light house">
            </div>
        </div>
        <?php if (have_rows('link_pages')) : ?>
            <div class="c-about__links">
                <?php $i = 1; 
                $animateCounter = 0; ?>
                <div class="o-layout o-layout--med">
                    <?php while (have_rows('link_pages')) :  the_row()?>
                        <?php $page = get_sub_field('page'); ?>
                        <div class="o-layout__item u-1/3@tabletWide u-1/2@tablet o-animateup u-animation-delay-<?php echo $animateCounter ?>">
                            <a class="c-about__tile" href="<?php the_permalink($page->ID) ?>">
                                <div class="c-about__boat">
                                    <img class="c-about__boat-<?php echo $i ?>" src="<?php echo ASSETS ?>/img/boat-<?php echo $i ?>.png" alt="a boat">
                                </div>
                                <h2 class="c-about__tile-title heading"><?php echo $page->post_title ?></h2>
                            </a>
                        </div>
                        <?php $i++ ?>
                        <?php $animateCounter++ ?>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="c-about__fund">
            <div class="o-layout">
                <div class="o-layout__item u-1/3@mobileLandscape">
                    <div class="c-about__bank o-animateup">
                        <img class="lazy" data-src="<?php echo ASSETS ?>/img/bank@2x.png" alt="Bank">
                    </div>
                </div>
                <div class="o-layout__item u-2/3@mobileLandscape o-animateup u-animation-delay-0">
                    <?php $content = get_field('fund_content') ?>
                    <h2 class="heading large-mob o-animateup u-animation-delay-1"><?php the_field('fund_heading') ?></h2>
                    <div class="c-about__fund-content intro-para o-animateup u-animation-delay-2">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_template_part('partials/latest-news') ?>
<?php endwhile; ?>

<?php get_footer(); ?>
