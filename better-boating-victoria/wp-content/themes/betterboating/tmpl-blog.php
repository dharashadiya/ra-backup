<?php
/*
 * Template Name: Blogs Page
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<?php 
    $news = new WP_Query(array(
        'status' => 'published'
    ));
?>
<div class="c-news js-change-stage">
	<?php 
		$categories = get_categories();
		$image = get_the_post_thumbnail_url();
	?>
	<div class="c-news__top-spacer"></div>
	<div class="c-news__stage" data-src="<?php echo $image ?>"><div class="c-news__stage-bg cover"></div>
	</div>
	<div class="o-wrapper o-wrapper--small">
		<div class="c-news__content o-animateup">
			<div class="c-news__breadcrumb">
				<div class="c-breadcrumb c-breadcrumb--dark">
					<?php if(function_exists('bcn_display')) {
						bcn_display();
					}?>
				</div>
			</div>
			<h1 class="heading"><?php the_title() ?></h1>
			<div class="intro-para"><?php the_content() ?></div>
		</div>
		<div class="c-news__filters">
			<div class="c-filters o-animateup">
				<div class="c-filters__filter-set">
					<div class="c-filters__single js-filter js-single-select current" data-filter="all">
						View All
					</div>
					<?php foreach ($categories as $category) : ?>
						<?php if ($category->slug != "uncategorized")  :?>
						<div class="c-filters__single js-filter js-single-select" data-filter="<?php echo $category->slug ?>">
							<?php echo $category->name ?>
						</div>
						<?php endif; endforeach; ?>
				</div>
			</div>
		</div>
		<div class="c-news__grid">
			<div class="o-layout o-layout--med">
				<?php $animateCounter2 = 0; ?>
				<?php $i = 0; ?>
				<?php foreach ($news->posts as $single_post) : ?>
					<?php 
					$image = get_the_post_thumbnail_url($single_post->ID);
					$category = get_the_category($single_post);
					
					?>
					<div class="o-layout__item u-1/3@tablet u-1/2@mobileLandscape js-filter-item" data-filter="all <?php echo $category[0]->slug ?>">
						<div class="c-latest-news__tile-wrap">
							<a href="<?php echo get_the_permalink($single_post) ?>" class="c-latest-news__tile" data-src="<?php echo $image ?>">
								<div class="c-latest-news__tile-content">
									<h3><?php echo $single_post->post_title ?></h3>
									<div class="c-latest-news__tile-date">
										<?php echo get_the_date('j F, Y', $single_post) ?>
									</div>
									<div class="c-latest-news__tile-category">
										Category:
									</div>
								</div>
								<div class="c-latest-news__bg cover"></div>
							</a>
							<a class="c-latest-news__category" href="/news/?filter=<?php echo clean_string($category[0]->name) ?>"><span><?php echo $category[0]->name ?></span></a>
						</div>
					</div>
					<?php $animateCounter2++;
					$i++;
						if ($animateCounter2 > 2) {
							$animateCounter2 = 0;
					} ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>

<?php endwhile; ?>

<?php get_footer(); ?>