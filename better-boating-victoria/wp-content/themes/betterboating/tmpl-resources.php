<?php
/*
 * Template Name: Resouces Page
 */
?>
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php 
$image = get_the_post_thumbnail_url();
?>
<div class="c-resources">
    <div class="c-resources__stage">
        <div class="c-resources__stage-bgimage cover lax" data-src="<?php echo $image ?>" data-lax-translate-y="0 0, vh 250" data-lax-anchor="self"></div>
        <div class="c-internal__stage-bg cover"></div>    
        <div class="o-wrapper o-wrapper--small pos-rel w-100 h-100">
            <div class="c-resources__breadcrumb">
                <div class="c-breadcrumb">
                    <?php if(function_exists('bcn_display')) {
                        bcn_display();
                    }?>
                </div>
            </div>
        </div>
    </div>
    <div class="o-wrapper o-wrapper--small js-change-stage">
        <div class="c-resources__top">
            <div class="c-resources__breadcrumb c-resources__breadcrumb--mobile">
                <div class="c-breadcrumb c-breadcrumb--dark">
                    <?php if(function_exists('bcn_display')) {
                        bcn_display();
                    }?>
                </div>
            </div>
            <div class="c-resources__content o-animateup">
                <h1 class="heading"><?php the_title() ?></h1>
                <div class="intro-para"><?php the_content() ?></div>
            </div>
            <div class="c-resources__boat o-animateup u-animation-delay-1">
                <img class="c-resources__boat-image" data-src="<?php echo ASSETS ?>/img/boat-3.png" alt="a boat">
            </div>
        </div>
        <?php  if( have_rows('resources') ): ?>
            <div class="c-resources__list o-animateup u-animation-delay-2">
                <?php while ( have_rows('resources') ) : the_row();  ?>
                    <?php 
                        $image = get_sub_field('image');
                        $file = get_sub_field('file');
                        $link = get_sub_field('link');
                    ?>
                    <div class="c-resources__single o-animateup ">
                        <div class="c-resources__image-wrap">
                            <img class="lazy" data-src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>">
                        </div>
                        <div class="c-resources__single-content">
                            <h3 class="large-mob heading-mob"><?php the_sub_field('title') ?></h3>
                            <p><?php the_sub_field('content') ?></p>
                            <?php if ($file) : ?>
                                <a download href="<?php echo $file['url'] ?>" class="o-btn"><?php the_sub_field('file_text') ?></a>
                            <?php else : ?>
                                <a target="_blank" href="<?php echo $link['url'] ?>" class="o-btn"><?php echo $link['title'] ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile ?>
            </div>
        <?php endif ?>
    </div>
</div>
<?php endwhile; ?>
<?php set_query_var('mod', 'grey') ?>
<?php get_template_part('partials/latest-news') ?>

<?php get_footer(); ?>
