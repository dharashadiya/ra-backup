<?php
/*
 * Template Name: Contact Page
 */
?>

<?php get_header(); ?>

<?php 
$facebook = get_field('facebook', 'options');
$instagram = get_field('instagram', 'options');
$twitter = get_field('twitter', 'options');
$address = get_field('address', 'options');
$email = get_field('contact_email', 'options');
$image = get_the_post_thumbnail_url();
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="c-contact">
    <div class="c-contact__feature">
        <div class="c-contact__feature-bgimage cover lax" data-src="<?php echo $image ?>" data-lax-translate-y="0 0, vh 250" data-lax-anchor="self"></div>
        <div class="c-contact__feature-bg cover"></div>
        <div class="o-wrapper o-wrapper--small w-100">
            <div class="c-contact__breadcrumb">
                <div class="c-breadcrumb">
                    <?php if(function_exists('bcn_display')) {
                        bcn_display();
                    }?>
                </div>
            </div>
        </div>
    </div>
    <div class="c-contact__main o-wrapper o-wrapper--small js-change-stage">
        <div class="o-layout o-layout--flush">
            <div class="o-layout__item u-3/4@laptop">
                <div class="c-contact__content">
                    <div class="c-contact__breadcrumb c-contact__breadcrumb--mobile">
                        <div class="c-breadcrumb c-breadcrumb--dark">
                            <?php if(function_exists('bcn_display')) {
                                bcn_display();
                            }?>
                        </div>
                    </div>
                    <h1 class="heading"><?php the_title() ?></h1>
                    <div class="intro-para">
                        <?php the_content() ?>
                    </div>
                    <div class="c-contact__form-wrap">
                        <?php echo do_shortcode('[contact-form-7 id="8" title="Contact Page Form"]') ?>
                    </div>
                </div>
            </div>
            <div class="o-layout__item u-1/4@laptop">
                <aside class="c-contact__aside">
                    <h3 class="heading">
                        <?php the_field('sidebar_heading') ?>
                    </h3>
                    <div class="c-contact__address">
                        <a target="_blank" href="https://www.google.com/maps?q=<?php echo $address ?>"><?php echo $address ?></a>
                    </div>
                    <div class="c-contact__email">
                        <a target="_blank" href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
                    </div>
                    <div class="c-contact__socials">
                        <?php if ($facebook) : ?>
                            <a href="<?php echo $facebook ;?>" target="_blank">
                                <?php svgicon('Facebook-Icon', '0 0 12.38 24.01'); ?>
                            </a>
                        <?php endif; ?>
                        <?php if ($instagram) : ?>
                            <a href="<?php echo $instagram ;?>" target="_blank">
                                <?php svgicon('Insta-Icon', '0 0 29.7 29.67'); ?>
                            </a>
                        <?php endif; ?>
                        <?php if ($twitter) : ?>
                            <a href="<?php echo $twitter ;?>" target="_blank">
                                <?php svgicon('Twitter-Icon', '0 0 26.42 21.83'); ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</div>
    <?php endwhile; ?>

<?php get_footer(); ?>

<!-- Form -->

<!-- <div class="o-layout">
    <div class="o-layout__item u-1/2">
        [text* first-name placeholder "First Name"]
    </div>
    <div class="o-layout__item u-1/2">
        [text* last-name placeholder "Last Name"]
    </div>
    <div class="o-layout__item">
        [email* email placeholder "Email Address"]
    </div>
    <div class="o-layout__item">
        [textarea* message placeholder "Message"]
    </div>
    [submit class:o-btn class:o-btn--secondary class:c-contact__submit "Send Message"]
</div> -->