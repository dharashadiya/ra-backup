<?php
/*
 * Template Name: Terms Internal Page
 */
?>
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php 
$image = get_the_post_thumbnail_url();
?>
<div class="c-internal c-internal--terms">
    <div class="o-wrapper o-wrapper--small">
        <div class="o-layout o-layout--flush js-change-stage">
            <div class="o-layout__item u-18/25@tablet">
                <div class="c-internal__breadcrumb c-internal__breadcrumb--mobile">
                    <div class="c-breadcrumb c-breadcrumb--dark">
                        <?php if(function_exists('bcn_display')) {
                            bcn_display();
                        }?>
                    </div>
                </div>
                <div class="c-internal__breadcrumb c-internal__breadcrumb--terms">
                    <div class="c-breadcrumb c-breadcrumb--dark">
                        <?php if(function_exists('bcn_display')) {
                            bcn_display();
                        }?>
                    </div>
                </div>
                <div class="c-internal__content c-internal__content--terms">
                    <h1 class="heading"><?php the_title() ?></h1>
                    <div class="intro-para">
                        <?php the_content() ?>
                    </div>
                    <div class="c-internal__second-content">
                        <h2 class="heading large"><?php the_field('second_heading') ?></h2>
                        <div class="intro-para"><?php the_field('second_content') ?></div>
                    </div>
                    <div class="c-internal__next">
                        <?php if (have_rows('page_links')) : ?>
                            <?php while (have_rows('page_links')) :  the_row()?>
                                <?php $page = get_sub_field('page');
                                if ($page->ID === get_the_ID()) {
                                    continue;
                                }?>
                                <a class="o-arrow" href="<?php echo get_the_permalink($page->ID) ?>">
                                    <?php echo $page->post_title ?>
                                </a>
                            <?php endwhile; ?>
                         <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="o-layout__item u-7/25@tablet sticky">
                <aside class="c-internal__side">
                    <?php if (have_rows('page_links')) : ?>
                        <div class="c-internal__links">
                            <h4>In this section: </h4>
                            <div class="c-sidelink">
                                <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'terms-menu',
                                        'container' => false,
                                        'menu_class' => 'c-sidelink__list',
                                    ));
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="c-internal__ramp">
                        <div class="c-ramp-tile">
                            <?php $ramp = get_field('ramp_upgrades_tile', 'options') ?>
                            <h3 class="heading"><?php echo $ramp['heading'] ?></h3>
                            <p><?php echo $ramp['content'] ?></p>
                            <a href="<?php echo $ramp['link']['url'] ?>" class="o-btn o-btn--secondary o-btn--med o-btn--no-border o-btn--no-shadow"><?php echo $ramp['link']['title'] ?></a>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>

<?php get_footer(); ?>
