<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<?php 
	$image = get_the_post_thumbnail_url();
	$categories = get_the_category();
	$ramps = new WP_Query(array(
		'posts_per_ramp'=> -1,
		'post_type'	=> 'ra-ramps',
		'status' => 'published',
		'orderby' => 'menu_order',
		'order'   => 'ASC',
	));
?>
<div class="c-single-ramp js-change-stage">
	<div class="o-wrapper o-wrapper--small pos-rel">
		<div class="o-layout">
			<div class="o-layout__item u-18/25@tablet">
				<div class="c-single-ramp__breadcrumb">
					<div class="c-breadcrumb c-breadcrumb--dark">
						<?php if(function_exists('bcn_display')) {
							bcn_display();
						}?>
					</div>
				</div>
				<div class="c-single-ramp__content">
					<h1 class="heading"><?php the_title(); ?></h1>
					<div class="intro-para">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="o-layout__item u-7/25@tablet">
				<div class="c-single-ramp__aside">
					<div class="c-single-ramp__links">
						<h4>In this section: </h4>
						<div class="c-sidelink">
							<?php foreach ($ramps->posts as $ramp) : ?>
								<a class="c-sidelink__item <?php echo($ramp->ID === get_the_id() ? "current" : "") ?>" href="<?php echo get_the_permalink($ramp->ID) ?>">
									<?php echo $ramp->post_title ?>
								</a>
							<?php endforeach; ?>
						</div>
					</div>
					<?php if(get_field('project_name')) :?>
						<div class="c-single-ramp__info-tile">
							<div>
								<h4>Project Name</h4>
								<p><?php the_field('project_name') ?></p>
								<h4>Location</h4>
								<p><?php the_field('location') ?></p>
							</div>
							<div>
								<h4>Objectives</h4>
								<?php the_field('objectives') ?>
								<h4>Timeline</h4>
								<?php the_field('timeline') ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<a class="js-to-top c-single-ramp__top-link" href="javascript:void(0)">Return to top</a>
		<?php 
			$next_post = get_next_post();
			if ($next_post){
				$next_post_link = get_the_permalink($next_post);
			} else {
				$next_post = end($ramps->posts);
				if ($next_post->ID === get_the_ID()) {
					$next_post = $ramps->posts[0];
				}
				$next_post_link = get_the_permalink($next_post->ID);
			}
		?>
		<a href="<?php echo $next_post_link ?>" class="o-arrow c-single-ramp__next-link"><?php echo $next_post->post_title ?> Upgrade</a>	
	</div>
</div>
<?php endwhile; ?>
<?php get_template_part('partials/ramp-upgrades') ?>

<?php get_footer(); ?>