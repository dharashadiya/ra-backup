<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<?php 
	$partnership = get_field('partnership_section');
	$about = get_field('what_we_do_section');
	$slides = get_field('slides');
	$popup = get_field("popup");
	$popup = get_field("popup");
	$enable_popup = $popup['enable_popup'];
?>
<div class="c-home">
	<div class="c-home__slider">
		<div class="c-home-slider">
			<div class="o-wrapper pos-rel">
				<div class="c-home-slider__content-slider">
					<?php $i = 0 ?>
					<?php foreach ($slides as $slide) : ?>
					<?php if ($i === 0) : ?>
						<div class="c-home-slider__content js-change-stage">
					<?php else : ?>
						<div class="c-home-slider__content hide">
					<?php endif; ?>
							<div class="c-home-slider__content-inner">
								<?php if ($i === 0) : ?>
									<h1 class="alt-head heading o-animateup"><?php echo $slide['heading'] ?></h1>
								<?php else : ?>
									<h2 class="alt-head heading o-animateup u-animation-delay-0"><?php echo $slide['heading'] ?></h2>
								<?php endif; ?>
								<div class="intro-para o-animateup u-animation-delay-0">
									<p><?php echo $slide['content'] ?></p>
								</div>
								<a class="o-btn o-animateup u-animation-delay-1" href="<?php echo $slide['link']['url'] ?>" target="<?php echo $slide['link']['target'] ?>" <?php echo ($slide['link']['target'] ? 'rel="noopener"' : '') ?>><?php echo $slide['link']['title'] ?></a>
							</div>
						</div>
						<?php $i++ ?>
					<?php endforeach; ?>
				</div>
				<div class="c-home-slider__scroll js-to-content">
					<?php svgicon('arrow-button', '0 0 18 18'); ?>
					Scroll down for more
				</div>
				<div class="c-home-slider__dots"></div>
			</div>
			<div class="c-home-slider__swirl">
			</div>
			<div class="c-home-slider__image-slider">
				<?php foreach ($slides as $slide) : ?>
					<div>
						<div class="c-home-slider__bg lax" data-src="<?php echo $slide['image']['url'] ?>" data-lax-translate-y="0 0, vh 250" data-lax-anchor="self">
							<?php if ($slide['central_image']['url']) :?>
								<img class="c-home-slider__feature" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo $slide['central_image']['url'] ?>" alt="<?php echo $slide['central_image']['title'] ?>">
							<?php endif; ?>
						</div>
						<?php if ($slide['mobile_image']['url']) :?>
							<div class="c-home-slider__mob-bg lax" data-src="<?php echo $slide['mobile_image']['url'] ?>" data-lax-translate-y="0 0, vh 250" data-lax-anchor="self">
								<?php if ($slide['central_image']['url']) :?>
									<img class="c-home-slider__feature" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo $slide['central_image']['url'] ?>" alt="<?php echo $slide['central_image']['title'] ?>">
								<?php endif; ?>
							</div>
						<?php else : ?>
							<div class="c-home-slider__mob-bg lax" data-src="<?php echo $slide['image']['url'] ?>" data-lax-translate-y="0 0, vh 250" data-lax-anchor="self">
								<?php if ($slide['central_image']['url']) :?>
									<img class="c-home-slider__feature" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo $slide['central_image']['url'] ?>" alt="<?php echo $slide['central_image']['title'] ?>">
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	<div class="c-home__about" id="content">
		<div class="o-wrapper pos-rel">
			<div class="c-home__about-inner">
				<div class="c-home__drawings" id="js-scene-1">
					<!-- <div class="c-home__speed-boat" data-depth="0.2">
						<img data-depth="0.2" class="c-home__speed-boat-background lazy" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo ASSETS ?>/img/BetterBoating-Illustrations-Parralax-Buoy-Background.png" alt="A Buot">
						<img class="c-home__speed-boat-image lazy" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo ASSETS ?>/img/BetterBoating-Illustrations-Parralax-Boat-3-Foreground.png" alt="A Speed Boat">
					</div>
					<div class="c-home__buoy" data-depth="0.3" id="">
						<img data--depth="0.2" class="c-home__buoy-background lazy" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo ASSETS ?>/img/BetterBoating-Illustrations-Parralax-Buoy-Background.png" alt="A Buot">
						<img class="c-home__buoy-image lazy" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo ASSETS ?>/img/BetterBoating-Illustrations-Parralax-Buoy-Foreground.png" alt="A Buoy">
					</div>
					<div class="c-home__small-boat" data-depth="0.5" id="">
						<img data--depth="0.2" class="c-home__small-boat-background lazy" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo ASSETS ?>/img/BetterBoating-Illustrations-Parralax-Buoy-Background.png" alt="A Buot">
						<img class="c-home__small-boat-image lazy" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo ASSETS ?>/img/BetterBoating-Illustrations-Parralax-Fishing-Boat-Foreground.png" alt="A Fishing Boat">
					</div> -->
					<!-- updated following -->
					<div class="c-home__hazard" data-depth="0.2">
						<!-- <img data-depth="0.2" class="c-home__speed-boat-background lazy" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo ASSETS ?>/img/BetterBoating-Illustrations-Parralax-Buoy-Background.png" alt="A Buot"> -->
						<img class="c-home__hazard-image lazy" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo ASSETS ?>/img/hazard@2x.png" alt="A Speed Boat">
					</div>
				</div>
				<div class="c-home__about-content">
					<h2 class="large large-mob heading o-animateup"><?php echo $about['heading'] ?></h2>
					<div class="intro-para o-animateup u-animation-delay-0">
						<p><?php echo $about['content'] ?></p>
					</div>
					<a class="o-btn desktop o-animateup u-animation-delay-1" href="<?php echo $about['link']['url'] ?>" target="<?php echo $about['link']['target'] ?>" <?php echo ($slide['link']['target'] ? 'rel="noopener"' : '') ?>><?php echo $about['link']['title'] ?></a>
					<a class="o-btn mobile o-animateup u-animation-delay-1" href="<?php echo $about['link']['url'] ?>" target="<?php echo $about['link']['target'] ?>" <?php echo ($slide['link']['target'] ? 'rel="noopener"' : '') ?>>Learn More</a>
				</div>
			</div>
		</div>
	</div>
	<div class="c-home__partnership js-change-stage lazy">
		<div class="c-home__partnership-bgimage cover lax" data-src="<?php echo ASSETS ?>/img/BetterBoating-Desktop-BoatingVic-Banner.jpg" data-src="<?php echo $slide['image']['url'] ?>" data-lax-translate-y="vh 0, -elh 200" data-lax-anchor="self"></div>
		<div class="o-wrapper">
			<div class="c-home__partnership-inner o-animateup">
				<div class="c-home__partnership-content">
					<h2 class="heading o-animateup u-animation-delay-0"><?php echo $partnership['heading'] ?></h2>
					<div class="intro-para o-animateup u-animation-delay-1">
						<p><?php echo $partnership['content'] ?></p>
					</div>
				</div>
				<div class="c-home__boating-vic o-animateup u-animation-delay-1">
					<img class="lazy" data-src="<?php echo ASSETS ?>/img/boating-vic-color@2x.png" alt="Boating VIC logo">
					<p><?php echo $partnership['boatingvic_content'] ?></p>
					<a class="o-btn o-btn--full" href="<?php echo $partnership['boatingvic_link']['url'] ?>" target="<?php echo $partnership['boatingvic_link']['target'] ?>" <?php echo ($slide['link']['target'] ? 'rel="noopener"' : '') ?>><?php echo $partnership['boatingvic_link']['title'] ?></a>
				</div>
				<div class="c-home__partnership-mobile">
					<img class="lazy" data-src="<?php echo ASSETS ?>/img/boating-vic@2x.png" alt="Boating VIC logo">
					<p><?php echo $partnership['content'] ?></p>
					<a class="o-btn o-btn--full-desktop" href="<?php echo $partnership['boatingvic_link']['url'] ?>" target="<?php echo $partnership['boatingvic_link']['target'] ?>" <?php echo ($slide['link']['target'] ? 'rel="noopener"' : '') ?>><?php echo $partnership['boatingvic_link']['title'] ?></a>
				</div>
			</div>
		</div>
	</div>
	<div class="c-home__upgrades">
		<?php get_template_part('partials/ramp-upgrades') ?>
	</div>
	<div class="c-home__map">
		<?php set_query_var('map_mod', 'home') ?>
		<?php get_template_part('partials/ramp-map') ?>
	</div>
	<div class="c-home__news">
		<?php get_template_part('partials/latest-news'); ?>
	</div>

	<!--MODAL -->
	<?php if( isset($enable_popup) && $enable_popup ) : ?>
		<div id="list-builder"></div>
		<div id="popup-box">
			<div id="popup-box-content"> 
				<span id="popup-close"><?php svgicon('close', '0 0  32 33'); ?></span>
				<div>
					<h1 class="c-home__title"><?php echo $popup['title']; ?></h1>
					<div class="c-home__content"><?php echo $popup['content']; ?></div>
					<a class="c-home__link o-btn o-btn--tertiary" href="<?php echo $popup['link']['url']; ?>"><?php echo $popup['link']['title']; ?></a>
				</div>
			</div>
		</div>
	<?php endif; ?>

</div>

<?php endwhile; ?>
<?php get_footer(); ?>