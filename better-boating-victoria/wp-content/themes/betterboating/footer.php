</div><!-- .c-content -->

<?php 
    $ramps = new WP_Query(array(
        'posts_per_page'=> -1,
        'post_type'	=> 'ra-ramps',
		'status' => 'published',
		'orderby' => 'title',
		'order'   => 'ASC',
	));
	$facebook = get_field('facebook', 'options');
	$instagram = get_field('instagram', 'options');
	$twitter = get_field('twitter', 'options');
	$address = get_field('address', 'options');
	$email = get_field('contact_email', 'options');
	$links = get_field('footer_department_links', 'options');
	$newsletter_active = get_field('newsletter_active', 'options');
?>
<footer class="c-footer">
	<div class="o-wrapper">
		<div class="c-footer__inner-wrap">
			<div class="c-footer__left">
				<h4>Connect With Us</h4>
				<a href="mailto:<?php the_field('contact_email', 'options') ?>"><?php the_field('contact_email', 'options') ?></a>
				<div class="c-footer__socials">
					<?php if ($facebook) : ?>
						<a aria-label="Facebook Link" href="<?php echo $facebook ;?>" target="_blank" rel="noopener">
							<?php svgicon('Facebook-Icon', '0 0 12.38 24.01'); ?>
						</a>
					<?php endif; ?>
					<?php if ($instagram) : ?>
						<a aria-label="Instagram Link" href="<?php echo $instagram ;?>" target="_blank" rel="noopener">
							<?php svgicon('Insta-Icon', '0 0 29.7 29.67'); ?>
						</a>
					<?php endif; ?>
					<?php if ($twitter) : ?>
						<a aria-label="Twitter Link" href="<?php echo $twitter ;?>" target="_blank" rel="noopener">
							<?php svgicon('Twitter-Icon', '0 0 26.42 21.83'); ?>
						</a>
					<?php endif; ?>
				</div>
				<?php if ($newsletter_active) :?>
					<div class="c-footer__newsletter">
						<h4>Sign up to our Newsletter</h4>
						<?php echo do_shortcode('[contact-form-7 id="167" title="Newsletter Subscription"]') ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="c-footer__quick-links">
				<h4>Quick Links</h4>
				<?php
					wp_nav_menu( array(
						'theme_location' => 'secondary-menu',
						'container' => false,
						'menu_id' => 'quick_nav'
					));
				?>
			</div>
			<div class="c-footer__upgrades">
				<h4>Current</br>Ramp upgrades</h4>
				<ul>
					<?php foreach ($ramps->posts as $ramp) : ?>
					<li>
						<a href="<?php echo get_the_permalink($ramp) ?>">
							<?php echo $ramp->post_title ?>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div  class="c-footer__brand-logo">
				<a class="c-footer__bbv-logo" href="/">
					<img class="c-footer__logo lazy" data-src="<?php echo ASSETS ?>/img/logo-color@2x.png" alt="Better Boating Logo">
				</a>
				<a class="c-footer__vfa-logo" href="https://vfa.vic.gov.au/" target="_blank">
					<img class="c-footer__logo lazy" data-src="<?php echo ASSETS ?>/img/VFA-Logo.png" alt="Victorian Fisheries Authority Logo">
				</a>
			</div>
		</div>
		<div class="c-footer__copy-wrap">
			<div class="c-footer__terms">
				<a href="/terms-and-conditions/">Terms&nbsp;of&nbsp;Use</a>&nbsp;|&nbsp;<a href="/privacy-policy/">Privacy</a><span class="c-footer__built-by">&nbsp;|&nbsp;Built&nbsp;by&nbsp;<a class="c-footer__built-by" href="https://rockagency.com.au/">Rock&nbsp;Agency</a>
				</span>
			</div>
			<div class="c-footer__copy">
				<p>&copy;&nbsp;Copyright&nbsp;Better&nbsp;Boating&nbsp;Victoria&nbsp;<?php echo date("Y"); ?>.</p>
			</div>
		</div>
	</div>
	<?php if ($links) : ?>
		<div class="c-footer__departments">
			<div class="o-wrapper">
				<div class="c-footer__state">
					<a href="https://www.vic.gov.au/" target="_blank" rel="noopener">
						<img src="<?php echo ASSETS ?>/img/vic-logo-black@2x.png" alt="Victorian State Government Logo">
					</a>		
				</div>
				<div class="c-footer__departments-inner">
					<?php foreach ($links as $link) : ?>
						<a rel="noopener" href="<?php echo $link['link']['url'] ?>" target="<?php echo $link['link']['target'] ?>"><?php echo $link['link']['title'] ?></a>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</footer>
<?php wp_footer(); ?>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh8t0xulUFZIvR28eg4smE_s61zloCJkw&callback=initMap"
    async defer></script> -->
</body>
</html>

<!-- News letter form -->

<!-- <div class="c-newsletter">
	<div class="c-newsletter__email">
		[email* email placeholder "Enter your email address ..."]
	</div>
	[submit " " class:c-newsletter__send]
</div> -->