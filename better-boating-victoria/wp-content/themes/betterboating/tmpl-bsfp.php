<?php
/*
 * Template Name: Boating Safety & Facilities Program (BSFP) Page
 */
?>
<?php
/*
 * Template Name: Terms Internal Page
 */
?>
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php 
$image = get_the_post_thumbnail_url();
?>
<div class="c-resources__stage">
    <div class="c-resources__stage-bgimage cover lax" data-src="<?php echo $image ?>" data-lax-translate-y="0 0, vh 250" data-lax-anchor="self"></div>
    <div class="c-internal__stage-bg cover"></div>    
    <div class="o-wrapper o-wrapper--small pos-rel w-100 h-100">
        <div class="c-resources__breadcrumb">
            <div class="c-breadcrumb">
                <?php if(function_exists('bcn_display')) {
                    bcn_display();
                }?>
            </div>
        </div>
    </div>
</div>
<div class="o-wrapper o-wrapper--small">
    <div class="o-layout o-layout--flush js-change-stage">
        <div class="o-layout__item u-20/25@tablet">
            
            
            <div class="c-internal__content c-internal__content--terms">
                <h1 class="heading"><?php the_title() ?></h1>
                <div class="intro-para">
                    <?php the_content() ?>
                </div>
            </div>
        </div>
        <div class="o-layout__item u-5/25@tablet sticky">
            <aside class="c-internal__side">   
            <div class="c-internal__hazard" data-depth="0.2">
                    <img class="c-internal__hazard-image lazy" src="<?php echo ASSETS ?>/img/1x1.trans.gif" data-src="<?php echo ASSETS ?>/img/hazard@2x.png" alt="A Speed Boat">
                </div>
            </aside>
        </div>
    </div>
</div>
<div class="c-home__news c-latest-news--grey">
        <?php get_template_part('partials/latest-news'); ?>
</div>


<?php endwhile; ?>

<?php get_footer(); ?>


		