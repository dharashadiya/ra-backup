<?php
/*
 * Template Name: Library Page
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php 
$image = get_the_post_thumbnail_url();
?>
<div class="c-library">
    <div class="c-internal__stage c-internal__stage--library" data-src="<?php echo $image ?>">
        <div class="c-internal__stage-bg cover"></div>
        <div class="o-wrapper o-wrapper--small">
            <div class="c-internal__breadcrumb">
                <div class="c-breadcrumb">
                    <?php if(function_exists('bcn_display')) {
                        bcn_display();
                    }?>
                </div>
            </div>
        </div>
    </div>
    <?php  ?>
    <div class="o-wrapper o-wrapper--small js-change-stage">
        <div class="c-library__head">
            <h1 class="heading"><?php the_title() ?></h1>
            <div class="c-library__head-content">
                <?php the_content(); ?>
            </div>
        </div>
        <hr/>
        <?php 
            $reports = get_field('reports');
        ?>
        <div class="c-library__report-wrap">
            <div class="o-layout o-layout--reports">
                <?php foreach($reports as $report) :?>
                    <div class="o-layout__item u-1/2@tablet">
                        <div class="c-report">
                            <div class="c-report__top" data-src="<?php echo $report['image']['url'] ?>">
                                <div class="c-report__grad"></div>
                                <div class="c-report__top-content">
                                    <div class="c-report__sub-title">
                                        <?php echo $report['sub_title'] ?>
                                    </div>
                                    <h4 class="c-report__title">
                                        <?php echo $report['title'] ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="c-report__bottom">
                                <div class="c-report__content">
                                    <?php echo $report['blurb'] ?>
                                </div>
                                <div class="c-report__button">
                                    <a download href="<?php echo $report['download_link']['url'] ?>" class="o-btn"><?php echo $report['download_link']['title'] ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>
<?php set_query_var('mod', 'grey') ?>
<?php get_template_part('partials/latest-news') ?>

<?php get_footer(); ?>
