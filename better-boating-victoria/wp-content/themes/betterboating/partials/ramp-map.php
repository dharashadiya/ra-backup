<?php 
    $markers = get_field('markers', 'options');
    wp_register_script( 'map_markers', false );
    wp_localize_script( 'map_markers', 'mapMarkers', $markers );
    wp_enqueue_script( 'map_markers' );
    $mod = get_query_var('map_mod');
    $map_content = get_field('map_content', 'options');
    if ($mod) {
        $mod_class = $mod;
    } else  {
        $mod_class = "";
    }
?>
<div class="c-map__outer-wrap js-change-stage" id="mapload">
    <div class="c-map__content c-map__content--<?php echo $mod_class ?>">
        <div class="o-wrapper c-map__inner-wrap">
            <div class="c-map__content-wrap c-map__content-wrap--<?php echo $mod_class ?> o-animateup">
                <h1 class="heading"><?php echo $map_content['heading'] ?></h1>
                <div class="intro-para">
                    <p><?php echo $map_content['content'] ?></p>
                </div>
                <?php if($mod) : ?>
                    <a class="o-btn" href="<?php echo $map_content['link']['url'] ?>">View Free Boat Ramps</a>
                    <?php else : ?>
                    <a class="o-btn" href="<?php echo $map_content['link']['url'] ?>"><?php echo $map_content['link']['title'] ?></a>
                <?php endif ?>
            </div>
            <?php if(!$mod) : ?>
                <div class="c-map__scroll js-to-content">
                    <?php svgicon('arrow-button', '0 0 18 18'); ?>
                    Scroll down for more information
                </div>
            <?php endif ?>
        </div>
        <div class="c-map__mob-swirl">
            <?php svgicon('BetterBoating-Home-Mobile-Ramp-Curve-2', '0 0 375 217.14'); ?>
        </div>
    </div>
    <div id="map" class="c-map c-map--<?php echo $mod_class ?>"></div>
</div>