<?php 
    $latest_news_content = get_field('latest_news_content', 'options');
    $latest_news_articles = get_field('feature_news_posts', 'options');
    $mod = get_query_var('mod');
    if ($mod) {
        $mod_class = "c-latest-news--" . $mod;
    } else  {
        $mod_class = "";
    }
?>
<div class="c-latest-news <?php echo $mod_class ?> js-change-stage">
    <?php if(!$mod) : ?>
        <div class="c-latest-news__mob-swirl">
            <?php svgicon('BetterBoating-Home-Mobile-Curve-1', '0 0 375 201.02'); ?>
        </div>
    <?php endif; ?>
    <div class="o-wrapper o-wrapper--small">
        <div class="c-latest-news__content o-animateup">
            <h2 class="heading large large-mob"><?php echo $latest_news_content['heading'] ?></h2>
            <div class="intro-para">
                <p><?php echo $latest_news_content['content'] ?></p>
            </div>
        </div>
        <div class="o-layout o-layout--med">
            <?php $i = 0; ?>
            <?php $animateCounter = 0; ?>
            <?php foreach ($latest_news_articles as $single_post_array) : ?>
                <?php
                $single_post = $single_post_array['post'];
                $image = get_the_post_thumbnail_url($single_post->ID);
                $category = get_the_category($single_post);
                ?>
                <div class="o-layout__item u-1/3@tablet u-1/2@mobileLandscape <?php echo ($i === 2 ? "last" : "") ?> <?php echo ($i < 6 ? "o-animateup" : "")?> u-animation-delay-<?php echo $animateCounter ?>">
                    <div class="c-latest-news__tile-wrap">
                        <a alt="News link" href="<?php echo get_the_permalink($single_post) ?>" class="c-latest-news__tile lazy" data-src="<?php echo $image ?>">
                            <div class="c-latest-news__tile-content">
                                <h3><?php echo $single_post->post_title ?></h3>
                                <div class="c-latest-news__tile-date">
                                    <?php echo get_the_date('j F, Y', $single_post) ?>
                                </div>
                                <div class="c-latest-news__tile-category">
                                    Category:
                                </div>
                            </div>
                            <div class="c-latest-news__bg cover"></div>
                        </a>
                        <a class="c-latest-news__category" href="/news/?filter=<?php echo clean_string($category[0]->name) ?>"><span><?php echo $category[0]->name ?></span></a>
                    </div>
                </div>
                <?php 
                $i++  ;
                $animateCounter++;
                if ($animateCounter > 3) {
                    $animateCounter = 0;
                }
                ?>
            <?php endforeach; ?>
        </div>
        <a href="/news" class="o-btn c-latest-news__button">View All News Posts</a>
    </div>
</div>