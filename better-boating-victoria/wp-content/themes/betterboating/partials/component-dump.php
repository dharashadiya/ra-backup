<h2 class="alt-head">Alt Head</h2>
<h1>h1</h1>
<h2>h2</h2>
<h3>h3</h3>
<h4>h4</h4>
<h5>h5</h5>
<h6>h6</h6>

<h1>Heading Style</h1>
<p class="intro-para">Intro Para ipsum dolor sit, amet consectetur adipisicing elit. Harum ab eveniet et itaque necessitatibus esse, quasi consectetur, tempore nam illum officia vitae! Incidunt nulla natus, debitis perferendis ullam facilis necessitatibus.</p>
<p>Body para ipsum dolor sit amet consectetur adipisicing elit. Quaerat ducimus aspernatur vero a consequatur magni alias, tenetur numquam enim cupiditate voluptas quidem, dolores quae expedita laborum est delectus quo quasi.</p>
<h3>h3 Heading</h3>
<ul>
    <li>list item</li>
    <li>Lorem ipsum dolor sit amet consectetur, ad</li>
    <li>Aperiam laborum provident expedita corrupti maiores at,</li>
    <li>odit qui dolor, aspernatur repudiandae</li>
</ul>
<a href="#" class="o-btn">Primary</a>
<a href="#" class="o-btn o-btn--secondary">Secondary</a>
<a href="">Link style</a>
<a href="" class="o-arrow">Arrow link</a>
<h4>Forms</h4>
<input type="text" placeholder="empty text field">