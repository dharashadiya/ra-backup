<?php 
    $ramps = new WP_Query(array(
        'posts_per_page'=> -1,
        'post_type'	=> 'ra-ramps',
        'status' => 'published',
        'orderby' => 'menu_order',
		'order'   => 'ASC',
    ));
    $ramp_content = get_field('ramp_upgrades_footer', 'options');
?>

<div class="c-ramp-upgrades">
    <h2 class="heading large-mob large"><?php echo $ramp_content['heading'] ?></h2>
    <div class="c-ramp-upgrades__slider-wrap">
        <div class="c-ramp-upgrades__slider-arrows next"></div>
        <div class="c-ramp-upgrades__slider-arrows prev"></div>
        <div class="c-ramp-upgrades__slider">
            <?php foreach ($ramps->posts as $ramp) : ?>
                <?php 
                $image = get_the_post_thumbnail_url($ramp->ID);
                $stage = get_the_terms($ramp, 'build_stage');
                ?>
                <div class="c-ramp-upgrades__slide">
                    <a href="<?php echo get_the_permalink($ramp->ID) ?>">
                        <div class="c-ramp-upgrades__image-wrap">
                            <img data-src="<?php echo $image ?>" alt="<?php echo $ramp->post_title ?>" class="c-ramp-upgrades__slide-image">
                        </div>   
                        <h3><?php echo $ramp->post_title ?></h3>
                        <span class="c-ramp-upgrades__progress">
                            <?php echo $stage[0]->name ?>
                        </span>
                        <p><?php echo wp_trim_words( $ramp->post_content, 16, '...' ) ?></p>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <a href="<?php echo $ramp_content['link']['url'] ?>" class="o-btn"><?php echo $ramp_content['link']['title'] ?></a>
</div>