<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<?php
$image = get_field('post_header_image');
$categories = get_the_category();
?>
<div class="c-post js-change-stage">
	<div class="o-wrapper o-wrapper--small">
		<?php if ($image) : ?>
			<div class="c-post__feature" data-src="<?php echo $image['url'] ?>"></div>
		<?php endif; ?>
		<div class="o-layout">
			<div class="o-layout__item u-18/25@tablet o-animateup">
				<div class="c-post__breadcrumb">
					<div class="c-breadcrumb c-breadcrumb--dark">
						<?php if(function_exists('bcn_display')) {
							bcn_display();
						}?>
					</div>
				</div>
				<div class="c-post__content o-animateup u-animation-delay-1">
					<h1 class="heading"><?php the_title(); ?></h1>
					<div class="c-post__info"><?php echo get_the_date('F j, Y') ?> | Category: <span><a href="/news/?filter=<?php echo clean_string($categories[0]->name) ?>"><?php echo $categories[0]->name ?></a></span></div>
					<div class="intro-para">
						<?php the_content(); ?>
					</div>
					<a href="/news">Return to Latest News</a>
				</div>
				<?php
					$contact_form = get_field('contact_form');
					$is_event_cat = false;
					foreach ($categories as $cat) {
						if ($cat->slug === 'events') {
							$is_event_cat = true;
						}
					}
				?>
				<?php if ($is_event_cat && $contact_form && !$contact_form['disable_form']) : ?>
					<div class="c-post__form">
						<?php if ($contact_form['title']) : ?>
							<h2 class="heading"><?php echo $contact_form['title']; ?></h2>
						<?php endif; ?>
						<?php if ($contact_form['copy']) : ?>
							<div class="intro-para">
								<p><?php echo $contact_form['copy']; ?></p>
							</div>
						<?php endif; ?>
						<?php echo str_replace('%-event-post-name-%', get_the_title(), do_shortcode('[contact-form-7 id="958" title="Event Page Contact Form"]')) ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="o-layout__item u-7/25@tablet sticky o-animateup u-animation-delay-2">
				<div class="c-post__ramp-tile">
					<div class="c-ramp-tile">
						<?php $ramp = get_field('ramp_upgrades_tile', 'options') ?>
						<h3 class="heading"><?php echo $ramp['heading'] ?></h3>
						<p><?php echo $ramp['content'] ?></p>
						<a href="<?php echo $ramp['link']['url'] ?>" class="o-btn o-btn--secondary o-btn--med o-btn--no-border o-btn--full o-btn--no-shadow"><?php echo $ramp['link']['title'] ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; ?>
<?php set_query_var('mod', 'grey') ?>
<?php get_template_part('partials/latest-news') ?>

<?php get_footer(); ?>