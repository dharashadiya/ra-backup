<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
	<title><?php wp_title(); ?> </title>

	<?php if(is_local_or_staging()) : ?>
		<meta name="robots" content="none" />
		<meta name="robots" content="noindex" />
		<meta name="googlebot" content="none" />
		<meta name="googlebot" content="noindex" />
	<?php endif; ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="156x156" href="<?php echo STYLESHEET_URL; ?>/assets/img/BetterBoating-Favicon-156x156.png">
	<?php wp_head(); ?>
	<?php include('partials/inlinescripts.min.php'); ?>
</head>
<?php 
 	$facebook = get_field('facebook', 'options');
	$instagram = get_field('instagram', 'options');
	$twitter = get_field('twitter', 'options');
	$dark_header = (is_single() || get_page_template_slug() === "tmpl-terms.php" || is_404() )  ? "dark" : "";
?>
<body <?php body_class(); ?>>
<header class="c-header <?php echo $dark_header ?>">
	<div class="o-wrapper o-wrapper--release">
		<div class="c-header__inner">
			<div class="c-header__left">
				<a class="c-header__logo" href="/">
					<?php svgicon('BetterBoating-Logo-White', '0 0 171.93 80.79'); ?>
					<img data-src="<?php echo ASSETS ?>/img/logo-color@2x.png" alt="Better Boating Logo">
				</a>
			</div>
			<div class="c-header__right">
				<div class="c-nav">
					<!-- <?php
						wp_nav_menu( array(
							'theme_location' => 'primary-menu',
							'container' => false,
							'menu_class' => 'c-nav__list',
								'menu_id' => 'main_nav'
						));
					?> -->
					<div class="c-nav__burger">
						<div href="javascript:void(0);" class="js-search-close js-toggle-menu" title="Open Close Navigation">
							<span class="c-toggle-menu <?php echo $toggle_class ?>"></span>&nbsp;
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<div class="c-nav-overlay" id="navOverlay">
	<div class="c-nav-overlay__nav centered">
		<?php
			wp_nav_menu( array(
				'theme_location' => 'secondary-menu',
				'container' => false,
				'menu_class' => 'c-nav-overlay__list',
				'menu_id' => 'overlay_nav'
			));
		?>
		<div class="c-nav-overlay__socials c-nav-overlay__socials--mobile">
			<?php if ($facebook) : ?>
				<a aria-label="Facebook Link" href="<?php echo $facebook ;?>" target="_blank" rel="noopener">
					<?php svgicon('Facebook-Icon', '0 0 12.38 24.01'); ?>
				</a>
			<?php endif; ?>
			<?php if ($instagram) : ?>
				<a aria-label="Instagram Link" href="<?php echo $instagram ;?>" target="_blank" rel="noopener">
					<?php svgicon('Insta-Icon', '0 0 29.7 29.67'); ?>
				</a>
			<?php endif; ?>
			<?php if ($twitter) : ?>
				<a aria-label="Twitter Link" href="<?php echo $twitter ;?>" target="_blank" rel="noopener">
					<?php svgicon('Twitter-Icon', '0 0 26.42 21.83'); ?>
				</a>
			<?php endif; ?>
		</div>
	</div>
	<div class="c-nav-overlay__socials">
		<?php if ($facebook) : ?>
			<a aria-label="Facebook Link" href="<?php echo $facebook ;?>" target="_blank" rel="noopener">
				<?php svgicon('Facebook-Icon', '0 0 12.38 24.01'); ?>
			</a>
		<?php endif; ?>
		<?php if ($instagram) : ?>
			<a aria-label="Instagram Link" href="<?php echo $instagram ;?>" target="_blank" rel="noopener">
				<?php svgicon('Insta-Icon', '0 0 29.7 29.67'); ?>
			</a>
		<?php endif; ?>
		<?php if ($twitter) : ?>
			<a aria-label="Twitter Link" href="<?php echo $twitter ;?>" target="_blank" rel="noopener">
				<?php svgicon('Twitter-Icon', '0 0 26.42 21.83'); ?>
			</a>
		<?php endif; ?>
	</div>
	<div class="c-nav-overlay__vic">
		<a href="https://www.vic.gov.au/" target="_blank" rel="noopener">
			<img src="<?php echo ASSETS ?>/img/vic-logo@2x.png" alt="Victorian State Government Logo">			
		</a>
	</div>
</div>

<div class="c-content">