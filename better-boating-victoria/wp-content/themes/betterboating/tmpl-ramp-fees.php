<?php
/*
 * Template Name: Fees Page
 */
?>

<?php get_header(); ?>

<?php 

?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php 
    $ramps = new WP_Query(array(
        'numberposts'=> -1,
        'post_type'	=> 'ra-ramps',
        'status' => 'published',
        'orderby' => 'menu_order',
		'order'   => 'ASC',
    ));
?>

<?php get_template_part('partials/ramp-map') ?>
<div class="c-boat-fees js-change-stage" id="content">
    <div class="o-wrapper o-wrapper--small">
        <div class="o-layout">
            <div class="o-layout__item u-18/25@tablet">
                <div class="c-boat-fees__breadcrumb o-animateup">
                    <div class="c-breadcrumb c-breadcrumb--dark">
                        <?php if(function_exists('bcn_display')) {
                            bcn_display();
                        }?>
                    </div>
                </div>
                <div class="c-boat-fees__content">
                    <h2 class="heading large-mob o-animateup u-animation-delay-0"><?php the_title(); ?></h2>
                    <div class="intro-para o-animateup u-animation-delay-1"><?php the_content(); ?></div>
                </div>
            </div>
            <div class="o-layout__item u-7/25@tablet sticky top-88">
                <?php $link = get_field('link') ?>
                <div class="c-boat-fees__aside o-animateup u-animation-delay-2">
                    <img class="lazy" data-src="<?php echo ASSETS ?>/img/boating-vic-color@2x.png" alt="Boating VIC logo">
                    <p><?php the_field('content') ?></p>
                    <a href="<?php echo $link['url'] ?>" target="<?php echo $link['target'] ?>" class="o-btn o-btn--secondary o-btn--no-border o-btn--full"><?php echo $link['title'] ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endwhile; ?>
<?php get_template_part('partials/ramp-upgrades'); ?>

<?php get_footer(); ?>
