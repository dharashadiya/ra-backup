import { isMobile } from "./template-functions";

export function parallax() {
    if ($('#js-scene-1').length && !isMobile()) {
        let sceneSpeedBoat = document.getElementById('js-scene-1');
        let parallaxInstance1 = new Parallax(sceneSpeedBoat, {
            // relativeInput: true,
        });
        parallaxInstance1.friction(0.2, 0.8);
    }
}

export function laxLibrary() {
    if (!isMobile()) {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE");
        var edge = ua.indexOf("Edge");
        if (msie > 0 || edge > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))
        return;
        lax.setup();
        const updateLax = () => {
            lax.update(window.scrollY);
            window.requestAnimationFrame(updateLax);
        };
        window.requestAnimationFrame(updateLax); 
    }
}