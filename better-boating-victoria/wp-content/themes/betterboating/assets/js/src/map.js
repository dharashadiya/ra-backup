// (function($) {
// 	window.console ||
// 		(window.console = {
// 			log: function() {}
// 		});
// 	window.googleMap || (window.googleMap = {});
// 	$.extend(googleMap, {
// 		init_vars: function() {
// 			// Global variables -> use as googleMap.VariableName in other files
// 			this.apiKey = "AIzaSyCGfu4FdTBW-gacADJGvMTaWDZGI-TuXSM";
// 			this.latitude = -37.8248196;
// 			this.longitude = 144.9897351;
// 			this.markerWidth = 128;
// 			this.markerHeight = 128;
// 			console.log(this.apiKey)
// 			return (this.contentString =
// 				'<div id="c-map__popup">' +
// 				"</div>" +
// 				"<h3>Company Name</h3>" +
// 				'<div id="c-map__poppup_content">' +
// 				"Address Goes Here" +
// 				"</div>");
// 		},
// 		onready: function() {
// 			this.init_vars();
// 			if ($("#map").length > 0) {
// 				return this.loadMap();
// 			}
// 		},
// 		loadMap: function() {
// 			var isDraggable;
// 			//styles = styling map
// 			let styles = [
// 			  {
// 			    featureType: "all",
// 			    stylers: [
// 			      { saturation: -92 },
// 			      { hue: "#2A00FF" },
// 			      { visibility: "simplified" },
// 			      { gamma: 2 }
// 			    ]
// 			  }
// 			]

// 			// isDraggable = if Modernizr.touchevents then false else true
// 			isDraggable = true;
// 			return $.getScript(
// 				"https://maps.googleapis.com/maps/api/js?key=" + this.apiKey,
// 				function() {
// 					var mapCanvas, mapOptions;
// 					mapCanvas = document.getElementById("map");
// 					mapOptions = {
// 						center: new google.maps.LatLng(
// 							this.latitude,
// 							this.longitude
// 						),
// 						zoom: 15,
// 						mapTypeId: google.maps.MapTypeId.ROADMAP,
// 						scrollwheel: false,
// 						mapTypeControl: false,
// 						streetViewControl: false,
// 						styles: styles
// 					};
// 					this.map = new google.maps.Map(mapCanvas, mapOptions);
// 					googleMap.map = this.map;
// 					return googleMap.markers();
// 				}
// 			);
// 		},
// 		fixBounds: function(points) {
// 			var bounds, i, len, point;
// 			bounds = new google.maps.LatLngBounds();
// 			for (i = 0, len = points.length; i < len; i++) {
// 				point = points[i];
// 				bounds.extend(point);
// 			}
// 			return this.map.fitBounds(bounds);
// 		},
// 		markers: function() {
// 			var contentString, image, infowindow, lat_lng, marker;
// 			image = {
// 				url: window.site.themeurl + "/assets/img/marker.png",
// 				scaledSize: new google.maps.Size(
// 					this.markerWidth,
// 					this.markerHeight
// 				)
// 			};
// 			lat_lng = {
// 				lat: this.latitude,
// 				lng: this.longitude
// 			};
// 			contentString = this.contentString;
// 			infowindow = new google.maps.InfoWindow({
// 				content: contentString
// 			});
// 			marker = new google.maps.Marker({
// 				position: lat_lng,
// 				icon: image,
// 				map: googleMap.map
// 			});
// 			return marker.addListener("click", function() {
// 				infowindow.open(map, marker);
// 			});
// 		}
// 	});
// 	return $(function() {
// 		return googleMap.onready();
// 	});
// })(jQuery);
var map;
const markerImg = $("#Themeurl").attr("content") + '/assets/img/map-markerv3@4x.png';
const styles = [
  {
    featureType: "all",
    elementType: "labels.text.fill",
    stylers: [
      {
        saturation: 36
      },
      {
        color: "#333333"
      },
      {
        lightness: 40
      }
    ]
  },
  {
    featureType: "all",
    elementType: "labels.text.stroke",
    stylers: [
      {
        visibility: "off"
      },
      {
        color: "#ffffff"
      },
      {
        lightness: 16
      }
    ]
  },
  {
    featureType: "all",
    elementType: "labels.icon",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "administrative",
    elementType: "geometry.fill",
    stylers: [
      {
        color: "#fefefe"
      },
      {
        lightness: 20
      }
    ]
  },
  {
    featureType: "administrative",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#473f65"
      },
      {
        lightness: 17
      },
      {
        weight: 1.2
      }
    ]
  },
  {
    featureType: "administrative.country",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#d55d26"
      }
    ]
  },
  {
    featureType: "administrative.country",
    elementType: "labels",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "administrative.province",
    elementType: "all",
    stylers: [
      {
        visibility: "on"
      }
    ]
  },
  {
    featureType: "administrative.province",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#d55d26"
      }
    ]
  },
  {
    featureType: "administrative.province",
    elementType: "labels",
    stylers: [
      {
        color: "#ff0000"
      },
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "administrative.province",
    elementType: "labels.text",
    stylers: [
      {
        visibility: "off"
      },
      {
        color: "#201847"
      }
    ]
  },
  {
    featureType: "landscape",
    elementType: "geometry",
    stylers: [
      {
        color: "#f5f5f5"
      },
      {
        lightness: 20
      }
    ]
  },
  {
    featureType: "landscape",
    elementType: "geometry.fill",
    stylers: [
      {
        color: "#473f65"
      }
    ]
  },
  {
    featureType: "landscape.man_made",
    elementType: "all",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "landscape.man_made",
    elementType: "geometry",
    stylers: [
      {
        visibility: "on"
      }
    ]
  },
  {
    featureType: "landscape.man_made",
    elementType: "geometry.fill",
    stylers: [
      {
        color: "#473f65"
      }
    ]
  },
  {
    featureType: "landscape.natural.terrain",
    elementType: "all",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "landscape.natural.terrain",
    elementType: "geometry",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "poi",
    elementType: "all",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "poi",
    elementType: "geometry",
    stylers: [
      {
        color: "#f5f5f5"
      },
      {
        lightness: 21
      }
    ]
  },
  {
    featureType: "poi.park",
    elementType: "all",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "poi.park",
    elementType: "geometry",
    stylers: [
      {
        color: "#dedede"
      },
      {
        lightness: 21
      },
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "poi.park",
    elementType: "geometry.fill",
    stylers: [
      {
        color: "#ffffff"
      }
    ]
  },
  {
    featureType: "road.highway",
    elementType: "all",
    stylers: [
      {
        color: "#473f65"
      }
    ]
  },
  {
    featureType: "road.highway",
    elementType: "geometry.fill",
    stylers: [
      {
        color: "#473f65"
      },
      {
        lightness: 17
      },
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "road.highway",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#ffffff"
      },
      {
        lightness: 29
      },
      {
        weight: 0.2
      },
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "road.arterial",
    elementType: "geometry",
    stylers: [
      {
        color: "#473f65"
      },
      {
        lightness: 18
      }
    ]
  },
  {
    featureType: "road.local",
    elementType: "geometry",
    stylers: [
      {
        color: "#473f65"
      },
      {
        lightness: 16
      },
      {
        visibility: "on"
      }
    ]
  },
  {
    featureType: "transit",
    elementType: "geometry",
    stylers: [
      {
        color: "#f2f2f2"
      },
      {
        lightness: 19
      },
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "transit.station",
    elementType: "all",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "all",
    stylers: [
      {
        visibility: "simplified"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "geometry",
    stylers: [
      {
        lightness: 17
      },
      {
        color: "#1e1843"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#ff0000"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "labels",
    stylers: [
      {
        visibility: "off"
      }
    ]
  }
];


if ($("#mapload").length > 0) {
  $.getScript(
    "https://maps.googleapis.com/maps/api/js?key=AIzaSyCh8t0xulUFZIvR28eg4smE_s61zloCJkw&callback=initMap"
  );
}

function initMap() {
	const desktopConfig = {
    center: { lat: -38.346003, lng: 143.201245 },
    zoom: 8,
    zoomControl: true,
    scrollwheel: false,
    dragabble: true,
    disableDefaultUI: true,
    styles: styles
  };
	const mobileConfig = {
    center: { lat: -37.501286, lng: 144.856808 },
    zoom: 7,
    disableDefaultUI: true,
    scrollwheel: false,
    dragabble: false,
    styles: styles
  };
	// mapMarkers is a variable fed in from PHP
	if (typeof mapMarkers != undefined){
		let map
		if (site.isMobile()) {
			map = new google.maps.Map(document.getElementById("map"), mobileConfig);
		} else {
			map = new google.maps.Map(document.getElementById("map"), desktopConfig);
		}
		mapMarkersContent = mapMarkers.map(function(marker){
			return '<div class="c-map-info">' +
				'<div class="c-map-info__price">Free</div>' +
				'<h4 class="heading">' + marker.title + '</h4>' +
				'<p>' + marker.description + '</p>' +
				'<div class="c-map-info__image" style="background-image: url(\'' + marker.image +'\');"></div>' +
				'<a class="c-map-info__live" target="_blank" href="' + marker.link + '">View Live Ramp Info</a>' +
			'</div>'
		})

		let infoWindow = new google.maps.InfoWindow();

		mapMarkers.forEach(function(marker, index) {
			let markerOnMap = new google.maps.Marker({
				position: { lat: +marker.lat, lng: +marker.lng },
				map: map,
				icon: {
					url: markerImg,
					scaledSize: new google.maps.Size(30, 36)
				},
				title: marker.title
      		});

			markerOnMap.addListener("click", function() {
				infoWindow.setContent(mapMarkersContent[index])
				infoWindow.open(map, markerOnMap);
				let coordinates = markerOnMap.getPosition()
				if (site.isMobile()) {
					map.panTo(markerOnMap.getPosition())
        } else  {
					let newCoordinates = new google.maps.LatLng(coordinates.lat() + 1, coordinates.lng() - 2)
					map.panTo(newCoordinates)
				}
			});
		})
	}
}