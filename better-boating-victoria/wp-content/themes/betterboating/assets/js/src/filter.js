export function filterControl() {
    const urlFilter = getUrlParameter("filter");
    const filters = $('.js-filter')

    if (urlFilter) {
        filters.removeClass("current");
        $(`.js-filter[data-filter*="${urlFilter}"]`).addClass("current");
        $(`.js-filter-item`).fadeOut(200);
        setTimeout(() => {
            $(`.js-filter-item[data-filter*="${urlFilter}"]`).fadeIn(200);
        }, 200);
    }

    if ($('.js-filter').length) {
        filters.click(event => {
            const target = $(event.target)
            let filter = event.target.dataset.filter
            filters.removeClass('current')
            target.addClass('current')
            $(`.js-filter-item`).fadeOut(200)
            setTimeout(() => {
                $(`.js-filter-item[data-filter*="${filter}"]`).fadeIn(200)
            }, 200);
        })
    }
}

function getUrlParameter(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
  var results = regex.exec(location.search);
  return results === null
    ? ""
    : decodeURIComponent(results[1].replace(/\+/g, " "));
}
