export function breakHeading() {
    $(".heading").lettering("words");
    $(".heading-mob").lettering("words");
}

export function scrollTop() {
    if ($('.js-to-top').length) {
        $('.js-to-top').click(() => {
            $("html,body").animate({ scrollTop: 0 }, "slow");
        });
    }

    if ($(".js-to-content").length) {
        $(".js-to-content").click(() => {
          let content = $("#content").position().top;
          $("html,body").animate({ scrollTop: content }, "slow");
        });
    }
}

export function checkFormIsValid() {
    if ($("#wpcf7-f8-p9-o1 form").length) {
        $("#wpcf7-f8-p9-o1 form").keypress(() => {
          if (
            [...document.querySelectorAll(".js-validate")].every(
              input => input.value
            )
          ) {
            $(".c-contact__submit").removeClass("invalid");
            $(".c-contact__submit").removeClass("o-btn--secondary");
          }
        });
    }
}