export function homeSlider() {
    if ($('.c-home-slider').length) {
        $(".c-home-slider__content").removeClass('hide')
        $(".c-home-slider__content-slider").slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: false,
          autoplay: true,
          infinite: true,
          fade: true,
          autoplaySpeed: 6000,
          asNavFor: ".c-home-slider__image-slider"
        });
        $(".c-home-slider__image-slider").slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
          autoplay: true,
          infinite: true,
          autoplaySpeed: 6000,
          appendDots: $('.c-home-slider__dots'),
          asNavFor: ".c-home-slider__content-slider"
        });
    }
}