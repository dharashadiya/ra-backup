export function rampsSlider() {
    if ($('.c-ramp__slider').length) {
        $(".c-ramp__slider").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            // autoplay: true,
            infinite: true,
            fade: true,
            dots: true,
            speed: 200,
            appendDots: '.c-ramp__slider-controls',
            customPaging: function(slick, index) {
                let title = slick.$slides[index].dataset.title
                return `<div class="c-ramp__slide-title js-hover-slick" data-index="${index}">${title}</div>`
            }
        });
        $('.js-hover-slick').hover((e) => {
            $(".c-ramp__slider").slick('slickGoTo', e.target.dataset.index)
        })
    }
}

export function rampUpgradesSlider() {
    if ($('.c-ramp-upgrades__slider').length) {
        $(".c-ramp-upgrades__slider").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            variableWidth: true,
            autoplay: true,
            infinite: true,
            centerMode: true,
            prevArrow: $('.c-ramp-upgrades__slider-arrows.prev'),
            nextArrow: $('.c-ramp-upgrades__slider-arrows.next'),
            responsive: [
                {
                    breakpoint: 1075,
                    settings : {
                        slidesToShow: 1,
                        arrows: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        arrows: false
                    }
                }
            ]
        });
    }
}

export function gridControl() {
    if ($('.c-ramp__grid').length) {
        $('.js-block').click(() => {
            if ($(".js-block").hasClass('active')) {
                return
            } else {
                $(".c-ramp__grid").fadeOut(200);
                $(".c-ramp__slider-wrap").fadeIn(200);
                $(".js-block").toggleClass('active')
                $(".js-grid").toggleClass("active");
            }
        })
        $('.js-grid').click(() => {
            if ($(".js-grid").hasClass("active")) {
              return;
            } else  {
                $(".c-ramp__grid").fadeIn(200);
                $(".c-ramp__slider-wrap").fadeOut(200);
                $(".js-grid").toggleClass('active')
                $(".js-block").toggleClass("active");
            }
        })
    }
}