export function changeHeaderStage() {
    if ($(".js-change-stage").length) {
        const offset = 0;
        $(window).scroll(function() {
            detectStage(offset)
        });
        detectStage(offset)
    }
}

function detectStage(offset) {
    const header = $(".c-header");
    const stageH = $(".js-change-stage").position().top;
    const logoPosition = $(".c-header__logo").offset().top + offset;
    if ($(window).scrollTop() < 40) {
        header.removeClass("backing");
    } else if (logoPosition > stageH) {
        header.addClass("backing");
    } else {
        header.removeClass("backing");
    }
}

export function siteNavControl() {
    let delta, lastScrollTop, navbarHeight, scrollPage;
    delta = 20;
    lastScrollTop = 0;
    navbarHeight = 100;
    const header = $(".c-header");

    $(window).scroll(function() {
        // scrollPage = $(window).scrollTop();
        // if (scrollPage > lastScrollTop && scrollPage > navbarHeight) {
        //     header.removeClass("nav-down").addClass("nav-up");
        // } else if (
        //     scrollPage + $(window).height() < $(document).height() &&
        //     lastScrollTop > scrollPage + delta
        // ) {
        //     header.removeClass("nav-up").addClass("nav-down");
        // }
        // if (scrollPage < navbarHeight) {
        //     header.removeClass("nav-down");
        // }
        // lastScrollTop = scrollPage;
    });
}

export function navOverlay() {
    $('.c-nav-overlay').hide(0)
    let headerHadDark = false
    $('.js-toggle-menu').click(() => {
        let headerClassDark = $('.c-header').hasClass('dark') ? true : false
        if ($('.c-nav-overlay').hasClass('is-open')) {
            $('.c-nav-overlay').hide()
            $('.js-toggle-menu').toggleClass('is-open')
            $('body, html').toggleClass('scroll-lock')
            $(".c-nav-overlay").removeClass("is-open");
            if (!site.isMobile()) {
                $('.c-nav__list').show()
            }
            if (headerHadDark) {
                $(".c-header").toggleClass("dark");
                headerHadDark = false
            }
        } else {
            if (headerClassDark) {
                $(".c-header").toggleClass("dark");
                headerHadDark = true
            }
            if (!site.isMobile()) {
                $(".c-nav__list").hide();
            }
            $('body, html').toggleClass('scroll-lock')
            $('.c-nav-overlay').show()
            $('.js-toggle-menu').toggleClass('is-open')
            $(".c-nav-overlay").addClass("is-open");
        }
    })
}
