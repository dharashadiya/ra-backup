/* global site */
// [1] Import functions here
import { homeSlider } from "./home";
import { changeHeaderStage, navOverlay } from "./header";
import { filterControl } from "./filter";
import { breakHeading, scrollTop, checkFormIsValid } from "./shared";
import { rampsSlider, gridControl, rampUpgradesSlider } from "./ramps";
import { parallax, laxLibrary } from "./animations";
import {
	isMobile,
	openLinkNewTab,
	superfishMenu,
	slider,
	cloudinaryImageCDN,
	lazyLoad,
	fadeInUp,
} from "./template-functions";
(function($) {
	window.site || (window.site = {});
	$.extend(
		site,
		{
			initVars: () => {
				// Global variables -> use as site.VariableName in other files
				site.body = $("body");
				site.root = $("html");
				site.header = $("#Top");
				site.main = $("#Main");
				site.side = $("#Secondary");
				site.siteurl = $("#Siteurl").attr("content");
				site.themeurl = $("#Themeurl").attr("content");
				return (site.ajaxurl = $("#Ajaxurl").attr("content"));
			},
			// [3] Call functions within either of these
			onReady: () => {
				svg4everybody()
				site.initVars();
				// site.parallax();
				site.breakHeading();
				site.fadeInUp();
				site.rampsSlider();
				site.gridControl();
				site.rampUpgradesSlider();
				// site.siteNavControl();
				site.changeHeaderStage();
				site.filterControl();
				site.navOverlay();
				site.homeSlider();
				site.scrollTop();
				site.checkFormIsValid();
				site.lazyLoad();
				site.laxLibrary();
				site.cloudinaryImageCDN();
				return site.isMobile();
			},
			onLoad: () => {
				// return site.slider()
			}
		},
		{
			// [2] Register functions here
			isMobile: isMobile,
			openLinkNewTab: openLinkNewTab,
			superfishMenu: superfishMenu,
			slider: slider,
			cloudinaryImageCDN: cloudinaryImageCDN,
			breakHeading: breakHeading,
			rampsSlider: rampsSlider,
			gridControl: gridControl,
			rampUpgradesSlider: rampUpgradesSlider,
			filterControl: filterControl,
			// siteNavControl: siteNavControl,
			changeHeaderStage: changeHeaderStage,
			navOverlay: navOverlay,
			homeSlider: homeSlider,
			scrollTop: scrollTop,
			checkFormIsValid: checkFormIsValid,
			parallax: parallax,
			lazyLoad: lazyLoad,
			laxLibrary: laxLibrary,
			fadeInUp: fadeInUp
		}
	);
	$(() => {

	// popup-dialog-box
	var delay = 3000; // milliseconds
    var cookie_expire = 0; // days
    var cookie = localStorage.getItem("list-builder");
    if(cookie == undefined || cookie == null) {
        cookie = 0;
    }
    if(((new Date()).getTime() - cookie) / (1000 * 60 * 60 * 24) > cookie_expire) {
        $("#list-builder").delay(delay).fadeIn("fast", () => {
            $("#popup-box").fadeIn("fast", () => {});
        });
        $("#popup-close").click(() => {
            $("#list-builder, #popup-box").hide();
            localStorage.setItem("list-builder", (new Date()).getTime());
        });
	}
	
		return site.onReady();
	});
	return $(window).on("load", () => {
	return site.onLoad();
	});
})(jQuery);
