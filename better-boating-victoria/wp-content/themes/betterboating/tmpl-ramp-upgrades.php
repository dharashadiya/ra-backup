<?php
/*
 * Template Name: Ramp Upgrades Page
 */
?>

<?php get_header(); ?>

<?php 

?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php 
    $ramps = new WP_Query(array(
        'numberposts'=> -1,
        'post_type'	=> 'ra-ramps',
        'status' => 'published',
        'orderby' => 'menu_order',
		'order'   => 'ASC',
    ));
?>
<div class="c-ramp js-change-stage">
    <div class="o-wrapper o-wrapper--small">
        <div class="c-ramp__upper">
            <div class="c-ramp__head o-animateup">
                <div class="c-ramp__head-content">
                    <div class="c-ramp__breadcrumbs">
                        <div class="c-breadcrumb c-breadcrumb--dark-to-white">
                            <?php if(function_exists('bcn_display')) {
                                bcn_display();
                            }?>
                        </div>
                    </div>
                    <h1 class="heading o-animateup u-animtation-delay-0"><?php the_title() ?></h1>
                    <div class="intro-para o-animateup u-animation-delay-1">
                        <?php the_content(); ?>
                    </div>
                </div>
                <div class="c-ramp__buoy o-animateup u-animation-delay-2">
                    <img class="c-ramp__buoy-image" data-src="<?php echo ASSETS ?>/img/BetterBoating-Illustrations-Parralax-Buoy-Foreground.png" alt="Buoy">
                </div>
            </div>
        </div>
        <div class="c-ramp__statement o-animateup u-animation-delay-2">
            <?php the_field('statement') ?>
        </div>
        <?php if($ramps->posts) : ?>
            <div class="c-ramp__gallery">
                <div class="c-ramp__control o-animateup">
                    <div class="c-ramp__grid-controls active js-grid">
                        <?php svgicon('4-square', '0 0 35 35'); ?>
                    </div>
                    <div class="c-ramp__block-control js-block"></div>
                </div>
                <div class="c-ramp__grid o-animateup">
                    <div class="o-layout o-layout--grid">
                        <?php foreach ($ramps->posts as $ramp) : ?>
                            <?php 
                            $image = get_the_post_thumbnail_url($ramp->ID);
                            $stage = get_the_terms($ramp, 'build_stage');
                            ?>
                            <div class="o-layout__item u-1/3@tablet u-1/2@mobileLandscape">
                                <div class="c-ramp__grid-tile o-animateup">
                                    <a href="<?php echo get_the_permalink($ramp->ID) ?>">
                                        <div class="c-ramp__grid-image-wrap">
                                            <img class="lazy" data-src="<?php echo $image ?>" alt="<?php echo $ramp->post_title ?>" class="c-ramp__grid-image">
                                        </div>
                                        <h3><?php echo $ramp->post_title ?></h3>
                                        <span class="c-ramp__progress">
                                            <?php echo $stage[0]->name ?>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>  
                    </div>
                </div>
                <div class="c-ramp__slider-wrap">
                    <div class="c-ramp__slider-controls"></div>
                    <div class="c-ramp__slider">
                        <?php foreach ($ramps->posts as $ramp) : ?>
                            <?php 
                            $image = get_the_post_thumbnail_url($ramp->ID);
                            ?>
                            <div class="c-ramp__slide" data-title="<?php echo $ramp->post_title ?>">
                                <a href="<?php echo get_the_permalink($ramp->ID) ?>">    
                                    <img src="<?php echo $image ?>" alt="<?php echo $ramp->post_title ?>">
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="c-ramp__lower o-animateup">
            <?php $lower_image = get_field('lower_image'); ?>
            <div class="o-layout">
                <div class="o-layout__item u-1/3@mobileLandscape">
                    <div class="c-ramp__hazard">
                        <img class="lazy" data-src="<?php echo $lower_image['url'] ?>" alt="Hazard">
                    </div>
                </div>
                <div class="o-layout__item u-2/3@mobileLandscape">
                    <div class="c-ramp__lower-content">
                        <h2 class="heading o-animateup u-animation-delay-1"><?php the_field('lower_heading') ?></h2>
                        <div class="intro-para o-animateup u-animation-delay-2"><?php the_field('lower_content') ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>
<?php get_template_part('partials/latest-news'); ?>

<?php get_footer(); ?>
