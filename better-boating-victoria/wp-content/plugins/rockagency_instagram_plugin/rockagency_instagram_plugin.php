<?php
/**
 * Plugin Name: Rockagency Instagram
 * Plugin URI: https://rockagency.com.au
 * Description: Registers shortcode for instagram plugin
 * Version: 1.0.1
 * Author: Axel Debenham-Lendon
 *
 * @package Rock Agency Instagram Plugin
 */


function instagram_shortcode($atts) {
    return "<div class='c-instagram' data-num-of-photos='". $atts['photos'] . "' data-instagram-handle='". $atts['handle']  ."'></div>";
}

function instagram_script() {
    wp_register_script('instagram.js', plugins_url('dist/instagram.js', __FILE__), array('jquery'),'1.0', true);
 
    wp_enqueue_script('instagram.js');
}

// Instagram API
add_action( 'wp_ajax_nopriv_instagram_api', 'instagram_api' );
add_action( 'wp_ajax_instagram_api', 'instagram_api' );
function instagram_api() {
	$username = $_POST['data'];
	$insta_source = file_get_contents('http://instagram.com/'. $username);
	$shards = explode('window._sharedData = ', $insta_source);
	$insta_json = explode(';</script>', $shards[1]); 
	$insta_array = json_decode($insta_json[0], TRUE);
	$insta_feed = array();
	foreach ($insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'] as $item) {
		$item = $item['node'];
		$insta_feed[] = array(
			'link' => 'http://instagram.com/p/' . $item['shortcode'],
			'image' => $item['display_url'],
			'thumbnails' => array(
				'small' => $item['thumbnail_resources'][2]['src'], 
				'medium' => $item['thumbnail_resources'][3]['src'],
				'large' => $item['thumbnail_resources'][4]['src'] ),
			'date' => $item['taken_at_timestamp'],
			'caption' => $item['caption'],
			// 'caption' => false,
			'likes' => $item['edge_liked_by']['count'],
			'comments' => $item['edge_media_to_comment']['count'],
			'likes' => $item['edge_liked_by']['count']
		);    
	}
	$insta_array = json_encode($insta_feed);
	wp_die($insta_array);
}
  
add_action( 'wp_enqueue_scripts', 'instagram_script' );
add_shortcode('instagram', 'instagram_shortcode');
