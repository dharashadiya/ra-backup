<?php
// Register Custom Post Type
function ramps_post_type() {

	$labels = array(
		'name'                  => _x( 'Ramps', 'Post Type General Name', 'ra-ramps' ),
		'singular_name'         => _x( 'Ramp', 'Post Type Singular Name', 'ra-ramps' ),
		'menu_name'             => __( 'Ramp Upgrades', 'ra-ramps' ),
		'name_admin_bar'        => __( 'Ramps', 'ra-ramps' ),
		'archives'              => __( 'Archives', 'ra-ramps' ),
		'attributes'            => __( 'Ramp Attributes', 'ra-ramps' ),
		'parent_item_colon'     => __( '', 'ra-ramps' ),
		'all_items'             => __( 'All Ramps', 'ra-ramps' ),
		'add_new_item'          => __( 'Add New Ramp', 'ra-ramps' ),
		'add_new'               => __( 'New Ramp', 'ra-ramps' ),
		'new_item'              => __( 'New Ramp', 'ra-ramps' ),
		'edit_item'             => __( 'Edit Ramp', 'ra-ramps' ),
		'update_item'           => __( 'Update Ramp', 'ra-ramps' ),
		'view_item'             => __( 'View Ramp', 'ra-ramps' ),
		'view_items'            => __( 'View Ramps', 'ra-ramps' ),
		'search_items'          => __( 'Search Ramps', 'ra-ramps' ),
		'not_found'             => __( 'No Ramps found', 'ra-ramps' ),
		'not_found_in_trash'    => __( 'No Ramps found in Trash', 'ra-ramps' ),
		'featured_image'        => __( 'Featured Image', 'ra-ramps' ),
		'set_featured_image'    => __( 'Set featured image', 'ra-ramps' ),
		'remove_featured_image' => __( 'Remove featured image', 'ra-ramps' ),
		'use_featured_image'    => __( 'Use as featured image', 'ra-ramps' ),
		'insert_into_item'      => __( 'Insert into Ramp', 'ra-ramps' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Ramp', 'ra-ramps' ),
		'items_list'            => __( 'Ramps list', 'ra-ramps' ),
		'items_list_navigation' => __( 'Ramps list navigation', 'ra-ramps' ),
		'filter_items_list'     => __( 'Filter Ramps list', 'ra-ramps' ),
	);
	$args = array(
		'label'                 => __( 'Ramp', 'ra-ramps' ),
		'description'           => __( 'Ramp Profiles', 'ra-ramps' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-align-none',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
		'rewrite'             	=> array(
			'slug' => 'ramp-upgrades',
			'with_front' => false
		)
	);
	register_post_type( 'ra-ramps', $args );
}

// Register Custom Taxonomy
function taxonomy_build_stage() {
  $labels = array(
    'name'                       => _x( 'Build stage', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Build stage', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Build stage', 'text_domain' ),
    'all_items'                  => __( 'All build stage', 'text_domain' ),
    'parent_item'                => __( 'Parent build stage', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent build stage:', 'text_domain' ),
    'new_item_name'              => __( 'New build stage', 'text_domain' ),
    'add_new_item'               => __( 'Add New build stage', 'text_domain' ),
    'edit_item'                  => __( 'Edit build stage', 'text_domain' ),
    'update_item'                => __( 'Update build stage', 'text_domain' ),
    'view_item'                  => __( 'View build stage', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate build stage with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove build stage', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular build stage', 'text_domain' ),
    'search_items'               => __( 'Search build stage', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'items_list'                 => __( 'build stage list', 'text_domain' ),
    'items_list_navigation'      => __( 'build stage list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => !false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
  );
  register_taxonomy( 'build_stage', array( 'ra-ramps' ), $args );
}
add_action( 'init', 'taxonomy_build_stage', 0 );
add_action( 'init', 'ramps_post_type', 0 );
?>