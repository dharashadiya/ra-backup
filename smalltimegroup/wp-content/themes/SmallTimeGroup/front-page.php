<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<?php
	$slider = get_field('feature_slider');
   	$date = date_create();
    $future_broadcasts = new WP_Query(array(
        'posts_per_page'=> -1,
        'post_type'	=> 'post',
        'status' => 'publish',
        'orderby' => 'meta_value',
        'order' => 'ASC',
        'meta_type' => 'DATETIME',
        'meta_key' => 'date',
        'meta_query' => array(
            array(
                'key' => 'end_date',
                'value' => date_format($date, 'Y-m-d H:i:s'),
                'compare' => '>',
                'type' => 'DATETIME'
            )
        )
	));
	$future_broadcasts->posts = ra_filter_broadcasts($future_broadcasts->posts);
	$latest_broadcast = '';
    foreach ($future_broadcasts->posts as $broadcast) {
        $start_date_str = get_field('date', $broadcast->ID);
        $end_date = get_field('end_date', $broadcast->ID);
        if (intval(strtotime($start_date_str)) < intval( strtotime("now") )) {
            $latest_broadcast = $broadcast;
            break;
        } else {
            $latest_broadcast = $broadcast;
            break;
        }
	}
	$latest_image = get_the_post_thumbnail_url($latest_broadcast);

?>
<main class="c-home">
	<div class="o-wrapper">
		<h1 class="c-home__title">
			<?php svgicon('SmallTime-Logo', '0 0 568.4 94.4'); ?>
		</h1>
	</div>
	<div class="o-wrapper o-wrapper--releaseMobile">
		<div class="c-home__slider">
			<?php if (have_rows('feature_slider')) :?>
			<div class="c-feature-slider">
				<?php while(have_rows('feature_slider')): the_row(); ?>
				<?php
					$link = get_sub_field('link');
					$image = get_sub_field('image');
					$content = get_sub_field('content');
					$mobile_content = get_sub_field('mobile_content');
				?>
				<div class="c-feature-slider__slide">
					<div class="c-feature-slider__content">
						<h3 class="c-feature-slider__sub-header">
							<?php the_sub_field('sub_heading') ?>
						</h3>
						<div class="c-feature-slider__header">
							<?php the_sub_field('heading') ?>
						</div>
						<p class="<?php echo ($mobile_content ? "hide-mob" : "" ) ?>"><?php echo $content ?></p>
						<?php if ($mobile_content) : ?>
							<p><?php echo $mobile_content ?></p>
						<?php endif; ?>
						<a href="<?php echo $link['url'] ?>" class="c-feature-slider__link"><?php echo $link['title'] ?>
							<?php svgicon('SmallTime-Icons-Plus-OffWhite', '0 0 16 16'); ?>
						</a>
					</div>
					<div class="c-feature-slider__image" style="background-image: url('<?php echo $image['url'] ?>');">
					</div>
				</div>
				<?php endwhile; ?>
				<?php if ($latest_broadcast) : ?>
				<div class="c-feature-slider__slide">
					<div class="c-feature-slider__content">
						<h3 class="c-feature-slider__sub-header">
							Small Time TV
						</h3>
						<div class="c-feature-slider__header">
							<?php echo $latest_broadcast->post_title ?><br />
							<?php $tags = get_the_tags($latest_broadcast->ID); ?>
							<?php if($tags) : ?>
								(<?php foreach($tags as $tag) : ?><?php echo $tags[0] != $tag ? ', ' : ''; ?><?php echo $tag->name; ?><?php endforeach; ?>)
								<?php else: ?>
							<?php endif; ?>
						</div>
						<?php $post_content = $latest_broadcast->post_content; ?>
						<p>Up next on <?php echo $tag->name; ?>: <?php  echo substr($post_content, 0, 360); ?>...</p>
						<a href="/small-time-tv" class="c-feature-slider__link">Watch more
							<?php svgicon('SmallTime-Icons-Plus-OffWhite', '0 0 16 16'); ?>
						</a>
					</div>
					<div class="c-feature-slider__image" style="background-image: url('<?php echo $latest_image ?>');">
					</div>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<?php
	$date = date_create();
	$events = new WP_Query(array(
		'posts_per_page'=> 4,
		'post_type'	=> 'post',
		'status' => 'published',
		'orderby' => 'meta_value',
		'order' => 'ASC',
		'meta_type' => 'DATETIME',
		'meta_key' => 'date',
		'meta_query' => array(
			array(
				'key' => 'date',
				'value' => date_format($date, 'Y-m-d H:i:s'),
				'compare' => '>',
				'type' => 'DATETIME'
			)
		)
	));
	?>
	<?php if ($events->post_count) : ?>
	<div class="o-wrapper">
		<div class="c-home__events">
			<h2 class="large"><?php the_field('events_section_title') ?></h2>
			<div class="c-events-panel">
				<div class="o-layout o-module">
				<?php
					$orig_query = $wp_query;
					$wp_query = $events;
					if ( have_posts() ) {
						while(have_posts()) {
							the_post();
							get_template_part('partials/loop-events-inner-show');
						}
					}
					$wp_query = $orig_query;
					wp_reset_query();
				?>
				</div>
			</div>
			<?php $event_link = get_field('events_section_link') ?>
			<div class="c-home__events-link-wrap o-animateup u-animation-delay-1">
				<a href="<?php echo $event_link['url'] ?>" class="c-home__events-link"><span><?php echo $event_link['title'] ?></span><?php svgicon('SmallTime-Icons-Plus-Black', '0 0 16 16'); ?></a>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div class="c-home__about">
		<?php $about = get_field('about') ?>
		<div class="o-wrapper">
			<div class="c-home__about-content">
				<h2 class="large"><?php echo $about['heading'] ?></h2>
				<!-- <h3><?php echo $about['sub_heading'] ?></h3>
				<?php echo $about['content'] ?> -->
				<a class="c-home__about-link" href="<?php echo $about['link']['url'] ?>"><?php echo $about['link']['title'] ?><?php svgicon('SmallTime-Icons-Plus-OffWhite', '0 0 16 16'); ?></a>
			</div>
		</div>
		<div class="c-home__about-image lazy" data-src="<?php echo $about['image']['url'] ?>"></div>
	</div>
	<?php
		$tv_posts = new WP_Query(array(
			'posts_per_page'=> 3,
			'status' => 'published',
			'orderby' => 'date',
			'order'   => 'DESC',
			'meta_query' => array(
				array(
					'key' => 'end_date',
					'value' => date_format($date, 'Y-m-d H:i:s'),
					'compare' => '<',
					'type' => 'DATETIME'
				)
			)
		));
		$tv_posts->posts = ra_filter_broadcasts($tv_posts->posts);
	?>
	<?php if ($tv_posts->posts) : ?>
	<div class="o-wrapper">
		<div class="c-home__tv">
			<?php $tv_section = get_field('small_time_live_section');
			 ?>
			<h2 class="large"><?php echo $tv_section['title'] ?></h2>
			<?php
				$orig_query = $wp_query;
				$wp_query  = $tv_posts;
				get_template_part('partials/loop-tv');
				$wp_query = $orig_query;
				wp_reset_query();
			?>
			<?php $tv_link = get_field('media_link') ?>
			<div class="c-home__events-link-wrap o-animateup u-animation-delay-1">
				<a href="<?php echo $tv_link['url'] ?>" class="c-home__tv-link"><span><?php echo $tv_link['title'] ?></span><?php svgicon('SmallTime-Icons-Plus-Black', '0 0 16 16'); ?></a>
			</div>
		</div>
	</div>
	<?php endif; ?>
</main>
<?php endwhile; ?>
<?php get_footer(); ?>