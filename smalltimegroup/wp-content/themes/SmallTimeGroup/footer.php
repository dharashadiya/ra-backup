    </div><!-- .c-content -->
</div><!-- .site-wrap -->
<?php wp_footer(); ?>
    <footer class="c-footer">
        <div class="o-wrapper">
            <div class="c-footer__top">
                <div class="c-footer__contact o-animateup">
                    <a href="https://www.google.com/maps/search/<?php the_field('address', 'options') ?>">NO. <?php the_field('address', 'options') ?></br><?php the_field('address_line_2', 'options') ?></a>
                    <?php if(get_field('phone', 'options')) : ?>
                    <a href="tel:<?php the_field('phone', 'options') ?>">PH — <?php the_field('phone_number', 'options') ?></a>
                    <?php endif; ?>
                    <a href="mailto:<?php the_field('email', 'options') ?>"><?php the_field('email', 'options') ?></a>
                </div>
                <div class="c-footer__hours o-animateup">
                    <?php the_field('hours', 'options') ?>
                </div>
                <nav class="c-footer__nav o-animateup" role="navigation">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'secondary-menu',
                            'container' => false,
                            'menu_class' => 'c-footer__nav-wrap',
                            'menu_id' => 'footer-menu'
                        ));
                    ?>
                </nav>
                <div class="c-footer__logo o-animateup">
                    <a href="/">
                        <?php svgicon('SmallTime-Logo-2', '0 0 112.95 140.77'); ?>
                    </a>
                </div>
            </div>
            <div class="c-footer__bottom">
                <div>
                    <div class="c-footer__socials o-animateup">
                        <?php get_template_part('partials/socials') ?>
                    </div>
                    <div class="c-footer__terms o-animateup">
                        <a href="terms-conditions">Terms & Conditions</a>&nbsp;|&nbsp;<a href="/privacy">Privacy Policy</a>
                        </span>
                        <span>&nbsp;Built by <a href="https://rockagency.com.au" target="_blank">Rock Agency</a>
                    </div>
                    <div class="c-footer__mobile-terms">
                        <a href="terms-conditions">Terms & Conditions</a>&nbsp;|&nbsp;<a href="/privacy">Privacy Policy</a>
                        <span>
                            © Copyright Small Time Group <?php echo date("Y"); ?>
                        </span>
                        <p>Built by <a href="https://rockagency.com.au" target="_blank">Rock Agency</a></p>
                    </div>
                </div>
                <div class="c-footer__copyright o-animateup">
                    © Copyright Small Time Group <?php echo date("Y"); ?>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>