export function formHandling() {
    if ($('.wpcf7-form').length) {
        const upload = $('input[type="file"]')
        upload.on("change", () => {
            upload.addClass('has-file')
        });
    }
}