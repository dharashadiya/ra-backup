export function homeSlider() {
    $('.c-feature-slider').slick({
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000,
    })
}