/* global site */
// [1] Import functions here
import { formHandling } from "./forms"
import { showDownloadLink } from "./artist-program";
import {
	isMobile,
	openLinkNewTab,
	superfishMenu,
	slider,
	cloudinaryImageCDN,
	lazyLoad,
	fadeInUp
} from "./template-functions";
import {
	mobileNav
} from "./header"
import {
	homeSlider
} from "./home"
import {
	barSlider
} from "./bar";
import {
	eventFilters
} from "./filters";
import {
	searchOverlay
} from "./searchOverlay";
import {
	coundownTimer,
	raFancyBox,
	streamControl,
	toggleUpcomingEvents
} from "./tv";
import {
	contactForm
} from "./contact";
import {
	loadMap
} from "./map";
import {
	parallax
} from "./parallax";
(function ($) {
	window.site || (window.site = {});
	$.extend(
		site, {
			initVars: () => {
				// Global variables -> use as site.VariableName in other files
				site.body = $("body");
				site.root = $("html");
				site.header = $("#Top");
				site.main = $("#Main");
				site.side = $("#Secondary");
				site.siteurl = $("#Siteurl").attr("content");
				site.themeurl = $("#Themeurl").attr("content");
				return (site.ajaxurl = $("#Ajaxurl").attr("content"));
			},
			// [3] Call functions within either of these
			onReady: () => {
				site.initVars();
				site.homeSlider();
				site.eventFilters();
				site.loadMap();
				site.coundownTimer();
				site.parallax();
				site.searchOverlay();
				site.contactForm();
				site.barSlider();
				site.showDownloadLink();
				site.raFancyBox();
				site.mobileNav();
				site.formHandling();
				site.streamControl();
				site.lazyLoad();
				site.toggleUpcomingEvents();
				site.fadeInUp();
				// site.cloudinaryImageCDN()
				return site.isMobile();
			},
			onLoad: () => {
				// return site.slider()
			}
		}, {
			// [2] Register functions here
			isMobile: isMobile,
			homeSlider: homeSlider,
			openLinkNewTab: openLinkNewTab,
			toggleUpcomingEvents: toggleUpcomingEvents,
			contactForm: contactForm,
			loadMap: loadMap,
			fadeInUp: fadeInUp,
			mobileNav: mobileNav,
			formHandling: formHandling,
			slider: slider,
			lazyLoad: lazyLoad,
			barSlider: barSlider,
			raFancyBox: raFancyBox,
			parallax: parallax,
			showDownloadLink: showDownloadLink,
			streamControl: streamControl,
			cloudinaryImageCDN: cloudinaryImageCDN,
			eventFilters: eventFilters,
			searchOverlay: searchOverlay,
			coundownTimer: coundownTimer,
		}
	);
	$(() => {
		return site.onReady();
	});
	return $(window).on("load", () => {
		return site.onLoad();
	});
})(jQuery);