export function mobileNav() {
    const targetElement = document.querySelector(".c-mobile-nav");
    $(".c-header__hamburger").on("click", () => {
        if ($(".js-toggle-menu").hasClass('is-open')) {
            $("body").removeClass("scroll-lock");
            $(".js-toggle-menu").removeClass("is-open");
            $(".c-mobile-nav").fadeOut(200);
            bodyScrollLock.enableBodyScroll(targetElement);
        } else {
            $("body").addClass("scroll-lock");
            $(".js-toggle-menu").addClass("is-open");
            $(".c-mobile-nav").fadeIn(200);
            bodyScrollLock.disableBodyScroll(targetElement);
        }
    });
}