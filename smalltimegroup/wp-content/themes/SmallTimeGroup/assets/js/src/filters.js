export function eventFilters() {
    const timing = 200
    let itemsToShow = 7;
    let filter = $(".js-event-filter.selected").data("filter");
    let filterItems

    function showFilterItems() {
        $('.js-filter-item').fadeOut(timing)
        if (filter === "all") {
          filterItems = $(`.js-filter-item`);
        } else {
          filterItems = $(`.js-filter-item[data-filter="${filter}"]`);
        }
        setTimeout(() => {
          filterItems.each((i, el) => {
            if (i < itemsToShow) {
              $(el).fadeIn(timing);
            }
          });
        }, timing);
    }

    $('.js-event-filter').click((event) => {
        $(".js-event-filter").removeClass('selected')
        $(event.target).addClass('selected')
        filter = $(event.target).data('filter')
        showFilterItems()
    })
    

    $('.js-show-more').click(() => {
        itemsToShow += 8;
        showFilterItems()
    })
}