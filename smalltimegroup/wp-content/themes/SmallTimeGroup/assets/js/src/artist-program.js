export function showDownloadLink() {
    const signupFormId = "571"
    document.addEventListener(
      "wpcf7mailsent",
      event => {
        console.log(event);
        if ("571" == event.detail.contactFormId) {
          $(".js-download-link").fadeIn(220);
        }
      },
      false
    );
}