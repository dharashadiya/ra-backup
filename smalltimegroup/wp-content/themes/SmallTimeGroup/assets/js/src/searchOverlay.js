export function searchOverlay() {
    const targetElement = document.querySelector(".c-search-overlay");
    $('.c-header__search').on('click', () => {
        $('body').addClass('scroll-lock')
        bodyScrollLock.disableBodyScroll(targetElement);
        $('.c-search-overlay').fadeIn(200)
    })
    
    $('.c-search-overlay__close span').on('click', () => {
        $('.c-search-overlay').fadeOut(200)
        $('body').removeClass('scroll-lock')
        bodyScrollLock.enableBodyScroll(targetElement);
    })

    $(".c-search-overlay__input").focus(() => {
        $(".c-search-overlay__input").toggleClass('placeholder-light')
    })
}