export function coundownTimer() {
	const dateString = $("#countdown").data("date");
	if ($("#countdown").length > 0 && dateString) {
		// Set the date we're counting down to
		// var countDownDate = new Date("Jan 5, 2021 15:37:25").getTime();
		const countDownDate = new Date(dateString.replace(' ', 'T')).getTime();
		// Update the count down every 1 second
		const x = setInterval(function () {
			// Get today's date and time
			const now = new Date().getTime();

			// Find the distance between now and the count down date
			const distance = countDownDate - now;

			// Time calculations for days, hours, minutes and seconds
			const days = Math.floor(distance / (1000 * 60 * 60 * 24));
			const hours = Math.floor(
				(distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
			);
			const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			const seconds = Math.floor((distance % (1000 * 60)) / 1000);

			// Display the result
			$('.days p').text(+days >= 10 ? days : `0${days}`)
			$(".hours p").text(+hours >= 10 ? hours : `0${hours}`);
			$(".minutes p").text(+minutes >= 10 ? minutes : `0${days}`);
			$(".seconds p").text(+seconds >= 10 ? seconds : `0${seconds}`);
			$("#countdown").css("opacity", 1);
			// If the count down is finished, write some text
			if (distance < 0) {
				clearInterval(x);
				// location.reload()
			}
		}, 1000);
	}
}

export function raFancyBox() {
	if ($(".js-fancybox").length > 0) {
		console.log("fancy");
		let minH = "auto";
		if (site.isMobile()) {
			minH = "1700";
		}
		$(".js-fancybox").fancybox({
			openSpeed: 0,
			speedIn: 0,
			closeSpeed: 0,
			speedOut: 0,
			maxWidth: "1500",
			minHeight: minH,
			width: "1500"
		});
	}
}

export function streamControl() {
	if ($('.c-tv').length) {
		const plyr = new Plyr('#player')
		$('.js-show-stream').click(() => {
			const timing = 200
			$('.js-stream-header').addClass('static')
			setTimeout(() => {
				$(".js-stream-header").fadeOut(timing);
				setTimeout(() => {
					plyr.play();
					$(".plyr").fadeIn(timing);
				}, timing);
			}, timing * 3);
		})
	}
}

export function toggleUpcomingEvents() {
	const toggleButton = $('.js-upcoming-toggle')
	if (toggleButton.length) {
		toggleButton.click(() => {
			$(".js-upcoming-extra").fadeToggle(200)
			toggleButton.toggleClass('open')
		})
	}
}