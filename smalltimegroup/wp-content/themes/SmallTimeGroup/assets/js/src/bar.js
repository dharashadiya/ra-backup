export function barSlider() {
  $(".c-slider").slick({
    dots: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 4000
  });
}
