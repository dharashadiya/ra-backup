export function parallax() {
    let rellax = new Rellax(".rellax", {
        speed: -2,
        center: true,
        wrapper: null,
        round: true,
        vertical: true,
        horizontal: false,
        breakpoints: [375, 770, 1280]
    });
}