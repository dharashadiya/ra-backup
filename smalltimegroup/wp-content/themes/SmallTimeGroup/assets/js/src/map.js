export function loadMap() {
  var styles;
  var googleMap = {};
  if ($("#map").length === 0) {
    return;
  }
  styles = [
    {
      featureType: "all",
      elementType: "labels.text.fill",
      stylers: [
        {
          saturation: 36
        },
        {
          color: "#000000"
        },
        {
          lightness: 40
        }
      ]
    },
    {
      featureType: "all",
      elementType: "labels.text.stroke",
      stylers: [
        {
          visibility: "on"
        },
        {
          color: "#000000"
        },
        {
          lightness: 16
        }
      ]
    },
    {
      featureType: "all",
      elementType: "labels.icon",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "administrative",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#000000"
        },
        {
          lightness: 20
        }
      ]
    },
    {
      featureType: "administrative",
      elementType: "geometry.stroke",
      stylers: [
        {
          color: "#000000"
        },
        {
          lightness: 17
        },
        {
          weight: 1.2
        }
      ]
    },
    {
      featureType: "landscape",
      elementType: "geometry",
      stylers: [
        {
          color: "#000000"
        },
        {
          lightness: 20
        }
      ]
    },
    {
      featureType: "poi",
      elementType: "geometry",
      stylers: [
        {
          color: "#000000"
        },
        {
          lightness: 21
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#000000"
        },
        {
          lightness: 17
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "geometry.stroke",
      stylers: [
        {
          color: "#000000"
        },
        {
          lightness: 29
        },
        {
          weight: 0.2
        }
      ]
    },
    {
      featureType: "road.arterial",
      elementType: "geometry",
      stylers: [
        {
          color: "#000000"
        },
        {
          lightness: 18
        }
      ]
    },
    {
      featureType: "road.local",
      elementType: "geometry",
      stylers: [
        {
          color: "#000000"
        },
        {
          lightness: 16
        }
      ]
    },
    {
      featureType: "transit",
      elementType: "geometry",
      stylers: [
        {
          color: "#000000"
        },
        {
          lightness: 19
        }
      ]
    },
    {
      featureType: "water",
      elementType: "geometry",
      stylers: [
        {
          color: "#000000"
        },
        {
          lightness: 17
        }
      ]
    }
  ];
  $.getScript(
    "https://maps.googleapis.com/maps/api/js?key=AIzaSyCz5z_MjWjROR8Dhr5ayZfRS23l91IvPAk",
    function() {
      var mapCanvas, mapOptions;
      mapCanvas = document.getElementById("map");
      let lat, lng
      if ($(window).width() < 768) {
        lat = -37.770554;
        lng = 144.960935;
      } else {
        lat = -37.768554;
        lng = 144.955935;
      }
      mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 15.97,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        mapTypeControl: false,
        streetViewControl: false,
        styles: styles,
        disableDefaultUI: true
      };
      this.map = new google.maps.Map(mapCanvas, mapOptions);
      googleMap.map = this.map;
      var marker, myLatLng;
      marker = {
        url: window.site.themeurl + "/assets/img/inline/SmallTimeMapMarker.svg",
        scaledSize: new google.maps.Size(78, 101)
      };
      myLatLng = {
        lat: -37.768554,
        lng: 144.960935
      };

      marker = new google.maps.Marker({
        position: myLatLng,
        icon: marker,
        map: googleMap.map
      });
      return;
    }
  );
}
