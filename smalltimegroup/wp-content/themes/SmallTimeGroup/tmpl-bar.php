<?php
/*
 * Template Name: Bar Page
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<main class="c-bar">
    <div class="o-wrapper o-wrapper--small">
        <section class="c-bar__top">
            <h1 class="large"><?php the_title() ?></h1>
            <h3><?php the_field('sub_heading') ?></h3>
            <?php the_content() ?>
        </div>
    </div>
    <section class="c-bar__menu">
        <div class="o-wrapper o-wrapper--small">
            <div class="o-layout o-layout--menu">
                    <div class="o-layout__item u-2/3@tablet u-1/1">
                        <div class="c-bar__menu-side">
							<table>
								<?php $table = get_field('eat_menu_table');
								if ($table) : ?>
									<tr class="c-bar__menu-header">
										<?php foreach ($table['header'] as $key => $title) : ?>
											<?php if ($key === 0) : ?>
												<th><h2><?php echo $title['c']; ?></h2></th>
											<?php else : ?>
												<th><h4><?php echo $title['c']; ?></h4></th>
											<?php endif; ?>
										<?php endforeach; ?>
									</tr>
								<?php endif; ?>
								<?php foreach ($table['body'] as  $row) : ?>
										<tr>
											<?php foreach ($row as $key => $column) : ?>
												<?php if ($key == 0) : ?>
													<td><span class="c-bar__menu-item"><?php echo $column['c']; ?></span></td>
												<?php else : ?>
													<td><span class="c-bar__menu-item-price"><?php echo $column['c']; ?></span></td>
												<?php endif; ?>
											<?php endforeach; ?>
										</tr>
								<?php endforeach; ?>
							</table>
                        </div>
                    </div>
            </div>
            <?php $menu_link = get_field('menu_link') ?>
            <div class="c-bar__menu-link-wrap">
				<a download href="<?php echo $menu_link['url'] ?>" class="c-bar__menu-link"><span><?php echo $menu_link['title'] ?></span><?php svgicon('Download-Button', '20 18.78'); ?></a>
			</div>
        </div>
        <?php $menu_image = get_field('menu_image') ?>
        <div class="c-bar__menu-image" style="background-image: url('<?php echo $menu_image['url'] ?>'); ">

        </div>
    </section>
    <section class="c-bar__slider">
        <?php if (have_rows('slider')) :?>
			<div class="c-slider">
				<?php while(have_rows('slider')): the_row(); ?>
                <?php $image = get_sub_field('image');?>
				<div class="c-slider__slide">
                    <div class="c-slider__bg cover"></div>
                    <div class="c-slider__content">
                        <div class="o-wrapper o-wrapper--small">
                            <div class="c-slider__content-inner">
                                <h2 class="c-slider__header">
                                    <?php the_sub_field('heading') ?>
                                </h2>
                                <?php the_sub_field('content') ?>
                            </div>
                        </div>
					</div>
					<div class="c-slider__image cover" style="background-image: url('<?php echo $image['url'] ?>');">
					</div>
				</div>
			<?php endwhile; ?>
			</div>
		<?php endif; ?>
    </section>
    <section class="c-bar__tv">
        <div class="o-wrapper o-wrapper--small">
            <div class="c-bar__lower-content">
                <h2><?php the_field('lower_heading') ?></h2>
                <?php the_field('lower_content') ?>
            </div>
        </div>
        <div class="o-wrapper o-wrapper--mid">
			<div class="c-events-panel">
				<div class="o-layout o-module">
				<?php
					$date = date_create();
					$events = new WP_Query(array(
						'posts_per_page'=> 4,
						'post_type'	=> 'ra-events',
						'status' => 'published',
						'orderby' => 'meta_value',
						'order' => 'ASC',
						'meta_type' => 'DATETIME',
						'meta_key' => 'date',
						'meta_query' => array(
							array(
								'key' => 'date',
								'value' => date_format($date, 'Y-m-d H:i:s'),
								'compare' => '>',
								'type' => 'DATETIME'
							)
						)
					));
					$orig_query = $wp_query;
					$wp_query = $events;
					if ( have_posts() ) {
						while(have_posts()) {
							the_post();
							get_template_part('partials/loop-events-inner-show');
						}
					}
					$wp_query = $orig_query;
					wp_reset_query();
				?>
				</div>
            </div>
            <?php $lower_link = get_field('lower_link') ?>
            <div class="c-bar__lower-link-wrap">
				<a href="<?php echo $lower_link['url'] ?>" class="c-bar__lower-link"><span><?php echo $lower_link['title'] ?></span><?php svgicon('SmallTime-Icons-Plus-Black', '0 0 16 16'); ?></a>
			</div>
        </div>
    </section>
</main>
<?php endwhile; ?>
<?php get_footer(); ?>