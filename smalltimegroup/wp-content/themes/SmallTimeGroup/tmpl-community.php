<?php
/*
 * Template Name: Community Page
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<main class="c-community">
    <div class="o-wrapper pos-rel">
        <section class="c-community__top-content">
            <h1><?php the_title() ?></h1>
            <h3><?php the_field('community_signup_heading') ?></h3>
        </section>
    </div>
    <section class="c-community__signup">
        <div class="o-wrapper">
            <div class="c-community__signup-inner">
                <h2><?php the_field('community_email_signup_heading') ?></h2>
                <?php echo do_shortcode('[contact-form-7 id="165" title="Community Signup"]') ?>
            </div>
        </div>
    </section>
    <section class="c-community__instagram">
        <div class="o-wrapper">
            <h2 class="o-animateup"><?php the_field('sub_heading') ?></h2>
            <div class="c-community__instagram-content o-animateup">
                <?php the_content() ?>
            </div>
            <div class="c-community__disclaimer o-animateup">
                <?php the_field('disclaimer') ?>
            </div>
        </div>
        <h3 class="o-animateup">Instagram: <a target="_blank" href="https://instagram.com/<?php the_field('instagram_handle', 'options') ?>"><?php the_field('instagram_handle', 'options') ?></a></h3>
        <div class="c-community__instagram-wrap o-animateup">
            <?php echo do_shortcode('[instagram-feed]') ?>
        </div>
    </section>
</main>
<?php endwhile; ?>
<?php get_footer(); ?>

<!-- Form -->

<!-- <div class="c-community__email">
    [email* email placeholder akismet:author_email "Email"]
</div>
<div class="c-community__submit">
    [submit "Join Community"]
</div> -->