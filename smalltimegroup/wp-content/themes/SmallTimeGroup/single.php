<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<main class="c-internal-event">
	<div class="o-wrapper">
		<div class="pos-rel">
			<div class="c-internal-event__top">
				<div class="c-internal-event__breadcrumbs">
					<div class="c-breadcrumbs">
						<?php if(function_exists('bcn_display')) {
							bcn_display();
						}?>
					</div>
				</div>
			</div>
			<div class="c-internal-event__inner-wrap">
				<div class="c-internal-event__content">
					<h1><?php the_title(); ?></h1>
					<div class="c-internal-event__date">
						<?php 
							$date = get_field('date');
							$date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
							$general_timing = get_field('evening_or_early');
							if ($general_timing == "Evening") {
								echo $date->format('D d M Y') . " Evening";
							} else if ($general_timing == "Early") {
								echo $date->format('D d M Y') . " Early";
							} else {
								echo $date->format('D d M Y, g:iA') . " AEST";  
							}
							$video_id = get_field('video_id');
							$type_of_video = get_field('type_of_video');
						?>
					</div>
					<div class="c-internal-event__details">
						<?php the_content(); ?>
					</div>
					<?php $links = get_field('artist_links', $latest_broadcast) ?>
					<?php if ($links): ?>
					<div class="c-internal-event__artist-links">
						<p>Artist Links: 
							</br>
						<?php 
							for ($i = 0; $i < count($links); $i++) { 
								if($i > 0) : ?>
									<span> | </span><a target="_blank" href="<?php echo $links[$i]['link']['url'] ?>"><?php echo $links[$i]['link']['title'] ?></a>
								<?php else : ?>
									<a target="_blank" href="<?php echo $links[$i]['link']['url'] ?>"><?php echo $links[$i]['link']['title'] ?></a>
								<?php endif;
							}
						?>
						</p>
					</div>
					<?php endif; ?>
					<?php $ticket_link = get_field('ticket_link');
						if ($ticket_link) : ?>
							<div class="c-internal-event__tickets">
								<a class="o-btn" target="_blank" href="<?php echo $ticket_link['url'] ?>">
									<?php echo $ticket_link['title'] ?>
								</a>
							</div>
					<?php endif; ?>
					<?php if($video_id) : ?>
						<?php if ( $type_of_video == "youtube" ) : ?>
							<a style="margin-top:24px;" href="https://www.youtube.com/embed/<?php echo $video_id ?>?autoplay=1" 
							class="js-fancybox fancybox-video fancybox.iframe o-btn">
								watch
							</a>
						<?php else : ?>
							<a style="margin-top:24px;" href="https://www.facebook.com/v2.5/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fvideo.php%3Fv%3D<?php echo $video_id; ?>" 
							class=" js-fancybox fancybox-video fancybox.iframe o-btn">
								watch
							</a>
						<?php endif; ?>
					<?php endif; ?>
				</div>
				<?php $image = get_the_post_thumbnail_url(); ?>
				<div class="c-internal-event__image" style="background-image: url('<?php echo $image ?>');">
					<?php if($video_id) : ?>
						<?php if ($type_of_video == "youtube" ) : ?>
							<a href="https://www.youtube.com/embed/<?php echo $video_id ?>?autoplay=1" class="js-fancybox fancybox-video fancybox.iframe c-internal-event__play">
								<?php svgicon('Play-Button', '0 0 139.241 139.241'); ?>
							</a>
						<?php else :?>
							<a href="https://www.facebook.com/v2.5/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fvideo.php%3Fv%3D<?php echo $video_id; ?>" class="js-fancybox fancybox-video fancybox.iframe c-internal-event__play">
								<?php svgicon('Play-Button', '0 0 139.241 139.241'); ?>
							</a>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</main>
<?php endwhile; ?>

<?php get_footer(); ?>