<?php
/*
 * Template Name: TV Page
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<?php

    $image = get_the_post_thumbnail_url();
    $date = date_create();
    $future_broadcasts = new WP_Query(array(
        'posts_per_page'=> -1,
        'post_type'	=> 'post',
        'status' => 'publish',
        'orderby' => 'meta_value',
        'order' => 'ASC',
        'meta_type' => 'DATETIME',
        'meta_key' => 'date',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'end_date',
                'value' => date_format($date, 'Y-m-d H:i:s'),
                'compare' => '>',
                'type' => 'DATETIME'
            ),
        )
    ));
    $future_broadcasts->posts = ra_filter_broadcasts($future_broadcasts->posts);
    $stream_on = false;
    $latest_broadcast = '';
    foreach ($future_broadcasts->posts as $broadcast) {
        $start_date_str = get_field('date', $broadcast->ID);
        $end_date = get_field('end_date', $broadcast->ID);
        if (intval(strtotime($start_date_str)) < intval( strtotime("now") )) {
            $latest_broadcast = $broadcast;
            $stream_on = true;
            break;
        } else {
            $latest_broadcast = $broadcast;
            break;
        }
    }
    $latest_image = get_the_post_thumbnail_url($latest_broadcast);
    $next_broadcast_time = get_field('date', $latest_broadcast);
    $date = get_field('date', $latest_broadcast->ID);
    $date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
    $general_timing = get_field('evening_or_early', $latest_broadcast->ID);
    if ($general_timing) {
        $next_broadcast_date = DateTime::createFromFormat('Y-m-d H:i:s', $next_broadcast_time);
        if (date('Y-m-d') == $date->format('Y-m-d')) {
            $today = true;
        }
    } else {
        $today = false;
    }
    if (get_field('stream_override', 'options')) {
        $stream_on = true;
    }
?>
<main class="c-tv" data-steam="<?php echo ($stream_on ? "on" : "") ?>">
    <?php if ($latest_broadcast) : ?>
        <div class="c-tv__header c-tv__header--live js-stream-header" style="display: <?php echo ($stream_on ? "" : "none") ?>; background-image: url('<?php echo ($latest_image ? $latest_image : $image) ?>');">
            <h1 class="c-tv__title">
                <?php the_title() ?>
            </h1>
            <div class="c-tv__play js-show-stream">
                <?php svgicon('Play-Button', '0 0 139.241 139.241'); ?>
            </div>
            <h2>View Live Stream</h2>
            <div class="c-tv__static js-static cover">
                <video playsinline autoplay loop muted src="<?php echo ASSETS ?>/video/Small-Time-VHS-Effect.mp4"></video>
            </div>
        </div>
        <div class="plyr__video-embed js-stream c-tv__stream" id="player" style="display:none;">
            <iframe
                width="100%"
                height="690"
                src="https://www.youtube.com/embed/<?php the_field('stream_embed_code', 'options') ?>?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1"
                allowfullscreen
                allowtransparency
                allow="autoplay"
            ></iframe>
        </div>
        <div class="c-tv__header" style="display: <?php echo ($stream_on ? "none" : "") ?>; background-image: url('<?php echo ($latest_image ? $latest_image : $image) ?>');">
            <h1 class="c-tv__title">
                <?php the_title() ?>
            </h1>
            <h3>Next Broadcast</h3>
            <div class="c-tv__countdown"  id="countdown" data-date="<?php echo ($today ? "" : $next_broadcast_time) ?>" style="opacity: <?php echo ($today ? "1" : "0") ?>;">
                <?php if($today) :?>
                <div class="today">
                    <?php
                    if ($general_timing == "Evening") {
                        echo "Tonight";
                    } else if ($general_timing == "Early") {
                        echo "Today";
                    } ?>
                </div>
                <?php else : ?>
                <div class="days">
                    <p>15</p>
                    <h3>Days</h3>
                </div>
                <div class="separator">
                    :
                </div>
                <div class="hours">
                    <p>07</p>
                    <h3>Hours</h3>
                </div>
                <div class="separator">
                    :
                </div>
                <div class="minutes">
                    <p>25</p>
                    <h3>Minutes</h3>
                </div>
                <div class="separator hide-mob">
                    :
                </div>
                <div class="seconds">
                    <p>06</p>
                    <h3>Seconds</h3>
                </div>
                <?php endif; ?>
            </div>
            <video class="c-tv__bg-static" playsinline autoplay loop muted src="<?php echo ASSETS ?>/video/Small-Time-VHS-Effect.mp4"></video>
        </div>
        <div class="o-wrapper">
            <div class="c-tv__next-broadcast">
                <div class="c-tv__live-badge" style="display: <?php echo ($stream_on ? "block" : "none") ?>;">
                    <img src="<?php echo ASSETS ?>/img/LiveOnAir.gif" alt="">
                </div>
                <?php $broadcast_type = get_the_category($latest_broadcast); ?>
                    <h2><?php echo ($broadcast_type[0]->name . ": " . $latest_broadcast->post_title )?></h2>
                    <h5><?php
                        if($date) {
                            if ($general_timing == "Evening") {
                                    echo $date->format('D d M Y') . " Evening";
                                } else if ($general_timing == "Early") {
                                    echo $date->format('D d M Y') . " Early";
                                } else {
                                    echo $date->format('D d M Y, g:iA') . " AEST";
                                }
                            }
                        ?>
                    </h5>
                    <p><?php echo $latest_broadcast->post_content ?></p>
                    <?php $links = get_field('artist_links', $latest_broadcast) ?>
                    <?php if ($links) : ?>
                    <div class="c-tv__artist-links">
                        <p>Artist Links:
                        <?php
                            for ($i=0; $i < count($links); $i++) {
                                if($i > 0) : ?>
                                    <span> | </span><a target="_blank" href="<?php echo $links[$i]['link']['url'] ?>"><?php echo $links[$i]['link']['title'] ?></a>
                                <?php else : ?>
                                    <a target="_blank" href="<?php echo $links[$i]['link']['url'] ?>"><?php echo $links[$i]['link']['title'] ?></a>
                                <?php endif;
                            }
                        ?>
                        </p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (count($future_broadcasts->posts) > 1) : ?>
    <div class="c-tv__upcoming">
        <div class="o-wrapper">
            <h2 class="o-animateup"><?php the_field('upcoming_title') ?></h2>
            <?php
            $i = 0;
            foreach($future_broadcasts->posts as $future_broadcast) : ?>
                <?php
                    $broadcast_date = get_field('date', $future_broadcast);
                    $broadcast_date_format = DateTime::createFromFormat('Y-m-d H:i:s', $broadcast_date);
                ?>
                <div class="c-tv__single-broadcast <?php echo ($i < 7 ? "o-animateup" : "js-upcoming-extra")?> "style="display: <?php echo ($i < 7 ? "flex" : "none")?>;">
                    <span><?php echo $broadcast_date_format->format("l d.m.y") ?></span>
                    <a href="<?php  echo get_the_permalink($future_broadcast->ID) ?>">
                        <?php echo $future_broadcast->post_title ?>
                        <?php svgicon('SmallTime-Icons-Plus-OffWhite', '0 0 16 16'); ?>
                    </a>
                </div>
            <?php
                $i++;
            endforeach;?>
            <div class="c-tv__upcoming-link-wrap o-animateup u-animation-delay-1">
                <a javascript="void:(0)" class="c-tv__upcoming-link js-upcoming-toggle">
                    <span class="more">view more <?php svgicon('SmallTime-Icons-Plus-OffWhite', '0 0 16 16'); ?></span>
                    <span class="less">view less <?php svgicon('Minus-Black', '0 0 16 4'); ?></span>
                </a>
            </div>
        </div>
    </div>
    <?php endif; ?>
    
    <?php
        $terms = get_terms( 'post_tag', array(
            'hide_empty' => true,
        ) );
        
        if (!$date) {
            $date = date_create();
        }
        foreach( $terms as $term ) :
            $tv_posts = new WP_Query(array(
            'posts_per_page'=> -1,
            'status' => 'published',
            'orderby' => 'date',
            'order'   => 'ASC',
            'tag' => $term->slug,
            'meta_query' => array(
                array(
                    'key' => 'end_date',
                    'value' => date_format($date, 'Y-m-d H:i:s'),
                    'compare' => '<',
                    'type' => 'DATETIME'
                )
                )
            ));
            $tv_posts->posts = ra_filter_broadcasts($tv_posts->posts);
    ?>

            <?php if($tv_posts->posts) : ?>
                <div class="c-tv__past">
                    <div class="o-wrapper">
                        <div class="c-tv__past-top o-animateup">
                            <h1><?php echo $term->name; ?></h1>
                            <div class="c-filters c-filters--tv">
                                    <div class="c-filters__single js-event-filter selected" data-filter="all">
                                        All
                                    </div>
                                    <div class="c-filters__single js-event-filter" data-filter="video">
                                        Videos
                                    </div>
                                    <div class="c-filters__single js-event-filter" data-filter="podcast">
                                        Podcasts
                                    </div>
                            </div>
                        </div>
                        <div class="c-tv__listing">
                        
                            <?php
                            
                                $orig_query = $wp_query;
                                $wp_query  = $tv_posts;
                                get_template_part('partials/loop-tv');
                                $wp_query = $orig_query;
                                wp_reset_query();
                            ?>
                            <div class="c-tv__load-more-wrap o-animateup">
                                <a href="javascript:void(0);" class="c-tv__load-more js-show-more"><span>load more</span><?php svgicon('SmallTime-Icons-Plus-Black', '0 0 16 16'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        
</main>
<?php endwhile; ?>
<?php get_footer(); ?>