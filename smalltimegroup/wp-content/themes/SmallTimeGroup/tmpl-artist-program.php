<?php
/*
 * Template Name: Artist Program Page
 */
?>
<!-- Artist Program is renamed to Production -->
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<main class="c-program" id="top">
    <section class="c-program__top">
        <?php $top_section = get_field('top_section') ?>
        <div class="o-wrapper o-wrapper--small">
            <div class="pos-rel">
                <div data-rellax-speed="-2" style="background-image: url('<?php echo $top_section['top_portrait']['url'] ?>');" class="c-program__top-portrait rellax"></div>
                <div data-rellax-speed="-1" style="background-image: url('<?php echo $top_section['top_landscape']['url'] ?>');" class="c-program__top-landscape rellax"></div>
                <div class="c-program__top-inner">
                    <h1><?php the_title() ?></h1>
                    <h4><?php echo $top_section['sub_heading'] ?></h4>
                    <?php the_content() ?>
                    <!-- <div class="c-program__signup">
                        <?php //echo do_shortcode('[contact-form-7 id="571" title="Artist Info Pack Signup"]') ?>
                    </div> -->
                    <a class="c-program__link js-download-link" href="<?php echo $top_section['link']['url'] ?>"><?php echo $top_section['link']['title'] ?><?php svgicon('Download-Button', '0 0 20 18.78'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="c-program__middle js-change-color">
        <div data-rellax-speed="-2"
            data-rellax-mobile-speed="0"
            data-rellax-tablet-speed="0" style="background-image: url('<?php echo $top_section['top_portrait']['url'] ?>');" class="c-program__top-portrait c-program__top-portrait--mobile rellax"></div>
        <div data-rellax-speed="-1"
            data-rellax-mobile-speed="0"
            data-rellax-tablet-speed="0" style="background-image: url('<?php echo $top_section['top_landscape']['url'] ?>');" class="c-program__top-landscape c-program__top-landscape--mobile rellax"></div>
        <?php $mid_portrait = get_field('middle_portrait') ?>
        <?php $mid_landscape = get_field('middle_landscape');?>
        <div class="o-wrapper o-wrapper--small">
            <div class="pos-rel">
                <div class="c-program__middle-portrait rellax"
                data-rellax-speed="-2">
                    <img data-src="<?php echo $mid_portrait['url'] ?>" alt="" class="lazy">
                </div>
                <div data-rellax-speed="-1" class="c-program__middle-landscape rellax">
                    <img data-src="<?php echo $mid_landscape['url'] ?>" alt="" class="lazy">
                </div>
                <div class="c-program__pillars-wrap">
                    <h2 class="o-animateup"><?php the_field('pillar_heading') ?></h2>
                    <?php if (have_rows('pillars')) :
                        while(have_rows('pillars')) : the_row();?>
                        <div class="c-program__pillar o-animateup">
                            <h3><?php the_sub_field('title') ?></h3>
                            <?php the_sub_field('content') ?>
                        </div>
                    <?php endwhile;
                    endif; ?>
                </div>
				<?php $below_pillar = get_field('below_pillars_section'); ?>
				<?php if ($below_pillar) : ?>
					<div class="c-program__below-pillars">
						<?php $lower_image = $below_pillar['below_pillar_image'] ?>
						<?php if ($lower_image) : ?>
							<div class="c-program__below-image rellax o-animateup">
								<img data-src="<?php echo $lower_image['url'] ?>" alt="" class="lazy">
							</div>
						<?php endif; ?>
						<div class="c-program__below-pillars-content o-animateup">
							<h2><?php echo $below_pillar['below_pillars_heading'] ?></h2>
							<?php echo $below_pillar['below_pillars_content'] ?>
						</div>
					</div>
				<?php endif; ?>
            </div>
        </div>
    </section>
    <?php $faq = get_field('faq_section');?>
	<?php if ($faq && $faq['questions'] && count($faq['questions']) > 0) : ?>
		<section class="c-program__faq">
			<div class="o-wrapper o-wrapper--small">
				<div class="o-layout o-module o-module--reverse-tabletWide">
					<div class="o-layout__item o-module__item u-1/2@tabletWide u-1/1">
						<div class="c-program__faq-content">
							<h3 class="o-animateup"><?php echo $faq['heading'] ?></h3>
							<?php foreach($faq['questions'] as $question) : ?>
								<div class="c-program__faq-question o-animateup">
									<h5>Q: <?php echo $question['question'] ?></h5>
									<p>A: <?php echo $question['answer'] ?></p>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
					<div class="o-layout__item o-module__item u-1/2@tabletWide u-1/1">
						<div data-src="<?php echo $faq['image']['url'] ?>" alt="<?php echo $faq['image']['title'] ?>" class="c-program__faq-image o-animateup lazy"></div>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
    <section class="c-program__application js-change-color">
        <?php $application = get_field('application_section') ?>
        <div class="o-wrapper">
            <div class="c-program__application-content">
                <h2 class="o-animateup"><?php echo $application['heading'] ?></h2>
                <h3 class="o-animateup"><?php echo $application['sub_heading'] ?></h3>
                <h3 class="o-animateup"><?php echo $application['sub_heading_2'] ?></h3>
            </div>
            <div class="c-program__application-form-wrap">
                <?php echo do_shortcode('[contact-form-7 id="351" title="Artist Development Program Application"]') ?>
            </div>
        </div>
    </section>
</main>
<?php endwhile; ?>
<?php get_footer(); ?>


<!-- Signup Form -->

<!-- <div class="c-program__email">
    [email* email placeholder akismet:author_email "Enter email to receive info pack"]
</div>
<div class="c-program__submit">
    [submit "Submit"]
</div> -->

<!-- Application Form -->

<!-- <div class="c-application-form">
    <div class="t-align-right">
        <p>*Required Field</p>
    </div>
    <div class="o-layout o-layout--contact">
        <div class="o-layout__item u-1/2@tabletWide u-1/1 o-animateup">
            [text* full-name placeholder "Name*"]
        </div>
        <div class="o-layout__item u-1/2@tabletWide u-1/1 o-animateup">
            [text* artist-name placeholder "Artist Name*"]
        </div>
        <div class="o-layout__item u-1/2@tabletWide u-1/1 o-animateup">
            [email* email placeholder "Email*"]
        </div>
        <div class="o-layout__item u-1/2@tabletWide u-1/1 o-animateup">
            [tel phone placeholder "Phone"]
        </div>
        <div class="o-layout__item o-animateup">
            [textarea* bio placeholder "Brief artist bio/summary*"]
        </div>
        <div class="o-layout__item o-animateup">
            [textarea* goals placeholder "Brief outline of goals as an artist*"]
        </div>
        <div class="o-layout__item o-animateup">
            [textarea* reason placeholder "Reason for interest in program*"]
        </div>
        <div class="o-layout__item u-1/2@tabletWide u-1/1 o-animateup">
            [text social-link-1 placeholder "Social Link 1"]
        </div>
        <div class="o-layout__item u-1/2@tabletWide u-1/1 o-animateup">
            [text social-link-2 placeholder "Social Link 2"]
        </div>
        <div class="c-application-form__upload o-animateup">
            [file upload limit:8000000 filetypes:wav|mp3]
        </div>
        <div class="o-layout__item t-align-left o-animateup">
            <span class="c-application-form__upload-text">(Can be a simple phone recording)</span>
        </div>
        <div class="c-application-form__submit o-animateup">
            <div class="c-application-form__submit-inner">
                [submit "Sign up today"]
            </div>
        </div>
    </div>
</div> -->