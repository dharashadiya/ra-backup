<?php get_header(); ?>

<div class="c-search">
	<main id="Main" class="c-main-content o-main" role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="c-cms-content c-search-content o-wrapper">
				<h1><?php printf( __( 'Search results: "%s"'), get_search_query() ); ?></h1>
				<?php
				$search_results = [];
				if ( have_posts() ) :
					while ( have_posts() ) { 
						the_post();	
						$search_results[$post->post_type][] = $post->ID;
					}
				?>
				<?php if ($search_results['post']) : ?>
					<h4>Events</h4>
					<section class="c-search__section c-search__section--events">
						<div class="c-events-panel">
							<div class="o-layout">
							<?php foreach ($search_results['post'] as $event) : 
								$post = get_post($event);
								get_template_part( 'partials/loop-events-inner' );
								wp_reset_postdata();
							endforeach; ?>
							</div>
						</div>
					</section>
				<?php endif; ?>
				<?php if ($search_results['page']): ?>
					<section class="c-search__section c-search__section--pages">
						<h4>Pages</h4>
						<div class="c-page-panel">
							<div class="o-layout">
							<?php foreach ($search_results['page'] as $page) : 
								$post = get_post($page);
								get_template_part( 'partials/loop-pages-search' );
								wp_reset_postdata();
							endforeach; ?>
							</div>
						</div>
					</section>
				<?php endif; ?>
				<?php else : ?>
					<p>No search results found.</p>
				<?php endif; ?>
			</div>
		</article>
	</main>
</div>

<?php get_footer(); ?>