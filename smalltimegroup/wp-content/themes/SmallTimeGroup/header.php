<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
	<title><?php wp_title(); ?> </title>

	<?php if(is_local_or_staging()) : ?>
		<meta name="robots" content="none" />
		<meta name="robots" content="noindex" />
		<meta name="googlebot" content="none" />
		<meta name="googlebot" content="noindex" />
	<?php endif; ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">
	<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '194863245161881');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=194863245161881&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->

	<?php wp_head(); ?>
	<?php include('partials/inlinescripts.min.php'); ?>
</head>

<body <?php body_class(); ?>>
<div class="site-wrap">
	<div class="c-top">
		<header class="c-header">
			<div class="o-wrapper">
				<div class="c-header__inner-wrap">
					<div class="c-header__left">
						<div class="c-header__logo">
							<a href="/">
								<?php svgicon('small-time-logo', '0 0 28 31'); ?>
							</a>
						</div>
						<div class="c-header__nav">
							<nav class="c-nav" role="navigation">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'primary-menu',
									'container' => false,
									'menu_class' => 'c-nav__inner',
									'menu_id' => 'menu'
								));
							?>
							</nav>
						</div>
					</div>
					<div class="c-header__right">
						<div class="c-header__socials">
							<?php get_template_part('partials/socials') ?>
						</div>
						<div class="c-header__search">
								<?php svgicon('search', '0 0 18.21 18.94'); ?>
						</div>
						<a href="javascript:void(0);" class="c-header__hamburger js-search-close js-toggle-menu" title="Open Close Navigation">
							<span class="c-toggle-menu"></span>&nbsp;
						</a>
					</div>
				</div>
			</div>
		</header>
	</div>
	<div class="c-mobile-nav">
		<nav class="c-mobile-nav__outer" role="navigation">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'mobile-menu',
					'container' => false,
					'menu_class' => 'c-mobile-nav__inner',
					'menu_id' => 'mob-menu'
				));
			?>
		</nav>
	</div>
	<!-- Search Overlay -->
	<div class="c-search-overlay" style="display:none;">
		<div class="c-search-overlay__close is-open">
			<span class="c-toggle-menu"></span>&nbsp;
		</div>
		<div class="o-wrapper">
			<form role="search" method="get" class="c-search-overlay__form search-form" action="<?php echo home_url( '/' ); ?>">
				<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
				<input type="search" class="c-search-overlay__input search-field" placeholder="Type something..." value="<?php echo get_search_query() ?>" name="s" title="Search for:" id="search" autocomplete="off"/>
				<span class="c-search-overlay__prompt">Click </span><input class="c-search-overlay__prompt-submit" type="submit" value="here"/><span class="c-search-overlay__prompt"> or press enter to search.</span>
			</form>
		</div>
	</div>

	<div class="c-content">