<?php
/*
 * Template Name: About Page
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<main class="c-about">
    <?php $image = get_the_post_thumbnail_url();
    $mobile_image = get_field('mobile_image') ?>
    <div class="c-about__top" style="background-image: url('<?php echo $image ?>');">    
        <div class="c-about__bg cover"></div>
        <div class="o-wrapper h-100">
            <div class="c-about__content">
                <h1><?php the_title() ?></h1>
                <?php the_content() ?>
            </div>
        </div>
    </div>
    <div class="c-about__top c-about__top--mobile" style="background-image: url('<?php echo $mobile_image['url'] ?>');">
        <div class="c-about__bg cover"></div>
        <div class="o-wrapper h-100">
            <div class="c-about__content">
                <h1><?php the_title() ?></h1>
                <?php the_content() ?>
            </div>
        </div>
    </div>
    <div class="o-wrapper">
        <div class="c-about__why">
            <h2><?php the_field('second_heading') ?></h2>
            <?php the_field('second_content') ?>
        </div>
        <div class="c-about__pillar-container">
            <?php if (have_rows('pillars')) :
                $i = 0;
                while(have_rows('pillars')): the_row();?>
                <div class="c-about__pillar c-about__pillar--<?php echo $i ?>">
                    <div class="c-about__pillar-image c-about__pillar-image--<?php echo $i ?> o-animateup">
                        <?php $sub_image = get_sub_field('image') ?>
                        <img src="<?php echo $sub_image['url']?>" alt="<?php echo $sub_image['title']?>">
                    </div>
                    <div class="c-about__pillar-content rellax o-animateup" data-rellax-mobile-speed="0"
                    data-rellax-tablet-speed="0">
                        <h4><?php the_sub_field('sub_heading') ?></h4>
                        <h2><?php the_sub_field('heading') ?></h2>
                        <?php the_sub_field('content') ?>
                        <?php 
                            $link = get_sub_field('link');
                        ?>
                        <a href="<?php echo $link['url'] ?>">
                            <span>
                                <?php echo $link['title'] ?>
                            </span>
                            <?php svgicon('SmallTime-Icons-Plus-Black', '0 0 16 16'); ?>
                        </a>
                    </div>
                </div>
                <?php $i++ ?>
            <?php endwhile;
            endif; ?>
        </div>
        <div class="c-about__contact o-animateup">
            <a href="/contact">Any Questions? <span>Get in Touch</span></a>
        </div>
    </div>
</main>
<?php endwhile; ?>
<?php get_footer(); ?>