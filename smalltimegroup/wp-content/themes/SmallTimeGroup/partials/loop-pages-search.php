
<?php $image = get_the_post_thumbnail_url();?>
<div class="o-layout__item u-1/3@tablet u-1/1@mobile js-filter-item" data-filter="<?php echo $filter ?>">
    <div class="c-page-panel__single">
        <a href="<?php echo get_the_permalink() ?>">
            <div class="c-page-panel__image" style="background-image: url('<?php echo $image ?>');">
            </div>
            <div class="c-page-panel__title">
                <?php the_title(); ?>
            </div>
        </a>
    </div>
</div>
