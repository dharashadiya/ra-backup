
<?php
    $categories = get_the_category();
    $image = get_the_post_thumbnail_url();
    $terms = get_the_terms(get_the_ID(), 'category');
    if (has_category('live-set') || has_category('interview')) {
        $filter = 'video';
    } else {
        $filter = 'podcast';
    }
	$video_id = get_field('video_id');
?>
<div class="o-layout__item u-1/3@tablet u-1/1@mobile js-filter-item" data-filter="<?php echo $filter ?>">
    <div class="c-tv-panel__single">
		<?php if ($video_id) : ?>
        	<a href="https://www.youtube.com/embed/<?php the_field('video_id') ?>?autoplay=1" class="js-fancybox fancybox-video fancybox.iframe">
		<?php else : ?>
        	<a href="javascript:void(0);" class="">
		<?php endif; ?>
            <div class="c-tv-panel__image" style="background-image: url('<?php echo $image ?>');">
                <div class="c-tv-panel__image-overlay">
                    <div class="c-tv-panel__category">
                        <?php echo $terms[0]->name ?>
                    </div>
                    <div class="c-tv-panel__title">
                        <?php the_title(); ?>
                    </div>
                    <?php if($terms[0]->slug === 'podcast') : ?>
                        <div class="c-tv-panel__icon c-tv-panel__icon--podcast">
                            <?php svgicon('SmallTime-Icons-Podcasts', '0 0 11.25 18'); ?>
                        </div>
                    <?php elseif($video_id && ($terms[0]->slug === 'interview' || $terms[0]->slug === 'live-set')) :  ?>
                        <div class="c-tv-panel__icon">
                            <?php svgicon('SmallTime-Icons-YouTube', '0 0 23.34 16.05'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <span class="c-tv-panel__date">
                <?php $date = get_the_date('D d, M') ?>
                <?php echo $date; ?>
            </span>
            <h3 class="c-tv-panel__heading">
                <?php the_title() ?>
            </h3>
            <p class="c-tv-panel__blurb">
                <?php echo wp_trim_words( get_the_content(), 24, '...' ) ?>
            </p>
        </a>
    </div>
</div>