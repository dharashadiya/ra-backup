
<?php if ( have_posts() ) : ?>
<div class="c-events-panel">
    <div class="o-layout o-module">
        <?php while ( have_posts() ) : the_post() ; ?>
            <?php get_template_part('partials/loop-events-inner') ?>
        <?php endwhile; ?>
    </div>
</div>
<?php endif; ?>
