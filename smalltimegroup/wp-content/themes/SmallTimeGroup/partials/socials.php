<?php 
    $facebook = get_field('facebook', 'options');
    $instagram = get_field('instagram', 'options');
    $spotify = get_field('spotify', 'options');
    $youtube = get_field('youtube', 'options');
    $podcast = get_field('podcast', 'options');
    if ($facebook && $instagram && $spotify && $youtube && $podcast) {
        $all_class= "c-socials--all";
    } else {
        $all_class = "";
    }
?>
<div class="c-socials <?php echo $all_class ?>">
    <?php if ($facebook): ?>
        <a href="<?php echo $facebook['url'] ?>" title="Facebook Link">
            <?php svgicon('SmallTime-Icons-Facebook', '0 0 9 18'); ?>
        </a>
    <?php endif; ?>
    <?php if ($instagram): ?>
        <a href="<?php echo $instagram['url'] ?>" title="Instagram Link">
        <?php svgicon('SmallTime-Icons-Instagram', '0 0 18 18'); ?>
        </a>
    <?php endif; ?>
    <?php if ($spotify): ?>
        <a href="<?php echo $spotify['url'] ?>" title="Spotify Link">
            <?php svgicon('SmallTime-Icons-Spotify', '0 0 18 18'); ?>
        </a>
    <?php endif; ?>
    <?php if ($youtube): ?>
        <a href="<?php echo $youtube['url'] ?>" title="Youtube Link">
            <?php svgicon('SmallTime-Icons-YouTube', '0 0 23.34 16.05'); ?>
        </a>
    <?php endif; ?>
    <?php if ($podcast): ?>
        <a href="<?php echo $podcast['url'] ?>" title="Podcast Link">
            <?php svgicon('SmallTime-Icons-Podcasts', '0 0 11.25 18'); ?>   
        </a>
    <?php endif; ?>

</div>