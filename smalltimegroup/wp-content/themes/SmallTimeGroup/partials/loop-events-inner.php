<?php 

    $event_info = get_query_var('event_info');
    $date = get_field('date');
    $now = time();
    $date_format = strtotime($date);
    $date_diff_end = $now - $date_format;
    $days_until_end =  round($date_diff_end / (60 * 60 * 24));
    $tags = get_the_tags();

    if ($date_format < $now) {
        $hide_event = true;
        $event_filter = 'past-event';
    } else  {
        $hide_event = false;
        $event_filter = 'upcoming-event';
    }
    $events_to_show = 8;
    if ($event_info['hide_upcoming'] || $event_info['index'] > $events_to_show) {
        $hide_event = true;
    }
    $background_color = get_field('background_color', $tags[0]);
?>
<div class="o-layout__item o-module__item u-1/2@tablet u-1/4@tabletWide u-1/1 js-filter-item" data-filter="<?php echo $event_filter ?>"  style="<?php echo ($hide_event ? "display: none;" : "") ?>">
    <div class="c-events-panel__single <?php echo $background_color; ?>">
    <?php $image = get_the_post_thumbnail_url(); ?>
        <a href="<?php echo get_the_permalink() ?>">
            <div class="c-events-panel__image lazy" data-src="<?php echo $image ?>">
            </div>
            <span class="c-events-panel__date">
                <?php $date = DateTime::createFromFormat('Y-m-d H:i:s', $date) ?>
                <?php echo $date->format('D d, M'); ?>
            </span>
            <h3 class="c-events-panel__title">
                <?php the_title() ?>
            </h3>
            <p class="c-events-panel__blurb">
                <?php $content = get_the_content();
                echo substr($content, 0, 120);?>...
            </p>
            <span class="c-events-panel__link">
                Read More...
            </span>
        </a>
    </div>
</div>