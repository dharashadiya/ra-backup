
<?php if ( have_posts() ) : ?>
<div class="c-tv-panel">
    <div class="o-layout">
        <?php $i = 0; ?>
        <?php while ( have_posts() ) : the_post() ;
		if ($post) :
            $categories = get_the_category();
            $image = get_the_post_thumbnail_url();
            $terms = get_the_terms(get_the_ID(), 'category');
            if (has_category('live-set') || has_category('interview')) {
                $filter = 'video';
            } else {
                $filter = 'podcast';
            }
			$video_id = get_field('video_id');
            $type_of_video = get_field('type_of_video');
            $date = get_field('date');
        ?>
    
        <div style="<?php echo($i > 8 ? "display:none;" : "") ?> (" class="o-layout__item u-1/3@tabletWide u-1/2@tablet u-1/1@mobile js-filter-item" data-filter="<?php echo $filter ?>">
            <div class="c-tv-panel__single o-animateup">
				<?php if ($video_id && $type_of_video == "youtube" ) : ?>
                	<a href="https://www.youtube.com/embed/<?php echo $video_id; ?>?autoplay=1" class="js-fancybox fancybox-video fancybox.iframe">
                <?php elseif ($video_id && $type_of_video == "facebook" ) : ?>
                    <a href="https://www.facebook.com/v2.5/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fvideo.php%3Fv%3D<?php echo $video_id; ?>" class="fb-video js-fancybox fancybox-video fancybox.iframe">
				<?php else : ?>
                	<a href="javascript:void(0);" class="">
				<?php endif; ?>
                        <div class="c-tv-panel__image lazy" data-src="<?php echo $image ?>">
                            <div class="c-tv-panel__image-overlay">
                                <div class="c-tv-panel__category">
                                    <?php echo $terms[0]->name ?>
                                </div>
                                <div class="c-tv-panel__title">
                                    <?php the_title(); ?> 
                                    <?php $tags = get_the_tags(); ?>
                                    <?php if($tags) : ?>
                                        (<?php foreach($tags as $tag) : ?><?php echo $tags[0] != $tag ? ', ' : ''; ?><?php echo $tag->name; ?><?php endforeach; ?>)
                                    <?php else: ?>
                                    <?php endif; ?> 
                                </div>
                                <?php if($terms[0]->slug === 'podcast') : ?>
                                    <div class="c-tv-panel__icon c-tv-panel__icon--podcast">
                                        <?php svgicon('SmallTime-Icons-Podcasts', '0 0 11.25 18'); ?>
                                    </div>
                                <?php elseif($video_id && ($terms[0]->slug === 'interview' || $terms[0]->slug === 'live-set')) :  ?>
                                    <div class="c-tv-panel__icon <?php echo $type_of_video; ?>">
                                        <?php if( $type_of_video == "youtube" ) : ?>
                                            <?php svgicon('SmallTime-Icons-YouTube', '0 0 23.34 16.05'); ?>
                                        <?php elseif( $type_of_video == "facebook" ) : ?>
                                            <?php svgicon('SmallTime-Icons-Facebook', '0 0 9 18'); ?>
                                            <?php else : ?>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </a>
                    <span class="c-tv-panel__date">
                        <?php //$date = get_the_date('D d, M') ?>
                        <?php $date = DateTime::createFromFormat('Y-m-d H:i:s', $date) ?>
                        <?php echo $date->format('D d, M'); ?>
                              
                    </span>
                     <a href="<?php echo get_the_permalink() ?>">
                        <h3 class="c-tv-panel__heading"> 
                        <?php the_title() ?>
                        </h3>
                    </a> 
                    <p class="c-tv-panel__blurb">
                        <?php echo wp_trim_words( get_the_content(), 24, '...' ) ?>
                    </p>
            </div>
        </div>
        <?php $i++;
		endif;
		endwhile; ?>
    </div>
</div>
<?php endif; ?>
