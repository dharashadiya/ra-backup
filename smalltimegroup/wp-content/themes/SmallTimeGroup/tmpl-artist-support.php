<?php
/*
 * Template Name: Artist Support Page
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<main class="c-program" id="top">
    <section class="c-program__top">
        <?php $top_section = get_field('top_section') ?>
        <div class="o-wrapper o-wrapper--small">
            <div class="pos-rel">
                <div data-rellax-speed="-2" style="background-image: url('<?php echo $top_section['top_portrait']['url'] ?>');" class="c-program__top-portrait rellax"></div>
				<div data-rellax-speed="-1" style="background-image: url('<?php echo $top_section['top_landscape']['url'] ?>');" class="c-program__top-landscape rellax"></div>
                <div class="c-program__top-inner">
                    <h1><?php the_title() ?></h1>
                    <h4><?php echo $top_section['sub_heading'] ?></h4>
                    <?php the_content() ?>
                </div>
            </div>
        </div>
    </section>
    <section class="c-program__middle js-change-color">
          <div data-rellax-speed="-2"
            data-rellax-mobile-speed="0"
            data-rellax-tablet-speed="0" style="background-image: url('<?php echo $top_section['top_portrait']['url'] ?>');" class="c-program__top-portrait c-program__top-portrait--mobile rellax"></div>
        <div data-rellax-speed="-1"
            data-rellax-mobile-speed="0"
            data-rellax-tablet-speed="0" style="background-image: url('<?php echo $top_section['top_landscape']['url'] ?>');" class="c-program__top-landscape c-program__top-landscape--mobile rellax"></div>
        <?php $mid_portrait = get_field('middle_portrait') ?>
        <?php $mid_landscape = get_field('middle_landscape');?>
        <div class="o-wrapper o-wrapper--small">
            <div class="pos-rel">
                <div class="c-program__middle-portrait rellax"
                data-rellax-speed="-2">
                    <img data-src="<?php echo $mid_portrait['url'] ?>" alt="" class="lazy">
                </div>
                <div data-rellax-speed="-1" class="c-program__middle-landscape rellax">
                    <img data-src="<?php echo $mid_landscape['url'] ?>" alt="" class="lazy">
                </div>
                <div class="c-program__pillars-wrap">
                    <h2 class="o-animateup"><?php the_field('pillar_heading') ?></h2>
                    <?php if (have_rows('pillars')) :
                        while(have_rows('pillars')) : the_row();?>
                        <div class="c-program__pillar o-animateup">
                            <h3><?php the_sub_field('title') ?></h3>
                            <?php the_sub_field('content') ?>
                        </div>
                    <?php endwhile;
                    endif; ?>
                </div>
            </div>
        </div>
    </section>
</main>
<?php endwhile; ?>
<?php get_footer(); ?>
