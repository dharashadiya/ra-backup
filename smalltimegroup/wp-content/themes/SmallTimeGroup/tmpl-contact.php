<?php
/*
 * Template Name: Contact Page
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<main class="c-contact">
    <div class="c-contact__top">
        <div id="map" class="c-contact__map"></div>
        <div class="c-contact__grad cover"></div>
        <div class="c-contact__overlay cover">
            <div class="o-wrapper o-wrapper--mid h-100">
                <div class="pos-rel h-100">
                    <div class="c-contact__top-content">
                        <h1><?php the_title() ?></h1>
                        <?php the_content() ?>
                        <div class="c-contact__details">
                            <a href="https://www.google.com/maps/search/<?php the_field('address', 'options') ?>">NO. <?php the_field('address', 'options') ?></a>
                            <?php if(get_field('phone', 'options')) : ?>
                            <a href="tel:<?php the_field('phone', 'options') ?>">PH — <?php the_field('phone_number', 'options') ?></a>
                            <?php endif; ?>
                            <a href="mailto:<?php the_field('email', 'options') ?>"><?php the_field('email', 'options') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-contact__body">
        <h2 class="o-animateup">
            <?php the_field('sub_heading') ?>
        </h2>
        <div class="o-animateup">
            <?php the_field('sub_content') ?>
        </div>
        <div class="c-contact__form-container">
                <?php echo do_shortcode('[contact-form-7 id="132" title="Contact Page"]') ?>
        </div>
    </div>
</main>
<?php endwhile; ?>
<?php get_footer(); ?>

<!-- Form -->
<!-- 
<div class="c-contact__form">
    <div class="c-contact__form-top o-animateup">
        [select* contacttype class:c-contact__drop-down include_blank "General Enquiry " "Artist Development Program " "Venue Hire" "Studio Hire for Recording Podcast" "Perform at Small Time " "Perform on Small Time TV " "Careers" "Other"]
        <span>*Required Field</span>
    </div>
    <div class="o-layout o-layout--contact">
        <div class="o-layout__item u-1/2@tablet u-1/1 o-animateup">
            [text* firstname placeholder "First Name*"]
        </div>
        <div class="o-layout__item u-1/2@tablet u-1/1 o-animateup">
            [text* lastname placeholder "Last Name*"]
        </div>
        <div class="o-layout__item u-1/2@tablet u-1/1 o-animateup">
            [email* email placeholder akismet:author_email "Email*"]
        </div>
        <div class="o-layout__item u-1/2@tablet u-1/1 o-animateup">
            [text phone placeholder "Phone"]
        </div>
        <div class="o-layout__item u-1/1 o-animateup">
            [textarea* Description placeholder "Description*"]
        </div>
    </div>
    <div class="c-contact__submit-wrap o-animateup">
        [submit "Submit Enquiry"]
    </div>
</div> -->