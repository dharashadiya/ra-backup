<?php
/*
 * Template Name: Events Page
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<main class="c-events">
    <div class="o-wrapper o-wrapper--small">
        <div class="c-events__top">
            <h1 class="c-events__title">
                <?php the_field('upper_title') ?>
            </h1>
            <?php
                $terms = get_terms( 'post_tag', array(
                    'hide_empty' => false,
                ) );
                                
                $date = date_create();
                // Loop Current Events
     
                    $events = new WP_Query(array(
                        'posts_per_page'=> -1,
                        'post_type'	=> 'post',
                        'status' => 'published',
                        'orderby' => 'meta_value',
                        'order' => 'ASC',
                        'meta_type' => 'DATETIME',
                        'meta_key' => 'date',
                        // 'tag' => $terms->slug,
                        'meta_query' => array(
                            array(
                                'key' => 'date',
                                'value' => date_format($date, 'Y-m-d H:i:s'),
                                'compare' => '>',
                                'type' => 'DATETIME'
                            )
                        )
                    ));
                ?>
        
                <div class="c-filters <?php echo ($events->post_count ? "" : "c-filters--flex-end" )?>">
                    <div class="c-filters__single js-event-filter <?php echo ($events->post_count ? "" : "selected" )?>" data-filter="past-event">
                        Past Events
                    </div>
                    <?php if($events->post_count > 0) :?>
                    <div class="c-filters__single js-event-filter" data-filter="upcoming-event">
                        Upcoming Events
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="o-wrapper o-wrapper--mid">
            <div class="c-events-panel" >
                <div class="o-layout o-module">
                    <?php
                        $orig_query = $wp_query;
                        $wp_query  = $events;
                        $i = 1;
                        if ( have_posts() ) {
                            while(have_posts()) {
                                set_query_var( 'event_info', array(
                                    "index" => $i,
                                    "hide_upcoming" => false
                                ) );
                                the_post();
                                get_template_part('partials/loop-events-inner');
                                $i++;
                            }
                        } else {

                        }
                        $wp_query = $orig_query;
                        wp_reset_query();
                        // Loop Past Events
                        $events = new WP_Query(array(
                            'posts_per_page'=> -1,
                            'post_type'	=> 'post',
                            'status' => 'published',
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'meta_type' => 'DATETIME',
                            'meta_key' => 'date',
                            'meta_query' => array(
                                array(
                                    'key' => 'date',
                                    'value' => date_format($date, 'Y-m-d H:i:s'),
                                    'compare' => '<',
                                    'type' => 'DATETIME'
                                )
                            )
                        ));
                        $orig_query = $wp_query;
                        $wp_query  = $events;
                        $i = 1;
                        if ( have_posts() ) {
                            while(have_posts()) {
                                set_query_var( 'event_info', array(
                                    "index" => $i,
                                    "hide_upcoming" => false
                                ));
                                the_post();
                                get_template_part('partials/loop-events-inner-show');
                                $i++;
                            }
                        }
                        $wp_query = $orig_query;
                        wp_reset_query();
                    ?>
                </div>
            </div>
            <div class="c-events__link-wrap">
                <a href="javascript:void(0);" class="c-events__more-link js-show-more"><span>View More</span><?php svgicon('SmallTime-Icons-Plus-Black', '0 0 16 16'); ?></a>
            </div>
        </div>
  
</main>

<?php endwhile; ?>
<?php get_footer(); ?>