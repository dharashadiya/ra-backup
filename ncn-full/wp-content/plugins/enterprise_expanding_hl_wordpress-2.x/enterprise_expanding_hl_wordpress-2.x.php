<?php
/*
Plugin Name: ReadSpeaker webReader
Plugin URI: http://www.readspeaker.com
Description: ReadSpeaker  Enterprise webReader for WordPress Blogs
Author: ReadSpeaker International B.v.
Version: 2.4.5
Author URI: http://www.readspeaker.com
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Array for texts that are used in the button
 */

function readspeaker_text_var()
{

    $text = array(
        'ar_ar' => array(
            'alt' => urldecode('ReadSpeaker%20%D8%A7%D9%8E%D8%B3%D8%AA%D9%85%D8%B9%D9%8F%20%D8%A5%D9%84%D9%89%20%D9%87%D8%B0%D9%87%20%D8%A7%D9%84%D8%B5%D9%81%D8%AD%D8%A9%D9%90%20%D9%85%D8%B3%D8%AA%D8%AE%D8%AF%D9%85%D8%A7'),
            'button_text' => urldecode('%D8%A7%D8%B3%D8%AA%D9%85%D8%B9'),
            'alt_dr' => urldecode('%D8%A5%D9%81%D8%AA%D8%AD%20%D9%87%D8%B0%D9%87%20%D8%A7%D9%84%D9%88%D8%AB%D9%8A%D9%82%D8%A9%20%D8%A8%D9%90%D9%80%20ReadSpeaker')
            ),
        'ca_es' => array(
            'alt' => urldecode('Escolteu%20aquesta%20plana%20utilitzant%20ReadSpeaker'),
            'button_text' => 'Escoltar',
            'alt_dr' => urldecode('Obre%20aquest%20document%20utilitzant%20ReadSpeaker%20docReader')
            ),
        'da_dk' => array(
            'alt' => urldecode('Lyt%20til%20denne%20side%20med%20ReadSpeaker'),
            'button_text' => 'Lyt',
            'alt_dr' => urldecode('%C3%85bn%20dette%20dokument%20med%20ReadSpeaker%E2%80%99s%20docReader')
            ),
        'de_de' => array(
            'alt' => urldecode('Um%20den%20Text%20anzuh&ouml;ren,%20verwenden%20Sie%20bitte%20ReadSpeaker'),
            'button_text' => 'Vorlesen',
            'alt_dr' => urldecode('Dieses%20Dokument%20mit%20ReadSpeaker%20docReader%20%C3%B6ffnen')
            ),
        'el_gr' => array(
            'alt' => urldecode('%CE%91%CE%BA%CE%BF%CF%8D%CF%83%CF%84%CE%B5%20%CE%B1%CF%85%CF%84%CE%AE%CE%BD%20%CF%84%CE%B7%CE%BD%20%CF%83%CE%B5%CE%BB%CE%AF%CE%B4%CE%B1%20%CF%87%CF%81%CE%B7%CF%83%CE%B9%CE%BC%CE%BF%CF%80%CE%BF%CE%B9%CF%8E%CE%BD%CF%84%CE%B1%CF%82%20ReadSpeaker'),
            'button_text' => urldecode('%CE%91%CE%BA%CE%BF%CF%8D%CF%83%CF%84%CE%B5'),
            'alt_dr' => urldecode('Open%20this%20document%20with%20ReadSpeaker%20docReader')
            ),
        'en_au' => array(
            'alt' => urldecode('Listen%20to%20this%20page%20using%20ReadSpeaker'),
            'button_text' => 'Listen',
            'alt_dr' => urldecode('Open%20this%20document%20with%20ReadSpeaker%20docReader')
            ),
        'en_uk' => array(
            'alt' => urldecode('Listen%20to%20this%20page%20using%20ReadSpeaker'),
            'button_text' => 'Listen',
            'alt_dr' => urldecode('Open%20this%20document%20with%20ReadSpeaker%20docReader')
            ),
        'en_us' => array(
            'alt' => urldecode('Listen%20to%20this%20page%20using%20ReadSpeaker'),
            'button_text' => 'Listen',
            'alt_dr' => urldecode('Open%20this%20document%20with%20ReadSpeaker%20docReader')
            ),
        'es_co' => array(
            'alt' => urldecode('Escucha%20esta%20p&aacute;gina%20utilizando%20ReadSpeaker'),
            'button_text' => 'Escuchar',
            'alt_dr' => urldecode('Abrir%20este%20documento%20utilizando%20el%20ReadSpeaker%20docReader')
            ),
        'es_es' => array(
            'alt' => urldecode('Escucha%20esta%20p&aacute;gina%20utilizando%20ReadSpeaker'),
            'button_text' => 'Escuchar',
            'alt_dr' => urldecode('Abrir%20este%20documento%20utilizando%20el%20ReadSpeaker%20docReader')
            ),
        'es_us' => array(
            'alt' => urldecode('Escucha%20esta%20p&aacute;gina%20utilizando%20ReadSpeaker'),
            'button_text' => 'Escuchar',
            'alt_dr' => urldecode('Abrir%20este%20documento%20utilizando%20el%20ReadSpeaker%20docReader')
            ),
        'eu_es' => array(
            'alt' => urldecode('Orri%20hau%20entzun%20ReadSpeaker%20erabiliz'),
            'button_text' => 'Entzun',
            'alt_dr' => urldecode('Ireki%20dokumentu%20hau%20ReadSpeaker%20docReader-en%20bidez')
            ),
        'fi_fi' => array(
            'alt' => urldecode('Kuuntele%20ReadSpeakerilla'),
            'button_text' => 'Kuuntele',
            'alt_dr' => urldecode('Avaa%20t&auml;m&auml;%20dokumentti%20ReadSpeaker%20docReaderill&auml;')
            ),
        'fo_fo' => array(
            'alt' => urldecode('Lurta%20eftir%20tekstinum%20&aacute;%20s&iacute;&eth;uni%20vi&eth;%20ReadSpeaker'),
            'button_text' => 'Lurta',
            'alt_dr' => urldecode('Opna%20hetta%20skjali%C3%B0%20vi%C3%B0%20ReadSpeaker%20docReader')
            ),
        'fr_fr' => array(
            'alt' => urldecode('Ecouter%20le%20texte%20avec%20ReadSpeaker'),
            'button_text' => 'Ecouter',
            'alt_dr' => urldecode('Ouvrez%20ce%20document%20avec%20ReadSpeaker%20docReader')
            ),
        'fy_nl' => array(
            'alt' => urldecode('L&uacute;sterje%20nei%20dizze%20pagina%20mei%20ReadSpeaker'),
            'button_text' => urldecode('Lees%20Voor'),
            'alt_dr' => urldecode('Open%20this%20document%20with%20ReadSpeaker%20docReader')
            ),
        'gl_es' => array(
            'alt' => urldecode('Escoite%20esta%20p&aacute;xina%20con%20axuda%20de%20ReadSpeaker'),
            'button_text' => 'Escoitar',
            'alt_dr' => urldecode('Abre%20este%20documento%20con%20ReadSpeaker%20docReader')
            ),
        'hi_in' => array(
            'alt' => urldecode('%E0%A4%B8%E0%A5%81%E0%A4%A8%E0%A5%8B'),
            'button_text' => urldecode('%E0%A4%B8%E0%A5%81%E0%A4%A8%E0%A5%8B'),
            'alt_dr' => urldecode('Open%20this%20document%20with%20ReadSpeaker%20docReader')
            ),
        'is_is' => array(
            'alt' => urldecode('Hlusta%C3%B0u%20%C3%A1%20%C3%BEessa%20s%C3%AD%C3%B0u%20lesna%20af%20ReadSpeaker'),
            'button_text' => 'Hlusta',
            'alt_dr' => urldecode('Open%20this%20document%20with%20ReadSpeaker%20docReader')
            ),
        'it_it' => array(
            'alt' => urldecode('Ascolta%20questa%20pagina%20con%20ReadSpeaker'),
            'button_text' => 'Ascolta',
            'alt_dr' => urldecode('Apri%20questo%20documento%20con%20ReadSpeaker%20docReader')
            ),
        'ja_jp' => array(
            'alt' => urldecode('%E9%9F%B3%E5%A3%B0%E3%81%A7%E8%AA%AD%E3%81%BF%E4%B8%8A%E3%81%92%E3%82%8B'),
            'button_text' => urldecode('%E8%AA%AD%E3%81%BF%E4%B8%8A%E3%81%92%E3%82%8B'),
            'alt_dr' => urldecode('%E3%83%AA%E3%83%BC%E3%83%89%E3%82%B9%E3%83%94%E3%83%BC%E3%82%AB%E3%83%BCdocReader%E3%81%A7%E3%81%93%E3%81%AE%E3%83%89%E3%82%AD%E3%83%A5%E3%83%A1%E3%83%B3%E3%83%88%E3%82%92%E9%96%8B%E3%81%8F')
            ),
        'ko_kr' => array(
            'alt' => urldecode('%EC%9D%BD%EA%B8%B0'),
            'button_text' => urldecode('%EC%9D%BD%EA%B8%B0'),
            'alt_dr' => urldecode('Open%20this%20document%20with%20ReadSpeaker%20docReader')
            ),
        'nl_be' => array(
            'alt' => urldecode('Laat%20de%20tekst%20voorlezen%20met%20ReadSpeaker'),
            'button_text' => urldecode('Lees%20Voor'),
            'alt_dr' => urldecode('Open%20dit%20document%20met%20ReadSpeaker%20docReader')
            ),
        'nl_nl' => array(
            'alt' => urldecode('Laat%20de%20tekst%20voorlezen%20met%20ReadSpeaker'),
            'button_text' => urldecode('Lees%20Voor'),
            'alt_dr' => urldecode('Open%20dit%20document%20met%20ReadSpeaker%20docReader')
            ),
        'no_nb' => array(
            'alt' => urldecode('Lytt%20til%20denne%20siden%20med%20ReadSpeaker'),
            'button_text' => 'Lytt',
            'alt_dr' => urldecode('&Aring;pne%20dette%20dokumentet%20med%20Readspeaker%20docReader')
            ),
        'pl_pl' => array(
            'alt' => urldecode('Pos%C5%82uchaj%20zawarto%C5%9Bci%20strony'),
            'button_text' => urldecode('Pos%C5%82uchaj'),
            'alt_dr' => urldecode('Pos%C5%82uchaj%20zawarto%C5%9Bci%20strony')
            ),
        'pt_pt' => array(
            'alt' => urldecode('Ouvir%20com%20ReadSpeaker'),
            'button_text' => 'Ouvir',
            'alt_dr' => urldecode('Abra%20este%20documento%20com%20o%20ReadSpeaker%20docReader')
            ),
        'ro_ro' => array(
			'alt' => 'Asculta',
            'button_text' => 'Asculta',
            'alt_dr' => urldecode('Open%20this%20document%20with%20ReadSpeaker%20docReader')
			),
        'ru_ru' => array(
            'alt' => urldecode('%D0%9F%D1%80%D0%BE%D1%81%D0%BB%D1%83%D1%88%D0%B0%D1%82%D1%8C%20%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D1%83%20%D0%BF%D1%80%D0%B8%20%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D0%B8%20ReadSpeaker'),
            'button_text' => urldecode('%D0%9F%D1%80%D0%BE%D1%81%D0%BB%D1%83%D1%88%D0%B0%D1%82%D1%8C'),
            'alt_dr' => urldecode('%D0%9F%D1%80%D0%BE%D1%81%D0%BB%D1%83%D1%88%D0%B0%D1%82%D1%8C%20%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D1%83%20%D0%BF%D1%80%D0%B8%20%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D0%B8%20ReadSpeaker')
            ),
        'sv_se' => array(
            'alt' => urldecode('Lyssna%20p&aring;%20sidans%20text%20med%20ReadSpeaker'),
            'button_text' => 'Lyssna',
            'alt_dr' => urldecode('&Ouml;ppna%20detta%20dokument%20med%20ReadSpeaker%20docReader')
            ),
        'tr_tr' => array(
            'alt' => urldecode('Bu%20sayfay%C4%B1%20ReadSpeaker%20ile%20dinle'),
            'button_text' => 'Dinle',
            'alt_dr' => urldecode('Open%20this%20document%20with%20ReadSpeaker%20docReader')
            ),
        'zh_cn' => array(
            'alt' => urldecode('%E8%AE%A9ReadSpeaker%E4%B8%BA%E4%BD%A0%E6%9C%97%E8%AF%BB%E9%A1%B5%E9%9D%A2'),
            'button_text' => urldecode('%E6%9C%97%E8%AF%BB'),
            'alt_dr' => urldecode('Open%20this%20document%20with%20ReadSpeaker%20docReader')
            )
        );

        return $text;
}

/**
 * Region Array
 */

function readspeaker_region()
{

    $region = array (
        'Africa' => 'af',
        'Asia' =>   'as',
        'East Asia' => 'eas',
        'Australia' => 'oc',
        'Europe' => 'eu',
        'North America' => 'na',
        'South America' => 'na'
    );
    return $region;
}

/**
 * Switch for phrases
 */

function readspeaker_switch($what)
{
        global $post;
        $id             = $post->ID;
        $this_text      = readspeaker_text_var();
        $this_region    = readspeaker_region();
        $region         = get_option("rs_region");


        $lang = get_option("rs_lang");
 

    switch ($what) {
        case 'alt':
            $text = $this_text["$lang"]['alt'];
            return $text;
        break;

        case 'button_text':
            $text = $this_text["$lang"]['button_text'];
            return $text;
        break;

        case 'alt_dr':
            $text = $this_text["$lang"]['alt_dr'];
            return $text;
        break;

        case 'region_code':
            $text = $this_region["$region"];
            return $text;
        break;

        case 'version':
            $text = '2.4.4';
            return $text;
        break;
    }
}


/**
 * ReadSpeaker actions and filters
 */

// Add ReadSpeaker JavaScript and Inline configuration
add_action( 'wp_head', 'readspeaker_dr_config_script' );
add_action( 'wp_head', 'readspeaker_script' );

//If the_content() is called from the theme this one is used
add_filter( 'the_content', 'readspeaker_html' );

//If the_excerpt() is called from the theme this should remove the listen button text that appears below listen button and add a listen button.
add_filter( 'the_excerpt', 'readspeaker_custom_excerpt' );

// Add link to options page fron Plugin page
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'rspeak_action_links' );

// Hook for adding admin menus
add_action( 'admin_menu', 'readspeaker_add_pages' );

// Add shortcode
add_shortcode( 'readspeaker_listen_button', 'readspeaker_shortcode' );




/**
 * Setup Readspeaker javascript
 */

function readspeaker_script()
{
    global $post;
    $rs_frontpage   = get_option( "rs_button_frontpage" );
    $rs_exclude     = get_option( "rs_exclude" );
    $rs_exclude     = explode( ',', $rs_exclude );
    $id             = isset($post->ID) ? $post->ID : '';
    $cid            = get_option( "rs_customer_id");
    if ($rs_frontpage === '1' && is_front_page()) {
        echo '';
    } elseif (in_array( $id, $rs_exclude ) || empty ( $cid )) {
        echo '';
    } else {
        $region_code    = readspeaker_switch( 'region_code' );
        $dr             = get_option( "rs_dr_activation" );
        if (! empty( $dr )) {
            $dr = '&dload=DocReader.AutoAdd';
        }
        echo '<script type="text/javascript" src="//cdn1.readspeaker.com/script/' . $cid . '/webReader/webReader.js?pids=wr' . $dr .'"></script>' ."\n";
    }

}

/**
 * Setup Config for docReader
 */

function readspeaker_dr_config_script()
{
    global $post;
    $rs_frontpage   = get_option( "rs_button_frontpage" );
    $rs_exclude     = get_option( "rs_exclude" );
    $rs_exclude     = explode( ',', $rs_exclude );
    $id             = isset($post->ID) ? $post->ID : '';

    if ($rs_frontpage === '1' && is_front_page()) {
        echo '';
    } elseif (in_array( $id, $rs_exclude)) {
        echo '';
    } else {
        $dr = get_option( "rs_dr_activation" );
        if (! empty( $dr )) {
            global $alt_dr;
            $rs_frontpage   =  get_option( "rs_button_frontpage" );
            $rs_exclude         =  get_option( "rs_exclude" );
            $rs_exclude         =  explode( ',', $rs_exclude );
            $lang                   = get_option( "rs_lang" );
            $alt_dr_text        = readspeaker_switch( 'alt_dr' );
        ?>
        <script type="text/javascript">
            <!--
            window.rsDocReaderConf = {lang: '<?php echo $lang ?>',
            img_alt: '<?php echo $alt_dr_text ?>'};
            //-->
        </script>

    <?php
        }
    }
}


/**
 * Building the ReadSpeaker Listen button
 */

function readspeaker_html($content)
{
    global $post;
    global $wp;
    $id             = $post->ID;
    $rs_frontpage   = get_option( "rs_button_frontpage" );
    $rs_shortcode   = get_option( "rs_shortcode" );
    $rs_exclude     = get_option( "rs_exclude" );
    $rs_exclude     = explode( ',', $rs_exclude );
    $cid            = get_option( "rs_customer_id" );
    if ( is_feed() ) {
        return $content;
    } elseif ( $rs_frontpage === '1' && is_front_page() ) {
        return $content;
    } elseif ( $rs_shortcode === '1' ) {
        return $content;
    } elseif ( in_array( $id, $rs_exclude ) || $cid == "" ) {
        return $content;
    } else {
        //absolute URI
        $url = get_option( 'siteurl' );
        $parts = parse_url( $url );
        $permalink = "{$parts['scheme']}://{$parts['host']}" . add_query_arg( null, null );
        $permalink  = urlencode($permalink);
        $thetime    = get_the_time( "M d", $post->ID );
        $lang   = get_option( "rs_lang" );
 
        $region_code    = readspeaker_switch( 'region_code' );
        $alt_text       = readspeaker_switch( 'alt' );
        $plugin_version = readspeaker_switch( 'version' );
        $button_style   = get_option( "rs_button_style" );
        $reading_area   = 'readid=rspeak_read_' . $id;
        if (empty( $button_style )) {
            $button_style = "";
        } else {
            $button_style = ' style="' . $button_style . '"';
        }
        $listen_button_text = readspeaker_switch( 'button_text' );

        $rspeak_url = readspeaker_listen_button();
        $rspeak_url .= '<div id="rspeak_read_%3$d">%11$s</div>';
        $thereturn = sprintf( $rspeak_url, $permalink, $region_code, $id, $cid, $lang, $button_style, $listen_button_text, $alt_text, $plugin_version, $reading_area, $content );

        return $thereturn;
    }
}


/**
 * Adding the Listen button to Excerpt, search and remove listen button text from Excerpt
 */

function readspeaker_custom_excerpt( $rs_excerpt )
{
    $cid = get_option( "rs_customer_id" );
    if ( empty($cid) ) {
        return readspeaker_html( $rs_excerpt );
    }
    else {
        // default values //
        $limit          = 1;
        $replace        = '';
        $search         = readspeaker_switch( 'button_text' );

        $search         = strval($search);
        // Loop until $search can no longer be found, or $limit is reached
        for ($i = 0; $i < $limit; $i++) {
            $pos = strpos( $rs_excerpt, $search );

            // Return $rs_excerpt if $search cannot be found
            if ($pos===false) {
                break;
            }

            // Get length of $search, to make proper replacement later on
            $search_len = strlen($search);

            // Replace
            $rs_excerpt = substr_replace( $rs_excerpt, $replace, $pos, $search_len );
        }

        return readspeaker_html( $rs_excerpt );
    }
}


/**
 *  ReadSpeaker Listen Button Shortcode
 */
function readspeaker_shortcode($atts)
{
    $rs_shortcode =  get_option( "rs_shortcode" );
    if ($rs_shortcode !== '1') {
        $thereturn = '';
        return $thereturn;
    } else {
        static $i = '1';
        global $post;
        $id             = $post->ID;
        $cid            = get_option( "rs_customer_id" );
        $button_style   = get_option( "rs_button_style" );
        $region_code    = readspeaker_switch( 'region_code' );

        $atts = shortcode_atts(array(
                'readid'    => '',
                'readclass' => ''
                ), $atts, 'readspeaker_listen_button');

        $lang = get_option( "rs_lang" );

        $listen_button_text = readspeaker_switch( 'button_text' );

            $alt_text = readspeaker_switch( 'alt' );

        if (empty($button_style )) {
            $button_style = "";
        } else {
            $button_style = ' style="' . $button_style . '"';
        }

       //absolute URI
        $url = get_option( 'siteurl' );
        $parts      = parse_url( $url );
        $permalink  = "{$parts['scheme']}://{$parts['host']}" . add_query_arg( null, null );


        if (! empty( $atts['readid'] ) && empty ( $atts['readclass'] )) {
            $reading_area       = 'readid=' . $atts['readid'];
        } elseif (empty( $atts['readid'] ) && ! empty ( $atts['readclass'] )) {
            $reading_area       = 'readclass=' . $atts['readclass'];
        } elseif (! empty( $atts['readid'] ) && ! empty ( $atts['readclass'] )) {
            $reading_area       = 'readid=' . $atts['readid'] . '&amp;readclass=' . $atts['readclass'];
        } else {
            $reading_area       = 'readid=post-' . $id;
        }

        $permalink      = urlencode($permalink);
        $plugin_version = readspeaker_switch( 'version' );

        $rspeak_url = readspeaker_listen_button();
        $thereturn = sprintf( $rspeak_url, $permalink, $region_code, $i, $cid, $lang, $button_style, $listen_button_text, $alt_text, $plugin_version, $reading_area );

        $i++;
        return $thereturn;
    }
}

/**
 * Available button types
 */

// Standard Listen button
function readspeaker_listen_button()
{
    $listen_button = '<!-- RS_MODULE_CODE_%9$s --><div id="readspeaker_button%3$d"%6$s class="rs_skip rsbtn rs_preserve"><a class="rsbtn_play" title="%8$s" accesskey="L" href="//app-%2$s.readspeaker.com/cgi-bin/rsent?customerid=%4$d&amp;lang=%5$s&amp;%10$s&amp;url=%1$s">
        <span class="rsbtn_left rsimg rspart"><span class="rsbtn_text"><span>%7$s</span></span></span>
        <span class="rsbtn_right rsimg rsplay rspart"></span>
        </a></div>';

    return $listen_button;
}

/**
 * The Admin Section
 */

// Link to options page from plugin page
function rspeak_action_links($links)
{
    $rspeaklink = array(
        '<a href="' . admin_url( 'options-general.php?page=ReadSpeakerOptions' ) . '">Settings</a>',
    );
    return array_merge( $links, $rspeaklink );
}


// Add the ReadSpeaker Options page
function readspeaker_add_pages()
{
    // Add a new submenu under Settings:
    add_options_page( 'ReadSpeaker Settings', 'ReadSpeaker Settings', 'administrator', 'ReadSpeakerOptions', 'readspeaker_options_page' );
}

function readspeaker_admin_msg($what)
{
    switch ($what) {
        case 'cid':
            $error_msg = '<div class="error"><p><strong>Error! check your customer ID</strong></p></div>';
            return $error_msg;
            break;

        case 'dr_id':
            $error_msg = '<div class="error"><p><strong>Error! check your docReaderID</strong></p></div>';
            return $error_msg;
            break;
    }
}




// readspeaker_options_page displays the page content for the ReadSpeakerOptions submenu
function readspeaker_options_page()
{
    // variables for the field and option names
    global $wp_version;
    $location       = $_SERVER['REQUEST_URI'];
    $alt            = readspeaker_text_var();
    $region_code    = readspeaker_region();
    $button_type    = array( 'standard button', 'split button' );
    $delete_button = 'Delete';
    $hidden_field_name  = 'readspeaker_submit_hidden';
    
    $opt_name1   = $data_field_name1   = 'rs_customer_id';
    $opt_name2   = $data_field_name2   = 'rs_lang';
    $opt_name3   = $data_field_name3   = 'rs_region';
    $opt_name4   = $data_field_name4   = 'rs_button_style';
    $opt_name5   = $data_field_name5   = 'rs_button_frontpage';
    $opt_name6   = $data_field_name6   = 'rs_dr_activation';
    $opt_name7   = $data_field_name7   = 'rs_exclude';
    $opt_name10  = $data_field_name10  = 'rs_shortcode';

    // Read in existing option value from database
    $opt_val1 = get_option( $opt_name1 );
    $opt_val2 = get_option( $opt_name2 );
    $opt_val3 = get_option( $opt_name3 );
    $opt_val4 = get_option( $opt_name4 );
    $opt_val5 = get_option( $opt_name5 );
    $opt_val6 = get_option( $opt_name6 );
    $opt_val7 = get_option( $opt_name7 );
    $opt_val10 = get_option( $opt_name10 );


    // See if the user has posted us some information
    // If they did, this hidden field will be set to 'Y'
    if (isset($_POST[ $hidden_field_name ])) {
        if ($_POST[ $hidden_field_name ] == 'Y') {
            if (isset($_POST[ $delete_button ])) {
                delete_option( $opt_name1, $opt_val1 );
                delete_option( $opt_name2, $opt_val2 );
                delete_option( $opt_name3, $opt_val3 );
                delete_option( $opt_name4, $opt_val4 );
                delete_option( $opt_name5, $opt_val5 );
                delete_option( $opt_name6, $opt_val6 );
                delete_option( $opt_name7, $opt_val7 );
                delete_option( $opt_name10, $opt_val10 );


                $opt_val1 = get_option( $opt_name1 );
                $opt_val2 = get_option( $opt_name2 );
                $opt_val3 = get_option( $opt_name3 );
                $opt_val4 = get_option( $opt_name4 );
                $opt_val5 = get_option( $opt_name5 );
                $opt_val6 = get_option( $opt_name6 );
                $opt_val7 = get_option( $opt_name7 );
                $opt_val10 = get_option( $opt_name10 );


            ?>
            <div class="updated"><p><strong><?php _e('All Settings deleted.', 'readspeaker_trans_domain' ); ?></strong></p></div>
            <?php
            } else {
                $old_val2 = $opt_val2;
            // Read their posted value

                $opt_val1 = isset($_POST[ $data_field_name1 ]) ? $_POST[ $data_field_name1 ] : '';
                $opt_val2 = isset($_POST[ $data_field_name2 ]) ? $_POST[ $data_field_name2 ] : '';
                $opt_val3 = isset($_POST[ $data_field_name3 ]) ? $_POST[ $data_field_name3 ] : '';
                $opt_val4 = isset($_POST[ $data_field_name4 ]) ? $_POST[ $data_field_name4 ] : '';
                $opt_val5 = isset($_POST[ $data_field_name5 ]) ? $_POST[ $data_field_name5 ] : '';
                $opt_val6 = isset($_POST[ $data_field_name6 ]) ? $_POST[ $data_field_name6 ] : '';
                $opt_val7 = isset($_POST[ $data_field_name7 ]) ? $_POST[ $data_field_name7 ] : '';
                $opt_val10 = isset($_POST[ $data_field_name10 ]) ? $_POST[ $data_field_name10 ] : '';
                if ( $wp_version >= 3.0 ) {

                    $opt_val1  = sanitize_text_field( $opt_val1 );
                    $opt_val2  = sanitize_text_field( $opt_val2 );
                    $opt_val3  = sanitize_text_field( $opt_val3 );
                    $opt_val4  = sanitize_text_field( $opt_val4 );
                    $opt_val6  = sanitize_text_field( $opt_val6 );
                    $opt_val7  = sanitize_text_field( $opt_val7 );
                }

            // Make sure Customer ID is correct format
                if (! empty( $opt_val1 )) {
                    $opt_val1 = trim( $opt_val1 );
                    $new_opt_val1 = $opt_val1;
                    if (preg_match( "/^[0-9]{1,2}$/", $opt_val1 )) {
                        $opt_val1 = $new_opt_val1;
                    } elseif (preg_match( "/^[0-9]{4,5}$/", $opt_val1 )) {
                        $opt_val1 = $new_opt_val1;
                    } else {
                        $opt_val1 = '';
                        $error = readspeaker_admin_msg( 'cid' );
                        echo $error;
                    }
                } else {
                    $error = readspeaker_admin_msg( 'cid' );
                    echo $error;
                }

            // Make sure docReaderID is correct format
                if (! empty( $opt_val6 )) {
                    $new_opt_val6 = strtolower($opt_val6);
                    if (!preg_match( "/^[a-z]{5}$/", $new_opt_val6 )) {
                        $opt_val6 = '';
                        $error = readspeaker_admin_msg( 'dr_id' );
                        echo $error;
                    } else {
                        $opt_val6 = $new_opt_val6;
                    }
                }
        
            // Save the posted value in the database
                update_option( $opt_name1, $opt_val1 );
                update_option( $opt_name2, $opt_val2 );
                update_option( $opt_name3, $opt_val3 );
                update_option( $opt_name4, $opt_val4 );
                update_option( $opt_name5, $opt_val5 );
                update_option( $opt_name6, $opt_val6 );
                update_option( $opt_name7, $opt_val7 );
                update_option( $opt_name10, $opt_val10 );

            // Put an options updated message on the screen
            ?>
            <div class="updated"><p><strong><?php _e( 'Settings saved.', 'readspeaker_trans_domain' ); ?></strong></p></div>

    <?php
            }
        }
    }
// Now display the options editing screen
    ?>
        <div class="wrap">
            <?php
               // If shortcode is activated display shortcode information on the screen

                $rs_shortcode =  get_option( "rs_shortcode" );
                if ($rs_shortcode == '1') {
                ?>  <div class="updated" style="border-left: none;">
                        <h3><?php _e( 'ReadSpeaker Shortcode', 'readspeaker_trans_domain' ); ?></h3>
                        <span class="description">Use the following shortcode:</span>
                        <p><code>[readspeaker_listen_button]</code></p>
                        <p>Default reading area is id="post-[post_id]".</p>
                        <span class="description">To define your own reading area the following optional parameters are available:</span>
                        <p><code>readid</code> - Define the reading area using ID-attribute (example: <code>[readspeaker_listen_button readid=read_this]</code> will make the reading area the id="read_this").</p>
                        <p><code>readclass</code> - Define the reading area using CLASS-attribute (example: <code>[readspeaker_listen_button readclass=read_this]</code> will make the reading area the class="read_this"). <br /></p>
                    </div> 
                <?php
                }
            ?>
            <h2 style="margin-bottom:10px;"><?php _e( 'ReadSpeaker Settings', 'readspeaker_trans_domain' );?></h2>
            <form name="form1" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI'] ); ?>">
                <input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">
                <span style="color:red;"><b>*</b></span><span class="description"><strong><?php _e( ' is required', 'readspeaker_trans_domain' ); ?></strong></span>
                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row"><label for="<?php echo $data_field_name1; ?>"><?php _e( "Customer ID", 'readspeaker_trans_domain' );?><span style="color:red;"><b>*</b></span></label></th>
                            <td>
                                <input type="text" name="<?php echo $data_field_name1; ?>" value="<?php echo $opt_val1; ?>" size="50">
                                <span class="description"><?php _e( 'Enter your ReadSpeaker Enterprise Customer ID (example: 1234).', 'readspeaker_trans_domain' ); ?></span>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for="<?php echo $data_field_name2; ?>"><?php _e( "Language", 'readspeaker_trans_domain' );?><span style="color:red;"><b>*</b></span></label></th>
                            <td>
								<select name="<?php echo $data_field_name2; ?>"> <?php
                                                               
                                foreach ($alt as $key => $val) {
                                    if ($key==$opt_val2) {
                                        echo "<option value='$key' selected='selected'>$key</option>";
                                    } else {
                                        echo "<option value='$key'>$key</option>";
                                    }
                                } ?>
                                </select>
                                <span class="description"><?php _e( 'Select your language.', 'readspeaker_trans_domain' ); ?></span>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">
                                <label for="<?php echo $data_field_name3; ?>"><?php _e( "Region code", 'readspeaker_trans_domain' );?><span style="color:red;"><b>*</b></span></label>
                            </th>
                            <td>
                                <select name="<?php echo $data_field_name3; ?>" id="<?php echo $data_field_name3; ?>">
                                    <?php
                                    foreach ($region_code as $key => $val) {
                                        if ($key==$opt_val3) {
                                            echo "<option value='$key' selected='selected'>$key</option>";
                                        } else {
                                            echo "<option value='$key'>$key</option>";
                                        }
                                    }
                                    ?>
                                </select>
                                <span class="description"><?php _e( 'Select your region.', 'readspeaker_trans_domain' ); ?></span>
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <h3 style="margin-bottom:10px;"><?php _e( 'Optional Options', 'readspeaker_trans_domain' );?></h3>
                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row"><label for ="<?php echo $data_field_name4; ?>"><?php _e( "Listen button CSS attribute", 'readspeaker_trans_domain' );?></label></th>
                            <td>
                                <input type="text" name="<?php echo $data_field_name4; ?>" value="<?php echo $opt_val4; ?>" size="50">
                                <span class="description"><?php _e( 'Add inline CSS to listen button (example: float:right;).', 'readspeaker_trans_domain' ); ?></span>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for ="<?php echo $data_field_name10; ?>"><?php _e( "Use shortcode", 'readspeaker_trans_domain' );?></label></th>
                            <td>
                                <input type="checkbox" name="<?php echo $data_field_name10; ?>" value="1"<?php checked( $opt_val10, '1' ); ?> />
                                <span class="description"><?php _e( 'Check to enable use of a shortcode. This will disable the automatic adding of the listen button.', 'readspeaker_trans_domain' ); ?></span>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for ="<?php echo $data_field_name6; ?>"><?php _e( "Enable docReader", 'readspeaker_trans_domain' );?></label></th>
                            <td>
                                <input type="text" name="<?php echo $data_field_name6; ?>" value="<?php echo $opt_val6; ?>" size="50">
                                <span class="description"><?php _e( 'Insert your docReader ID to enable ReadSpeaker docReader.', 'readspeaker_trans_domain' ); ?></span><br />
                                <span><strong><?php _e( 'Please note: docReader needs to be enabled in your ReadSpeaker account. To enable docReader, contact your sales contact at ReadSpeaker.', 'readspeaker_trans_domain' ); ?></strong><br />
                                    <?php _e( 'You can read more about docReader here: <a href="http://www.readspeaker.com/readspeaker-docreader/" target="_blank">http://www.readspeaker.com/readspeaker-docreader/</a>', 'readspeaker_trans_domain' ); ?>
                                </span>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for ="<?php echo $data_field_name5; ?>"><?php _e( "Exclude from frontpage", 'readspeaker_trans_domain' );?></label></th>
                            <td>
                                <input type="checkbox" id="<?php echo $data_field_name5; ?>" name="<?php echo $data_field_name5; ?>" value="1" <?php checked( $opt_val5, '1' ); ?> />
                                <span class="description"><?php _e( 'Check to exclude listen button from frontpage.', 'readspeaker_trans_domain' ); ?></span>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for ="<?php echo $data_field_name7; ?>"><?php _e( "Exclude specific pages", 'readspeaker_trans_domain' );?></label></th>
                            <td>
                                <input type="text" name="<?php echo $data_field_name7; ?>" value="<?php echo $opt_val7; ?>" size="50">
                                <span class="description"><?php _e( 'Add post-ID, comma separated (example: 1,80,293).', 'readspeaker_trans_domain' ); ?></span>
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <p class="submit" style="float:left; margin-right: 15px;">
                        <input class="button button-primary" type="submit" name="Submit" value="<?php _e('Update Options', 'readspeaker_trans_domain' ) ?>" />
                    </p>
                    <p class="submit" style="margin-top: 26px;">
                        <input class="button delete" type="submit" name="Delete" value="<?php _e( 'Delete Options', 'readspeaker_trans_domain' ) ?>" />
                    </p>
                </form>
            </div>
            <?php
}
