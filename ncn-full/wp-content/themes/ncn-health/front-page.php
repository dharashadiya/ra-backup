<?php get_header(); ?>
<div class="c-home">
    <div class="c-home__header">
        <?php
			$banner_image = get_field( 'banner_image' );
			$banner_image_mobile = get_field( 'banner_image_mobile' );
			if (!$banner_image_mobile) {
				$banner_image_mobile = $banner_image;
			}
		?>
        <div class="c-home__header_image js-changeimage-mob" style="background-image: url(<?php echo esc_url( $banner_image['url'] ); ?>)" data-bg-mobile="<?php echo $banner_image_mobile['url']; ?>">
        </div>
        <div class="o-wrapper">
            <div class="c-home__header_content">
                <h1><?php the_field( 'banner_text' ); ?></h1>
            </div>
            <div class="c-lookup">
                <form role="search" method="get" class="c-lookup__form" action="<?php echo home_url( '/services' ); ?>">
                    <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
                    <?php //svgicon('search-icon', '0 0 24 24'); ?>
                    <input type="search" class="search-field c-lookup__input"
                        placeholder="<?php echo esc_attr_x( 'What are you looking for?', 'placeholder' ) ?>"
                        value="<?php echo get_search_query() ?>" name="_search_services"
                        title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" id="search" />
                    <input type="submit" value="Search" class="o-btn c-lookup__submit" />
                </form>
            </div>
            <div class="viewBtn">
                <a class="c-lookup__service" href="/services">Or view A-Z of our services</a>
                <a href="#" class="downArrow"><img src="<?php echo ASSETS; ?>/img/down-arrow-whitebg.svg"
                        alt="Down arrow"></a>
            </div>
        </div>
    </div>
    <?php
        include('home-sections/home-services.php');
        include('home-sections/home-news.php');
        include('home-sections/home-maps.php');
        include('home-sections/home-values.php');
    ?>
</div>
<?php get_footer(); ?>