<?php
    $args = array(
        'post_type' => 'ra-news',
        'orderby' => 'date',
        'order' => 'DESC',
        'facetwp' => true
    );
    $query = new WP_Query( $args ); 

?>
<div class="serviceFilter pt-2">
    <div class="filterBlock searchByLocation">
        <label>FILTER NEWS</label>
        <?php echo do_shortcode('[facetwp facet="news_categories"]'); ?>
    </div>
</div>

<div class="c-home">
    <?php
            if ( $query->have_posts() ) {
                echo '<div class="o-layout o-layout--medium o-module">';
                while ( $query->have_posts() ) {
                    $query->the_post();
                    ?>
    <div class="o-layout__item u-2/6@tablet u-3/6@mobileLandscape o-module__item newsHolder">
        <div class="c-home__news_bubble">
            <div class="c-home__news_bubble_wrapper">
                <div class="c-home__news_bubble_header">

                    <ul class="newsCat">
                        <?php foreach ( get_the_terms( get_the_ID(), 'news_category' ) as $tax ) { ?>
                        <li><span class="tag"><?php echo $tax->name; ?></span>
                        </li>
                        <?php } ?>
                    </ul>

                    <span><?php echo(get_field('expected_time')); ?> min
                        read&nbsp;&nbsp;•&nbsp;&nbsp;<?php $post_date = get_the_date( 'M Y' ); echo $post_date;?></span>
                </div>
                <div class="c-home__news_bubble_content">
                    <h4><a href="<?php echo(get_permalink());?>"><?php echo(get_the_title());?></a></h4>
                    <p><?php echo(get_field('news_short_description')); ?></p>
                    <a class="readmore" href="<?php echo(get_permalink());?>">Read more</a>
                </div>
            </div>
            <?php
                $image = get_field('thumbnail_image', get_the_ID());
                if ($image) {
                    $image = get_field('thumbnail_image');
                } else {
                    $image = "/wp-content/themes/ncn-health/assets/img/example-blog-image.jpg";
                }
            ?>
            <div class="newsImage">
                <a href="<?php echo(get_permalink());?>" class="imgHolder"
                    style="background: url(<?php echo $image; ?>) no-repeat top center/cover"></a>
            </div>

        </div>
    </div>
    <?php
                }
                echo '</div>';
            }
            /* Restore original Post Data */
            wp_reset_postdata();
        ?>
</div>