<div class="c-resources">
    <div class="c-resources__bg"></div>
    <?php
        $heading = get_sub_field('heading');
        $copy = get_sub_field('copy');
    ?>
    <div class="c-resources__container">
        <h2><?php echo($heading);?></h2>
        <p><?php echo($copy);?></p>
        <div class="o-layout o-layout--medium">
        <?php $lengthofitems = count(get_sub_field('link')); ?>
            <div class="o-layout__item u-1/2@tablet">
                <?php foreach (get_sub_field('link') as $index => $link) { ?>
                    <?php if($index < $lengthofitems/2) { ?>
                        <a href="<?php echo($link['link']); ?>"><?php echo($link['text']); ?></a>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="o-layout__item u-1/2@tablet">
                <?php foreach (get_sub_field('link') as $index1 => $link ) { ?>
                    <?php if($index1 >= $lengthofitems/2) { ?>
                        <a href="<?php echo($link['link']); ?>"><?php echo($link['text']); ?></a>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>