<?php
    $heading = get_sub_field('heading');
    $phone = get_sub_field('phone');
    $email = get_sub_field('email');
    $address = get_sub_field('address');
?>
<div class="c-contactlist">
    <h2><?php echo($heading);?></h2>
    <a href="tel:<?php echo $item['phone'] ?>"><?php echo $item['phone'] ?></a>
    <a href="mailto:<?php echo $item['email'] ?>"><?php echo $item['email'] ?></a>
    <p><?php echo($address);?></p>
</div>