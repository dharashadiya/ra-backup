<?php
    $heading = get_sub_field('title');
    $staff = get_sub_field('staff');
?>
<div class="c-teamslider">
    <h2><?php echo($heading);?></h2>
    <div class="c-teamslider__bg"></div>
    <ul class="c-slider">
        <?php
            foreach ( $staff as $item ) {
                echo('<li class="c-slide">');
                    echo('<div class="c-teamslider__content">');
                    echo('<img src="' . $item['image'] . '">');
                    echo('<h4>' . $item['name'] . '</h4>');
                    echo('<p>' . $item['position'] . '</p>');
                echo('</div></li>');
            }
        ?>
    </ul>
</div>