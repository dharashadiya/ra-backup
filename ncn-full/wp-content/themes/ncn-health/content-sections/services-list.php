<?php
    $heading = get_sub_field('heading');
    $cat = get_sub_field('category');
    $categories = get_terms( 'service_category', ['parent' => $cat[0]]);
    $args = array(
        'post_type' => 'ra-service',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'ASC',
        'orderby' => 'title',
        'tax_query' => array(
            array(
              'taxonomy' => 'service_category',
              'field' => 'id',
              'terms' => $cat,

            )
        )
    );
    $query = new WP_Query( $args );

    $locations = array();

    if ($query->have_posts()) {
        while($query->have_posts()) {
            $query->the_post();
            $locationsReturned = get_field('location');
            foreach ($locationsReturned as $value) {
                array_push($locations, $value);
            }
        }
    }
    $locations = array_unique($locations);
?>
<div class="c-categorylist">
    <h2><?php echo($heading);?></h2>
    <div class="c-categorylist__location">
        <h4>Filter By Location</h4>
        <div class="c-categorylist__location_links">
            <a class="active" data-location="all" href="javascript:void(0);">All</a><?php
                foreach ($locations as $value) {
                    echo('<a data-location="'.$value.'" href="javascript:void(0);">'.$value.'</a>');
                }
            ?>
        </div>
    </div>
    <?php
        if ( $query->have_posts() ) {
            echo '<div class="o-layout o-layout--medium c-categorylist__container o-module">';
            while ( $query->have_posts() ) {
                $query->the_post();
                echo('<div data-location-target="');
                foreach (get_field('location') as $loc) {
                    echo($loc);
                    echo(' ');
                }
                echo('"class="active c-categorylist__button o-layout__item u-1/3@tablet o-module__item"><div class="c-categorylist__item">');
                    printf( '<a href="%1$s"><span>'.get_the_title().'</span></a>',
                        esc_html( get_permalink() )
                    );
                echo('</div></div>');
            }
            echo '</div>';
        }
        /* Restore original Post Data */
        wp_reset_postdata();
    ?>
</div>