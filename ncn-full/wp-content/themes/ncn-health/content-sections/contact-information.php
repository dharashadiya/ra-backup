<?php
    $heading = get_sub_field('heading');
?>
<div class="c-contactlist">
    <h2><?php echo($heading);?></h2>
    <div class="o-layout o-layout--medium c-contactlist__container">
        <?php foreach (get_sub_field('contact') as $item) { ?>
        <div class="o-layout__item u-1/2@tablet c-contactlist__item">
            <h6><?php echo $item['name']; ?></h6>
            <?php if($item['phone']): ?><div class="listBlock"><span>P:&nbsp;</span><a
                    href="tel:<?php echo $item['phone'] ?>">
                    <?php echo $item['phone'] ?></a></div><?php endif; ?>

            <?php if($item['fax']): ?><div class="listBlock"><span>F:&nbsp;</span><a
                    href="tel:<?php echo $item['fax'] ?>">
                    <?php echo $item['fax'] ?></a></div><?php endif; ?>

            <?php if($item['location_address']): ?><div class="listBlock"><span>A:&nbsp;</span>
                <span><?php echo $item['location_address'] ?></span>
            </div><?php endif; ?>

            <?php if($item['email']): ?><div class="listBlock"><span>E:&nbsp;</span><a
                    href="mailto:<?php echo $item['email'] ?>">
                    <?php echo $item['email'] ?></a></div><?php endif; ?>
        </div>
        <?php } ?>
    </div>
</div>