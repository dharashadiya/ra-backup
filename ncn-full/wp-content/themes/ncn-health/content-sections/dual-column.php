<?php
    $header_one = get_sub_field('column_one')['heading'];
    $copy_one = get_sub_field('column_one')['copy'];
    $header_two = get_sub_field('column_two')['heading'];
    $copy_two = get_sub_field('column_two')['copy'];
?>
<div class="o-layout">
    <div class="o-layout__item u-1/2@tablet">
        <h4><?php echo $header_one; ?></h4>
        <p><?php echo $copy_one; ?></p>
    </div>
    <div class="o-layout__item u-1/2@tablet">
        <h4><?php echo $header_two; ?></h4>
        <p><?php echo $copy_two; ?></p>
    </div>
</div>