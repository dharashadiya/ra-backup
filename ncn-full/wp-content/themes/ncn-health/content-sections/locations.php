<div class="c-locations">
    <h2><?php echo($heading);?></h2>
    <div class="o-module locationContainer">
        <?php foreach (get_sub_field('campus') as $item) { ?>
        <div
            class="animate o-layout__item o-module__item u-3/6@mobileLandscape u-2/6@tablet c-post-card animated locationLists">
            <div class="locationHolder">
                <a href="<?php echo $item['link'] ?>;">
                    <h6><?php echo $item['name']; ?></h6>
                    <!-- <img src="<?php //echo($item['image'])?>" alt=""> -->
                    <div class="locationImgHolder" style="background-image: url(<?php echo($item['image'])?>)"></div>
                </a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>