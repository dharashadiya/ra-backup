<?php
    if( get_field('banner_colour') ) {
        $banner_colour = strtolower(get_field('banner_colour'));
    } else {
        $banner_colour = 'blue';
    }
    
    if(is_page()){
        $shownav = get_field('show_nav');
    }
    if(is_tax()){
        $taxonomy_prefix = 'service_category';
        $term_id = get_queried_object_id();
        $term_id_prefixed = $taxonomy_prefix .'_'. $term_id;
        $shownav = get_field('show_nav', $term_id_prefixed);
    } 
?>