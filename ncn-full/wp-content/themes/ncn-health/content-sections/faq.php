<?php
    // IM SO SORRY ABOUT THIS BUT SOMETIMES YOU GOTTA ROLL WITH IT
    $numbermappings = array("one","two","three", "four", "five", "sixa");
?>

<div class="c-faq">
    <div class="c-faq__header">
        <h2><?php echo (get_sub_field('header'));?></h2>
        <div class="c-faq__header_links">
            <?php foreach (get_sub_field('faq_tab') as $faqCounter => $item) { ?>
                  <a class="c-faq__header_link" href="javascript:void(0);" data-faq="<?php echo($faqCounter);?>">Tab <?php echo($numbermappings[$faqCounter]);?></a>
            <?php }?>
        </div>
    </div>
    <?php foreach (get_sub_field('faq_tab') as $faqIndex => $item) { ?>
        <div class="o-layout o-layout--medium">
            <div class="c-faq__table <?php if ($faqIndex == 0) { echo('active');}?>" data-faqtable="<?php echo($faqIndex);?>">
                <div class="o-layout__item u-1/2@tablet">
                    <?php $lengthofitems = count($item['item']); ?>
                    <?php foreach ($item['item'] as $index => $qa) { ?>
                        <?php if($index < $lengthofitems/2) { ?>
                            <h6><?php echo $qa['question']; ?></h6>
                            <p><?php echo $qa['answer']; ?></p>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="o-layout__item u-1/2@tablet">
                    <?php foreach ($item['item'] as $index => $qa ) { ?>
                        <?php if($index >= $lengthofitems/2) { ?>
                            <h6><?php echo $qa['question']; ?></h6>
                            <p><?php echo $qa['answer']; ?></p>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>