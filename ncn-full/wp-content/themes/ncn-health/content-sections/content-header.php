<div class="c-content__header colouredOverlay">

    <!--<div class="c-content__header colouredOverlay" style="background-image: url(<?php echo STYLESHEET_URL; ?>/assets/img/ncn-<?php //echo($banner_colour); ?>-background-overlay.png);">-->

    <?php
        $term = get_queried_object();
        $taxonomy_prefix = $term->taxonomy;
        $term_id = $term->term_id;
        $term_id_prefixed = $taxonomy_prefix .'_'. $term_id;

		$bgi = get_the_post_thumbnail_url($post->ID);
		$bgi_mobile = get_field( 'featured_image_mobile', $post->ID );
		if (!$bgi_mobile) {
			$bgi_mobile = $bgi;
		}
		if (is_tax()) {
			$bgi = get_field( 'banner_image_for_taxonomies', $term_id_prefixed );
			$bgi_mobile = get_field( 'banner_image_for_taxonomies_mobile', $term_id_prefixed );
			if (!$bgi_mobile) {
				$bgi_mobile = $bgi;
			}
			$bgi = $bgi['url'];
			$bgi_mobile = $bgi_mobile['url'];
		}
	?>
	<?php if ($bgi) : ?>
		<div class="c-content__header_image js-changeimage-mob" style="background-image: url(<?php echo $bgi ?>);" data-bg-mobile="<?php echo $bgi_mobile; ?>" ></div>
	<?php else : ?>
		<div class="c-content__header_image"></div>
	<?php endif; ?>



    <div class="o-wrapper">
        <div class="coloredBg banner-<?php if(is_tax()){ the_field( 'banner_colour', $term_id_prefixed ); } else { echo $banner_colour; } ; ?>"
            style="background: url('<?php echo STYLESHEET_URL; ?>/assets/img/flag-<?php if(is_tax()){ the_field( 'banner_colour', $term_id_prefixed ); } else { echo $banner_colour; } ; ?>.png') no-repeat top left/cover;">
            <h1>
                <?php
                if(is_category()){
                    single_cat_title();
                }if(is_tax()){
                    single_term_title();
                } else {
                    the_title();
                }
                ?>
            </h1>
            <div class="mobile-breadcrumb">
                <?php if(function_exists('bcn_display')) {
						bcn_display();
					}
				?>
            </div>
        </div>
    </div>

</div>