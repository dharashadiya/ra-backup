<div class="c-amenities">
    <div class="c-amenities__bg"></div>
    <?php
        $heading = get_sub_field('heading');
        $copy = get_sub_field('copy');
    ?>
    <div class="c-amenities__container">
        <h2><?php echo($heading);?></h2>
        <p><?php echo($copy);?></p>
        <div class="o-layout o-layout--medium">
            <?php $lengthofitems = count(get_sub_field('row')); ?>

            <?php foreach (get_sub_field('row') as $index => $item ) { ?>
            <?php if($index % 3 == 0) { ?>
            <div class="o-layout__item u-1/3@tablet">
                <div class="c-amenities__item">
                    <?php svgicon('icon', '0 0 36 36'); ?>
                    <div class="c-amenities__content">
                        <?php if($item['heading']):?><h4><?php echo($item['heading']);?></h4><?php endif; ?>
                        <?php if($item['copy']):?><p><?php echo($item['copy']);?></p><?php endif; ?>
                    </div>
                </div>
            </div>
            <?php } elseif ($index  % 2 == 0) {?>
            <div class="o-layout__item u-1/3@tablet">
                <div class="c-amenities__item">
                    <?php svgicon('icon', '0 0 36 36'); ?>
                    <div class="c-amenities__content">
                        <?php if($item['heading']):?><h4><?php echo($item['heading']);?></h4><?php endif; ?>
                        <?php if($item['copy']):?><p><?php echo($item['copy']);?></p><?php endif; ?>
                    </div>
                </div>
            </div>
            <?php } else {?>
            <div class="o-layout__item u-1/3@tablet">
                <div class="c-amenities__item">
                    <?php svgicon('icon', '0 0 36 36'); ?>
                    <div class="c-amenities__content">
                        <?php if($item['heading']):?><h4><?php echo($item['heading']);?></h4><?php endif; ?>
                        <?php if($item['copy']):?><p><?php echo($item['copy']);?></p><?php endif; ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>