<?php
    $heading = get_sub_field('heading');
    $item = get_sub_field('item');
?>
<div class="c-testimonial">
    <h2><?php echo($heading);?></h2>
    <div class="c-testimonial__container">
        <?php
            foreach ( $item as $index => $content ) {
                echo('<div class="c-testimonial__item">');
                    echo('<div class="c-testimonial__content">');
                    echo('<p>' . $content['copy'] . '</p>');
                    echo('<h4>' . $content['author'] . '</h4>');
                    echo('<p>' . $content['position'] . '</p>');
                echo('</div></div>');
            }
        ?>
    </div>
</div>