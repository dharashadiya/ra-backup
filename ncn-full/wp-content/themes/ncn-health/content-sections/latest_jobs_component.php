</div>
</div>
</div>


<div class="vacancyComponent">
    <div class="o-wrapper">
        <div class="c-home__news_header">
            <h2>Latest Jobs</h2>
            <a href="<?php echo get_permalink(230); ?>">View All</a>
        </div>

        <div class="vacancyLists">
            <?php $args = array( 'post_type' => 'ra-vacancy', 'posts_per_page' => 3, 'facetwp' => true );
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
            ?>
            <div class="listHolder">
                <div class="listContent">
                    <div class="title pt-1"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>

                    <?php $location_values = get_field( 'location' ); ?>
                    <?php if ( $location_values ) : ?>
                    <div class="location">
                        <?php foreach ( $location_values as $location_value ) : ?>
                        <span><?php echo esc_html( $location_value ); ?></span>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>

                    <div class="thumbnail">
                        <a href="<?php the_permalink(); ?>"
                            style="background: url(<?php the_field( 'thumbnail_image' ); ?>) no-repeat top center/cover"></a>
                    </div>
                    <?php $shortInfo = get_field('role_short_description'); 
                        if($shortInfo) {
                    ?>
                    <div class="shortInfo mb-2">
                        <?php echo strip_tags( wp_trim_words($shortInfo, 25) ) . "..."; ?>
                    </div>
                    <?php } ?>

                    <div class="readMore text-center">
                        <a class="o-btn" href="<?php the_permalink(); ?>">Position Description</a>
                    </div>

                </div>

            </div>
            <?php endwhile; endif; wp_reset_postdata(); ?>
        </div>
    </div>
</div>





<div class="o-wrapper">
    <div class="o-layout o-layout--large3">
        <div class="o-layout__item u-8/10@tabletWide leftBlock">