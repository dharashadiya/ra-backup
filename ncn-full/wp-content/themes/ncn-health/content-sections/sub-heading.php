<div class="subHeadingHolder">
    <?php $sub_heading_copy = get_sub_field('sub_heading_text'); ?>
    <h5><?php echo $sub_heading_copy; ?></h5>
</div>