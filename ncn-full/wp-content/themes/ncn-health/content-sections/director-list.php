<div class="c-director" style="margin-top:<?php the_sub_field( 'margin_top_for_director_block' ); ?>px">
    <div class="o-layout o-layout--medium o-layout--flush">
        <?php foreach (get_sub_field('director') as $item) { ?>
        <div class="o-layout__item u-1/3@tablet">
            <div class="c-director__wrap">
                <img src="<?php echo $item['image']; ?>">
                <h6 class="c-director__name"><?php echo $item['name']; ?></h6>
                <h6 class="c-director__position"><?php echo $item['position']; ?></h6>
                <div class="more">
                    <?php echo strip_tags($item['description'], '<br>'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>