<?php
    $args = array(
        'post_type' => 'ra-vacancy',
        'orderby' => 'date',
        'order' => 'DESC',
    );
    $query = new WP_Query( $args ); 

?>
<div class="c-home" style="padding-top: 3rem">
    <?php
            if ( $query->have_posts() ) {
                echo '<div class="o-layout o-layout--medium o-module">';
                while ( $query->have_posts() ) {
                    $query->the_post();
                    ?>
    <div class="o-layout__item u-1/3@tablet o-module__item">
        <div class="c-home__news_bubble">
            <div class="c-home__news_bubble_wrapper">
                <div class="c-home__news_bubble_header">
                    <ul class="newsCat">
                        <?php foreach ( get_the_terms( get_the_ID(), 'news_category' ) as $tax ) { ?>
                        <li><a href="<?php echo get_category_link( $tax->term_id ); ?>"><?php echo $tax->name; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                    <span><?php echo(get_field('expected_time')); ?> min
                        read&nbsp;&nbsp;•&nbsp;&nbsp;<?php $post_date = get_the_date( 'F Y' ); echo $post_date;?></span>
                </div>
                <div class="c-home__news_bubble_content">
                    <h4><a href="<?php echo(get_permalink());?>"><?php echo(get_the_title());?></a></h4>
                    <p><?php echo(get_field('news_short_description')); ?></p>
                    <a class="readmore" href="<?php echo(get_permalink());?>">Read more</a>
                </div>
            </div>
            <?php
                                $image = get_field('thumbnail_image', get_the_ID());
                                if ($image) {
                                    $image = get_field('thumbnail_image');
                                } else {
                                    $image = "/wp-content/themes/ncn-health/assets/img/example-blog-image.jpg";
                                }
                            ?>
            <img src="<?php echo $image; ?>" alt="">
        </div>
    </div>
    <?php
                }
                echo '</div>';
            }
            /* Restore original Post Data */
            wp_reset_postdata();
        ?>
</div>