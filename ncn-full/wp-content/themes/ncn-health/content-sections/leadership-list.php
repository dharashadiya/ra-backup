<div class="c-leadership">
    <div class="c-leadership__background"></div>
    <h2>Our Leadership Team</h2>
    <div class="o-layout o-layout--medium2">
    <?php foreach (get_sub_field('staff') as $item) { ?>
        <div class="o-layout__item u-1/4@tablet">
            <img src="<?php echo $item['image']; ?>">
            <h6><?php echo $item['first_name']; ?></h6>
            <h6><?php echo $item['last_name']; ?></h6>
            <p><?php echo $item['position']; ?></p>
        </div>
    <?php } ?>
    </div>
</div>