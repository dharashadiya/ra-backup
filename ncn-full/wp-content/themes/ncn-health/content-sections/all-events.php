<?php
    $args = array(
        'post_type' => 'ra-event',
        'orderby' => 'date',
        'order' => 'DESC',
    );
    $query = new WP_Query( $args ); 

?>
<div class="c-events">
    <?php
        if ( $query->have_posts() ) {
            echo '<div class="o-layout o-layout--medium o-module">';
            while ( $query->have_posts() ) {
                $query->the_post();
                ?>
    <div class="o-layout__item u-1/3@tablet o-module__item">
        <div class="c-events__news_bubble">
            <div class="c-events__news_bubble_wrapper">
                <?php
                    $image = get_field('thumbnail_image', get_the_ID());
                    if ($image) {
                        $image = get_field('thumbnail_image');
                    } else {
                        $image = "/wp-content/themes/ncn-health/assets/img/example-blog-image.jpg";
                    }
                ?>
                <!-- <img src="<?php //echo $image; ?>" alt=""> -->
                <div class="eventImage" style="background: url(<?php echo $image; ?>) no-repeat top center/cover"></div>
                <div class="c-events__news_bubble_content">
                    <?php $link = get_field( 'link' ); ?>
                    <div class="contentBlock">

                        <h4><?php if ( $link ) : ?><a href="<?php echo esc_url( $link['url'] ); ?>"
                                target="<?php echo esc_attr( $link['target'] ); ?>"><?php endif; ?><?php echo(get_the_title());?><?php if ( $link ) : ?></a><?php endif; ?>
                        </h4>

                        <?php if(get_field('event_date')):?>
                        <div class="eventDate"><?php the_field( 'event_date' ); ?></div>
                        <?php endif; ?>

                        <!-- <p><?php //echo(get_field('event_short_description')); ?></p> -->
                        <?php $shortInfo = get_field('event_short_description'); ?>
                        <div class="shortInfo"><?php echo strip_tags( substr($shortInfo, 0, 120) ) . "..."; ?></div>
                    </div>
                    <div class="pt-1 readMore test">
                        <?php if ( $link ) : ?><a href="<?php echo esc_url( $link['url'] ); ?>"
                            target="<?php echo esc_attr( $link['target'] ); ?>"><?php endif; ?>
                            More info<?php if ( $link ) : ?></a><?php endif; ?></div>
                </div>
            </div>
        </div>
    </div>
    <?php
            }
            echo '</div>';
        }
        /* Restore original Post Data */
        wp_reset_postdata();
    ?>
</div>