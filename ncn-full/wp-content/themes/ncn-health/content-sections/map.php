<?php
    $heading = get_sub_field('heading');
    $map = get_sub_field('map');
?>
<div class="c-imageslider">
    <h2><?php echo($heading);?></h2>
</div>

<div id="maps">
    <div class="c-home__maps_map" id="map-<?php echo($map);?>"></div>
</div>