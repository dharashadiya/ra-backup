<?php
    $heading = get_sub_field('heading');
    $copy = get_sub_field('copy');
?>
<div class="c-contactlist">
    <h2><?php echo($heading);?></h2>
    <p><?php echo($copy);?></p>
    <div class="o-layout o-layout--medium c-contactlist__container">
        <?php foreach (get_sub_field('contact') as $item) { ?>
        <div class="o-layout__item u-1/2@tablet c-contactlist__item">
            <?php if($item['department']): ?><h6><?php echo $item['department']; ?></h6><?php endif; ?>
            <?php if($item['name']): ?><p><?php echo $item['name']; ?></p><?php endif; ?>
            <?php if($item['position']): ?><div><?php echo $item['position']; ?></div><?php endif; ?>
            <?php if($item['email']): ?><a
                href="mailto:<?php echo $item['email'] ?>"><?php echo $item['email'] ?></a><?php endif; ?>
            <?php if($item['phone']): ?><a
                href="tel:<?php echo $item['phone'] ?>"><?php echo $item['phone'] ?></a><?php endif; ?>
            <?php if($item['fax']): ?><div><a href="tel:<?php echo $item['fax'] ?>"><?php echo $item['fax'] ?></a></div>
            <?php endif; ?>
        </div>
        <?php } ?>
    </div>
</div>