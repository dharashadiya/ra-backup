<?php
$pt=get_sub_field('padding_top');
$pb=get_sub_field('padding_bottom');
?>

<div class="accordionBlock accordions" style="padding-top:<?php echo $pt; ?>; padding-bottom: <?php echo $pb; ?>">
    <ul>
        <?php if ( have_rows( 'accordion_content' ) ) : while ( have_rows( 'accordion_content' ) ) : the_row(); ?>
        <li>
            <div class="question">
                <h3><?php the_sub_field( 'title' ); ?></h3>
            </div>
            <div class="answer"><?php the_sub_field( 'content' ); ?></div>
        </li>
        <?php endwhile; endif; ?>
    </ul>
</div>