<?php
    $heading = get_sub_field('heading');
    $item = get_sub_field('item');
?>
<div class="c-imageslider">
    <h2><?php echo($heading);?></h2>
    <ul class="c-slider">
        <?php
            foreach ( $item as $image ) {
                echo('<li class="c-slide">');
                    echo('<div class="c-imageslider__content">');
                    echo('<img src="' . $image['image'] . '">');
                    echo('<p>' . $image['description'] . '</p>');
                echo('</div></li>');
            }
        ?>
    </ul>
</div>