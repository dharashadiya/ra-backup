<div class="c-sidebyside">
    <?php $sidebysideindex = 1; ?>
    <?php foreach (get_sub_field('row') as $item) { ?>
    <?php
            switch ($item['colour']) {
                case 'Green':
                    $defined_colour = '#00C269';
                    break;
                case 'Blue':
                    $defined_colour = '#05ADB5';
                    break;
                case 'Purple':
                    $defined_colour = '#5E2EAB';
                    break;
                default:
                    $defined_colour = '#424242';
            }
        ?>
    <?php
            if($sidebysideindex % 2 != 0) {
        ?>
    <div class="o-layout o-layout--sidebyside o-module">
        <div class="o-layout__item u-1/2@tablet o-module__item">
            <figure><img src="<?php echo $item['image']; ?>"></figure>
        </div>
        <div class="o-layout__item u-1/2@tablet c-sidebyside__content o-module__item">
            <div class="c-sidebyside__block">
                <h2 style="color: <?php echo $defined_colour; ?>"><?php echo $item['heading']; ?></h2>
                <?php echo $item['copy']; ?>
                <a class="o-btn btn-<?php echo strtolower($item['colour']); ?>"
                    href="<?php echo $item['link']; ?>"><?php echo $item['link_text']; ?></a>
            </div>
        </div>
    </div>
    <?php } else { ?>
    <div class="o-layout o-layout--sidebyside o-module">
        <div class="o-layout__item u-1/2@tablet c-sidebyside__content o-module__item">
            <div class="c-sidebyside__block">
                <h2 style="color: <?php echo $defined_colour; ?>"><?php echo $item['heading']; ?></h2>
                <?php echo $item['copy']; ?>
                <a class="o-btn btn-<?php echo strtolower($item['colour']); ?>"
                    href="<?php echo $item['link']; ?>"><?php echo $item['link_text']; ?></a>
            </div>
        </div>
        <div class="o-layout__item u-1/2@tablet o-module__item">
            <figure><img src="<?php echo $item['image']; ?>"></figure>
        </div>
    </div>
    <?php } ?>
    <?php $sidebysideindex++; } ?>
</div>