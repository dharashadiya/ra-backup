<?php 
$heading = get_sub_field('heading');
$category = get_sub_field( 'category' ); ?>
<?php if ( $category ) : ?>
<?php $get_terms_args = array( 'taxonomy' => 'service_category', 'hide_empty' => 0, 'include' => $category,); ?>
<?php $terms = get_terms( $get_terms_args ); ?>
<div class="c-categorylist">
    <h2><?php echo($heading);?></h2>
    <div class="o-layout o-layout--medium c-categorylist__container o-module">
        <?php foreach ( $terms as $term ) : ?>
        <div class="o-layout__item u-1/3@tablet o-module__item categoryListsCustom">
            <div class="c-categorylist__item">
                <a href="<?php echo esc_url( get_term_link( $term ) ); ?>"><?php echo esc_html( $term->name ); ?></a>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>


<?php /*
    $heading = get_sub_field('heading');
    $cat = get_sub_field('category');
    $categories = get_terms( 'service_category', ['parent' => $cat[0]]);
?>
<div class="c-categorylist">
    <h2><?php echo($heading);?></h2>
    <div class="o-layout o-layout--medium c-categorylist__container o-module">
        <?php
            foreach ( $categories as $index => $category ) {
                echo('<div class="o-layout__item u-1/3@tablet o-module__item"><div class="c-categorylist__item">');
                    printf( '<a href="%1$s"><span>%2$s</span></a>',
                        esc_url( get_category_link( $category->term_id ) ),
                        esc_html( $category->name )
                    );
                echo('</div></div>');
            }
        ?>
    </div>
</div>
<?php OLD CODE WHICH DOES NOT DISPLAY PARENT CATEGORY */ ?>