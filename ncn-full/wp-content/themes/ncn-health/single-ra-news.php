<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<?php
		$header_image = get_the_post_thumbnail_url($post->ID);
		$download_files = get_field('files');
		$description = get_field('news_copy');
		$time = get_field('expected_time');
		$categories = get_the_terms($post->ID, 'news_category');
	?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="c-single__header" style="background-image: none">
        <?php
					if($header_image) {
						echo('<div style="background-image: url('. $header_image .');"></div>');
					} else {
						echo('<div class="c-content__header_image"></div>');
					}
				?>
    </div>
    <div class="c-content__content">
        <div class="o-wrapper">
            <div class="breadcrumb"><a href="" rel="nofollow">Home</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="/our-community/"
                    rel="nofollow">Community</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="/our-community/news/"
                    rel="nofollow">News</a>&nbsp;&nbsp;/&nbsp;&nbsp;<?php the_title(); ?></div>
            <?php if($download_files) {
							echo('<div class="o-layout o-layout--large3"><div class="o-layout__item u-8/10@tabletWide leftBlock">');
						} else {
							echo('<div class="o-layout o-layout--large3"><div class="o-layout__item u-8/10@tabletWide leftBlock">');
						}?>
            <h1><?php the_title(); ?></h1>
            <div class="c-single__articleinfo">
                <?php
									if($categories[0]) {
										echo('<a href="'.get_category_link( $categories[0]->term_id ).'" class="c-single__bubble">'.esc_html( $categories[0]->name ).'</a>');
									}
								?>
                <span><?php echo($time);?> min read • <?php echo(get_the_date('F Y'));?></span>
            </div>
            <?php echo($description); ?>
        </div>
        <?php if($download_files) {
							echo('<div class="o-layout__item u-2/10@tabletWide">');
							echo('<h6>Download Files</h6>');
							echo '<ul class="c-sidenav">';
							foreach ($download_files as $file) {
								echo '<li class=""><a href='.$file['file'].'>'.$file['file_name'].'</a></li>';
							}
							echo('</div>');
							echo('</div>');
						}?>
    </div>
    </div>
    </div>
    <?php wp_link_pages(); ?>
</article>
<?php endwhile; ?>
<?php get_footer(); ?>