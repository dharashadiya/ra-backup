<footer class="c-footer">
    <div class="c-subscribe">
        <div class="o-wrapper">
            <div class="o-layout">
                <div class="o-layout__item u-1/2@tablet">
                    <p class="c-subscribe__copy">STAY UP-TO-DATE WITH OUR COMMUNITY NEWSLETTER</p>
                </div>
                <div class="o-layout__item u-1/2@tablet">
                    <?php //echo do_shortcode( '[contact-form-7 id="11" title="Monthly Newsletter"]' ); ?>
                    <div>
                        <form class="js-cm-form" id="subForm"
                            action="https://www.createsend.com/t/subscribeerror?description=" method="post"
                            data-id="2BE4EF332AA2E32596E38B640E905619615D887AEA7A54179F9CD475C0E1ED858AE465F41593A2F424978C890B461D0E83CDD9A3DA7FE81D28C41B6D2000AFDE">
                            <div class="subscribeFormHolder">
                                <div class="inputHolder">
                                    <input autocomplete="Email" aria-label="Email"
                                        class="js-cm-email-input qa-input-email" id="fieldEmail" maxlength="200"
                                        name="cm-yuiudyu-yuiudyu" placeholder="Enter your email address" required=""
                                        type="email">
                                </div>
                                <div class="buttonHolder">
                                    <button class="wpcf7-submit" type="submit">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <script type="text/javascript"
                        src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>
                </div>
            </div>
        </div>
    </div>
    <div class="c-emergency">
        <div class="o-wrapper">
            <a href="tel:000">
                <h4>In an emergency call 000</h4>
            </a>
        </div>
    </div>
    <div class="c-bottom">
        <div class="o-wrapper">
            <div class="o-layout c-bottom__flex">
                <div class="o-layout__item u-1/2@tablet">
                    <img src="<?php echo STYLESHEET_URL; ?>/assets/img/ncn-logo-white.png" alt="NCN Logo"
                        class="c-bottom__logo">
                    <div class="c-bottom__content socialLinks socialLinksMobile">
                        <span>Follow us</span>
                        <span><a href="https://www.facebook.com/ncnhealth/" target="_blank" rel="nofollow"
                                class="c-bottom__social">
                                <img src="<?php echo STYLESHEET_URL; ?>/assets/img/Facebook.png" alt="Facebook">
                            </a></span>
                        <?php /*
                        <span><a href="https://twitter.com/ncnhealth" target="_blank" rel="nofollow"
                                class="c-bottom__social">
                                <img src="<?php echo STYLESHEET_URL; ?>/assets/img/Twitter.png" alt="Twitter">
                        </a></span> */?>
                    </div>

                    <div class="c-bottom__content">
                        <img src="<?php echo STYLESHEET_URL; ?>/assets/img/aboriginal-flag.jpg" alt="Aboriginal Flag"
                            class="c-bottom__flag">
                        <img src="<?php echo STYLESHEET_URL; ?>/assets/img/torres-flag.jpg" alt="Torres Strait Flag"
                            class="c-bottom__flag">
                        <p>NCN Health acknowledges the Traditional Owners of the land we gather and work on and pay
                            respects to their Elders, past and present.</p>
                    </div>
                    <div class="c-bottom__content">
                        <img src="<?php echo STYLESHEET_URL; ?>/assets/img/pride-flag.jpg" alt="Pride Flag"
                            class="c-bottom__flag">
                        <img src="<?php echo STYLESHEET_URL; ?>/assets/img/lbgtiq-flag.jpg" alt="LBTIQ Flag"
                            class="c-bottom__flag">
                        <p>NCN Health celebrates gender, sexual & body diversity.</p>
                    </div>
                    <div class="c-bottom__content socialLinks socialLinksDesk">
                        <span>Follow us</span>
                        <span><a href="https://www.facebook.com/ncnhealth/" target="_blank" rel="nofollow"
                                class="c-bottom__social">
                                <img src="<?php echo STYLESHEET_URL; ?>/assets/img/Facebook.png" alt="Facebook">
                            </a></span>
                        <?php /*
                        <span><a href="https://twitter.com/ncnhealth" target="_blank" rel="nofollow"
                                class="c-bottom__social">
                                <img src="<?php echo STYLESHEET_URL; ?>/assets/img/Twitter.png" alt="Twitter">
                        </a></span> */?>
                    </div>
                </div>
                <div class="o-layout__item u-1/2@tablet c-bottom__flexer">
                    <div class="c-bottom__linklists">
                        <ul>
                            <li>Quick Links</li>
                            <li><a href="/about">About</a></li>
                            <li><a href="/services">Services</a></li>
                            <li><a href="<?php echo get_permalink(88); ?>">Contact</a></li>
                            <li><a href="<?php echo get_permalink(70); ?>">Locations</a></li>
                            <li><a href="<?php echo get_permalink(90); ?>">Staff Login</a></li>
                        </ul>
                        <ul>
                            <li>Careers &amp; Volunteers</li>
                            <li><a href="<?php echo get_permalink(230); ?>">Current Vacancies</a></li>
                            <li><a href="<?php echo get_permalink(233); ?>">Graduate Nursing Program</a></li>
                            <li><a href="<?php echo get_permalink(235); ?>">Students &amp; Education</a></li>
                            <li><a href="<?php echo get_permalink(237); ?>">Volunteers</a></li>
                        </ul>
                    </div>
                    <div class="c-bottom__extra">
                        <ul>
                            <li><a href="<?php echo get_permalink(148); ?>">Privacy Policy</a></li>
                            <li><a href="/terms-conditions/">Terms &amp; Conditions</a></li>
                            <li>&copy; <?php echo date("Y"); ?> NCN Health</li>
                        </ul>
                        <div class="scrollTop"> <a href="#"> <img src="<?php echo ASSETS; ?>/img/scroll-top.svg"
                                    alt="Scroll top"> </a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="c-search__container" data-toggle-content="search">
    <div class="o-wrapper o-wrapper--1080">
        <a class="c-overlay__close" data-toggle="search"><img src="<?php echo ASSETS; ?>/img/close.svg" alt="Close"></a>
        <div class="c-search">
            <form role="search" method="get" class="c-action  c-search__form search-form"
                action="<?php echo home_url( '/' ); ?>">
                <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
                <input type="search" class="search-field c-search__input"
                    placeholder="<?php echo esc_attr_x( 'Enter your search', 'placeholder' ) ?>"
                    value="<?php echo get_search_query() ?>" name="s"
                    title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" id="search" />
                <input type="submit" value="Submit" class="c-search__submit o-btn btn-purple" />
            </form>
        </div>
    </div>
</div>

<a href="#" class="scrollTopBtn"><img src="<?php echo ASSETS; ?>/img/scroll-top.svg" alt="Scroll top"></a>
<?php wp_footer(); ?>
</body>

</html>