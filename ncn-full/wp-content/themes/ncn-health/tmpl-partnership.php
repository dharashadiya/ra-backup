<?php
/*
 * Template Name: Partnership
 */
?>

<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ;
    include('content-sections/banner.php');
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php include('content-sections/content-header.php'); ?>
    <div class="o-wrapper">
        <div class="breadcrumb" style="margin-top: 24px">
            <?php if(function_exists('bcn_display')) {
                    bcn_display();
                }
            ?>
        </div>
        <div class="partnershipList">
            <?php if ( have_rows( 'partnership_content' ) ) : while ( have_rows( 'partnership_content' ) ) : the_row(); ?>
            <div class="partnershipBlock">
                <?php $logo_partnership = get_sub_field( 'logo_partnership' ); ?>
                <div class="imgBlock"
                    style="background: url(<?php echo esc_url( $logo_partnership['url'] ); ?>) no-repeat center/contain">
                </div>
                <div class="contentBlock">
                    <h3><?php the_sub_field( 'title_partnership' ); ?></h3>
                    <div class="txt"><?php the_sub_field( 'content' ); ?></div>
                </div>
            </div>
            <?php endwhile; endif; ?>
        </div>
    </div>



</article>


<?php endwhile; ?>
<?php get_footer(); ?>