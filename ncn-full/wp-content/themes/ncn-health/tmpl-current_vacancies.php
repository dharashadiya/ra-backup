<?php
/*
 * Template Name: Current Vacancies
 */
?>

<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ;
    include('content-sections/banner.php');
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php include('content-sections/content-header.php'); ?>
    <div class="serviceWrapper">
        <div class="o-wrapper">
            <div class="breadcrumb">
                <?php if(function_exists('bcn_display')) {
                        bcn_display();
                    }
                ?>
            </div>

            <div class="introService">
                <h2><?php the_field( 'quick_intro_title' ); ?></h2>
                <?php the_field( 'quick_intro_content' ); ?>
            </div>

            <div class="applicationProcess">
                <h2><?php the_field( 'title_for_the_accordion' ); ?></h2>
                <div class="accordionContent">
                    <?php the_field( 'hidden_accordion_content' ); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="servicesList mt-3">
        <div class="o-wrapper">
            <div class="serviceFilter">
                <div class="filterBlock searchBlock">
                    <label>Search Vacancies</label>
                    <?php echo do_shortcode('[facetwp facet="search_services"]'); ?>
                </div>
                <div class="filterBlock searchByCat">
                    <label>Select Category</label>
                    <?php echo do_shortcode('[facetwp facet="vacancy_categories"]'); ?>
                </div>
                <div class="filterBlock searchByLocation">
                    <label>Filter by location</label>
                    <?php echo do_shortcode('[facetwp facet="vacancy_location"]'); ?>
                </div>
            </div>

            <div class="facetSelections">
                <?php echo facetwp_display( 'selections' ); ?>
            </div>

            <div class="vacancyLists">
                <?php $args = array( 'post_type' => 'ra-vacancy', 'posts_per_page' => 9, 'facetwp' => true );
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                ?>
                <div class="listHolder">
                    <div class="listContent">
                        <div class="title pt-1"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>

                        <?php $location_values = get_field( 'location' ); ?>
                        <?php if ( $location_values ) : ?>
                        <div class="location">
                            <?php foreach ( $location_values as $location_value ) : ?>
                            <span><?php echo esc_html( $location_value ); ?></span>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>

                        <div class="thumbnail">
                            <a href="<?php the_permalink(); ?>"
                                style="background: url(<?php the_field( 'thumbnail_image' ); ?>) no-repeat top center/cover"></a>
                        </div>
                        <?php $shortInfo = get_field('role_short_description'); 
                            if($shortInfo) {
                        ?>
                        <div class="shortInfo mb-2">
                            <?php echo strip_tags( wp_trim_words($shortInfo, 18) ) . "..."; ?>
                        </div>
                        <?php } ?>

                        <div class="readMore text-center">
                            <a class="o-btn" href="<?php the_permalink(); ?>">Position Description</a>
                        </div>

                    </div>

                </div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>

            <div class="text-center loadMore pt-2"><?php echo do_shortcode('[facetwp facet="load_more"]'); ?></div>


        </div>
    </div>


</article>


<?php endwhile; ?>
<?php get_footer(); ?>
<script>
$('.applicationProcess h2').on('click', function() {
    $(this).toggleClass('active');
    $('.applicationProcess .accordionContent').slideToggle();
})
</script>