<?php
/*
 * Template Name: Content Page
 */
?>

<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ;
		include('content-sections/banner.php');
	?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php include('content-sections/content-header.php'); ?>


    <?php $show_latest_news = get_field('show_latest_news'); ?>
    <div class="c-content__content <?php echo $show_latest_news ? 'has-latest-news' : '';?>">

        <div class="o-wrapper">
            <div class="breadcrumb">
                <?php if(function_exists('bcn_display')) {
						bcn_display();
					}
				?>
            </div>
            <?php
				if($shownav) {
					echo('<div class="o-layout o-layout--large3"><div class="o-layout__item u-8/10@tabletWide leftBlock">');
				} else {
					echo('<div class="o-layout o-layout--large3"><div class="o-layout__item u-8/10@tabletWide leftBlock">');
				}
			?>
            <?php
				if( have_rows('content') ): while( have_rows('content') ): the_row();
					if( get_row_layout() == 'heading' ):
						include('content-sections/heading.php');
					elseif( get_row_layout() == 'sub_heading' ): 
						include('content-sections/sub-heading.php');
					elseif( get_row_layout() == 'copy' ): 
						include('content-sections/copy.php');
					elseif( get_row_layout() == 'dual_column' ):
						include('content-sections/dual-column.php');
					elseif( get_row_layout() == 'director_list' ):
						include('content-sections/director-list.php');
					elseif( get_row_layout() == 'leadership_list' ):
						include('content-sections/leadership-list.php');
					elseif( get_row_layout() == 'key_contacts' ):
						include('content-sections/key_contacts.php');
					elseif( get_row_layout() == 'side_by_side' ):
						include('content-sections/side-by-side.php');
					elseif( get_row_layout() == 'image' ): 
						include('content-sections/image.php');
					elseif( get_row_layout() == 'faq' ): 
						include('content-sections/faq.php');
					elseif( get_row_layout() == 'information_and_resources' ): 
						include('content-sections/resources.php');
					elseif( get_row_layout() == 'a' ): 
						include('content-sections/resources.php');
					elseif( get_row_layout() == 'services_category' ): 
						include('content-sections/services-category.php');
					elseif( get_row_layout() == 'services_list' ): 
						include('content-sections/services-list.php');
					elseif( get_row_layout() == 'amenities_&_programs' ): 
						include('content-sections/amenities-programs.php');
					elseif( get_row_layout() == 'team_slider' ): 
						include('content-sections/team-slider.php');
					elseif( get_row_layout() == 'image_slider' ): 
						include('content-sections/image-slider.php');
					elseif( get_row_layout() == 'testimonial' ): 
						include('content-sections/testimonial.php');
					elseif( get_row_layout() == 'map' ): 
						include('content-sections/map.php');
					elseif( get_row_layout() == 'contact_information' ): 
						include('content-sections/contact-information.php');
					elseif( get_row_layout() == 'single_contact' ): 
						include('content-sections/single-contact.php');
					elseif( get_row_layout() == 'locations' ): 
						include('content-sections/locations.php');
					elseif( get_row_layout() == 'accordion_block' ): 
						include('content-sections/accordion_block.php');
					elseif( get_row_layout() == 'latest_news_component' ): 
						include('content-sections/latest_news_component.php');
					elseif( get_row_layout() == 'latest_jobs_component' ): 
						include('content-sections/latest_jobs_component.php');
					elseif( get_row_layout() == 'latest_events_component' ): 
						include('content-sections/latest_events_component.php');
					endif;
				endwhile; endif; 
			?>
            <?php
					if (is_page('services')) {
						include('content-sections/all-services.php');
					}
					if (is_page('events')) {
						include('content-sections/all-events.php');
					}
					if (is_page('news')) {
						include('content-sections/all-news.php');
					}
					if (is_page('current-vacancies')) {
						include('content-sections/all-vacancies.php');
					}
					if (is_page('contact')) {
						echo '<div class="contactFormHolder">';
						echo do_shortcode('[contact-form-7 id="473" title="Main Contact Submission"]');
						echo '</div>';
					}
				?>
        </div>

        <?php if($shownav) {
			echo('<div class="o-layout__item sideNavigation u-2/10@tabletWide">');
			echo('<div class="c-sidenav">' . get_sidenav() . '</div>');
		}?>
    </div>
    </div>
    <?php 
	/*
		$show_latest_news = get_field('show_latest_news');
	?>
    <?php if($show_latest_news) : ?>
    <?php include('home-sections/home-news.php'); ?>
    <?php else: ?>
    <?php endif; */?>
    </div>
    <?php wp_link_pages(); ?>
</article>
<?php endwhile; ?>
<?php get_footer(); ?>