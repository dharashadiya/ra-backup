<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
<?php
		$header_image = get_the_post_thumbnail_url($post->ID);
		$download_files = get_field('files');
		$description = get_field('role_description');
	?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php 
    if( get_field('banner_colour') ) {
    $banner_colour = strtolower(get_field('banner_colour'));
    } else {
    $banner_colour = 'blue';
	}
	?>
    <?php include('content-sections/content-header.php'); ?>
    <div class="c-content__content">
        <div class="o-wrapper">
            <div class="breadcrumb"><a href="" rel="nofollow">Home</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a
                    href="/careers-volunteers/" rel="nofollow">Careers &amp; Volunteers</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a
                    href="/careers-volunteers/current-vacancies/" rel="nofollow">Current
                    Vacancies</a>&nbsp;&nbsp;/&nbsp;&nbsp;<?php the_title(); ?></div>
            <?php if($download_files) {
							echo('<div class="o-layout o-layout--large3"><div class="o-layout__item u-8/10@tablet">');
						} else {
							echo('<div><div>');
						}?>
            <h1><?php the_title(); ?></h1>
            <?php echo($description); ?>
        </div>
        <?php if($download_files) {
							echo('<div class="o-layout__item u-2/10@tablet">');
							echo('<h6>Download Files</h6>');
							echo '<ul class="c-sidenav">';
							foreach ($download_files as $file) {
								echo '<li class=""><a href='.$file['file'].'>'.$file['file_name'].'</a></li>';
							}
							echo('</div>');
							echo('</div>');
						}?>
    </div>
    </div>
    </div>
    <?php wp_link_pages(); ?>
</article>
<?php endwhile; ?>
<?php get_footer(); ?>