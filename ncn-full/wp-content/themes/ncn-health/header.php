<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- make IE render page content in highest IE mode available -->
    <title><?php wp_title(); ?> </title>

    <?php if(is_local_or_staging()) : ?>
    <meta name="robots" content="none" />
    <meta name="robots" content="noindex" />
    <meta name="googlebot" content="none" />
    <meta name="googlebot" content="noindex" />
    <?php endif; ?>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="<?php echo AUTHOR; ?>">
    <meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
    <meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
    <meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-Q71RNVJVQZ"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-Q71RNVJVQZ');
    </script>
    <!-- <link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico"> -->
    <link rel="icon" type="image/png" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.png" />
    <link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120"
        href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152"
        href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800&display=swap"
        rel="stylesheet">
    <?php wp_head(); ?>
    <?php include('partials/inlinescripts.min.php'); ?>
</head>
<?php $gray_bg = get_field('add_gray_background_to_the_page'); ?>
<?php if($gray_bg == 1) { ?>
<style>
body {
    background: #f6f6f6
}
</style>
<?php } ?>

<body <?php body_class(); ?>>



    <?php if(get_field('enabled', 'options')) { ?>
    <style>
    .c-notification {
        display: none;
    }
    </style>


    <?php
        $background = get_field('colour', 'options');
        $text = get_field('text', 'options');
        $link = get_field('link', 'options');
        $mobileText = get_field( 'text_to_show_on_mobile', 'option' );
        

        //$cookie = $_COOKIE['notification-cookie'];
        // if ($cookie != $text) {
        //     setcookie('notification-cookie', $text);
        //     echo('<div class="c-notification" style="background-color: ' . $background . ';">');
        //     echo('<div class="o-wrapper">');
        //     echo('<a href="' . $link . '">' . $text . '</a>');
        //     echo('</div>');
        //     echo('</div>');
        // }
    }
    ?>
    <div class="c-notification" style="background: <?php echo $background; ?>">
        <div class="o-wrapper">
            <div class="c-notification__content">
                <a class="desktop" href="<?php echo $link; ?>"><?php echo $text; ?></a>
                <a class="mobileOnly" href="<?php echo $link; ?>"><?php echo $mobileText; ?></a>
            </div>
        </div>
        <a href="#" id="close">
            <svg id="Capa_1" enable-background="new 0 0 413.348 413.348" height="512" viewBox="0 0 413.348 413.348"
                width="512" xmlns="http://www.w3.org/2000/svg">
                <path class="closeIcon"
                    d="m413.348 24.354-24.354-24.354-182.32 182.32-182.32-182.32-24.354 24.354 182.32 182.32-182.32 182.32 24.354 24.354 182.32-182.32 182.32 182.32 24.354-24.354-182.32-182.32z"
                    fill="#ffffff" />
            </svg>
        </a>
    </div>
    <div class="c-top">
        <header class="c-header o-wrapper">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/about">About</a></li>
                <li><a href="/contact">Contact</a></li>
                <li><a href="/locations">Locations</a></li>
                <li><a href="<?php echo get_permalink(90); ?>">Staff Login</a></li>
            </ul>
        </header>
    </div>
    <div class="c-banner">
        <div class="o-wrapper">
            <a href="/">
                <img src="<?php echo STYLESHEET_URL; ?>/assets/img/ncn-logo-colour.jpg" alt="NCN Logo"
                    class="c-banner__logo">
            </a>
            <div class="c-banner__right">
                <div class="c-banner__right_flex">
                    <?php
					wp_nav_menu( array(
						'theme_location' => 'primary-menu',
						'container' => false,
						'menu_class' => 'c-nav',
						'menu_id' => 'menu',
						'link_before' => '<svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path class="arrow" fill-rule="evenodd" clip-rule="evenodd" d="M6.09504 7.62991L0.184153 1.97062C-0.0613844 1.72422 -0.0613844 1.32412 0.184153 1.07771L1.07328 0.184805C1.31881 -0.0616018 1.71686 -0.0616018 1.9624 0.184805L6.53928 4.50537C6.78482 4.75177 7.1835 4.75177 7.42841 4.50537L12.0376 0.184805C12.2831 -0.0616018 12.6812 -0.0616018 12.9267 0.184805L13.8158 1.07771C14.0614 1.32412 14.0614 1.72422 13.8158 1.97062L7.87329 7.62991C7.38221 8.12336 6.58611 8.12336 6.09504 7.62991Z" fill="#424242"/>
							</svg><span>',
						'link_after' => '</span>'
					));
				?>
                    <a href="javascript:void(0);" class="c-banner__right_icon">

                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" class="mainSearch"
                                d="M23.8004 21.8591L17.5906 15.6033C17.3574 15.3688 17.3341 15.0075 17.5145 14.7304C20.0605 10.8253 19.4667 5.45483 15.7104 2.24708C12.096 -0.838616 6.59454 -0.733017 3.09262 2.48159C-0.927007 6.17139 -1.02849 12.4518 2.78954 16.2726C6.04529 19.5311 11.0846 19.9267 14.7853 17.4787C15.0631 17.295 15.4272 17.319 15.6617 17.5549L21.8578 23.7976C22.1259 24.067 22.5606 24.0677 22.8294 23.7989L23.799 22.8287C24.0664 22.5612 24.0671 22.1279 23.8004 21.8591ZM4.72874 14.3313C2.07915 11.6797 2.07915 7.36522 4.72874 4.71358C7.37832 2.06194 11.6894 2.06125 14.339 4.71358C16.9885 7.36522 16.9885 11.6797 14.339 14.3313C11.6894 16.983 7.37832 16.983 4.72874 14.3313Z"
                                fill="#424242" />
                        </svg>

                    </a>
                    <a href="<?php echo get_permalink(578); ?>" class="o-btn o-btn--nav extraNav">Telehealth</a>
                </div>
            </div>


        </div>
        <div id="nav-icon3">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <a href="javascript:void(0);"
            class="c-banner__right_icon forMobile"><?php svgicon('search-icon', '0 0 24 24'); ?></a>
    </div>

    <nav class="c-site-mobile animate">
        <div class="mobileMenu">
            <div class="navHolder">
                <div class="mobileNavWrapper">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'mobile-menu',
                            'container' => false,
                            'menu_class' => 'c-nav-mobile',
                        ));
                    ?>
                    <div class="socialLinksMobile">
                        <div class="telehealth">
                            <a href="<?php echo get_permalink(578); ?>" class="o-btn btn-purple">Telehealth</a>
                        </div>
                        <div class="quicksocialLinks">
                            <a href="https://www.facebook.com/ncnhealth/" target="_blank" rel="nofollow"><img
                                    src="<?php echo ASSETS; ?>/img/facebook.svg" alt="Facebook"></a>
                            <?php /*<a href="https://twitter.com/ncnhealth" target="_blank" rel="nofollow"><img
                                    src="<?php echo ASSETS; ?>/img/twitter.svg" alt="Twitter"></a> */?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div class="c-content">