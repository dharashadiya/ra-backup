<div class="c-home__services">
    <div class="o-wrapper">
        <h2>50+ services across these categories</h2>


        <div class="o-layout o-module">
            <?php if ( have_rows( 'choose_service_categories' ) ) : while ( have_rows( 'choose_service_categories' ) ) : the_row(); ?>



            <div class="o-layout__item o-module__item u-1/5@tablet">
                <?php $service_category_link = get_sub_field( 'service_category_link' ); ?>
                <?php $get_terms_args = array(
                    'taxonomy' => 'service_category',
                    'hide_empty' => 0,
                    'include' => $service_category_link,
                ); ?>
                <?php $terms = get_terms( $get_terms_args ); ?>
                <?php if ( $terms ) { ?>
                <?php foreach ( $terms as $term ) : ?>
                <a href="<?php echo esc_url( get_term_link( $term ) ); ?>" class="c-home__services_bubble">
                    <?php $icon = get_sub_field( 'icon' ); ?>
                    <?php svgicon('back-arrow', '0 0 14 13'); ?>
                    <div class="c-home__services_bubble_content">
                        <div class="iconBox"><?php if ( $icon ) : ?><img class="style-svg"
                                src="<?php echo esc_url( $icon['url'] ); ?>"
                                alt="<?php echo esc_attr( $icon['alt'] ); ?>" /><?php endif; ?></div>
                        <p><?php echo esc_html( $term->name ); ?></p>
                    </div>
                </a>
                <?php endforeach; ?>
                <?php } ?>
            </div>
            <?php endwhile; endif; ?>


        </div>
        <a class="o-btn o-btn__services btn-purple" href="/services">Explore All Services</a>
    </div>
</div>