<div class="c-home__values">
    <div class="o-wrapper">
        <h2><?php the_field( 'values_main_title', 7 ); ?></h2>
        <div class="o-layout">
            <?php if ( have_rows( 'values_content' ) ) : while ( have_rows( 'values_content' ) ) : the_row(); ?>
            <div class="o-layout__item u-1/3@tablet">
                <div class="c-home__values_bubble">
                    <h4><?php the_sub_field( 'title' ); ?></h4>
                    <p><?php the_sub_field( 'short_excerpt' ); ?></p>
                </div>
            </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</div>