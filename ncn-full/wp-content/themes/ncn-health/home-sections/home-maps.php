<div class="c-home__maps">
    <div class="o-wrapper">
        <div class="c-home__maps_header">
            <h2>Where to find us</h2>
            <a href="/contact">Contact Us</a>
        </div>
        <div id="maps" class="o-layout">
            <div class="o-layout__item u-1/3@tablet">
                <div class="c-home__maps_item">
                    <h4>Nathalia Campus</h4>
                    <div class="c-home__maps_map" id="map-nathalia"></div>
                    <p>36-44 McDonell St,<br>Nathalia VIC 3638<br>(03) 5866 9444</p>
                </div>
            </div>
            <div class="o-layout__item u-1/3@tablet">
                <div class="c-home__maps_item">
                    <h4>Cobram Campus</h4>
                    <div class="c-home__maps_map" id="map-cobram"></div>
                    <p>24-32 Broadway St,<br>Cobram VIC 3644<br>(03) 5871 0777</p>
                </div>
            </div>
            <div class="o-layout__item u-1/3@tablet">
                <div class="c-home__maps_item">
                    <h4>Numurkah Campus</h4>
                    <div class="c-home__maps_map" id="map-numurkah"></div>
                    <p>2 Katamatite Rd,<br>Numurkah VIC 3636<br>(03) 5862 0555</p>
                </div>
            </div>
        </div>
    </div>
</div>