/* global site */
// [1] Import functions here
import {
	isMobile,
	openLinkNewTab,
	superfishMenu,
	searchFunc,
	cloudinaryImageCDN,
	faqSwitcher,
	locationSelector,
	slicker,
	customMatchHeight,
	imageChangeMob,
	injectContentIntoMenu
} from "./template-functions";
import {
	googleMap
} from "./map";
(function($) {
	window.site || (window.site = {});
	$.extend(
		site,
		{
			initVars: () => {
				// Global variables -> use as site.VariableName in other files
				site.body = $("body");
				site.root = $("html");
				site.header = $("#Top");
				site.main = $("#Main");
				site.side = $("#Secondary");
				site.siteurl = $("#Siteurl").attr("content");
				site.themeurl = $("#Themeurl").attr("content");
				return (site.ajaxurl = $("#Ajaxurl").attr("content"));
			},
			// [3] Call functions within either of these
			onReady: () => {
				// svg4everybody();
				site.initVars();
				site.searchFunc();
				site.superfishMenu();
				site.faqSwitcher();
				site.locationSelector();
				site.slicker();
				site.customMatchHeight();
				site.injectContentIntoMenu();
				site.imageChangeMob();
				return site.isMobile();
			},
			onLoad: () => {
				return
			}
		},
		{
			// [2] Register functions here
			isMobile: isMobile,
			openLinkNewTab: openLinkNewTab,
			superfishMenu: superfishMenu,
			searchFunc: searchFunc,
			cloudinaryImageCDN: cloudinaryImageCDN,
			faqSwitcher: faqSwitcher,
			locationSelector: locationSelector,
			slicker: slicker,
			customMatchHeight: customMatchHeight,
			imageChangeMob: imageChangeMob,
			injectContentIntoMenu: injectContentIntoMenu,
		}
	);
	$(() => {
		return site.onReady();
	});
	return $(window).on("load", () => {
		return site.onLoad();
	});
})(jQuery);
