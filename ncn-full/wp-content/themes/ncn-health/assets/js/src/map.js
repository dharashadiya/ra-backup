(function($) {
	window.console ||
		(window.console = {
			log: function() {}
		});
	window.googleMap || (window.googleMap = {});
	$.extend(googleMap, {
		init_vars: function() {
			// Global variables -> use as googleMap.VariableName in other files
			this.apiKey = 'AIzaSyAslcGFnsmByU88y_LJfiUufynmRkTRGlo';
			this.markerWidth = 46.4;
			this.markerHeight = 50;
			return 
		},
		onready: function() {
			this.init_vars();
			if ($("#maps").length > 0) {
				return this.loadMap();
			}
		},
		loadMap: function() {
			var isDraggable;
			var styles = [
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#e9e9e9"
						},
						{
							"lightness": 17
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#f5f5f5"
						},
						{
							"lightness": 20
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 17
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 29
						},
						{
							"weight": 0.2
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 18
						}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 16
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#f5f5f5"
						},
						{
							"lightness": 21
						}
					]
				},
				{
					"featureType": "poi.park",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#dedede"
						},
						{
							"lightness": 21
						}
					]
				},
				{
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"color": "#ffffff"
						},
						{
							"lightness": 16
						}
					]
				},
				{
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"saturation": 36
						},
						{
							"color": "#333333"
						},
						{
							"lightness": 40
						}
					]
				},
				{
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#f2f2f2"
						},
						{
							"lightness": 19
						}
					]
				},
				{
					"featureType": "administrative",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#fefefe"
						},
						{
							"lightness": 20
						}
					]
				},
				{
					"featureType": "administrative",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#fefefe"
						},
						{
							"lightness": 17
						},
						{
							"weight": 1.2
						}
					]
				}
			]

			// isDraggable = if Modernizr.touchevents then false else true
			isDraggable = true;
			return $.getScript(
				"https://maps.googleapis.com/maps/api/js?key=" + this.apiKey,
				function() {
					this.nathalia = $("#map-nathalia").length > 0
					this.cobram = $("#map-cobram").length > 0
					this.numurkah = $("#map-numurkah").length > 0
					var mapCanvas1, mapOptions1;
					mapCanvas1 = document.getElementById("map-nathalia");
					mapOptions1 = {
						center: new google.maps.LatLng(
							-36.056341,
							145.197655
						),
						zoom: 15,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						scrollwheel: false,
						mapTypeControl: false,
						streetViewControl: false,
						styles: styles
					};
					var mapCanvas2, mapOptions2;
					mapCanvas2 = document.getElementById("map-cobram");
					mapOptions2 = {
						center: new google.maps.LatLng(
							-35.9160355,
							145.6491806
						),
						zoom: 15,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						scrollwheel: false,
						mapTypeControl: false,
						streetViewControl: false,
						styles: styles
					};
					var mapCanvas3, mapOptions3;
					mapCanvas3 = document.getElementById("map-numurkah");
					mapOptions3 = {
						center: new google.maps.LatLng(
							-36.0993453,
							145.4435593
						),
						zoom: 15,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						scrollwheel: false,
						mapTypeControl: false,
						streetViewControl: false,
						styles: styles
					};

					if (this.nathalia) {
						this.map1 = new google.maps.Map(mapCanvas1, mapOptions1);
						googleMap.map1 = this.map1;

					}
					if (this.cobram) {
						this.map2 = new google.maps.Map(mapCanvas2, mapOptions2);
						googleMap.map2 = this.map2;
					}
					if (this.numurkah) {
						this.map3 = new google.maps.Map(mapCanvas3, mapOptions3);
						googleMap.map3 = this.map3;
					}
					return googleMap.markers();
				}
			);
		},
		fixBounds: function(points) {
			var bounds, i, len, point;
			bounds = new google.maps.LatLngBounds();
			for (i = 0, len = points.length; i < len; i++) {
				point = points[i];
				bounds.extend(point);
			}
			return this.map.fitBounds(bounds);
		},
		markers: function() {
			var image, marker1, marker2, marker3;
			this.nathalia = $("#map-nathalia").length > 0
			this.cobram = $("#map-cobram").length > 0
			this.numurkah = $("#map-numurkah").length > 0
			image = {
				url: window.site.themeurl + "/assets/img/marker.png",
				scaledSize: new google.maps.Size(
					this.markerWidth,
					this.markerHeight
				)
			};
			const lat_lng1 = {
				lat: -36.056341,
				lng: 145.197655
			};
			const lat_lng2 = {
				lat: -35.9160355,
				lng: 145.6491806
			};
			const lat_lng3 = {
				lat: -36.0993453,
				lng: 145.4435593
			};
			if (this.nathalia) {
				marker1 = new google.maps.Marker({
					position: lat_lng1,
					icon: image,
					map: googleMap.map1
				});
			}
			if (this.cobram) {
				marker2 = new google.maps.Marker({
					position: lat_lng2,
					icon: image,
					map: googleMap.map2
				});
			}
			if (this.numurkah) {
				marker3 = new google.maps.Marker({
					position: lat_lng3,
					icon: image,
					map: googleMap.map3
				});
			}
			return
		}
	});
	return $(function() {
		return googleMap.onready();
	});
})(jQuery);
