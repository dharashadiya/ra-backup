export function isMobile() {
	var result;
	result = false;
	if (
		/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
			navigator.userAgent
		)
	) {
		result = true;
	}
	return result;
}
// Open External Link in new tab
export function openLinkNewTab() {
	var base;
	base = window.location.hostname;
	return $("a").each(() => {
		let url = $(this).attr("href");
		if (
			url.indexOf("http") !== -1 &&
			(url.indexOf("javascript:void(0)") < 0 && url.indexOf(base) < 0)
		) {
			$(this).attr("target", "_blank");
			return $(this).attr("rel", "noopener");
		}
	});
}

export function cloudinaryImageCDN() {
	const localhost = site.siteurl.includes(".test") ? true : false;
	imagesLoaded("body", {
		background: true
	}, () => {
		Array.from($("[data-img], [data-bg], [data-src]")).forEach(image => {
			if (localhost) {
				if (image.classList.contains("lazy")) {
					return
				} else if (image.tagName === "IMG") {
					image.src = image.dataset.src;
				} else {
					image.style.backgroundImage = `url(${image.dataset.src})`;
				}
			} else {
				const clientWidth = image.clientWidth;
				const pixelRatio = window.devicePixelRatio || 1.0;
				const imageParams = "w_" + 100 * Math.round((clientWidth * pixelRatio) / 100) + ",c_fill,g_auto,f_auto";
				const baseUrl = "https://res.cloudinary.com/rock-agency/image/fetch";
				let url;
				if (image.classList.contains("lazy")) {
					image.dataset.src = baseUrl + "/" + imageParams + "/" + image.dataset.src;
				} else if (image.tagName === "IMG") {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.src = url;
				} else {
					url = baseUrl + "/" + imageParams + "/" + image.dataset.src;
					image.style.backgroundImage = `url(${url})`;
				}
			}
		});
	});
}

export function superfishMenu() {
	if ($(window).width() > 1000) {
		$(".c-nav").superfish({
			animation: {
				height: "show"
			},
			delay: 250,
			speed: 'fast',
			onBeforeShow: function () {
				$('.c-top').addClass('subnav-open')
			},
			onBeforeHide: function () {
				$('.c-top').removeClass('subnav-open')
			}
		});
	} else {
		$('.c-nav').superfish('destroy');
	}
}
export function slider() {
	return $(".c-slider").cycle({
		log: false,
		// swipe: true
		slides: ".c-slide",
		"auto-height": "calc",
		timeout: 5000,
		speed: 600
	});
}

export function searchFunc() {
	$('body').on('click', '.c-banner__right_icon', function () {
		$('.c-search__input').focus();
		$('.c-search__container').addClass('is-active');
		$('.c-search').addClass('is-active');
		return $('html').addClass('overlay-open');
	});
	return $('body').on('click', '.c-overlay__close', function () {
		$('.c-search__container').removeClass('is-active');
		$('.c-search').removeClass('is-active');
		return $('html').removeClass('overlay-open');
	});
}

// Fade in animation
export function fadeInUp() {
	return setTimeout(() => {
		return $(".o-animateup").viewportChecker({
			classToAdd: "animated",
			offset: 60
		});
	}, 500);
}

// LazyLoad
export function lazyLoad() {
	return $('.lazyload').Lazy({
		threshold: 400,
		effect: 'fadeIn',
	})
}

// FaqSwitcher
export function faqSwitcher() {
	$('body').on('click', '.c-faq__header_link', function () {
		var dataId = $(this).attr("data-faq");
		$('.c-faq__table').removeClass('active');
		$('[data-faqtable="' + dataId + '"]').addClass('active');
	});
}

// LocationSelector
export function locationSelector() {
	$('body').on('click', '.c-categorylist__location_links a', function () {
		var location = $(this).attr("data-location");
		$('.c-categorylist__location_links a').removeClass('active');
		$('[data-location="' + location + '"]').addClass('active');
		$('.c-categorylist__button').removeClass('active');
		$('[data-location-target*="' + location + '"]').addClass('active');
		if (location == 'all') {
			$('.c-categorylist__button').addClass('active');
		}
	});
}

export function slicker() {
	$('.c-slider').slick({
		infinite: true,
		slidesToShow: 3,
		slide: '.c-slide',
	});
}

export function injectContentIntoMenu() {
	const removeUnderline = (list) => {
		const length = list.length
		console.log(length)
		console.log(length % 2)
		if (length % 2) {
			$(list[length - 1]).css("border-bottom", "none");
		} else {
			$(list[length - 1]).css("border-bottom", "none");
			$(list[length - 2]).css("border-bottom", "none");
		}
	}
	removeUnderline($('#menu-item-216 ul').children())
	removeUnderline($('#menu-item-222 ul').children())
	removeUnderline($('#menu-item-239 ul').children())
	$('#menu-item-216 > ul ').append(`
		<a href='/services' class='c-nav__services'>
			<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M1.53069e-06 9L12.17 9L6.58 14.59L8 16L16 8L8 4.39685e-07L6.59 1.41L12.17 7L1.1506e-06 7L1.53069e-06 9Z" fill="white" />
			</svg >
			View A-Z Services
		</a>`
	)
}

export function imageChangeMob() {
	if ($('.js-changeimage-mob').length > 0 && $(window).width() < 770) {
		$('.js-changeimage-mob').each(function (index, elem) {
			let bgI = $(this).attr('data-bg-mobile')
			if (bgI) {
				$(this).css('background-image', `url(${bgI})`)
			}
		})
	}
}

export function customMatchHeight() {
	jQuery( document ).ready( function() {
		$('.c-events__news_bubble_content .contentBlock').matchHeight({
			property: 'min-height'
		});
		$('.vacancyLists .listHolder .shortInfo').matchHeight({
			property: 'min-height'
		});

		$('.vacancyLists .listHolder .title').matchHeight({
			property: 'min-height'
		});

		$('.c-home__news_bubble_content').matchHeight({
			property: 'min-height'
		});



	})
	$( document ).ajaxComplete(function() {
		$('.c-events__news_bubble_content .contentBlock').matchHeight({
			property: 'min-height'
		});
		$('.vacancyLists .listHolder .shortInfo').matchHeight({
			property: 'min-height'
		});
		$('.vacancyLists .listHolder .title').matchHeight({
			property: 'min-height'
		});
		$('.c-home__news_bubble_content').matchHeight({
			property: 'min-height'
		});
	});

	jQuery( document ).ready( function() {
		var len = $('.vacancySearchHolder div.vacancyLists .listHolder').length;
		if(len<1){
			$('.vacancySearchHolder').hide();
		}

		var pageSearchLenght = $('.pageSearchHolder div.pageSearchLists .pageSearchBox').length;
		if(pageSearchLenght<1){
			$('.pageSearchHolder').hide();
		}

		var newsSearchlength = $('.newsSearchHolder div.o-layout .o-module__item').length;
		if(newsSearchlength<1){
			$('.newsSearchHolder').hide();
		}

		var eventSearchlength = $('.eventsSearchHolder div.o-layout .o-module__item').length;
		if(eventSearchlength<1){
			$('.eventsSearchHolder').hide();
		}

		var serviceSearchlength = $('.serviceSearchHolder div.titleLists .titleName').length;
		if(serviceSearchlength<1){
			$('.serviceSearchHolder').hide();
		}
	})

	$(document).ready(function() {
		// Configure/customize these variables.
		// also need to add .morecontent span display none on css
		var showChar = 180; // How many characters are shown by default
		var ellipsestext = "...";
		var moretext = " Show more (+)";
		var lesstext = " Show less (-)";

		$('.more').each(function() {
			var content = $(this).html();
			if (content.length > showChar) {
				var c = content.substr(0, showChar);
				var h = content.substr(showChar, content.length - showChar);
				var html = c + '<span class="moreellipses">' + ellipsestext + '</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';
				$(this).html(html);
			}
		});

		$(".morelink").click(function() {
			if ($(this).hasClass("less")) {
				$(this).removeClass("less");
				$(this).html(moretext);
			} else {
				$(this).addClass("less");
				$(this).html(lesstext);
			}
			$(this).parent().prev().fadeToggle();
			$(this).prev().fadeToggle();
			return false;
		});

		$('ul.sub-menu').css('visibility', 'visible');
	});

	$(document).ready(function() {
		$('.accordions .answer:first').show();
		$('.accordions .question:first').addClass('active');
		$('.accordions .question').on('click', function(e) {
			e.preventDefault();
			// var ansBlock = $(this).parent().children('.answer');
			// var questionBlock = $(this).parent().children('.question');
			// $('.answer').not(ansBlock).slideUp();
			// $('.question').not(questionBlock).removeClass('active');
			// $(this).toggleClass('active');
			// ansBlock.slideToggle();
			$(this).toggleClass('active');
			$(this).next().slideToggle();
		})
	});


	//cookies for top header notification
	$(document).ready(function() {
        // If the 'hide cookie is not set we show the message
        if (!readCookie('hide-notification-cookie')) {
            //alert('no cookie');
            $('.c-notification').css('display', 'block');
        }
        // Add the event that closes the popup and sets the cookie that tells us to
        // not show it again until one day has passed.
        $('#close').on('click', function() {
            $('.c-notification').hide();
            createCookie('hide-notification-cookie', true, 1)
            return false;
        })
    });

    function createCookie(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        } else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
	}


	


	$("div.viewBtn a.downArrow").on('click',function() {
		$('html, body').animate({
			scrollTop: $(".c-home__services").offset().top
		}, 600);
	});

	$('newsCat li a').on('click', function(e){
		e.preventDefault();
	})

	// sorting list items using sort() based on data-title value
	var $list = $('#mylist'),
    $li = $list.children('li');
	$li.sort(function(a, b) {
		// getting data-title attribute from dom object using getAttribute()
		var a1 = a.getAttribute('data-title'),
			b1 = b.getAttribute('data-title');
		//comparing values for sorting
		return (a1 < b1) ? -1 : (a1 > b1) ? 1 : 0;
	});
	// updating sorted order sorted order
	$li.appendTo($list);

	// $("ul.c-nav-mobile li.menu-item-has-children ul.sub-menu").css({
	// 	paddingTop: $("div.c-banner").innerHeight()
	// });

	//*mobile navigation */
	$('#nav-icon3').on('click', function () {
		$(this).toggleClass('open');
	});
	$('#nav-icon3').on('click', function () {
		$('body').toggleClass('overflowHidden');
		$('.headerWrapper').toggleClass('showMenu');
		$('.c-site-mobile').fadeToggle(100);

		// $("div.mobileMenu div.navHolder").css({
		// 	paddingBottom: $("div.socialLinksMobile").innerHeight()
		// });
	});

	$('.c-nav-mobile>li>a').on('click', function(e) {
		if ($(this).parent().hasClass('menu-item-has-children')) {
			e.preventDefault();
		}
		$(this).parent().toggleClass('active');
		$(this).parent().find('ul.sub-menu').fadeIn();
	})


	$("div.mobileMenu div.navHolder").css({
		paddingTop: $("div.c-banner").innerHeight()
	});

	// $("div.mobileMenu ul.sub-menu").css({
	// 	marginTop: $("div.c-banner").innerHeight() - 25
	// });
	


	$('ul.c-nav-mobile li.menu-item-has-children a').on('click', function(){
		$('.navHolder').addClass('showingChildMenu');
		$('.navHolder').css('left', '-100%');
		$('.mobileNavWrapper').css('width', '100%')
		
		//$('ul.c-nav-mobile>li').hide();
	})

	$('#nav-icon3').on('click', function(){
		$('ul.sub-menu').fadeOut('fast');
		$('.navHolder').removeClass('showingChildMenu');
		$('.navHolder').css('left', '0');
		$('.mobileNavWrapper').css('width', '50%');
		$('.c-nav-mobile>li').removeClass('active');
	})

	$('ul.sub-menu').before().on('click',function(){
		$('ul.sub-menu').fadeOut('fast');
		
		$('.c-nav-mobile>li').removeClass('active');
		setTimeout(function(){
			$('.mobileNavWrapper').css('width', '50%');
			$('.navHolder').css('left', '0');
			$('.navHolder').removeClass('showingChildMenu');
		},300); 
		
	})

	

	
	$(".scrollTop a, a.scrollTopBtn").on('click',function(e) {
		e.preventDefault();
		$("html").animate({ scrollTop: 0 }, "slow");
	});

	$(window).scroll(function () {
		if ($(this).scrollTop() > 500) {
			$('a.scrollTopBtn').css('opacity', 1).removeClass('hide');
		} else {
			$('a.scrollTopBtn').css('opacity', 0).addClass('hide');
		}
	})


	//HIDING SCROLL TOP WHEN REACHED TO FOOTER
	function isOnScreen(elem) {
		// if the element doesn't exist, abort
		if( elem.length == 0 ) {
			return;
		}
		var $window = jQuery(window)
		var viewport_top = $window.scrollTop()
		var viewport_height = $window.height()
		var viewport_bottom = viewport_top + viewport_height
		var $elem = jQuery(elem)
		var top = $elem.offset().top
		var height = $elem.height()
		var bottom = top + height
	
		return (top >= viewport_top && top < viewport_bottom) ||
		(bottom > viewport_top && bottom <= viewport_bottom) ||
		(height > viewport_height && top <= viewport_top && bottom >= viewport_bottom)
	}
	
	jQuery( document ).ready( function() {
		window.addEventListener('scroll', function(e) {
			if( isOnScreen( jQuery( '.c-bottom__extra' ) ) ) { /* Pass element id/class you want to check */
				$('a.scrollTopBtn').hide();
			 }	else {
				$('a.scrollTopBtn').show();
			 }
		});
	});
	//END HIDING SCROLL TOP WHEN REACHED TO FOOTER



}