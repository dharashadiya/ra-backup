jQuery(document).ready(function($) {
    $('#service_category-all ul.categorychecklist li').each(function() {
        if ($(this).find('ul').length) {
            $(this).addClass('is-parent');
            $(this).prepend('<span class="toggler"></span>')
        }
    });
    $('#service_category-all ul.categorychecklist li .toggler').click(function() {
        $(this).parent().toggleClass('open');
        $(this).parent().children('ul.children').slideToggle('slow');
    });
});
