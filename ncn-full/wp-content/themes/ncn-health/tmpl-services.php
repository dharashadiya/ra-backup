<?php
/*
 * Template Name: Services
 */
?>

<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post() ;
    include('content-sections/banner.php');
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php include('content-sections/content-header.php'); ?>
    <div class="serviceWrapper">
        <div class="o-wrapper">
            <div class="breadcrumb">
                <?php if(function_exists('bcn_display')) {
                        bcn_display();
                    }
                ?>
            </div>

            <div class="introService">
                <h2><?php the_field( 'quick_intro_title' ); ?></h2>
                <?php the_field( 'quick_intro_content' ); ?>
            </div>


        </div>
    </div>

    <div class="servicesList mt-3">
        <div class="o-wrapper">
            <div class="serviceFilter">
                <div class="filterBlock searchBlock">
                    <label>Search Services</label>
                    <?php echo do_shortcode('[facetwp facet="search_services"]'); ?>
                </div>
                <div class="filterBlock searchByCat">
                    <label>Select Category</label>
                    <?php echo do_shortcode('[facetwp facet="service_category"]'); ?>
                </div>
                <div class="filterBlock searchByLocation">
                    <label>Filter by location</label>
                    <?php echo do_shortcode('[facetwp facet="service_location"]'); ?>
                </div>
            </div>

            <div class="facetSelections">
                <?php echo facetwp_display( 'selections' ); ?>
            </div>

            <div class="titleLists">
                <?php $args = array( 'post_type' => 'ra-service', 'posts_per_page' => 40, 'facetwp' => true, 'order' => 'ASC', 'orderby' => 'title' );
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                ?>
                <div class="titleName"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>

            <div class="text-center loadMore pt-2"><?php echo do_shortcode('[facetwp facet="load_more"]'); ?></div>


        </div>
    </div>


</article>



<?php endwhile; ?>
<?php get_footer(); ?>

<?php $is_search = $_GET['_search_services']; 
if(!empty($is_search)) { ?>
<script>
jQuery('html, body').animate({
    scrollTop: jQuery(".servicesList").offset().top
}, 800);
</script>
<?php }
?>