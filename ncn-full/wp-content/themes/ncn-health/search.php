<?php get_header(); ?>

<div class="c-search">
    <main id="Main" class="c-main-content o-main" role="main">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="c-cms-content c-search-content o-wrapper">
                <!-- <h1><?php //printf( __( 'Search results for: "%s"'), get_search_query() ); ?></h1> -->
                <h2 class="pt-3 pb-2">
                    <?php $allsearch = new WP_Query("s=$s&showposts=0");
				echo $allsearch ->found_posts.' results for '; ?>
                    <?php printf( __( '"%s"'), get_search_query() ); ?>
                </h2>
                <?php global $post; ?>
                <?php //query_posts('showposts=#'); ?>
                <?php query_posts($query_string . '&showposts=1000'); ?>

                <?php if ( have_posts() ) : ?>
                <div class="pageSearchHolder searchHolder">
                    <h4>Page Results</h4>
                    <div class="pageSearchLists">
                        <?php while ( have_posts() ) : the_post(); ?>
                        <?php if ( $post->post_type == 'page' ) : ?>
                        <div class="pageSearchBox">
                            <h5><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h5>
                            <div class="permalink"><a
                                    href="<?php echo get_permalink(); ?>"><?php echo get_permalink(); ?></a>
                            </div>
                        </div>
                        <?php endif; endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>

                <?php if ( have_posts() ) : ?>
                <div class="vacancySearchHolder pt-3 searchHolder">
                    <h4 class="pb-2">Job Results</h4>
                    <div class="vacancyLists">
                        <?php while ( have_posts() ) : the_post(); ?>
                        <?php if ( $post->post_type == 'ra-vacancy' ) : ?>
                        <div class="listHolder">
                            <div class="listContent">
                                <div class="title pt-1"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </div>
                                <?php $location_values = get_field( 'location' ); ?>
                                <?php if ( $location_values ) : ?>
                                <div class="location">
                                    <?php foreach ( $location_values as $location_value ) : ?>
                                    <span><?php echo esc_html( $location_value ); ?></span>
                                    <?php endforeach; ?>
                                </div>
                                <?php endif; ?>
                                <div class="thumbnail">
                                    <a href="<?php the_permalink(); ?>"
                                        style="background: url(<?php the_field( 'thumbnail_image' ); ?>) no-repeat top center/cover"></a>
                                </div>
                                <?php $shortInfo = get_field('role_short_description');
                                        if($shortInfo) {
                                    ?>
                                <div class="shortInfo mb-2">
                                    <?php echo strip_tags( wp_trim_words($shortInfo, 25) ) . "..."; ?>
                                </div>
                                <?php } ?>
                                <div class="readMore text-center mb-3">
                                    <a class="o-btn" href="<?php the_permalink(); ?>">Position Description</a>
                                </div>
                            </div>
                        </div>
                        <?php endif; endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>


                <?php if ( have_posts() ) : ?>
                <div class="newsSearchHolder pt-3 searchHolder">
                    <h4>News Results</h4>
                    <div class="o-layout o-layout--medium o-module">
                        <?php while ( have_posts() ) : the_post(); ?>
                        <?php if ( $post->post_type == 'ra-news' ) : ?>
                        <pre><?php print_r($post); ?></pre>
                        <div class="o-layout__item u-2/6@tablet u-3/6@mobileLandscape o-module__item">
                            <div class="c-home__news_bubble">
                                <div class="c-home__news_bubble_wrapper">
                                    <div class="c-home__news_bubble_header">
                                        <ul class="newsCat">
                                            <?php foreach ( get_the_terms( get_the_ID(), 'news_category' ) as $tax ) { ?>
                                            <li><a href="javascript:void(0);"><?php echo $tax->name; ?></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                        <span><?php echo(get_field('expected_time')); ?> min
                                            read&nbsp;&nbsp;•&nbsp;&nbsp;<?php $post_date = get_the_date( 'M Y' ); echo $post_date;?></span>
                                    </div>
                                    <div class="c-home__news_bubble_content">
                                        <h4><a href="<?php echo(get_permalink());?>"><?php echo(get_the_title());?></a>
                                        </h4>
                                        <p><?php echo(get_field('news_short_description')); ?></p>
                                        <a class="readmore" href="<?php echo(get_permalink());?>">Read more</a>
                                    </div>
                                </div>
                                <?php
                                $image = get_field('thumbnail_image', get_the_ID());
                                if ($image) {
                                    $image = get_field('thumbnail_image');
                                } else {
                                    $image = "/wp-content/themes/ncn-health/assets/img/example-blog-image.jpg";
                                }
                            ?>
                                <div class="newsImage">
                                    <a href="<?php echo(get_permalink());?>" class="imgHolder"
                                        style="background: url(<?php echo $image; ?>) no-repeat top center/cover"></a>
                                </div>
                            </div>
                        </div>
                        <?php endif; endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>



                <?php if ( have_posts() ) : ?>
                <div class="eventsSearchHolder pt-3 searchHolder">
                    <h4>Events Results</h4>
                    <div class="o-layout o-layout--medium o-module">
                        <?php while ( have_posts() ) : the_post(); ?>
                        <?php if ( $post->post_type == 'ra-event' ) : ?>
                        <pre><?php print_r($post); ?></pre>

                        <div class="o-layout__item u-1/3@tablet o-module__item">
                            <div class="c-events__news_bubble">
                                <div class="c-events__news_bubble_wrapper">
                                    <?php
                                        $image = get_field('thumbnail_image', get_the_ID());
                                        if ($image) {
                                            $image = get_field('thumbnail_image');
                                        } else {
                                            $image = "/wp-content/themes/ncn-health/assets/img/example-blog-image.jpg";
                                        }
                                    ?>
                                    <!-- <img src="<?php //echo $image; ?>" alt=""> -->
                                    <div class="eventImage"
                                        style="background: url(<?php echo $image; ?>) no-repeat top center/cover"></div>
                                    <div class="c-events__news_bubble_content">
                                        <div class="contentBlock">
                                            <h4><a
                                                    href="<?php echo get_permalink(); ?>"><?php echo(get_the_title());?></a>
                                            </h4>
                                            <!-- <p><?php //echo(get_field('event_short_description')); ?></p> -->
                                            <?php $shortInfo = get_field('event_short_description'); ?>
                                            <div class="shortInfo">
                                                <?php echo strip_tags( substr($shortInfo, 0, 120) ) . "..."; ?></div>
                                        </div>
                                        <div class="pt-1 readMore"><a href="<?php echo(get_permalink());?>">More
                                                info</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php endif; endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>



                <?php if ( have_posts() ) : ?>
                <div class="serviceSearchHolder pt-3 servicesList searchHolder">
                    <h4>Service Results</h4>
                    <div class="titleLists pt-2">
                        <?php while ( have_posts() ) : the_post(); ?>
                        <?php if ( $post->post_type == 'ra-service' ) : ?>
                        <div class="titleName"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                        <?php endif; endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>





            </div>
        </article>
    </main>
    <!-- <?php get_sidebar(); ?> -->
</div>
<?php get_footer(); ?>