<?php

define( 'GOOGLE_ANALYTICS', '');
include('functions/boilerplate.php');
include('functions/admin-config.php');
include('functions/theme-helpers.php');
include('functions/clean-gallery.php');
include('functions/acf-config.php');
include('functions/shortcodes.php');
// include('functions/woocommerce-helpers.php');
// include('functions/woocommerce-customisation.php');
include('functions/breadcrumbs.php');
include('functions/dashboard.php');
include('functions/side-nav.php');

// Author meta tag
define( 'AUTHOR', 'NCN Health');

// Enable post thumbnails
add_theme_support( 'post-thumbnails' );

// Enable nav menus
add_theme_support( 'nav-menus' );

add_action('admin_init', 'remove_textarea');

function remove_textarea() {
	remove_post_type_support( 'page', 'editor' );
}

// Custom image sizes
add_image_size( 'preview', 300, 200, false );

// make custom image size choosable from add media
add_filter( 'image_size_names_choose', 'site_custom_sizes' );
function site_custom_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'preview' => 'Preview',
	) );
}

// For development
//show_admin_bar(!is_localhost() && is_user_logged_in());

// Register nav menus
function register_site_menus() {
	register_nav_menu( 'primary-menu', 'Primary Menu' );
}
add_action('after_setup_theme', 'register_site_menus');

// Register mobile menus
function register_mobile_menus() {
	register_nav_menu( 'mobile-menu', 'Mobile Menu' );
}
add_action('after_setup_theme', 'register_mobile_menus');

// Register widget areas
function site_widgets_init() {
	// Register main sidebar
	register_sidebar( array(
		'name' => 'Sidebar Widget Area',
		'id' => 'aside-sidebar',
		'description' => 'Widget in this area will be shown in sidebar area on all pages',
		'before_widget' => '<section id="%1$s" class="c-widget c-widget--side %2$s %1$s"><div class="c-widget__content">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="c-widget__heading">',
		'after_title' => '</h3>'
	));

	// Register footer sidebar
	register_sidebar( array(
		'name' => 'Footer Widget Area',
		'id' => 'footer-sidebar',
		'description' => 'Widget in this area will be shown in footer area on all pages',
		'before_widget' => '<section id="%1$s" class="c-widget c-widget--footer %2$s %1$s"><div class="c-widget__content">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="c-widget__heading">',
		'after_title' => '</h3>'
	));
}
add_action( 'widgets_init', 'site_widgets_init' );

// Remove contact from styles from all pages
add_filter('wpcf7_load_js', '__return_false');
add_filter('wpcf7_load_css', '__return_false');

// Load scripts using wp's enqueue
function site_scripts() {
	if ( !is_admin() ) {
		// Remove jquery that wordpress includes
		wp_deregister_script('jquery');
		$site_js = (is_localhost()) ? JS . '/dist/site.js' : JS . '/dist/site.min.js';
		//$site_css = (is_localhost()) ? CSS . '/screen.dev.css' : CSS . '/screen.min.css';

		// Include all js including jQuery and register with name 'jquery' so other jquery dependable scripts load as well
		wp_enqueue_script('jquery',$site_js, false, $GLOBALS['site_cachebust'], true);
		//wp_enqueue_style('site', $site_css, false);
		if(is_localhost()){
			wp_enqueue_style( 'site', CSS . '/screen.dev.css?time=' . filemtime(get_stylesheet_directory() . "/assets/css/screen.dev.css") );
		} else {
			wp_enqueue_style( 'site', CSS . '/screen.min.css?time=' . filemtime(get_stylesheet_directory() . "/assets/css/screen.min.css") );
		}

	}
}
add_action('wp_enqueue_scripts', 'site_scripts');

// Boolean to check if the cf7 scripts are required
function load_cf7_scripts() {
	return ( is_page('contact') );
}

// Check if staging or test enviroment
function is_local_or_staging() {
	if ($_SERVER['SERVER_ADDR'] == '103.27.32.31' || $_SERVER['SERVER_ADDR'] == '127.0.0.1') {
		return true;
	} else {
		return false;
	}
}

// For Debugging
function ra_console_log( $data ){
  echo '<script>';
  echo 'console.log('. json_encode( $data ) .')';
  echo '</script>';
}

// Load cf7 scripts if required
function site_load_cf7_scripts() {
	if ( load_cf7_scripts() ) {
		if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
			wpcf7_enqueue_scripts();
		}
		// Not loading wpcf7 styles at all, if you need wpcf7 styles uncomment below lines
		// if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
		//   wpcf7_enqueue_styles();
		// }
	}
}
add_action( 'wp', 'site_load_cf7_scripts' );

//Allow search to search through pages and posts. Add any custom post type in array to search by them as well.
function site_search($query) {
	if (!is_admin() && $query->is_search) {
		$query->set('post_type', array('page','post','ra-event', 'ra-news', 'ra-vacancy', 'ra-service'));
	}
	return $query;
}
add_filter('pre_get_posts', 'site_search');

// REMOVE GUTTENBURG
// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

//enqueue admin style
function enqueue_admin_style() {
	if(is_admin()) {
		wp_enqueue_style( 'custom-admin-style', get_stylesheet_directory_uri() . '/assets/adminCss/custom-admin-style.css?time=' . filemtime(get_stylesheet_directory() . "/assets/adminCss/custom-admin-style.css") );
	}
}
add_action('init', 'enqueue_admin_style');


//enqueue admin script
function add_script_to_menu_page() {
		wp_register_script( 'some-js', get_stylesheet_directory_uri() . '/assets/adminJs/custom.js?time=' . filemtime(get_stylesheet_directory() . "/assets/adminJs/custom.js") );
    wp_enqueue_script( 'some-js' );
}
add_action( 'admin_enqueue_scripts', 'add_script_to_menu_page' );


// automatically select parent when sub cats are selected
add_action('save_post', 'assign_parent_terms', 10, 2);
function assign_parent_terms($post_id, $post){
    $arrayPostTypeAllowed = array('ra-service');
    //$arrayTermsAllowed = array('product_cat', 'brand');
    $arrayTermsAllowed = array('service_category');
    //Check post type
    if(!in_array($post->post_type, $arrayPostTypeAllowed)){
        return $post_id;
    }else{
        // get all assigned terms
        foreach($arrayTermsAllowed AS $t_name){
            $terms = wp_get_post_terms($post_id, $t_name );
            foreach($terms as $term){
                while($term->parent != 0 && !has_term( $term->parent, $t_name, $post )){
                    // move upward until we get to 0 level terms
                    wp_set_post_terms($post_id, array($term->parent), $t_name, true);
                    $term = get_term($term->parent, $t_name);
                }
            }
        }
    }
}

function stop_reordering_my_categories($args) {
    $args['checked_ontop'] = false;
    return $args;
}

// Let's initiate it by hooking into the Terms Checklist arguments with our function above
add_filter('wp_terms_checklist_args','stop_reordering_my_categories');

//hide count on dropdown filter on facets
add_filter( 'facetwp_facet_dropdown_show_counts', '__return_false' );

//adding alt tag
function rock_add_img_alt_tag_title($attr, $attachment = null) {
    $img_title = trim(strip_tags($attachment->post_title));
    if (empty($attr['alt'])) {
        $attr['alt'] = $img_title;
        $attr['title'] = $img_title;
    }
    return $attr;
}
add_filter('wp_get_attachment_image_attributes', 'rock_add_img_alt_tag_title', 10, 2);

function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

?>