<?php
    // function get_sidenav() {
    //     global $post;
    //     if (is_page()) {
    //         if ($post->post_parent) {
    //             echo '<h6>'.get_the_title($post->post_parent).' Pages</h6>';
    //             echo '<ul class="c-sidenav">';
    //             echo '<li><a href='.get_permalink($post->post_parent).'>'.get_the_title($post->post_parent).'</a></li>';
    //             wp_list_pages(array(
    //                 'child_of' => $post->post_parent,
    //                 'depth' => 2,
    //                 'title_li' => ""
    //             ));
    //             echo '</ul>';
    //         } else {
    //             echo '<h6>'.get_the_title($post->ID).' Pages</h6>';
    //             echo '<ul class="c-sidenav">';
    //             echo '<li class="current_page_item"><a href='.get_permalink($post->ID).'>'.get_the_title($post->ID).'</a></li>';
    //             wp_list_pages(array(
    //                 'child_of' => $post->ID,
    //                 'depth' => 2,
    //                 'title_li' => ""
    //             ));
    //             echo '</ul>';
    //         }
    //     }
    // }

function get_sidenav() { 
    global $post;
    if ( is_page() || is_single() ) {
        echo '<div class="parentTitle">' . $post->post_title . "</div>";
    ?>
<ul class="sideNavLists">
    <?php 
            wp_list_pages(array(
            'child_of' => $post->ID,
            'title_li' => ''
            )); 
        ?>
</ul>
<?php
        } // if ends for is_page 
if(is_tax('service_category')){
    $queriedObj = get_queried_object();
    //print_r($queriedObj);
    $termID = $queriedObj->term_id;
    $taxonomyName = "service_category";
    $termchildren = get_term_children( $termID, $taxonomyName );
    //print_r ($termchildren);


    echo '<div class="parentTitle">' . $queriedObj->name . '</div>';
    if (!empty($termchildren)) {
        echo '<ul class="sideNavLists termChild" id="mylist">';
        foreach ($termchildren as $child) {
        $term = get_term_by( 'id', $child, $taxonomyName );
        echo '<li data-title="' . $term->name . '"><a href="' . get_term_link( $term->slug, $taxonomyName ) . '">' . $term->name . '</a></li>';
        }
        echo '</ul>';
    } else {
        
        //print_r($termID);
        $args = array(
        'post_type' => 'ra-service',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'title',
        'tax_query' => array(
            array(
            'taxonomy' => 'service_category',
            'field' => 'term_id',
            'terms' => $termID,
            'operator' => 'IN',
            )
        )
        );

        $query = new WP_Query( $args );
        echo '<ul class="sideNavLists">';
        while ( $query->have_posts() ) {
        $query->the_post();
            echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
        }
        echo '</ul>';
        wp_reset_postdata();
    }
  
}
}

?>