<?php
    function get_breadcrumb() {
        global $post;
        if (is_page() || is_single() || is_tax()) {
            echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
            if ( $post->post_parent ) {
                $page_acnestors = array_reverse(get_post_ancestors($post));
                foreach ($page_acnestors as $ancestor) {
                    echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
                    echo '<a href="'.get_permalink( $ancestor ).'" rel="nofollow">'.get_the_title( $ancestor ).'</a>';
                }
            }
            echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
            echo the_title();
        }
    }
?>