<?php
// Register Custom Post Type
add_action( 'init', 'ra_event_post_type', 0 );
function ra_event_post_type() {
	$args = array(
    'label'                 => __( 'Event', 'ra-event' ),
    'description'           => __( 'Events', 'ra-event' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => 5,
    'menu_icon'          => 'dashicons-edit-page',
    'rewrite'                    => array(
          'with_front'               => false,
          'slug'                     => 'event'
    ),
    'supports'           => array( 'title', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes' ),
  );
	register_post_type( 'ra-event', $args );
}
// Register Custom Taxonomy
function taxonomy_event_category() {
  $labels = array(
    'name'                       => _x( 'Event Category', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Event Category', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Event Category', 'text_domain' ),
    'all_items'                  => __( 'All Event Categories', 'text_domain' ),
    'parent_item'                => __( 'Parent Event Category', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Event Category:', 'text_domain' ),
    'new_item_name'              => __( 'New Event Category', 'text_domain' ),
    'add_new_item'               => __( 'Add New Event Category', 'text_domain' ),
    'edit_item'                  => __( 'Edit Event Category', 'text_domain' ),
    'update_item'                => __( 'Update Event Category', 'text_domain' ),
    'view_item'                  => __( 'View Event Category', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate Event Category with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove Event Category', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Event Category', 'text_domain' ),
    'search_items'               => __( 'Search Event Category', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'items_list'                 => __( 'Event Category list', 'text_domain' ),
    'items_list_navigation'      => __( 'Event Category list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => !false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite'                    => array(
      'with_front'               => false,
      'slug'                     => 'event-category'
    )
  );
  register_taxonomy( 'Event_category', array( 'ra-event' ), $args );
}
add_action( 'init', 'taxonomy_event_category', 0 );
?>