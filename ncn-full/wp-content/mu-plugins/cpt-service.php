<?php
// Register Custom Post Type
add_action( 'init', 'ra_service_post_type', 0 );
function ra_service_post_type() {
	$args = array(
        'label'              => __( 'Service', 'ra-service' ),
        'description'        => __( 'Services', 'ra-service' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'menu_icon'          => 'dashicons-admin-tools',
        'rewrite'                    => array(
          'with_front'               => false,
          'slug'                     => 'service'
        ),
        'supports'           => array( 'title', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes' ),
    );
	register_post_type( 'ra-service', $args );
}
// Register Custom Taxonomy
function taxonomy_service_category() {
  $labels = array(
    'name'                       => _x( 'Service Category', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Service Category', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Service Category', 'text_domain' ),
    'all_items'                  => __( 'All Service Categories', 'text_domain' ),
    'parent_item'                => __( 'Parent Service Category', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Service Category:', 'text_domain' ),
    'new_item_name'              => __( 'New Service Category', 'text_domain' ),
    'add_new_item'               => __( 'Add New Service Category', 'text_domain' ),
    'edit_item'                  => __( 'Edit Service Category', 'text_domain' ),
    'update_item'                => __( 'Update Service Category', 'text_domain' ),
    'view_item'                  => __( 'View Service Category', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate Service Category with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove Service Category', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Service Category', 'text_domain' ),
    'search_items'               => __( 'Search Service Category', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'items_list'                 => __( 'Service Category list', 'text_domain' ),
    'items_list_navigation'      => __( 'Service Category list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => !false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite'                    => array(
      'with_front'               => false,
      'slug'                     => 'service-category'
    )
  );
  register_taxonomy( 'service_category', array( 'ra-service' ), $args );
}
add_action( 'init', 'taxonomy_service_category', 0 );
?>