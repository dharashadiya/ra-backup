<?php
// Register Custom Post Type
add_action( 'init', 'ra_news_post_type', 0 );
function ra_news_post_type() {
	$args = array(
    'label'                 => __( 'News', 'ra-news' ),
    'description'           => __( 'News posts', 'ra-news' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => 5,
    'menu_icon'          => 'dashicons-testimonial',
    'rewrite'                    => array(
          'with_front'               => false,
          'slug'                     => 'news'
    ),
    'supports'           => array( 'title', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes' ),
  );
	register_post_type( 'ra-news', $args );
}
// Register Custom Taxonomy
function taxonomy_news_category() {
  $labels = array(
    'name'                       => _x( 'News Category', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'News Category', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'News Category', 'text_domain' ),
    'all_items'                  => __( 'All News Categories', 'text_domain' ),
    'parent_item'                => __( 'Parent News Category', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent News Category:', 'text_domain' ),
    'new_item_name'              => __( 'New News Category', 'text_domain' ),
    'add_new_item'               => __( 'Add New News Category', 'text_domain' ),
    'edit_item'                  => __( 'Edit News Category', 'text_domain' ),
    'update_item'                => __( 'Update News Category', 'text_domain' ),
    'view_item'                  => __( 'View News Category', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate News Category with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove News Category', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular News Category', 'text_domain' ),
    'search_items'               => __( 'Search News Category', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'items_list'                 => __( 'News Category list', 'text_domain' ),
    'items_list_navigation'      => __( 'News Category list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => !false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite'                    => array(
      'with_front'               => false,
      'slug'                     => 'news-category'
    )
  );
  register_taxonomy( 'news_category', array( 'ra-news' ), $args );
}
add_action( 'init', 'taxonomy_news_category', 0 );
?>