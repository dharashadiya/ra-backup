<?php
// Register Custom Post Type
add_action( 'init', 'ra_vacancy_post_type', 0 );
function ra_vacancy_post_type() {
	$args = array(
    'label'                 => __( 'Job Vacancy', 'ra-vacancy' ),
    'description'           => __( 'Job Vacancies', 'ra-vacancy' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => 5,
    'menu_icon'          => 'dashicons-edit-page',
    'rewrite'                    => array(
          'with_front'               => false,
          'slug'                     => 'vacancy'
    ),
    'supports'           => array( 'title', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes' ),
  );
	register_post_type( 'ra-vacancy', $args );
}
// Register Custom Taxonomy
function taxonomy_vacancy_category() {
  $labels = array(
    'name'                       => _x( 'Vacancy Category', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Vacancy Category', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Vacancy Category', 'text_domain' ),
    'all_items'                  => __( 'All Vacancy Categories', 'text_domain' ),
    'parent_item'                => __( 'Parent Vacancy Category', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Vacancy Category:', 'text_domain' ),
    'new_item_name'              => __( 'New Vacancy Category', 'text_domain' ),
    'add_new_item'               => __( 'Add New Vacancy Category', 'text_domain' ),
    'edit_item'                  => __( 'Edit Vacancy Category', 'text_domain' ),
    'update_item'                => __( 'Update Vacancy Category', 'text_domain' ),
    'view_item'                  => __( 'View Vacancy Category', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate Vacancy Category with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove Vacancy Category', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Vacancy Category', 'text_domain' ),
    'search_items'               => __( 'Search Vacancy Category', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'items_list'                 => __( 'Vacancy Category list', 'text_domain' ),
    'items_list_navigation'      => __( 'Vacancy Category list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => !false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite'                    => array(
      'with_front'               => false,
      'slug'                     => 'vacancy-category'
    )
  );
  register_taxonomy( 'vacancy_category', array( 'ra-vacancy' ), $args );
}
add_action( 'init', 'taxonomy_vacancy_category', 0 );
?>