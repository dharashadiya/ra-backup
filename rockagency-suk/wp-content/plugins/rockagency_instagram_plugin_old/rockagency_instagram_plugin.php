<?php
/**
 * Plugin Name: Rockagency Instagram OLD
 * Plugin URI: https://rockagency.com.au
 * Description: Registers shortcode for instagram plugin
 * Version: 1.0.1
 * Author: Axel Debenham-Lendon
 *
 * @package Rock Agency Instagram Plugin
 */


function instagram_shortcode($atts) {
    return "<div class='c-instagram o-layout o-module' data-num-of-photos='". $atts['photos'] . "' data-instagram-handle='". $atts['handle']  ."'></div>";
}

function instagram_script() {
    wp_register_script('instagram.js', plugins_url('dist/instagram.js', __FILE__), array('jquery'),'1.0', true);
 
    wp_enqueue_script('instagram.js');
}
  
add_action( 'wp_enqueue_scripts', 'instagram_script' );
add_shortcode('instagram', 'instagram_shortcode');
