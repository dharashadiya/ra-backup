<?php
/**
 * Class to handle the store credit coupons.
 *
 * @package WC_Store_Credit/Classes
 * @since   3.0.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * WC_Store_Credit_Coupons class.
 */
class WC_Store_Credit_Coupons {

	/**
	 * Constructor.
	 *
	 * @since 3.0.0
	 */
	public function __construct() {
		add_filter( 'woocommerce_coupon_discount_types', array( $this, 'add_discount_type' ) );
		add_filter( 'woocommerce_product_coupon_types', array( $this, 'product_coupon_types' ) );
		add_filter( 'woocommerce_coupon_is_valid_for_cart', array( $this, 'is_valid_for_cart' ), 10, 2 );
		add_filter( 'woocommerce_coupon_is_valid', array( $this, 'is_valid' ), 10, 2 );
		add_filter( 'woocommerce_coupon_error', array( $this, 'error_message' ), 10, 3 );
	}

	/**
	 * Registers the 'store_credit' discount type.
	 *
	 * @since 3.0.0
	 *
	 * @param array $discount_types The coupon types.
	 * @return array
	 */
	public function add_discount_type( $discount_types ) {
		$discount_types['store_credit'] = _x( 'Store Credit', 'discount type label', 'woocommerce-store-credit' );

		return $discount_types;
	}

	/**
	 * Registers the 'store_credit' coupon as a product discount type.
	 *
	 * @since 3.0.0
	 *
	 * @param array $types The cart coupon types.
	 * @return array
	 */
	public function product_coupon_types( $types ) {
		$types[] = 'store_credit';

		return $types;
	}

	/**
	 * Checks if the coupon is valid for cart.
	 *
	 * @since 3.0.0
	 *
	 * @param bool      $valid  True if the coupon is valid. False otherwise.
	 * @param WC_Coupon $coupon The coupon object.
	 * @return bool
	 */
	public function is_valid_for_cart( $valid, $coupon ) {
		if ( wc_is_store_credit_coupon( $coupon ) ) {
			// Not valid for the cart if the coupon has product restrictions.
			$valid = ! (
				count( $coupon->get_product_ids() ) || count( $coupon->get_product_categories() ) ||
				count( $coupon->get_excluded_product_ids() ) || count( $coupon->get_excluded_product_categories() ) ||
				$coupon->get_exclude_sale_items()
			);
		}

		return $valid;
	}

	/**
	 * Validates a store credit coupon.
	 *
	 * @since 3.0.0
	 *
	 * @param bool      $valid  True if the coupon is valid. False otherwise.
	 * @param WC_Coupon $coupon The coupon object.
	 * @return bool
	 */
	public function is_valid( $valid, $coupon ) {
		if ( $valid && wc_is_store_credit_coupon( $coupon ) && $coupon->get_amount() <= 0 ) {
			$valid = false;
		}

		return $valid;
	}

	/**
	 * Filters the coupon error message.
	 *
	 * @since 3.0.0
	 *
	 * @param string    $message Error message.
	 * @param int       $code    Error code.
	 * @param WC_Coupon $coupon  Coupon object.
	 * @return mixed
	 */
	public function error_message( $message, $code, $coupon ) {
		if ( 100 === $code && wc_is_store_credit_coupon( $coupon ) ) {
			$message = _x( 'There is no credit remaining on this coupon.', 'error message', 'woocommerce-store-credit' );
		}

		return $message;
	}
}

return new WC_Store_Credit_Coupons();
