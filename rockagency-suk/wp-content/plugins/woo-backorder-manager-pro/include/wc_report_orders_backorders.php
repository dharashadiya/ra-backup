<?php
/** If this file is called directly, abort. */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'WC_Report_Stock' ) ) {
	require_once( WP_PLUGIN_DIR . '/woocommerce/includes/admin/reports/class-wc-report-stock.php' );
}

/**
 * class WC_Report_Orders_Backorders
 * the 'Orders On backorder' report class
 */
class WC_Report_Orders_Backorders extends WC_Report_Stock {

	/**
	 * No items found text
	 */
	public function no_items() {
		_e( 'No backordered items found.', 'woo-backorder-manager-pro' );
	}


	/**
	 * Get Products matching stock criteria
	 */
	public function get_items() {
		global $wpdb;

		$this->items = array();

		$backordered = translate( 'Backordered', 'woocommerce' ); // Multilanguage compatible

		// Get orders using a query (too advanced for get_posts)
		$this->items = $wpdb->get_results( "SELECT {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id, {$wpdb->prefix}woocommerce_order_items.order_id, {$wpdb->prefix}woocommerce_order_items.order_item_name, {$wpdb->posts}.post_date, {$wpdb->prefix}woocommerce_order_itemmeta.meta_value AS quantity
		FROM {$wpdb->prefix}woocommerce_order_itemmeta
		LEFT JOIN {$wpdb->prefix}woocommerce_order_items ON ( {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id )
		LEFT JOIN {$wpdb->posts} ON ({$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID)
		WHERE ({$wpdb->prefix}woocommerce_order_itemmeta.meta_key = '{$backordered}')
		AND ({$wpdb->posts}.post_status IN ('wc-processing', 'wc-pending', 'wc-on-hold'))
		AND ({$wpdb->prefix}woocommerce_order_itemmeta.meta_value != '0')
		ORDER BY {$wpdb->posts}.post_date DESC" );

	}


	/**
	 * Output the report
	 */
	public function output_report() {
		global $wcbm_total_backorder_items;

		$this->prepare_items();
		echo '<div class="woocommerce-reports-wide">';
		$this->display();
		echo '</div>';
		echo '<p><a href="'. plugin_dir_url( __FILE__ ) . 'csv-export-orders.php">' . __( 'CSV export', 'woo-backorder-manager-pro' ) . '</a> | ' . sprintf ( __( 'Total orders with items on backorder: %d', 'woo-backorder-manager-pro' ), $wcbm_total_backorder_items ) . '</p>';
	}


	/**
	 * Get column value.
	 * @param mixed $item
	 * @param string $column_name
	 */
	public function column_default( $item, $column_name ) {
		global $wcbm_total_backorder_items;

		if ( ! $wcbm_total_backorder_items ) {
			$wcbm_total_backorder_items = 0;
		}

		switch( $column_name ) {

			case 'order_id' :
				echo '<a href="/wp-admin/post.php?post=' . $item->order_id . '&action=edit">#' . $item->order_id . '</a>';
				$wcbm_total_backorder_items++;
				break;

			case 'product' :
				// Get product ID
				$product_id = wc_get_order_item_meta( $item->order_item_id, '_product_id', true );
				// Get WC_Product
				$product = wc_get_product( $product_id );
				echo '<a href="/wp-admin/post.php?post=' . $product_id . '&action=edit">';
				// SKU available?
				if ( $sku = $product->get_sku() ) {
					echo $sku . ' - ';
				}
				echo $item->order_item_name . '</a>';
				break;

			case 'customer_name' :
				$first_name = get_post_meta( $item->order_id, '_billing_first_name', true );
				$last_name = get_post_meta( $item->order_id, '_billing_last_name', true) ;
				echo $first_name . ' ' . $last_name;
				break;

			case 'order_date' :
				echo $item->post_date;
				break;

			case 'quantity' :
				echo $item->quantity;
				break;
		}
	}


	/**
	 * get_columns function.
	 */
	public function get_columns() {

		$columns = array(
			'order_id'      => __( 'Order', 'woo-backorder-manager-pro' ),
			'product'       => __( 'Product', 'woo-backorder-manager-pro' ),
			'customer_name' => __( 'Customer name', 'woo-backorder-manager-pro' ),
			'order_date'    => __( 'Date', 'woo-backorder-manager-pro' ),
			'quantity'      => __( 'Quantity', 'woo-backorder-manager-pro' ),
		);

		return $columns;
	}


	/**
	 * prepare_items function.
	 */
	public function prepare_items() {
		$this->_column_headers = array( $this->get_columns(), array(), $this->get_sortable_columns() );

		$this->get_items();
	}
}
