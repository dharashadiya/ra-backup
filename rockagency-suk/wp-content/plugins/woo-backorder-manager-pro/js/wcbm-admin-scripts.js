/**
 * WooCommerce Backorder Manager Pro admin scripts
 *
 * @package WooCommerce Backorder Manager Pro
 */

jQuery( document ).ready(
    function( $ ) {
        'use strict';

        // Simple product.
        function backorder_max_visible() {
            if ( 'no' == $( '#_backorders' ).val() ) {
                $( '#_backorder_max_container' ).hide();
                $( '#_backorder_description_container' ).hide();
            } else {
                $( '#_backorder_max_container' ).show();
                $( '#_backorder_description_container' ).show();
            }
        }

        $( '#_backorders' ).parent().after( $( '#_backorder_discription_container' ) ).after( $( '#_backorder_max_container' ) );
        backorder_max_visible();

        $( '#_backorders' ).on(
            'change',
            function() {
                backorder_max_visible();
            }
        );

        // Variations loaded?
        $( '#woocommerce-product-data' ).on(
            'woocommerce_variations_loaded',
            function() {

                function backorder_variable_max_visible( input_id ) {
                    var i = input_id.replace( 'variable_backorders', '' );
                    if ( 'no' == $( '#' + input_id ).val() ) {
                        $( '._backorder_max_wrapper_' + i ).hide();
                        $( '._backorder_description_wrapper_' + i ).hide();
                    } else {
                        $( '._backorder_max_wrapper_' + i ).show();
                        $( '._backorder_description_wrapper_' + i ).show();
                    }
                }

                $( 'select[name^="variable_backorders"]' ).each(
                    function() {
                        var i = this.id.replace( 'variable_backorders', '' );
                        $( this ).parent().after( $( '._backorder_description_wrapper_' + i ) ).after( $( '._backorder_max_wrapper_' + i ) );
                        backorder_variable_max_visible( this.id );
                    }
                );

                $( 'select[name^="variable_backorders"]' ).on(
                    'change',
                    function() {
                        backorder_variable_max_visible( this.id );
                    }
                );
            }
        );

    }
);
