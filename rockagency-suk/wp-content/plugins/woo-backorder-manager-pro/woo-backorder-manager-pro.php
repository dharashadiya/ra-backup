<?php
/**
 * Plugin Name: WooCommerce Backorder Manager Pro
 * Plugin URI: https://1.envato.market/c/1206953/275988/4415?subId1=wbmp&subId2=plugin&u=https%3A%2F%2Fcodecanyon.net%2Fitem%2Fwoocommerce-backorder-manager%2F19624014
 * Description: View reports with units in backorder, set maximum allowed number of backorders, manage backorder email notifications, export backorders to CSV.
 * Version: 2.1.1
 * Author: jeffrey-wp
 * Author URI: https://1.envato.market/c/1206953/275988/4415?subId1=wbmp&subId2=plugin&subId3=profile&u=https%3A%2F%2Fcodecanyon.net%2Fuser%2Fjeffrey-wp%2F
 * Text Domain: woo-backorder-manager-pro
 * Domain Path: /languages/
 * WC tested up to: 3.9
 */

/** If this file is called directly, abort. */
defined( 'ABSPATH' ) || exit; // Exit if accessed directly


/**
 * class WooBackorderManagerPro
 * the main class
 */
class WooBackorderManagerPro {

	/**
	 * Initialize the hooks and filters
	 */
	public function __construct() {
		add_action( 'admin_notices', array( $this, 'wcbm_admin_notices' ) );
		add_filter( 'plugin_action_links', array( $this, 'wcbm_plugin_action_links' ), 10, 2 );
		add_action( 'plugins_loaded', array( $this, 'wcbm_load_plugin_textdomain' ) );

		add_filter( 'woocommerce_get_sections_products', array( $this, 'add_section_backordermanager' ) );
		add_filter( 'woocommerce_get_settings_products', array( $this, 'backordermanager_all_settings' ), 10, 2 );

		add_action( 'woocommerce_email', array( $this, 'woocommerce_remove_email_notifications' ) );

		add_action( 'admin_enqueue_scripts', array( $this, 'wcbm_admin_scripts' ), 10, 1 );
		add_action( 'woocommerce_product_options_inventory_product_data', array( $this, 'wcbm_product_options_inventory_product_data' ), 10, 1 );
		add_action( 'woocommerce_process_product_meta', array( $this, 'wcbm_process_product_meta' ) );
		add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'wcbm_product_after_variable_attributes' ), 10, 3 );
		add_action( 'woocommerce_save_product_variation', array( $this, 'wcbm_save_product_variation' ), 10, 2 );
		add_filter( 'woocommerce_product_backorders_allowed', array( $this, 'wcbm_product_backorders_allowed' ), 10, 3 );
		add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'wcbm_add_to_cart_validation' ), 10, 5 );
		add_action( 'woocommerce_reduce_order_stock', array( $this, 'wcbm_reduce_order_stock' ), 10, 1 );
		add_filter( 'woocommerce_quantity_input_max', array( $this, 'wcbm_quantity_input_max' ), 10, 2 );
		add_filter( 'woocommerce_cart_item_quantity', array( $this, 'wcbm_cart_item_quantity' ), 10, 3 );
		add_filter( 'woocommerce_admin_stock_html', array( $this, 'wcbm_admin_stock_html' ), 10, 3 );
		add_filter( 'woocommerce_get_stock_html', array( $this, 'wcbm_get_stock_html' ), 10, 2 );
		add_filter( 'woocommerce_get_availability_text', array( $this, 'wcbm_get_availability_text' ), 10, 2 );
		add_filter( 'woocommerce_cart_item_backorder_notification', array( $this, 'wcbm_cart_item_backorder_notification' ), 10, 2);

		add_filter( 'manage_edit-product_columns', array( $this, 'wcbm_show_column_max_backorder' ), 10, 1 );
		add_action( 'manage_product_posts_custom_column', array( $this, 'wcbm_product_column_max_backorder' ), 10, 2 );
		add_filter( 'manage_edit-product_sortable_columns', array( $this, 'wcbm_product_column_sortable_max_backorder' ), 10, 1 );
		add_action( 'pre_get_posts', array( $this, 'wcbm_product_column_max_backorder_orderby' ) );

		add_filter( 'woocommerce_admin_reports', array( $this, 'register_backorder_reports' ), 10, 1 );
	}


	/**
	 * Show admin notices
	 * @action admin_notices
	 */
	public function wcbm_admin_notices() {

		// check if WooCommerce is active
		if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

			// Checks if WooCommerce is "Network Active" on a multisite installation of WordPress.
			if ( is_plugin_active_for_network( 'woocommerce/woocommerce.php' ) ) {
				return false;
			}
			?>
			<div class="error"><p>
				<?php printf(
					__( '%s plugin is enabled but not effective. It requires WooCommerce in order to work.', 'woo-backorder-manager-pro' ),
					'<strong>WooCommerce Backorder Manager Pro</strong>'
				); ?>
			</p></div>
			<?php
		}

		if ( class_exists( 'WooCommerceBackorderManager' ) ) {
			?>
			<div class="error"><p>
				<?php printf(
					__( 'The plugin <strong>WooCommerce Backorder Manager</strong> has been deactivated, because the plugin %s is active.', 'woo-backorder-manager-pro' ),
					'<strong>WooCommerce Backorder Manager Pro</strong>'
				); ?>
			</p></div>
			<?php
			deactivate_plugins( str_replace( '\\', '/', dirname( __DIR__ ) ) . '/woocommerce-backorder-manager/woocommerce-backorder-manager.php' );
		}

		if ( class_exists( 'WooBackorderManager' ) ) {
			?>
			<div class="error"><p>
				<?php printf(
					__( 'The plugin <strong>WooCommerce Backorder Manager</strong> has been deactivated, because the plugin %s is active.', 'woo-backorder-manager-pro' ),
					'<strong>WooCommerce Backorder Manager Pro</strong>'
				); ?>
			</p></div>
			<?php
			deactivate_plugins( str_replace( '\\', '/', dirname( __DIR__ ) ) . '/woo-backorder-manager/woo-backorder-manager.php' );
		}
	}


	/**
	 * Show extra links on plugin overview page
	 * @param array $links
	 * @param string $file
	 * @return array
	 * @filter plugin_action_links
	 */
	public function wcbm_plugin_action_links( $links, $file ) {
		if ( $file != plugin_basename( __FILE__ ) )
			return $links;

		return array_merge(
			array(
				'settings' => '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=products&section=backordermanager' ) . '">' . __( 'Settings', 'woo-backorder-manager-pro' ) . '</a>',
				'report_orders'   => '<a href="' . admin_url( 'admin.php?page=wc-reports&tab=orders&report=on_backorder' ) . '">' . __( 'Orders report', 'woo-backorder-manager-pro' ) . '</a>',
				'report_stock'   => '<a href="' . admin_url( 'admin.php?page=wc-reports&tab=stock&report=on_backorder' ) . '">' . __( 'Stock report', 'woo-backorder-manager-pro' ) . '</a>',
			),
			$links
		);
	}


	/**
	 * Load text domain
	 * @action plugins_loaded
	 */
	public function wcbm_load_plugin_textdomain() {
		load_plugin_textdomain( 'woo-backorder-manager-pro', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}


	/**
	 * Create the section 'Backorder Manager' beneath the products tab
	 * @param array $sections
	 * @filter woocommerce_get_sections_products
	 */
	public function add_section_backordermanager( $sections ) {
		$sections['backordermanager'] = __( 'Backorder Manager', 'woo-backorder-manager-pro' );
		return $sections;
	}


	/**
	 * Add settings to the section 'Backorder Manager'
	 * @param array $settings
	 * @param string $current_section
	 * @filter woocommerce_get_settings_products
	 */
	public function backordermanager_all_settings( $settings, $current_section ) {

		// check if the current section is what we want
		if ( $current_section == 'backordermanager' ) {

			$settings = array(
				'section_title' => array(
					'name'     => __( 'Backorder Manager', 'woo-backorder-manager-pro' ),
					'type'     => 'title',
					'desc'     => '',
					'id'       => 'wcbm_section_title'
				),
				'wcbm_backorder_mail_notification' => array(
					'name'     => __( 'Backorder email notifications', 'woo-backorder-manager-pro' ),
					'type'     => 'checkbox',
					'desc_tip' => __( 'Don\'t send email notifications when a backorder is made.', 'woo-backorder-manager-pro' ),
					'desc'     => __( 'Disable backorder email notifications', 'woo-backorder-manager-pro' ),
					'id'       => 'wcbm_backorder_mail_notification'
				),
				'wcbm_show_available_items' => array(
					'name'     => __( 'Show available backorders', 'woo-backorder-manager-pro' ),
					'type'     => 'checkbox',
					'desc_tip' => __( 'Show available items in shop when backorder notification is enabled and stock status is 0 or less.', 'woo-backorder-manager-pro' ),
					'desc'     => __( 'Show available items when backorder notification is enabled', 'woo-backorder-manager-pro' ),
					'id'       => 'wcbm_show_available_items'
				),
				'section_end' => array(
					'type' => 'sectionend',
					'id'   => 'wcbm_section_end'
				)
			);

		}

		return $settings;
	}


	/*
	 * Unhook/remove WooCommerce Emails
	 * @param $email_class
	 * @action woocommerce_email
	 */
	function woocommerce_remove_email_notifications( $email_class ) {

		$backorder_mail_notification = get_option( 'wcbm_backorder_mail_notification' );

		if ( false !== $backorder_mail_notification && 'no' !== $backorder_mail_notification ) {
			// unhooks sending email backorders during store events
			remove_action( 'woocommerce_product_on_backorder_notification', array( $email_class, 'backorder' ) );
		}
	}


	/**
	 * Load JavaScript on new product page and edit product page.
	 *
	 * @param string $hook The name of the hook.
	 * @action admin_enqueue_scripts
	 */
	function wcbm_admin_scripts( $hook ) {

		if ( $hook == 'post-new.php' || $hook == 'post.php' ) {

			global $post;

			if ( 'product' === $post->post_type ) {

				$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

				wp_enqueue_script( 'wcbm_admin_scripts', plugins_url( 'js/wcbm-admin-scripts' . $suffix . '.js', __FILE__ ), array( 'jquery' ), '2.1.1', true );

			}
		}
	}


	/**
	 * Display extra field on inventory product tab.
	 *
	 * @action woocommerce_product_options_inventory_product_data
	 */
	public function wcbm_product_options_inventory_product_data() {

		echo '<span id="_backorder_max_container">';

		woocommerce_wp_text_input(
			array(
				'id'                => '_backorder_max',
				'label'             => __( 'Maximum number of backorders', 'woo-backorder-manager-pro' ),
				'desc_tip'          => true,
				'description'       => __( 'Maximum number of items available for backorder. Leave empty, fill in 0 or -1 for no maximum.', 'woo-backorder-manager-pro' ),
				'type'              => 'number',
				'custom_attributes' => array(
					'step' => 'any',
					'min'  => -1
				),
				'data_type'         => 'backorder_max'
			)
		);

		woocommerce_wp_text_input(
			array(
				'id'                => '_backorder_description',
				'label'             => __( 'Backorder description', 'woo-backorder-manager-pro' ),
				'desc_tip'          => true,
				'description'       => __( 'Override default WooCommerce backorder description shown on the website, with for example date back in stock.', 'woo-backorder-manager-pro' ),
				'type'              => 'text',
				'placeholder'       => __( 'Available on backorder', 'woocommerce' ),
				'data_type'         => 'backorder_description'
			)
		);

		echo '</span>';
	}


	/**
	 * Save custom WooCommerce product data
	 * @param int post_id
	 * @action woocommerce_process_product_meta
	 */
	public function wcbm_process_product_meta( $post_id ) {
		$backorder_max = isset( $_POST['_backorder_max'] ) ? intval( $_POST['_backorder_max'] ) : '';
		update_post_meta( $post_id, '_backorder_max', $backorder_max );

		$backorder_description = isset( $_POST['_backorder_description'] ) ? $_POST['_backorder_description'] : '';
		update_post_meta( $post_id, '_backorder_description', $backorder_description );
	}


	/**
	 * Display extra field on product_variation.
	 *
	 * @param int    $loop
	 * @param array  $variation_data
	 * @param object $variation
	 * @action woocommerce_product_after_variable_attributes
	 */
	public function wcbm_product_after_variable_attributes( $loop, $variation_data, $variation ) {

		woocommerce_wp_text_input(
			array(
				'id'                => '_backorder_max[' . $variation->ID . ']',
				'label'             => __( 'Maximum number of backorders', 'woo-backorder-manager-pro' ),
				'desc_tip'          => false,
				'description'       => __( 'Maximum number of items available for backorder. Leave empty, fill in 0 or -1 for no maximum.', 'woo-backorder-manager-pro' ),
				'type'              => 'number',
				'custom_attributes' => array(
					'step' => 'any',
					'min'  => -1
				),
				'data_type'         => 'backorder_max',
				'value'             => get_post_meta( $variation->ID, '_backorder_max', true ),
				'wrapper_class'     => 'form-row form-row-full _backorder_max_wrapper_' . $loop,
			)
		);

		woocommerce_wp_text_input(
			array(
				'id'                => '_backorder_description[' . $variation->ID . ']',
				'label'             => __( 'Backorder description', 'woo-backorder-manager-pro' ),
				'desc_tip'          => true,
				'description'       => __( 'Override default WooCommerce backorder description shown on the website, with for example date back in stock.', 'woo-backorder-manager-pro' ),
				'type'              => 'text',
				'data_type'         => 'backorder_description',
				'value'             => get_post_meta( $variation->ID, '_backorder_description', true ),
				'placeholder'       => __( 'Available on backorder', 'woocommerce' ),
				'wrapper_class'     => 'form-row form-row-full _backorder_description_wrapper_' . $loop,
			)
		);
	}


	/**
	 * Save custom WooCommerce variation product data
	 * @param int $post_id the variation id
	 * @param $i
	 * @action woocommerce_save_product_variation
	 */
	public function wcbm_save_product_variation( $post_id, $i ) {
		$backorder_max = isset( $_POST['_backorder_max'][$post_id] ) ? intval( $_POST['_backorder_max'][$post_id] ) : '';
		update_post_meta( $post_id, '_backorder_max', $backorder_max );

		$backorder_description = isset( $_POST['_backorder_description'][$post_id] ) ? $_POST['_backorder_description'][$post_id] : '';
		update_post_meta( $post_id, '_backorder_description', $backorder_description );
	}


	/**
	 * Override filter woocommerce_product_backorders_allowed
	 * @param boolean $var backorders allowed
	 * @param int $post_id
	 * @param object $product WooCommerce product
	 * @action woocommerce_product_backorders_allowed
	 */
	public function wcbm_product_backorders_allowed( $var, $post_id, $product ) {

		if ( $var !== false ) {

			// Before WooCommerce 2.7 id of variable product had to be called with get_variation_id()
			if ( version_compare( WC_VERSION, '2.7', '<' ) ) {
				if ( null !== $product->get_variation_id() ) {
					$post_id = $product->get_variation_id();
				}
			}

			$backorder_max = get_post_meta( $post_id, '_backorder_max', true );
			if ( $backorder_max != '' ) {

				$stock = $product->get_stock_quantity();

				if ( $backorder_max < 1 ) { // 0 or -1 = no max
					$var = true;
				} elseif ( $stock <= ( 0 - $backorder_max ) ) { // stock has dropped below maximum number of backorders
					$var = false;
				}
			}
		}

		return $var;
	}


	/**
	 * Validate product quantity when added to cart
	 * @param bool $valid
	 * @param string $product_id
	 * @param int $quantity
	 * @param string $variation_id
	 * @param string $variations
	 * @filter woocommerce_add_to_cart_validation
	 */
	function wcbm_add_to_cart_validation( $valid, $product_id, $quantity, $variation_id = '', $variations = '' ) {

		global $woocommerce;

		if ( ! empty( $variation_id ) ) {
			$product_id = $variation_id;
		}

		$product = wc_get_product( $product_id );

		if ( $product->backorders_allowed() ) { // backorders allowed?

			$backorder_max = get_post_meta( $product_id, '_backorder_max', true );

			if ( $backorder_max > 0 ) { // 0 or -1 = no max
				$stock = $product->get_stock_quantity();
				$availablestock = $stock - ( 0 - $backorder_max );

				$already_in_cart = 0;// Keep a running total to count variations

				// search the cart for the product in question
				foreach ( $woocommerce->cart->get_cart() as $cart_item_keys => $values ) {

					if ( $product_id == $values['variation_id'] || $product_id == $values['product_id'] ) {

						// Add that quantity to total qty for this product
						$already_in_cart += (int) $values['quantity'];

					}

				}

				$product_title = $product->get_title();

				if ( ! empty( $already_in_cart ) ) {
					// there was already a quantity of this item in cart prior to this addition
					// check if the total of already_in_cart + current addition quantity is more than available stock
					if ( ( $already_in_cart + $quantity ) > $availablestock ) {

						// show notice maximum allowed products
						wc_add_notice( apply_filters( 'wbmp_max_qty_already_notice', sprintf( __( 'You can add a maximum of %1$s %2$s\'s to %3$s. You already have %4$s.', 'woo-backorder-manager-pro' ),
								$availablestock,
								$product_title,
								'<a href="' . esc_url( wc_get_cart_url() ) . '">' . __( 'your cart', 'woo-backorder-manager-pro' ) . '</a>',
								$already_in_cart ),
							$availablestock,
							$product_title,
							$already_in_cart ),
						'error' );

						$valid = false;
					}
				} else {
					// none were in cart previously
					// just in case they manually type in an amount greater than we allow, check the input number here too
					if ( $quantity > $availablestock ) {

						// show notice maximum allowed products
						wc_add_notice( apply_filters( 'wbmp_max_qty_notice', sprintf( __( 'You can add a maximum of %1$s %2$s\'s to %3$s.', 'woo-backorder-manager-pro' ),
								$availablestock,
								$product_title,
								'<a href="' . esc_url( wc_get_cart_url() ) . '">' . __( 'your cart', 'woo-backorder-manager-pro' ) . '</a>' ),
							$availablestock,
							$product_title ),
						'error' );

						$valid = false;
					}

				}
			}
		}
		return $valid;
	}


	/**
	 * When reducing order stock check if product goes out of stock
	 * If maximum number of backorders is set and maximum number is reached automatically set product to out of stock
	 * @param object $order
	 * @action woocommerce_reduce_order_stock
	 */
	function wcbm_reduce_order_stock( $order ) {

		foreach ( $order->get_items() as $item ) {

			if ( $item->is_type( 'line_item' ) && ( $product = $item->get_product() ) && ( 'yes' === $product->get_backorders() || 'notify' === $product->get_backorders() ) ) {

				$backorder_max = get_post_meta( $product->get_id(), '_backorder_max', true );

				if ( $backorder_max > 0 ) { // 0 or -1 = no max
					$stock          = $product->get_stock_quantity();
					$availablestock = $stock - ( 0 - $backorder_max );

					if ( $availablestock <= 0 ) { // no more orders allowed
						$product->set_stock_status( 'outofstock' );
						$product->set_backorders( 'no' ); // disable backorders to show 'out of stock'
						$product->save();
					}
				}
			}
		}
	}


	/**
	 * Maximum quantity input
	 * @param int $max_number (can also be empty)
	 * @param object $product WooCommerce product
	 * @filter woocommerce_quantity_input_max
	 */
	public function wcbm_quantity_input_max( $max_number, $product ) {

		if ( $product->backorders_allowed() ) { // backorders allowed?

			$post_id = $product->get_id();
			$stock = $product->get_stock_quantity();
			$quantity_input_max = 20;

			// Before WooCommerce 2.7 id of variable product had to be called with get_variation_id()
			if ( version_compare( WC_VERSION, '2.7', '<' ) ) {
				if ( null !== $product->get_variation_id() ) {
					$post_id = $product->get_variation_id();
				}
			}

			$backorder_max = get_post_meta( $post_id, '_backorder_max', true );
			if ( $backorder_max > 0 ) { // 0 or -1 = no max
				$availablestock = $stock - ( 0 - $backorder_max );
				if ( $availablestock < $quantity_input_max ) { // available stock is to low for maximum quantity products
					$max_number = $availablestock;
				}
			}
		}

		return $max_number;
	}


	/**
	 * Maximum quantity input in WooCommerce cart
	 * @param string $product_quantity input field
	 * @param string $cart_item_key
	 * @param array $cart_item
	 * @filter woocommerce_cart_item_quantity
	 */
	public function wcbm_cart_item_quantity( $product_quantity, $cart_item_key, $cart_item ) {

		$product = $cart_item['data'];

		if ( $product->backorders_allowed() ) { // backorders allowed?

			$post_id = $product->get_id();
			$stock = $product->get_stock_quantity();
			$quantity_input_max = 20;

			// Before WooCommerce 2.7 id of variable product had to be called with get_variation_id()
			if ( version_compare( WC_VERSION, '2.7', '<' ) ) {
				if ( null !== $product->get_variation_id() ) {
					$post_id = $product->get_variation_id();
				}
			}

			$backorder_max = get_post_meta( $post_id, '_backorder_max', true );
			if ( $backorder_max > 0 ) { // 0 or -1 = no max
				$availablestock = $stock - ( 0 - $backorder_max );
				if ( $availablestock < $quantity_input_max ) { // available stock is to low for maximum quantity products
					$max_number = $availablestock;
				}
			}

			if ( isset( $max_number ) ) {
				$count_replaces = 0;
				$product_quantity = preg_replace( '/max="*."/i', 'max="' . $max_number . '"', $product_quantity, -1, $count_replaces ); // replace max attribute
				if ( $count_replaces === 0 ) { // add max attribute is not available
					$product_quantity = str_replace('<input ', '<input max="' . $max_number . '" ', $product_quantity );
				}
			}
		}

		return $product_quantity;
	}


	/**
	 * Show items available for backorder on overview page
	 * @param string $stock_html
	 * @param object $product WooCommerce product
	 * @filter woocommerce_admin_stock_html
	 */
	public function wcbm_admin_stock_html( $stock_html, $product ) {

		if ( $product->backorders_allowed() && $product->is_in_stock() ) { // backorders allowed and product in stock?

			$post_id = $product->get_id();
			$stock = $product->get_stock_quantity();
			$quantity_input_max = 20;

			// Before WooCommerce 2.7 id of variable product had to be called with get_variation_id()
			if ( version_compare( WC_VERSION, '2.7', '<' ) ) {
				if ( null !== $product->get_variation_id() ) {
					$post_id = $product->get_variation_id();
				}
			}

			$backorder_max = get_post_meta( $post_id, '_backorder_max', true );
			if ( $backorder_max > 0 ) { // 0 or -1 = no max
				$availablestock = $stock - ( 0 - $backorder_max );
				$stock_html .= ' ' . sprintf( __( 'max %d available through backorder', 'woo-backorder-manager-pro' ), $availablestock );
			}
		}

		return $stock_html;
	}


	/**
	 * Show available backorder amount when backorder notification is enabled.
	 *
	 * @param string html
	 * @param WC_Product $product WooCommerce product
	 * @filter woocommerce_get_stock_html
	 */
	function wcbm_get_stock_html( $html, $product ) {

		if ( $product->is_type( 'variable' ) ) {
			$product_id = $product->get_variation_id();
		} else {
			$product_id = $product->get_id();
		}

		$show_available_items = get_option( 'wcbm_show_available_items' );
		if ( false !== $show_available_items && 'no' !== $show_available_items ) {

			// Only when backorder notification is enabled
			if ( $product->backorders_require_notification() ) {

				$backorder_max = get_post_meta( $product_id, '_backorder_max', true );

				if ( $backorder_max > 0 ) { // 0 or -1 = no max
					$stock = $product->get_stock_quantity();
					$availablestock = $stock - ( 0 - $backorder_max );
					$html .= '<div class="wbpm-backorder-available wbpm-backorder-available-' . $availablestock . '">' . apply_filters( 'wbmp_available_text', sprintf( __( 'max %d available through backorder', 'woo-backorder-manager-pro' ), $availablestock ), $availablestock ) . '</div>';
				} else {
					$html .= '<div class="wbpm-backorder-available wbpm-backorder-available-unlimited">' . apply_filters( 'wbmp_available_text_unlimited', __( 'unlimited available through backorder', 'woo-backorder-manager-pro' ) ) . '</div>';
				}
			}
		}

		return $html;
	}


	/**
	 * Show custom availability text for backorders on product page.
	 *
	 * @param string     $availability
	 * @param WC_Product $product WooCommerce product.
	 * @filter woocommerce_get_availability_text
	 */
	function wcbm_get_availability_text( $availability, $product ) {

		if ( $product->is_type( 'variable' ) ) {
			$product_id = $product->get_variation_id();
		} else {
			$product_id = $product->get_id();
		}

		$backorder_description = get_post_meta( $product_id, '_backorder_description', true );
		if ( ! empty( $backorder_description ) ) {
			if ( $product->managing_stock() && $product->is_on_backorder( 1 ) ) {
				$availability = $product->backorders_require_notification() ? esc_html( $backorder_description ) : '';
			} elseif ( ! $product->managing_stock() && $product->is_on_backorder( 1 ) ) {
				$availability = esc_html( $backorder_description );
			}
		}

		return $availability;
	}


	/**
	 * Show custom availability text for backorders in cart.
	 * Available since WooCommerce 3.4.0, $product_id is available since WooCommerce 3.5.0 so use 0 as fallback.
	 *
	 * @param string $html
	 * @param int    $product_id WooCommerce product ID.
	 * @filter woocommerce_cart_item_backorder_notification
	 */
	function wcbm_cart_item_backorder_notification( $html, $product_id = 0 ) {

		$backorder_description = get_post_meta( $product_id, '_backorder_description', true );
		if ( ! empty( $backorder_description ) ) {
			$html = '<p class="backorder_notification">' . esc_html( $backorder_description ) . '</p>';
		}

		return $html;
	}


	/**
	 * Add extra product column 'Max backorders'
	 * @param array $columns
	 * @filter manage_edit-product_columns
	 */
	function wcbm_show_column_max_backorder( $columns ) {

		// add column
		$columns['max_backorder'] = __( 'Max backorders', 'woo-backorder-manager-pro' );

		return $columns;
	}


	/**
	 * Show data in product column
	 * @param string $column
	 * @param int $postid
	 * @action manage_product_posts_custom_column
	 */
	function wcbm_product_column_max_backorder( $column, $postid ) {

		if ( $column == 'max_backorder' ) {

			$product = wc_get_product( $postid );
			if ( $product->is_type( 'variable' ) ) {
				$variations = $product->get_available_variations();
				// sum max backorders for variable products
				$backorder_max_total = NULL;
				foreach ( $variations as $variation ) {

					$variation_id = $variation['variation_id'];

					if ( 'no' != get_post_meta( $variation_id, '_backorders', true ) ) {

						$backorder_max = get_post_meta( $variation_id, '_backorder_max', true );
						if ( $backorder_max != '' ) {
							if ( $backorder_max < 1 ) {
								$backorder_max_total = '-1';
								break;
							}
							$backorder_max_total += intval( $backorder_max );
						}

					}
				}
			} elseif ( 'no' != get_post_meta( $postid, '_backorders', true ) ) {
				$backorder_max_total = get_post_meta( $postid, '_backorder_max', true );
			}

			if ( isset( $backorder_max_total ) ) {

				// show 'unlimited' when there is no backorder limit
				if ( $backorder_max_total != '' && intval( $backorder_max_total ) < 1 ) {
					$backorder_max_total = __( 'unlimited', 'woo-backorder-manager-pro' );
				}

				echo $backorder_max_total;
			}
		}

	}


	/**
	 * Set 'Max backorders' as sortable column
	 * @param array $columns
	 * @filter manage_edit-product_sortable_columns
	 */
	function  wcbm_product_column_sortable_max_backorder( $columns ) {

		// add column
		$columns['max_backorder'] = 'max_backorder';

		return $columns;
	}


	/**
	 * Make 'Max backorders' sortable
	 * @param object $query WP_Query
	 * @action pre_get_posts
	 */
	function wcbm_product_column_max_backorder_orderby( $query ) {
		if ( ! is_admin() )
			return;

		$orderby = $query->get( 'orderby');

		if ( 'max_backorder' == $orderby ) {
			$query->set( 'meta_key', '_backorder_max' );
			$query->set( 'orderby', 'meta_value_num' );
		}
	}


	/**
	 * Show reports 'On Backorder'
	 * @param array $reports
	 * @filter woocommerce_admin_reports
	 */
	public function register_backorder_reports( $reports ) {

		$orders_on_backorder = array(
			'title'       => __( 'On backorder', 'woo-backorder-manager-pro' ),
			'description' => '',
			'hide_title'  => true,
			'callback'    => array( $this, 'get_backorder_orders_report' ),
		);

		$reports['orders']['reports']['on_backorder'] = $orders_on_backorder;


		$stock_on_backorder = array(
			'title'       => __( 'On backorder', 'woo-backorder-manager-pro' ),
			'description' => '',
			'hide_title'  => true,
			'callback'    => array( $this, 'get_backorder_stock_report' ),
		);

		$reports['stock']['reports']['on_backorder'] = $stock_on_backorder;


		return $reports;
	}



	/**
	 * Callback for orders on backorder report
	 */
	public function get_backorder_orders_report() {
		include_once( 'include/wc_report_orders_backorders.php' );

		if ( ! class_exists( 'WC_Report_Orders_Backorders' ) )
			return;

		$report = new WC_Report_Orders_Backorders;
		$report->output_report();
	}


	/**
	 * Callback for stock on backorder report
	 */
	public function get_backorder_stock_report() {
		include_once( 'include/wc_report_stock_backorders.php' );

		if ( ! class_exists( 'WC_Report_Stock_Backorders' ) )
			return;

		$report = new WC_Report_Stock_Backorders;
		$report->output_report();
	}

}
$woobackordermanagerpro = new WooBackorderManagerPro();
