=== WooCommerce Backorder Manager Pro ===
Contributors: jeffrey-wp
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=SSNQMST6R28Q2
Tags: woocommerce, backorder, backorders, stock, out of stock, backorder manager, report, notications, e-commerce, ecommerce, CSV export
Requires at least: 4.0
Tested up to: 5.3.2
Stable tag: 2.1.1

Backorder Manager for WooCommerce. View reports with products and orders in backorder, set maximum allowed number of backorders, manage backorder email notifications.

== Description ==

The best management plugin for backorders in WooCommerce. View reports with products and orders in backorder, set maximum allowed number of backorders, manage backorder email notifications.

= Features =
* View report 'Products On Backorder' in WooCommerce (with export option)
* View report 'Orders with Backorders' in WooCommerce (with export option)
* Set the maximum allowed number of backorders for a product and product variation
* When the maximum allowed number of backorders is reached the stock status will switch to 'Out of stock'
* Show products available for backorder on product overview page
* Show maximum backorders in sortable column on product overview page
* Option to show available backorder items in shop when backorder notification is enabled
* Option to show custom backorder description on product page and cart
* Manage backorder email notifications
* Export backorders to CSV
* Developer friendly with filters wbmp_max_qty_notice, wbmp_max_qty_already_notice, wbmp_available_text and wbmp_available_text_unlimited
* Compatible with WooCommerce 2.5 - 3.8
* Multilanguage ready


== Roadmap ==
* Add more export formats