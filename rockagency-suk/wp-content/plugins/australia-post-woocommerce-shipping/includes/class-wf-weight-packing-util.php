<?php 
if(!class_exists('WeightPacketUtil')){

    class WeightPacketUtil{ 
        public function pack_items_into_weight_box($items,  $max_weight){
            $this->debug = (get_option('debug_mode') == 'yes' )? true : false;
            $boxes      =   array();
            $unpacked   =   array();
            foreach($items as $item){
                $fitted         =   false;
                $item_weight    =   ( float )$item['weight'];

                foreach($boxes as $box_key  =>  $box){
                    if(($max_weight - $box['weight'])   >=  $item_weight){
                        $boxes[$box_key]['weight'] = $boxes[$box_key]['weight'] + $item_weight;
                        $boxes[$box_key]['items'][] = $item['data'];
                        $fitted = true;
                        break;
                    }else{
                        $fitted = false;
                    }
                }

                if(!$fitted){
                    if($item_weight <=  $max_weight){
                        $boxes[]    =   array(
                            'weight' =>  $item_weight,
                            'items' =>  array($item['data']),
                        );
                    }else{
                        $unpacked[] =   array(
                            'weight' =>  $item_weight,
                            'items' =>  array($item['data']),
                        );
                    }                   
                }
            }

            $weight_packed_boxes = $boxes;
            $count_weight_packed_boxes = 0;

            /*Filtering packed boxes with repeated quantity values*/
            foreach($boxes as $box){
                $boxes_new = array();
                $box_items = $box['items'];
                $box_item_product_ids = array();
                foreach($box_items as $box_item){
                    if(isset($box_item['variation_id']) && !empty($box_item['variation_id'])){
                        $box_item_product_ids[] = $box_item['variation_id'];
                    }else{
                        $box_item_product_ids[] = $box_item['product_id'];
                    }
                }

                $box_item_products_id_with_quantity = array();
                $count_box_item_products_id_with_quantity = 0;
                $box_item_product_ids_unique = array_unique($box_item_product_ids);
                $box_item_product_ids_quantity = array_count_values($box_item_product_ids);

                foreach($box_item_product_ids_quantity as $box_item_product_ids_quantity_key => $box_item_product_ids_quantity_element){
                    foreach($box_items as $box_item){
                        if(isset($box_item['variation_id']) && ($box_item['variation_id'] == $box_item_product_ids_quantity_key)){
                            $box_item['quantity'] = $box_item_product_ids_quantity[$box_item_product_ids_quantity_key];
                            $boxes_new[] = $box_item;
                            break;
                        }else if(isset($box_item['product_id']) && ($box_item['product_id'] == $box_item_product_ids_quantity_key)){
                            $box_item['quantity'] = $box_item_product_ids_quantity[$box_item_product_ids_quantity_key];
                            $boxes_new[] = $box_item;
                            break;
                        }
                    }
                }
                $weight_packed_boxes[$count_weight_packed_boxes]['items'] = $boxes_new;
                $count_weight_packed_boxes++;
            }

            $result = new WeightPackResult();
            $result->set_packed_boxes($weight_packed_boxes);
            $result->set_unpacked_items($unpacked);
            return $result;
        }
        
        public function pack_all_items_into_one_box($items){
            $boxes          =   array();
            $total_weight   =   0;
            $box_items      =   array();
            foreach($items as $item){
                $total_weight   =   ( float )$total_weight + ( float )$item['weight'];
                $box_items[] =   $item['data'];
            }
            $boxes[]    =   array(
                'weight'    =>  $total_weight,
                'items'     =>  $box_items
            );
            $result =   new WeightPackResult();
            $result->set_packed_boxes($boxes);
            return $result;
        }

        /**
         * Output a message
         */
        public function debug($message, $type = 'notice') {
            if ($this->debug) {
                if (version_compare(WOOCOMMERCE_VERSION, '2.1', '>=')) {
                    wc_add_notice($message, $type);
                } else {
                    global $woocommerce;

                    $woocommerce->add_message($message);
                }
            }
        }

    }
}