<?php
$this->init_settings(); 
global $woocommerce;
$wc_main_settings = array();
$posted_services = array();
$this->contracted_rates = $this->get_option('contracted_rates') == 'yes' ? true : false;
if(isset($_POST['wf_aus_rates_save_changes_button']))
{
    $wc_main_settings = get_option('woocommerce_wf_australia_post_settings');
    $wc_main_settings['title'] = (isset($_POST['wf_australia_post_title'])) ? sanitize_text_field($_POST['wf_australia_post_title']) : ((isset($this->settings['wf_australia_post_starTrack_rates']) && ($this->settings['wf_australia_post_starTrack_rates'] == 'yes'))? __( 'StarTrack', 'wf-shipping-auspost'):   __( 'Australia Post', 'wf-shipping-auspost' ));
    $wc_main_settings['elex_stratrack_title'] = (isset($_POST['wf_australia_startrack_post_title'])) ? sanitize_text_field($_POST['wf_australia_startrack_post_title']) :  __( 'StarTrack', 'wf-shipping-auspost');
    $wc_main_settings['availability'] = (isset($_POST['wf_australia_post_availability']) && $_POST['wf_australia_post_availability'] ==='all') ? 'all' : 'specific';
    $wc_main_settings['startrack_availability'] = (isset($_POST['wf_australia_post_startrack_availability']) && $_POST['wf_australia_post_startrack_availability'] ==='all') ? 'all' : 'specific';
    $services         = array();
    $sub_services     = array();
    $startrack_services = array();
    $custom_sub_services = $this->non_contracted_alternate_services;

    if(isset($_POST['australia_post_service'])){
        $posted_services  = $_POST['australia_post_service'];

        if(is_array($posted_services) && !empty($posted_services)){
            foreach ( $posted_services as $code => $settings ) {

                $services[ $code ] = array(
                    'name'                  => wc_clean( $settings['name'] ),
                    'order'                 => wc_clean( $settings['order'] ),
                    'enabled'               => isset( $settings['enabled'] ) ? true : false,
                    'adjustment'            => wc_clean( $settings['adjustment'] ),
                    'adjustment_percent'    => str_replace( '%', '', wc_clean( $settings['adjustment_percent'] ) ),
                    'extra_cover'           => isset( $settings['extra_cover'] ) ? true : false,
                    'delivery_confirmation' => isset( $settings['delivery_confirmation'] ) ? true : false,
                );

            }
        }
    }

    if(isset($_POST['startrack_service'])){
        $posted_services  = $_POST['startrack_service'];

        if(is_array($posted_services) && !empty($posted_services)){
            foreach ( $posted_services as $code => $settings ) {

                $startrack_services[ $code ] = array(
                    'name'                  => wc_clean( $settings['name'] ),
                    'order'                 => wc_clean( $settings['order'] ),
                    'enabled'               => isset( $settings['enabled'] ) ? true : false,
                    'adjustment'            => wc_clean( $settings['adjustment'] ),
                    'adjustment_percent'    => str_replace( '%', '', wc_clean( $settings['adjustment_percent'] ) ),
                    'extra_cover'           => isset( $settings['extra_cover'] ) ? true : false,
                    'delivery_confirmation' => isset( $settings['delivery_confirmation'] ) ? true : false,
                );

            }
        }
    }

    if(isset($_POST['auspost_sub_services'])){
        $sub_services_in_request  = isset($_POST['auspost_sub_services'])? $_POST['auspost_sub_services']: array();
        $subservices_name = $_POST['subservices_name'];

        foreach($custom_sub_services as $custom_sub_service_key => $custom_sub_service_value){
            $sub_services[$custom_sub_service_key] = array(
                'name' => !empty($subservices_name[$custom_sub_service_key])? $subservices_name[$custom_sub_service_key]: $custom_sub_service_value['name'],
                'enabled' => isset($sub_services_in_request[$custom_sub_service_key])? true: false,
                'main_service' => $custom_sub_service_value['main_service']
            );
        }
    }
        
    $wc_main_settings['services'] = $services;
    $wc_main_settings['sub_services'] = $sub_services;
    $wc_main_settings['startrack_services'] = $startrack_services;
    $wc_main_settings['offer_rates'] = (isset($_POST['wf_australia_post_offer_rates'])) ? 'cheapest' : 'all';
    $wc_main_settings['show_insurance_checkout_field'] = (isset($_POST['wf_australia_post_show_insurance_checkout_field'])) ? 'yes' : '';
    $wc_main_settings['show_authority_to_leave_checkout_field'] = (isset($_POST['wf_australia_post_show_authority_to_leave_checkout_field'])) ? 'yes' : '';
    $wc_main_settings['show_signature_required_field'] = (isset($_POST['wf_australia_post_show_signature_required_field'])) ? 'yes' : '';
    
    if($wc_main_settings['availability'] === 'specific')
    {
        $wc_main_settings['countries'] = isset($_POST['wf_australia_post_countries']) ? $_POST['wf_australia_post_countries'] : '';
    }

    update_option("services_saved_in_settings", true);
    
    update_option('woocommerce_wf_australia_post_settings',$wc_main_settings);
}

$general_settings = get_option('woocommerce_wf_australia_post_settings');
$this->custom_services = isset($general_settings['services']) ? $general_settings['services'] : $this->settings['services'];
$auspost_customer_account_error_on_rates_settings = get_option('auspost_customer_account_error');
if($this->rate_type == 'startrack'){
    $this->api_pwd = $this->settings['wf_australia_post_starTrack_api_pwd'];
    $this->api_account_no = $this->settings['wf_australia_post_starTrack_api_account_no'];
}else if(empty($this->settings['wf_australia_post_starTrack_api_account_no'])){
    update_option("insufficient_authentication_data", 'yes');
}

if(!empty($auspost_customer_account_error_on_rates_settings)){
    echo '<div class="error"><p>' . __($auspost_customer_account_error_on_rates_settings, 'wf-shipping-auspost') . '</p></div>';
    delete_option('auspost_customer_account_error');
}
?>

<table>
    <tr valign="top">
        <td style="width:30%;font-weight:800;">
            <label for="wf_australia_post_delivery_time"><?php _e('Show/Hide','wf-shipping-auspost') ?></label>
        </td>
        <td scope="row" class="titledesc" style="display: block;margin-bottom: 20px;margin-top: 3px;">
            <fieldset style="padding:3px;">
                <input class="input-text regular-input " type="checkbox" name="wf_australia_post_offer_rates" id="wf_australia_post_offer_rates" style="" value="yes" <?php echo (isset($general_settings['offer_rates']) && $general_settings['offer_rates'] ==='cheapest') ? 'checked' : ''; ?> placeholder="">  <?php _e('Show Cheapest Rates Only','wf-shipping-auspost') ?> <span class="woocommerce-help-tip" data-tip="<?php _e('On enabling this, the cheapest rate will be shown in the cart/checkout page.','wf-shipping-auspost') ?>"></span>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <td style="width:30%;font-weight:800;">
            <label for="wf_australia_post_delivery_time"><?php _e('Enable Checkout fields','wf-shipping-auspost') ?></label>
        </td>
        <td scope="row" class="titledesc" style="display: block;margin-bottom: 20px;margin-top: 3px;">
            <fieldset style="padding:3px;">
                <input class="input-text regular-input " type="checkbox" name="wf_australia_post_show_insurance_checkout_field" id="wf_australia_post_show_insurance_checkout_field" style="" value="yes" <?php echo (isset($general_settings['show_insurance_checkout_field']) && $general_settings['show_insurance_checkout_field'] ==='yes') ? 'checked' : ''; ?> placeholder="">  <?php _e('Extra Cover','wf-shipping-auspost') ?> <span class="woocommerce-help-tip" data-tip="<?php _e('Enable this field to let customers choose Extra Cover option','wf-shipping-auspost') ?>"></span>
            </fieldset>
            <fieldset style="padding:3px;">
                <input class="input-text regular-input " type="checkbox" name="wf_australia_post_show_authority_to_leave_checkout_field" id="wf_australia_post_show_authority_to_leave_checkout_field" style="" value="yes" <?php echo (isset($general_settings['show_authority_to_leave_checkout_field']) && $general_settings['show_authority_to_leave_checkout_field'] ==='yes') ? 'checked' : ''; ?> placeholder="">  <?php _e('Authority To Leave','wf-shipping-auspost') ?> <span class="woocommerce-help-tip" data-tip="<?php _e('Enable this field to let customers choose whether they want to apply Authority To Leave option','wf-shipping-auspost') ?>"></span>
            </fieldset>
            <fieldset style="padding:3px;">
                <input class="input-text regular-input " type="checkbox" name="wf_australia_post_show_signature_required_field" id="wf_australia_post_show_signature_required_field" style="" value="yes" <?php echo (isset($general_settings['show_signature_required_field']) && $general_settings['show_signature_required_field'] ==='yes') ? 'checked' : ''; ?> placeholder="">  <?php _e('Signature Required','wf-shipping-auspost') ?> <span class="woocommerce-help-tip" data-tip="<?php _e('Enable this field to let customers choose Signature Required on delivery.','wf-shipping-auspost') ?>"></span>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <td colspan="2">
            <?php
            include_once( 'wf_html_services.php' );
            ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align:right;padding-right: 10%;">
            <br/>
            <input type="submit" value="<?php _e('Save Changes','wf-shipping-auspost') ?>" class="button button-primary" name="wf_aus_rates_save_changes_button">
            
        </td>
    </tr>

    </table>