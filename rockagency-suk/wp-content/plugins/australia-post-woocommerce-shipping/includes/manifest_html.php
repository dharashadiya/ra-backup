<style>
    .manifest_generation_input{
        width: 100%;
    }

    .download_manifest_button{
        width: 100% !important;
    }

    .print_manifest_button{
        width: 65% !important;
    }

    .manifest_generation_input_td_print_manifest{
        width: 170px !important;
    }

    .custom_button {
        width: 120% !important,
        margin-left: 10% !important
    }

    .elex_manifest_download_icon{
        padding: 1% 3% !important;
        width: 10% !important;
    }

    .elex_manifest_delete_checkbox{
        padding: 1% 4% !important;
        width: 11% !important;
    }

    .elex_manifest_print_icon{
        padding: 1% 3% !important;
        width: 10% !important;
    }

    .elex_manifest_numbers, .elex_manifest_woocommerce_order_ids, .elex_manifest_woocommerce_order_shipment_ids{
        padding: 0% 2% !important;
        text-align: left;
        width: 14% !important; 
    }

    .elex_auspost_delete_manifests{
        margin: 0% 56% !important;
        width: 5% !important;
    }

    .elex_manifest_manifest_generated_date{
        width: 13% !important;
        padding: 0% 2% !important;
    }

    #elex_auspost_manifest_paging_nav{
        width: 16% !important;
    }

    #elex_auspost_manifests_history_table{
        border:1px solid #ddd;
        width:100%;
        height: auto;
        font-size:small !important;
        margin-bottom:5px;
        width: 61% !important;
    }

    /*Pagination*/
    .paging-nav {
        text-align: right;
        padding-top: 2px;
        margin: 5px 45% !important;
    }
    .paging-nav a{
        margin: auto 1px;
        text-decoration: none;
        display: inline-block;
        padding: 1px 7px;
        background: #91b9e6;
        color: white;
        border-radius: 3px;
    }
    .paging-nav .selected-page{
        background: #187ed5;
        font-weight: bold;
    }
    .paging-nav, #tableData {
        width: 200px;
        margin: 0 auto;
        font-family: Arial, sans-serif;
    }

    #tableData {
        margin-top: 100px;
        border-spacing: 0;
        border: 1px dotted #ccc;
    }
    #tableData th {
        background: #e5f0fb;
        text-align: left;
        border-bottom: 2px solid #91b9e6;
    }
    #tableData td {
        padding: 3px 10px;
        border-bottom: 1px dotted #ccd;
    }
</style>

<script>
    jQuery(document).ready(function(){
        jQuery('.manifest_generation_type').change(function(){
            var method = jQuery('.manifest_generation_input').val();
            switch(method)
            {
                case 'order_id':
                    jQuery('.shipdate').show();
                    jQuery('.order_id').show();
                    jQuery('.shipdate').hide();
                    jQuery('.shipdate').prop('required', false);
                    break;
                case 'shipdate':
                    jQuery('.order_id').hide();
                    jQuery('.shipdate').show();
                    jQuery('.shipdate').prop('required', true);
                    jQuery('.order_id').prop('required', false);
                    break;
                default:
                    jQuery('.order_id').show();
                    jQuery('.shipdate').hide();
                    jQuery('.order_id').prop('required', true);
                    jQuery('.shipdate').prop('required', false);
            }
        });

        var method_on_load = jQuery('.manifest_generation_input').val();

        if(method_on_load == 'order_id'){
            jQuery('.order_id').show();
            jQuery('.shipdate').hide();
            jQuery('.order_id').prop('required', true);
            jQuery('.shipdate').prop('required', false);
        }

        jQuery('#method_type_info_more').click(function(){
            jQuery('#method_type_info_more').closest('td').hide();
            jQuery('#method_type_info_less').closest('td').show();
            jQuery('#manifest_generation_method_info').show();
        });
        jQuery('#method_type_info_less').click(function(){
            jQuery('#method_type_info_more').closest('td').show();
            jQuery('#method_type_info_less').closest('td').hide();
            jQuery('#manifest_generation_method_info').hide();
        });
        jQuery('#more_order_id').click(function(){
            jQuery('#more_order_id').closest('td').hide();
            jQuery('#less_order_id').closest('td').show();
            jQuery('#order_id').show();
        });
        jQuery('#less_order_id').click(function(){
            jQuery('#more_order_id').closest('td').show();
            jQuery('#less_order_id').closest('td').hide();
            jQuery('#order_id').hide();
        });
    });
</script>

<h1><?php _e('Australia Post Orders Manifest', 'wf-auspost-shipping'); ?></h1>
<form id="elex_auspost_manifest_form" method="POST">
    <table id="elex_auspost_manifest_table">
        <tr>
            <td>
                <h3><?php _e('Select Orders', 'wf-auspost-shipping'); ?></h3>
            </td>
            <td>
                <select name="manifest_generation_type" class="manifest_generation_type manifest_generation_input">
                    <!--<option value="all"><?php // _e('ALL', 'wf-auspost-shipping'); ?></option> --><!-- Will provide in next version -->
                    <option value="order_id"><?php _e('By Order ID', 'wf-auspost-shipping'); ?></option>
                    <option value="shipdate"><?php _e('By Date of Label Generation', 'wf-auspost-shipping'); ?></option>
                </select>
            </td>
            <td>
                <a href='#' id='method_type_info_more'><?php _e('More Info', 'wf-auspost-shipping'); ?></a>
            </td>
            <td style="display: none;">
                <a href="#" id='method_type_info_less'><?php _e('Less', 'wf-auspost-shipping'); ?></a>
            </td>
        </tr>
        <tr>
            <td colspan="2" id='manifest_generation_method_info' style="display: none;">
                <p> <?php _e('A Manifest generation request is submitted based on the method of generation you select:', 'wf-auspost-shipping'); ?></p>
                <ul style="list-style: disc">
                    <ol>
                        <!--<li><?php //_e('“ALL” – Includes Shipment ids of recent 30 orders.', 'wf-auspost-shipping'); ?></li>--> <!-- Will provide in next version-->
                        <li><?php _e('“Order ID” – Submits the specific Shipment Ids of the WooCommerce Order Ids you enter.', 'wf-auspost-shipping'); ?></li>
                        <li><?php _e('“Date of Label Generation” – Include all Shipment Ids generated on the specified date.', 'wf-auspost-shipping'); ?></li>
                    </ol>
            </td>
        </tr>
        <tr class="order_id" style="display: none">
            <td>
                <h3><?php _e('Order IDs', 'wf-auspost-shipping'); ?></h3>
            </td>
            <td width="300px">
                <input type="text" name="order_id" class="manifest_generation_input">
            </td>
            <td>
                <a href='#' id='more_order_id'><?php _e('More Info', 'wf-auspost-shipping'); ?></a>
            </td>
            <td style="display: none;">
                <a href="#" id='less_order_id'><?php _e('Less', 'wf-auspost-shipping'); ?></a>
            </td>
        </tr>
        <tr>
            <td colspan="2" id="order_id" style="display: none;">
                <?php _e('Shipment Ids of these WooCommerce Order IDs separated by comma (no spaces) will be used to generate the Manifest.','wf-auspost-shipping'); ?>
            </td>
        </tr>
        <tr class="shipdate" style="display: none;">
            <td>
                <h3><?php _e('Label Generation Date', 'wf-auspost-shipping'); ?></h3>
            </td>
            <td>
                <input type="date" name="shipdate" class="manifest_generation_input">
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="<?php _e('Generate', 'wf-auspost-shipping'); ?>" name="generate" data-tip="<?php _e('Generate Manifest', 'wf-shipping-auspost'); ?>">
            </td>
        </tr>
    </table>
    <br><br>

    <!-- AusPost Order Manifest History Table -->
    <?php $manifest_history = get_option('elex_auspost_manifest_history');?>
    <?php if(!empty($manifest_history)):?>
        <table id="elex_auspost_manifests_history_table" border="1">
            <thead>
                <tr>
                    <th><?php _e('Manifest Number', 'wf-shipping-auspost');?></th>
                    <th><?php _e('Order Ids', 'wf-shipping-auspost');?></th>
                    <th><?php _e('Shipment Ids', 'wf-shipping-auspost');?></th>
                    <th><?php _e('Date', 'wf-shipping-auspost');?></th>
                    <th><?php _e('Download', 'wf-shipping-auspost');?></th>
                    <th><?php _e('Print', 'wf-shipping-auspost');?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php

                    $order_summary_download_url = $order_summary_download_url_auspost = admin_url('/admin.php?page=auspost_manifest&print_manifest=yes&manifest_download_method_selected=attachment');
                    $order_summary_print_url = $order_summary_print_url_auspost = admin_url('/admin.php?page=auspost_manifest&print_manifest=yes&manifest_download_method_selected=inline');
                    $order_summary_download_url_startrack = admin_url('/admin.php?page=auspost_manifest&print_manifest=yes&manifest_download_method_selected=attachment');
                    $order_summary_print_url_startrack = admin_url('/admin.php?page=auspost_manifest&print_manifest=yes&manifest_download_method_selected=inline');

                    rsort($manifest_history);

                    foreach($manifest_history as $manifests){
                        $manifests_array = $manifests;
                        $manifests_array = array_shift($manifests_array);
                        if(is_array($manifests_array) && !empty($manifests_array)){
                            $manifest_orders_array = '';
                            $manifest_generation_date = '';
                            $startrack_order_summary_id = '';
                            $auspost_order_summary_id = '';
                            $manifest_numbers = '';
                            $manifest_numbers_array = array();
                            foreach($manifests as $manifest){
                                $order_ids_array = '';
                                $shipment_ids_string = '';
                                $manifest_numbers .= $manifest['id']."<br>";
                                $manifest_numbers_array[] = $manifest['id'];
                                $manifest_generation_date = $manifest['date'];
                                foreach($manifest['data'] as $manifest_datum_key => $manifest_datum_value){
                                    $order_ids_array .= ' '.$manifest_datum_key. ',';
                                    $shipment_ids = $manifest_datum_value['shipment_ids'];
                                    foreach($shipment_ids as $shipment_id){
                                        $shipment_ids_string .= ' '.$shipment_id.',';
                                    }
                                }
                                if($manifest['type'] == 'StarTrack'){
                                    $startrack_order_summary_id = $manifest['id'];
                                }else{
                                    $auspost_order_summary_id = $manifest['id'];
                                }
                            }

                            $manifest_numbers_delete = implode(',', $manifest_numbers_array);

                            echo "<tr>";
                            echo "<td class='elex_manifest_numbers'>".$manifest_numbers."</td>";
                            $order_summary_download_url_auspost .= '&auspost_order_id='.$auspost_order_summary_id;
                            $order_summary_print_url_auspost .= '&auspost_order_id='.$auspost_order_summary_id;
                            $order_summary_download_url_startrack .= '&startrack_order_id='.$startrack_order_summary_id;
                            $order_summary_print_url_startrack .= '&startrack_order_id='.$startrack_order_summary_id;
                            echo "<td class='elex_manifest_woocommerce_order_ids'>".rtrim($order_ids_array, ',')."</td>";
                            echo "<td class='elex_manifest_woocommerce_order_shipment_ids'>".rtrim($shipment_ids_string, ',')."</td>";
                            echo "<td class='elex_manifest_manifest_generated_date'>".$manifest_generation_date."</td>";
                            echo "<td class='elex_manifest_download_icon'>AusPost<br><a class='button' target='_blank' href=".$order_summary_download_url_auspost."><i class='fa fa-download'></i></a>";
                            if($startrack_order_summary_id != ''){
                                echo "<br><br>StarTrack<a class='button' target='_blank' href=".$order_summary_download_url_startrack."><i class='fa fa-download'></i></a>";
                            }
                            echo '</td>';
                            echo "<td class='elex_manifest_print_icon'>AusPost<br><a class='button' target='_blank' href=".$order_summary_print_url_auspost."><i class='fa fa-print'></i></a>";
                            if($startrack_order_summary_id != ''){
                                echo "<br><br>StarTrack<br><a class='button' target='_blank' href=".$order_summary_print_url_startrack."><i class='fa fa-print'></i></a>";
                            }
                            echo '</td>';
                            echo "<td class='elex_manifest_delete_checkbox'><input name='elex_auspost_delete_manifest_id[]' value='".$manifest_numbers_delete."' type='checkbox'></td>";
                            echo "</tr>";
                        }else{
                            $order_summary_download_url .= '&auspost_order_id='.$manifests['id'];
                            $order_summary_print_url .= '&auspost_order_id='.$manifests['id'];
                            echo "<tr>";
                            echo "<td class='elex_manifest_numbers'>".$manifests['id']."</td>";
                            $order_ids_array = '';
                            $shipment_ids_string = '';
                            foreach($manifests['data'] as $manifest_datum_key => $manifest_datum_value){
                                $order_ids_array .= ' '.$manifest_datum_key. ',';
                                $shipment_ids = $manifest_datum_value['shipment_ids'];
                                foreach($shipment_ids as $shipment_id){
                                    $shipment_ids_string .= ' '.$shipment_id.',';
                                }
                            }
                            $order_manifest_generated_date = isset($manifests['date'])? $manifests['date']: '';
                            echo "<td class='elex_manifest_woocommerce_order_ids'>".rtrim($order_ids_array, ',')."</td>";
                            echo "<td class='elex_manifest_woocommerce_order_shipment_ids'>".rtrim($shipment_ids_string, ',')."</td>";
                            echo "<td class='elex_manifest_manifest_generated_date'>".$order_manifest_generated_date."</td>";
                            echo "<td class='elex_manifest_download_icon'><a class='button' target='_blank' href=".$order_summary_download_url."><i class='fa fa-download'></i></a></td>";
                            echo "<td class='elex_manifest_print_icon'><a class='button' target='_blank' href=".$order_summary_print_url."><i class='fa fa-print'></i></a></td>";
                            echo "<td class='elex_manifest_delete_checkbox'><input name='elex_auspost_delete_manifest_id[]' value='".$manifests['id']."' type='checkbox'></td>";
                            echo "</tr>";
                        }
                    }
                ?>
            </tbody>
        </table>
        <input type="submit" class="elex_auspost_delete_manifests" value="Delete" >
    <?php endif;?>
</form>
<br><br>

<script type="text/javascript">
    jQuery(document).ready(function(){
        <?php if(empty($manifest_history)){?>
            jQuery('#elex_auspost_manifests_history_table').hide();
            jQuery('.elex_auspost_delete_manifests').hide();
        <?php }

        if(!empty($manifest_history) && sizeof($manifest_history) > 10){?>
            jQuery('#elex_auspost_manifests_history_table').paging({limit:10});
        <?php }else{?>
            jQuery('#elex_auspost_manifests_history_table').show();
            jQuery('#elex_auspost_manifest_paging_nav').hide();
        <?php } ?>
    });

</script>

<?php 

    /*Showing error and success notices*/
    $stored_shipping_ids_with_existing_manifests = get_option('elex_auspost_shipping_ids_with_existing_manifests');
    if($stored_shipping_ids_with_existing_manifests != ''){
        echo '<div class="error"><p>' . sprintf(__('Manifest already generated for the shipment id(s) %s', 'wf-shipping-auspost'), $stored_shipping_ids_with_existing_manifests) . '</p></div>';
        delete_option('elex_auspost_shipping_ids_with_existing_manifests');
    }

    $manifest_status = get_option('elex_auspost_manifest_generated');

    /* Showing success notice for manifest generation for provided orders */
    if($manifest_status){
        $manifest_orders_shipment_ids = get_option('elex_auspost_orders_shipments_for_generated_manifest');
        echo '<div class="notice notice-success"><p>' . sprintf(__('Manifest generated for the provided %s', 'wf-shipping-auspost'), $manifest_orders_shipment_ids) . '</p></div>';
        delete_option('elex_auspost_manifest_generated');
    }

    $stored_invalid_orders = get_option('elex_auspost_uneligible_orders');

    /* Showing error notice for invalid Order ids */
    if($stored_invalid_orders != ''){
        echo '<div class="error"><p>' . sprintf(__('MANIFEST NOT GENERATED, ORDERS NOT FOUND FOR THE ORDERS ID(S) %s', 'wf-shipping-auspost'), $stored_invalid_orders) . '</p></div>';
        delete_option('elex_auspost_uneligible_orders');
    }

    $stored_orders_with_no_shipments = get_option('elex_auspost_orders_with_no_shipments');

    /* Showing error notice for the Order ids for which the shipments are not created */
    if($stored_orders_with_no_shipments != ''){
        echo '<div class="error"><p>' . sprintf(__('MANIFEST NOT GENERATED, NO SHIPMENTS FOUND FOR THE ORDER(S) %s', 'wf-shipping-auspost'), $stored_orders_with_no_shipments) . '</p></div>';
        delete_option('elex_auspost_orders_with_no_shipments');
    }

    $stored_orders_with_no_label_ids = get_option('elex_auspost_orders_with_no_label_ids');

    /* Showing error notice for Order ids for which shipping labels are not generated */
    if($stored_orders_with_no_label_ids != ''){
        echo '<div class="error"><p>' . sprintf(__('MANIFEST NOT GENERATED, NO LABELS FOUND FOR THE ORDER(S) %s', 'wf-shipping-auspost'), $stored_orders_with_no_label_ids) . '</p></div>';
        delete_option('elex_auspost_orders_with_no_label_ids');
    }

    $auspost_settings = get_option('woocommerce_wf_australia_post_settings');
    $contract_api_key = $auspost_settings['api_key'];
    $contract_api_password = $auspost_settings['api_pwd'];
    $contract_api_account_number = $auspost_settings['api_account_no'];
    $contract_api_password_startrack = '';
    $contract_api_account_number_startrack = '';

    if(isset($auspost_settings['wf_australia_post_starTrack_rates_selected']) && ($auspost_settings['wf_australia_post_starTrack_rates_selected'] == true)){
        $contract_api_password_startrack = $auspost_settings['wf_australia_post_starTrack_api_pwd'];
        $contract_api_account_number_startrack = $auspost_settings['wf_australia_post_starTrack_api_account_no'];
    }

    $order_auspost_label_id = '';
    $manifest_download_method = (isset($auspost_settings['dir_download']) && $auspost_settings['dir_download'] =='yes') ? 'attachment' : 'inline';
    $contracted_api_mode = isset($auspost_settings['contracted_api_mode']) ? $auspost_settings['contracted_api_mode'] : 'test';

    /* MANIFEST GENERATION PART */

    if(!empty($_GET) && isset($_GET['print_manifest'])){

        $previously_generated_auspost_order_id = get_option("elex_auspost_current_order_id");
        $auspost_manifest_order_id = isset($_GET['auspost_order_id'])? $_GET['auspost_order_id']: '';
        $startrack_manifest_order_id = isset($_GET['startrack_order_id'])? $_GET['startrack_order_id']: '';

        if(isset($_GET['print_manifest']) && ($_GET['print_manifest'] == 'yes')){
            if($startrack_manifest_order_id != ''){
                if(isset($_GET['manifest_download_method_selected']) && ($_GET['manifest_download_method_selected'] == 'attachment')){
                    elex_auspost_print_order_summary($startrack_manifest_order_id, $contract_api_key, $contract_api_password_startrack, $contract_api_account_number_startrack, $contracted_api_mode, $manifest_download_method = 'attachment');
                }else if(isset($_GET['manifest_download_method_selected']) && ($_GET['manifest_download_method_selected'] == 'inline')){
                    elex_auspost_print_order_summary($startrack_manifest_order_id, $contract_api_key, $contract_api_password_startrack, $contract_api_account_number_startrack, $contracted_api_mode, $manifest_download_method = 'inline');
                }    
            }else{
                if(isset($_GET['manifest_download_method_selected']) && ($_GET['manifest_download_method_selected'] == 'attachment')){
                    elex_auspost_print_order_summary($auspost_manifest_order_id, $contract_api_key, $contract_api_password, $contract_api_account_number, $contracted_api_mode, $manifest_download_method = 'attachment');
                }else if(isset($_GET['manifest_download_method_selected']) && ($_GET['manifest_download_method_selected'] == 'inline')){
                    elex_auspost_print_order_summary($auspost_manifest_order_id, $contract_api_key, $contract_api_password, $contract_api_account_number, $contracted_api_mode, $manifest_download_method = 'inline');
                }
            }
        }

        return;
    }else if(!empty($_POST)){
        global $WooCommerce;
        global $wpdb;

        $manifest_generation_type = isset($_POST['manifest_generation_type'])? $_POST['manifest_generation_type']: '';
        $orders_ids = array();
        $uneligible_orders = '';
        $eligible_orders = array();
        $manifest_ids_to_delete = array();

        if(isset($_POST['elex_auspost_delete_manifest_id'])){
            $manifest_ids_to_delete = $_POST['elex_auspost_delete_manifest_id'];
            elex_auspost_delete_manifest_data($manifest_ids_to_delete);
        }else{
            if($manifest_generation_type == 'shipdate' && !empty($_POST['shipdate'])){
                $shipdate = $_POST['shipdate'];
                $query_string_to_get_postmeta_label_generated_dates = "SELECT `post_id`,`meta_value` FROM `".$wpdb->prefix."postmeta` WHERE `meta_key` LIKE 'wf_woo_australiapost_labelId_generation_date' ";

                $posts_meta_label_generated_id = $wpdb->get_results($query_string_to_get_postmeta_label_generated_dates, OBJECT);// OBJECT - result will be an object

                foreach($posts_meta_label_generated_id as $posts_meta){
                    if($posts_meta->meta_value == $shipdate){
                        $orders_ids[] = $posts_meta->post_id;
                    }
                }

                $order_validation_results = elex_auspost_validate_orders($orders_ids);

                if(is_array($order_validation_results) && !empty($order_validation_results)){
                    $uneligible_orders = $order_validation_results['invalid_orders'];
                    $eligible_orders = $order_validation_results['valid_orders'];
                }

                if($uneligible_orders != ''){
                    update_option("elex_auspost_uneligible_orders", $uneligible_orders);
                }

            }

            if($manifest_generation_type == 'all'){

                $query = new WC_Order_Query( array(
                    'limit'     => 30,
                    'orderby'   => 'date',
                    'order'     => 'DESC',
                    'return'    => 'ids',
                ) );
                $orders_ids = $query->get_orders();
            }

            if($manifest_generation_type == 'order_id' && !empty($_POST['order_id'])){
                $requested_orders_ids = $_POST['order_id'];
                $orders_ids = explode(',', $requested_orders_ids);

                $order_validation_results = elex_auspost_validate_orders($orders_ids);

                if(is_array($order_validation_results) && !empty($order_validation_results)){
                    $uneligible_orders = $order_validation_results['invalid_orders'];
                    $eligible_orders = $order_validation_results['valid_orders'];
                }

                if($uneligible_orders != ''){
                    update_option("elex_auspost_uneligible_orders", $uneligible_orders);
                }
            }

            $shipment_ids_array = array();
            $orders_with_no_shipment_created = '';
            $orders_with_no_label_generated = '';
            $order_shipment_ids_array = array();
            $startrack_shipment_ids = array();

            if(!empty($eligible_orders)){
                foreach($eligible_orders as $order_id){
                    $order = new WC_Order($order_id);
                    $shipment_ids = get_post_meta($order_id, 'wf_woo_australiapost_shipmentId', true);
                    $order_startrack_shipment_ids = get_post_meta($order_id, 'elex_auspost_startrack_shipment_ids', true);
                    if(!empty($order_startrack_shipment_ids)){
                        foreach($order_startrack_shipment_ids as $order_startrack_shipment_id){
                            $startrack_shipment_ids[] = $order_startrack_shipment_id;
                        }
                    }

                    $shipment_ids_for_order_id = array();
                    $order_meta_data = $order->get_meta_data();
                    $filtered_startrack_shipment_ids = array();
                    $filtered_auspost_shipment_ids = array();
                    if(is_array($shipment_ids) && !empty($shipment_ids)){
                        foreach($shipment_ids as $shipment_id){
                            $shipment_label_id = get_post_meta($order_id, 'wf_woo_australiapost_labelId'.$shipment_id, true);
                            if(!empty($shipment_label_id)){
                                $shipment_ids_array[] = $shipment_id;
                                $shipment_ids_for_order_id[] = $shipment_id;
                            }else{
                                $orders_with_no_label_generated = $orders_with_no_label_generated. $order_id . ',';
                            }
                        }
                    }else if(!empty($shipment_ids)){
                        $shipment_ids_array[] = $shipment_ids;
                        $shipment_ids_for_order_id[] = $shipment_ids;
                    }else{
                        $orders_with_no_shipment_created = $orders_with_no_shipment_created . $order_id . ',';
                    }

                    foreach($shipment_ids_for_order_id as $shipment_id_for_order_id){
                        $order_shipment_ids_array[$order_id]['shipment_ids'][] =  $shipment_id_for_order_id;   
                    }
                }

                $eligible_shipment_ids = array();

                if(!empty($shipment_ids_array)){
                    foreach($shipment_ids_array as $shipment_id){
                        $eligible_shipment_ids[] = $shipment_id;                 
                    }
                }else{
                    $orders_with_no_shipment_created = rtrim($orders_with_no_shipment_created, ',');
                    update_option("elex_auspost_orders_with_no_shipments", $orders_with_no_shipment_created);

                    if($orders_with_no_label_generated != ''){
                        $orders_with_no_label_generated = rtrim($orders_with_no_label_generated, ',');
                        update_option('elex_auspost_orders_with_no_label_ids', $orders_with_no_label_generated);
                    }
                }

                foreach($eligible_shipment_ids as $shipment_id){
                    if(!empty($startrack_shipment_ids) && in_array($shipment_id, $startrack_shipment_ids)){
                        $filtered_startrack_shipment_ids[] = $shipment_id;
                    }else{
                        $filtered_auspost_shipment_ids[] = $shipment_id;
                    }
                }

                update_option('creating_new_manifests_auspost_elex', true);

                if(!empty($filtered_startrack_shipment_ids)){
                    update_option('create_manifest_startarck_auspost_elex', true);
                    elex_auspost_generate_bulk_order_manifest($contract_api_key, $contract_api_password_startrack, $contract_api_account_number_startrack, $filtered_startrack_shipment_ids, $contracted_api_mode, $order_shipment_ids_array);
                    delete_option('create_manifest_startarck_auspost_elex');
                }

                update_option("manifest_generation_in_progress_auspost_elex", true);

                if(!empty($filtered_auspost_shipment_ids)){
                    elex_auspost_generate_bulk_order_manifest($contract_api_key, $contract_api_password, $contract_api_account_number, $filtered_auspost_shipment_ids, $contracted_api_mode, $order_shipment_ids_array);
                }

                delete_option('startrack_manifest_generated_auspost_elex');
                delete_option('manifest_generation_in_progress_auspost_elex');
                delete_option('creating_new_manifests_auspost_elex');

                wp_redirect(admin_url('/admin.php?page=auspost_manifest'));
                exit(); 
            }else{
                echo '<div class="error"><p>' . sprintf(__('NO ORDERS FOUND', 'wf-shipping-auspost')) . '</p></div>';
            }
        }
    }

    /* Delete manifests for provided manifest number */
    function elex_auspost_delete_manifest_data($manifest_ids_to_delete){
        $manifest_history = get_option('elex_auspost_manifest_history');
        $ids_delete_to_manifest = array();
        foreach($manifest_ids_to_delete as $manifest_ids){
            $manifest_ids_array = explode(',', $manifest_ids);
            foreach($manifest_ids_array as $manifest_id){
                $ids_delete_to_manifest[] = $manifest_id;
            }
        }
        
        foreach ($ids_delete_to_manifest as $id_delete_to) {
           foreach($manifest_history as $order_manifests_key => $order_manifests){
                foreach($order_manifests as $order_manifests_data){
                    if($order_manifests_data['id'] == $id_delete_to){
                        unset($manifest_history[$order_manifests_key]);
                        break;
                    }
                }
           }
        }

        update_option('elex_auspost_manifest_history', $manifest_history);
        wp_redirect(admin_url('/admin.php?page=auspost_manifest'));
    }

    /* Generates order summary for provided shipment ids */
    function elex_auspost_generate_bulk_order_manifest($contract_api_key, $contract_api_password, $contract_api_account_number, $shipment_ids, $contracted_api_mode, $order_shipment_ids_array){

        $service_base_url = 'https://digitalapi.auspost.com.au/test/shipping/v1/';

        $shipping_ids_with_existing_manifests = '';

        $user_ok = elex_auspost_user_permission();
        if (!$user_ok)
            return;

        if ($contracted_api_mode == 'live') {
            $service_base_url = str_replace('test/', '', $service_base_url);
        }

        $shipment_ids_array = array();

        foreach($shipment_ids as $shipment_id){
            $shipment_ids_array[]['shipment_id'] = $shipment_id;
        }

        $service_order_url = $service_base_url . 'orders';

        $info = array(
            'shipments' => $shipment_ids_array,
        );

        $rqs_headers = elex_auspost_buildHttpHeaders($info, $contract_api_key, $contract_api_password, $contract_api_account_number);

        $res = wp_remote_post($service_order_url, array(
            'method' => 'PUT',
            'httpversion' => '1.1',
            'headers' => $rqs_headers,
            'body' => json_encode($info)
        ));

        if (is_wp_error($res)) {
            $error_string = $res->get_error_message();
            update_option("manifest_generation_error_message", $error_string);
            wp_redirect(admin_url('admin.php?page=auspost_manifest'));
            exit;
        }

        $response_array = isset($res['body']) ? json_decode($res['body']) : array();

        $order_ids_shipment_ids_for_existing_manifest = '';

        if (!empty($response_array->errors)) {
            $response_error = (array)$response_array->errors;
            if(is_array($response_error) && !empty($response_error)){
                foreach($response_error as $response_error_element){
                    if($response_error_element->code = '44016' || $response_error_element->code = '44017'){
                        if(isset($response_error_element->context->shipment_id)){
                            $shipping_ids_with_existing_manifests = $shipping_ids_with_existing_manifests.','.$response_error_element->context->shipment_id . ',';
                        }
                    }
                }
            }

            if($shipping_ids_with_existing_manifests != ''){
                $shipping_ids_with_existing_manifests = ltrim($shipping_ids_with_existing_manifests, ',');
                $shipping_ids_with_existing_manifests = rtrim($shipping_ids_with_existing_manifests, ',');
                $shipping_ids_with_existing_manifests_array = explode(',', $shipping_ids_with_existing_manifests);
                foreach ($order_shipment_ids_array as $id => $shipments) {
                    foreach($shipments['shipment_ids'] as $shipment_id){
                        foreach($shipping_ids_with_existing_manifests_array as $shipping_ids_with_existing_manifests_element){
                            if($shipment_id == $shipping_ids_with_existing_manifests_element){
                                $order_ids_shipment_ids_for_existing_manifest .= ' '.$shipment_id. ',';     
                            }
                        }
                    }
                    $order_ids_shipment_ids_for_existing_manifest = rtrim($order_ids_shipment_ids_for_existing_manifest, ',');
                    $order_ids_shipment_ids_for_existing_manifest .= ' of Order id '.$id.",";
                }
                $order_ids_shipment_ids_for_existing_manifest = rtrim($order_ids_shipment_ids_for_existing_manifest, ',');
                update_option('elex_auspost_shipping_ids_with_existing_manifests', $order_ids_shipment_ids_for_existing_manifest);
                header("Refresh:0");
            }
        }
        
        $auspost_shipping_order_id = '';

        if(isset($response_array->order->order_id))
        {
            $order_ids_shipments_pairs_for_manifest_generated = '';
            $auspost_shipping_order_id = $response_array->order->order_id;
            $request_type_startrack = get_option('create_manifest_startarck_auspost_elex', false);
            if($request_type_startrack){
                update_option('startrack_manifest_generated_auspost_elex', true);
            }

            update_option("elex_auspost_current_order_id", $auspost_shipping_order_id);
            update_option('elex_auspost_manifest_generated', true);
            $manifest_generated_date = current_time('Y-m-d', 0);
            $request_type_startrack = get_option('create_manifest_startarck_auspost_elex', false);
            $order_shipment_number_array = array();
            foreach ($order_shipment_ids_array as $key => $order_shipment_id) {
                $order = new WC_Order($key);
                $order_shipment_number_array[$order->get_order_number()] = $order_shipment_id;
            }

            $current_manifest_data = array('id' => $auspost_shipping_order_id, 'data' => $order_shipment_number_array, 'date' => $manifest_generated_date, 'type' => $request_type_startrack? 'StarTrack': 'AusPost');
            $stored_manifest_history = get_option('elex_auspost_manifest_history');
            $manifest_history_new = array();
            $new_manifest_generation = get_option('creating_new_manifests_auspost_elex', false);
            $startrack_manifest_generated = get_option('startrack_manifest_generated_auspost_elex', false);
            $manifest_generation_progress = get_option('manifest_generation_in_progress_auspost_elex', false);
            if(!empty($stored_manifest_history)){
                if($new_manifest_generation){
                    $stored_manifest_history[count($stored_manifest_history)][] =  $current_manifest_data;
                }
                update_option('elex_auspost_manifest_history', $stored_manifest_history); 
            }else{
                $manifest_history_new[0][] = $current_manifest_data;
                update_option('elex_auspost_manifest_history', $manifest_history_new);
            }
           
            foreach ($order_shipment_ids_array as $id => $shipments) {
                foreach($shipments['shipment_ids'] as $shipment_id ){
                    $order_ids_shipments_pairs_for_manifest_generated .= 'for shipment(s) '.$shipment_id.',';
                }
                $order_ids_shipments_pairs_for_manifest_generated = rtrim($order_ids_shipments_pairs_for_manifest_generated, ',');
                $order_ids_shipments_pairs_for_manifest_generated .= ' of Order id '.$id.',';
            }
            $order_ids_shipments_pairs_for_manifest_generated = rtrim($order_ids_shipments_pairs_for_manifest_generated, ',');
            update_option('elex_auspost_orders_shipments_for_generated_manifest', $order_ids_shipments_pairs_for_manifest_generated);
        }else{
            update_option('elex_auspost_manifest_generated', false);
        }
        return;   
    }

    /* Print generated order summary */
    function elex_auspost_print_order_summary($auspost_shipping_order_id, $contract_api_key, $contract_api_password, $contract_api_account_number, $contracted_api_mode, $manifest_download_method){
        if($auspost_shipping_order_id != ''){
            $service_base_url = 'https://digitalapi.auspost.com.au/test/shipping/v1/';

            $get_order_summary_url = $service_base_url.'accounts/'.$contract_api_account_number.'/orders/'.$auspost_shipping_order_id.'/summary';

            if ($contracted_api_mode == 'live') {
                $get_order_summary_url = str_replace('test/', '', $get_order_summary_url);
            }

            $rqs_headers = array(
                'Authorization' => 'Basic ' . base64_encode($contract_api_key . ':' . $contract_api_password),
                'AUSPOST-PARTNER-ID' => 'ELEXTENSION-7752',
                'Account-Number' => $contract_api_account_number,
            );
                            
            $order_manifest_response = wp_remote_get($get_order_summary_url, array(
                         'headers' => $rqs_headers,
                )
            );

            $decoded_response_body = $order_manifest_response['body'];

            $file = 'Australia-Post-' . $auspost_shipping_order_id . '.pdf';
            header('Content-Type: application/pdf');
            header('Content-Disposition: '.$manifest_download_method.'; filename='.basename($file));
            ob_clean(); flush();
            print($decoded_response_body);
            exit;
        }else{
            echo '<div class="error"><p>' . sprintf(__('No Manifest found to print', 'wf-shipping-auspost')) . '</p></div>';
        }
    }

    /* Checking is the user a administrator or shop manager */
    function elex_auspost_user_permission() {
        // Check if user has rights to generate invoices
        $current_user = wp_get_current_user();
        $user_ok = false;
        if ($current_user instanceof WP_User) {
            if (in_array('administrator', $current_user->roles) || in_array('shop_manager', $current_user->roles)) {
                $user_ok = true;
            }
        }
        return $user_ok;
    }

    /* Building HTTP headers */
    function elex_auspost_buildHttpHeaders($request, $contract_api_key, $contract_api_password, $contract_api_account_number) {
        $a_headers = array(
            'Authorization' => 'Basic ' . base64_encode($contract_api_key . ':' . $contract_api_password),
            'content-type' => 'application/json',
            'AUSPOST-PARTNER-ID' => 'ELEXTENSION-7752',
            'Account-Number' => $contract_api_account_number
        );
        return $a_headers;
    }

    /* Validating WooCommerce orders*/
    function elex_auspost_validate_orders($orders_ids){
        global $wpdb;

        $valid_orders = array();
        $invalid_orders = '';

        foreach ($orders_ids as $order_id) {
            $results = $wpdb->get_results( "SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key = '_order_number' AND meta_value = ".$order_id);
            
            if(empty($results))
            {
                $order = wc_get_order( $order_id );
                if(empty($order))
                {
                    $invalid_orders = $invalid_orders. ' ' .$orders_id. ',';
                }
                else
                {
                    $valid_orders[] = $order_id;
                }
            }
            else
            {
                $valid_orders[] = $results[0]->post_id;;
            }
        }
        $output_data = array(
            'valid_orders' => $valid_orders,
            'invalid_orders' => $invalid_orders
        );

        return $output_data;
    }
?>