<?php get_header(); ?>

<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
			$wrapper_classes = is_page('size-chart')
				? 'c-sizechart o-wrapper t-align-center'
				: 'o-wrapper o-wrapper--small';

			$default_classes = 'c-default c-section--pad';
			if (is_cart()) {
				$default_classes = 'c-default c-section--pad c-cart';
			} elseif (is_checkout()) {
				$default_classes = 'c-section--pad c-checkout';
			}

		?>

		<?php $disable_store = get_field('disable_store', 'options');
		if ($disable_store && $disable_store['enable'] && (is_cart() || is_checkout()) ) : ?>
			<div class="<?php echo( $wrapper_classes); ?> c-default c-section--pad c-cms-content t-align-center">
				<h1><?php the_title() ?></h1>
				<p>
					<?php if ($disable_store['message']) : ?>
						<?php echo $disable_store['message']; ?>
					<?php else : ?>
						The store is currently closed.
					<?php endif; ?>
				</p>
				<p>&nbsp;</p>
			</div>
		<?php else : ?>
			<div class="<?php echo( $wrapper_classes); ?> c-cms-content">
				<div class="<?php echo( $default_classes); ?>">
					<h1 class="o-animateup"><?php the_title(); ?></h1>

					<?php if(is_page('size-chart')) :?>
						<div class="c-sizechart__container">
							<div class="c-sizechart__copy imp o-animateup"><?php the_content(); ?></div>
							<div class="c-sizechart__body">
								<?php get_template_part( 'partials/size-chart' ); ?>
							</div>
						</div>
					<?php else: ?>
						<div class="o-animateup"><?php the_content(); ?></div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
	<?php wp_link_pages(); ?>
	</article>
	<?php endwhile; ?>
</main>

<?php get_footer(); ?>