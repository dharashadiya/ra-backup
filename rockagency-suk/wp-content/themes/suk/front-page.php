<?php get_header(); ?>


<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<!-- STAGE SECTION -->
			<?php $video = get_field('story_section_background_video'); ?>
			<div class="c-home c-home__stage <?php echo ($video ? 'c-home--video' : '') ?>">
				<?php if ($video): ?>
					<div class="c-home__stage-video-wrap">
						<video class="c-home__stage-video centered" preload="metadata" autoplay loop playsinline poster="<?php echo ASSETS; ?>/img/landing-video-placeholder.jpg" muted>
							<source src="<?php echo $video['url'] ?>">
						</video>
					</div>
				<?php else : ?>
					<div class="c-home__stage__bg cover parallax-this" style="background-image:url(<?php echo get_field('stage_background') ;?>)"></div>
				<?php endif; ?>
 				<div class="c-home__stage__shade cover"></div>
 				<div class="o-wrapper">
 					<div class="c-home__stage__container centered t-align-center">
						<h1 class="u-alpha o-animateup"><?php the_title(); ?></h1>

 						<a title="Visit Shop page" class="o-btn o-btn--brand o-animateup" href="/shop">Explore the Collection</a>
 					</div>
 				</div>
			</div>

			<!-- PRODUCT SECTION -->
			<div class="c-home c-home__products o-animateup">
				<div class="o-wrapper o-wrapper--mini">
					<div class="imp o-animateup t-align-center"><?php the_content(); ?></div>
				</div>
				<?php echo do_shortcode('[featured_products per_page="-1" columns="2"]') ;?>

				<div class="c-home__products_link t-align-center o-animateup"><a title="Shop SUK Workwear collection" class="o-btn" href="/shop">Shop Our Range</a></div>
			</div>

			<!-- STORY SECTION -->
			<div class="c-home c-home__story">
			<div class="c-home__story__bg cover" style="background-image:url(<?php echo get_field('story_section_background') ;?>)"></div>
				<div class="o-wrapper">
					<div class="c-home__story__container t-align-center">
						<h1 class="u-alpha o-animateup"><?php the_field('story_section_title') ;?></h1>
						<div class="imp o-animateup"><?php the_field('story_section_copy') ;?></div>
						<a title="Check our story" class="o-btn o-btn--white o-animateup" href="/our-story">Read more</a>
					</div>
				</div>
			</div>
			<?php get_template_part( 'partials/post-meta' ); ?>
			<?php wp_link_pages(); ?>
		</article>
	<?php endwhile; ?>
</main>
<!-- <?php get_sidebar(); ?> -->


<?php get_footer(); ?>