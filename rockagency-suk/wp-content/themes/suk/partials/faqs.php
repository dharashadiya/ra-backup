<?php 
if (is_product()) {
	$faq_sections = get_field('faq_sections', get_page_by_title('Frequently Asked Questions')->ID);
} else {
	$faq_sections = get_field('faq_sections');
}
if ($faq_sections) :
	foreach ($faq_sections as $section) { ?>
	<div class="c-faq__section" id="<?php echo(stripString($section['section_title']) ); ?>">
		<?php if ($section['section_title']) : ?>
			<h2 class=" <?php echo (is_product() ? '' : 'o-animateup'); ?>"><?php echo $section['section_title']; ?></h2>
		<?php endif; ?>
		<?php foreach ($section['faqs'] as $faq) { ?>
			<div class="c-faq__section_each <?php echo (is_product() ? '' : 'o-animateup'); ?>">
				<h3><?php echo $faq['question']; ?></h3>
				<div class="answer"><?php echo $faq['answer']; ?></div>
			</div>
		<?php } ?>
	</div>
	<div class="c-faq__border o-animateup"></div>
	<?php }
endif;?>