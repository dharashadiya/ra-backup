<div class="o-wrapper t-align-center">
	<h1 class="c-section--overlay__heading">Size Chart</h1>
	<div class="c-section--overlay__copy imp"><?php echo get_post_field('post_content', get_page_by_title('Size Chart')->ID); ?></div>
	<div class="c-section--overlay__body">
		<!-- SIZE CHART -->
		<?php ;
			$size_chart_content = get_field('size_chart_content');
			$tables = array();

			foreach ($size_chart_content as $single_chart) {
				if ($single_chart['size_table']== 'Custom Table') {
					array_push($tables, get_field('size_chart_content'));
				} else if ($single_chart['size_table']== 'Short Body') {
					array_push($tables,get_field('short_body_chart', 'options'));
				} else if ($single_chart['size_table']== 'Long Body') {
					array_push($tables, get_field('long_body_chart', 'options'));
				}
			}

			if ($size_chart_content) {
				$size_content = '';
				foreach ($tables as $size_chart) {
					$heading = get_field('single_chart_title', 'options');
					// if (count($tables) > 1) {
					if (isset($size_chart[0]['heading'])) {
						$heading = $size_chart[0]['heading'];
					}
					$table = $size_chart[0]['chart'];
					$custom_table = '';

					if ($heading) {
						$size_content .= '<div class="c-sizechart__container">';
						$size_content .= '<h3>' . $heading . '</h3>';
					}
					if( $table && count( $table['body'] ) > 0 ) {
						$size_content .= '<div class="c-sizechart__body">';
						$size_content .= '<span class="cm">Measurements (cm)</span>';

						$custom_table .= '<table class="table-wrapper">';

						if( $table && count( $table['header'] ) > 1 ) {
							$custom_table .= '<tr>';
							foreach($table['header'] as $item) {
								$custom_table .= '<th>' . $item['c'] . '</th>';
							}
							$custom_table .= '</tr>';
						}

						foreach($table['body'] as $items) {
							$custom_table .= '<tr>';
							foreach($items as $item) {
								$custom_table .= '<td>' . $item['c'] . '</td>';
							}
							$custom_table .= '</tr>';
						}
						$custom_table .= '</table>';

						$size_content .= $custom_table;
						$size_content .= '</div>';
					}
					$size_content .= '</div>';

				}
				echo $size_content;
			}
		?>
	</div>
</div>


<!-- SIZING DIAGRAMS -->
<?php
	$sizing_diagrams = get_field('sizing_section', 'options');
	if ($sizing_diagrams) { ?>
		<div class="c-sizechart-diagram">
			<?php foreach ($sizing_diagrams as $key => $section) { ?>
				<div class="c-sizechart-diagram__container <?php echo ($key % 2 == 0 ? 'even' : 'odd' ); ?>">
				<div class="o-wrapper">
					<div class="o-layout o-layout--large">
						<div class="o-layout__item u-desk-one-half c-sizechart-diagram__content">
							<h3><?php echo $section['section_title']; ?></h3>
							<div class="t-small"><?php echo $section['section_main_copy']; ?></div>
						</div><div class="o-layout__item u-desk-one-half c-sizechart-diagram__image">
							<div class="o-layout o-layout--flush">
							<div class="o-layout__item u-lap-two-thirds c-sizechart-diagram__tag">
								<?php foreach ($section['image_tags'] as $tag) : ?>
									<div class="c-sizechart-diagram__image-tag">
										<p><span class="tag t-align-center"><?php echo $tag['tag']; ?></span> <span class="title"><?php echo $tag['title']; ?></span></p>
										<p class="t-small"><?php echo $tag['content']; ?></p>
									</div>
								<?php endforeach; ?>
							</div><div class="o-layout__item u-lap-one-third">
								<img src="<?php echo $section['image']['url']; ?>" alt="<?php echo $section['image']['alt']; ?>">
							</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			<?php } ?>
			<div class="product-link-container t-align-center">
				<a href="javascript:void(0)" class="product-link cs-overlay__close" title="Goto Product">Back to Product</a>
			</div>
		</div>
	<?php }
?>