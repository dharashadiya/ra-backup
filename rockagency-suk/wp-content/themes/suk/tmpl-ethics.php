<?php
/*
 * Template Name: Ethics page
 */
?>

<?php get_header(); ?>

<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<!-- STAGE SECTION -->
		<div class="c-ethics c-ethics__stage cs-stage t-align-center">
		<div class="c-ethics__stage__bg parallax-this cover" style="background-image:url(<?php echo get_field('stage_background') ;?>)"></div>
			<div class="o-wrapper">
				<div class="c-ethics__container">
					<h1 class="u-alpha o-animateup"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>

		<!-- CONTENT SECTION -->
		<div class="c-ethics c-ethics__content">
			<div class="o-wrapper o-wrapper--small o-animateup">
				<?php the_content(); ?>
			</div>
		</div>
		<?php wp_link_pages(); ?>
	</article>
	<?php endwhile; ?>
</main><?php // get_sidebar(); ?>

<?php get_footer(); ?>