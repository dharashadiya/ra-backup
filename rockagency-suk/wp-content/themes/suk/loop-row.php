<?php if ( have_posts() ) : //Start of loop ?>
	<div class="c-rows">
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="c-row c-row--child">
				<a href="<?php the_permalink(); ?>" class="c-row__img-wrap" title="Visit <?php the_title() ?>">
					<?php the_post_thumbnail('row-thumb', array('class' => 'c-row__img')); ?>
				</a>
				<div class="c-row__content">
					<h2 class="c-row__heading"><a title="visit <?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<div class="c-row__description"><?php echo excerpt($post->ID, 40); ?></div>
				</div>
			</div>
		<?php endwhile; // End the loop. ?>
	</div>
<?php endif; ?>