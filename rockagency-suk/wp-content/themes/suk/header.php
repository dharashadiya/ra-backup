<!DOCTYPE html>
<html class="no-js preload" <?php language_attributes(); ?> dir="ltr">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
	<title><?php wp_title(); ?> </title>

	<?php if(is_local_or_staging()) : ?>
		<meta name="robots" content="none" />
		<meta name="robots" content="noindex" />
		<meta name="googlebot" content="none" />
		<meta name="googlebot" content="noindex" />
	<?php endif; ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
	<meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
	<meta name="ajaxurl" content="<?php echo admin_url('admin-ajax.php'); ?>" id="Ajaxurl">
	<link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">

	<script src="<?php echo ASSETS; ?>/lib/jquery-1.11.3.js"></script>
	<?php wp_head(); ?>
	<?php include('partials/inlinescripts.min.php'); ?>

    <?php if(!is_local_or_staging()) : ?>
        <!-- Global site tag (gtag.js) - Google Ads: 777651881 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-
        777651881"></script>
        <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         gtag('config', 'AW-777651881');
		 gtag('config', '<?php echo GOOGLE_ANALYTICS ?>');
        </script>

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '699390283836044');
        fbq('track', 'PageView');
        </script>
        <noscript>
         <img height="1" width="1"
        src="https://www.facebook.com/tr?id=699390283836044&ev=PageView
        &noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
    <?php endif; ?>

</head>

<body <?php body_class('archive post-type-archive post-type-archive-product logged-in admin-bar woocommerce woocommerce-page customize-support'); ?>>
<?php
	$top = " ";
	if ( is_front_page() || get_page_template_slug() == 'tmpl-our-story.php' ) :
		$top = "c-top--special";
	else :
		$top = "c-top--default";
	endif;
?>
<div class="c-top <?php echo $top ;?>">
	<?php $banner = get_field('banner_strip', 'options'); ?>
	<?php if ($banner && $banner['activate'] && $banner['content']) : ?>
		<div class="c-top__banner">
			<a href="javascript:void(0);" class="c-top__banner-close"><?php svgicon('close', '0 0 30 30'); ?></a>
			<?php echo $banner['content']; ?>
		</div>
	<?php endif; ?>
	<header class="c-header">
		<div class="o-wrapper o-wrapper--full">
			<a href="#Main" class="c-skip" title="Skip to main content" style="opacity: 0;">Skip to main content</a>
			<?php
				$logo_wrap_class = "c-logo-wrap";
				if (!is_front_page()) {
					$logo_wrap_open = '<a class="' . $logo_wrap_class . ' is-link" href="/" rel="home" title="Back to home">';
					$logo_wrap_close = '</a>';
				} else {
					$logo_wrap_open = '<span class="' . $logo_wrap_class . '">';
					$logo_wrap_close = '</span>';
				}

				echo $logo_wrap_open;
				echo svgicon('logo', '0 0 128 64', 'c-header__logo');
				echo $logo_wrap_close;
			?>

			<a class="c-nav-toggle" title="mobile navigation link" href="javascript:void(0);">
				<span class="menu-lable"></span>
			</a>
			<nav class="c-site-nav" role="navigation">
				<div class="c-site-nav__primary">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'primary-menu',
						'container' => false,
						'menu_class' => 'c-nav c-nav--left',
						'menu_id' => 'menu'
					));
					?>
				</div>

				<div class="c-site-nav__secondary">
					<ul class="c-nav c-nav--right">
						<!-- <li class="c-nav-account"><a href="/my-account" title="View your account">Account</a></li> -->

						<li class="c-nav-cart clearfix"><a href="/bag" title="View Cart">Bag
							<?php
								global $woocommerce;
								if ($woocommerce->cart->cart_contents_count == 0 ) {
									echo ( '<div class="header-cart-count bag-empty"><span class="count"></span>' . get_svgicon('bag', '0 0 21 21') . '</div>');
								} else {
									echo ( '<div class="header-cart-count bag-full"><span class="count">(' . $woocommerce->cart->cart_contents_count . ')</span>' . get_svgicon('bag-full', '0 0 21 21') . '</div>' );
								}
							?>
						</a></li>
						<li class="c-nav-secondary <?php echo (is_page('faq') ? 'current-menu-item' : ''); ?>"><a title="View Shipping policy" href="/faq">Shipping</a></li>
						<li class="c-nav-secondary <?php echo (is_page('faq') ? 'current-menu-item' : ''); ?>"><a title="View Returns policy" href="/faq">Returns</a></li>
						<li class="c-nav-secondary <?php echo (is_page('privacy') ? 'current-menu-item' : ''); ?>"><a title="View Privacy Policy" href="/privacy">Privacy Policy</a></li>
						<li class="c-nav-secondary <?php echo (is_page('terms') ? 'current-menu-item' : ''); ?>"><a title="View Terms and conditions" href="/terms">Terms & Conditions</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</header>
</div>

<div class="c-content">