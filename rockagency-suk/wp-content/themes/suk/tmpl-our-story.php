<?php
/*
 * Template Name: Our Story page
 */
?>

<?php get_header(); ?>

<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<article <?php post_class(); ?>>
			<div class="c-story">
				<!-- STAGE SECTION -->
				<div class="c-story c-story__stage t-align-center">
				<div class="c-story__stage__bg parallax-this cover" style="background-image:url(<?php echo get_field('stage_background') ;?>)"></div>
					<div class="o-wrapper">
						<div class="c-story__container">
							<h1 class="u-alpha o-animateup"><?php the_title(); ?></h1>
							<div class="imp o-animateup"><?php the_content(); ?></div>
						</div>
					</div>
				</div>

				<!-- CONTENT SECTION -->
				<div class="c-story c-story__content">
					<div class="o-wrapper">
						<div class="content">
							<?php the_field('top_content'); ?>
						</div>
						<?php $i = 1 ; ?>
						<?php if ( have_rows('story_information') ) while ( have_rows('story_information') ) : the_row();
							$layout_class = 'even-item';
							$image = get_sub_field('story_image');
							if ($i % 2 !=0) {
								$layout_class = '';
							} ?><div class="o-layout o-layout--huge clearfix <?php echo $layout_class; ?>">
								<div class="o-layout__item u-lap-wide-two-fifths u-one-whole">
									<div class="c-story__container o-animateup">
										<h2><?php the_sub_field('story_title') ;?></h2>
										<img class="lazyload" alt="<?php echo $image['alt'] ;?>" src="<?php echo ASSETS?>/img/1x1.trans.gif" data-src="<?php echo $image['url'] ;?>">
										<div><?php the_sub_field('story_copy') ;?></div>
									</div>
								</div><div class="c-story__image o-layout__item u-lap-wide-three-fifths u-one-whole">
									<img class="lazyload o-animateup o-animation-delay-1" alt="<?php echo $image['alt'] ;?>" src="<?php echo ASSETS?>/img/1x1.trans.gif" data-src="<?php echo $image['url'] ;?>">
								</div>
							</div><?php 
							$i++;
						endwhile; ?>
					</div>
				</div>

				<!-- AMBASSADOR SECTION -->
				<div class="c-story c-story__ambassadors t-align-center">
					<div class="o-wrapper">
						<?php if ( have_rows('story_ambassadors') ) : ?>
							<div class="c-story__container o-animateup">
								<h2><?php the_field('story_ambassadors_title') ;?></h2>
								<div class="medium"><?php the_field('story_ambassadors_main_copy') ;?></div>
							</div>
							<div class="o-layout">
								<?php $i = 0;
								while ( have_rows('story_ambassadors') ) : the_row() 
									?><div class="o-layout__item u-lap-one-third u-one-whole">
										<div class="c-story__container o-animateup <?php echo ($i % 3 == 0 ? '' : ($i % 3 == 1 ? 'o-animation-delay-1' : 'o-animation-delay-2') );?>">
											<?php 
												$photo = get_sub_field('photo');
												$twitter = get_sub_field('twitter');
												$facebook = get_sub_field('facebook');
												$instagram = get_sub_field('instagram');
												$website = get_sub_field('website');
											?>
											<div class="c-story__ambassadors_img">
												<img class="lazyload" src="<?php echo ASSETS?>/img/1x1.trans.gif" data-src="<?php echo $photo['url'] ;?>" alt="<?php echo $photo['alt'] ;?>">
											</div>
											<div class="c-story__ambassadors_copy t-align-left">
												<h3 class="alt"><?php the_sub_field('name'); ?></h3>
												<p> <?php the_sub_field('short_intro') ;?> </p>
												<?php if($twitter)  { ?>
													<a class="c-story__ambassadors_link" target="_blank" href="<?php echo $twitter; ?>" title="Follow <?php the_sub_field('name'); ?> on twitter"><?php echo svgicon('twittericon', '0 0 27 27') ?></a>
												<?php } ?>

												<?php if($facebook)  { ?>
													<a class="c-story__ambassadors_link" target="_blank" href="<?php echo $facebook; ?>" title="Like <?php the_sub_field('name'); ?> on facebook"><?php echo svgicon('fbicon', '0 0 27 27') ?></a>
												<?php } ?>

												<?php if($instagram)  { ?>
													<a class="c-story__ambassadors_link" target="_blank" href="<?php echo $instagram; ?>" title="Follow <?php the_sub_field('name'); ?> on instagram"><?php echo svgicon('instaicon', '0 0 27 27') ?></a>
												<?php } ?>

												<?php if($website)  { ?>
													<a class="c-story__ambassadors_link" target="_blank" href="<?php echo $website; ?>" title="Visit <?php the_sub_field('name'); ?>'s website"><?php echo svgicon('websiteicon', '0 0 26 26') ?></a>
												<?php } ?>
											</div>
										</div>
									</div><?php $i++;
								endwhile; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php wp_link_pages(); ?>
		</article>
	<?php endwhile; ?>
</main>
<!-- <?php get_sidebar(); ?> -->

<?php get_footer(); ?>