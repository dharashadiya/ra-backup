<?php  get_header(); ?>

<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post() ; ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="c-cms-content o-wrapper o-wrapper--small">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
				<?php get_template_part( 'partials/post-meta' ); ?>
			</div>
			<?php wp_link_pages(); ?>
		</article>
	<?php endwhile;
	else : ?>
		<article>
			<div class="c-default c-section--pad">
				<div class="o-wrapper o-wrapper--small t-align-center">
					<h1>Page Not found</h1>
					<div class="c-cms-content t-imp">The content you are looking for not found. Please use navigation menu to go through the site.</div>
				</div>
			</div>
		</article>
	<?php endif; ?>
</main>
<!-- <?php get_sidebar(); ?> -->

<?php get_footer(); ?>