<?php
/**
 * Customized
 *
 *
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product_count;
$product_count = 0;
$layout = '';
if( is_front_page() ) {
	$layout = 'cs-carousel';
} ?>
<div class="c-shop">
	<div class="<?php echo $layout ;?> c-products columns-<?php echo esc_attr( wc_get_loop_prop( 'columns' ) ); ?>">
		<?php if (!is_front_page()) : ?>
			<div class="o-layout o-module o-layout--small">
		<?php endif;?>