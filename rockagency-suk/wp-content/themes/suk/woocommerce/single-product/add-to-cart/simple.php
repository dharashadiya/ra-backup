<?php
/**
 * Customized
 * 
 * 
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product );
?>

<?php if ( $product->is_in_stock() ) : ?>
	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo $product->id; ?>">
		<div class="c-product__sbox c-product__sbox--qty">
			<?php 
				/**
				 * @since 2.1.0.
				 */
				do_action( 'woocommerce_before_add_to_cart_button' );

				/**
				 * @since 3.0.0.
				 */
				do_action( 'woocommerce_before_add_to_cart_quantity' );
			?>
        	<p class="qty_lable o-animateup">Quantity</p>
			<div class="imp c-product__sbox__qty o-animateup">	
				<a title="Decrease quantity of <?php echo $product->get_title(); ?>" href="javascript:void(0);" class="qty_minus product-<?php echo $product->id ?>" data-product="<?php echo $product->id ?>"> - </a>
				
				<?php
					woocommerce_quantity_input( array(
						'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
						'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
						'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $product->get_min_purchase_quantity(),
					) );
				?>
				
				<a title="Increase quantity of <?php echo $product->get_title(); ?>" href="javascript:void(0);" class="qty_plus" data-product="<?php echo $product->id ?>"> + </a>
				<?php 
					/**
					 * @since 3.0.0.
					 */
					do_action( 'woocommerce_after_add_to_cart_quantity' );
				?>
			</div>
			<div class="c-product__sbox-total" style="display: none;">$<span id="totalAmount"></span> <span class="t-c-primary">Total</span></div>
		</div>
		

		<button type="submit"
			data-quantity="1"
			data-product="<?php echo $product->id; ?>"
			data-product_id="<?php echo $product->id; ?>"
			class="o-btn o-btn__cart c-product--addtocart button alt ajax_add_to_cart add_to_cart_button product_type_simple">
			Add To Bag
		</button>
		
		<?php
			/**
			 * @since 2.1.0.
			 */
			do_action( 'woocommerce_after_add_to_cart_button' );
		?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
