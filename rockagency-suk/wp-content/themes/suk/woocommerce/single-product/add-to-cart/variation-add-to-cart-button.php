<?php
/**
 * Customized
 * 
 * 
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<div class="c-product__sbox c-product__sbox--qty">
        
		<?php 
			/**
			 * @since 3.0.0.
			 */
			do_action( 'woocommerce_before_add_to_cart_quantity' );
		?>
		<style>
			.pwgc-subtitle {
				font-size: 13px !important;
				line-height: 1.465 !important;
				color: #767676 !important;
				margin-top: 5px;
			}
			.pwgc-field-container {
				margin: 14px 0;
			}
		</style>
		
		<p class="qty_lable o-animateup">Quantity</p>
		<div class="imp c-product__sbox__qty o-animateup">
			<a title="Decrease quantity of <?php echo $product->get_title(); ?>" href="javascript:void(0);" class="qty_minus product-<?php echo $product->id ?>" data-product="<?php echo $product->id ?>"> - </a>

			<?php
				woocommerce_quantity_input( array(
					'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
					'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
					'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $product->get_min_purchase_quantity(),
				) );
			?>
				
			<a title="Increase quantity of <?php echo $product->get_title(); ?>" href="javascript:void(0);" class="qty_plus" data-product="<?php echo $product->id ?>"> + </a>
			<?php 
				/**
				 * @since 3.0.0.
				 */
				do_action( 'woocommerce_after_add_to_cart_quantity' );
			?>
		</div>
			
		<div class="c-product__sbox-total" style="display: none;"><span class="t-c-primary">Total: </span>$<span id="totalAmount"></span></div>
	</div>

	<button type="submit"
		data-quantity="1" 
		data-product="<?php echo $product->id; ?>" 
		data-product_id="<?php echo $product->id; ?>" 
		class="o-btn o-btn__cart c-product--addtocart single_add_to_cart_button button alt" data-product="<?php echo get_the_ID(); ?>">
		Add To Bag
	</button>
	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>