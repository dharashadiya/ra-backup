<?php
/**
 * Email Styles
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-styles.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load colors.
$bg        = get_option( 'woocommerce_email_background_color' );
$body      = get_option( 'woocommerce_email_body_background_color' );
$base      = get_option( 'woocommerce_email_base_color' );
$base_text = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text      = get_option( 'woocommerce_email_text_color' );

// Pick a contrasting color for links.
$link_color = wc_hex_is_light( $base ) ? $base : $base_text;

if ( wc_hex_is_light( $body ) ) {
	$link_color = wc_hex_is_light( $base ) ? $base_text : $base;
}

$bg_darker_10    = wc_hex_darker( $bg, 10 );
$body_darker_10  = wc_hex_darker( $body, 10 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$base_lighter_40 = wc_hex_lighter( $base, 40 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
// body{padding: 0;} ensures proper scale/positioning of the email in the iOS native email app.
?>
body {
	padding: 0;
}

#wrapper {
	background-color: <?php echo esc_attr( $bg ); ?>;
	margin: 0;
	padding: 70px 0;
	-webkit-text-size-adjust: none !important;
	width: 100%;
}

#template_container {
	box-shadow: 0 5px 12px rgba(0, 0, 0, 0.1) !important;
	background-color: <?php echo esc_attr( $body ); ?>;
	border-radius: 3px !important;
}

#template_header {
	background-color: #f1eada;
	border-radius: 3px 3px 0 0 !important;
	color: #eb4221;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	vertical-align: middle;
	font-family: Arial, "Helvetica Neue", Helvetica, Roboto, sans-serif;
}

#template_header h1,
#template_header h1 a {
	color: #eb4221;
}

th {
	border: 0;
}

.td {
	color: #4c676d;
	border: 0;
	vertical-align: middle;
}

.dark {
	color: #11343d;
}

blockquote {
	margin: 8px 0;
	padding: 0;
	font-style: italic;
	color: #11343d;
	font-weight: bold;
}
#template_footer td {
	padding: 0;
	border-radius: 6px;
}

#template_footer #credit {
	border: 0;
	color: <?php echo esc_attr( $base_lighter_40 ); ?>;
	font-family: Arial, "Helvetica Neue", Helvetica, Roboto, sans-serif;
	font-size: 12px;
	line-height: 1.2;
	text-align: center;
	padding: 0 48px 48px 48px;
}

#body_content {
	background-color: <?php echo esc_attr( $body ); ?>;
}

#body_content .order-details {
	padding-bottom: 12px;
	border-bottom: 1px solid #d3d2d2;
}

#body_content table td {
	padding: 36px;
}

#body_content table td td {
	padding: 10px 0;
	text-transform: uppercase;
}

#body_content table td th {
	padding: 8px 0 0;
	border-bottom: 1px solid #e7ebec;
}

#body_content td ul.wc-item-meta {
	font-size: 11px;
	line-height: 1.2;
	margin: 0;
	padding: 0;
	list-style: none;
}

#body_content td ul.wc-item-meta li {
	margin: 0;
	padding: 0;
}

#body_content p {
	margin: 0;
}

#body_content_inner {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	font-family: Arial, "Helvetica Neue", Helvetica, Roboto, sans-serif;
	font-size: 13px;
	line-height: 1.2;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

#body_content .order-details {
	font-size: 12px;
	line-height: 1.2;
}

#body_content .order-details .footer-first-row .blank {
	padding: 0;
	border: 0;
	border-bottom: 1px solid #e7ebec;
	height: 0;
}

#body_content .email-footer {
	background-color: #a58449;
	color: rgba(255, 255, 255, .75);
	font-family: Arial, "Helvetica Neue", Helvetica, Roboto, sans-serif;
	font-size: 12px;
	line-height: 1.6;
	padding: 24px 36px 48px;
}

.address {
	padding: 0;
	border: 0;
	font-family: Arial, "Helvetica Neue", Helvetica, Roboto, sans-serif;
	font-weight: 100;
	font-style: normal;
}

.text {
	color: <?php echo esc_attr( $text ); ?>;
	font-family: Arial, "Helvetica Neue", Helvetica, Roboto, sans-serif;
}

#header_wrapper {
	padding: 44px 48px;
	display: block;
	text-align: center;
}

#header_wrapper img {
	width: 100px;
}

h1 {
	color: #a58449;
	font-family: "Times New Roman", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 25px;
	font-weight: bold;
	line-height: 1;
	margin: 0;
	margin-bottom: 6px;
	text-transform: uppercase;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

h2 {
	font-family: "Times New Roman", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 17px;
	font-weight: bold;
	line-height: 1.1;
	text-transform: uppercase;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

#body_content_inner h2 {
	color: #eb4221;
	margin-bottom: 18px;
}
.email-footer h2 {
	margin-top: 20px;
	margin-bottom: 6px;
	color: #fff;
}
h3 {
	color: #11343d;
	display: block;
	font-family: Arial, "Times New Roman", "Helvetica Neue", Helvetica, Roboto, sans-serif;
	font-size: 13px;
	font-weight: normal;
	text-transform: uppercase;
	letter-spacing: 1px;
	line-height: 1.3;
	margin: 16px 0 8px;
}

a {
	color: #11343d;
	font-weight: normal;
	text-decoration: underline;
}
.date {
	font-size: 10px;
	line-height: 1.2;
}
img {
	border: none;
	display: inline-block;
	font-size: 14px;
	font-weight: bold;
	height: auto;
	outline: none;
	text-decoration: none;
	text-transform: capitalize;
	vertical-align: middle;
	margin-<?php echo is_rtl() ? 'left' : 'right'; ?>: 10px;
}

#addresses a {
	color: rgba(255, 255, 255, .75);
}

.suk-email-image {
	width: 100%;
}
<?php
