<?php
/*
Customized


Copyright (C) 2016-2017 Pimwick, LLC

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

do_action( 'woocommerce_email_header', $email_heading, $email );

?>

<style type="text/css">
    .pwgc-section {
        font-family: Arial, "Helvetica Neue", Helvetica, Roboto, sans-serif;
        color: #333333;
        margin: 24px 0;
    }

    .pwgc-header,
    .pwgc-amount,
    .pwgc-card-number {
        color: #a58449;
        font-family: "Times New Roman", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
        font-size: 20px;
        font-weight: bold;
        line-height: 1;
        margin: 0;
        margin-bottom: 6px;
        text-transform: uppercase;
    }
    .pwgc-header {
        font-size: 25px !important;
    }
    .pwgc-amount {
        color: #000 !important;
    }
    .pwgc-message {
        margin-top: 24px;
    }

    .pwgc-message p,
    .pwgc-message span,
    .pwgc-message div {
        font-family: Arial, "Helvetica Neue", Helvetica, Roboto, sans-serif;
        font-size: 18px;
        line-height: 1.4;
        margin-bottom: 0;
    }
    .pwgc-gift-card-container {
        background-color: #FFF;
        padding: 24px 24px;
        margin-top: 24px;
        background: #fbf9f4;
    }
    .pwgc-amount-container {
        float: left;
        width: 30%;
    }
    .pwgc-card-number-container {
        float: left;
        width: 68%;
    }

    .pwgc-label {
        font-family: Arial, "Helvetica Neue", Helvetica, Roboto, sans-serif;
        font-size: 11px;
        line-height: 1.5;
        margin-bottom: 4px;
        color: #88999e;
    }
    .pwgc-redeem-button {
        display: inline-block;
        padding: 12px 24px;
        border-radius: 0;
        text-decoration: none;
        text-transform: uppercase;
        font-weight: bold;
        color: #FFF;
        font-family: "Times New Roman", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
        background-color: #eb4221;
        font-size: 14px;
        line-height: 1;
    }
</style>
<?php
    if ( !empty( $item_data->message ) ) {
        ?>
        <div class="pwgc-section pwgc-message">
            <?php echo nl2br( $item_data->message ); ?>
        </div>
        <?php
    }
?>

<div class="pwgc-gift-card-container">
    <div class="pwgc-section pwgc-header">
        <?php printf( __( '%s Gift Card', 'pw-woocommerce-gift-cards' ), get_option( 'blogname' ) ); ?>
    </div>
    <div class="clearfix">
        <div class="pwgc-section pwgc-amount-container">
            <div class="pwgc-label"><?php _e( 'Amount', 'pw-woocommerce-gift-cards' ); ?></div>
            <div class="pwgc-amount"><?php echo wc_price( $item_data->amount ); ?></div>
        </div>
    
        <div class="pwgc-section pwgc-card-number-container">
            <div class="pwgc-label"><?php _e( 'Gift Card Number', 'pw-woocommerce-gift-cards' ); ?></div>
            <div class="pwgc-card-number"><?php echo $item_data->gift_card_number; ?></div>
        </div>
    </div>

    <div class="pwgc-section">
        <a href="<?php echo $item_data->redeem_url; ?>" class="pwgc-redeem-button"><?php _e( 'Redeem Gift Card', 'pw-woocommerce-gift-cards' ); ?></a>
    </div>
</div>

<?php do_action( 'woocommerce_email_footer' ); ?>
