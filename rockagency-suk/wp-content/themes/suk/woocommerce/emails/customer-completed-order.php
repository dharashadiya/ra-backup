<?php
/**
 * Customized
 *
 *
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer first name */ ?>
<h2><?php printf( esc_html__( '%s %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ), esc_html( $order->get_billing_last_name() ) ); ?></h2>
<?php /* translators: %s: Site title */ ?>
<p><?php esc_html_e( 'We have finished processing your order.', 'woocommerce' ); ?></p>
<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

$tracking = get_post_meta( $order->id, 'TrackingNumber', true );
$trackingCarrier = get_post_meta( $order->id, 'CarrierName', true );

if ($tracking) : ?>
	<br>
	<h2 style="margin: 0; margin-bottom:10px;">Tracking Number</h2>
	<div style="margin:0;"> <?php echo $trackingCarrier; ?>
		<?php if ($trackingCarrier == 'Couriers Please') : ?>
			: <a href="https://www.couriersplease.com.au/tools-track/no/<?php echo $tracking; ?>" target="_blank"><?php echo $tracking; ?></a>
		<?php else : ?>
			: <a href="https://auspost.com.au/mypost/track/#/details/<?php echo $tracking; ?>" target="_blank"><?php echo $tracking; ?></a>
		<?php endif; ?>
	</div>
<?php endif;

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
