<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.4.0
 */

defined( 'ABSPATH' ) || exit;

wc_print_notices();

do_action( 'woocommerce_before_cart' );
global $attributes;
?>

<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<div class="c-cart__product shop_table shop_table_responsive cart woocommerce-cart-form__contents">
		<?php do_action( 'woocommerce_before_cart_contents' ); ?>
		<?php
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) :
			$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) :
				$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );

		?>

		<div class="c-cart__each cart-product-<?php echo $cart_item['product_id'] ?> o-module woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
			<div class="c-cart__thumb o-module__item">
				<a class="o-module__content product-thumbnail" title="Goto <?php echo get_the_title($cart_item['product_id']); ?>" href="<?php echo esc_url( $product_permalink ) ;?>" style="background-image: url(<?php echo get_the_post_thumbnail_url( $cart_item['product_id'], 'thumb' ); ?>)">
				</a>
			</div>
			<div class="c-cart__description o-module__item">
				<div class="o-module__content">
					<div class="c-cart__description__left">
						<!-- Name -->
						<a title="Goto <?php echo get_the_title($cart_item['product_id']); ?>" class="product-name" href="<?php echo esc_url( $product_permalink ) ;?>">
							<h3><?php echo get_the_title($cart_item['product_id']); ?></h3>
						</a>

						<!-- Variations -->
						<?php if ($cart_item['variation'] && count($cart_item['variation']) > 0) :
							foreach ($cart_item['variation'] as $variation => $value) {
								$attr_name = '';
								if (strpos($variation, 'pa') !== false ) {
									$attr_name = ucfirst(str_replace('-', ' ', str_replace('attribute_pa_', '', $variation ) ) );
								} else {
									$attr_name = ucfirst(str_replace('-', ' ', str_replace('attribute_', '', $variation ) ) );
								} ?>
								<div class="product-variation">
									<span class="title"><?php echo $attr_name; ?>:</span> <span class="value"><?php echo $value; ?></span>
								</div>
							<?php } ?>
						<?php endif; ?>

						<!-- Quantity -->
						<div class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
							<span class="title">Quantity: </span>

							<?php
								if ( $_product->is_sold_individually() ) :
									$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
								else :
									$product_quantity = woocommerce_quantity_input( array(
										'input_name'    => "cart[{$cart_item_key}][qty]",
										'input_value'   => $cart_item['quantity'],
										'max_value'     => $_product->get_max_purchase_quantity(),
										'min_value'     => '0',
										'product_name'  => $_product->get_name(),
									), $_product, false );
								endif;

								echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
							?>

							<a title="Decrease quantity of <?php echo get_the_title($cart_item['product_id']); ?>" href="javascript:void(0);" class="cs-cart qty_minus" data-product="<?php echo $cart_item['product_id'] ?>"> - </a>

							<a title="Increase quantity of <?php echo get_the_title($cart_item['product_id']); ?>"  href="javascript:void(0);" class="cs-cart qty_plus" data-product="<?php echo $cart_item['product_id']; ?>"> + </a>
						</div>
					</div>

					<!-- Price -->
					<div class="c-cart__description__right product-subtotal imp" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
						<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
						?>
					</div>
				</div>
			</div>
			<div class="product-remove">
				<?php
					echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
						'<a href="%s" title="Remove this product from cart" class="remove cover" aria-label="%s" data-product_id="%s" data-product_sku="%s"></a>',
						esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
						__( 'Remove this item', 'woocommerce' ),
						esc_attr( $product_id ),
						esc_attr( $_product->get_sku() )
					), $cart_item_key );
				?>
			</div>
		</div>
		<?php
			endif;
		endforeach;
		?>

		<?php do_action( 'woocommerce_cart_contents' ); ?>

		<div class="c-cart__actions o-layout o-layout--flush o-module">
			<div class="o-layout__item o-module__item u-lap-one-half">
				<?php if ( wc_coupons_enabled() ) : ?>
					<div class="c-cart__actions__left coupon o-module__content">
						<label for="coupon_code"><?php esc_html_e( 'Coupon Code', 'woocommerce' ); ?></label>
						<div class="form">
							<input type="text" name="coupon_code" class="input input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Enter Coupon Code', 'woocommerce' ); ?>" />
							<input type="submit" class="button input input-submit" name="apply_coupon" value="<?php esc_attr_e( 'Enter', 'woocommerce' ); ?>" />
						</div>
						<?php do_action( 'woocommerce_cart_coupon' ); ?>
					</div>
				<?php endif; ?>
			</div><div class="o-layout__item o-module__item u-lap-one-half">
				<div class="c-cart__actions__right o-module__content">
					<div class="clearfix">
						<span class="t-float-left">Sub Total</span>
						<span class="t-float-right"><?php wc_cart_totals_subtotal_html(); ?></span>
					</div>
					<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
						<div class="cart-discount clearfix coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
							<span class="t-float-left"><?php wc_cart_totals_coupon_label( $coupon ); ?></span>
							<span class="t-float-right" data-title="<?php echo esc_attr( wc_cart_totals_coupon_label( $coupon, false ) ); ?>"><?php wc_cart_totals_coupon_html( $coupon ); ?></span>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<button type="submit" class="button update-cart" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>
			<?php do_action( 'woocommerce_cart_actions' ); ?>

			<?php wp_nonce_field( 'woocommerce-cart' ); ?>

		<?php do_action( 'woocommerce_after_cart_contents' ); ?>

		<div class="c-cart__checkout t-align-center">
			<a title="Continue Checkout" href="<?php echo esc_url( wc_get_checkout_url() );?>" class="o-btn o-btn--checkout checkout-button button alt wc-forward">
					<?php esc_html_e( 'Continue Checkout', 'woocommerce' ); ?>
			</a>
			<a title="Goto Shop" href="/shop" class="shop-link">Back to Shop</a>
		</div>
	</div>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>

<?php do_action( 'woocommerce_after_cart' ); ?>