<?php
/**
 * Customized
 *
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
// do_action( 'woocommerce_before_main_content' );

$cat = esc_attr( sanitize_text_field($_GET['cat']) );
$cat = get_term_by( 'slug', $cat, 'product_cat' );

$terms = get_terms( 'product_cat', array(
    'hide_empty' => true,
) ); ?>
<div class="c-section--pad">
<div class="o-wrapper o-wrapper--small">
<?php if (is_shop()) : ?>
	<div class="c-shop-cats">
		<div class="o-layout">
			<?php foreach ($terms as $item) :
				$thumbnail_id = get_woocommerce_term_meta( $item->term_id, 'thumbnail_id', true );
				$image = wp_get_attachment_url( $thumbnail_id );
				?><div class="o-layout__item u-lap-wide-one-third u-x-wide-palm-one-half o-animateup">
					<a href="<?php echo SITE; ?>/product-category/<?php echo $item->slug; ?>" class="c-shop-cat">
						<div class="c-shop-cat__img" style="background-image: url(<?php echo $image ?>)"></div>
						<h2 class="c-shop-cat__title"><?php echo $item->name; ?></h2>
					</a>
				</div><?php
			endforeach; ?>
		</div>
	</div>
<?php else : ?>
<header class="woocommerce-products-header c-shop__header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
<?php

if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked wc_print_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	// do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 *
			 * @hooked WC_Structured_Data::generate_product_data() - 10
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
// do_action( 'woocommerce_sidebar' );
?>

<?php endif; ?>
</div><!-- c-section--pad -->
</div><!-- o-wrapper o-wrapper--small -->
<?php get_footer( 'shop' ); ?>
