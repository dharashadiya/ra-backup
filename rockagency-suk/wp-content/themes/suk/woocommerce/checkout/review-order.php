<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="shop_table woocommerce-checkout-review-order-table">
	<?php
		do_action( 'woocommerce_review_order_before_cart_contents' );

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>
				<div class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
					<div class="clearfix">
						<!-- Name -->
						<div class="product-name t-float-left">
							<?php echo get_the_title($cart_item['product_id']); ?>
						</div>

						<!-- Quantity -->
						<div class="product-quantity t-float-left">
							<?php if ($cart_item['quantity'] && $cart_item['quantity'] > 1) : ?>
							<span>&nbsp;x<?php echo $cart_item['quantity']; ?></span>
							<?php endif; ?>
						</div>

						<!-- Price -->
						<div class="product-total t-float-right">
							<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
						</div>
					</div>

					<!-- Variations -->
					<?php if ($cart_item['variation'] && count($cart_item['variation']) > 0) :
						foreach ($cart_item['variation'] as $variation => $value) {
							$attr_name = '';
							if (strpos($variation, 'pa') !== false ) {
								$attr_name = ucfirst(str_replace('-', ' ', str_replace('attribute_pa_', '', $variation ) ) );
							} else {
								$attr_name = ucfirst(str_replace('-', ' ', str_replace('attribute_', '', $variation ) ) );
							} ?>
							<div class="product-variation">
								<span><?php echo $attr_name; ?>: <?php echo $value; ?></span>
							</div>
						<?php } ?>

					<?php endif; ?>
				</div>
				<?php
			}
		}

		do_action( 'woocommerce_review_order_after_cart_contents' );
	?>

	<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
		<div class="shipping clearfix">
			<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>
			<?php wc_cart_totals_shipping_html(); ?>
			<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>
		</div>
	<?php endif; ?>

	<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
		<div class="cart-discount clearfix coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
			<div class="t-float-left"><?php wc_cart_totals_coupon_label( $coupon ); ?></div>
			<div class="t-float-right"><?php wc_cart_totals_coupon_html( $coupon ); ?></div>
		</div>
	<?php endforeach; ?>

	<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
		<div class="fee">
			<div><?php echo esc_html( $fee->name ); ?></div>
			<div><?php wc_cart_totals_fee_html( $fee ); ?></div>
		</div>
	<?php endforeach; ?>

	<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
		<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
			<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
				<div class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
					<div><?php echo esc_html( $tax->label ); ?></div>
					<div><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
				</div>
			<?php endforeach; ?>
		<?php else : ?>
			<div class="tax-total">
				<div><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></div>
				<div><?php wc_cart_totals_taxes_total_html(); ?></div>
			</div>
		<?php endif; ?>
	<?php endif; ?>

	<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

	<div class="order-total clearfix">
		<div class="t-float-left"><?php _e( 'Total', 'woocommerce' ); ?></div>
		<div class="t-float-right"><?php wc_cart_totals_order_total_html(); ?></div>
	</div>

	<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

	</div>
	</div>
