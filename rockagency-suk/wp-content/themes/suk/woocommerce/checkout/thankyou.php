<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php if(!is_local_or_staging()) : ?>
    <!-- Event snippet for Adwords Conversion conversion page -->
    <script>
     gtag('event', 'conversion', {
     'send_to': 'AW-777651881/gycWCJyCla0BEKmN6PIC',
     'transaction_id': ''
     });
    </script>
<?php endif; ?>

<div class="c-thankyou woocommerce-order">

	<?php if ( $order ) : ?>
	<?php
		$items = [];
		foreach($order->get_items() as $item) {
			$data = $item->get_data();
			$gtag_item_data = array(
				"id" => $data['id'],
				"name" => $data['name'],
				"quantity" => $data['quantity'],
				"price" => $data['total']
			);
			array_push($items, $gtag_item_data);
		}
		?>
	<script>
	if (typeof gtag === "function") {
	gtag('event', 'purchase', {
		"transaction_id": "<?php echo $order->get_id() ?>",
		"affiliation": "Suk Workwear",
		"value": <?php echo $order->get_total() ?>,
		"currency": "<?php echo $order->get_currency() ?>",
		"tax": <?php echo $order->get_total_tax() ?>,
		"shipping": <?php echo $order->get_shipping_total() ?>,
		"items": <?php echo json_encode($items) ?>
	});
	}

	</script>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed o-animateup"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions o-animateup">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>
			<div class="o-layout o-layout--large">
				<div class="o-layout__item u-lap-one-half c-thankyou__header o-animateup">
					<h2 class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); ?></h2>
					<span class="order-receive-text"> You will receive an email with your order details </span>
				</div><div class="o-layout__item u-lap-one-half c-thankyou__order">
					<div class="order-details">
						<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
							<li class="woocommerce-order-overview__order order o-animateup">
								<h5 class="title"><?php _e( 'Order number:', 'woocommerce' ); ?> </h5>
								<h5 class="value"><?php echo $order->get_order_number(); ?></h5>
							</li>

							<li class="woocommerce-order-overview__date date o-animateup">
								<h5 class="title"><?php _e( 'Date:', 'woocommerce' ); ?> </h5>
								<h5 class="value"><?php echo wc_format_datetime( $order->get_date_created() ); ?></h5>
							</li>

							<?php if ( $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
								<li class="woocommerce-order-overview__email email o-animateup">
									<h5 class="title"><?php _e( 'Email:', 'woocommerce' ); ?> </h5>
									<h5 class="value"><?php echo $order->get_billing_email(); ?></h5>
								</li>
							<?php endif; ?>

							<li class="woocommerce-order-overview__total total o-animateup">
								<h5 class="title"><?php _e( 'Total:', 'woocommerce' ); ?> </h5>
								<h5 class="value"><?php echo $order->get_formatted_order_total(); ?></h5>
							</li>

							<?php if ( $order->get_payment_method_title() ) : ?>
								<li class="woocommerce-order-overview__payment-method method o-animateup">
									<h5 class="title"><?php _e( 'Payment method:', 'woocommerce' ); ?> </h5>
									<h5 class="value"><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></h5>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<?php // do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received o-animateup"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

	<?php endif; ?>

</div>
