<?php
/**
 * Customized
 *
 *
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
global $product_count;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$layout_class = "o-layout__item u-lap-wide-one-third u-x-wide-palm-one-half o-module__item";
if (is_front_page()) {
	$layout_class = "cs-carousel-cell";
}
$coming_soon = get_field('coming_soon', $product->ID);
$categories = 'cat-all';
$terms = get_the_terms( get_the_ID(), 'product_cat' );
foreach ($terms as $term) {
    $categories .= ' cat-' . $term->slug;
}
?>
<div class="c-products__each cs-product-each <?php echo $layout_class; ?> <?php echo $categories; ?>  o-animateup">
	<a title="View <?php the_title() ?>" href="<?php echo get_post_permalink($product->id) ?>" class="c-products__item cs-slide product-image o-module__content" data-slug="<?php echo $product->slug; ?>" data-product="<?php echo $product->id ?>">
		<div <?php wc_product_class( 'c-products__item_each', $product ); ?>>
			<?php
			/**
			 * woocommerce_before_shop_loop_item hook.
			 *
			 * @hooked woocommerce_template_loop_product_link_open - 10
			 */
			// do_action( 'woocommerce_before_shop_loop_item' );

			/**
			 * woocommerce_before_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );

			$hover_img = get_field('product_hover_image');
			if (!$hover_img['url']) {
				$hover_img['url'] = get_the_post_thumbnail_url();
			}
			?>

			<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title() ?> Image" class="product-img cover" />
			<?php if ($coming_soon) :?>
			<div class="coming-soon-hover cover">
				<img src="<?php echo ASSETS ?>/img/Suk-ComingSoon-Graphic.png" alt="Coming soon">
			</div>
			<?php else : ?>
			<img src="<?php echo $hover_img['url']; ?>" alt="<?php the_title() ?> Image" class="product-img-hover cover">
			<?php endif; ?>

			<?php if (!$coming_soon) : ?>
			<div class="c-products__info">
				<?php
					/**
					 * woocommerce_shop_loop_item_title hook.
					 *
					 * @hooked woocommerce_template_loop_product_title - 10
					 */
					do_action( 'woocommerce_shop_loop_item_title' );

				?>
				<?php

					/**
					 * woocommerce_after_shop_loop_item_title hook.
					 *
					 * @hooked woocommerce_template_loop_rating - 5
					 * @hooked woocommerce_template_loop_price - 10
					 */
					if ($product->is_type( 'variable' )) {
						echo ('<h3 class="price"> <span class="woocommerce-Price-amount" >$' . $product->get_variation_price('min') . '</span></h3>');
					} else {
						do_action( 'woocommerce_after_shop_loop_item_title' );
					}

					/**
					 * woocommerce_after_shop_loop_item hook.
					 *
					 * @hooked woocommerce_template_loop_product_link_close - 5
					 * @hooked woocommerce_template_loop_add_to_cart - 10
					 */
					// do_action( 'woocommerce_after_shop_loop_item' );
				?>
			</div>
			<?php endif; ?>
		</div>
	</a>
</div>
<?php $product_count++; ?>