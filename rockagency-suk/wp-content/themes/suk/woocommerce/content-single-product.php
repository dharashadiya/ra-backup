<?php
/**
 * Customized
 *
 *
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */


if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

if (is_product()) :
	$item = array(
		'id' => $product->sku,
		'name' => $product->name,
		'price' => $product->price,
	); ?>
	<script>
		if (typeof gtag === "function") {
			gtag('event', 'view_item', { "items": [ <?php echo json_encode($item); ?> ] });
		}
	</script>
<?php endif; ?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'c-product c-section--pad', $product ); ?> >
<div class="o-wrapper c-singleProduct">
<div class="o-layout o-layout--large">
	<div class="o-layout__item u-lap-one-half u-one-whole o-animateup">
		<?php
			$attachment_ids = $product->get_gallery_attachment_ids();
			$thumb = get_the_post_thumbnail_url();
			$coming_soon = get_field('coming_soon');

			if (count($attachment_ids)) :
				$image_links = array($thumb);
				foreach( $attachment_ids as $attachment_id ) {
					$image_link = wp_get_attachment_url( $attachment_id );
					$image_links[] = $image_link;
				}
			?>
				<div class="c-product-slider__wrapper">
					<div class="c-product-slider" data-cycle-pager-template="<a title='Pager for <?php echo get_the_title();?>' href='#'><img src='{{href}}' alt='{{title}}'></a>" >
						<?php foreach ($image_links as $image) : ?>

							<a title="Images for <?php echo get_the_title(); ?>" href="<?php echo $image; ?>" rel="product-image" class="fancybox c-product-slide" style="background-image: url('<?php echo $image; ?>');">
							<?php if ($coming_soon) :?>
								<div class="c-product-slider__coming-soon cover">
									<img src="<?php echo ASSETS ?>/img/Suk-ComingSoon-Graphic.png" alt="Coming soon">
								</div>
							<?php endif; ?>
							</a>
						<?php endforeach ?>
						<div class="c-product-slider__prev cycle-prev"><span></span></div>
						<div class="c-product-slider__next cycle-next"><span></span></div>
						<div class="c-product-slider__pager cycle-pager"></div>
					</div>
				</div>
			<?php else :
				echo '<a title="Images for ' . get_the_title() . '" href="' . $thumb . '" rel="product-image" class="fancybox lazyload c-product-slide" data-src="' . $thumb . '"></a>';
			endif ?>

			<?php // do_action( 'woocommerce_product_thumbnails' ); ?>

	</div><div class="o-layout__item u-lap-one-half u-one-whole">

		<div class="summary entry-summary c-product--description">
			<?php
				/**
				 * Hook: woocommerce_before_single_product_summary.
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				// do_action( 'woocommerce_before_single_product_summary' );
			?>

			<?php
				/**
				 * Hook: Woocommerce_single_product_summary.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 * @hooked WC_Structured_Data::generate_product_data() - 60
				 */

				$disable_store = get_field('disable_store', 'options');
				if ($coming_soon || ($disable_store && $disable_store['enable'])) {
					remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
				}
				do_action( 'woocommerce_single_product_summary' );
			?>

			<?php
				/**
				 * Hook: woocommerce_after_single_product_summary.
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_upsell_display - 15
				 * @hooked woocommerce_output_related_products - 20
				 */
				// do_action( 'woocommerce_after_single_product_summary' );
			?>


			<?php if (!$coming_soon) : ?>
				<?php do_action( 'woocommerce_before_single_product' ); ?>
				<div class="extra-link o-animateup">
					<a href="javascript:void(0)" title="View size chart" class="o-btn o-btn--sizeChart cs-open-overlay" data-overlay="sizeChart"><?php echo svgicon('sizechart', '0 0 16 16'); ?>Size Chart</a>
					<a href="javascript:void(0)" title="Check Delivery & returns policy" class="cs-open-overlay delivery" data-overlay="faqs">Delivery &amp; Returns</a>
				</div>
			<?php endif; ?>
			<a href="/shop" class="shop-link o-animateup" title="Back to shop">Back to Shop</a>
		</div>
	</div>
</div>
</div>
</div>

<?php
$all_products = array();
if( $product->is_type( 'simple' ) ){
	$all_products[get_the_ID()] = array(
		'type' => 'simple',
		'name' => get_the_title(),
		'price' => $product->price,
		'reg_price' => $product->regular_price,
		'sale_price' => $product->sale_price,
	);

} elseif( $product->is_type( 'variable' ) ){
	$product_variations = $product->get_available_variations();
	$variations = array();
	foreach ($product_variations as $variation) {
		foreach ($variation['attributes'] as $attributes => $val) {
			$name = $val;
		}
		$variations[$name] = array(
			'price' => $variation['display_price'],
			'display_regular_price' => $variation['display_regular_price']
		);
	}
	$all_products[get_the_ID()] = array(
		'type' => 'variable',
		'name' => get_the_title(),
		'slug' => $product->slug,
		'price' => $product->price,
		'variations' => $variations,
		'reg_price' => $product->regular_price,
		'sale_price' => $product->sale_price,
	);
}
?>
<script>var allProducts = <?php echo json_encode($all_products); ?>;
</script>

<?php do_action( 'woocommerce_after_single_product' ); ?>

<!-- SIZE CHART -->
<div class="cover c-sizechart c-section--overlay cs-overlay sizeChart" style="display: none;">
	<a href="javascript:void(0)" title="close Size Chart" class="c-section--overlay__close cs-overlay__close"><?php echo svgicon('close', '0 0 30 30'); ?></a>
	<div class="c-section--overlay__container">
		<?php get_template_part( 'partials/size-chart' ); ?>
	</div>
</div>
