<?php
/*
 * Template Name: Contact Page
 */
?>

<?php get_header(); ?>

<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="c-contact c-section--pad c-cms-content">
			<div class="o-wrapper">
				<div class="o-layout">
					<div class="o-layout__item u-lap-one-half">
						<div class="c-contact__left">
							<h1 class="o-animateup"><?php the_title(); ?></h1>
							<div class="imp o-animateup"><?php the_content(); ?></div>
							<?php 
								$email = get_field('email','options');
								$phone = get_field('phone','options'); 
							?>
							<?php if ($email) { ?>
								<div class="o-animateup">
									<span>e:&nbsp;</span><a title="Email us on <?php echo $email;?>" class="email" href="mailto:<?php echo $email;?>"><?php echo $email;?></a>
								</div>
							<?php }
							if ($phone) { ?>
								<div class="o-animateup">
									<span>p:&nbsp;</span><a title="call us on <?php echo $phone;?>" class="phone" href="tel:<?php echo $phone;?>"><?php echo $phone;?></a>
								</div>
							<?php } ?>

							<div class="c-footer__social">
								<ul>
									<!-- <li class="o-animateup"><a title="Follow us on twitter" target="_blank" href="https://twitter.com/sukworkwear" rel="noopener"><?php echo svgicon('twittericon', '0 0 27 27') ; ?></a></li> -->
									<li class="o-animateup"><a title="Like us on Facebook" target="_blank" href="https://www.facebook.com/sukworkwear/" rel="noopener"><?php echo svgicon('fbicon', '0 0 27 27') ; ?></a></li>
									<li class="o-animateup"><a title="Follow us on Instagram" target="_blank" href="https://www.instagram.com/sukworkwear/" rel="noopener"><?php echo svgicon('instaicon', '0 0 27 27') ; ?></a></li>
								</ul>							
							</div>
						</div>
					</div><div class="o-layout__item u-lap-one-half">
						<div class="c-contact__right o-animateup o-animation-delay-1">
							<?php echo do_shortcode(get_field('contact_form','options')); ?>
						</div>
					</div>
				</div>
			</div>
			<!-- <?php get_template_part( 'partials/post-meta' ); ?> -->
		</div>
	<?php wp_link_pages(); ?>
	</article>
	<?php endwhile; ?>
</main><?php // get_sidebar(); ?>

<?php get_footer(); ?>