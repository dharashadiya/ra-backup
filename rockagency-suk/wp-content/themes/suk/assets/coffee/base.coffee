( ($) ->
	window.console or= {log: -> }
	window.site or= {}

	$.extend site,
		init_vars: ->
			# Global variables -> use as site.VariableName in other files
			@body = $('body')
			@root = $('html')
			@header = $('#Top')
			@main = $('#Main')
			@side = $('#Secondary')
			@siteurl = $('#Siteurl').attr('content')
			@themeurl = $('#Themeurl').attr('content')

		onready: ->
			@init_vars()
			# @stopPreloadTransitions()
			@topBanner()
			@pageInOut()
			@menuEffects()
			@menuToggle()
			@fadeUpAnimation()
			@gallery()
			if $('.parallax-this').length > 0
				@parallax()

		onload: ->
			@lazyLoad()
			if $('.woocommerce').length > 0
				@productFilter()
				@expandDescription()
				@addItem()
				@addToCart()
				@addBagToCart()
				@updateCart()
			@openCloseOverlay()
			# @customSelect()
			@slider()
			if $(".cs-carousel").length > 0
				@flickitySlider()


		stopPreloadTransitions: ->
			$('html, body').fadeOut()
			$('html, body').removeClass 'preload'
			$('html, body').fadeIn()

		topBanner: ->
			bannerPopped = Cookies.get('bannerPopped')
			if $('.c-top__banner').length > 0 && !bannerPopped
				setTimeout (->
					$('.c-top__banner').show(200)
					if $('.c-section--pad').length > 0
						setTimeout (->
							$('.c-section--pad').animate({'padding-top': $('.c-top').height() + 80 + 'px' })
						), 200
				), 1000

			$(document).on 'click', '.c-top__banner-close', ->
				$('.c-top__banner').hide(200)
				date = new Date()
				date.setTime(date.getTime() + (15 * 60 * 1000))
				Cookies.set('bannerPopped', 'true', { expires: date })
				if $('.c-section--pad').length > 0
					setTimeout (->
						$('.c-section--pad').animate({'padding-top': $('.c-top').height() + 80 + 'px' })
					), 200

		pageInOut: ->
			setTimeout (->
				$('body').fadeIn(500)
			), 200

			# Checks if ctrl(windows) / cmd(mac) is pressed
			cntrlOrCmdNotPressed = true
			$(document).keydown (event) ->
				if event.which == 17 || event.which == 91
					cntrlOrCmdNotPressed = false
			$(document).keyup ->
				cntrlOrCmdNotPressed = true

			$(document).on 'click', 'a', (e) ->
				animate = false
				url = $(this).attr('href')
				comp = new RegExp(location.host)

				if url.startsWith('mailto:') || url.startsWith('tel:') || url.startsWith('javascript') || url.includes('remove_item') || $(this).attr('target') == '_blank' || $(this).hasClass('fancybox')
					animate = false
				else if url && url == '/'
					url = site.siteurl
					animate = true
				else if url && url.includes(site.siteurl)
					animate = true
				else if url && url.startsWith('/')
					url = site.siteurl + url

				if animate && cntrlOrCmdNotPressed && comp.test(url)
					e.preventDefault()
					setTimeout (->
						$('body').fadeOut(500)
						window.location.href = url
					), 300

		menuEffects: ->
			fadeStart = 10
			fadePace = 200
			$(window).scroll ->
				scrollTop = $(window).scrollTop()
				bg = 0
				if scrollTop < fadeStart
					bg = 0
				else if scrollTop > fadeStart
					bg = 0.6 * scrollTop / fadePace
					if bg > 1
						bg = 1
				$('.c-top').css('background-color', 'rgba(255, 255, 255,' + bg + ')')

			# Hide Header on on scroll down
			didScroll = undefined
			lastScrollTop = 0
			delta = 5
			navbarHeight = $('.c-top').outerHeight()

			hasScrolled = ->
				st = $(this).scrollTop()
				if st < navbarHeight
					$('.c-top').removeClass('nav-up nav-down')
				else
					if Math.abs(lastScrollTop - st) <= delta
						return
					if st > lastScrollTop and st > navbarHeight
						$('.c-top').removeClass('nav-down').addClass('nav-up')
					else
						if st + $(window).height() < $(document).height()
							$('.c-top').removeClass('nav-up').addClass('nav-down')
				lastScrollTop = st

			$(window).scroll (event) ->
				didScroll = true

			setInterval (->
				if didScroll
					hasScrolled()
					didScroll = false
			), 250

		menuToggle: ->
			$(document).on 'click', '.c-nav-toggle', ->
				$('html, body').toggleClass('scroll-lock')
				$('.c-top, .menu-lable').toggleClass('menu-toggle')

		productFilter: ->
			$(document).on 'click', '.cs-openClose-filter', ->
				$('.cs-filter').toggleClass('is-open')

			$(document).on 'click', '.cs-cat-filter', ->
				$('.cs-product-each').fadeOut(250)
				$('.cs-cat-filter').removeClass('is-active')
				$('.cs-filter').removeClass('is-open')
				$(this).addClass('is-active')
				category = $(this).attr('data-cat')
				$('.cs-current-filter').html(category)
				setTimeout (->
					$('.cs-product-each.cat-' + category).fadeIn(500)
				), 300

		addTotalAmount = (product) ->
			setTimeout (->
				if allProducts[product] && allProducts[product].type == 'variable'
					selectedVal = $('.c-product#product-' + product + ' .woocommerce-variation-price .woocommerce-Price-amount').html()

					# If same variation price to all products, woo doesn't calculate single variation price. So get normal price for further calculations
					if !selectedVal
						selectedVal = $('.c-product#product-' + product + ' .price .woocommerce-Price-amount').html()

					if selectedVal
						totalAmount = parseInt( selectedVal.replace('<span class="woocommerce-Price-currencySymbol">$</span>', '') ) * parseInt($('input.product-qty.product-' + product).val())

				else if allProducts[product] && allProducts[product].type == 'simple'
					totalAmount = allProducts[product].price * $('.c-product#product-' + product + ' input.product-qty').val()

				if totalAmount
					$('.c-product#product-' + product + ' #totalAmount').html(totalAmount.toFixed(2))
					$('.c-product#product-' + product + ' .c-product__sbox-total').show()
			), 200

		expandDescription: ->
			$(document).on 'click', '.short_desc_expand', ->
				$('.woocommerce-product-details__short-description').toggleClass('is-open')
				if ( $(this).html() == 'Read More' )
					$(this).html("Read Less")
				else
					$(this).html("Read More")


		getParameterByName: (name) ->
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]")
			regex = new RegExp("[\\?&]" + name + "=([^&#]*)")
			results = regex.exec(location.search)
			if results == null then "" else decodeURIComponent(results[1].replace(/\+/g, " "))

		addItem: ->
			$(document).on 'click', '.qty_plus, .qty_minus', ->
				product = $(this).data('product')
				# On cart page
				if $(this).hasClass('cs-cart')
					item = $(this).parent().find('input.product-qty')
					qty = parseInt($(item).val())
					if $(this).hasClass('qty_plus')
						$(item).val(qty + 1)
					else
						$(item).val(qty - 1)
					$('.update-cart').prop('disabled', false).trigger 'click'

				# Internal Products page
				else
					item = $('input.product-qty.product-' + product)
					qty = parseInt($(item).val())
					if $(this).hasClass('qty_plus')
						$(item).val(qty + 1)
						$('button[data-product_id=' + product + ']').attr('data-quantity', qty + 1 )
					else
						$(item).val(qty - 1)
						$('button[data-product_id=' + product + ']').attr('data-quantity', qty - 1 )
						if qty <= 1
							$(item).val(1)
					addTotalAmount(product)

		addToCart: ->
			$('form.cart').on 'change', 'input.qty', ->
				if $(this).attr('data-product-id')
					product = $(this).attr('data-product-id')
					addTotalAmount(product)
				if @value == '0'
					@value = '1'
				$(@form).find('button[data-quantity]').data 'quantity', @value
				return

			$('form.cart').on 'change', 'select', ->
				if $(this).siblings('span[data-select-product]')
					product = $(this).siblings('span[data-select-product]').attr('data-select-product')
					addTotalAmount(product)


		addBagToCart: ->
			$(document).on 'click', '.c-product--addtocart', ->
				product = undefined
				price = undefined
				if !$(this).hasClass('disabled')
					product = $(this).attr('data-product')
					price = $('form[data-product_id="' + product + '"] #totalAmount').text()

					if !price
						price = allProducts[product].price

					setTimeout (->
						quantity = $('.header-cart-count').html()
						$('.product-count').html quantity
					), 1000
					setTimeout (->
						$('.bag_on_overlay').addClass('is-open')
					), 1500

				setTimeout (->
					$('.c-product--addtocart').removeClass('added')
				), 4000

		updateCart: ->
			setInterval (->
				$('.product-quantity .quantity input.qty').on 'keyup', ->
					event.preventDefault
					if( event.key != "Backspace" ) && ( event.key != "Delete" )
						$('.update-cart').trigger 'click'
			), 500

		openCloseOverlay: ->

			openOverlay = (overlay) ->
				$('.cs-overlay.' + overlay).show(0).addClass('is-open')
				$('html, body').addClass('scroll-lock')
				$('.c-product').addClass('is-blured')

			hash = window.location.hash
			if hash == '#sizeChart'
				openOverlay('sizeChart')

			$(document).on 'click', '.cs-open-overlay', ->
				overlay = $(this).attr('data-overlay')
				openOverlay(overlay)

			$(document).on 'click', '.cs-overlay__close', ->
				$('.cs-overlay').removeClass('is-open').hide(0)
				$('html, body').removeClass('scroll-lock')
				$('.c-product').removeClass('is-blured')

		# customSelect: ->
		# 	closeAllSelect = (elmnt) ->
		# 		#a function that will close all select boxes in the document, except the current select box:
		# 		arrNo = []
		# 		options = $('.select-items')
		# 		selected = $('.select-selected')
		# 		i = 0
		# 		while i < selected.length
		# 			if elmnt == selected[i]
		# 					arrNo.push i
		# 			else
		# 				$(selected[i]).removeClass('select-arrow-active')
		# 			i++

		# 		i = 0
		# 		while i < options.length
		# 			if arrNo.indexOf(i)
		# 				$(options[i]).addClass('select-hide')
		# 			i++
		# 		return

		# 	customSelect = $('.custom-select')
		# 	i = 0
		# 	while i < customSelect.length
		# 		selElmnt = customSelect[i].getElementsByTagName('select')[0]

		# 		#for each element, create a new DIV that will act as the selected item
		# 		currentElement = document.createElement('DIV')
		# 		$(currentElement).addClass('select-selected')
		# 		$(currentElement).html(selElmnt.options[selElmnt.selectedIndex].innerHTML)
		# 		customSelect[i].appendChild currentElement

		# 		# for each element, create a new DIV that will contain the option list
		# 		allOptions = document.createElement('DIV')
		# 		$(allOptions).addClass('select-items select-hide')
		# 		j = 1

		# 		while j < selElmnt.length
		# 			# for each option in the original select element, create a new DIV that will act as an option item:
		# 			c = document.createElement('DIV')
		# 			c.innerHTML = selElmnt.options[j].innerHTML

		# 			c.addEventListener 'click', (e) ->
		# 				#when an item is clicked, update the original select box, and the selected item
		# 				s = @parentNode.parentNode.getElementsByTagName('select')[0]
		# 				h = @parentNode.previousSibling
		# 				i = 0
		# 				while i < s.length
		# 					if s.options[i].innerHTML == @innerHTML
		# 						s.selectedIndex = i
		# 						h.innerHTML = @innerHTML
		# 						y = @parentNode.getElementsByClassName('same-as-selected')
		# 						k = 0
		# 						while k < y.length
		# 							y[k].removeAttribute 'class'
		# 							k++
		# 						@setAttribute 'class', 'same-as-selected'
		# 						break
		# 					i++
		# 				h.click()
		# 				return
		# 			allOptions.appendChild c
		# 			j++

		# 		customSelect[i].appendChild allOptions
		# 		currentElement.addEventListener 'click', (e) ->
		# 			#when the select box is clicked, close any other select boxes, and open/close the current select box:

		# 			e.stopPropagation()
		# 			closeAllSelect this
		# 			@nextSibling.classList.toggle 'select-hide'
		# 			@classList.toggle 'select-arrow-active'
		# 			return
		# 		i++

		# 	document.addEventListener 'click', closeAllSelect

		parallax: ->
			$(window).scroll ->
				scrollTop = $(window).scrollTop()
				$('.parallax-this').css( 'transform', 'translate3d(0, ' + scrollTop * .15 + 'px, 0)' )

				# Check if front page
				if ( window.location.pathname == '/' )
					storyPosTop = $('.c-home__story').position().top
					if scrollTop + $(window).height() > storyPosTop - 100
						$('.c-home__story__bg').css( 'transform', 'translate3d(0, ' + scrollTop * .15 + 'px, 0)' )

		fadeUpAnimation: ->
			$('.o-animateup').viewportChecker
				classToAdd: 'animated'
				offset: 60

		gallery: ->
			loadCSS("#{@themeurl}/assets/css/jquery.fancybox.css")
			$.getScript "#{@themeurl}/assets/lib/jquery.fancybox.min.js", ->
			$('.fancybox').fancybox({
				beforeShow: ->
					alt = this.element.find('img').attr('alt')
					this.inner.find('img').attr('alt', alt)
					this.title = alt
				})

			gallery = $('.gallery')
			if gallery.length > 0
				gallery.addClass('o-layout')
				$('.gallery-item').addClass('o-layout__item u-lap-one-half u-wide-palm-one-third u-wall-one-quarter')

		lazyLoad: ->
			$('.lazyload').Lazy({
				threshold: 400,
				effect: 'fadeIn',
			})

		slider: ->
			if $('.c-slider').length > 0
				$('.c-slider').cycle({
					slides: '.c-slide'
					'auto-height': 'calc'
					'timeout': 5 * 1000
					'speed': 600
				})

			if $('.c-product-slider').length > 0
				$('.c-product-slider').cycle({
					slides: '.c-product-slide'
					'auto-height': "calc"
					'timeout': 100 * 1000
					'speed': 10
					'fx': 'scrollHorz'
					'swipe': true
				})

		flickitySlider: ->
			$(".cs-carousel").flickity({
				# options
				cellAlign: "left",
				contain: true,
				freeScroll: true,
				# wrapAround: true,
				prevNextButtons: true,
				# groupCells: '80%',
				fullscreen: true,
				lazyLoad: true,
			})
			$(".cs-carousel").on( 'dragStart.flickity', () =>
				$(".cs-carousel").css('pointer-events', 'none')
			)
			$(".cs-carousel").on('dragEnd.flickity', () =>
				$(".cs-carousel").css('pointer-events', 'all')
			)

	$ ->
		site.onready()

	$(window).load ->
		site.onload()

)(jQuery)