<?php
/*
 * Template Name: Parent Page
 */
?>

<?php get_header(); ?>

<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<article <?php post_class(); ?>>
			<div class="c-parent">
				<!-- STAGE SECTION -->
				<div class="c-parent c-parent__stage t-align-center">
				<div class="c-parent__stage__bg parallax-this cover" style="background-image:url(<?php echo get_field('stage_background') ;?>)"></div>
					<div class="c-parent__stage__shade cover"></div>
					<div class="o-wrapper">
						<div class="c-parent__container">
							<h1 class="u-alpha"><?php the_title(); ?></h1>
						</div>
					</div>
				</div>

				<!-- CONTENT SECTION -->
				<div class="c-parent c-parent__content">
					<div class="o-wrapper">
						<?php the_content(); ?>
					</div>
				</div>
	
			<?php
				$items = new WP_Query();
				$items->query(array(
					'post_type' => 'page',
					'status' => 'published',
					'posts_per_page' => -1,
					'post_parent' => $post->ID,
					'paged' => $paged
				));

				$orig_query = $wp_query;
				$wp_query = $items;
				get_template_part( 'loop','row' );
				$wp_query = $orig_query;
			?>
			<?php wp_link_pages(); ?>
		</article>
	<?php endwhile; ?>
</main>
<!-- <?php get_sidebar(); ?> -->

<?php get_footer(); ?>