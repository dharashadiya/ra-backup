</div><!-- .c-content -->

<!-- FAQs -->
<div class="cover c-faq c-section--overlay cs-overlay faqs" style="display: none;">
	<a href="javascript:void(0)" title="close Size Chart" class="c-section--overlay__close cs-overlay__close"><?php echo svgicon('close', '0 0 30 30'); ?></a>
	<div class="c-section--overlay__container">
		<div class="o-wrapper">
			<h1 class="c-section--overlay__heading">Delivery &amp; Returns</h1>
			<div class="c-section--overlay__copy imp"><?php echo get_post_field('post_content', get_page_by_title('Frequently Asked Questions')->ID); ?></div>
			<div class="c-section--overlay__body">
				<?php get_template_part( 'partials/faqs' ); ?>
			</div>
			<a href="javascript:void(0)" class="product-link cs-overlay__close" title="Goto Product">Back to Product</a>
		</div>
	</div>
</div>

<div class="c-footer t-align-center">
	<div class="c-footer__map">
		<!-- <?php echo 'map loads here' ?> -->
		<!-- <div id="map" class="c-map"></div> -->
	</div>
	<?php if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
	<div class="c-footer__widgets">
		<?php dynamic_sidebar( 'footer-sidebar' ); ?>
	</div>
	<?php endif; ?>
	<!-- INSTAGRAM SECTION -->
	<?php if (!is_page(array('bag', 'checkout'))) : ?>
	<div class="c-footer c-footer__instagram t-align-center">
		<div class="o-wrapper o-animateup">
			<h2>#SUKWorkwear</h2>
			<p class="imp">A fierce community of creators & innovators  <?php echo svgicon('instaicon', '0 0 27 27') ?></p>
			<div class="c-instagram__feeds">
				<?php echo do_shortcode('[wdi_feed id="1"]') ?>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="c-footer__newsletter">
		<div class="o-wrapper o-animateup">
			<h3>Newsletter Signup</h3>
			<?php echo do_shortcode(get_field('newsletter_form','options')); ?>
		</div>
	</div>
	<div class="c-footer__content">
		<div class="o-wrapper">
			<div class="o-layout">
				<div class="c-footer__menu o-layout__item u-desk-one-fifth u-one-whole">
					<ul class="t-align-left">
						<li class="o-animateup"><a title="Goto home" href="/">Home</a></li>
						<li class="o-animateup"><a title="Goto Shop" href="/shop">Shop</a></li>
						<li class="o-animateup"><a title="Goto our story" href="/our-story">Our Story</a></li>
						<li class="o-animateup"><a title="Goto ethics" href="/ethics">Ethics</a></li>
					</ul>
				</div><div class="c-footer__menu c-footer__menu-2 o-layout__item u-desk-one-fifth u-one-whole">
					<ul class="t-align-left">
						<li class="o-animateup"><a title="View Contact information" href="/contact">Contact</a></li>
						<li class="o-animateup"><a title="View Frequently asked questions" href="/faq/">FAQs</a></li>
						<li class="o-animateup"><a title="View Shipping policy" href="/faq/#shipping">Shipping</a></li>
						<li class="o-animateup"><a title="View Returns policy" href="/faq/#returns">Returns</a></li>
					</ul>
				</div><div class="c-footer__logo o-layout__item u-desk-one-fifth u-one-whole o-animateup">
					<a title="Goto homegage" href="/"><?php echo svgicon('footerlogo', '0 0 108 172') ; ?></a>
				</div><div class="c-footer__social o-layout__item u-desk-one-fifth u-one-whole">
					<ul>
						<!-- <li class="o-animateup"><a title="Follow us on twitter" target="_blank" href="https://twitter.com/sukworkwear" rel="noopener"><?php echo svgicon('twittericon', '0 0 27 27') ; ?></a></li> -->
						<li class="o-animateup"><a title="Like us on Facebook" target="_blank" href="https://www.facebook.com/sukworkwear/" rel="noopener"><?php echo svgicon('fbicon', '0 0 27 27') ; ?></a></li>
						<li class="o-animateup"><a title="Follow us on Instagram" target="_blank" href="https://www.instagram.com/sukworkwear/" rel="noopener"><?php echo svgicon('instaicon', '0 0 27 27') ; ?></a></li>
					</ul>
				</div><div class="c-footer__contact o-layout__item u-desk-one-fifth u-one-whole">
					<div class="contact">
						<h3 class="o-animateup">Contact</h3>
						<?php
							$email = get_field('email','options');
							$phone = get_field('phone','options');
						?>
						<?php if ($email) { ?>
							<div class="o-animateup">
								<span>e:&nbsp;</span><a title="Email us on <?php echo $email;?>" class="email" href="mailto:<?php echo $email;?>"><?php echo $email;?></a>
							</div>
						<?php }
						if ($phone) { ?>
							<div class="o-animateup">
								<span>p:&nbsp;</span><a title="call us on <?php echo $phone;?>" class="phone" href="tel:<?php echo $phone;?>"><?php echo $phone;?></a>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="c-footer__copy">
		<div class="o-wrapper">
			<p>&copy;&nbsp;Sük&nbsp;Workwear&nbsp;<?php echo date("Y"); ?>.  <a title="View Privacy Policies" href="/privacy/">Privacy</a> &nbsp;|&nbsp; <a title="View Terms and Conditions" href="/terms/">Terms&nbsp;&amp;&nbsp;Conditions</a> <span class="hide-on-mobile">&nbsp;|&nbsp;</span> <span>Website&nbsp;Design&nbsp;by&nbsp;<a title="Visit Rock Agency" href="https://rockagency.com.au/">Rock&nbsp;Agency</a>.</span></p>
		</div>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>