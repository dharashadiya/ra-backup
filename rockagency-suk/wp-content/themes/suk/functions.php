<?php
// Check if Local or Staging website
function is_local_or_staging() {
	if ($_SERVER['SERVER_ADDR'] == '103.27.32.31' || $_SERVER['SERVER_ADDR'] == '127.0.0.1') {
		return true;
	} else {
		return false;
	}
}

define( 'GOOGLE_ANALYTICS', 'UA-70041565-51');
include('functions/boilerplate.php');
include('functions/admin-config.php');
include('functions/theme-helpers.php');
include('functions/clean-gallery.php');
include('functions/acf-config.php');
include('functions/shortcodes.php');
include('functions/woocommerce-helpers.php');
include('functions/woocommerce-customisation.php');

// Author meta tag
define( 'Nikita Shah', 'SUK Theme');

// Enable post thumbnails
add_theme_support( 'post-thumbnails' );

// Enable nav menus
add_theme_support( 'nav-menus' );

// Custom image sizes
add_image_size( 'featured-thumb', 300, 180, true );

// make custom image size choosable from add media
add_filter( 'image_size_names_choose', 'site_custom_sizes' );
function site_custom_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'featured-thumb' => 'Featured Thumb',
	) );
}

// Register nav menus
function register_site_menus() {
	register_nav_menu( 'primary-menu', 'Primary Menu' );
}
add_action('after_setup_theme', 'register_site_menus');

// Register widget areas
function site_widgets_init() {
	// Register main sidebar
	register_sidebar( array(
		'name' => 'Sidebar Widget Area',
		'id' => 'aside-sidebar',
		'description' => 'Widget in this area will be shown in sidebar area on all pages',
		'before_widget' => '<section id="%1$s" class="c-widget c-widget--side %2$s %1$s"><div class="c-widget__content">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="c-widget__heading">',
		'after_title' => '</h3>'
	));

	// Register footer sidebar
	register_sidebar( array(
		'name' => 'Footer Widget Area',
		'id' => 'footer-sidebar',
		'description' => 'Widget in this area will be shown in footer area on all pages',
		'before_widget' => '<section id="%1$s" class="c-widget c-widget--footer %2$s %1$s"><div class="c-widget__content">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="c-widget__heading">',
		'after_title' => '</h3>'
	));
}
add_action( 'widgets_init', 'site_widgets_init' );

// Remove contact from styles from all pages
add_filter('wpcf7_load_js', '__return_false');
add_filter('wpcf7_load_css', '__return_false');

// Load scripts using wp's enqueue
function site_scripts() {
	if ( !is_admin() ) {
		// Remove jquery that wordpress includes
		wp_deregister_script('jquery');
		$site_js = (is_localhost()) ? JS . '/site.js' : JS . '/site.min.js';
		$site_css = (is_localhost()) ? CSS . '/screen.dev.css' : CSS . '/screen.min.css';

		// Include all js including jQuery and register with name 'jquery' so other jquery dependable scripts load as well
		wp_enqueue_script('jquery',$site_js, false, $GLOBALS['site_cachebust'], true);
		wp_enqueue_style('site', $site_css, false);
	}
}
add_action('wp_enqueue_scripts', 'site_scripts');

// Boolean to check if the cf7 scripts are required
function load_cf7_scripts() {
	// return ( is_page('contact') );
	return true;
}

// Load cf7 scripts if required
function site_load_cf7_scripts() {
	if ( load_cf7_scripts() ) {
		if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
			wpcf7_enqueue_scripts();
		}
		// Not loading wpcf7 styles at all, if you need wpcf7 styles uncomment below lines
		// if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
		//   wpcf7_enqueue_styles();
		// }
	}
}
add_action( 'wp', 'site_load_cf7_scripts' );

// Allow search to search through pages and posts. Add any custom post type in array to search by them as well.
function site_search($query) {
	if (!is_admin() && $query->is_search) {
		$query->set('post_type', array('page','post'));
	}
	return $query;
}
add_filter('pre_get_posts', 'site_search');


// Instagram API
add_action( 'wp_ajax_nopriv_instagram_api', 'instagram_api' );
add_action( 'wp_ajax_instagram_api', 'instagram_api' );
function instagram_api() {
	$username = $_POST['data'];
	$insta_source = file_get_contents('http://instagram.com/'. $username);
	$shards = explode('window._sharedData = ', $insta_source);
	$insta_json = explode(';</script>', $shards[1]);
	$insta_array = json_decode($insta_json[0], TRUE);
	$insta_feed = array();
	foreach ($insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'] as $item) {
		$item = $item['node'];
		$insta_feed[] = array(
			'link' => 'http://instagram.com/p/' . $item['shortcode'],
			'image' => $item['display_url'],
			'thumbnails' => array(
				'small' => $item['thumbnail_resources'][2]['src'],
				'medium' => $item['thumbnail_resources'][3]['src'],
				'large' => $item['thumbnail_resources'][4]['src'] ),
			'date' => $item['taken_at_timestamp'],
			'caption' => $item['caption'],
			// 'caption' => false,
			'likes' => $item['edge_liked_by']['count'],
			'comments' => $item['edge_media_to_comment']['count'],
			'likes' => $item['edge_liked_by']['count']
		);
	}
	$insta_array = json_encode($insta_feed);
	wp_die($insta_array);
}

// REMOVE GUTTENBURG
// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

?>