<?php

// if ( ! function_exists( 'print_attribute_radio' ) ) {
//     function print_attribute_radio( $checked_value, $value, $label, $name ) {
//         global $product;

//         $input_name = 'attribute_' . esc_attr( $name ) ;
//         $esc_value = esc_attr( $value );
//         $product_id = $product->get_id();
//         $id = esc_attr( $name . '_v_' . $value . $product_id ); //added product ID at the end of the name to target single products
//         $checked = checked( $checked_value, $value, false );
//         $filtered_label = apply_filters( 'woocommerce_variation_option_name', $label, esc_attr( $name ) );
//         printf( '<label class="radio"><input type="radio" name="%1$s" value="%2$s" id="%3$s" %4$s><span class="checkmark"></span><span class="name imp">%5$s</span><span data-select-product="%6$s"></span></label>', $input_name, $esc_value, $id, $checked, $filtered_label, $product_id);
//     }
// }

// remove default thumbnail function
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

//change select options first lebel
add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'my_wc_filter_dropdown_args', 10 );
function my_wc_filter_dropdown_args( $args ) {
    if (strpos($args['attribute'], 'pa') !== false ) {
        $args['show_option_none'] = 'Choose a ' . ucfirst(str_replace('-', ' ', str_replace('pa_', '', $args['attribute'] ) ) );
    } else {
        $args['show_option_none'] = 'Choose a ' . $args['attribute'];
    }
    return $args;
}

// Update cart count after add to cart AJAX call
add_filter( 'woocommerce_add_to_cart_fragments', 'header_cart_count', 10, 1 );
function header_cart_count( $fragments ) {
    $bag = '';
    if (WC()->cart->get_cart_contents_count() == 0) {
        $bag = '<div class="header-cart-count bag-empty product-in"><span class="count"></span>' . get_svgicon('bag', '0 0 21 21') . '</div>';
    } else {
        $bag = '<div class="header-cart-count bag-full product-in"><span class="count">(' . WC()->cart->get_cart_contents_count()  . ')</span>' . get_svgicon('bag-full', '0 0 21 21') . '</div>';
    }
    $fragments['div.header-cart-count'] = $bag;
    return $fragments;
}

// Add placeholders to checkout fields
add_filter( 'woocommerce_checkout_fields' , 'site_custom_override_checkout_fields', 10 );
function site_custom_override_checkout_fields( $fields ) {
    $fields['billing']['billing_first_name']['placeholder'] = 'Enter First Name';
    $fields['billing']['billing_last_name']['placeholder'] = 'Enter Last Name';
    $fields['billing']['billing_company']['placeholder'] = 'Enter Company';
    $fields['billing']['billing_address_1']['label'] = 'Enter Street Address';
	$fields['billing']['billing_country']['placeholder'] = 'Select Country';
    $fields['billing']['billing_city']['placeholder'] = 'Enter Suburb';
	$fields['billing']['billing_postcode']['placeholder'] = 'Enter Postcode';
    $fields['billing']['billing_phone']['placeholder'] = 'Enter Phone';
	$fields['billing']['billing_email']['placeholder'] = 'Enter Email';
    $fields['billing']['billing_state']['placeholder'] = 'Select State';

    $fields['shipping']['shipping_first_name']['placeholder'] = 'Enter First Name';
    $fields['shipping']['shipping_last_name']['placeholder'] = 'Enter Last Name';
    $fields['shipping']['shipping_company']['placeholder'] = 'Enter Company';
    $fields['shipping']['shipping_address_1']['placeholder'] = 'Enter Street Address';
	$fields['shipping']['shipping_country']['placeholder'] = 'Select Country';
    $fields['shipping']['shipping_city']['placeholder'] = 'Enter Suburb';
	$fields['shipping']['shipping_postcode']['placeholder'] = 'Enter Postcode';
    $fields['shipping']['shipping_phone']['placeholder'] = 'Enter Phone';
	$fields['shipping']['shipping_email']['placeholder'] = 'Enter Email';
	$fields['shipping']['shipping_state']['placeholder'] = 'Select State';

	return $fields;
}


// Changing default placeholder and label for checkout form.
add_filter( 'woocommerce_default_address_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
     $fields['address_1']['placeholder'] = 'Please Select Your Address';
     $fields['address_1']['label'] = 'Enter full address and choose from dropdown menu';
     return $fields;
}

/**
 * @snippet       Display $0.00 Amount For Free Shipping Rates @ WooCommerce Cart & Checkout
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=72869
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.1.2
 */
add_filter( 'woocommerce_cart_shipping_method_full_label', 'suk_add_0_to_shipping_label', 10, 2 );
function suk_add_0_to_shipping_label( $label, $method ) {
    if ( ! ( $method->cost > 0 ) ) {
        $label .= ': ' . wc_price(0);
    }
    return $label;
}

// change total number of products to be shown on page to 999 (remove pagination)
add_filter('loop_shop_per_page', create_function('$cols', 'return 999;'), 20);


// Update Out of Stock Message
add_filter( 'woocommerce_get_availability', 'wcs_custom_get_availability', 1, 2);
function wcs_custom_get_availability( $availability, $_product ) {
    // Change In Stock Text
    // if ( $_product->is_in_stock() ) {
    //     $availability['availability'] = __('Available!', 'woocommerce');
    // }
    // Change Out of Stock Text
    if ( ! $_product->is_in_stock() ) {
        // $availability['availability'] = __('Out of stock! <br />To be notified when this product is in stock <a href="#backorder-notification-container" class="backorder-notification">click here</a>.', 'woocommerce');
        $availability['availability'] = __('Out of stock! <br />To get notified when this product is in stock, please subscribe here. <br />', 'woocommerce');
    }
    return $availability;
}

// Hide Afterpay if there is Gift Card in Cart
add_filter('woocommerce_available_payment_gateways', 'conditional_payment_gateways', 10, 1);
function conditional_payment_gateways( $available_gateways ) {
    // Not in backend (admin)
    if( is_admin() )
        return $available_gateways;

    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $gift_card = false;

        // Get all product's Categories
        $terms = get_the_terms( $cart_item['product_id'], 'product_cat' );
        foreach ($terms as $term) {
            $product_cat .= ', '. $term->name;
        }

        // Check if Gift Card
        if (strpos($product_cat, 'Gift Cards') !== false) {
            $gift_card = true;
        }
    }
    // Remove Afterpay payment gateway for Gift Cards
    if($gift_card)
        unset($available_gateways['afterpay']); // unset 'cod'

    return $available_gateways;
}

// Display Tracking Number from Dear field value on the order edit page
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'ra_tracking_no_order_meta', 10, 1 );
function ra_tracking_no_order_meta($order){
	$carrier = get_post_meta( $order->id, 'CarrierName', true );
	$tracking_no = get_post_meta( $order->id, 'TrackingNumber', true );
    if ($tracking_no) {
        echo '<p class="form-field form-field-wide"><strong>Tracking Number: </strong>' . $tracking_no . '<br>';
		if ($carrier && $carrier == 'Couriers Please') {
        	echo '<strong>Carrier Name: </strong><a target="_blank" href="https://www.couriersplease.com.au/tools-track/no/' . $tracking_no . '">' . $carrier . '</a>';
		} else {
        	echo '<strong>Carrier Name: </strong><a target="_blank" href="https://auspost.com.au/mypost/track/#/details/' . $tracking_no . '">' . $carrier . '</a>';
		}
		echo '</p>';
    }
}
// This will send customer emails to dev
// add_filter( 'woocommerce_email_headers', 'ra_custom_headers_filter_function', 10, 2);
// function ra_custom_headers_filter_function( $headers, $object ) {
//     if ( in_array( $object, array( 'customer_completed_order', 'customer_processing_order' ) ) ) {
//         $headers .= 'BCC: Rock Agency <jaydev+suk@rockagency.com.au>' . "\r\n";
//     }
//     return $headers;
// }