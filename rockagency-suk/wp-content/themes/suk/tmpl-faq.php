<?php
/*
 * Template Name: FAQ Page
 */
?>

<?php get_header(); ?>

<main id="Main" class="c-main-content o-main" role="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="c-faq c-section--pad c-cms-content">
			<div class="o-wrapper o-wrapper--small">
				<h1 class="o-animateup"><?php the_title(); ?></h1>
				<div class="imp o-animateup"><?php the_content(); ?></div>
				<?php get_template_part( 'partials/faqs' ); ?>
			</div>
			<!-- <?php get_template_part( 'partials/post-meta' ); ?> -->
		</div>
	<?php wp_link_pages(); ?>
	</article>
	<?php endwhile; ?>
</main><?php // get_sidebar(); ?>

<?php get_footer(); ?>