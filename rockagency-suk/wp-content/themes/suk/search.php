<?php get_header(); ?>

<div class="o-wrapper">
	<div class="o-layout">
		<main id="Main" class="c-main-content o-main o-layout__item" role="main">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h1 class="u-alpha"><?php printf( __( 'Search results for: "%s"'), get_search_query() ); ?></h1>
				<?php if ( have_posts() ) : ?>
					<?php get_template_part( 'loop', 'row' ); ?>
				<?php else : ?>
					<p>No search results found.</p>
				<?php endif; ?>
			</article>
		</main>
		<!-- <?php get_sidebar(); ?> -->
	</div>
</div>

<?php get_footer(); ?>